package DataStructure.LinkedList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahulrana on 04/01/19.
 */
public class LinkedList {


    Node head;

    static class Node {
        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }

    public static void printLinkedList(Node n) {
        System.out.print("\n");
        Node n1 = n;
        while (n != null) {
            System.out.print(n.data + "\n");
            n = n.next;
        }
    }

    /**
     * add a node at the start of linked list
     *
     * @param newData
     */
    public void push(int newData) {
        Node n1 = new Node(newData);
        n1.next = head;
        head = n1;
    }

    /**
     * Add node after a specific node
     *
     * @param prevNode
     * @param data
     */
    // 5 step process for inserting a node after given node
    public void insertAfter(Node prevNode, int data) {

        // 1. check if node is  null
        if (prevNode == null) {
            System.out.print("Previous node can not be null");
            return;
        }

        // 2. create new node 3. assign value
        Node n1 = new Node(data);

        //4. make next of the new node as next of the previous node
        n1.next = prevNode.next;

        // 5. make previous node next as new node
        prevNode.next = n1;
    }

    /**
     * Add Node at the end of the linked list
     *
     * @param data
     */
    public void addLast(int data) {
        // create a new node
        Node n1 = new Node(data);

        if (head == null) {
            // if the linked list is empty then make the new node as head and return
            head = n1;
            return;
        }

        n1.next = null;

        // traverse till the end of linked list
        Node last = head;
        while (last.next != null) {
            last = last.next;
        }

        last.next = n1;
    }

    public void deleteKey(int key) {

        Node temp = head, prev = null;

        // if head node itself holds the key to be deleted
        if (temp != null && temp.data == key) {
            head = temp.next; // shift the head to next node
            return;
        }

        // search for the key, also keep track of the previous node
        while (temp != null && temp.data != key) {
            prev = temp;
            temp = temp.next;
        }

        if (temp == null) {
            return; // if the key was not found
        }

        prev.next = temp.next;
    }

    public void deleteNode(int position) {
        if (head == null) // if linked list is empty
            return;

        Node temp = head;

        // if head need to be removed
        if (position == 0) {
            head = temp.next;
            return;
        }

        // find the previous node of the node to be deleted
        for (int i = 0; temp != null && i < position - 1; i++) {
            temp = temp.next;
        }

        // if position is more then the number of nodes
        if (temp == null || temp.next == null) {
            return;
        }

        // Node->next need to be deleted
        // store pointer of the next of the node to be deleted
        Node next = temp.next.next;

        temp.next = next;
    }

    public int getCount() {
        return getCountRec(head);
    }

    public int getCountRec(Node node) {
        if (node == null) {
            return 0;
        }

        return 1 + getCountRec(node.next);
    }

    public boolean searchNode(Node head, int key) {
        Node temp = head;

        while (temp != null) {
            if (temp.data == key)
                return true;
            temp = temp.next;
        }
        return false;
    }

    public boolean searchNodeRec(Node head, int key) {

        if (head == null)
            return false;

        if (head.data == key)
            return true;

        return searchNodeRec(head.next, key);
    }

    public int getNthNode(int index) {
        Node current = head;
        int count = 0;

        while (current != null) {
            if (count == index)
                return current.data;

            count++;
            current = current.next;
        }

        assert (false);
        return 0;
    }

    public int getNthFromLast(int index) {
        // first get the length of the linked list
        int length = getCount();

        // if index is more than length return
        if (index > length)
            return 0;

        // get the (len - n+1)th node from the start, its not possible to back trace in linked list
        Node temp = head;
        for (int i = 1; i < length - index + 1; i++) {
            temp = temp.next;
        }

        if (temp == null)
            return 0;

        return temp.data;
    }

    public void printNthFromLastUsingTwoReference(int n) {
        Node mainPointer = head;
        Node refPointer = head;

        int count = 0;

        if (head != null) {
            while (count < n) {

                if (refPointer == null) {
                    System.out.print(n + " is greater than the num. of nodes in the list");
                    return;
                }

                refPointer = refPointer.next;
                count++;
            }

            while (refPointer != null) {
                refPointer = refPointer.next;
                mainPointer = mainPointer.next;
            }

            System.out.print("Node num. " + n + " from the last is " + mainPointer.data);

        }
    }

    public void printMiddleUsingSlowAndFastPointer() {
        Node slowPointer = head;
        Node fastPointer = head;

        if (head != null) {
            while (fastPointer != null && fastPointer.next != null) {
                fastPointer = fastPointer.next.next;
                slowPointer = slowPointer.next;
            }

            System.out.print("\nmiddle element is:" + slowPointer.data);
        }

    }

    public int keyOccurence(int key) {
        int count = 0;
        Node temp = head;
        while (temp != null) {
            if (temp.data == key)
                count++;

            temp = temp.next;
        }
        return count;
    }

    public boolean detectLoopInALinkedList(Node n) {
        List<Node> lNode = new ArrayList<>();
        while (n != null) {
            if (lNode.contains(n)) {
                return true;
            }

            lNode.add(n);
            n = n.next;
        }
        return false;
    }

    public boolean detectLoopUsingSlowAndFastPointer() {
        Node sPointer = head;
        Node fPointer = head;
        while (sPointer != null && fPointer != null && fPointer.next != null) {
            fPointer = fPointer.next.next;
            sPointer = sPointer.next;
            if (sPointer == fPointer)
                return true;
        }
        return false;
    }

    public int getLoopLength() {
        Node sPointer = head;
        Node fPointer = head;
        while (sPointer != null && fPointer != null && fPointer.next != null) {
            fPointer = fPointer.next.next;
            sPointer = sPointer.next;
            if (sPointer == fPointer)
                return countNodes(sPointer);
        }
        return 0;
    }

    public int countNodes(Node n) {
        Node temp = n;
        int count = 1;
        while (temp.next != n) {
            temp = temp.next;
            count++;
        }


        return count;
    }

    public void reverseLinkedList(Node n) {
        Node current = n;
        Node next;
        Node previous = null;

        while (current != null) {
            next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        head = previous;
    }
}
