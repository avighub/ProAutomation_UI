package framework.utility.common;

import com.aventstack.extentreports.ExtentTest;
import framework.utility.globalConst.ConfigInput;

/**
 * Created by rahulrana on 26/01/18.
 */
public class App {
    private static ExtentTest pNode;

    public static App init(ExtentTest t1){
        pNode = t1;
        return new App();
    }

    public void openApplication(){
        try{
            DriverFactory.getDriver().get(ConfigInput.url);
            pNode.info("Navigate to URL");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
