package framework.utility.reportManager;

import framework.utility.common.DriverFactory;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;

/**
 * Created by rahul.rana on 6/7/2017.
 */
public class ScreenShot {

    public static String captureScreen() {
        String fileName =  System.currentTimeMillis() + ".png";
        try {
            File src = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(src, new File("./reports/ScreenShots/" + fileName));
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return "./ScreenShots/" + fileName;
    }

}
