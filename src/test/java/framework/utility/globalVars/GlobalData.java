package framework.utility.globalVars;

import java.util.List;

/**
 * Created by rahul.rana on 5/7/2017.
 * Use variable that shall be loaded once during the entire execution
 */
public class GlobalData {

    public static List<String> someList;
}
