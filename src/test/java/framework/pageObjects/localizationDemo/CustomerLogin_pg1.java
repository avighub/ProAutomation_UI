package framework.pageObjects.localizationDemo;

import com.aventstack.extentreports.ExtentTest;
import com.gargoylesoftware.htmlunit.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rahulrana on 06/12/17.
 */
public class CustomerLogin_pg1 {

    private ExtentTest pageInfo = null;
    /**
     * Page Objects
     */
    @FindBy (className = "paneltitle")
    WebElement panelTitle;

    @FindBy (id = "Email")
    WebElement txtEmail;

    @FindBy (id = "Password")
    WebElement txtPassword;

    @FindBy (className = "btn-primary")
    WebElement btnSubmit;

    public CustomerLogin_pg1(WebDriver driver, ExtentTest t1){
        PageFactory.initElements(driver, this);
        pageInfo = t1;
    }


}
