package framework.pageObjects;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.objectIdentification.ObjectIdentificationPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import sun.jvm.hotspot.debugger.Page;

/**
 * Created by rahulrana on 13/02/18.
 */
public class PageForLinkedInRegistration_pg1 extends PageInit {

    @FindBy (id = "reg-firstname1")
    private WebElement txtFirstName;

    public void setFirstName(String text) throws Exception {
        setText(txtFirstName, text, "First Name");
    }

    public PageForLinkedInRegistration_pg1(ExtentTest t1) {
        super(t1);
    }
    public static PageForLinkedInRegistration_pg1 init(ExtentTest t1){
        return new PageForLinkedInRegistration_pg1(t1);
    }
}
