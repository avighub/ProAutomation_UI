package framework.pageObjects.userWizard;

import com.aventstack.extentreports.ExtentTest;
import framework.utility.common.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rahulrana on 16/12/17.
 */
public class UserWizard_pg2 {
    private static ExtentTest pageInfo;

    /*
   Common Page objects like navigate, logout, etc
    */
    @FindBy(name = "email")
    private WebElement txtEmail;

    @FindBy(name = "password")
    private WebElement txtPassword;

    @FindBy(name = "password2")
    private WebElement txtPassword2;

    @FindBy(id = "wizard-submit-next-02")
    private WebElement btnComplete;

    @FindBy(id = "success-message")
    private WebElement headerSuccessMessage;

    public UserWizard_pg2 setEmail(String text){
        txtEmail.sendKeys(text);
        pageInfo.info("Set Email - "+ text);
        return this;
    }

    public UserWizard_pg2 setPassword(String text){
        txtPassword.sendKeys(text);
        pageInfo.info("Set Password - "+ text);
        return this;
    }

    public String getSuccessmessage(){
        return headerSuccessMessage.getText();
    }

    public UserWizard_pg2 setConfirmPassword(String text){
        txtPassword2.sendKeys(text);
        pageInfo.info("Set Confirm Password - "+ text);
        return this;
    }

    public void clickComplete(){
        btnComplete.click();
        pageInfo.info("Click on Complete!");
    }

    public static UserWizard_pg2 init(ExtentTest t1){
        pageInfo = t1;
        return PageFactory.initElements(DriverFactory.getDriver(), UserWizard_pg2.class);
    }
}
