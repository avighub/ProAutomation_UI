package scripts.demoScripts;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import framework.utility.common.ExcelUtils;
import framework.utility.globalConst.FilePath;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by rahulrana on 19/11/17.
 */
public class DataDrivenTest {
    private WebDriver driver;
    ExtentReports extent;

    /**
     * Get the Driver
     */
    @BeforeClass
    public void beforeClassMethod() {
        // initialize driver
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // initialize reports
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("reports/extent.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
    }

    @Test
    public void run() throws Exception {
        Object[][] data = ExcelUtils.getTableArray(FilePath.testDataPath + "DataSheet.xlsx", "Sheet 1");

    }

    @AfterClass
    public void afterClassMethod(){
        driver.quit();
    }
}
