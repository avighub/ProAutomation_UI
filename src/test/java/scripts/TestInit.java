package scripts;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import framework.utility.common.Assertion;
import framework.utility.common.DriverFactory;
import framework.utility.common.Navigation;
import framework.utility.globalConst.ConfigInput;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * Created by rahulrana on 05/11/17.
 */
public class TestInit {
    protected static ExtentReports extent;
    protected ExtentTest pNode;
    protected static WebDriver driver;
    protected WebDriverWait wait;
    protected Assertion assertions;

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite(ITestContext suite) throws Exception {
        // load the automation.properties file
        ConfigInput.init();

        // initialize reports
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("reports/extent.html");
        htmlReporter.config().setChartVisibilityOnOpen(false);
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        // display the Environment details to the extent report Dashboard
        extent.setSystemInfo("Application URL", ConfigInput.url);
        extent.setSystemInfo("Browser", ConfigInput.browser);
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClassRun() {
        // initialize driver
        driver = DriverFactory.getDriver();

        // create the parent node
        pNode = extent.createTest(getClass().getSimpleName());
        Markup m = MarkupHelper.createLabel("Setup for Test: " + getClass().getSimpleName(), ExtentColor.BLUE);
        pNode.info(m);

        wait = new WebDriverWait(driver, ConfigInput.explicitWait); // todo next topic under Synchronization
        assertions = new Assertion(driver, pNode); // todo next topic under assertion & Validation
    }

    @BeforeTest(alwaysRun = true)
    public void beforeTest() {

    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method) {
        System.out.print("START TEST: " + method.getName() + "\n");
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) {

        //assertions.finalizeSoftAssert();

        if (result.getStatus() == ITestResult.SUCCESS) {
            System.out.print(result.getName() + ": Executed Successfully");
            //pNode.pass(result.getName() + ": Test Case Executed Successfully!");
        } else if (result.getStatus() == ITestResult.FAILURE) {
            System.out.print(result.getName() + ": Execution Failed");
            String code = "Method: " + result.getName() + "\n" +
                    "Reason: " + result.getThrowable().toString();
            Markup m = MarkupHelper.createCodeBlock(code);
            //pNode.fail(m);
        } else if (result.getStatus() == ITestResult.SKIP) {
            System.out.print(result.getName() + ": Execution Skipped");
            //pNode.skip(result.getName() + ":Test Case Executed Skipped!");
        }

        extent.flush();
        System.out.print("\nEND TEST: " + result.getName());
    }

    @AfterTest(alwaysRun = true)
    public void afterTest() {
        // todo write your code here
    }


    @AfterClass(alwaysRun = true)
    public void afterClassRun() {
        // Quit the driver
        driver.close();
        driver = null;
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        // todo write your code here
    }

    protected void markSetupAsFailure(Exception e) {
        pNode.fail("Exception: " + e.toString());
        Assert.fail(e.getMessage());
    }

    public void markTestAsFailure(Exception e, ExtentTest t1) {
        e.printStackTrace();
        t1.fail("Test Case Failed due to Exception");
        t1.error(e);
        Assert.fail(e.getMessage());
    }

}
