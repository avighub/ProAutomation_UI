package scripts.PDFBox;

import framework.utility.globalConst.FilePath;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.testng.annotations.Test;

import java.io.*;
import java.util.List;

/**
 * Created by rahulrana on 11/10/18.
 */
public class PrintTextLocations extends PDFTextStripper {
    public PrintTextLocations() throws IOException {
    }


    @Test
    public void run() throws IOException {

        PDDocument document = PDDocument.load(new File(FilePath.pdfFile));
        PDFTextStripper stripper = new PrintTextLocations();
        stripper.setSortByPosition(true);
        stripper.setStartPage(0);
        stripper.setEndPage(document.getNumberOfPages());

        Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
        stripper.writeText(document, dummy);
    }



    /**
     * Override the default functionality of PDFTextStripper.
     */
    @Override
    protected void writeString(String string, List<TextPosition> textPositions) throws IOException {
        for (TextPosition text : textPositions) {
            System.out.println("String[" + text.getXDirAdj() + "," +
                    text.getYDirAdj() + " fs=" + text.getFontSize() + " xscale=" +
                    text.getXScale() + " height=" + text.getHeightDir() + " space=" +
                    text.getWidthOfSpace() + " width=" +
                    text.getWidthDirAdj() + "]" + text.getUnicode());
        }
    }

    /**
     * This will print the usage for this document.
     */
    private static void usage() {
        System.err.println("Usage: java " + PrintTextLocations.class.getName() + " <input-pdf>");
    }
}
