package scripts;

import framework.utility.globalConst.FilePath;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by rahulrana on 11/10/18.
 */
public class PdfTest {

    @Test
    public void run() throws IOException {
        try{
            PDDocument document = PDDocument.load(new File(FilePath.pdfFile));

            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition( true );
            Rectangle rect = new Rectangle( 10, 280, 275, 60 );
            stripper.addRegion( "class1", rect );
            PDPage firstPage = document.getPage(0);
            stripper.extractRegions( firstPage );
            System.out.println( "Text in the area:" + rect );
            System.out.println( stripper.getTextForRegion( "class1" ) );
        }catch (Exception e){
            e.printStackTrace();
        }




    }
}
