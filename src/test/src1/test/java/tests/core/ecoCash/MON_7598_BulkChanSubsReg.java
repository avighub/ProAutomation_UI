package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import com.opencsv.CSVReader;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.pageObjects.userManagement.BulkChUserRegistrationPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

public class MON_7598_BulkChanSubsReg extends TestInit {

    private OperatorUser netAdmin;
    private User chSSA, chUserToRegister;
    private String[] header, userDetails;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            netAdmin = DataFactory.getOperatorUserWithAccess("BLK_CHUSR", Constants.NETWORK_ADMIN);
            chSSA = DataFactory.getChannelUserWithCategory(Constants.SPECIAL_SUPER_AGENT);
            chUserToRegister = new User(Constants.RETAILER, chSSA);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1)
    public void MON_7598_001() {
        ExtentTest t1 = pNode.createNode("MON_7598_01",
                "Verify that the downloaded CSV template has all the newly added fields");

        try {
            Login.init(t1).login(netAdmin);
            String[] expectedHeaders = {"Autosweep Allowed (Y/N)", "ContactIDType", "AllowedDays", "AllowedFormTime", "AllowedToTime", "CommercialField1", "CommercialField2", "CommercialField3", "CommercialField4", "CommercialField5", "CommercialField6", "CommercialField7", "CommercialField8", "CommercialField9", "CommercialField10", "CommercialField11", "ShopName"};
            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t1).navBulkChUserAndSubsciberRegistrationAndModification();
            page.SelectChannelUserRegistrationService();
            BulkChUserAndSubRegAndModPage.clickOnStartDownload();
            Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                    "Able to download CSV template file", t1);
            String fileName = FilePath.dirFileDownloads + "BulkUserRegId.csv";
            CSVReader reader = new CSVReader(new FileReader(fileName));
            String[] header = reader.readNext();
            List headersList = Arrays.asList(header);
            for (String h : expectedHeaders) {
                if (!headersList.contains(h)) {
                    markTestAsFailure("Header not found in template", t1);
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void MON_7598_002() {
        ExtentTest t2 = pNode.createNode("MON_7598_002", "Initiate and approve Bulk Channel User registration");
        try {

            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            Login.init(t2).login(optUsr);

            String csvFile = ExcelUtil.getBulkUserRegistrationFile();

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t2);

            page.navBulkChUserAndSubsciberRegistrationAndModification()
                    .SelectChannelUserRegistrationService();

            ChannelUserManagement.init(t2)
                    .writeBulkChUserRegistrationDetailsInCSV(FilePath.fileBulkChUserReg,
                            chUserToRegister,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            true,
                            false);

            //submit and initiate batch
            String batchId = page.uploadFile(csvFile)
                    .submitCsv()
                    .getBatchId();

            Assertion.verifyActionMessageContain("bulkUpload.user.reg.success", "Batch with id " + batchId + " successfully initiated.", t2);

            //Approve batch
            page.navBulkRegistrationAndModificationApproval();
            page.selectDateRangeForApproval(new DateAndTime().getDate(0), new DateAndTime().getDate(0));
            page.ClickOnSubmit();
            page.approveUsingBatchId(batchId);
            Assertion.verifyActionMessageContain("bulk.approval.successful", "Batch with id " + batchId + " successfully approved.", t2);

            //get the values entered in the newly added fields
            CSVReader reader = new CSVReader(new FileReader(csvFile));
            header = reader.readNext();
            userDetails = reader.readNext();
            userDetails = Arrays.copyOfRange(userDetails, userDetails.length - 17, userDetails.length);
            Assertion.verifyEqual(MobiquityGUIQueries.checkIfNewBulkFieldsEnteredInDB(batchId, userDetails), true, "Verify that the entered values are correctly added to the DB", t2);


        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7598_003() {
        ExtentTest t3 = pNode.createNode("MON_7598_003", "Initiate and approve Bulk Channel User modification");
        try {

            // Note, do not use DataFactory User if modifying functional Data, like Bank, wallet Etc
            User chUserToModify = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t3).createChannelUserDefaultMapping(chUserToModify, false);

            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            Login.init(t3).login(optUsr);

            String file = ExcelUtil.getBulkUserRegistrationFile();

            ChannelUserManagement.init(t3)
                    .writeBulkChUserModificationDetailsInCSV(FilePath.fileBulkChUserReg,
                            chUserToModify,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            "M",
                            "A",
                            true,
                            false,
                            "ONLINE", "N");

            // Upload the File
            Login.init(t3).login(netAdmin);
            BulkChUserRegistrationPage page = BulkChUserRegistrationPage.init(t3);
            page.navBulkUserRegistration();
            page.uploadFile(FilePath.fileBulkChUserReg);
            page.submitCsv();

            // Verify that user Registration is Successful
            String message = driver.findElements(By.className("actionMessage")).get(0).getText();
            Assertion.verifyMessageContain(message, "bulkUpload.channel.label.success",
                    "Bulk Channel User Modification", t3);
            String batchId = message.split("ID :")[1].trim();

            ChannelUserManagement.init(t3)
                    .approveBulkRegistration(batchId);

            //get the values entered in the newly added fields
            CSVReader reader = new CSVReader(new FileReader(file));
            header = reader.readNext();
            userDetails = reader.readNext();
            userDetails = Arrays.copyOfRange(userDetails, userDetails.length - 17, userDetails.length);
            Assertion.verifyEqual(MobiquityGUIQueries.checkIfNewBulkFieldsEnteredInDB(batchId, userDetails), true, "Verify that the entered values are correctly added to the DB", t3);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();
    }

}
