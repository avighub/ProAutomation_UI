package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import com.google.common.collect.ImmutableMap;
import framework.dataEntity.SfmResponse;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.enquiries.ChannelSubsEnq_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setTxnConfig;
import static framework.util.jigsaw.CommonOperations.validateRecentNotification;

//Defect MON-8444
public class MON_7948_ResendNotificationFromCCE extends TestInit {

    private OperatorUser netStat;
    private RechargeOperator RCoperator;
    private User whsUser, sub, merchant, cashoutUser;
    private Biller biller;
    private ChannelSubsEnq_pg1 channelSubsEnqPg1;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest t1 = pNode.createNode("Setup", "Initiating Test");
        try {
            channelSubsEnqPg1 = new ChannelSubsEnq_pg1(t1);
            netStat = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            whsUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            cashoutUser = DataFactory.getChannelUserWithAccess("COUT_WEB");
            biller = BillerManagement.init(t1)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);
            biller.Email = "biller." + biller.LoginId + "@mail.com";
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            merchant = DataFactory.getMerchantUserWithOnlineOfflineType(Constants.MERCHANT, false);
            RCoperator = OperatorUserManagement.init(t1).getRechargeOperator(DataFactory.getDefaultProvider().ProviderId);
            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalance(whsUser);

            ServiceCharge tRule = new ServiceCharge(Services.CASHOUT, sub, whsUser, null, null, null, null);
            TransferRuleManagement.init(t1)
                    .configureTransferRule(tRule);

            ServiceCharge tRuleBillPay = new ServiceCharge(Services.BILL_PAYMENT, sub, biller,
                    null, null, null, null);
            TransferRuleManagement.init(t1)
                    .configureTransferRule(tRuleBillPay);

            //create transfer rule for Offline Bill Payment
            ServiceCharge bill = new ServiceCharge(Services.AgentAssistedPresentmentBillPayment, whsUser,
                    biller, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(bill);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /*
    This is a sample case for referral to automate all resend notification cases
    please remove this once all cases were automated by referring to this
     */
    @Test(groups = {FunctionalTag.MONEY_SMOKE})
    public void sampleCase() throws Exception {
        ExtentTest test = pNode.createNode("Sample Automation Case For Resend Notification Test");

        User chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
        OperatorUser optUsr = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

        String txnId = Transactions.init(test).initiateCashIn(sub, chUsr, new BigDecimal("1")).TransactionId;

        String originalNotificationMessageSub = Transactions.init(test).fetchRecentNotification(sub.MSISDN);

        String originalNotificationMessageChUser = Transactions.init(test).fetchRecentNotification(chUsr.MSISDN);

        Login.init(test).login(optUsr);
        String msg = Enquiries.init(test)
                .enterCCEPortal(Constants.SUBSCRIBER_REIMB, Constants.CCE_MOBILE_NUMBER, sub.MSISDN)
                .clickOnTransactionDetails().searchTxnId(txnId).clickResendNotification().getActionMsg();

        Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", test);

        Enquiries.init(test).switchToMainWindow();

        msg = Enquiries.init(test)
                .enterCCEPortal(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER, chUsr.MSISDN)
                .clickOnTransactionDetails().searchTxnId(txnId).clickResendNotification().getActionMsg();

        Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", test);

        validateRecentNotification(of("toWhom", sub.MSISDN, "count", 10), originalNotificationMessageSub, test);

        validateRecentNotification(of("toWhom", chUsr.MSISDN, "count", 10), originalNotificationMessageChUser, test);
    }

    @Test(priority = 1)
    public void MON_7948_001() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_7948_001",
                "Check if both the resend notification buttons are present on resend notification screen");
        try {

            Login.init(t1).login(netStat);

            channelSubsEnqPg1 = Enquiries.init(t1)
                    .enterCCEPortal(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER, whsUser.MSISDN);
            channelSubsEnqPg1.clickOnTransactionDetails();
            channelSubsEnqPg1.clickOnAccordionLastFiveTxns();

            Assertion.verifyMessageEquals(driver.findElement(By.id("resend_sms_btn")) != null ? "true" : "false", "true", "Found resend SMS button", t1);
            Assertion.verifyMessageEquals(driver.findElement(By.id("resend_sms_btn_both")) != null ? "true" : "false", "true", "Found resend SMS to both button", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void MON_7948_02a() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7948_02a",
                "Check if resend notification to both parties for agent assisted bill payment transaction gets sent as expected");
        try {
            //config service charge and commission

            //in ch user, owner = whs
            //retailer parent is ssa (created by whs) so for ret, parent is ssa and owner is whs
            //pseudo user - does parent's work, uses parent's wallet
            //here, pseudo user is doing the txn. commission goes to owner. whs can be parent/owner
            //login as whs. create pseudo user. config commission rule in pricing so that it goes to whs. bill amt goes to biller. pseudo user and his creator gets the notif for debit. parent  gets notif for commision. biller get notif that credit.
            //4 notifs
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 2)
    public void MON_7948_02b() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_7948_02b",
                "Check if resend notification to both parties for bill payment transaction in TA state gets sent as expected");
        try {

            //perform Bill Pay
            SfmResponse response = Transactions.init(t1)
                    .initiateOnlineBillerPayment(biller, sub, "20", true);
            String tid = response.TransactionId;

            Login.init(t1).login(netStat);
            String msg = Enquiries.init(t1)
                    .enterCCEPortal(Constants.SUBSCRIBER_REIMB, Constants.CCE_MOBILE_NUMBER, sub.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(tid).clickResendToBothNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);

            //verify
            String notificationCheckForSub = Transactions.init(t1).fetchRecentNotification(sub.MSISDN);
            String notificationCheckForBiller = Transactions.init(t1).fetchRecentNotification(biller.Email);

            validateRecentNotification(of("toWhom", sub.MSISDN, "count", 10), notificationCheckForSub, t1);
            validateRecentNotification(of("toWhom", biller.Email, "count", 10), notificationCheckForBiller, t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void MON_7948_02c() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_7948_02c",
                "Check if resend notification to both parties for cashout transaction in TA state gets sent to all involved parties");
        try {

            //perform Cash Out
            BigDecimal amount = new BigDecimal("2");
            SfmResponse response = Transactions.init(t1)
                    .performCashOut(sub, whsUser, amount);
            TransactionManagement.init(t1)
                    .approvePendingCashout(response.TransactionId, true);

            Login.init(t1).login(netStat);
            String msg = Enquiries.init(t1)
                    .enterCCEPortal(Constants.SUBSCRIBER_REIMB, Constants.CCE_MOBILE_NUMBER, sub.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(response.TransactionId).clickResendToBothNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);

            String notificationCheckForSub = Transactions.init(t1).fetchRecentNotification(sub.MSISDN);
            String notificationCheckForWhs = Transactions.init(t1).fetchRecentNotification(whsUser.MSISDN);

            //verify
            validateRecentNotification(of("toWhom", sub.MSISDN, "count", 10), notificationCheckForSub, t1);
            validateRecentNotification(of("toWhom", whsUser.MSISDN, "count", 10), notificationCheckForWhs, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4)
    public void MON_7948_03a() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7948_03a",
                "Check if resend notification for merchant payment gets sent to the user we searched for");
        String txnAmount = "10";
        try {

            String txnId = Transactions.init(t1)
                    .initiateMerchantPayment(merchant, sub, txnAmount)
                    .verifyMessage("api.merchantpay.completed", txnAmount, sub.MSISDN, merchant.MSISDN).TransactionId;

            Login.init(t1).login(netStat);
            String msg = Enquiries.init(t1)
                    .enterCCEPortal(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER, merchant.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(txnId).clickResendNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);

            //check notification for CCE user - TRUE. For other user - FALSE
            //TODO:- See the false part
            String notificationCheckForMerchant = Transactions.init(t1).fetchRecentNotification(merchant.MSISDN);    //TRUE
            String notificationCheckForSub = Transactions.init(t1).fetchRecentNotification(sub.MSISDN);  //FALSE

            validateRecentNotification(of("toWhom", merchant.MSISDN, "count", 10), notificationCheckForMerchant, t1);
            validateRecentNotification(of("toWhom", sub.MSISDN, "count", 10), notificationCheckForSub, t1); //FALSE
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 5)
    public void MON_7948_03b() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7948_03b",
                "Verify that in transaction correction, the email is not sent to the operator user");

        try {

            Login.init(t1).login(cashoutUser);
            String tid = TransactionManagement.init(t1).performCashOut(sub, "10", null);
            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));
            String txnid = TransactionCorrection.init(t1).initiateTxnCorrection(tid, true, true);
            TransactionCorrection.init(t1).approveOrRejectTxnCorrection(txnid, true);

            Login.init(t1).login(netStat);
            String msg = Enquiries.init(t1)
                    .enterCCEPortal(Constants.SUBSCRIBER_REIMB, Constants.CCE_MOBILE_NUMBER, sub.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(txnid).clickResendToBothNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);

            //TODO :- Check
            OperatorUser usrInitTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRECTION");
            String notificationCheckForOpUser = Transactions.init(t1).fetchRecentNotification(usrInitTxnCorrection.Email);  //FALSE On Resend Notification, no email sent to operator user.


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6)
    public void MON_7948_03c() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7948_03c",
                "Verify that in Recharge Self transaction, when in TA state, on clicking resend notification, expected notification gets sent to the transaction initiator");

        try {
            //recharge op does
            SfmResponse response = Transactions.init(t1).rechargeSelf(whsUser, RCoperator.OperatorId, "20", Constants.NORMAL_WALLET);

            //go to CCE with respective Channel User's credentials and resend notification
            Login.init(t1).login(netStat);
            String msg = Enquiries.init(t1)
                    .enterCCEPortal(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER, whsUser.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(response.TransactionId).clickResendToBothNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);

            //hit API and validate notification
            String notificationCheckForWhsUser = Transactions.init(t1).fetchRecentNotification(whsUser.MSISDN);    //TRUE
            validateRecentNotification(of("toWhom", whsUser.MSISDN, "count", 10), notificationCheckForWhsUser, t1);

            String notificationCheckForRCOperator = Transactions.init(t1).fetchRecentNotification(RCoperator.Email);    //TRUE
            validateRecentNotification(of("toWhom", RCoperator.Email, "count", 10), notificationCheckForRCOperator, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7)
    public void MON_7948_05() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7948_05",
                "Verify that on clicking resend notification to both parties after Transaction Correction, expected notification gets sent to the operator user by email and as sms to the other user");

        try {
            Login.init(t1).login(cashoutUser);
            String tid = TransactionManagement.init(t1).performCashOut(sub, "10", null);
            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));
            String txnid = TransactionCorrection.init(t1).initiateTxnCorrection(tid, true, true);
            TransactionCorrection.init(t1).approveOrRejectTxnCorrection(txnid, true);

            Login.init(t1).login(netStat);
            String msg = Enquiries.init(t1)
                    .enterCCEPortal(Constants.SUBSCRIBER_REIMB, Constants.CCE_MOBILE_NUMBER, sub.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(txnid).clickResendToBothNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);


            String notificationCheckForSub = Transactions.init(t1).fetchRecentNotification(sub.MSISDN);
//            String notificationCheckForWhs = Transactions.init(t1).fetchRecentNotification(whsUser.MSISDN);
            validateRecentNotification(of("toWhom", biller.Email, "count", 10), notificationCheckForSub, t1);
//            validateRecentNotification(of("toWhom", whsUser.MSISDN, "count", 10), notificationCheckForWhs, t1);
            //sender, recv, netadmn who init, netadmn who approve will get
            //check before and init
            //TODO:- Notification as email for operator?


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8)
    public void MON_7948_006() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7948_006",
                "Verify that when send notification to both parties is clicked twice after Offline Bill Pay, all involved parties get the notification twice");

        try {
            SfmResponse response = Transactions.init(t1).initiateOfflineBillPayment(biller, whsUser, "10", DataFactory.getRandomNumberAsString(5));

            Login.init(t1).login(netStat);
            String msg = Enquiries.init(t1)
                    .enterCCEPortal(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER, whsUser.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(response.TransactionId).clickResendToBothNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);

            //check if notification sent and assert
            String notificationCheckForBiller = Transactions.init(t1).fetchRecentNotification(biller.Email);
            String notificationCheckForWhs = Transactions.init(t1).fetchRecentNotification(whsUser.MSISDN);
            validateRecentNotification(of("toWhom", biller.Email, "count", 10), notificationCheckForBiller, t1);
            validateRecentNotification(of("toWhom", whsUser.MSISDN, "count", 10), notificationCheckForWhs, t1);

            Enquiries.init(t1).switchToMainWindow();

            String msg2 = Enquiries.init(t1)
                    .enterCCEPortal(Constants.SUBSCRIBER_REIMB, Constants.CCE_MOBILE_NUMBER, sub.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(response.TransactionId).clickResendToBothNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);


            String notificationCheckForBiller2 = Transactions.init(t1).fetchRecentNotification(biller.Email);
            String notificationCheckForWhs2 = Transactions.init(t1).fetchRecentNotification(whsUser.MSISDN);
            validateRecentNotification(of("toWhom", biller.Email, "count", 10), notificationCheckForBiller2, t1);
            validateRecentNotification(of("toWhom", whsUser.MSISDN, "count", 10), notificationCheckForWhs2, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9)
    public void MON_7948_007() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7948_007",
                "Verify that when send notification is clicked twice after Cash In, only the party who is searched in the CCE portal gets the notification twice");

        try {

            String txnId = Transactions.init(t1).initiateCashIn(sub, whsUser, new BigDecimal("1")).TransactionId;

            Login.init(t1).login(netStat);
            String msg = Enquiries.init(t1)
                    .enterCCEPortal(Constants.SUBSCRIBER_REIMB, Constants.CCE_MOBILE_NUMBER, sub.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(txnId).clickResendNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg, "notification.msg.success", "Notification Msg", t1);

            String notificationCheckForSub = Transactions.init(t1).fetchRecentNotification(sub.MSISDN);
            validateRecentNotification(of("toWhom", sub.MSISDN, "count", 10), notificationCheckForSub, t1);

            Enquiries.init(t1).switchToMainWindow();

            String msg2 = Enquiries.init(t1)
                    .enterCCEPortal(Constants.SUBSCRIBER_REIMB, Constants.CCE_MOBILE_NUMBER, sub.MSISDN)
                    .clickOnTransactionDetails().searchTxnId(txnId).clickResendNotification().getActionMsg();
            Assertion.verifyMessageEquals(msg2, "notification.msg.success", "Notification Msg", t1);


            String notificationCheckForSub2 = Transactions.init(t1).fetchRecentNotification(sub.MSISDN);
            validateRecentNotification(of("toWhom", sub.MSISDN, "count", 10), notificationCheckForSub2, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
