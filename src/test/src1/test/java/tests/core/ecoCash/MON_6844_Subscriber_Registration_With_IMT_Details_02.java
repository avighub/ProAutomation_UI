package tests.core.ecoCash;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : v5.1 System Case: Subscriber Registration/Modification With IMT Details: Negative
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 11/12/2018.
 */

/*
This Functionality Is No More Valid After MON-7618
 */
/*public class Suite_MON_6844_Subscriber_Registration_With_IMT_Details_02 extends TestInit {
    private User chUser, subUser, existingSub;
    private List<String> actualErrorMessages, expectedMessage;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");

        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        subUser = new User(Constants.SUBSCRIBER);
        existingSub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        SystemPreferenceManagement.init(setup)
                .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "Y");
    }

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest test = pNode.createNode("Test_01", "To verify that appropriate error message is " +
                "displayed when'IMT Enable' is selected in subscriber registration page and bellow fields were not selected: " +
                "IMT ID TYPE, IMT ID NUMBER, ID ISSUE PLACE, IS ISSUE COUNTRY, POSTAL CODE,BIRTH CITY,NATIONALITY" +
                "BIRTH COUNTRY, COUNTRY RESIDENCY, PASSPORT ISSUE COUNTRY, ISSUE DATE, EXPIRY DATE," +
                "PASSPORT CITY, PASSPORT ISSUE DATE, OCCUPATION, EMPLOYEE NAME");
        try {
            Login.init(test).login(chUser);
            AddSubscriber_Page1.init(test)
                    .navAddSubscriber().checkIsIDExpire()
                    .setIsIMTEnabled(subUser.isIMTEnabled)
                    .clickNext();

            expectedMessage = new ArrayList<>();
            expectedMessage.addAll(Arrays.asList("imt.id.type", "imt.id.number",
                    "imt.id.issue.place", "imt.id.issue.country",
                    "imt.postal.code", "imt.birth.city", "imt.nationality",
                    "imt.birth.country", "imt.country.residency", "imt.passport.issue.country",
                    "imt.issue.date", "imt.expiry.date", "imt.passport.city",
                    "imt.passport.issue.date", "imt.occupation", "imt.emp.name"));


            actualErrorMessages = Assertion.getAllErrorMessages();
            for (int i = 0; i < expectedMessage.size(); i++) {
                String expectedErrorMessage = MessageReader.getMessage(expectedMessage.get(i), null);
                Assertion.verifyListContains(actualErrorMessages, expectedErrorMessage, "Verify Error Message", test);
            }
            Utils.captureScreen(test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("Test_02", "To verify that appropriate error message is " +
                "displayed when'IMT Enable' is selected in subscriber modification page and bellow fields were not selected: " +
                "IMT ID TYPE, IMT ID NUMBER, ID ISSUE PLACE, IS ISSUE COUNTRY, POSTAL CODE,BIRTH CITY,NATIONALITY" +
                "BIRTH COUNTRY, COUNTRY RESIDENCY, PASSPORT ISSUE COUNTRY, ISSUE DATE, EXPIRY DATE," +
                "PASSPORT CITY, PASSPORT ISSUE DATE, OCCUPATION, EMPLOYEE NAME");
        try {
            Login.init(test).login(chUser);

            SubscriberManagement.init(test)
                    .initiateSubscriberModification(existingSub);

            AddSubscriber_Page1.init(test)
                    .setIsIMTEnabled(existingSub.isIMTEnabled);

            ModifySubscriber_page2.init(test).clickOnNextPg2();

            expectedMessage = new ArrayList<>();
            expectedMessage.addAll(Arrays.asList("imt.id.type", "imt.id.number",
                    "imt.id.issue.place", "imt.id.issue.country",
                    "imt.postal.code", "imt.birth.city", "imt.nationality",
                    "imt.birth.country", "imt.country.residency", "imt.passport.issue.country",
                    "imt.issue.date", "imt.expiry.date", "imt.passport.city",
                    "imt.passport.issue.date", "imt.occupation", "imt.emp.name"));

            actualErrorMessages = Assertion.getAllErrorMessages();
            for (int i = 0; i < expectedMessage.size(); i++) {
                String expectedErrorMessage = MessageReader.getMessage(expectedMessage.get(i), null);
                Assertion.verifyListContains(actualErrorMessages, expectedErrorMessage, "Verify Error Message", test);
            }
            Utils.captureScreen(test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("Test_03", "To verify that appropriate error message is " +
                "displayed when Issue date is greater than expiry date in subscriber modification page");
        try {

            SubscriberManagement.init(test)
                    .initiateSubscriberModification(existingSub);

            AddSubscriber_Page1.init(test)
                    .setIsIMTEnabled(existingSub.isIMTEnabled);

            ModifySubscriber_page2.init(test)
                    .setIdIssueDate((new DateAndTime().getDate(-9)))
                    .setExpiryDate((new DateAndTime().getDate(-10))).
                    clickOnNextPg2();

            Assertion.verifyErrorMessageContain("channeluser.error.expirydate.lessthan.issuedate", "Invalid Expire Date", test);
            Assertion.verifyErrorMessageContain("channeluser.error.expirydate.lessthan.currentdate", "Invalid Expire Date", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4)
    public void Test_04() {
        ExtentTest test = pNode.createNode("Test_04", "To verify that when IMT ID TYPE is selected as " +
                "Passport and EXPIRY DATE selected is less than the 90 days from the current date then appropriate error message should be displayed in the UI");
        try {
            String days = MobiquityGUIQueries.fetchDefaultValueOfPreference("PASSPORT_EXPIRY_DATE_LIMIT");

            SubscriberManagement.init(test)
                    .initiateSubscriberModification(existingSub);

            AddSubscriber_Page1.init(test)
                    .setIsIMTEnabled(existingSub.isIMTEnabled);

            ModifySubscriber_page2.init(test)
                    .selectImtIdType()
                    .setExpiryDate((new DateAndTime().getDate(2)))
                    .clickOnNextPg2();

            Assertion.verifyErrorMessageContain("imt.id.expiry.date", "IMT Id Expiry Date", test, days);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest teardown = pNode.createNode("Teardown", "Concluding Test");
        try {
            SystemPreferenceManagement.init(teardown)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "N");
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
*/
