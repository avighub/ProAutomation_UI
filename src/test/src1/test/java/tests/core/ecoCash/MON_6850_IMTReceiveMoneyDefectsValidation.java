package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.pricingEng.PricingEngine_Page1;
import framework.pageObjects.pricingEng.PricingEngine_Page2;
import framework.pageObjects.serviceCharge.ServiceCharge_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Services;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/**
 * Created by ravindra.dumpa on 2/19/2019.
 */
public class MON_6850_IMTReceiveMoneyDefectsValidation extends TestInit {


    @Test
    public void iMTPayerTypeTest() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_7093", "Verify while defining IMT receive money service charge,charge payer should not be sender(pricing engine-->who pays drop down)");

        try {
            //Login as Networkadmin to define Service Charge
            OperatorUser usrSChargeCreator = DataFactory.getOperatorUserWithAccess("SVC_ADD");
            Login.init(t1).login(usrSChargeCreator);
            PricingEngine_Page1 page1 = new PricingEngine_Page1(t1);
            page1.navToPricingEngine();
            PricingEngine_Page2 page2 = new PricingEngine_Page2(t1);
            //Select Service Type Stock Liquidation Service
            page1.selectServiceType("International Receive Money");
            Thread.sleep(4000);
            page2.navToAddService();
            WebElement element = driver.findElement(By.id("charge-rule-0-charge-statement-0-charge-payer"));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].click();", element);
            Select sel = new Select(driver.findElement(By.id("charge-rule-0-charge-statement-0-charge-payer")));
            Thread.sleep(4000);
            sel.selectByVisibleText("Receiver");
            //Verifying Only Sender Option is available in dropdown
            List<WebElement> options = sel.getOptions();
            for (int i = 1; i < options.size(); i++) {
                String opt = options.get(i).getText();
                Assert.assertEquals(false, opt.contentEquals("Sender"));
                Assert.assertEquals(false, opt.contentEquals("Sender Parent"));
                Assert.assertEquals(false, opt.contentEquals("Receiver Parent"));
                Assert.assertEquals(false, opt.contentEquals("Sender Owner"));
                Assert.assertEquals(false, opt.contentEquals("Receiver Owner"));
            }


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test
    public void iMTServiceshouldnotavailbleInServiceChargeTest() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_7100", "Verify IMT receive money should not available in serice drop down in service charge page");
        try {
            //Login as Networkadmin to define Service Charge
            OperatorUser usrSChargeCreator = DataFactory.getOperatorUserWithAccess("SVC_ADD");
            Login.init(t1).login(usrSChargeCreator);

            ServiceCharge_Pg1 page1 = ServiceCharge_Pg1.init(t1)
                    .navAddServiceCharge();
            Select sel = new Select(driver.findElement(By.id("paymentTypeServicesId")));
            Thread.sleep(4000);
            List<WebElement> options = sel.getOptions();
            for (int i = 1; i < options.size(); i++) {
                String opt = options.get(i).getAttribute("value");
                Assert.assertEquals(false, opt.contentEquals(Services.INTERNAT_REC_MONEY));
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

}
