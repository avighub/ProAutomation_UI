package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.stockManagement.StockWithdrawalApproval_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class MON_7609_MakerCheckerforStockWithdrawal extends TestInit {

    private OperatorUser naInitiator, naApprover, bankAdmInitiator,
            bankAdminApprover, bankUserApprover, bankAdminInitiator, bankUserInitiator,
            bankAdmApproverN, bankUsrApproverN;
    private String txnAmount;
    private CurrencyProvider defaultProvider;
    private String trustBank;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific for this suite");
        try {
            txnAmount = "50";
            defaultProvider = GlobalData.defaultProvider;
            trustBank = DataFactory.getTrustBankName(defaultProvider.ProviderName);
            naInitiator = DataFactory.getOperatorUserWithAccess("STK_WITHDRAW", Constants.NETWORK_ADMIN);
            naApprover = DataFactory.getOperatorUserWithAccess("STK_WITHDRAW_APP", Constants.NETWORK_ADMIN);
            bankAdminInitiator = OperatorUserManagement.init(s1).getBankAdminWithAccess("STK_WITHDRAW", trustBank);
            bankUserInitiator = OperatorUserManagement.init(s1).getBankUserWithAccess("STK_WITHDRAW", trustBank);
            bankAdminApprover = OperatorUserManagement.init(s1).getBankAdminWithAccess("STK_WITHDRAW_APP", trustBank);
            bankUserApprover = OperatorUserManagement.init(s1).getBankUserWithAccess("STK_WITHDRAW_APP", trustBank);

            // create a new Bank and accounts, create Bank Admin and Bank User
            Bank bank = new Bank(defaultProvider.ProviderId,
                    Constants.TEMP_BANK_TRUST,
                    null,
                    null,
                    true,
                    false);
            CurrencyProviderMapping.init(s1).addBankAndAccountDetails(bank);

            bankAdmApproverN = OperatorUserManagement.init(s1).getBankAdminWithAccess("STK_WITHDRAW_APP", bank.BankName);
            bankUsrApproverN = OperatorUserManagement.init(s1).getBankUserWithAccess("STK_WITHDRAW_APP", bank.BankName);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7609_01() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7609-01",
                "Verify that Same Network admin cannot approve for Stock Withdrawal");
        String txnId = "";

        try {
            Login.init(t1).login(naInitiator);
            txnId = StockManagement.init(t1).initiateStockWithdrawal(txnAmount, defaultProvider.ProviderId, trustBank);
            Boolean isPresent1 = StockWithdrawalApproval_page1.init(t1).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent1, false, txnId + ", Verify that The same Network admin does not get Option to approve/reject Withdrawal Request", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON-7609-02", "Verify that Network admin Who is not the initiator can approve for Stock Withdrawal");
        try {
            Login.init(t2).login(naApprover);
            Boolean isPresent2 = StockWithdrawalApproval_page1.init(t2).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent2, true,
                    txnId + ", Verify that Network Admin other than Initiator get Option to approve/reject Stock Withdraw Request", t2);

            StockManagement.init(t2).approveRejectStockWithdrawal(txnId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7609_03() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7609-03",
                "Verify that Same Super admin cannot approve for Stock Withdrawal");
        String txnId = "";

        try {
            Login.init(t1).loginAsSuperAdmin("STK_WITHDRAW");
            txnId = StockManagement.init(t1).initiateStockWithdrawal(txnAmount, defaultProvider.ProviderId, trustBank);
            Boolean isPresent = StockWithdrawalApproval_page1.init(t1).isApprovalLinkPresentInLeftNavigation();
            Assertion.verifyEqual(isPresent, false, "Verify that The same Super admin does not get Option to approve/reject Withdrawal Request", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON-7609-04",
                "Verify that Super admin can approve for Stock Withdrawal not initiated by itself");

        try {
            Login.init(t1).loginAsSuperAdmin("STK_WITHDRAW_APP");
            Boolean isPresent = StockWithdrawalApproval_page1.init(t1).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent, true, txnId + ", Verify that Super admin with approval rights can approve a Stock Withdrawal Request", t1);
            StockManagement.init(t2).approveRejectStockWithdrawal(txnId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7609_05() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7609-05",
                "Verify that Stock Withdrawal initiated by Network admin can be Approved by Bank Admin who is linked to the same Bank Account");
        String txnId = "";

        try {
            Login.init(t1).login(naInitiator);
            txnId = StockManagement.init(t1).initiateStockWithdrawal(txnAmount, defaultProvider.ProviderId, trustBank);

            Login.init(t1).login(bankAdminApprover);
            Boolean isPresent = StockWithdrawalApproval_page1.init(t1).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent, true, txnId + ", Verify that Bank admin Can Approve Stock withdraw,, initiated by Network admin", t1);
            StockManagement.init(t1).approveRejectStockWithdrawal(txnId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7609_06() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7609-06",
                "Verify that Stock Withdrawal initiated by Network admin can be Approved by Bank User who is linked to the same Bank Account");
        String txnId = "";

        try {
            Login.init(t1).login(naInitiator);
            txnId = StockManagement.init(t1).initiateStockWithdrawal(txnAmount, defaultProvider.ProviderId, trustBank);

            Login.init(t1).login(bankUserApprover);
            Boolean isPresent = StockWithdrawalApproval_page1.init(t1).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent, true, txnId + ", Verify that Bank User Can Approve Stock withdraw,, initiated by Network admin", t1);
            StockManagement.init(t1).approveRejectStockWithdrawal(txnId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void MON_7609_07() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7609-07",
                "Verify that bank admin cannot approve own Stock Withdrawal initiation");
        String txnId = "";

        try {
            Login.init(t1).login(bankAdminInitiator);
            txnId = StockManagement.init(t1).initiateStockWithdrawal(txnAmount, defaultProvider.ProviderId, trustBank);
            Boolean isPresent1 = StockWithdrawalApproval_page1.init(t1).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent1, false, txnId + ", Verify that bank admin does not get the option to approve/reject own Withdrawal Request", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON-7609-08",
                "Verify that bank admin who is not the initiator can approve for Stock Withdrawal");
        try {
            Login.init(t2).login(bankAdminApprover);
            Boolean isPresent2 = StockWithdrawalApproval_page1.init(t2).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent2, true,
                    txnId + ", Verify that bank admin other than initiator will get the option to approve/reject Stock Withdraw Request", t2);

            StockManagement.init(t2).approveRejectStockWithdrawal(txnId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6)
    public void MON_7609_09() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7609-09",
                "Verify that bank user cannot approve own Stock Withdrawal initiation");
        String txnId = "";

        try {
            Login.init(t1).login(bankUserInitiator);
            txnId = StockManagement.init(t1).initiateStockWithdrawal(txnAmount, defaultProvider.ProviderId, trustBank);
            Boolean isPresent1 = StockWithdrawalApproval_page1.init(t1).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent1, false, txnId + ", Verify that bank user does not get the option to approve/reject Withdrawal Request", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON-7609-10", "Verify that bank user who is not the initiator can approve for Stock Withdrawal");
        try {
            Login.init(t2).login(bankUserApprover);
            Boolean isPresent2 = StockWithdrawalApproval_page1.init(t2).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent2, true,
                    txnId + ", Verify that bank user other than initiator will get the option to approve/reject Stock Withdraw Request", t2);

            StockManagement.init(t2).approveRejectStockWithdrawal(txnId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7)
    public void MON_7609_11() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7609-11",
                "Verify that user/admin of a different bank cannot approve a bank admin's Stock Withdrawal initiation");
        String txnId = "";
        Login.init(t1).login(bankAdminInitiator);
        txnId = StockManagement.init(t1).initiateStockWithdrawal(txnAmount, defaultProvider.ProviderId, trustBank);
        try {
            Login.init(t1).login(bankAdmApproverN);
            Boolean isPresent1 = StockWithdrawalApproval_page1.init(t1).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent1, false, txnId + ", Verify that admin of a different bank cannot approve a bank admin's Stock Withdrawal initiation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        try {
            Login.init(t1).login(bankUsrApproverN);
            Boolean isPresent2 = StockWithdrawalApproval_page1.init(t1).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent2, false,
                    txnId + ", Verify that user of a different bank cannot approve a bank admin's Stock Withdrawal initiation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON-7609-12",
                "Verify that user/admin of a different bank cannot approve a bank user's Stock Withdrawal initiation");
        Login.init(t2).login(bankUserInitiator);
        txnId = StockManagement.init(t2).initiateStockWithdrawal(txnAmount, defaultProvider.ProviderId, trustBank);
        try {
            Login.init(t2).login(bankAdmApproverN);
            Boolean isPresent1 = StockWithdrawalApproval_page1.init(t2).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent1, false, txnId + ", Verify that admin of a different bank cannot approve a bank admin's Stock Withdrawal initiation", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        try {
            Login.init(t2).login(bankUsrApproverN);
            Boolean isPresent2 = StockWithdrawalApproval_page1.init(t2).navApproveStockWithdrawal().isRequestAvailableForApproval(txnId);
            Assertion.verifyEqual(isPresent2, false,
                    txnId + ", Verify that user of a different bank cannot approve a bank user's Stock Withdrawal initiation", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }
}
