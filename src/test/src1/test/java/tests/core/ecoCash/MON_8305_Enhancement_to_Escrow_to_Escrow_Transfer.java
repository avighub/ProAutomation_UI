package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.escrowToEscrowTransfer.EscrowToEscrowTransfer;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static framework.util.common.Assertion.finalizeSoftAsserts;

public class MON_8305_Enhancement_to_Escrow_to_Escrow_Transfer extends TestInit {
    private OperatorUser e2eInitiator, e2eApprover;
    private String fromBank, toBank, referenceNo;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        try {
            toBank = DataFactory.getNonTrustBankName(DataFactory.getDefaultProvider().ProviderName);
            fromBank = DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName);

            e2eInitiator = DataFactory.getOperatorUserWithAccess("ESCROW_INIT");
            e2eApprover = DataFactory.getOperatorUserWithAccess("ESCROW_APP");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        finalizeSoftAsserts();
    }

    @BeforeMethod(alwaysRun = true)
    public void generateUniqueRefNum() {
        referenceNo = DataFactory.getRandomNumberAsString(5);
    }

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest test = pNode.createNode("MON_8305_01", "verify that, valid error message should display " +
                "if duplicate reference number is entered while initiating escrow to escrow transfer");
        try {
            Login.init(test).login(e2eInitiator);

            String txnId = EscrowToEscrowTransfer.init(test)
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName,
                            fromBank, toBank, referenceNo, Constants.MIN_TRANSACTION_AMT);

            Login.init(test).login(e2eApprover);

            EscrowToEscrowTransfer.init(test)
                    .approveEscrowToEscrowTransfer(txnId);

            startNegativeTest();
            EscrowToEscrowTransfer.init(test)
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName,
                            fromBank, toBank, referenceNo, Constants.MIN_TRANSACTION_AMT);

            Assertion.verifyActionMessageContain("e2e.duplicate.ref.num", "Duplicate Reference Number", test);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("MON_8305_02", "verify that, valid error message should display " +
                "if duplicate reference number is entered while initiating escrow to escrow transfer while original transaction is in TI state");
        try {
            Login.init(test).login(e2eInitiator);

            EscrowToEscrowTransfer.init(test)
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName,
                            fromBank, toBank, referenceNo, Constants.MIN_TRANSACTION_AMT);

            startNegativeTest();
            EscrowToEscrowTransfer.init(test)
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName,
                            fromBank, toBank, referenceNo, Constants.MIN_TRANSACTION_AMT);

            Assertion.verifyActionMessageContain("e2e.duplicate.ref.num", "Duplicate Reference Number", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("MON_8305_03", "verify that user should be able to perform escrow to escrow transfer" +
                "with the duplicate reference number if original transaction was rejected by approver");
        try {
            Login.init(test).login(e2eInitiator);

            String txnId = EscrowToEscrowTransfer.init(test)
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName,
                            fromBank, toBank, referenceNo, Constants.MIN_TRANSACTION_AMT);

            Login.init(test).login(e2eApprover);

            EscrowToEscrowTransfer.init(test)
                    .rejectEscrowToEscrowTransfer(txnId, "Rejected");

            Login.init(test).login(e2eInitiator);
            EscrowToEscrowTransfer.init(test)
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName,
                            fromBank, toBank, referenceNo, Constants.MIN_TRANSACTION_AMT);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }
}
