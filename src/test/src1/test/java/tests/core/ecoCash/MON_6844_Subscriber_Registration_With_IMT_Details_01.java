package tests.core.ecoCash;


/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : v5.1 System Case: Subscriber Registration/Modification With IMT Details
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 11/8/2018.
 */

/*
This Functionality Is No More Valid After MON-7618
 */
/*public class Suite_MON_6844_Subscriber_Registration_With_IMT_Details_01 extends TestInit {
    private User chUsr, newSub, existingSub;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            existingSub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "N");
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest test = pNode.createNode("MON_6844_01", "To verify that subscriber registration page should" +
                " not display the KYC details fields related to IMT in the UI if IMT_SEND_ENABLED is N in the system preference");
        try {
            Login.init(test).login(chUsr);

            AddSubscriber_Page1 page = new AddSubscriber_Page1(test);
            page.navAddSubscriber();

            boolean status = Utils.checkElementPresent("isIMTEnabled", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, false, "KYC fields related to IMT are not available on subscriber registration page", test, true);

            test = pNode.createNode("MON_6844_02", "To verify that subscriber modification page should not " +
                    "display the KYC details fields related to IMT in the UI if IMT_SEND_ENABLED is N in the system preference ");

            SubscriberManagement.init(test)
                    .initiateSubscriberModification(existingSub);

            status = Utils.checkElementPresent("isIMTEnabled", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, false, "KYC fields related to IMT are not available on subscriber modification page", test, true);

            test = pNode.createNode("MON_6844_03", "To verify that user should be able to modify subscriber by selecting 'IMT ENABLE' " +
                    "check box that was unselected during registration and entering all the details related to KYC details of IMT ");

            newSub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(test).createSubscriber(newSub);

            SystemPreferenceManagement.init(test)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "Y");

            SubscriberManagement.init(test)
                    .subsModifyInitAndApprove(newSub);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("MON_6844_04", "To verify User is able to select the " +
                "'Enable WU Services' check box and register the subscriber ");
        try {
            SystemPreferenceManagement.init(test)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "Y");
            newSub = new User(Constants.SUBSCRIBER);
            newSub.isMoneyGramSevicesEnabled = false;

            SubscriberManagement.init(test).createSubscriber(newSub);

            test = pNode.createNode("MON_6844_05", "To verify that 'Expiry Date' selection field should be " +
                    "displayed in the UI when 'ID Expires' check box is selected in the subscriber registration page");

            Login.init(test).login(chUsr);
            AddSubscriber_Page1.init(test).navAddSubscriber();

            boolean isIdExpire = Utils.checkElementPresent("dojo.displayExpiryDate", Constants.FIND_ELEMENT_BY_NAME);
            Assertion.verifyEqual(isIdExpire, true, "Id Expires Field Is Visible", test);
            Utils.scrollToAnElement(driver.findElement(By.id("isIdExpires")));
            Utils.captureScreen(test);

            test = pNode.createNode("MON_6844_06", " To verify User is able to un select the " +
                    "'Enable WU Services' check box and modify the subscriber");

            newSub.isWUServiceEnabled = false;
            newSub.isMoneyGramSevicesEnabled = true;
            SubscriberManagement.init(test)
                    .subsModifyInitAndApprove(newSub);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("MON_6844_07", "To verify that user should be able to add a " +
                "subscriber by selecting 'IMT ENABLE' check box and entering all the details related to KYC details of IMT ");

        try {
            newSub = new User(Constants.SUBSCRIBER);
            newSub.isWUServiceEnabled = true;
            newSub.isMoneyGramSevicesEnabled = true;

            SubscriberManagement.init(test).createSubscriber(newSub);

            test = pNode.createNode("MON_6844_08", "To verify that user should be able to modify " +
                    "subscriber by un selecting 'IMT ENABLE' check box selected during modification");

            newSub.isIMTEnabled = false;
            SubscriberManagement.init(test)
                    .subsModifyInitAndApprove(newSub);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest teardown = pNode.createNode("Teardown", "Concluding Test");
        try {
            SystemPreferenceManagement.init(teardown)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "N");
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
*/
