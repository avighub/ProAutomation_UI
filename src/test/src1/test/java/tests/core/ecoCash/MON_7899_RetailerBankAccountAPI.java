package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by rahul.rana on 3/18/2019.
 */
public class MON_7899_RetailerBankAccountAPI extends TestInit {

    private User retailer;
    private CurrencyProvider defaultProvider;
    private Bank trust, nonTrust;

    @BeforeClass(alwaysRun = true)
    public void before() {
        ExtentTest t1 = pNode.createNode("Setup", "Setup specific to this suite");
        try {
            // base set users are created with all banks provided in Config Input
            retailer = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            defaultProvider = GlobalData.defaultProvider;
            trust = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            nonTrust = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);

            // Configure Non Financial service charge fpr Retailer Bank Request
            ServiceChargeManagement.init(t1)
                    .configureNonFinancialServiceCharge(new ServiceCharge(Services.GET_BANKS_PER_PROVIDER,
                            retailer, new OperatorUser(Constants.OPERATOR),
                            null,
                            null,
                            null,
                            null)
                    );

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7899_01() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7899_01",
                "Verify, if Bank ID field is present in the request then" +
                        "Bank details for that specific Bank Linked to the User is shown in the response");

        try {
            String dataResponse = Transactions.init(t1).retailerBankRequest(retailer,
                    defaultProvider.ProviderId,
                    trust.BankID,
                    true)
                    .getDataFromResponseBody();

            // verify that Bank Details for Trust Bank is shown
            Assertion.verifyEqual(dataResponse.contains(trust.BankName), true,
                    "Bank details for :" + trust.BankID + " Linked to the User is shown in the response, when bank id was passed as  part of request", t1, false);
            Assertion.verifyEqual(dataResponse.contains(nonTrust.BankName), false,
                    "Bank details for :" + nonTrust.BankID + " Linked  to the User is NOT shown in the response", t1, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2)
    public void MON_7899_02() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7899_02",
                "Verify, if Bank ID field is NOT present in the request then" +
                        " Details for all the banks linked to the user is shown in the Response");

        try {
            String dataResponse = Transactions.init(t1).retailerBankRequest(retailer,
                    defaultProvider.ProviderId,
                    null,
                    false)
                    .getDataFromResponseBody();

            // verify that Bank Details for Trust Bank is shown
            Assertion.verifyEqual(dataResponse.contains(trust.BankName), true,
                    "Bank details for :" + trust.BankID + " Linked to the User is shown, Bank ID is not mandatory and is Not a field in txn Request", t1, false);
            Assertion.verifyEqual(dataResponse.contains(nonTrust.BankName), true,
                    "Bank details for :" + nonTrust.BankID + " Linked to the User is shown, Bank ID is not mandatory and is Not a field in txn Request", t1, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3)
    public void MON_7899_03() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7899_03",
                "Verify, if Bank ID field is  present in the request and the value is passed as NULL then," +
                        " Details for all the banks linked to the user is shown in the Response");

        try {
            String dataResponse = Transactions.init(t1).retailerBankRequest(retailer,
                    defaultProvider.ProviderId,
                    "",
                    true)
                    .getDataFromResponseBody();

            // verify that Bank Details for Trust Bank is shown
            Assertion.verifyEqual(dataResponse.contains(trust.BankName), true,
                    "Bank details for :" + trust.BankID + " Linked to the User is shown, Bank ID is not mandatory and is passed as NULL as field in txn Request", t1, false);
            Assertion.verifyEqual(dataResponse.contains(nonTrust.BankName), true,
                    "Bank details for :" + nonTrust.BankID + " Linked to the User is shown, Bank ID is not mandatory and is passed as NULL as field in txn Request", t1, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
