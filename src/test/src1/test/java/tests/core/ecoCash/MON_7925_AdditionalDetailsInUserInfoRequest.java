package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by rahul.rana on 3/18/2019.
 */
public class MON_7925_AdditionalDetailsInUserInfoRequest extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void run() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7925_01", "Verify that response of USERINFOREQ API " +
                "should be display two additional field:" +
                "DOB and Gender");

        try {
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TxnResponse res = Transactions.init(t1).userInfo(whs, Constants.CHANNEL_REIMB);

            if (res.response.extract().jsonPath().getString("COMMAND.DOB") != null) {
                t1.pass("DOB is displayed for USERINFOREQ, Channel User: " + whs.MSISDN);
                t1.info("DOB: " + res.response.extract().jsonPath().getString("COMMAND.DOB"));
            } else {
                t1.fail("DOB is NOT displayed for USERINFOREQ, Channel User");
            }

            if (res.response.extract().jsonPath().getString("COMMAND.GENDER") != null) {
                t1.pass("gender is displayed for USERINFOREQ, Channel User:" + whs.MSISDN);
                t1.info("Gender: " + res.response.extract().jsonPath().getString("COMMAND.GENDER"));
            } else {
                t1.fail("GENDER is NOT displayed for USERINFOREQ, Channel User");
            }

            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            TxnResponse res1 = Transactions.init(t1).userInfo(sub, Constants.CUSTOMER_REIMB);

            if (res1.response.extract().jsonPath().getString("COMMAND.DOB") != null) {
                t1.pass("DOB is displayed for USERINFOREQ, Customer User: " + sub.MSISDN);
                t1.info("DOB: " + res1.response.extract().jsonPath().getString("COMMAND.DOB"));
            } else {
                t1.fail("DOB is NOT displayed for USERINFOREQ, Customer");
            }

            if (res1.response.extract().jsonPath().getString("COMMAND.GENDER") != null) {
                t1.pass("gender is displayed for USERINFOREQ, Customer User:" + sub.MSISDN);
                t1.info("Gender: " + res1.response.extract().jsonPath().getString("COMMAND.GENDER"));
            } else {
                t1.fail("GENDER is NOT displayed for USERINFOREQ, Customer");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}
