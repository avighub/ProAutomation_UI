package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class MON_7615_TestSupportForOnlineOfflineTransactionCorrection extends TestInit {

    private OperatorUser usrCreator;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.NETWORK_ADMIN);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test()
    public void MON_7615_01() {
        ExtentTest t1 = pNode.createNode("MON_7615_01",
                "Verify UI Change | User registration Page | New Check Box for Supports Online transaction Reversal | default is disabled");

        try {
            AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(t1);

            Login.init(t1)
                    .login(usrCreator);

            // * Navigate to Add Channel User Page *
            pageOne.navAddChannelUser();

            Assertion.verifyEqual(pageOne.getStatusSupportOnlineTransacionCheckBox(), false,
                    "Verify that the Checkbox for Support Online Transaction Reversal is available and is Disabled By default", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7615_02() {
        ExtentTest t1 = pNode.createNode("MON_7615_02",
                "Create Online Channel User | Supports Online transaction Reversal | Verify DB entry");

        try {
            User chUser = new User(Constants.MERCHANT);
            chUser.setMerchantType(Constants.MERCHANT_TYPE_ONLINE);
            chUser.setOnlineTxnReversalStatus(true);

            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(chUser, false);

            // get the DB Status,
            String dbStatusIsReversalEnabled = MobiquityGUIQueries.getUserCurrentDBStatus(chUser, "supports_online_trans_reversal");
            Assertion.verifyEqual(dbStatusIsReversalEnabled, "Y", "Verify that Merchant's type is Online and Status " +
                    "for 'Support Online transaction reversal is set to 'Y'", t1);

            User chUser1 = new User(Constants.MERCHANT);
            chUser1.setMerchantType(Constants.MERCHANT_TYPE_ONLINE);
            chUser1.setOnlineTxnReversalStatus(false);

            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(chUser1, false);

            // get the DB Status,
            String dbStatusIsReversalEnabled1 = MobiquityGUIQueries.getUserCurrentDBStatus(chUser1, "supports_online_trans_reversal");
            Assertion.verifyEqual(dbStatusIsReversalEnabled1, "N", "Verify that Merchant's type is Online and Status " +
                    "for 'Support Online transaction reversal is set to 'N'", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7615_03() {
        ExtentTest t1 = pNode.createNode("MON_7615_03",
                "Create Biller User | Supports Online transaction Reversal | Verify DB entry");

        try {
            Biller newBiller = new Biller(DataFactory.getDefaultProvider().ProviderName,
                    Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_ONLINE,
                    Constants.BILL_SERVICE_LEVEL_ADHOC
            );
            newBiller.setOnlineTxnReversalStatus(true);
            BillerManagement.init(t1).createBiller(newBiller);

            // get the DB Status,
            String dbStatusIsReversalEnabled = MobiquityGUIQueries.getBillerCurrentDBStatus(newBiller, "supports_online_trans_reversal");
            Assertion.verifyEqual(dbStatusIsReversalEnabled, "Y", "Verify that Biller's Status " +
                    "for 'Support Online transaction reversal is set to 'Y'", t1);

            Biller newBiller1 = new Biller(DataFactory.getDefaultProvider().ProviderName,
                    Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_ONLINE,
                    Constants.BILL_SERVICE_LEVEL_ADHOC
            );
            newBiller1.setOnlineTxnReversalStatus(false);
            BillerManagement.init(t1).createBiller(newBiller1);

            // get the DB Status,
            String dbStatusIsReversalEnabled1 = MobiquityGUIQueries.getBillerCurrentDBStatus(newBiller1, "supports_online_trans_reversal");
            Assertion.verifyEqual(dbStatusIsReversalEnabled1, "N", "Verify that Biller's Status " +
                    "for 'Support Online transaction reversal is set to 'N'", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7615_04() {
        ExtentTest test = pNode.createNode("MON_7615_04",
                "Bulk | Create Online Channel User | Supports Online transaction Reversal | Action must Pass");
        try {

            User chUser = new User(Constants.MERCHANT);
            chUser.setMerchantType(Constants.MERCHANT_TYPE_ONLINE);
            chUser.setOnlineTxnReversalStatus(true);

            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            Login.init(test).login(optUsr);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(test);

            page.navBulkChUserAndSubsciberRegistrationAndModification()
                    .SelectChannelUserRegistrationService();

            ChannelUserManagement.init(test)
                    .writeBulkChUserRegistrationDetailsInCSV(FilePath.fileBulkChUserReg,
                            chUser,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            true,
                            false,
                            "ONLINE", "Y");

            String batchId = page.uploadFile(FilePath.fileBulkChUserReg)
                    .submitCsv()
                    .getBatchId();

            page.navBulkRegistrationAndModificationMyBatchesPage()
                    .ClickOnBatchSubmit()
                    .downloadLogFileUsingBatchId(batchId);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test()
    public void MON_7615_05() {
        ExtentTest test = pNode.createNode("MON_7615_05",
                "Bulk Negative Test| Create Offline Channel User | Supports Online transaction Reversal | Must fail with error");
        try {

            User chUser = new User(Constants.MERCHANT);
            chUser.setMerchantType(Constants.MERCHANT_TYPE_ONLINE);
            chUser.setOnlineTxnReversalStatus(true);

            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            Login.init(test).login(optUsr);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(test);

            page.navBulkChUserAndSubsciberRegistrationAndModification()
                    .SelectChannelUserRegistrationService();

            ChannelUserManagement.init(test)
                    .writeBulkChUserRegistrationDetailsInCSV(FilePath.fileBulkChUserReg,
                            chUser,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            true,
                            false,
                            "OFFLINE", "Y");

            String batchId = page.uploadFile(FilePath.fileBulkChUserReg)
                    .submitCsv()
                    .getBatchId();

            page.navBulkRegistrationAndModificationMyBatchesPage()
                    .ClickOnBatchSubmit()
                    .downloadLogFileUsingBatchId(batchId);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }
}
