package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by ravindra.dumpa on 11/21/2018.
 */
public class MON_6844_BulkSub_RegAndMod extends TestInit {

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "Y");
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {
        ExtentTest t5 = pNode.createNode("Test_01", "Channel admin able register subscriber through bulk");
        /*
         * Login as Operator user
         * Create Subscriber Object
         * Goto bulk channel and subscriber registration page
         * Select the service
         * write the data in csv file
         * Upload csv file
         * go to approve page and verify summary filed
         */
        //Login as Operator user
        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("BULK_INITIATE").get(0); // todo  move to before class

        try {
            Login.init(t5).login(dwFileUser);

            User subsriber = new User(Constants.SUBSCRIBER);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t5);

            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectSubscriberRegistrationService();

            SubscriberManagement.init(t5)
                    .writeDataToBulkSubsRegistrationCsv(subsriber,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            false);

            page.uploadFile(FilePath.fileBulkSubReg);

            page.submitCsv();

            String subBatchId = page.getBatchId();

            Assertion.verifyActionMessageContain("bulkUpload.user.reg.success",
                    "Bulk Channel subscriber Registration", t5, "1", "1", "0");

            OperatorUser dwFileUser1 = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE"); // todo  move to before class

            Login.init(t5).login(dwFileUser1);

            page.navBulkRegistrationAndModificationApproval();

            page.ClickOnSubmit();

            page.clickOnSummaryandVerifyFields(subBatchId);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t5);
        }


    }

}
