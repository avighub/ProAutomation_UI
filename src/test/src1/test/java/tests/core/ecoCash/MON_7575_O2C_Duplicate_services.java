package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.UsrBalance;
import framework.entity.User;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class MON_7575_O2C_Duplicate_services extends TestInit {
    private CurrencyProvider provider1;
    private String trustBank;
    private User whsUser, whsUser1;
    private String txnId;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        try {
            provider1 = DataFactory.getDefaultProvider();
            trustBank = DataFactory.getTrustBankName(provider1.ProviderName);
            whsUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
            whsUser1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @AfterClass(alwaysRun = true, groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void teardown() {
        // make sure that the preference is set to 'N'
        ExtentTest t1 = pNode.createNode("teardown", "Teardown specific to this suite");

        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "N");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_01() {
        ExtentTest t1 = pNode.createNode("MON-7575-01",
                "Verify when performing O2C, the amount field is disabled when LINK_STOCK_CREATION_WITH_O2C is set to 'Y'");


        //
    }

    @Test(priority = 2, groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7575-02",
                "Verify when O2C is successful when correct reference id is provided and LINK_STOCK_CREATION_WITH_O2C is set to 'Y'");

        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "Y");

            // pre balance
            UsrBalance preBalance = MobiquityGUIQueries.getUserBalance(whsUser, null, provider1.ProviderName);
            String txnAmt = "99";

            SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "Y");

            // Create Network Stock
            txnId = StockManagement.init(t1)
                    .initiateAndApproveNetworkStock(provider1.ProviderName, trustBank, txnAmt);

            BigDecimal preWalletBalance = MobiquityDBAssertionQueries.getWalletBalance(Constants.WALLET_101);

            // perform O2C
            TransactionManagement.init(t1)
                    .initiateAndApproveO2CWithProvider(whsUser, provider1.ProviderName, txnAmt, txnId);

            // post Balance
            UsrBalance postBalance = MobiquityGUIQueries.getUserBalance(whsUser, null, provider1.ProviderName);
            BigDecimal postWalletBalance = MobiquityDBAssertionQueries.getWalletBalance(Constants.WALLET_101);

            // verify that User balance is credited
            Assertion.verifyAccountIsCredited(preBalance.Balance, postBalance.Balance, new BigDecimal(txnAmt),
                    "Verify User Account is Credited", t1);

            // verify that System wallet balance is Credited
            Assertion.verifyAccountIsDebited(preWalletBalance, postWalletBalance, new BigDecimal(txnAmt),
                    "Verify System wallet is Debited", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2, dependsOnMethods = "test_02", groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_03() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7575-03",
                "Verify Same reference id cannot be used twice");

        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "Y");

            // perform O2C
            ConfigInput.isConfirm = false;
            TransactionManagement.init(t1)
                    .initiateAndApproveO2CWithProvider(whsUser, provider1.ProviderName, "10", txnId);

            Assertion.verifyErrorMessageContain("error.stock.txn.already.linked",
                    "Verify reference id can not be random, or not of a valid stock initiation", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, dependsOnMethods = "test_02", groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_04() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7575-04",
                "Verify Same reference id cannot be used by two different users");

        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "Y");

            ConfigInput.isConfirm = false;
            TransactionManagement.init(t1)
                    .initiateAndApproveO2CWithProvider(whsUser1, provider1.ProviderName, "10", txnId);

            Assertion.verifyErrorMessageContain("error.stock.txn.already.linked",
                    "Verify reference id can not be random, or not of a valid stock initiation", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_05() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7575-05",
                "Verify O2C error message when incorrect reference id is provided and LINK_STOCK_CREATION_WITH_O2C is set to 'Y'");

        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "Y");

            // perform O2C
            ConfigInput.isConfirm = false;
            TransactionManagement.init(t1)
                    .initiateAndApproveO2CWithProvider(whsUser, provider1.ProviderName, "10", "1234");

            Assertion.verifyErrorMessageContain("error.stock.txn.id.invalid",
                    "Verify reference id can not be random, or not of a valid stock initiation", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_06() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7575-06",
                "Verify O2C error message when network stock creation is failed or not approved");
        String txnAmt = "99";
        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "Y");

            //initiate network stock
            String txnId = StockManagement.init(t1).initiateNetworkStock(provider1.ProviderName, trustBank, txnAmt);

            //initiate O2C
            ConfigInput.isConfirm = false;
            TransactionManagement.init(t1).initiateO2CWithProvider(whsUser, provider1.ProviderName, txnAmt, txnId);
            Assertion.verifyErrorMessageContain("error.stock.txn.id.invalid",
                    "Verify reference id can not be random, or not of a valid stock initiation", t1);

            //Reject initiated network stock
            ConfigInput.isConfirm = true;
            StockManagement.init(t1).rejectNetworkStockL1(txnId);

            //initiate O2C
            ConfigInput.isConfirm = false;
            TransactionManagement.init(t1).initiateO2CWithProvider(whsUser, provider1.ProviderName, txnAmt, txnId);

            Assertion.verifyErrorMessageContain("error.stock.txn.id.invalid",
                    "Verify reference id can not be random, or not of a valid stock initiation", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_07() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-7575-07",
                "Verify O2C error message when multiple O2C transactions are initiated with the same network stock transaction");
        String txnAmt = "99";
        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "Y");

            //initiate and approve network stock
            String txnId = StockManagement.init(t1)
                    .initiateAndApproveNetworkStock(provider1.ProviderName, trustBank, txnAmt);

            //initiate and reject O2C
            ConfigInput.isConfirm = false;
            TransactionManagement.init(t1).initiateAndRejectO2C(whsUser, provider1.ProviderName, txnAmt, "Pass O2C", txnId);

            //try new O2C request
            ConfigInput.isConfirm = true;
            TransactionManagement.init(t1)
                    .initiateAndApproveO2CWithProvider(whsUser1, provider1.ProviderName, "10", txnId);


            //initiate third O2C request
            ConfigInput.isConfirm = false;
            TransactionManagement.init(t1).initiateO2CWithProvider(whsUser, provider1.ProviderName, txnAmt, txnId);
            Assertion.verifyErrorMessageContain("error.stock.txn.already.linked",
                    "Verify reference id can not be random, or not of a valid stock initiation", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}

