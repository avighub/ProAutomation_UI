package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page2;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page3;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page4;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.pageObjects.userManagement.ModifyChannelUser_pg1;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.OracleDB;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.ResultSet;
import java.util.List;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : v5.1 Grade Network Association
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 12/10/2018.
 */
public class MON_7292_Grade_Network_Association extends TestInit {

    private User silverSub, chUser, silverChUser;
    private OperatorUser optUsr;
    private String msisdn, silverSubGrade, silverChUserGrade, providerName;


    /*defect: 8310 (if acquisition is not successful
     then need to restart old txn for each new network prefix
      */
    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest test = pNode.createNode("Setup", "Initiating Test");
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            silverSubGrade = DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER);
            silverChUserGrade = DataFactory.getGradeName(Constants.SILVER_WHOLESALER);
            silverSub = new User(Constants.SUBSCRIBER, silverSubGrade);
            silverChUser = new User(Constants.WHOLESALER, silverChUserGrade);
            providerName = DataFactory.getDefaultProvider().ProviderName;

            ServiceCharge acqFee = new ServiceCharge(Services.ACQFEE, silverSub, chUser,
                    null, null, null, null);

            ServiceChargeManagement.init(test)
                    .configureServiceCharge(acqFee);

            addNewNetworkPrefix("99", test);

            SystemPreferenceManagement.init(test)
                    .updateSystemPreference("IS_CHANGE_MSISDN_ALLOWED", "Y");

            /*SystemPreferenceManagement.init(test)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "N");*/

            SystemPreferenceManagement.init(test)
                    .updateSystemPreference("IS_REMITTANCE_WALLET_REQUIRED", "FALSE");
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        msisdn = "99" + DataFactory.getRandomNumberAsString(8);
    }

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest test = pNode.createNode("Test_01", "verify that user should be able create a " +
                "specific grade based on country code successfully through API");
        try {
            Transactions.init(test)
                    .gradeNetworkAssociation(Constants.SILVER_SUBSCRIBER, "99");

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            Transactions.init(test)
                    .gradeNetworkAssociation(Constants.SILVER_WHOLESALER, "99");

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("Test_02", "verify that while creating subscribers through WEB, " +
                "if the subscriber is from outside of parent country, then grade \n" +
                "which was created based upon country code should be assign to subscriber while assigning wallet");
        try {

            Login.init(test).login(chUser);
            silverSub.setMSISDN(msisdn);
            SubscriberManagement.init(test).addInitiateSubscriber(silverSub);
            CommonUserManagement.init(test).assignWebGroupRole(silverSub);

            verifySpecificGradeExists(Constants.SILVER_SUBSCRIBER, test);

            CommonUserManagement.init(test).mapWalletPreferences(silverSub);
            AddChannelUser_pg5.init(test).clickNext(silverSub.CategoryCode);
            AlertHandle.handleUnExpectedAlert(test);
            AlertHandle.handleUnExpectedAlert(test);

            CommonUserManagement.init(test)
                    .verifyAndConfirmDetails(silverSub, false);

            SubscriberManagement.init(test)
                    .addInitiatedApprovalSubs(silverSub);

            Transactions.init(test)
                    .subscriberAcquisition(silverSub);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("Test_03", "verify that while modifying subscriber through web, " +
                "if modified msisdn is from outside of parent country, then grade \n" +
                "which was created based upon country code should be assign to subscriber while assigning wallet");
        try {
            Login.init(test).login(chUser);

            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(test)
                    .addSubscriber(sub, false)
                    .addInitiatedApprovalSubs(sub);

            Transactions.init(test)
                    .subscriberAcquisition(sub);

            Login.init(test).login(chUser);
            SubscriberManagement.init(test)
                    .initiateSubscriberModification(sub);

            sub.setMSISDN(msisdn);

            ModifySubscriber_page2.init(test).setNewMSISDN(sub.MSISDN);
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            ModifySubscriber_page2.init(test).clickOnNextPg2();
            ModifySubscriber_page3.init(test).nextPage();

            verifySpecificGradeExists(Constants.SILVER_SUBSCRIBER, test);

            Utils.putThreadSleep(10000);
            completeUserModification(sub, test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_04() {
        ExtentTest test = pNode.createNode("Test_04", "verify that while creating channel user through WEB, " +
                "if the channel user is from outside of parent country, then grade \n" +
                "which was created based upon country code should be assign to channel user while assigning wallet");
        try {
            Login.init(test).login(optUsr);
            silverChUser.setMSISDN(msisdn);
            ChannelUserManagement.init(test)
                    .initiateChannelUser(silverChUser)
                    .assignHierarchy(silverChUser)
                    .assignWebGroupRole(silverChUser);

            verifySpecificGradeExists(Constants.SILVER_WHOLESALER, test);

            CommonUserManagement.init(test)
                    .mapWalletPreferences(silverChUser);

            AddChannelUser_pg5.init(test).clickNext(silverChUser.CategoryCode);
            AlertHandle.handleUnExpectedAlert(test);
            AlertHandle.handleUnExpectedAlert(test);

            AddChannelUser_pg5.init(test)
                    .completeUserCreation(silverChUser, false);

            CommonUserManagement.init(test)
                    .addInitiatedApproval(silverChUser, true);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5)
    public void Test_05() {
        ExtentTest test = pNode.createNode("Test_05", "verify that while modifying channel user through web, " +
                "if modified msisdn is from outside of parent country, then grade \n" +
                "which was created based upon country code should be assign to channel user while assigning wallet");
        try {
            User whsUsr = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(test)
                    .createChannelUserDefaultMapping(whsUsr, false);

            Login.init(test).login(optUsr);
            ChannelUserManagement.init(test)
                    .initiateChannelUserModification(whsUsr);

            String previousMsisdn = whsUsr.MSISDN;
            whsUsr.setMSISDN(msisdn);
            whsUsr.setEmail("WHS" + msisdn + "@mail.com");

            ModifyChannelUser_pg1.init(test)
                    .setEmailId(whsUsr.Email)
                    .setMsisdn(whsUsr.MSISDN);

            CommonChannelUserPage.init(test)
                    .clickNxtModify()
                    .clickNextUserHeirarcy()
                    .clickNextWebRole();

            verifySpecificGradeExists(Constants.SILVER_WHOLESALER, test);

            whsUsr.setMSISDN(previousMsisdn);

            completeUserModification(whsUsr, test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6)
    public void Test_06() {
        ExtentTest test = pNode.createNode("Test_06", "verify that user should be able delete a grade " +
                "successfully which was created based on country code through API");
        try {
            Transactions.init(test)
                    .gradeNetworkDissociation(Constants.SILVER_SUBSCRIBER, "99");

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            Transactions.init(test)
                    .gradeNetworkDissociation(Constants.SILVER_WHOLESALER, "99");
        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 7)
    public void Test_07() {
        ExtentTest test = pNode.createNode("Test_07", " verify that if no grade is created based on country code. " +
                "then subscriber should be successfully created with existing grade \n" +
                "present in the system, even if subscriber's msisdn is from outside of parent country");
        try {
            Login.init(test).login(chUser);

            User sub = new User(Constants.SUBSCRIBER);
            sub.setMSISDN(msisdn);

            SubscriberManagement.init(test)
                    .addSubscriber(sub, false)
                    .addInitiatedApprovalSubs(sub);

            Transactions.init(test)
                    .subscriberAcquisition(sub);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 8)
    public void Test_08() {
        ExtentTest test = pNode.createNode("Test_08", "verify that if no grade is created based on country code. " +
                "then subscriber should be successfully modified with existing grade \n" +
                "present in the system, even if subscriber's msisdn is from outside of parent country");
        try {
            Login.init(test).login(chUser);
            User sub = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(test)
                    .addSubscriber(sub, false)
                    .addInitiatedApprovalSubs(sub);

            Transactions.init(test)
                    .subscriberAcquisition(sub);

            Login.init(test).login(chUser);
            SubscriberManagement.init(test)
                    .initiateSubscriberModification(sub);

            sub.setMSISDN(msisdn);

            ModifySubscriber_page2.init(test)
                    .setNewMSISDN(sub.MSISDN);

            ModifySubscriber_page2.init(test).clickOnNextPg2();
            ModifySubscriber_page3.init(test)
                    .nextPage().clickSubmitPg4()
                    .clickOnConfirmPg4().clickFinalConfirm();

            String actual = Assertion.getActionMessage();
            String expected = MessageReader.getDynamicMessage("subs.modify.message.initiateBy", chUser.FirstName, chUser.LastName);
            Assertion.verifyEqual(actual, expected, "Modify User", test);

            SubscriberManagement.init(test)
                    .modifyApprovalSubs(sub);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 9)
    public void Test_09() {
        ExtentTest test = pNode.createNode("Test_09", "verify that if no grade is created based on country code. " +
                "then channel user should be successfully created with existing grade \n" +
                "present in the system, even if channel user's new msisdn is from outside of parent country");
        try {
            User whsUsr = new User(Constants.WHOLESALER);
            whsUsr.setMSISDN(msisdn);

            ChannelUserManagement.init(test)
                    .createChannelUserDefaultMapping(whsUsr, false);
        } catch (Exception e) {

        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 10)
    public void Test_10() {
        ExtentTest test = pNode.createNode("Test_10", "verify that if no grade is created based on country code. " +
                "than channel user should be successfully modified with existing grade \n" +
                "present in the system, even if channel user's new msisdn is from outside of parent country");
        try {
            User whsUsr = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(test)
                    .createChannelUserDefaultMapping(whsUsr, false);

            String previousMsisdn = whsUsr.MSISDN;

            Login.init(test).login(optUsr);

            ChannelUserManagement.init(test)
                    .initiateChannelUserModification(whsUsr);

            whsUsr.setMSISDN(msisdn);
            whsUsr.setEmail("WHS" + msisdn + "@mail.com");

            ModifyChannelUser_pg1.init(test)
                    .setEmailId(whsUsr.Email)
                    .setMsisdn(whsUsr.MSISDN);

            CommonChannelUserPage.init(test)
                    .clickNxtModify()
                    .clickNextUserHeirarcy()
                    .clickNextWebRole()
                    .clickNextWalletMap()
                    .clickNextBankMapModification()
                    .finalSubmitForModification();

            Assertion.verifyActionMessageContain("channeluser.modify.approval",
                    "Modify User initiated, " + whsUsr.LoginId, test, whsUsr.FirstName, whsUsr.LastName);

            whsUsr.setMSISDN(previousMsisdn);

            ChannelUserManagement.init(test)
                    .approveRejectModifyChannelUser(whsUsr);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    private void completeUserModification(User user, ExtentTest test) throws Exception {
        Select setGrade;
        Select setTCP;

        List<WebElement> grades = driver.findElements(By.xpath("//select[contains(@name,'channelGradeSelected')]"));
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        List<WebElement> tcp = driver.findElements(By.xpath("//select[contains(@name,'tcpSelected')]"));

        for (WebElement grade : grades) {
            setGrade = new Select(grade);
            setGrade.selectByIndex(1);
            Utils.putThreadSleep(Constants.TWO_SECONDS);
        }

        for (WebElement t : tcp) {
            setTCP = new Select(t);
            setTCP.selectByIndex(1);
            Utils.putThreadSleep(Constants.TWO_SECONDS);
        }

        if (user.GradeCode.equalsIgnoreCase(Constants.GOLD_SUBSCRIBER)) {
            ModifySubscriber_page4.init(test)
                    .clickSubmitPg4()
                    .clickOnConfirmPg4()
                    .clickFinalConfirm();

            String actual = Assertion.getActionMessage();
            String expected = MessageReader.getDynamicMessage("subs.modify.message.initiateBy", chUser.FirstName, chUser.LastName);
            Assertion.verifyEqual(actual, expected, "Modify User", test);

            SubscriberManagement.init(test)
                    .modifyApprovalSubs(user);
        } else {
            CommonChannelUserPage.init(test).clickNextWalletMap()
                    .clickNextBankMapModification().finalSubmitForModification();

            Assertion.verifyActionMessageContain("channeluser.modify.approval",
                    "Modify User initiated, " + user.LoginId, test, user.FirstName, user.LastName);

            ChannelUserManagement.init(test)
                    .approveRejectModifyChannelUser(user);
        }
    }

    private void verifySpecificGradeExists(String expectedUserGrade, ExtentTest test) {
        try {
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            String actualGrade = null;

            WebElement element = driver.findElement(By.name("counterList[0].channelGradeSelected"));

            Select select = new Select(element);
            List<WebElement> options = select.getOptions();
            if (options.size() <= 2) {
                for (WebElement option : options) {
                    if (option.getAttribute("value").equalsIgnoreCase(expectedUserGrade)) {
                        actualGrade = option.getAttribute("value");
                        break;
                    }
                }
            }

            element.click();
            Assertion.verifyEqual(actualGrade, expectedUserGrade, "Verify That Only Grade Linked With User's Country Code " +
                    "Must Be Available For Selection", test, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addNewNetworkPrefix(String networkPrefix, ExtentTest test) {
        OracleDB dbConn = new OracleDB();
        String prefix = null;
        String getCountryPrefix = "select series from network_prefixes where prefix_id = 2";
        String insertCountryPrefix = "insert into network_prefixes values('2','" + networkPrefix + "','SN','PRE','OPT','Y',null,null,null,null,null)";
        try {
            ResultSet result = dbConn.RunQuery(getCountryPrefix);
            while (result.next()) {
                prefix = result.getString("SERIES");
                if (prefix != null)
                    test.info("Country Code Already Present Under NETWORK_PREFIX Table");
            }
            if (prefix == null) {
                dbConn.RunQuery(insertCountryPrefix);
                dbConn.RunQuery("commit");
                test.info("New Network Prefix Has Been Added Successfully Under NETWORK_PREFIX Table");
            }
            Utils.putThreadSleep(Constants.TWO_SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
