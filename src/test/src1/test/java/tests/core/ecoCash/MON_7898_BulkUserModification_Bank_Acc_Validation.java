package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.entity.User;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Rahul Rana on 4/15/2019.
 */
public class MON_7898_BulkUserModification_Bank_Acc_Validation extends TestInit {

    private User whs, subs;
    private CurrencyProvider defProvider;
    private Bank trustBank, nonTrustBank;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        ExtentTest t1 = pNode.createNode("Setup", "Create setup for This Suite");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            defProvider = GlobalData.defaultProvider;
            trustBank = DataFactory.getAllTrustBanksLinkedToProvider(defProvider.ProviderName).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defProvider.ProviderName).get(0);

            whs = new User(Constants.WHOLESALER);
            subs = new User(Constants.SUBSCRIBER);

            // create users
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void mon_7898_01() {
        ExtentTest t1 = pNode.createNode("MON-7898-01",
                "Verify that With BANK_ACC_LINKING_VIA_MSISDN = 'Y', Opt user should not be able to modify Channel User by providing" +
                        "account number and customer number other than MSISDN");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            whs.custIdOptional = DataFactory.getRandomNumberAsString(9);
            whs.accNumOtional = DataFactory.getRandomNumberAsString(9);

            String file = ExcelUtil.getBulkUserRegistrationFile();
            ChannelUserManagement.init(t1)
                    .writeBulkChUserModificationDetailsInCSV(file,
                            whs,
                            defProvider.ProviderName,
                            trustBank.BankName,
                            "M",
                            "A",
                            true,
                            false);

            startNegativeTest();
            ChannelUserManagement.init(t1)
                    .initiateApproveBulkUserModification(file);

            // verify that banks are added to the User
            if (Assertion.verifyErrorMessageContain("bulk.upload.fail", "Verify that Bulk Upload has Failed", t1)) {
                Assertion.verifyErrorLogFileForMessage("error.bulk.accnum.custid.must.be.msisdn", t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void mon_7898_02() {
        ExtentTest t1 = pNode.createNode("MON-7898-02",
                "Verify that With BANK_ACC_LINKING_VIA_MSISDN = 'Y', " +
                        "Opt user should not be able to modify Channel User by providing" +
                        "account number and customer number of already registered Channel User");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            User whs02 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            whs.custIdOptional = whs02.MSISDN;
            whs.accNumOtional = whs02.MSISDN;

            String file = ExcelUtil.getBulkUserRegistrationFile();
            ChannelUserManagement.init(t1)
                    .writeBulkChUserModificationDetailsInCSV(file,
                            whs,
                            defProvider.ProviderName,
                            trustBank.BankName,
                            "M",
                            "A",
                            true,
                            false);

            startNegativeTest();
            ChannelUserManagement.init(t1)
                    .initiateApproveBulkUserModification(file);

            // verify that banks are added to the User
            if (Assertion.verifyErrorMessageContain("bulk.upload.fail", "Verify that Bulk Upload has Failed", t1)) {
                Assertion.verifyErrorLogFileForMessage("error.bulk.accnum.custid.must.be.msisdn", t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void mon_7898_03() {
        ExtentTest t1 = pNode.createNode("MON-7898-03",
                "Verify that With BANK_ACC_LINKING_VIA_MSISDN = 'Y', Opt user is able to modify " +
                        "an existing Channel user, add 1st Bank account with it's MSISDN as account number and id number");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            // set the msisdn as Cust Id and Acc num
            whs.custIdOptional = whs.MSISDN;
            whs.accNumOtional = whs.MSISDN;
            String file = ExcelUtil.getBulkUserRegistrationFile();
            ChannelUserManagement.init(t1)
                    .writeBulkChUserModificationDetailsInCSV(file,
                            whs,
                            defProvider.ProviderName,
                            trustBank.BankName,
                            "M",
                            "A",
                            true,
                            false);

            ChannelUserManagement.init(t1)
                    .initiateApproveBulkUserModification(file);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4)
    public void mon_7898_04() {
        ExtentTest t1 = pNode.createNode("MON-7898-04",
                "Verify that With BANK_ACC_LINKING_VIA_MSISDN = 'Y', Opt user is able to modify " +
                        "an existing Channel user, add 2nd Bank account with it's MSISDN as account number and id number");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
            whs.custIdOptional = whs.MSISDN;
            whs.accNumOtional = whs.MSISDN;
            String file = ExcelUtil.getBulkUserRegistrationFile();
            ChannelUserManagement.init(t1)
                    .writeBulkChUserModificationDetailsInCSV(file,
                            whs,
                            defProvider.ProviderName,
                            nonTrustBank.BankName,
                            "M",
                            "A",
                            false,
                            false);

            ChannelUserManagement.init(t1)
                    .initiateApproveBulkUserModification(file);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void mon_7898_05() {
        ExtentTest t1 = pNode.createNode("MON-7898-05",
                "Verify that With BANK_ACC_LINKING_VIA_MSISDN = 'Y', Opt user is able to modify " +
                        "an existing Subscriber user, add 1st Bank account with it's MSISDN as account number and id number");
        try {

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
            subs.custIdOptional = subs.MSISDN;
            subs.accNumOtional = subs.MSISDN;

            String file = ExcelUtil.getBulkUserRegistrationFile();

            SubscriberManagement.init(t1)
                    .writeDataToBulkSubsModificationCsv(subs,
                            file,
                            defProvider.ProviderName,
                            trustBank.BankName,
                            "M",
                            "A",
                            true,
                            false);


            SubscriberManagement.init(t1)
                    .initiateApproveBulkSubscriberModification(file);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
