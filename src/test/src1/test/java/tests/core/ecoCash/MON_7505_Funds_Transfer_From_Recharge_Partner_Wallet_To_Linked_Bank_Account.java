package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.RechargeOperator;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0 Build 11
 * Objective        : v5.1 System Case: Bank Account Association/Disassociation For Recharge Operator
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 14/01/2019
 */
public class MON_7505_Funds_Transfer_From_Recharge_Partner_Wallet_To_Linked_Bank_Account extends TestInit {

    private String user, trustBankName, nonTrustBankName, trustBankId,
            nonTrustBankId, operatorId, operatorName;
    private CurrencyProvider provider;

    private RechargeOperator rechargeOpt;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");
        try {
            provider = DataFactory.getDefaultProvider();
            user = Constants.rechargeOperator;

            trustBankName = DataFactory.getTrustBankName(provider.ProviderName);
            trustBankId = DataFactory.getBankId(trustBankName);

            nonTrustBankName = DataFactory.getNonTrustBankName(provider.ProviderName);
            nonTrustBankId = DataFactory.getBankId(nonTrustBankName);

            operatorId = MobiquityGUIQueries.getOperatorId();
            if (operatorId == null) {
                Login.init(setup).login(setup);
                rechargeOpt = new RechargeOperator(provider.ProviderId);
                OperatorUserManagement.init(setup).addRechargeOperator(rechargeOpt);
                operatorId = rechargeOpt.OperatorId;
            }
            operatorName = MobiquityGUIQueries.getOperatorName(operatorId);

            SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {
        ExtentTest test = pNode.createNode("Test_01", "verify the a valid user should be able to associate " +
                "recharge operator wallet with bank");
        try {

            BankAccountAssociation.init(test)
                    .associateBankAccountsForRechargeOperator(provider.ProviderName, user, operatorName, trustBankName);

            BankAccountAssociation.init(test)
                    .associateBankAccountsForRechargeOperator(provider.ProviderName, user, operatorName, nonTrustBankName);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_02() throws Throwable {
        ExtentTest test = pNode.createNode("Test_02", "verify the a valid user should be able to disassociate recharge operator wallet with bank");
        try {

            String bankAccNum = MobiquityGUIQueries.getOperatorBankAccountNum(null, trustBankId, operatorName);
            BankAccountAssociation.init(test)
                    .disassociateBankAccountsForRechargeOperator(provider.ProviderName, user, operatorName, bankAccNum);

            bankAccNum = MobiquityGUIQueries.getOperatorBankAccountNum(null, nonTrustBankId, operatorName);
            BankAccountAssociation.init(test)
                    .disassociateBankAccountsForRechargeOperator(provider.ProviderName, user, operatorName, bankAccNum);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest test = pNode.createNode("Teardown", "Concluding Test");
        try {
            SystemPreferenceManagement.init(test)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
