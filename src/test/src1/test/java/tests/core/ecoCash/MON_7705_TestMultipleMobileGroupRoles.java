package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.Wallet;
import framework.entity.MobileGroupRole;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.MobileRoles;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class MON_7705_TestMultipleMobileGroupRoles extends TestInit {

    private MobileGroupRole role1, role2, role3;
    private User receiver;
    private CurrencyProvider provider1;
    private Wallet wallet1;
    private String defaultBank;
    private User whs;
    private BigDecimal amt;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        ExtentTest t1 = pNode.createNode("createMultipleMobileRoles", "Verify creating multiple mobile roles");

        try {
            provider1 = DataFactory.getDefaultProvider();
            wallet1 = GlobalData.defaultWallet;
            defaultBank = GlobalData.defaultBankName;
            receiver = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(receiver);

            role1 = new MobileGroupRole(receiver.CategoryCode, receiver.GradeName);
            role1.setMobileRoleName("AUTW1" + receiver.CategoryCode + receiver.GradeCode);
            role1.setMobileRoleCode(role1.ProfileName);
            role1.addApplicableService(MobileRoles.WALLET_P2P_SEND_MONEY);
            role1.addApplicableService(MobileRoles.WALLET_CASHIN_RECEIVER);
            role1.setProvider(provider1.ProviderName);
            role1.setPayInstrument("WALLET");
            role1.setPayInstrumentDetails(wallet1.WalletId);

            // create wallet mobile role and do not add p2p send money service
            role2 = new MobileGroupRole(receiver.CategoryCode, receiver.GradeName);
            role2.setMobileRoleName("AUTW2" + receiver.CategoryCode + receiver.GradeCode);
            role2.setMobileRoleCode(role2.ProfileName);
            role2.addApplicableService(MobileRoles.WALLET_P2P_SEND_MONEY);
            role2.removeApplicableService(MobileRoles.WALLET_CASHIN_RECEIVER);
            role2.setProvider(provider1.ProviderName);
            role2.setPayInstrument("WALLET");
            role2.setPayInstrumentDetails(wallet1.WalletId);

            //Bank Role 1
            role3 = new MobileGroupRole(receiver.CategoryCode, receiver.GradeName);
            role3.setMobileRoleName("AUTB1" + receiver.CategoryCode + receiver.GradeCode);
            role3.setMobileRoleCode(role3.ProfileName);
            role3.addApplicableService(MobileRoles.BANK_RECHARGE_OTHER);
            role3.addApplicableService(MobileRoles.BANK_UTILITY_BILL_PAY);
            role3.setProvider(provider1.ProviderName);
            role3.setPayInstrument("BANK");
            role3.setPayInstrumentDetailsUsingName(defaultBank);

            GroupRoleManagement.init(t1).addUpdateMobileRole(role1);
            GroupRoleManagement.init(t1).addUpdateMobileRole(role2);
            GroupRoleManagement.init(t1).addUpdateMobileRole(role3);

            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(whs);

            amt = new BigDecimal("5");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7705_1() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_7705_1",
                "Create roles combinations with different services and assign to users.");


        try {
            User sub1 = new User(Constants.SUBSCRIBER);
            sub1.addMobileRole(role1); // assign wallet role 1
            sub1.addMobileRole(role3); // assign wallet role 3

            User sub2 = new User(Constants.SUBSCRIBER);
            sub2.addMobileRole(role2);

            SubscriberManagement.init(t1).createSubscriber(sub1);
            SubscriberManagement.init(t1).createSubscriber(sub2);

            //try performing P2P send money which is an allowed service with mobile role 1
            Transactions.init(t1)
                    .initiateCashIn(sub1, whs, new BigDecimal(2));

            startNegativeTest();
            Transactions.init(t1)
                    .initiateCashIn(sub2, whs, new BigDecimal(3))
                    .verifyMessage("service.failure.requisite.rights.missing",
                            "Verify that Service is Failed when User doesn't have correct Mobile Role assigned - User Created through UI");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 2)
    public void MON_7705_2() throws Exception {
        ExtentTest t2 = pNode.createNode("MON_7705_2",
                "Verify that User is successfully created and has mobile role associated.");

        try {
            User sub_03 = new User(Constants.SUBSCRIBER);
            sub_03.addMobileRole(role1);

            // map wallet preference for new Wallet
            SubscriberManagement.init(t2).createDefaultSubscriberUsingAPI(sub_03);

            // do Cash in and try other service which is not there in role 1
            Transactions.init(t2)
                    .initiateCashIn(sub_03, whs, amt);

            User sub_04 = new User(Constants.SUBSCRIBER);
            sub_04.addMobileRole(role3);

            // map wallet preference for new Wallet
            SubscriberManagement.init(t2).createDefaultSubscriberUsingAPI(sub_04);

            // do p2p and try other service which is not there in role 1

            startNegativeTest();
            Transactions.init(t2)
                    .initiateCashIn(sub_04, whs, amt)
                    .verifyMessage("service.failure.requisite.rights.missing",
                            "Verify that Service is Failed when User doesn't have correct Mobile Role assigned - user created through API");
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3)
    public void MON_7705_3() {
        ExtentTest test = pNode.createNode("Test_05", "Verify Users can be created in Bulk using Multiple Mobile Roles");
        try {

            User sub_04 = new User(Constants.SUBSCRIBER);
            sub_04.addMobileRole(role1);

            User sub_05 = new User(Constants.SUBSCRIBER);
            sub_05.addMobileRole(role2);

            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            Login.init(test).login(optUsr);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(test);

            page.navBulkChUserAndSubsciberRegistrationAndModification()
                    .SelectSubscriberRegistrationService();

            SubscriberManagement.init(test)
                    .writeDataToBulkSubsRegistrationCsv(sub_04,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            false)
                    .writeDataToBulkSubsRegistrationCsv(sub_05,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            true);

            String batchId = page.uploadFile(FilePath.fileBulkSubReg)
                    .submitCsv()
                    .getBatchId();

            page.navBulkRegistrationAndModificationMyBatchesPage()
                    .ClickOnBatchSubmit()
                    .downloadLogFileUsingBatchId(batchId);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void teardown() {
        ExtentTest t1 = pNode.createNode("teardown", "teardown specific for this suite");
        try {
            String[] payIdArr = DataFactory.getPayIdApplicableForSubs();
            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(new User(Constants.SUBSCRIBER), payIdArr);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }
}
