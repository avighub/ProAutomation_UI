package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Reverse_Automated_O2C
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_Reverse_Automated_O2C extends TestInit {

    @Test(priority = 1)
    public void UAT_Reverse_Automated_O2C_TUNG51547() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51547",
                "To verify that the Reverse Automated O2C transaction should be successful\n" +
                        "1) When both service charge and combination is defined like Service Charge % and fixed and Commission % and fixed.\n" +
                        "2) All the Tax Applicable check boxes are selected and both % and fixed taxes are define.");

        try {
            //create a channel user
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            //perform O2C
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs);
            TransactionManagement.init(t1).performO2CforCommissionWallet(whs, "10", "Test");
            //create transfer rule and service charge for automatic reimbursement
            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);

            ServiceCharge auto_Reimbursement = new ServiceCharge(Services.AUTO_REIMBURSEMENT, whs, opt,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(auto_Reimbursement);

            //get the initial balance of channel user
            UsrBalance prebalance = MobiquityGUIQueries.
                    getUserBalance(whs, null, null);

            BigDecimal initUsrBalance = prebalance.Balance;
            t1.info("Initial user balance is " + initUsrBalance);

            //get the initial balance of IND01
            BigDecimal optPreBalance = MobiquityGUIQueries
                    .fetchOperatorBalance(DataFactory.getDefaultProvider().ProviderId, "IND01");

            t1.info("Initial system balance is " + optPreBalance);

            //perform stock management

            StockManagement.init(t1).initiateAndApproveNetworkStock(DataFactory.getDefaultProvider()
                    .ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "20");

            //performing reversed automated 02C
            Transactions.init(t1)
                    .autoReimbursement(whs, "10", DataFactory.getDefaultBankIdForDefaultProvider(), DataFactory.getRandomNumberAsString(4));

            //check user balance got debited
            UsrBalance postbalance = MobiquityGUIQueries.
                    getUserBalance(whs, null, null);

            BigDecimal currentUsrBalance = postbalance.Balance;

            ChannelUserManagement.init(t1).
                    checkIfUserBalanceisCredited(initUsrBalance, currentUsrBalance, true, "ChannelUser");

            //check IND01 balance got credited
            OperatorUserManagement.init(t1).operatorBalancecheck(optPreBalance,
                    false, DataFactory.getDefaultProvider().ProviderId, "IND01");

            //Check channel user has received SMS
            SMSReader.init(t1)
                    .verifyNotificationContain(whs.MSISDN, "reverse.automated.notification.sms", "10");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
