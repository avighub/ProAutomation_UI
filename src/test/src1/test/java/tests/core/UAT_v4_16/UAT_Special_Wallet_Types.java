package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that commission wallet cannot be de-associated with channel user if commission wallet contain balance.
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_Special_Wallet_Types extends TestInit {

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG47072", "To verify that commission wallet cannot be de-associated with channel user if commission wallet contain balance.");

        try {
            User chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            TransactionManagement.init(t1)
                    .performO2CforCommissionWallet(chUsr, "10", "Test");

//            ChannelUserManagement.init(t1)
//                    .suspendOrDeleteUserWallet(chUsr,"Deleted",Constants.COMMISSION_WALLET);

            //Checking that channel user cannot be deleted
            ChannelUserManagement.init(t1).startNegativeTest()
                    .initiateChannelUserDelete(chUsr);

            Assertion.verifyErrorMessageContain("some.wallets.have.balance",
                    "Wallet cannot be deleted, balance associated with the user wallet", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
