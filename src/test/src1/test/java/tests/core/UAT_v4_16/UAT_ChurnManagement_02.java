package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_ChurnManagement_02
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_ChurnManagement_02 extends TestInit {


    @Test(priority = 1, enabled = false)
    public void UAT_Churn_Management_CR_TUNG12116() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12116", "To verify that system should\n" +
                " not able to approve modify initiated user if subscriber is already churn initiated");

        try {
            User usr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            Login.init(t1).login(usr);
            User subs = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(t1)
                    .createDefaultSubscriberUsingAPI(subs);

            SubscriberManagement.init(t1)
                    .subscriberModificationInitiation(subs);

            CommonUserManagement.init(t1).churnInitiateUser(subs);

            Login.init(t1).login(usr);
            SubscriberManagement.init(t1)
                    .startNegativeTest()
                    .modifyApprovalSubs(subs);

            //party.message.churn.approval
            Assertion.verifyErrorMessageContain("party.message.churn.approval",
                    "verify that Cannot approve modification", t1, subs.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void UAT_Churn_Management_CR_TUNG12120() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12120",
                "To verify that the system should not allow to churn initiate for channel user\n" +
                        " like Wholesaler/Headmerchant who has child user.");

        try {
            //Get a Parent Channel Channel User
            String parentCategory = DataFactory.getParentCategoryCode(Constants.RETAILER);
            User whs = DataFactory.getChannelUserWithCategory(parentCategory);
            //Churn the wholesaler
            CommonUserManagement.init(t1)
                    .startNegativeTest()
                    .churnInitiateUser(whs);

            ChurnUser_Page1.init(t1).checkLogFileSpecificMessage(whs.MSISDN, "churn.error.UserWithChild");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, enabled = false)
    public void UAT_Churn_Management_CR_TUNG12145_TUNG12171() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12145", "To verify that settlement\n" +
                " of churned balance(s) can be initiated only to a registered customer's primary wallet." +
                " To verify that settlement amount should be credited into Primary wallet of Reciever Subscriber.");


        ExtentTest t2 = pNode.createNode("TUNG12171", "To verify that settlement\n" +
                " of churned balance(s) can be initiated only to a registered customer's primary wallet." +
                " To verify that settlement amount should be credited into Primary wallet of Reciever Subscriber.");

        try {
            //get an existing subscriber
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //get subscriber balance
            UsrBalance subsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            BigDecimal initSubsBalance = subsBalance.Balance;

            //create channel user
            User whsTobeChurned = CommonUserManagement.init(t1).getChurnedUser(Constants.WHOLESALER, null);

            //get the channeluser initial balance
            UsrBalance preWhsbalance = MobiquityGUIQueries.getUserBalance(whsTobeChurned, null, null);
            BigDecimal initbalance = preWhsbalance.Balance;


            //get initail churn balance
            BigDecimal preChurnBalance = MobiquityGUIQueries
                    .fetchOperatorBalance(DataFactory.getDefaultProvider().ProviderId, "IND10");

            //churn the channel user
            CommonUserManagement.init(t1).churnInitiateAndApprove(whsTobeChurned);

            Thread.sleep(3000);

            //check the channeluser balance is debited
            BigDecimal currentuserbalance;

            UsrBalance postWhsbalance = MobiquityGUIQueries.
                    getUserBalance(whsTobeChurned, null, null);

            currentuserbalance = postWhsbalance.Balance;


            // and churnwallet balance is credited
            BigDecimal postChurnBalance = OperatorUserManagement.init(t1).
                    operatorBalancecheck(preChurnBalance, false, "101", "IND10");

            //churn settelment
            Thread.sleep(10000);

            CommonUserManagement.init(t1).
                    initiateChurnSettlement(whsTobeChurned, subs);

            CommonUserManagement.init(t1).
                    approveChurnSettlement(whsTobeChurned.MSISDN);

            //After settlement get the channel user balance
            UsrBalance BalanceAfterSettlement = MobiquityGUIQueries.
                    getUserBalance(subs, null, null);

            BigDecimal subsBalanceAfterSettlement = BalanceAfterSettlement.Balance;


            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(initSubsBalance,
                    subsBalanceAfterSettlement, false, "Subscriber");


            BigDecimal churnbalanceAfterSettlement = OperatorUserManagement.init(t1).
                    operatorBalancecheck(postChurnBalance, true, "101", "IND10");

            SMSReader sms = new SMSReader();
            sms.verifyRecentNotification(subs.MSISDN, "churn.settle.label.sms.amount.transfer", subs.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
