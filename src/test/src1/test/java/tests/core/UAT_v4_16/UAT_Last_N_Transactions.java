package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : Last N Transactions Details
 * Created Date     : 02/02/2018
 * @author          :gurudatta.praharaj
 */
public class UAT_Last_N_Transactions extends TestInit {
    private OperatorUser opt;
    private User sub, wholeSaler;
    private String provider;
    private ServiceCharge LastPendingTxn;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Pre Condition", "Pre-Requisite,Define service and Commission for Last N transaction");

        try {
            //Login using operator user
            provider = DataFactory.getDefaultProvider().ProviderName;
            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB");
            sub = new User(Constants.SUBSCRIBER);

            TransactionManagement.init(setup).makeSureChannelUserHasBalance(wholeSaler);

            //creating transfer rules for cash out
            ServiceCharge CashOut = new ServiceCharge(Services.CASHOUT, sub, wholeSaler, null, null, null, null);
            TransferRuleManagement.init(setup).configureTransferRule(CashOut);

            if (!ConfigInput.isCoreRelease) {
                ServiceChargeManagement.init(setup).configureServiceCharge(CashOut);
            }

            //defining service charge for lastNTransactionsHistory

            LastPendingTxn = new ServiceCharge(Services.LAST_PENDING_TRANSACTION, sub, opt, null, null, null, null);
            ServiceChargeManagement.init(setup).
                    configureNonFinancialServiceCharge(LastPendingTxn);

            //modify service charge for lastNTransactionsHistory For Barred Users
            LastPendingTxn.setNFSCServiceCharge("0");
            LastPendingTxn.setNFSCCommission("0");

            ServiceChargeManagement.init(setup)
                    .modifyNFSCInitAndApprove(LastPendingTxn);

            //create subscriber & perform cash in
            SubscriberManagement.init(setup)
                    .createDefaultSubscriberUsingAPI(sub);

            Transactions.init(setup)
                    .initiateCashIn(sub, wholeSaler, new BigDecimal("10"));

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest test = pNode.createNode("Test_UAT_TUNG51528", "To verify that Subscriber having  USSD  rights " +
                "can check his last Three pending transactions.");

        //login as channel user
        try {
            Login.init(test).login(wholeSaler);

            // initiate 3 cash out transactions without approval (cash out must be two step)
            for (int i = 1; i <= 3; i++) {
                Transactions.init(test)
                        .initiateCashOut(sub, wholeSaler, new BigDecimal(Integer.toString(i)));
                Utils.putThreadSleep(Constants.TWO_SECONDS);
            }
            Transactions.init(test)
                    .Last_Pending_Transaction(sub, "3");
        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() throws Exception {
        ExtentTest test = pNode.createNode("Test_UAT_TUNG51529", "To verify that the Barred Subscriber can check his last 3 Pending transaction.");
        try {
            //bar the subscriber as both
            Login.init(test).login(wholeSaler);
            SubscriberManagement.init(test)
                    .barSubscriber(sub, Constants.BAR_AS_BOTH);

            //check pending transaction history
            Transactions.init(test)
                    .Last_Pending_Transaction(sub, "3");
        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void teardown() {
        ExtentTest teardown = pNode.createNode("Post Condition");
        try {
            LastPendingTxn.setNFSCServiceCharge("0.1");
            LastPendingTxn.setNFSCCommission("0.1");

            ServiceChargeManagement.init(teardown)
                    .modifyNFSCInitAndApprove(LastPendingTxn);
        } catch (Exception e) {
            markTestAsFailure(e, teardown);
        }
        Assertion.finalizeSoftAsserts();
    }
}
