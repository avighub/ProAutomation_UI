package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.pageObjects.autoDebit.AutoDebit_SI_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that customer can self initiate to disable the Standing-Instruction by USSD
 * Author Name      : Nirupama mk
 * Created Date     : 02/02/2018
 */
public class UAT_StandingInstructions extends TestInit {

    private User sub1, sub2, chUser;
    private OperatorUser optUser1, cce;
    private ServiceCharge standingInstructionsService;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {

        ExtentTest setup = pNode.createNode("setup", "Pre-Condition,Configuring Non-Financial Service Charges");

        try {

            // fetch existing Users from App Data
            sub1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            sub2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUser1 = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
            cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);

            // Configuring non-financial services charge
            standingInstructionsService = new ServiceCharge(Services.ENABLE_STANDING_INSTRUCTIONS_BY_SUBS, sub1, optUser1, null, null, null, null);

            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceCharge(standingInstructionsService);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() {
        ExtentTest t1 = pNode.createNode("TUNG48858", "Verify that Subscriber can enable & disable Standing Instruction and view beneficiaries via USSD");
        try {

            Transactions.init(t1).enableStandardInstructionsBySubscriber(sub1, sub2, "20", "DAILY")
                    .verifyStatus(Constants.TXN_SUCCESS);

            Transactions.init(t1).viewListOfBeneficiaryForADSI(sub1, "SI")
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyBeneficiaryMsisdn(sub2.MSISDN)
                    .verifyDebitAmount("20.00");

            Transactions.init(t1).disableStandardInstructionsBySubscriber(sub1, sub2)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyMessage("si.disable");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG48985", "Verify that Channel user is able to enable & disable Standing Instruction service via WEB");
        try {
            Login.init(t1).login(chUser);
            AutoDebit_SI_pg1.init(t1).enableStandingInstructionsDetails(sub1, sub2, "10");
            Assertion.verifyActionMessageContain("si.message.enable.request.success.fully.done", "Standing Instruction Enabled successfully", t1, sub1.MSISDN, sub2.MSISDN);

            AutoDebit_SI_pg1.init(t1).disableStandingInstructionsDetails(sub1, sub2);
            Assertion.verifyActionMessageContain("si.disabled.successfully", "Standing Instruction Disabled successfully", t1, sub1.MSISDN, sub2.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG1295", "Verify that CCE is able to enable Standing Instruction service via WEB");
        try {
            Login.init(t1).login(cce);
            AutoDebit_SI_pg1.init(t1).enableStandingInstructionsDetails(sub1, sub2, "10");
            Assertion.verifyActionMessageContain("si.message.enable.request.success.fully.done", "Standing Instruction Enabled successfully", t1, sub1.MSISDN, sub2.MSISDN);

            ExtentTest t2 = pNode.createNode("TUNG1298 ", "Verify that CCE is able to view list of beneficiary for Standing Instruction via WEB");

            Login.init(t2).login(cce);
            AutoDebit_SI_pg1.init(t2).viewListOfStandingInstructionBeneficiary(sub1);

            String benMsisdn = driver.findElement(By.xpath("(//*[contains(text(),'Beneficiary MSISDN')]/../..//td[@class='tabcol'])[1]")).getText();
            String amount = driver.findElement(By.xpath("(//*[contains(text(),'Debit Amount')]/../..//td[@class='tabcol'])[2]")).getText();

            Assertion.verifyEqual(benMsisdn, sub2.MSISDN, "Verifying Customer's MSISDN", t2);
            Assertion.verifyEqual(amount, "10", "Verifying Amount", t2, true);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

        ExtentTest t3 = pNode.createNode("TUNG1296 ", "Verify that Channel user is able to disable Standing Instruction service via WEB");
        try {
            Login.init(t3).login(cce);
            AutoDebit_SI_pg1.init(t3).disableStandingInstructionsDetails(sub1, sub2);
            Assertion.verifyActionMessageContain("si.disabled.successfully", "Standing Instruction Disabled successfully", t3, sub1.MSISDN, sub2.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}