package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : UAT_AutoDebit_03
 * Author Name      : Pushpalatha Pattabiraman, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_AutoDebit_03 extends TestInit {

    private Biller biller;
    private OperatorUser optUsr;
    private User subUsr;
    private String providerName, billAccNumber;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Setup specific to this Script");

        try {
            ServiceCharge utilityReg = new ServiceCharge(Services.UTILITY_REGISTRATION, new User(Constants.SUBSCRIBER), new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);
            ServiceCharge autoDebitEnable = new ServiceCharge(Services.ENABLE_AUTODEBIT, new User(Constants.SUBSCRIBER), new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(utilityReg);
            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(autoDebitEnable);

            subUsr = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            TransactionManagement.init(setup).makeSureLeafUserHasBalance(subUsr);

            optUsr = DataFactory.getOperatorUserWithAccess("UTL_BILREG");

            providerName = DataFactory.getDefaultProvider().ProviderName;

            biller = BillerManagement.init(setup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void TUNG9424() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG9424",
                "To verify that subscriber can initiate to enable the Auto-Debit by USSD");
        try {

            Login.init(t1).login(optUsr);
            //Add bill for customer
            billAccNumber = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subUsr.MSISDN, billAccNumber);

            BillerManagement.init(t1).initiateSubscriberBillerAssociation(biller);

            //Initial Balance of subscriber
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(subUsr, null, null);
            BigDecimal initialBal = balance.Balance;

            Transactions.init(t1)
                    .autoDebitSubsSelfReq(biller.BillerCode, subUsr.MSISDN, billAccNumber, "5");

            //Check for Approval notification
            SMSReader.init(t1)
                    .verifyNotificationContain(subUsr.MSISDN, "autodebit.subsSelf.notification");

            //Login as biller
            Login.init(t1).login(biller);

            //Approve initiated Auto Debit Enable request
            BillerManagement.init(t1)
                    .approveorRejectAutoDebitEnableByBiller(subUsr, true);

            //Get the current balance of subscriber
            balance = MobiquityGUIQueries.getUserBalance(subUsr, null, null);
            BigDecimal currentBal = balance.Balance;

            //check service charge is deducted from subscriber
            BigDecimal amount = initialBal.subtract(currentBal);
            Assertion.verifyAccountIsDebited(initialBal, currentBal, amount,
                    "Service Charge Is Deducted From Subscriber's Normal Wallet", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("teardown", "teardown specific to this script");
        /*
        remove subscriber from biller
        delete the biller
         */
        try {
            Login.init(tearDown).login(biller);

            CustomerBill bill = biller.getAssociatedBill();
            BillerManagement.init(tearDown)
                    .deleteSubsBillerAssociation(bill);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
