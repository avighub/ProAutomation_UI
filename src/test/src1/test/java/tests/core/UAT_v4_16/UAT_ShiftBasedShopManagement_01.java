package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.Employee;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : Verify that non-financial service charge defined for Create Employee service is deducted properly
 * When Service Charge fixed is defined and Commission fixed is defined
 * Author Name      : Nirupama MK, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_ShiftBasedShopManagement_01 extends TestInit {
    private ServiceCharge sCharge;
    private OperatorUser operator;
    private User whsUser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");

        try {
            whsUser = new User(Constants.WHOLESALER);
            operator = new OperatorUser(Constants.OPERATOR);

            sCharge = new ServiceCharge(Services.CREATE_EMPLOYEE, whsUser, operator,
                    null, null, null, null);

            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceCharge(sCharge);

            operator = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    //Defect: Service charge is not deducting after employee creation (MOB-408)
    public void Test_01() {
        ExtentTest t1 = pNode.createNode("TUNG51550 ", " Verify that non-financial service charge\n" +
                " defined for Create Employee service is deducted properly:\n" +
                "When Service Charge fixed is defined and Commission fixed is defined");
        try {
            // existing chnlUsr might already be associated with employee, Hence Create new chnlUser
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whsUser, false);

            TransactionManagement.init(t1)
                    .initiateAndApproveO2C(whsUser, "10", "Test");

            UsrBalance initialUserBalance = MobiquityGUIQueries.
                    getUserBalance(whsUser, null, null);

            Employee employee = new Employee(whsUser.MSISDN, whsUser.CategoryCode);
            Transactions.init(t1)
                    .createEmployee(employee);

            SMSReader.init(t1).verifyNotificationContain(whsUser.MSISDN, "employee.creation");

            UsrBalance postUserBalance = MobiquityGUIQueries.
                    getUserBalance(whsUser, null, null);

            BigDecimal amount = postUserBalance.Balance.subtract(initialUserBalance.Balance);

            Assertion.verifyAccountIsNotAffected(initialUserBalance.Balance, postUserBalance.Balance,
                    "Service Charge Of Amount " + amount + " Successfully Debited", t1);

            // todo, above is  not an exact validation, as the fixed service charge is set to 0 by default
            // much elaborate verifications are required
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    // @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("Teardown", "Concluding Test");
        try {
            Login.init(tearDown).login(operator);
            sCharge.setIsNFSC();
            ServiceChargeManagement.init(tearDown)
                    .deleteNFSChargeInitiate(sCharge)
                    .approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_DELETE);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}