package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Employee;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Shop Manager can get the list of employees created under him.
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_ShiftBasedShopManagement_02 extends TestInit {
    private User retailer;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            retailer = new User(Constants.RETAILER);

            ServiceCharge createEmp = new ServiceCharge(Services.CREATE_EMPLOYEE, retailer, new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);
            ServiceCharge getEmp = new ServiceCharge(Services.Get_EmployeeList, retailer, new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(getEmp);
            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(createEmp);

            ChannelUserManagement.init(setup).createChannelUserDefaultMapping(retailer, false);
            TransactionManagement.init(setup).initiateAndApproveO2C(retailer, Constants.MIN_O2C_AMOUNT, "Test");
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51557", "To verify that Shop Manager can get the list of employees created under him.");

        try {
            //create Employee
            Employee emp = new Employee(retailer.MSISDN, retailer.CategoryCode);
            Transactions.init(t1).createEmployee(emp);

            // Get list of Employees
            Transactions.init(t1).getEmployeeList(emp);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
