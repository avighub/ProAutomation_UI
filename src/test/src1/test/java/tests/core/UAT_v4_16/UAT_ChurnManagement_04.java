package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify whether the system is displaying all settlement pending transaction for churn settlement.");
 * Author Name      : Nirupama mk
 * Created Date     : 02/02/2018
 */

public class UAT_ChurnManagement_04 extends TestInit {
    private int currentuserbalance, initbalance;

    @Test(priority = 1)
    public void Test_01() throws Exception {

        ExtentTest t2 = pNode.createNode("TUNG12173 ", "To verify whether the system\n" +
                " is displaying all settlement pending transaction for churn settlement.");
        try {
            //create subscriber with Acquisition
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            User channeluser = CommonUserManagement.init(t2).getChurnedUser(Constants.WHOLESALER, null);

            OperatorUser OptUser4 = DataFactory.getOperatorUsersWithAccess("CHURNMGMT_MAIN").get(0);
            Login.init(t2).login(OptUser4);

            //Churn settlement initiation  to an active subscriber
            CommonUserManagement.init(t2).initiateChurnSettlement(channeluser, sub);

            ChurnUser_Page1.init(t2).navUserChurn_Settlement_Approval().selectRecord(channeluser.MSISDN).rejectRecord().confirmApproval();

            Assertion.verifyErrorMessageContain("churn.settlement.rejected", "settlement rejected", t2);

            CommonUserManagement.init(t2).initiateChurnSettlement(channeluser, sub);

            CommonUserManagement.init(t2).approveChurnSettlement(channeluser.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}


