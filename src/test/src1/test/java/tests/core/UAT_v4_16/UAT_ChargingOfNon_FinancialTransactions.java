package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that active Channel User should be able to do
 * the balance enquiry if Service Category Type is selected as Wallet
 * Author Name      : Nirupama mk, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_ChargingOfNon_FinancialTransactions extends TestInit {

    private User channeluse;
    private OperatorUser o2cinit;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");

        try {

            o2cinit = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            channeluse = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //  service charges for non-Financial Transactions
            ServiceCharge chUserWalletBal = new ServiceCharge(Services.BALANCE_ENQUIRY, new User(Constants.WHOLESALER), new OperatorUser(Constants.NETWORK_ADMIN),
                    null, null, null, null);

            ServiceCharge subUserWalletBal = new ServiceCharge(Services.BALANCE_ENQUIRY, new User(Constants.SUBSCRIBER), new OperatorUser(Constants.NETWORK_ADMIN),
                    null, null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(chUserWalletBal);
            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(subUserWalletBal);


        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void TUNG51438() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51438 ", "To verify that active Channel User\n" +
                " should be able to do the balance enquiry if Service Category Type is selected as Wallet.");
        try {
            TransactionManagement.init(t1).initiateAndApproveO2C(channeluse, "10", "Test");

            Transactions.init(t1).BalanceEnquiry(channeluse);
            SMSReader.init(t1).verifyNotificationContain(channeluse.MSISDN, "balance.enq");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void TUNG51440() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51440 ", "To verify that channel user should be\n" +
                " able to do the balance enquiry of the commission wallet.");
        try {
            //  O2C transaction for "Commission" Wallet
            Login.init(t1).login(o2cinit);

            TransferRuleManagement.init(t1).CreateO2CTRulesForCommissionWallet();
            Login.init(t1).login(o2cinit);

            String txn = TransactionManagement.init(t1)
                    .initiateO2CForSpecificWallet(channeluse,
                            DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName,
                            DataFactory.getDefaultProvider().ProviderId,
                            "65", "5646");

            TransactionManagement.init(t1).o2cApproval1(txn);

            Transactions.init(t1).BalanceEnquiry(channeluse);
            SMSReader.init(t1).verifyNotificationContain(channeluse.MSISDN, "balance.enq");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51441 ", " WEB : To verify that Subscriber should\n" +
                " not be able to do the Balance Enquiry if default wallet doesn't have sufficient amount\n" +
                " for Wallet balance enquiry.");

        try {
            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(sub);

            TxnResponse response = Transactions.init(t1).subscriberBalanceEnquiry(sub);
            response.assertStatus("99990");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}