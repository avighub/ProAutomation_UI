package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Channel user can register customer in Mobiquity system using USSD
 * Author Name      : Nirupama mk
 * Created Date     : 02/02/2018
 */
public class UAT_DecimalPointsChangesAcrossTheSystem extends TestInit {

    private String PrefCode = "AMT_DISPLAY_POLICY", DefaultValue;
    private String amount = "10.03";
    private User channeluser;
    private OperatorUser o2cinit;


    @BeforeClass(alwaysRun = true)
    public void precondition() {

        ExtentTest setup = pNode.createNode("Setup", "Setup Specific to this Script");
        try {

            DefaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference(PrefCode);

            o2cinit = DataFactory.getOperatorUsersWithAccess("O2C_INIT").get(0);
            channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //  service charges for non-Financial Transactions
            ServiceCharge balance = new ServiceCharge(Services.BALANCE_ENQUIRY, channeluser, o2cinit, null, null, null, null);
            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(balance);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest t1 = pNode.createNode("TUNG6397 ", "To verify that the transaction amount and service charge\n" +
                " values displayed in SMS and logs depends on the preference 'AMT_DISPLAY_POLICY'.");
        try {
            // to update System Preference to "RULE1"
            SystemPreferenceManagement.init(t1).updateSystemPreference(PrefCode, "RULE1");

            // O2C transaction
            Login.init(t1).login(o2cinit);
            String txnid = TransactionManagement.init(t1).initiateO2C(channeluser, amount, "1234");
            TransactionManagement.init(t1).o2cApproval1(txnid);

            // fetch the balance
            UsrBalance balnc = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            String balnc1 = balnc.Balance.toString();
            System.out.println(balnc1);

            try {
                double balnc2 = Double.parseDouble(balnc1);
                t1.pass("set to RULE2 :: Valid Format :: " + balnc2);
            } catch (NumberFormatException e) {
                t1.fail("fail :: invalid format :: " + balnc1);
            }

            //  check Channel user in the SMS notification
            Transactions.init(t1).BalanceEnquiry(channeluser);
            String amount = SMSReader.init(t1).fetchAmountFromNotification(channeluser.MSISDN, "balance.enq");
            try {
                double amt = Double.parseDouble(amount);
                t1.pass("set to RULE3 :: Vaid Format :: " + amt);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                t1.fail("fail :: invalid format :: " + amount);
                Assert.fail();
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest t1 = pNode.createNode("TUNG6397 ", "To verify that the transaction amount and service charge\n" +
                " values displayed in SMS and logs depends on the preference 'AMT_DISPLAY_POLICY'.");
        try {
            //  to update system Preference to "RULE2"
            SystemPreferenceManagement.init(t1).updateSystemPreference(PrefCode, "RULE2");

            //  O2C transaction
            Login.init(t1).login(o2cinit);
            String txnid = TransactionManagement.init(t1).initiateO2C(channeluser, amount, "1234");
            TransactionManagement.init(t1).o2cApproval1(txnid);

            // fetch the balance
            UsrBalance balnc = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            String balnc1 = balnc.Balance.toString();

            try {
                double balnc2 = Double.parseDouble(balnc1);
                t1.pass("set to RULE2 :: Valid Format :: " + balnc2);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                t1.fail("fail :: invalid format :: " + balnc1);
            }

            //  check Channel user in the SMS notification
            Transactions.init(t1).BalanceEnquiry(channeluser);
            String amount = SMSReader.init(t1).fetchAmountFromNotification(channeluser.MSISDN, "balance.enq");
            try {
                double amt = Double.parseDouble(amount);
                t1.pass("set to RULE3 :: Vaid Format :: " + amt);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                t1.fail("fail :: invalid format :: " + amount);
                Assert.fail();
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest t1 = pNode.createNode("TUNG6397 ", "To verify that the transaction amount and service charge\n" +
                " values displayed in SMS based on the preference 'AMT_DISPLAY_POLICY'.");
        try {
            //   to update System Preference to RULE3
            SystemPreferenceManagement.init(t1).updateSystemPreference(PrefCode, "RULE3");

            //   O2C transaction
            Login.init(t1).login(o2cinit);
            String txnid = TransactionManagement.init(t1).initiateO2C(channeluser, amount, "1234");
            TransactionManagement.init(t1).o2cApproval1(txnid);

            // fetch the balance
            UsrBalance balnc = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            String balnc1 = balnc.Balance.toString();
            System.out.println(balnc1);

            try {
                double amt = Double.parseDouble(balnc1);
                t1.pass("set to RULE3 :: Vaid Format :: " + amt);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                t1.fail("fail :: invalid format :: " + balnc1);
                Assert.fail();
            }

            //  check Channel user in the SMS notification
            TxnResponse res = Transactions.init(t1).BalanceEnquiry(channeluser);
            res.verifyStatus(Constants.TXN_SUCCESS);
            String amount = SMSReader.init(t1).fetchAmountFromNotification(channeluser.MSISDN, "balance.enq");
            try {
                int amt = Integer.parseInt(amount);
                t1.pass("set to RULE3 :: Vaid Format :: " + amt);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                t1.fail("fail :: invalid format :: " + amount);
                Assert.fail();
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() {

        ExtentTest tearDown = pNode.createNode("Teardown", "Re-Setting preference");
        try {
            SystemPreferenceManagement.init(tearDown).updateSystemPreference(PrefCode, DefaultValue);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}