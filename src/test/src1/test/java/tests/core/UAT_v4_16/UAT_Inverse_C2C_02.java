package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.jigsaw.JigsawOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Inverse_C2C_02
 * Author Name      : Pushpalatha Pattabiraman,Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_Inverse_C2C_02 extends TestInit {

    private User whsSender, whsReceiver;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");

        try {
            whsSender = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            whsReceiver = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(setup).createChannelUserDefaultMapping(whsReceiver, false);
            TransactionManagement.init(setup).initiateAndApproveO2C(whsReceiver, "10", "Test");

            ServiceCharge inversec2c = new ServiceCharge(Services.INVERSEC2C, whsSender, whsReceiver,
                    null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(inversec2c);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void UAT_Inverse_C2C_TUNG51437() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51437",
                "To verify that Inverse C2C service is not successful if Wrong PIN is entered\n" +
                        " 3 time and should be barred due to Invalid PIN attempts.");

        try {

            //Initiate Inverse C2C
            Login.init(t1).login(whsSender);
            String txnid1 = TransactionManagement.init(t1).inverseC2C(whsReceiver, "5");

            //Confirm Inverse C2C
            String reqID = MobiquityGUIQueries.dbGetServiceRequestId(txnid1);
            SfmResponse resopnse;

            try {
                JigsawOperations.setUseServiceInBody(false);

                Transactions.init(t1)
                        .inversec2cConfirmationChangingMPINTPIN(whsReceiver, reqID, "12ft", "1@3$")
                        .assertMessage("invc2c.label.transfer.confirm.fail.mpin");

                Transactions.init(t1)
                        .inversec2cConfirmationChangingMPINTPIN(whsReceiver, reqID, "13578", "13.58")
                        .assertMessage("invc2c.label.transfer.confirm.fail.mpin");

                Transactions.init(t1)
                        .inversec2cConfirmationChangingMPINTPIN(whsReceiver, reqID, "13.57", "!@#$")
                        .assertMessage("invc2c.label.transfer.confirm.fail.mpin");

                Transactions.init(t1)
                        .inversec2cConfirmationChangingMPINTPIN(whsReceiver, reqID, "13.57", "!@#$")
                        .assertMessage("invc2c.label.transfer.fail.nxtattempt.block");

                Transactions.init(t1)
                        .inversec2cConfirmationChangingMPINTPIN(whsReceiver, reqID, "13.57", "!@#$")
                        .assertMessage("invc2c.label.transfer.fail.block");

            } finally {
                JigsawOperations.setUseServiceInBody(true);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
