package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.CMDExecutor;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_BankCashOut_01
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_BankCashOut_01 extends TestInit {

    @BeforeClass(enabled = false)
    public void precondition() throws Exception {


        Markup m = MarkupHelper.createLabel("Pre-Condition,Setting parameter and restarting server", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            String cmdText1 = "sed -i 's/TRANSACTION_TIME_OUT=525600000/TRANSACTION_TIME_OUT=1/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText1);
            String cmdText2 = "sed -i 's/TIMEOUT_TRANSACTION_AVERAGE_TIME=1118880000/TIMEOUT_TRANSACTION_AVERAGE_TIME=1/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText2);
            String cmdText3 = "sed -i 's/FAIL_SECURE_TRANSACTION_CLEANUP_TIME=52560000/FAIL_SECURE_TRANSACTION_CLEANUP_TIME=1/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText3);
            String cmdText4 = "sed -i 's/THIRD_PARTY_TRANSACTION_TIME_OUT=10/THIRD_PARTY_TRANSACTION_TIME_OUT=1/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText4);
            CMDExecutor.performServerRestart(pNode);

            Thread.sleep(8000);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(enabled = false)
    public void UAT_Bank_Cash_Out_TUNG51504() throws Exception, MoneyException {
        ExtentTest t1 = pNode.createNode("TUNG51504", "To verify that Cash out to Bank Account\n" +
                " request should get time out if txn is not confirmed within configurable time when channel user\n" +
                " and customer both are belongs to same MFS provider.");

        String bankId = DataFactory.getBankId(DataFactory.getDefaultBankNameForDefaultProvider());

        //create a wholesaler with bank
        User channeluser = new User(Constants.WHOLESALER);
        ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, true);


        //Make sure channel user has enough balance
        TransactionManagement.init(t1).makeSureChannelUserHasBalance(channeluser, new BigDecimal(10));

        //create a subscriber with bank
        User subs = new User(Constants.SUBSCRIBER);
        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, true);

        //Make sure subscriber has enough balance
        TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal(10));

        //define service charge and transfer rule
        ServiceCharge bankcash = new ServiceCharge(Services.BANK_CASH_OUT, subs, channeluser, null, null, null, null);
        ServiceChargeManagement.init(t1).configureServiceCharge(bankcash);
        TransferRuleManagement.init(t1).configureTransferRule(bankcash);

        //fetch the bank account no for channel user
        Map<String, String> AccountNum = MobiquityGUIQueries.dbGetAccountDetails(channeluser, DataFactory.getDefaultProvider().ProviderId, bankId);
        String channaccount = AccountNum.get("ACCOUNT_NO");

        //decrypt the account no.
        DesEncryptor d = new DesEncryptor();
        String decchanacc = d.decrypt(channaccount);
        //System.out.println(decchanacc);

        //fetch the bank account no for subscriber
        Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(subs, DataFactory.getDefaultProvider().ProviderId, bankId);
        String subaccount = AccountNum1.get("ACCOUNT_NO");

        //decrypt the account no.
        DesEncryptor d1 = new DesEncryptor();
        String decsubacc = d1.decrypt(subaccount);
        System.out.println(decsubacc);

        //Initiate Bank cash out
        String txnid = Transactions.init(t1).bankCashOutInitiate(subs, channeluser, decchanacc, decsubacc, 10);

        //wait for 4 minutes
        Thread.sleep(240000);

        //Confirm the initiated request
        String msg = Transactions.init(t1).startNegativeTest().bankCashOutConfirmation(subs, txnid);

        Assertion.verifyMessageContain(msg, "", "Transaction Time out", t1);
    }

    @AfterClass(enabled = false)
    public void postcondition() throws Exception {

        Markup m1 = MarkupHelper.createLabel("Post-Condition,Resetting parameter and restarting server", ExtentColor.BLUE);
        pNode.info(m1); // Method Start Marker

        try {

            String cmdText1 = "sed -i 's/TRANSACTION_TIME_OUT=1/TRANSACTION_TIME_OUT=525600000/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText1);
            String cmdText2 = "sed -i 's/TIMEOUT_TRANSACTION_AVERAGE_TIME=1/TIMEOUT_TRANSACTION_AVERAGE_TIME=1118880000/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText2);
            String cmdText3 = "sed -i 's/FAIL_SECURE_TRANSACTION_CLEANUP_TIME=1/FAIL_SECURE_TRANSACTION_CLEANUP_TIME=52560000/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText3);
            String cmdText4 = "sed -i 's/THIRD_PARTY_TRANSACTION_TIME_OUT=1/THIRD_PARTY_TRANSACTION_TIME_OUT=10/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText4);
            CMDExecutor.performServerRestart(pNode);

            Thread.sleep(8000);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }
}

