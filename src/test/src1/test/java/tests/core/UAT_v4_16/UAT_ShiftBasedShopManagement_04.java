package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.Employee;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_ShiftBasedShopManagement_04
 * Author Name      : Pushpalatha Pattabiraman, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_ShiftBasedShopManagement_04 extends TestInit {

    private User retailer;
    private OperatorUser optUsr;

    @BeforeClass(alwaysRun = true)

    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");

        try {
            retailer = new User(Constants.RETAILER);
            optUsr = new OperatorUser(Constants.NETWORK_ADMIN);

            ServiceCharge createEmp = new ServiceCharge(Services.CREATE_EMPLOYEE, retailer, optUsr,
                    null, null, null, null);

            ServiceCharge suspendEmp = new ServiceCharge(Services.SUSPEND_EMPLOYEE, retailer, optUsr, null,
                    null, null, null);

            ServiceCharge resumeEmp = new
                    ServiceCharge(Services.RESUME_EMPLOYEE, retailer, optUsr, null,
                    null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(createEmp);
            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(suspendEmp);
            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(resumeEmp);

            ChannelUserManagement.init(setup).createChannelUserDefaultMapping(retailer, false);
            TransactionManagement.init(setup).initiateAndApproveO2C(retailer, "50", "Test");
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void TUNG51555() {
        ExtentTest t1 = pNode.createNode("TUNG51555",
                "Verify that non-financial service charge defined for Resume Employee service is deducted properly");
        try {

            //Get the initial balance of User
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(retailer, null, null);

            BigDecimal initialUserBalance = balance.Balance;
            t1.info("Initial balance of user is " + initialUserBalance);

            //Create an employee under the channel user
            Employee emp = new Employee(retailer.MSISDN, retailer.CategoryCode);
            Transactions.init(t1).createEmployee(emp);

            //Suspend Employee
            Transactions.init(t1).suspendEmployee(emp);

            //Resume Employee
            Transactions.init(t1).resumeEmployee(emp);

            //Get current balance of User
            balance = MobiquityGUIQueries.getUserBalance(retailer, null, null);
            BigDecimal currentUserBalance = balance.Balance;
            t1.info("Current balance of user is " + currentUserBalance);

            //Check service charge is deducted
            BigDecimal amount = initialUserBalance.subtract(currentUserBalance);
            Assertion.verifyAccountIsDebited(initialUserBalance, currentUserBalance, amount, "Service Charge Deducted", t1);

//            if (currentUserBalance < initialUserBalance) {
//                float amount = (float) (initialUserBalance - currentUserBalance);
//
//                Assertion.verifyNotEqual("Initial user Balance: " + initialUserBalance, "Current User Balance: " + currentUserBalance,
//                        "Service charge of amount: " + amount + " deducted properly", t1);
//
//            } else {
//                t1.fail("Service charge is not deducted properly");
//            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
