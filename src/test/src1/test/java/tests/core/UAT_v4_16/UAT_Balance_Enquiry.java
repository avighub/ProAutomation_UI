//package tests.core.UAT_v4_16;
//
//import com.aventstack.extentreports.ExtentTest;
//import com.comviva.common.DesEncryptor;
//import com.comviva.mmoney.exception.MoneyException;
//import framework.entity.OperatorUser;
//import framework.entity.ServiceCharge;
//import framework.entity.User;
//import framework.features.apiManagement.Transactions;
//import framework.features.systemManagement.ServiceChargeManagement;
//import framework.features.transactionManagement.TransactionManagement;
//import framework.features.userManagement.ChannelUserManagement;
//import framework.util.common.Assertion;
//import framework.util.common.DataFactory;
//import framework.util.dbManagement.MobiquityGUIQueries;
//import framework.util.globalConstant.Constants;
//import framework.util.globalConstant.Services;
//import framework.util.globalVars.ConfigInput;
//import org.testng.annotations.Test;
//import tests.core.base.TestInit;
//
//import java.util.Map;
//
///***
// * Company Name     : Comviva Technologies Ltd.
// * Application Name : Mobiquity 4.16.0
// * Objective        : UAT_Balance_Enquiry
// * Author Name      : Pushpalatha Pattabiraman
// * Created Date     : 02/02/2018
// */
//
//public class UAT_Balance_Enquiry extends TestInit {
//
//    @Test(priority = 1)//required multiple MFS providers
//    public void UAT_Balance_Enquiry_TUNG51482() throws Exception, MoneyException {
//        ExtentTest t1 = pNode.createNode("TUNG51482", "To verify that Channel user registered\n" +
//                " with multiple MFS provider & associated with Bank account cannot check Bank Account balance\n" +
//                " for particular MFS provider if Channel user is Barred in the entered MFS provider.");
//        if (ConfigInput.isSecondryProvider) {
//            try {
//                String bankIdProvider1 = DataFactory.getBankId(DataFactory.getDefaultBankNameForDefaultProvider());
//                String bankIdProvider2 = DataFactory.getBankId(DataFactory.getNonDefaultProvider().getDefaultBankName());
//
//                User whs = new User(Constants.WHOLESALER);
//                ChannelUserManagement.init(t1).createChannelUser(whs, true);
//                //whs.writeDataToExcel();
//
//                TransactionManagement.init(t1).initiateAndApproveO2C(whs, Constants.MIN_O2C_AMOUNT, "Test");
//
//                //User whs=DataFactory.getUserUsingMsisdn("7792046234");
//
//                //fetch the bank account no for MFS1
//                Map<String, String> AccountNum = MobiquityGUIQueries.dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, bankIdProvider1);
//                String channaccountMfs1 = AccountNum.get("ACCOUNT_NO");
//                DesEncryptor d = new DesEncryptor();
//                String accNo1 = d.decrypt(channaccountMfs1);
//                t1.info(accNo1);
//
//                //fetch the bank account no for MFS2
//                Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(whs, DataFactory.getNonDefaultProvider().ProviderId, bankIdProvider2);
//                String channaccountMfs2 = AccountNum1.get("ACCOUNT_NO");
//                //DesEncryptor d1 = new DesEncryptor();
//                String accNo2 = d.decrypt(channaccountMfs2);
//                t1.info(accNo2);
//
//                //ChannelUserManagement.init(t1).barChannelUser(whs,Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_SENDER);
//
//                //  service charges for non-Financial Transactions
//                OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
//                ServiceCharge balanc = new ServiceCharge(Services.Bank_BalanceEnquiry, whs, opt, null, null, null, null);
//                ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balanc);
//                Transactions.init(t1).channelUsrBalanceEnquiryBANK(whs, DataFactory.getDefaultProvider().ProviderId, bankIdProvider1, accNo1);
//                Transactions.init(t1).channelUsrBalanceEnquiryBANK(whs, DataFactory.getNonDefaultProvider().ProviderId, bankIdProvider2, accNo2);
//            } catch (Exception e) {
//                markTestAsFailure(e, t1);
//            } finally {
//                Assertion.finalizeSoftAsserts();
//            }
//        } else {
//            t1.skip("2nd MFS Provider Required");
//        }
//    }
//}
