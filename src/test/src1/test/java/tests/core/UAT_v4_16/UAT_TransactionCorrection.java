package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setTxnProperty;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To perform transaction correction
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */


public class UAT_TransactionCorrection extends TestInit {
    private String pref = null;
    private MobiquityGUIQueries m = new MobiquityGUIQueries();
    private User cashoutUser, subs;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Setup Specific for this Test");
        try {
//            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));
//            pref = m.fetchDefaultValueOfPreference("TXN_CORR_SERVICETYPE");
//
//            if (!pref.contains("CASHOUT")) {
//
//                SystemPreferenceManagement.init(setup).updateSystemPreference("TXN_CORR_SERVICETYPE", "'P2P','CASHIN','CASHOUT','C2C','INVC2C'");
//            }

            cashoutUser = DataFactory.getChannelUserWithAccess("COUT_WEB");
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge cashoutService = new ServiceCharge(Services.CASHOUT, subs, cashoutUser, null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(cashoutService);

            ServiceCharge Transcorrec = new ServiceCharge(Services.TXN_CORRECTION, cashoutUser, subs, null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(Transcorrec);

            TransactionManagement.init(setup).makeSureLeafUserHasBalance(subs);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51456", "To verify that valid  user cannot initiate a  Successful Cashout revert transaction when user click on reject button , through transaction correction  when both users belong to same MFS provider.");
        try {

            t1.info("Set txn properties : txn.reversal.allowed.services");
            setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
            setTxnProperty(of("txn.reversal.allowed.services", Services.CASHOUT));

            Login.init(t1).login(cashoutUser);

            String tid = TransactionManagement.init(t1).performCashOut(subs, Constants.MIN_CASHOUT_AMOUNT, null);

            String referencenum = new MobiquityGUIQueries().dbGetReferencenumber(tid);

            System.out.println(referencenum);

            Transactions.init(t1).cashoutApprovalBysubs(subs, referencenum);

            String txnid = TransactionCorrection.init(t1).initiateTxnCorrection(tid, true, true);

            TransactionCorrection.init(t1).rejectTxnCorrection(txnid);

            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnid),
                    Constants.TXN_STATUS_FAIL, "Check Fail Transaction In DB", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    //@AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {

        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {
            if (!pref.contains("CASHOUT")) {

                SystemPreferenceManagement.init(tearDown).updateSystemPreference("TXN_CORR_SERVICETYPE", pref);
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

