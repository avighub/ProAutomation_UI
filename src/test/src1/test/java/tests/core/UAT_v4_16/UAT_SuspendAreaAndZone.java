package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Geography;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.GeographyManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that  Network Admin can update/Suspend Zone and area
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */


public class UAT_SuspendAreaAndZone extends TestInit {
    private OperatorUser opt;
    private Geography geo1;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        try {
            opt = DataFactory.getOperatorUsersWithAccess("VIEWGRPHDOMAIN").get(0);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void geoSuspendArea() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51310", "To verify that  Network Admin can update/Suspend Zone");


        //Add zone and Area
        try {
            Login.init(t1).login(opt);
            Geography geo = new Geography();
            GeographyManagement.init(t1).addZone(geo);
            GeographyManagement.init(t1).addArea(geo);
            String zonename = geo.ZoneName;
            String areaname = geo.AreaName;

            // Create Channeluser

            User channeluser = new User(Constants.WHOLESALER);

            channeluser.setZone(zonename);
            channeluser.setGeography(areaname);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            // SuspendArea

            Login.init(t1).login(opt);
            GeographyManagement.init(t1).modifyArea(geo, "Suspended");
            Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Geographical domain is modified successfully", t1);

            // Login As Channeluser

            Login.init(t1).startNegativeTest().tryLogin(channeluser.LoginId, channeluser.Password);
            Assertion.verifyErrorMessageContain("opt.error.geonotmatch", "Geography not active", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void geoSuspendZone() throws Exception {
        ExtentTest t1 = pNode.createNode("Suspend Zone_TUNG51312", "To verify that  Network Admin can update/Suspend Zone");
        try {
            Login.init(t1).login(opt);

            //Add zone and Area

            geo1 = new Geography();
            GeographyManagement.init(t1).addZone(geo1);
            GeographyManagement.init(t1).addArea(geo1);
            String zonename = geo1.ZoneName;
            // Create ChannelAdmin

            OperatorUser channeladmin = new OperatorUser(Constants.CHANNEL_ADMIN);
            channeladmin.setGeography(zonename);
            OperatorUserManagement.init(t1).initiateOperatorUser(channeladmin).approveOperatorUser(channeladmin);

            Login.init(t1).login(opt);

            // SuspendZone

            GeographyManagement.init(t1).modifyZone(geo1, "Suspended");
            Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Geographical domain is modified successfully", t1);

            // Login As ChannelAdmin

            Login.init(t1).startNegativeTest().tryLogin(channeladmin.LoginId, channeladmin.Password);
            Assertion.verifyErrorMessageContain("opt.error.geonotmatch", "Geography not active", t1);

            // ActiveZone

//            GeographyManagement.init(t1).modifyZone(geo1, "Suspended");
//            Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Geographical domain is modified successfully", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {
            Login.init(pNode).login(opt);

            // ActivateZone

            GeographyManagement.init(tearDown).modifyZone(geo1, "Active");
            Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Geographical domain is modified successfully", tearDown);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
