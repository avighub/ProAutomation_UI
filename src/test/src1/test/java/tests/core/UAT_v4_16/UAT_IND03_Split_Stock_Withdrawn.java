package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.stockManagement.StockWithdrawal_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static framework.features.apiManagement.Transactions.getOperatorAvailableBalance;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that in the stock withdrawn from IND03 available balance should be displayed when clicked on available balance
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_IND03_Split_Stock_Withdrawn extends TestInit {


    @Test(priority = 1)
    public void Test_01() {

        ExtentTest t1 = pNode.createNode("TUNG51015", "To verify that in the stock withdrawn from IND03 available balance should be displayed when clicked on available balance");
        try {
            OperatorUser stockWithdrawUser = DataFactory.getOperatorUserWithAccess("STK_WITHDRAW");

            String amount = getOperatorAvailableBalance(DataFactory.getDefaultProvider().ProviderId, "IND03");
            System.out.println("stock amount = " + amount);

            //login as networkAdmin
            Login.init(t1).login(stockWithdrawUser);
            StockWithdrawal_page1 stockWithdrawl_page1 = new StockWithdrawal_page1(t1);
            stockWithdrawl_page1.navToStockWithdrawalPage();
            stockWithdrawl_page1.walletID_Select("IND03");

            //verify available balance should be displayed when clicked on available balance
            stockWithdrawl_page1.availableBalanceLinkCheck(amount);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
