package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that valid User can view the details of the Subscriber.
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_Subscriber_Information extends TestInit {
    private User subsInfoViewer;
    private User subscriber;
    private String provider;
    private String paymentInst;
    private String walletType;
    private UserFieldProperties fields;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        try {
            provider = DataFactory.getDefaultProvider().ProviderId;
            paymentInst = "WALLET";
            walletType = DataFactory.getDefaultWallet().WalletName;
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void TC01() {

        ExtentTest t1 = pNode.createNode("TUNG51393", "To verify that valid User can view the details of the Subscriber.");
        try {

            subsInfoViewer = DataFactory.getChannelUserWithAccess("SUBS_INF");

            subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Login.init(t1).
                    login(subsInfoViewer);

            // Verifying that System should successfully display the following information:Subscriber mobile number and Self mobile number fields
            SubscriberManagement.init(t1).viewSubscriberInformation(provider, paymentInst, walletType, subscriber);
            verifyIfLabelExists1(UserFieldProperties.getField("Sub.MSISDN"), t1);
            verifyIfLabelExists1(UserFieldProperties.getField("Self.MSISDN"), t1);

            //Verify Subscriber Information (Hyperlink) and Self Information (Hyperlink)
            verifyIfHyperLinkExists1(UserFieldProperties.getField("Self.Info"), t1);
            verifyIfHyperLinkExists1(UserFieldProperties.getField("Sub.Info"), t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    //Method to verify label
    private void verifyIfLabelExists1(String labelName, ExtentTest t1) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.xpath("//label[contains(text(),'" + labelName + "')]")).isDisplayed()) {
            t1.pass("Successfully verified the Label for element - " + labelName);
        } else {
            t1.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t1);
        }
    }

    //method to verify hyper link
    private void verifyIfHyperLinkExists1(String labelName, ExtentTest t1) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.xpath("//a[contains(@href,'" + labelName + "')]")).isDisplayed()) {
            t1.pass("Successfully verified the Label for element - " + labelName);
        } else {
            t1.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t1);
        }
    }
}
