package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that  channel user is  retrieving the  account details, using  My Account Details service
 * Author Name      : Saraswathi Annamalai, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */


public class UAT_AccountDetails extends TestInit {

    private User chUsr;
    private OperatorUser optUsr;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUsr = new OperatorUser(Constants.NETWORK_ADMIN);

            ServiceCharge accDetail = new ServiceCharge(Services.ACCOUNT_DETAILS, chUsr, optUsr,
                    null, null, null, null);

            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceCharge(accDetail);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void channelUserDetails() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG4154", "To verify that channel user is  retrieving the\n" +
                " account details, using  My Account Details service");
        try {
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(chUsr, null, null);
            BigDecimal preBalance = balance.Balance;
            t1.info("Pre User Balance: " + preBalance);

            Transactions.init(t1).myAccountDetails(chUsr);

            balance = MobiquityGUIQueries.getUserBalance(chUsr, null, null);
            BigDecimal currentBalance = balance.Balance;
            t1.info("Pre User Balance: " + currentBalance);

            BigDecimal amount = preBalance.subtract(currentBalance);
            Assertion.verifyAccountIsDebited(preBalance, currentBalance, amount,
                    "Service Charge Of Amount: " + amount + "Is Debited From User's Wallet", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
