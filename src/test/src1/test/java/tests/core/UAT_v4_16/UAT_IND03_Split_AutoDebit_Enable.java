package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Proper Service charge should be credited to IND03 wallet & debited from IND03B wallet when Auto Debit Enable Service is performed.
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_IND03_Split_AutoDebit_Enable extends TestInit {
    private OperatorUser naUtilBillReg;
    private User wholeSaler, sub;
    private Biller biller;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() {

        ExtentTest setup = pNode.createNode("Setup", "Defining service charge and commission to enable autoDebit");
        try {

            wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB");
            OperatorUser OptUser1 = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            naUtilBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");

            biller = BillerManagement.init(setup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            //Make Sure that the Channel User has balance more than the threshold amount
            TransactionManagement.init(setup)
                    .makeSureChannelUserHasBalance(wholeSaler);

            //create subscriber
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            TransactionManagement.init(setup).makeSureLeafUserHasBalance(sub);

            //Defining service charge for Subscriber Biller Association
            ServiceCharge utility = new ServiceCharge(Services.UTILITY_REGISTRATION, sub, OptUser1, null, null, null, null);
            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceCharge(utility);

            //Defining service charge to enable auto debit with commission amount 0.1
            ServiceCharge autodebit = new ServiceCharge(Services.EnableAuto_Debit, sub, OptUser1, null, null, null, null);
            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceChargewithCommission(autodebit);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT})
    public void Test_01() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG50291", "To verify that Proper Service charge should be credited to IND03 wallet & debited from IND03B wallet when Auto Debit Enable Service is performed.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            //check initial balance of INDO3
            BigDecimal optPreBalanceIND03 = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03");
            t1.info("Initial system balance is " + optPreBalanceIND03);

            //check initial balance of INDO3B
            BigDecimal optPreBalanceIND03B = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03B");
            t1.info("Initial system balance is " + optPreBalanceIND03B);

            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            // add above bill to the Biller object
            biller.addBillForCustomer(sub.MSISDN, billAccNumber);

            // Login as Operator user with Bill Registration Role
            Login.init(t1)
                    .login(naUtilBillReg);

            // Initiate Subscriber Biller Association
            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);

            //Login as Biller
            Login.init(t1)
                    .login(biller);

            //Enable Auto debit
            String txnId = BillerManagement.init(t1)
                    .enableAutoDebit(sub.MSISDN, billAccNumber);

            //Customer has to confirm the Auto Debit Enable via USSD
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            Transactions.init(t1)
                    .AutoDebitConfirmation_Customer(sub.MSISDN, txnId);

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            //check post balance of IND03
            BigDecimal optPostBalanceIND03 = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03");
            t1.info("Current system balance is " + optPostBalanceIND03);

            //check post balance of IND03B
            BigDecimal optPostBalanceIND03B = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03B");
            t1.info("current system balance is " + optPostBalanceIND03B);

            BigDecimal amount = optPostBalanceIND03.subtract(optPreBalanceIND03);
            Assertion.verifyAccountIsCredited(optPreBalanceIND03, optPostBalanceIND03, amount, "Service Charge Is Credited To IND03 Wallet", t1);

            amount = optPreBalanceIND03B.subtract(optPostBalanceIND03B);
            Assertion.verifyAccountIsDebited(optPreBalanceIND03B, optPostBalanceIND03B, amount, "Commission Amount Is Debited From IND03B Wallet", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {
            CustomerBill bill = biller.getAssociatedBill();
            BillerManagement.init(tearDown)
                    .deleteSubsBillerAssociation(bill);

        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
