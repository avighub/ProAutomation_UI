package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;
import static framework.features.apiManagement.Transactions.setUMSPropertiesAsync;
import static framework.util.jigsaw.CommonOperations.setTxnProperty;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Cash_In_Others_01
 * @author          : gurudatta.praharaj
 * @version         : Mobiquity 4.16.0
 */

public class UAT_Cash_In_Others_01 extends TestInit {
    private User transactor, depositor, receiver;
    private OperatorUser optUsr;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("setup", "Setup specific for this suite");
        try {
            transactor = DataFactory
                    .getChannelUserWithCategoryAndGradeCode(Constants.WHOLESALER, Constants.GOLD_WHOLESALER);

            depositor = DataFactory
                    .getChannelUserWithCategoryAndGradeCode(Constants.SUBSCRIBER, Constants.GOLD_SUBSCRIBER);

            receiver = new User(Constants.SUBSCRIBER);
            optUsr = new OperatorUser(Constants.NETWORK_ADMIN);

            TransactionManagement.init(setup).makeSureChannelUserHasBalance(transactor);
            TransactionManagement.init(setup).makeSureLeafUserHasBalance(depositor);

            ServiceCharge cashInOth = new ServiceCharge(Services.CASH_IN_OTHERS1, transactor, receiver,
                    null, null, null, null);

            TransferRuleManagement.init(setup).configureTransferRule(cashInOth);

            cashInOth = new ServiceCharge(Services.CASH_IN_OTHERS1, transactor, optUsr,
                    null, null, null, null);

            TransferRuleManagement.init(setup).configureTransferRule(cashInOth);


            Map<String, String> preferences = new LinkedHashMap();
            preferences.put("IS_REMITTANCE_WALLET_REQUIRED", "FALSE");
            preferences.put("DOMESTIC_REMIT_WALLET_PAYID", Constants.NORMAL_WALLET);

            SystemPreferenceManagement.init(setup)
                    .updateMultipleSystemPreferences(preferences);

            //set UMS Properties
            setUMSPropertiesAsync(of(
                    "allow.unknownParty.CASHINOTHR", true,
                    "unknownParty.ignore.legacyRule.CASHINOTHR", true,
                    "unknownParty.identifier.CASHINOTHR", "mobileNumber",
                    "ignoreUnknownPartyNetwork", true,
                    "disAllowUnknownPartyOutOfTheNetwork", false

            ));

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            //set TXN Properties
            setTxnProperty(of("sendPasscode.unRegistered.CASHINOTHR", "receiver,depositor"));
            setTxnProperty(of("generate.differentPasscode.CASHINOTHR", "true"));
            setTxnProperty(of("mfsTenantId", "mfsPrimaryTenant"));

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            Transactions.init(setup)
                    .initiateCashInOthers(receiver, transactor, depositor, "10");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() {
        ExtentTest test = pNode.createNode("TUNG51599",
                "To verify that receiver customer mapped Wallet should get credited with Cashed-in amount\n" +
                        " when unregistered customer is registered through retailer base USSD registration.");
        try {

            String[] payIdArr = DataFactory.getPayIdApplicableForSubs();

            CurrencyProviderMapping.init(test)
                    .mapWalletPreferencesUserRegistration(receiver, payIdArr);

            Transactions.init(test)
                    .SubscriberRegistrationByChannelUserWithKinDetails(receiver);

            UsrBalance balance = MobiquityGUIQueries
                    .getUserBalance(receiver, null, null);

            BigDecimal preReceiverBalance = balance.Balance;

            Transactions.init(test).subscriberAcquisition(receiver);

            balance = MobiquityGUIQueries
                    .getUserBalance(receiver, null, null);

            BigDecimal postReceiverBalance = balance.Balance;

            BigDecimal amount = postReceiverBalance.subtract(preReceiverBalance);

            Assertion.verifyAccountIsCredited(preReceiverBalance, postReceiverBalance, amount, "Wallet Credited With Cash in Others Amount", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }
}
