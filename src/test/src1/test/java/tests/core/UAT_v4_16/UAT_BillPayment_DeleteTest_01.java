package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that the Bill company cannot be deleted if the Net Account Balance of the Company is not zero.
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_BillPayment_DeleteTest_01 extends TestInit {
    private OperatorUser nwAdmin;
    private User subsUser, whs;
    private Biller biller;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {
        ExtentTest tSetup = pNode.createNode("Setup", "Create new Biller. " +
                "Create a SUBS for testing purpose. " +
                "Make sure that transfer rule AgentAssistedBillPayment is configured.");

        try {
            whs = DataFactory.getChannelUserWithAccess("BP_RET");
            nwAdmin = DataFactory.getOperatorUserWithAccess("UTL_CDEL");

            whs = DataFactory.getChannelUserWithAccess("BP_RET");
            nwAdmin = DataFactory.getOperatorUserWithAccess("UTL_CDEL");
            subsUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            biller = BillerManagement.init(tSetup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            //Make Sure that the Channel User has balance more than the threshold amount
            TransactionManagement.init(tSetup)
                    .makeSureChannelUserHasBalance(whs);

            //perform Cashin
            TransactionManagement.init(tSetup)
                    .makeSureLeafUserHasBalance(subsUser);

            //biller = BillerManagement.init(tSetup).getDefaultBiller();

            ServiceCharge billPayment = new ServiceCharge(Services.AgentAssistedBillPayment, whs, biller, null, null, null, null);
            TransferRuleManagement.init(tSetup)
                    .configureTransferRuleForBillPayment(billPayment);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51453", "To verify that the Bill company cannot be deleted if the Net Account Balance of the Company is not zero.");
        try {
            // add above bill to the biller object
            String billAccNumber = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subsUser.MSISDN, billAccNumber);

            // add above bill to the Biller object
            biller.addBillForCustomer(subsUser.MSISDN, billAccNumber);

            UsrBalance balance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            BigDecimal preBillerBalance = balance.Balance;


            //Perform Bill Payment
            ChannelUserManagement.init(t1).billPayment(biller, Constants.BILL_PROCESS_TYPE_OFFLINE,
                    Services.AgentAssistedBillPayment, billAccNumber, "10", subsUser);

            balance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            BigDecimal postBillerBalance = balance.Balance;

            BigDecimal amount = postBillerBalance.subtract(preBillerBalance);
            Assertion.verifyAccountIsCredited(preBillerBalance, postBillerBalance, amount, "Bill Payment Done Successfully", t1);

            BillerManagement.init(t1).startNegativeTest();
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BILLER_DELETION_INITIATION);
            BillerManagement.init(t1).initiateBillerDeletion(biller);

            //verify that the Bill company cannot be deleted
            Assertion.verifyActionMessageContain("biller.pending.balance",
                    "Biller Can't Be Deleted", t1, postBillerBalance.toString());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}