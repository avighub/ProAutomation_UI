package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Bank Management screen should display while Registering Subscriber into mobiquity system.
 *                  : To verify that Wallet Management screen should display while Registering Subscriber into mobiquity system.
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_Subscriber_Management extends TestInit {
    private UserFieldProperties fields;

    /**
     * Verifying fields on Bank management screen
     *
     * @throws Exception
     */

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51298", "To verify that Bank Management screen should\n" +
                " display while Registering Subscriber into mobiquity system.");

        try {

            //login as Channel user
            User Wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            Login.init(t1).login(Wholesaler);

            //creating subscriber
            User sub = new User(Constants.SUBSCRIBER);

            //Add all mandatory field on Subscriber information(page 1) and click on submit button.
            SubscriberManagement.init(t1).addInitiateSubscriber(sub);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(sub)
                    .mapWalletPreferences(sub);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            /**
             * To verify that Bank Management screen should display following fields
             * MFS Provider(Airtel)
             Bank
             Mobile Group Role ¿ Bank
             Grade
             TCP Profile Name
             Customer ID
             Account No
             Primary Account
             Account Type
             Unique Payment Instrument Number
             Status
             *
             */

            boolean mfsProvider = Utils.checkElementPresent("bankCounterList[0].providerSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean linkedBank = Utils.checkElementPresent("bankCounterList[0].paymentTypeSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean grade = Utils.checkElementPresent("bankCounterList[0].channelGradeSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean mobileGroupRole = Utils.checkElementPresent("bankCounterList[0].groupRoleSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean tcpProfileName = Utils.checkElementPresent("bankCounterList[0].tcpSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean customerID = Utils.checkElementPresent("bankCounterList[0].customerId", Constants.FIND_ELEMENT_BY_NAME);
            boolean accountNum = Utils.checkElementPresent("bankCounterList[0].accountNumber", Constants.FIND_ELEMENT_BY_NAME);
            boolean primaryAccount = Utils.checkElementPresent("bankCounterList[0].primaryAccountSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean accountType = Utils.checkElementPresent("bankCounterList[0].accountTypeSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean status = Utils.checkElementPresent("bankCounterList[0].statusSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean delete = Utils.checkElementPresent("delete0", Constants.FIND_ELEMENT_BY_ID);

            Assertion.verifyEqual(mfsProvider, true, "MFS Provider Dropdown Is Available", t1);
            Assertion.verifyEqual(linkedBank, true, "Linked Banks Dropdown Is Available", t1);
            Assertion.verifyEqual(grade, true, "Available Grades Dropdown Is Available", t1);
            Assertion.verifyEqual(mobileGroupRole, true, "Mobile Group roles Dropdown Is Available", t1);
            Assertion.verifyEqual(tcpProfileName, true, "TCP Profile Name Dropdown Is Available", t1);
            Assertion.verifyEqual(customerID, true, "Customer ID Text Field Is Available", t1);
            Assertion.verifyEqual(accountNum, true, "Bank Account Number Test Field Is Available", t1);
            Assertion.verifyEqual(primaryAccount, true, "Primary Account Dropdown Is Available", t1);
            Assertion.verifyEqual(accountType, true, "Account Type Dropdown Is Available", t1);
            Assertion.verifyEqual(status, true, "Status Dropdown Is Available", t1);
            Assertion.verifyEqual(delete, true, "Delete Button Is Available", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * Verifying fields on Wallet management screen
     *
     * @throws Exception
     */

    @Test(priority = 2)
    public void Test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51296", "To verify that Wallet Management screen should display while Registering Subscriber into mobiquity system.");
        try {

            //login as Channel user
            User Wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            Login.init(t1).login(Wholesaler);

            //creating subscriber
            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).addInitiateSubscriber(sub);
            CommonUserManagement.init(t1).assignWebGroupRole(sub);

            /**
             * To verify that Wallet Management screen should display following fields
             * MFS Provider(Airtel)
             Bank
             Mobile Group Role Bank
             Grade
             TCP Profile Name
             Customer ID
             Account No
             Primary Account
             Account Type
             Unique Payment Instrument Number
             Status
             *
             */
            boolean mfsProvider = Utils.checkElementPresent("counterList[0].providerSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean walletType = Utils.checkElementPresent("counterList[0].paymentTypeSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean grade = Utils.checkElementPresent("counterList[0].channelGradeSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean mobileGroupRole = Utils.checkElementPresent("counterList[0].groupRoleSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean tcpName = Utils.checkElementPresent("counterList[0].tcpSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean upin = Utils.checkElementPresent("counterList[0].uniquePaymentNumber", Constants.FIND_ELEMENT_BY_NAME);
            boolean primaryAcc = Utils.checkElementPresent("counterList[0].uniquePaymentNumber", Constants.FIND_ELEMENT_BY_NAME);
            boolean interestProfile = Utils.checkElementPresent("counterList[0].IntAssocProfile", Constants.FIND_ELEMENT_BY_NAME);
            boolean status = Utils.checkElementPresent("counterList[0].statusSelected", Constants.FIND_ELEMENT_BY_NAME);
            boolean delete = Utils.checkElementPresent("delete0", Constants.FIND_ELEMENT_BY_ID);

            Assertion.verifyEqual(mfsProvider, true, "MFS Provider Dropdown Is Available", t1);
            Assertion.verifyEqual(walletType, true, "Wallet Type Dropdown Is Available", t1);
            Assertion.verifyEqual(grade, true, "Grade Dropdown Is Available", t1);
            Assertion.verifyEqual(mobileGroupRole, true, "Mobile Group Role Dropdown Is Available", t1);
            Assertion.verifyEqual(tcpName, true, "TCP Dropdown Is Available", t1);
            Assertion.verifyEqual(upin, true, "UNIP Dropdown Is Available", t1);
            Assertion.verifyEqual(primaryAcc, true, "Primary Account Dropdown Is Available", t1);
            Assertion.verifyEqual(interestProfile, true, "Interest Profile Dropdown Is Available", t1);
            Assertion.verifyEqual(status, true, "Status Dropdown Is Available", t1);
            Assertion.verifyEqual(delete, true, "Delete Link Is Available", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
