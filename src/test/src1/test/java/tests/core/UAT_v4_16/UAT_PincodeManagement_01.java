package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that channel user can change his tpin if channel user barred as sender/reciever/both
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_PincodeManagement_01 extends TestInit {

    private OperatorUser channelAdmin;
    private User channeluser;

    @BeforeClass(alwaysRun = true)
    void setup_PCM() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Creating service charge for Tpin");
        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CHANGE_TPIN, new User(Constants.WHOLESALER), new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);
            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(sCharge);

            channelAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            channeluser = new User(Constants.WHOLESALER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test
    void Test_PCM_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12310",
                "To verify that channel user can change his tpin if channel user barred as sender/reciever/both");
        try {
            //Creating Channel User
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMappingWithoutChangingTpin(channeluser);
            Login.init(t1).login(channelAdmin);

            //channel user barred as sender
            ChannelUserManagement.init(t1)
                    .barChannelUser(channeluser, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_SENDER);

            //Channel User performing Transactions & verify that channel user can change his tpin if channel user barred as sender/reciever/both
            Transactions.init(t1).changeChannelUserTPin(channeluser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
