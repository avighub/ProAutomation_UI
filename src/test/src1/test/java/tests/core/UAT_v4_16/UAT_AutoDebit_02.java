package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Channel Users should be able to initiate Auto Debit when Channel User's Barred as Sender or Both..
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_AutoDebit_02 extends TestInit {

    private OperatorUser optUser1, naUtilBillReg, baruser;
    private User subsUser, channeluser;
    private Biller biller;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {

        ExtentTest tSuite = pNode.createNode("Setup", "Get Default Biller. " +
                "Get Biller User. " +
                "Configure Service charge UTILITY_REGISTRATION & EnableAuto_Debit. " +
                "Create a Bared User");
        try {
            subsUser = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(tSuite)
                    .createDefaultSubscriberUsingAPI(subsUser);

            optUser1 = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            naUtilBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");
            baruser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");

            biller = BillerManagement.init(tSuite)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            //Defining service charge for Subscriber Biller Association
            ServiceCharge utilityReg = new ServiceCharge(Services.UTILITY_REGISTRATION, subsUser, optUser1, null, null, null, null);
            ServiceCharge autoDebit = new ServiceCharge(Services.EnableAuto_Debit, subsUser, optUser1, null, null, null, null);

            ServiceChargeManagement.init(tSuite)
                    .configureNonFinancialServiceCharge(utilityReg);

            //Defining service charge to enable auto debit
            ServiceChargeManagement.init(tSuite)
                    .configureNonFinancialServiceCharge(autoDebit);

            //creating the channeluser
            channeluser = CommonUserManagement.init(tSuite)
                    .getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_BOTH, null);

            // make sure subscriber has balance
            TransactionManagement.init(tSuite)
                    .makeSureLeafUserHasBalance(subsUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void TUNG48757() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG48757",
                "To verify that Channel Users should be able to initiate Auto Debit when \n" +
                        "Channel User's Barred as Sender.");

        try {
            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            // add above bill to the Biller object
            biller.addBillForCustomer(subsUser.MSISDN, billAccNumber);

            Login.init(t1)
                    .login(naUtilBillReg);

            // Initiate Subscriber Biller Association
            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);

            //Login as ChannelUser
            Login.init(t1).login(channeluser);

            //verify that Channel Users should be able to initiate Auto Debit
            BillerManagement.init(t1)
                    .enableAutoDebitForChannelUser(subsUser.MSISDN, biller.BillerCode, billAccNumber);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        ExtentTest tearDown = pNode.createNode("Teardown", "Teardown specific to this test");
        try {
            /*
            remove subscriber from biller
            delete biller
             */
            Login.init(tearDown).login(optUser1);

            CustomerBill bill = biller.getAssociatedBill();
            BillerManagement.init(tearDown)
                    .deleteSubsBillerAssociation(bill);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
