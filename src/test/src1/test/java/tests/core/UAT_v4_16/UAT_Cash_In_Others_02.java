package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Cash_In_Others_02
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_Cash_In_Others_02 extends TestInit {

    private String unregPref, remiPref, domesPref;

    private User whs, unSubs, regSubs;
    private OperatorUser opt;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest setup = pNode.createNode("setup", "Initiating Test");
        try {
            MobiquityGUIQueries m = new MobiquityGUIQueries();

            //REGISTERINGSUB_THRESHOLD
            unregPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("UNREGSUB_PAYER_UNIQUE");

            if (!unregPref.equalsIgnoreCase("RULE0")) {

                String displayAllowedReg = MobiquityGUIQueries
                        .fetchDisplayAllowedOnGUI("UNREGSUB_PAYER_UNIQUE");

                String modifyAllowedReg = MobiquityGUIQueries
                        .fetchmodifyAllowedFromGUI("UNREGSUB_PAYER_UNIQUE");

                if (displayAllowedReg
                        .equalsIgnoreCase("N") || modifyAllowedReg
                        .equalsIgnoreCase("N")) {

                    MobiquityGUIQueries
                            .updateDisplayAndModifiedAllowedFromGUI("UNREGSUB_PAYER_UNIQUE", "Y");
                }

                SystemPreferenceManagement.init(setup)
                        .updateSystemPreference("UNREGSUB_PAYER_UNIQUE", "RULE0");
            }

            //IS_REMITTANCE_WALLET_REQUIRED and DOMESTIC_REMIT_WALLET_PAYID
            remiPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_REMITTANCE_WALLET_REQUIRED");

            if (remiPref
                    .equalsIgnoreCase("FALSE")) {
                String displayAllowedDom = MobiquityGUIQueries
                        .fetchDisplayAllowedOnGUI("DOMESTIC_REMIT_WALLET_PAYID");

                String modifyAllowedDom = MobiquityGUIQueries
                        .fetchmodifyAllowedFromGUI("DOMESTIC_REMIT_WALLET_PAYID");

                if (displayAllowedDom
                        .equalsIgnoreCase("N") || modifyAllowedDom.equalsIgnoreCase("N")) {

                    MobiquityGUIQueries
                            .updateDisplayAndModifiedAllowedFromGUI("DOMESTIC_REMIT_WALLET_PAYID", "Y");
                }

                domesPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("DOMESTIC_REMIT_WALLET_PAYID");
                String walletId = DataFactory.getDefaultWallet().WalletId;

                SystemPreferenceManagement.init(setup)
                        .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", walletId);

                opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
                whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

                //Get a registered subscriber
                regSubs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

                //Unregistered Subscriber
                unSubs = new User(Constants.SUBSCRIBER);

                ServiceCharge cashOther = new ServiceCharge(Services.CASH_IN_OTHERS1, whs, opt,
                        null, null, null, null);

                TransferRuleManagement.init(setup)
                        .configureTransferRule(cashOther);
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void UAT_Cash_In_Others_TUNG51600() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51600",
                "To verify that Unregistered customer is won't able to sent money to customer\n" +
                        " if rule 0 is set for UNREGSUB_PAYER_UNIQUE preference.");
        try {
            //Perform Cash In Others
            Transactions.init(t1).initiateCashInOthers(unSubs, whs, regSubs, "5", false)
                    .verifyMessage("cashinOthers.label.transfer.fail");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {

        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {

            SystemPreferenceManagement.init(tearDown)
                    .updateSystemPreference("UNREGSUB_PAYER_UNIQUE", unregPref);

            if (remiPref.equalsIgnoreCase("FALSE")) {

                SystemPreferenceManagement.init(tearDown)
                        .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", domesPref);
            }
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
