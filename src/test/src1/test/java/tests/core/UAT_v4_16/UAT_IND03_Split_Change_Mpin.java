package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Proper Service charge should be credited to IND03 wallet & debited from IND03B wallet when Change Mpin Service is performed.
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_IND03_Split_Change_Mpin extends TestInit {

    private User channeluser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest setup = pNode.createNode("setup", "Initiating Test");

        try {

            String defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            OperatorUser optUser = new OperatorUser(Constants.NETWORK_ADMIN);
            channeluser = new User(Constants.WHOLESALER);

            //Defining service charge to change Mpin
            ServiceCharge changeMpinSC = new ServiceCharge(Services.CHANGE_MPIN, channeluser, optUser, null, null, defaultProvider, defaultProvider);
            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceChargewithCommission(changeMpinSC);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void Test1() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG50289", "To verify that Proper Service charge should be credited to IND03 wallet & debited from IND03B wallet when Change Mpin Service is performed.");


        try {
            //check initial balance of INDO3
            BigDecimal optPreBalanceIND03 = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03");
            t1.info("Initial system balance is " + optPreBalanceIND03);

            //check initial balance of INDO3B
            BigDecimal optPreBalanceIND03B = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03B");
            t1.info("Initial system balance is " + optPreBalanceIND03B);

            //create channel User
            ChannelUserManagement.init(t1).createChannelUserDefaultMappingWithoutChangingTpin(channeluser);
            OperatorUser OptUser2 = DataFactory.getOperatorUsersWithAccess("O2C_INIT").get(0);
            Login.init(t1).login(OptUser2);

            //Perform O2C
            String txn = TransactionManagement.init(t1).initiateO2C(channeluser, "65", "5646");
            TransactionManagement.init(t1).o2cApproval1(txn);

            //Perform change Mpin service
            Transactions.init(t1)
                    .changeChannelUserMPinSecondTime(channeluser, "2468");

            //check post balance of INDO3
            BigDecimal optPostBalanceIND03 = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03");
            t1.info("Current IND03 balance is " + optPostBalanceIND03);

            //check post balance of IND03B
            BigDecimal optPostBalanceIND03B = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03B");
            t1.info("current IND03B balance is " + optPostBalanceIND03B);

            BigDecimal amount = optPostBalanceIND03.subtract(optPreBalanceIND03);
            Assertion.verifyAccountIsCredited(optPreBalanceIND03, optPostBalanceIND03, amount,
                    "Service Charge Is Credited To IND03 Wallet", t1);

            amount = optPreBalanceIND03B.subtract(optPostBalanceIND03B);
            Assertion.verifyAccountIsDebited(optPreBalanceIND03B, optPostBalanceIND03B, amount,
                    "Commission Is Debited From IND03B", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

