package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Auto-Debit should not initiate or enable when the Service Charge is not defined
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_Additional_AutoDebit extends TestInit {

    private OperatorUser OptUser1, naUtilBillReg;
    private User wholeSaler, sub;
    private Biller biller;
    private ServiceCharge autoDebit;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Setup Specific to this Script");
        /*
        create a biller with type Online and Premium
        create a subscriber and perform cash in
        create service charge for subscriber and biller association
        delete service charge for auto debit if already created
         */

        try {
            wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB");
            OptUser1 = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            naUtilBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");

            TransactionManagement.init(setup)
                    .makeSureChannelUserHasBalance(wholeSaler);

            biller = BillerManagement.init(setup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            TransactionManagement.init(setup).makeSureLeafUserHasBalance(sub);

            ServiceCharge utility = new ServiceCharge(Services.UTILITY_REGISTRATION, sub, OptUser1, null, null, null, null);

            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceCharge(utility);

            autoDebit = new ServiceCharge(Services.EnableAuto_Debit, sub, OptUser1, null, null, null, null);

            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceCharge(autoDebit)
                    .deleteNFSChargeAllVersions(autoDebit);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void TUNG15283() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG15283", "To verify that Auto-Debit should not initiate or enable when the Service Charge is not defined");
        /*
        Initiate subscriber biller association
        Login as new biller
        enable auto debit for subscriber with out creating service charge
        validate error message
         */
        try {
            naUtilBillReg = DataFactory.getOperatorUsersWithAccess("UTL_BILREG").get(0);

            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            biller.addBillForCustomer(sub.MSISDN, billAccNumber);

            Login.init(t1)
                    .login(naUtilBillReg);

            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);

            Login.init(t1)
                    .login(biller);

            BillerManagement.init(t1)
                    .startNegativeTest()
                    .enableAutoDebit(sub.MSISDN, billAccNumber);

            Assertion.verifyErrorMessageContain("autodebit.enable.error.message.ui", "No Service Charge is defined", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        ExtentTest tearDown = pNode.createNode("Teardown", "TearDown Specific to this Suite");
        /*
        redefine auto debit service charge
         */
        try {
            ServiceChargeManagement.init(tearDown)
                    .configureNonFinancialServiceCharge(autoDebit);

            CustomerBill bill = biller.getAssociatedBill();
            BillerManagement.init(tearDown).deleteSubsBillerAssociation(bill);

        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

