package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkCashInCsv;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_BulkPayoutTool_01
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_BulkPayoutTool_01 extends TestInit {

    private User whs, subs;
    private OperatorUser optUser, optUserApp;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Script");

        try {
            // Get Specific Users
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            subs = new User(Constants.SUBSCRIBER);
            optUser = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            optUserApp = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE");
            TransactionManagement.init(eSetup).makeSureChannelUserHasBalance(whs);

            ServiceCharge CashOut = new ServiceCharge(Services.CASHOUT, subs, whs, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(CashOut);

            SubscriberManagement.init(eSetup).createSubscriberDefaultMapping(subs, true, false);
            TransactionManagement.init(eSetup).makeSureLeafUserHasBalance(subs);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void UAT_Bulk_Payout_Tool_TUNG51356() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51356",
                "To verify that system should be able to perform the CashIN service through\n" +
                        " Bulk payout tool when both is associated with multiple wallet/Bank/MFS");

        try {
            BigDecimal txnAmount = new BigDecimal(10);

            //get channel user pre balance
            UsrBalance preChannelUsrBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            t1.info("Pre Chanel User Balance: " + preChannelUsrBalance.Balance);

            //get subscriber pre balance
            UsrBalance preSubscriberBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            t1.info("Pre Subscriber Balance: " + preSubscriberBalance.Balance);

            // Download and update the Csv File with Single entry
            List<BulkCashInCsv> entBillPayList = new ArrayList<>();
            entBillPayList.add(new BulkCashInCsv("1", GlobalData.defaultProvider.ProviderId, GlobalData.defaultWallet.WalletId,
                    whs.MSISDN, GlobalData.defaultWallet.WalletId, subs.MSISDN,
                    txnAmount.toString(), "Remark Ok"));

            String fileName = BulkPayoutTool.init(t1)
                    .downloadAndUpdateBulkCashInCsv(entBillPayList);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, fileName);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, batchId, true);

            Utils.putThreadSleep(5000);

            //get channel user post balance
            UsrBalance postChannelUsrBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            t1.info("Post Chanel User Balance: " + postChannelUsrBalance.Balance);

            //get subscriber post balance
            UsrBalance postSubscriberBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            t1.info("Post Subscriber Balance: " + postSubscriberBalance.Balance);

            Assertion.verifyAccountIsDebited(preChannelUsrBalance.Balance, postChannelUsrBalance.Balance, txnAmount, "Verify that Channel User Balance is Debited", t1);
            Assertion.verifyAccountIsCredited(preSubscriberBalance.Balance, postSubscriberBalance.Balance, txnAmount, "Verify that Subscriber User Balance is Credited", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.MONEY_SMOKE})
    public void UAT_Bulk_Payout_Tool_TUNG51358() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51358", "To verify that system should be able\n" +
                " to perform the service >> Cash Out (1 step) through Bulk payout tool.").assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            //perform o2c transaction
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs, new BigDecimal(110));

            //get channel user pre balance
            UsrBalance preChannelUsrBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            BigDecimal preChUsrBal = preChannelUsrBalance.Balance;

            //create a subscriber
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

            //perform cash in
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal(10));

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            //get subscriber pre balance
            UsrBalance preSubscriberBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            BigDecimal preSubsBal = preSubscriberBalance.Balance;

            //Generate the csv file
            String fileName = BulkPayoutTool.init(t1).generateNewBulkCashinCsvFile(subs.MSISDN, whs.MSISDN);

            //Login with user who has bulk payout initiate access
            Login.init(t1).login(optUser);

            //Initiate bulk payout for Cash in
            String batchId = BulkPayoutTool.init(t1).initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHOUT, fileName);

            //Login with user who has bulk payout approve access
            Login.init(t1).login(optUserApp);

            //Approve bulk payout for Cash in
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHOUT, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_CASHOUT, batchId, true, true);

            //get channel user post balance
            UsrBalance postChannelUsrBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            BigDecimal postChUsrBal = postChannelUsrBalance.Balance;

            //get subscriber post balance
            UsrBalance postSubscriberBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            BigDecimal postSubsBal = postSubscriberBalance.Balance;

            Assertion.verifyAccountIsCredited(preChUsrBal, postChUsrBal, new BigDecimal(50), "Balance Credited", t1);

            Assertion.verifyAccountIsDebited(preSubsBal, postSubsBal, new BigDecimal(50), "Balance Debited", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
