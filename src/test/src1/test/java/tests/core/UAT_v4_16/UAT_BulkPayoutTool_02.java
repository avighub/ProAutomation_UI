package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that system should be able to perform the P2P Transfer service through Bulk payout tool.
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_BulkPayoutTool_02 extends TestInit {

    private User sub1, sub2;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Pre-Condition,Creating Subscriber");
        Markup m1 = MarkupHelper.createLabel("Pre-Condition,Creating Subscriber's", ExtentColor.BLUE);
        pNode.info(m1); // Method Start Marker
        try {
            User wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB");

            //Make Sure that the Channel User has balance more than the threshold amount
            TransactionManagement.init(setup)
                    .makeSureChannelUserHasBalance(wholeSaler);

            //Sender subscriber is registered through Web Registration by channel user
            //sub1 = new User(Constants.SUBSCRIBER);
            sub1 = CommonUserManagement.init(setup).getDefaultSubscriber(null);

            Login.init(setup).login(wholeSaler);
            TransactionManagement.init(setup)
                    .performCashIn(sub1, "150", null);

            //Reciever subscriber is registered through USSD by channel user.
            sub2 = new User(Constants.SUBSCRIBER);
            Transactions.init(setup).SubscriberRegistrationByChannelUserWithKinDetails(sub2);
            Transactions.init(setup).subscriberAcquisition(sub2);

            ServiceCharge p2p = new ServiceCharge(Services.P2P, sub1, sub2,
                    null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRuleForBillPayment(p2p);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TUNG51357() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51357", "To verify that system should be able\n" +
                " to perform the P2P Transfer service through Bulk payout tool.").assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            //fetch channel sub balance
            BigDecimal sub1prebalance = MobiquityGUIQueries.getUserBalance(sub1, null, null).Balance;
            BigDecimal sub2prebalance = MobiquityGUIQueries.getUserBalance(sub1, null, null).Balance;

            //Generate the csv file
            String fileName1 = BulkPayoutTool.init(t1).generateBulkP2PCsvFile(sub1.MSISDN, sub2.MSISDN, "10");

            //Login with user who has bulk payout initiate access

            //SuperAdmin saBulkPay = DataFactory.getSuperAdminWithAccess(Roles.NEW_BULK_PAYOUT_INITIATE);
            Login.init(t1).loginAsOperatorUserWithRole(Roles.NEW_BULK_PAYOUT_INITIATE);

            //Initiate bulk payout for Cashin
            String batchId = BulkPayoutTool.init(t1).initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, fileName1);

            //Login with user who has bulk payout approve access
            //SuperAdmin saBulkPayApp = DataFactory.getSuperAdminWithAccess("BULK_APPROVE");
            Login.init(t1).loginAsOperatorUserWithRole(Roles.NEW_BULK_PAYOUT_APPROVE);

            //Approve bulk payout for P2P
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, batchId, true, true);

            BigDecimal sub1postbalance = MobiquityGUIQueries.getUserBalance(sub2, null, null).Balance;

            BigDecimal sub2postbalance1 = MobiquityGUIQueries.getUserBalance(sub2, null, null).Balance;

            //verify Sender subscriber(Payer) wallet will be debited
            //Assertion.verifyAccountIsDebited(sub1prebalance,sub1postbalance,)
            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(sub1prebalance,
                    sub1postbalance, true, "Payer");

            //verify receiver subscriber(Payee) wallet will be credited.
            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(sub2prebalance,
                    sub2postbalance1, false, "Payee");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

    }
}
