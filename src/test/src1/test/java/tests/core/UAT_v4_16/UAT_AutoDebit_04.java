package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_AutoDebit_04
 * Author Name      : Pushpalatha Pattabiraman, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_AutoDebit_04 extends TestInit {

    private User whsUser, subUser;
    private Biller biller;
    private String providerName, billAccNum;
    private OperatorUser optUsr;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "setup specific for this Script");

        try {
            whsUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            subUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            billAccNum = DataFactory.getRandomNumberAsString(5);
            providerName = DataFactory.getDefaultProvider().ProviderName;
            optUsr = DataFactory.getOperatorUserWithAccess("UTL_BILREG");

            TransactionManagement.init(setup).makeSureLeafUserHasBalance(subUser);

            ServiceCharge utilityReg = new ServiceCharge(Services.UTILITY_REGISTRATION, subUser, new OperatorUser(Constants.NETWORK_ADMIN),
                    null, null, null, null);

            ServiceCharge autoDebitEnable = new ServiceCharge(Services.ENABLE_AUTODEBIT, subUser, new OperatorUser(Constants.NETWORK_ADMIN),
                    null, null, null, null);

            ServiceChargeManagement.init(setup).
                    configureNonFinancialServiceCharge(autoDebitEnable);

            ServiceChargeManagement.init(setup).
                    configureNonFinancialServiceCharge(utilityReg);

            biller = BillerManagement.init(setup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void TUNG9452() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG9452",
                "To Verify that customer who's auto debit enable rejected by biller, \n" +
                        " should be able to perform auto debit again successfully.");
        try {

            Login.init(t1).login(optUsr);
            biller.addBillForCustomer(subUser.MSISDN, billAccNum);

            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);

            //Enable Auto Debit Subscriber Self Request
            Transactions.init(t1).
                    autoDebitSubsSelfReq(biller.BillerCode, subUser.MSISDN, billAccNum, "5");

            //Login as biller
            Login.init(t1).
                    login(biller);

            //Reject Auto Debit Enable request subUser
            BillerManagement.init(t1).startNegativeTest().
                    approveorRejectAutoDebitEnableByBiller(subUser, false, "Improper Data");

            Assertion.verifyActionMessageContain("enableautodebit.label.reject.confirm",
                    "Auto Debit Enable Approved", t1, subUser.MSISDN);

            //Enable Auto Debit for the rejected subscriber
            Transactions.init(t1).
                    autoDebitSubsSelfReq(biller.BillerCode, subUser.MSISDN, billAccNum, "5");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("teardown", "teardown specific to this Test");
        try {
            CustomerBill bill = biller.getAssociatedBill();

            BillerManagement.init(tearDown)
                    .deleteSubsBillerAssociation(bill);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

