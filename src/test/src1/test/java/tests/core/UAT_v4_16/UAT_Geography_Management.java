package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Geography;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.GeographyManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Geography_Management
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_Geography_Management extends TestInit {

    private OperatorUser opt;
    private Geography geo;

    @Test(priority = 1)
    public void UAT_Geography_Management_TUNG51308() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51308", "To verify that Network Admin can add Zone.\n" +
                " Also verify that a single area cannot be assosciated with multiple zones");
        try {
            //Add a new Zone
            opt = DataFactory.getOperatorUserWithAccess("VIEWGRPHDOMAIN");

            Login.init(t1).login(opt);

            geo = new Geography();

            GeographyManagement.init(t1).addZone(geo);

            //Fetch the newly added zone name
            String zonename = geo.ZoneName;

            //Create a Channel Admin and check whether the newly added zone is displayed
            OperatorUser opt1 = DataFactory.getOperatorUsersWithAccess("PTY_ASU").get(0);

            OperatorUser channelAdmin = new OperatorUser(Constants.CHANNEL_ADMIN);

            Login.init(t1).login(opt1);

            OperatorUserManagement.init(t1).checkForAddedZone(channelAdmin, zonename);

            Login.init(t1).performLogout();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {
            Login.init(tearDown).login(opt);
            GeographyManagement.init(tearDown).deleteZone(geo);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
