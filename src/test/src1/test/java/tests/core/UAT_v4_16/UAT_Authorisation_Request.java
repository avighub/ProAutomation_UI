package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Authorisation_Request
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_Authorisation_Request extends TestInit {

    private User subsSender, subsReceiver;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");

        try {
            subsSender = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            subsReceiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

            TransactionManagement.init(setup).makeSureLeafUserHasBalance(subsSender);

            ServiceCharge p2pService = new ServiceCharge(Services.P2P, subsSender, subsReceiver,
                    null, null, null, null);

            TransferRuleManagement.init(setup).configureTransferRule(p2pService);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 1, enabled = false) //AUTHREQ Request For Cash In/Cash Out Will No Longer Work On Old TXN
    public void UAT_Authorisation_Request_TUNG51476() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51476", "Authorisation request (Cash-In through USSD):\n" +
                " To verify that channel user is able to perform authorisation request for Cash In service through USSD.");
        if (!ConfigInput.isCoreRelease) {
            try {
                User retailer = DataFactory.getChannelUserWithCategory(Constants.RETAILER);

                User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

                //Get initial balance of retailer and subscriber
                UsrBalance cbal = MobiquityGUIQueries.getUserBalance(retailer, null, null);
                UsrBalance sbal = MobiquityGUIQueries.getUserBalance(subs, null, null);
                double cinitialBal = cbal.Balance.doubleValue();
                double sinitialBal = sbal.Balance.doubleValue();
                t1.info("Initial balance of retailer is : " + cinitialBal + " and " +
                        "Initial balance of subscriber is : " + sinitialBal);

                TransactionManagement.init(t1).makeSureChannelUserHasBalance(retailer);

                //Configure Transfer Rule for P2P
                ServiceCharge ser = new ServiceCharge(Services.CASHIN, retailer, subs,
                        null, null, null, null);

                TransferRuleManagement.init(t1).configureTransferRule(ser);

                //Perform Authorization request
                Transactions.init(t1).authorizationRequest(retailer, subs, "CI", "6");

                //Get balance of retailer and subscriber after authorization request
                UsrBalance cbal1 = MobiquityGUIQueries.getUserBalance(retailer, null, null);
                UsrBalance sbal1 = MobiquityGUIQueries.getUserBalance(subs, null, null);
                double cCurrentBal = cbal1.Balance.doubleValue();
                double sCurrentBal = sbal1.Balance.doubleValue();

                t1.info("Current balance of retailer is : " + cCurrentBal + " and " +
                        "Current balance of subscriber is : " + sCurrentBal);

                //Check channel users and subscribers wallets are not debited and credited respectively
                Assert.assertEquals(cinitialBal, cCurrentBal);
                t1.info("There is no debit in Channel user's wallet");

                Assert.assertEquals(sinitialBal, sCurrentBal);
                t1.info("There is no credit in Subscriber's wallet");
            } catch (Exception e) {
                markTestAsFailure(e, t1);
            } finally {
                Assertion.finalizeSoftAsserts();
            }
        } else {
            t1.info("Authorisation request For Cash In/Cash Out Will No Longer Work On Old TXN(Application Issue)");
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void UAT_Authorisation_Request_TUNG51477() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51477", "Authorisation request for P2P transfer (SMT)\n" +
                " through USSD :\n" +
                "To verify that subscriber is able to perform authorisation request for P2P service through USSD.");

        try {
            UsrBalance subSenderBal = MobiquityGUIQueries.getUserBalance(subsSender, null, null);
            UsrBalance subReceiverBal = MobiquityGUIQueries.getUserBalance(subsReceiver, null, null);

            String senderInitialBalance = subSenderBal.Balance.toString();
            String receiverInitialBalance = subReceiverBal.Balance.toString();

            //Perform Authorization request
            Transactions.init(t1).authorizationRequest(subsSender, subsReceiver, "P2P", "6");

            subSenderBal = MobiquityGUIQueries.getUserBalance(subsSender, null, null);
            subReceiverBal = MobiquityGUIQueries.getUserBalance(subsReceiver, null, null);

            String senderCurrentBalance = subSenderBal.Balance.toString();
            String receiverCurrentBalance = subReceiverBal.Balance.toString();

            Assertion.verifyEqual(senderInitialBalance, senderCurrentBalance,
                    "There is no debit in sender Subscriber's wallet", t1);

            Assertion.verifyEqual(receiverInitialBalance, receiverCurrentBalance,
                    "There is no credit in receiver Subscriber's wallet", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
