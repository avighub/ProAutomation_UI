package tests.core.base;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.CustomerTCP;
import framework.entity.InstrumentTCP;
import framework.entity.MobileGroupRole;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.TCPManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.groupRole.GroupRole_Page1;
import framework.pageObjects.groupRole.GroupRole_Page2;
import framework.pageObjects.tcp.AddCustomerTCP_pg1;
import framework.pageObjects.tcp.AddInstrumentTCP_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.GlobalData;
import org.apache.commons.collections.ListUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CreateBaseSetUsersForGenericExecution extends TestInit {

    @DataProvider(name = "baseSetInput")
    public static Object[][] getTestData() throws Exception {
        String filePath = FilePath.fileBaseSetInput;
        return ExcelUtil.getTableArray(filePath, 0);
    }

    @Test(priority = 1, dataProvider = "baseSetInput")
    public void createBaseSetUsers(String categoryCode, String gradeCode, String bankNames, String regType, String numOfUser, String include) throws Exception, MoneyException {
        if (include.equalsIgnoreCase("yes")) {
            ExtentTest t1 = pNode.createNode("CREATE_MOBILE_ROLES_" + gradeCode,
                    "Create Mobile Roles for the Grade '" + gradeCode + "' Category '" + categoryCode + "'");

            String domainName = DataFactory.getDomainName(categoryCode);
            String categoryName = DataFactory.getCategoryName(categoryCode);
            String gradeName = DataFactory.getGradeName(gradeCode);

            // get Bank List from ConfigInput.xlsx
            List<String> bankList = DataFactory.getAllBankNamesForMobileRoleCreation();

            // get Available list of mobile roles from DB
            List<MobileGroupRole> dbMoileRoles = MobiquityGUIQueries.fetchAvailableMobileGroupRoles();

            // if any additional bank is provided from the Excel Input file, add that to the bank list
            if (!bankNames.equalsIgnoreCase("")) {
                List<String> bankFromInputFile = Arrays.asList(bankNames.split("\\s*,\\s*"));
                bankList = ListUtils.union(bankList, bankFromInputFile);
            }

            // get the List of expected Instrument TCPs in the system,
            // as both mobile role and inst TCP has similar combination,
            // we are using same list for both mobile role and TCP creation
            List<InstrumentTCP> lInstTcp = DataFactory.getListOfRequiredInstrumentTCPs(
                    domainName,
                    categoryCode,
                    gradeName,
                    bankList
            );

            try {

                for (InstrumentTCP instTCP : lInstTcp) {

                    MobileGroupRole tempRole = new MobileGroupRole(categoryCode, instTCP.GradeName);
                    tempRole.setProvider(instTCP.CurrencyProvider);
                    tempRole.setPayInstrument(instTCP.PaymentInstrument);
                    tempRole.setPayInstrumentType(instTCP.InstrumentType);
                    tempRole.setMobileProvider(instTCP.CurrencyProvider);
                    tempRole.addApplicableService("CHECK_ALL"); // if role need to be created, select all roles

                    ExtentTest t2 = pNode.createNode("CREATE_MOBILE_ROLE_" + tempRole.MobileProvider + "_" +
                                    tempRole.CategoryName + "_" +
                                    tempRole.GradeName + "_" +
                                    tempRole.MobilePayInstType,
                            "Create Mobile role for Given Combination");

                    // check if role combination already exist in DB
                    MobileGroupRole dbMobRole = GroupRoleManagement.init(t1).isRoleCombinationAlreadyExisting(dbMoileRoles,
                            tempRole.CategoryName,
                            tempRole.GradeName,
                            tempRole.MobileProvider,
                            tempRole.MobilePayInstType
                    );

                    if (dbMobRole == null) {
                        // create the mobile role, as it's not exist in DB
                        GroupRoleManagement.init(t2).addMobileRole(tempRole);
                        tempRole.writeDataToExcel();
                    }else if (isMobileRoleAvailableInAppData(dbMobRole.ProfileName)) {
                        // role present in AppData
                        t2.pass("Mobile Group Role exist: " + dbMobRole.ProfileName);
                    } else {
                        // role is present and not written in app Data
                        t2.pass("Mobile Group Role exist: " + dbMobRole.ProfileName);
                        dbMobRole.writeDataToExcel();
                        DataFactory.loadMobileGroupRole();
                    }
                }

                // initialize global variables for Mobile role
                DataFactory.loadMobileGroupRole();

                /*
                Test 2, create Customer TCP
                 */
                ExtentTest t2 = pNode.createNode("CREATE_CUSTOMER_TCPs_" + categoryCode,
                        "Create Customer TCP for the category '" + categoryCode + "', regtype '" + regType + "'");
                List<CustomerTCP> requiredCustomerTCPs = DataFactory.loadRequiredCustomerTCPs(
                        domainName,
                        categoryName,
                        regType
                );

                // Get Existing TCP Map from UI
                Login.init(t2).loginAsSuperAdmin("TCP_USER");
                AddCustomerTCP_pg1 nav = AddCustomerTCP_pg1.init(t2);
                nav.navAddCustomerTcp();
                Map<String, CustomerTCP> tcpMap = nav.getExistingCustomerTCPmap();

                for (CustomerTCP tcp : requiredCustomerTCPs) {
                    String tcpNameFromUI = TCPManagement.init(t2).getExistingTCPName(tcpMap, tcp);
                    if (tcpNameFromUI == null) {
                        TCPManagement.init(t2).createAndApproveCustomerTCP(tcp);
                    } else {
                        t2.pass("TCP exists: " + tcpNameFromUI);
                    }
                }

                ExtentTest t3 = pNode.createNode("CREATE_INSTRUMENT_TCPs_" + categoryCode,
                        "Create Instrument TCP for the category '" + categoryCode + "', Grade '" + gradeName + "'");

                // Get Existing TCP Map from UI
                AddInstrumentTCP_pg1 navInst = AddInstrumentTCP_pg1.init(t3);
                Login.init(t3).login(DataFactory.getOperatorUserWithAccess("TCP_INSTRMENT"));
                navInst.navAddInstrumentTcp();
                Map<String, InstrumentTCP> tcpInstMap = navInst.getExistingInstrumentTCPmap();

                for (InstrumentTCP tcp : lInstTcp) {

                    String tcpNameFromUI = TCPManagement.init(t3).getExistingInstrumentTCPName(tcpInstMap, tcp);

                    if (tcpNameFromUI == null) {
                        TCPManagement.init(t3)
                                .createAndApproveInstrumentTCP(tcp);
                        tcp.writeDataToExcel();
                        DataFactory.loadInstrumentTCP();
                    } else if (isInstrumentTCPAvailableInAppData(tcpNameFromUI)) {
                        t3.pass("TCP exists: " + tcpNameFromUI);
                    } else {
                        tcp.setProfileName(tcpNameFromUI);
                        tcp.setIsCreated();
                        tcp.setIsApproved();
                        tcp.writeDataToExcel();
                        DataFactory.loadInstrumentTCP();
                    }
                }

                ExtentTest t4 = pNode.createNode("CREATE_USER_TCPs_" + categoryCode,
                        "Create Mobile Roles for the category '" + categoryCode + "', grade '" + gradeCode + "'");
                for (int i = 0; i < Integer.parseInt(numOfUser); i++) {

                    User newUser = new User(categoryCode, gradeName);
                    newUser.addLinkedBanks(bankList);

                    if (categoryCode.equals(Constants.SUBSCRIBER)) {
                        SubscriberManagement.init(t4)
                                .createBaseSetSubscriber(newUser);
                    } else {
                        ChannelUserManagement.init(t4)
                                .createBaseSetChannelUser(newUser);
                    }

                    if (newUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
                        newUser.writeDataToExcel();

                        if (categoryCode.equals(Constants.SUBSCRIBER)) {
                            TransactionManagement.init(t4)
                                    .makeSureLeafUserHasBalance(newUser);
                        } else {
                            TransactionManagement.init(t4)
                                    .makeSureChannelUserHasBalance(newUser);
                        }
                        newUser.writeDataToExcel();
                    }
                }
            } catch (Exception e) {
                Assertion.raiseExceptionAndStop(e, t1);
            }
        }
    }

    private boolean isMobileRoleAvailableInAppData(String roleName) {
        for (MobileGroupRole role : GlobalData.mobileGroupRole) {
            if (role.ProfileName.equalsIgnoreCase(roleName)
                    && role.MobileRoleStatus.equalsIgnoreCase("ACTIVE")) {
                return true;
            }
        }
        return false;
    }

    private boolean isInstrumentTCPAvailableInAppData(String roleName) {
        for (InstrumentTCP role : GlobalData.instrumentTCPs) {
            if (role.ProfileName.equalsIgnoreCase(roleName)
                    && role.APPROVED.equalsIgnoreCase("Y")) {
                return true;
            }
        }
        return false;
    }
}
