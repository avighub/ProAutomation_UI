package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.dataEntity.TxnResponse;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.billerManagement.BillerModification_Page1;
import framework.pageObjects.billerManagement.BillerRegistrationPage1;
import framework.pageObjects.billerManagement.BillerSuspend_Page;
import framework.pageObjects.billerManagement.SubsBillerAssociation_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.BillerAttribute;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Suite_BillManagement extends TestInit {
    private Biller biller, biller1;
    private OperatorUser optBulkUtilBillReg, channelAdmin, customerCareExecutive, billerRegistor;
    private User whs, sub, barredAsReceiverSubUser, retailer;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest setupNode = pNode.createNode("setup", "Setup Specific for this Suite");
        try {
            optBulkUtilBillReg = DataFactory.getOperatorUserWithAccess(Roles.BULK_BILLER_ASSOCIATION);
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            channelAdmin = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            retailer = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            sub = DataFactory.getSubscriberUser();
            customerCareExecutive = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            billerRegistor = DataFactory.getOperatorUserWithAccess(Roles.BILLER_REGISTRATION);

            biller = BillerManagement.init(setupNode)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            //Defining service charge for Subscriber Biller Association
            ServiceCharge utility = new ServiceCharge(Services.UTILITY_REGISTRATION, sub, optBulkUtilBillReg, null, null, null, null);
            ServiceChargeManagement.init(setupNode)
                    .configureNonFinancialServiceCharge(utility);

            // Configure Non Financial Service Charge for RETAILER_BILLPAY
            ServiceCharge sCharge1 = new ServiceCharge(Services.RETAILER_BILLPAY, sub, optBulkUtilBillReg, null, null, null, null);
            ServiceChargeManagement.init(setupNode)
                    .configureNonFinancialServiceCharge(sCharge1);

            // Configure Non Financial Service Charge for DELETE_SUBSCRIBER_BILLER_ASSOCIATION
            ServiceCharge delSubsReg = new ServiceCharge(Services.DELETE_ASSOCIATION, sub, optBulkUtilBillReg, null, null, null, null);
            ServiceChargeManagement.init(setupNode).configureNonFinancialServiceCharge(delSubsReg);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0273() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0273 ",
                "Bill Upload: Bill Pay >> Bill Upload(Manual) \n" +
                        "To verify that Bill Upload is successful when valid details are provided.").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.BILL_PAYMENT);
        try {

            String accNo1 = DataFactory.getRandomNumberAsString(5);

            Login.init(t1).login(optBulkUtilBillReg);

            String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1);
            BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0645_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0645_a ",
                "To verify that User other than network admin  is not able to add Biller Category successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);


        ArrayList<Object> fields = new ArrayList<>();
        fields.add(customerCareExecutive);
        fields.add(channelAdmin);

        try {
            int i;
            for (i = 0; i < fields.size(); i++) {
                try {
                    Login.init(t1).login(fields.get(i));
                    fl.contentFrame();
                    driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
                    fl.clickLink("UTIL_ALL");
                    driver.findElement(By.id("UTIL_UTL_MCAT"));
                    t1.fail("Bill Category Management Option is available for users other than networkadmin");
                } catch (Exception e) {
                    continue;
                }
            }
            t1.pass("Bill Category Management Option is not available for users other than networkadmin");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0645_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0645_b ",
                "To verify that Wholesaler is not able to add Biller Category successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            try {
                Login.init(t1).login(whs);
                driver.findElement(By.id("UTIL_ALL"));
                t1.fail("Bill Category Management Option is available for users other than networkadmin");
            } catch (Exception e) {

                t1.pass("Bill Category Management Option is not available for users other than networkadmin");
                t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0262() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0262 ",
                "To verify that the Biller Code should be unique in system during biller creation and second biller can not use this again.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            biller1 = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);
            biller1.setBillerCode(biller.BillerCode);
            Login.init(t1).login(billerRegistor);
            startNegativeTestWithoutConfirm();
            BillerManagement.init(t1).initiateBillerRegistration(biller1);
            Assertion.verifyErrorMessageContain("utility.biller.same.billercode", "Negative Biller Code Test", t1);
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0648_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0648_a ",
                "To verify that Wholesaler  is not able to perform Biller registration successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            try {
                Login.init(t1).login(whs);
                driver.findElement(By.id("UTIL_ALL"));
                t1.fail("Biller registration Option is available for users other than networkadmin");

            } catch (Exception e) {
                t1.pass("Biller registration Option is not available for users other than networkadmin");
                t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0648_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0648_b ",
                "To verify that CCE  is not able to perform Biller registration successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            try {
                Login.init(t1).login(customerCareExecutive);
                fl.contentFrame();
                driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
                fl.clickLink("UTIL_ALL");
                driver.findElement(By.id("UTIL_UTL_CREG"));
                t1.fail("Biller registration Option is available for users other than networkadmin");
            } catch (Exception e) {

                t1.pass("Biller registration Option is not available for users other than networkadmin");
                t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0650_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0650_a ",
                "To verify that User other than netadmin is not able to delete initiate a biller successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        ArrayList<Object> fields = new ArrayList<>();
        fields.add(customerCareExecutive);
        fields.add(channelAdmin);

        try {
            int i;
            for (i = 0; i < fields.size(); i++) {
                try {
                    Login.init(t1).login(fields.get(i));
                    fl.contentFrame();
                    driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
                    fl.clickLink("UTIL_ALL");
                    driver.findElement(By.id("UTIL_UTL_CDEL"));
                    t1.fail("Biller deletion Option is available for users other than networkadmin");
                } catch (Exception e) {
                    continue;
                }
            }
            t1.pass("Biller deletion Option is not available for users other than networkadmin");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0650_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0650_b ",
                "To verify that Wholesaler  is not able to perform Biller deletion.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            try {
                Login.init(t1).login(whs);
                driver.findElement(By.id("UTIL_ALL"));
                t1.fail("Biller deletion Option is available for users other than networkadmin");

            } catch (Exception e) {
                t1.pass("Biller deletion Option is not available for users other than networkadmin");
                t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0651_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0651_a ",
                "To verify that User other than netadmin is not able to approve the delete initiation of a biller .");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        ArrayList<Object> fields = new ArrayList<>();
        fields.add(customerCareExecutive);
        fields.add(channelAdmin);

        try {
            int i;
            for (i = 0; i < fields.size(); i++) {
                try {
                    Login.init(t1).login(fields.get(i));
                    fl.contentFrame();
                    driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
                    fl.clickLink("UTIL_ALL");
                    driver.findElement(By.id("UTIL_UTL_CDAP"));
                    t1.fail("Biller deletion Approval Option is available for users other than networkadmin");
                } catch (Exception e) {
                    continue;
                }
            }
            t1.pass("Biller deletion Approval Option is not available for users other than networkadmin");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0651_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0651_b ",
                "To verify that Wholesaler  is not able to Approve Biller deletion.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            try {
                Login.init(t1).login(whs);
                driver.findElement(By.id("UTIL_ALL"));
                t1.fail("Biller deletion Approval Option is available for users other than networkadmin");

            } catch (Exception e) {
                t1.pass("Biller deletion Approval Option is not available for users other than networkadmin");
                t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0649_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0649_a ",
                "To verify that User other than netadmin is not able to modify biller .");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        ArrayList<Object> fields = new ArrayList<>();
        fields.add(customerCareExecutive);
        fields.add(channelAdmin);

        try {
            int i;
            for (i = 0; i < fields.size(); i++) {
                try {
                    Login.init(t1).login(fields.get(i));
                    fl.contentFrame();
                    driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
                    fl.clickLink("UTIL_ALL");
                    driver.findElement(By.id("UTIL_UTL_CMOD"));
                    t1.fail("Biller Modification Option is available for users other than networkadmin");
                } catch (Exception e) {
                    continue;
                }
            }
            t1.pass("Biller Modification Option is not available for users other than networkadmin");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0649_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0649_b ",
                "To verify that Wholesaler  is not able to modify biller.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            try {
                Login.init(t1).login(whs);
                driver.findElement(By.id("UTIL_ALL"));
                t1.fail("Biller Modification Option is available for users other than networkadmin");

            } catch (Exception e) {
                t1.pass("Biller Modification Option is not available for users other than networkadmin");
                t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0646_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0646_a ",
                "To verify that User other than netadmin is not able to Add biller validations successfully through web.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        ArrayList<Object> fields = new ArrayList<>();
        fields.add(customerCareExecutive);
        fields.add(channelAdmin);

        try {
            int i;
            for (i = 0; i < fields.size(); i++) {
                try {
                    Login.init(t1).login(fields.get(i));
                    fl.contentFrame();
                    driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
                    fl.clickLink("UTIL_ALL");
                    driver.findElement(By.id("UTIL_UTL_CAVAL"));
                    t1.fail("Add biller validations Option is available for users other than networkadmin");
                } catch (Exception e) {
                    continue;
                }
            }
            t1.pass("Add biller validations Option is not available for users other than networkadmin");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0646_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0646_b ",
                "To verify that Wholesaler  is not able to Add biller validations successfully through web.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            try {
                Login.init(t1).login(whs);
                driver.findElement(By.id("UTIL_ALL"));
                t1.fail("Add biller validations  Option is available for users other than networkadmin");

            } catch (Exception e) {
                t1.pass("Add biller validations  through web. Option is not available for users other than networkadmin");
                t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0267() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0267 ",
                "To verify that network admin is able to perform Bulk Biller association successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            Login.init(t1).login(optBulkUtilBillReg);
            BillerManagement.init(t1).initiateBulkBillerRegistration(biller);
            Utils.captureScreen(t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 16, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0280() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0280 ",
                "To verify that the network admin is able to De-associate subscribers with the biller successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {

            Login.init(t1).login(optBulkUtilBillReg);
            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            // add above bill to the Biller object
            biller.addBillForCustomer(sub.MSISDN, billAccNumber);
            // Login as Operator user with Bill Registration Role
            Login.init(t1)
                    .login(optBulkUtilBillReg);

            // Initiate Subscriber Biller Association
            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);

            //Deassociate the biller

            CustomerBill bill = biller.getAssociatedBill();
            BillerManagement.init(t1)
                    .deleteSubsBillerAssociation(bill);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0277() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0277 ",
                "To verify that the If subscriber is barred in the system, " +
                        "subscriber shouldn't get associated with the biller.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {

            Login.init(t1).login(optBulkUtilBillReg);

            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            User barredUser = CommonUserManagement.init(t1).
                    getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_BOTH, null);
            // add above bill to the Biller object
            biller.addBillForCustomer(barredUser.MSISDN, billAccNumber);
            // Login as Operator user with Bill Registration Role
            Login.init(t1)
                    .login(optBulkUtilBillReg);

            startNegativeTest();
            // Initiate Subscriber Biller Association
            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);

            Assertion.verifyErrorMessageContain("utility.billerassociation.subscriber.barred", "Biller Association not allowed when subscriber is barred", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0278() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0278 ",
                "To verify that the If joining fees is not paid, subscriber shouldn't get associated with the biller.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {

            User subs1 = new User(Constants.SUBSCRIBER);
            Login.init(t1).loginAsChannelUserWithRole(Roles.ADD_SUBSCRIBER);
            SubscriberManagement.init(t1).addSubscriber(subs1, false);
            Login.init(t1).loginAsChannelUserWithRole(Roles.ADD_SUBSCRIBER_APPROVAL);
            SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs1);

            String acqPaid = MobiquityGUIQueries.fetchAcquisitionPaid(subs1.MSISDN);

            if (acqPaid.equalsIgnoreCase("N")) {
                String billAccNumber = DataFactory.getRandomNumberAsString(5);
                // add above bill to the Biller object
                biller.addBillForCustomer(subs1.MSISDN, billAccNumber);
                //Login as Operator user with Bill Registration Role
                Login.init(t1).loginAsOperatorUserWithRole(Roles.SUBSCRIBER_BILLER_ASSOCIATION);

                CustomerBill bill = biller.getNonAssociatedBill();

                SubsBillerAssociation_pg1.init(t1)
                        .navAddBillerValidation()
                        .selectProvider(biller.ProviderName)
                        .setSubscriberMsisdn(bill.CustomerMsisdn) // customer for which the bill association has to be done
                        .clickOnSubmit();

                Assertion.verifyErrorMessageContain("mBanking.message.notRegister",
                        "Verify System should not able to associate  subscriber with the biller if he has not paid joining fees", t1);


            } else {
                t1.warning("Acquisition Fees has already been paid. This functionality cannot be tested.");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * CODE FIXED
     *
     * @throws Exception
     */
    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0274() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0274 ",
                "To verify that the check service level option reflecting on the system should be -  Standard, premium, adhoc and both, during biller registration.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            Login.init(t1).login(billerRegistor);

            BillerRegistrationPage1 page = new BillerRegistrationPage1(t1);

            page.navBillerRegistration();
            page.clickAddNewMerchant();
            page.selectBillerGradeName(biller.GradeName);

            List<String> opt1 = page.getAllValuesFromServiceLevelDropdown();
            opt1.remove("");

            List<String> opt2 = Arrays.asList(Constants.BILL_SERVICE_LEVEL_PREMIUM,
                    Constants.BILL_SERVICE_LEVEL_STANDARD, Constants.BILL_SERVICE_LEVEL_BOTH, Constants.BILL_SERVICE_LEVEL_ADHOC);

            Collections.sort(opt1);
            Collections.sort(opt2);

            Assertion.verifyListsAreEqual(opt1, opt2, "Verify the Service Level Values", t1);
            Utils.captureScreen(t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0276() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0276 ",
                "To verify that the operator user can't not suspend biller if biller is alread in delete initiated state.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);

        try {
            Biller biller = BillerManagement.init(t1)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_PREMIUM, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_OVER_PAYMENT);


            /*biller = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            BillerManagement.init(t1).createBiller(biller);*/

            //Initiate Biller Deletion. Login included
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BILLER_DELETION_INITIATION);
            BillerManagement.init(t1).initiateBillerDeletion(biller);

            Login.init(t1).login(optBulkUtilBillReg);


            BillerSuspend_Page page = new BillerSuspend_Page(t1);
            page.navBillerSuspension();

            try {
                page.selectBillerToSuspend(biller.BillerCode);
                t1.fail("Even After biller is delete initiated,User is available for suspension");
            } catch (Exception e) {
                t1.pass("Biller is not available for suspend, since User is in delete initiate state");
            }

            //Modify Biller

            ExtentTest p2 = pNode.createNode("TC_ECONET_0276_b ",
                    "To verify that the operator user can't not modify biller if biller is already in delete initiated state.");

            BillerModification_Page1 page1 = new BillerModification_Page1(t1);

            page1.navBillerModification();
            try {
                page1.selectBillerToModify(biller.BillerCode);
                p2.fail("Even After biller is delete initiated,User is available for modification");

            } catch (Exception e) {
                p2.pass("Biller is not available for Modification, since User is in delete initiate state");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BILLER_DELETION_APPROVAL);
            BillerManagement.init(t1).approveBillerDeletion(biller);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 21, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0284() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0284 ",
                "  To verify that when status is M while uploading Bill, Bill will be modified").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);
        try {

            String accNo1 = DataFactory.getRandomNumberAsString(5);
            String accNo2 = DataFactory.getRandomNumberAsString(6);
            System.out.println("New Bill Number" + accNo2);

            String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1, "M");
            BulkPayoutTool.init(t1).updateDetailsInCSVFile(filename1, accNo1, accNo2);
            t1.pass("Bill can be modified,if we Give status as M");


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * @throws Exception
     */
    @Test(priority = 22, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0285() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0285 ",
                "To verify that when status is D while uploading Bill, Bill will be deleted").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT);
        try {

            String accNo1 = DataFactory.getRandomNumberAsString(5);

            Biller biller = BillerManagement.init(t1).getDefaultBiller();

            String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1, "D");

            Login.init(t1).login(optBulkUtilBillReg);

            BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename1);

            startNegativeTest();

            TxnResponse response = Transactions.init(t1).viewBillBySubscriber(biller, sub, accNo1);

            response.assertStatus("00292");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 23, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0564() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0564 ",
                "To verify that Edgars will be changed from Offline Biller to Online Biller in the system.").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.BILLER_REGISTRATION);
        try {

            //Create Offline Biller
            biller1 = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);
            BillerManagement.init(t1).initAndApproveBillerRegistration(biller1);

            //Modify Biller Process type from Offline to Online
            biller1.setProcessType(Constants.BILL_PROCESS_TYPE_ONLINE);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.BILLER_MODIFICATION);

            BillerManagement.init(t1).modifyBiller(biller1);

            t1.pass("Biller Process Type can be Modified from Offline to Online");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 24, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0802() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0802 ",
                "  To verify that subscriber is able to confirm the Subscriber Biller association").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.BILL_PAYMENT);
        try {
            // Make sure a bill is already registered
            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            CustomerBill newBill = new CustomerBill(biller, sub.MSISDN, billAccNumber, "Test Bill");

            // Perform Bill association by retailer
            TxnResponse response = Transactions.init(t1)
                    .subsBillerRegistrationByRetailer(retailer, newBill, DataFactory.getDefaultWallet().WalletId)
                    .verifyStatus(Constants.TXN_SUCCESS);

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            //Approve subscriber Biller association
            Transactions.init(t1).subsBillerAssociationApproval(newBill.CustomerMsisdn, response.TxnId);

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            // Delete Subscriber Biller registration
            Transactions.init(t1)
                    .delBillerRegistrationByRetailer(retailer, newBill, DataFactory.getDefaultWallet().WalletId)
                    .assertStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */
    @Test(priority = 25, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.BILL_PAYMENT})
    public void TC_ECONET_0803() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0802 ",
                "  To verify that the subscriber is able to associate him with the biller successfully" +
                        " through using his handset.").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.BILL_PAYMENT);
        try {
            User sub = new User(Constants.SUBSCRIBER);
            Transactions transactionsObj = Transactions.init(t1);

            transactionsObj.selfRegistrationForSubscriberWithKinDetails(sub);
            transactionsObj.changeCustomerMpinTpin(sub);
            transactionsObj.initiateCashIn(sub, whs, new BigDecimal(5));
            transactionsObj.billAssociationBySubscriber(sub, biller);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}