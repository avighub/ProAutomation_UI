package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.pinManagement.ManageSelfPinResetRules_pg1;
import framework.pageObjects.preferences.SystemPreferences_Page1;
import framework.pageObjects.userManagement.AddChannelUser_pg3;
import framework.pageObjects.walletPreference.SystemPreference_page1;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.MobileRoles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Preference Management
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 4/09/2018
 */


public class Suite_PreferencesManagement extends TestInit {
    private SuperAdmin saMapWallet;
    private OperatorUser netadmin, channelAdmin;
    private User channelUser, subscriber;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {
            saMapWallet = DataFactory.getSuperAdminWithAccess("CAT_PREF");
            channelAdmin = DataFactory.getOperatorUserWithCategory("BCU");
            netadmin = DataFactory.getOperatorUserWithCategory("NWADM");
            channelUser = DataFactory.getChannelUserWithCategory("WHS");
            subscriber = DataFactory.getChannelUserWithCategory("SUBS");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_PREFERENCES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0030() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0030", "To verify that a SuperAdmin should not be  able to set the Preferences if Modify_Allowed='N' for that preference.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_PREFERENCES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);
        Login.init(t1).loginAsSuperAdmin(saMapWallet);
        String code = MobiquityGUIQueries.GuiModifyIsNotAllowed();
        t1.info("System Preference Modify_Allowed is N for " + code);
        try {
            new SystemPreferences_Page1(t1).navigateToSystemPreference();
            WebElement checkBox = DriverFactory.getDriver().findElement(By.xpath("//*[contains(@value,'" + code + "')]/ancestor::tr[1]/td[1]/input[@type='checkbox']"));
            Actions mouseOver = new Actions(driver);
            mouseOver.moveToElement(checkBox);
            if (!checkBox.isEnabled()) {
                t1.pass("System Preference is found  and its Not Editable");
            } else {
                t1.fail("System Preference is found  and its  Editable");
            }

            t1.info("System Preference", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SYSTEM_PREFERENCES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0032() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0032", "To verify that the ChannelUser and ChannelAdmin other than superadmin &  Network Admin user can't set the System Preferences and wallet preferences");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SYSTEM_PREFERENCES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);
        try {
            try {
                Login.init(t1).login(channelAdmin);
                driver.findElement(By.id("MPREF_ALL"));
                t1.fail("ChannelAdmin have rights to change Preference");
            } catch (Exception e) {
                t1.pass("ChannelAdmin Does not have rights to change Preference");
            }
            try {
                Login.init(t1).login(channelUser);
                driver.findElement(By.id("MPREF_ALL"));
                t1.fail("ChannelUser have rights to change Preference");
            } catch (Exception e) {
                t1.pass("ChannelUser Does not have rights to change Preference");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0033() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0033", "To verify that the Proper error message should get displayed on web during Group Role deletion by netadmin if any subscriber associated with that group role");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);
        try {

            Login.init(t1).login(netadmin);
            Thread.sleep(3000);
            //Checking if group role can be deleted
            SystemPreference_page1 page = new SystemPreference_page1(t1);
            page.navigateTGroupRoleManagementPage();
            GroupRoleManagement.init(t1).V5_openMobileRolePage(Constants.SUBSCRIBER, subscriber.GradeName, t1);
            page.V5_grouproleradioButton();
            page.V5_deleteButton();
            AlertHandle.acceptAlert(t1);
            Assertion.verifyErrorMessageContain("grouprole.cant.be.deleted.if.user.associated", "Group Role Cant be deleted", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0034_0035() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0035",
                "To verify that the Subscriber should not be  able to perform existing service if existing role has been removed for the same in Existing Group role");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG).assignAuthor(Author.SARASWATHI);

        try {
            ServiceCharge balEnq = new ServiceCharge(Services.BALANCE_ENQUIRY, subscriber, netadmin, null, null, null, null);
            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balEnq);
            GroupRoleManagement.init(t1)
                    .addOrRemoveSpecificMobileRole(subscriber,
                            DataFactory.getDefaultWallet().WalletName,
                            MobileRoles.WALLET_BALANCE_ENQUIRY,
                            DataFactory.getDefaultProvider().ProviderName,
                            false);

            TxnResponse response = Transactions.init(t1).subscriberBalanceEnquiry(subscriber);
            String errorMessage = response.Message;
            t1.info("Error Message : =>" + errorMessage);
            Assert.assertTrue(errorMessage.contains("Service authorization has failed, as the parties involved in the transaction do not have the requisite rights"));
            t1.info("Service authorization has failed, as the parties involved in the transaction do not have the requisite rights");

            ExtentTest t2 = pNode.createNode("TC_ECONET_0034",
                    "To verify that the Subscriber should be able to perform new service if " +
                            "new role has been assigned for the same in Existing Group role.");

            GroupRoleManagement.init(t2)
                    .addOrRemoveSpecificMobileRole(subscriber,
                            DataFactory.getDefaultWallet().WalletName,
                            MobileRoles.WALLET_BALANCE_ENQUIRY,
                            DataFactory.getDefaultProvider().ProviderName,
                            true);

            TxnResponse response1 = Transactions.init(t2).subscriberBalanceEnquiry(subscriber);
            String status = response1.TxnStatus;
            Assert.assertEquals(status, "200");
            t1.info("Balance Enquiry can be verified by subscriber");


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            ExtentTest teardown = pNode.createNode("teardown", "Teardown");
            GroupRoleManagement.init(teardown)
                    .addOrRemoveSpecificMobileRole(subscriber,
                            DataFactory.getDefaultWallet().WalletName,
                            MobileRoles.WALLET_BALANCE_ENQUIRY,
                            DataFactory.getDefaultProvider().ProviderName,
                            true);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG}, enabled = false)
    public void TC_ECONET_0601() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0601",
                "To verify that the Maximum account balance allowed can be enabled from system preference.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference("MAX_ACC_BAL_ALLOWED", "FALSE");

            Login.init(t1).login(netadmin);

            ManageSelfPinResetRules_pg1 page = new ManageSelfPinResetRules_pg1(t1);
            page.navManageSelfRestPinRules();
            page.clickOnEdit();
            try {
                WebElement element = driver.findElement(By.name("maxAccBalance"));
                t1.fail("Eventhough Preference is Set to False,Field is not becoming non Mandatory");
            } catch (Exception e) {
                t1.pass("Field is becoming non Mandatory,after setting preference to False");
            }
            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference
                    ("MAX_ACC_BAL_ALLOWED", "TRUE");
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0036() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0036",
                "To verify that the Subscriber on registration is not assigned a web role when IS_CONSUMER_PORTAL_REQUIRED to be made as false in system preference.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG).assignAuthor(Author.SARASWATHI);

        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference
                    ("IS_CONSUMER_PORTAL_REQUIRED", "FALSE");

            User subs = new User(Constants.SUBSCRIBER);

            Login.init(t1).login(channelUser);

            SubscriberManagement.init(t1).addInitiateSubscriber(subs);
            AddChannelUser_pg3 groupRole = AddChannelUser_pg3.init(pNode);

            try {
                groupRole.selectWebGroupRole(subs.WebGroupRole);
                t1.fail("Eventhough Preference is Set to False,WebGroupRoleField is Visible in Web");
            } catch (Exception e) {
                t1.pass("When Preference is Set to False,WebGroupRoleField is not Visible in Web");
            }
            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference
                    ("IS_CONSUMER_PORTAL_REQUIRED", "TRUE");
        }
    }
}
