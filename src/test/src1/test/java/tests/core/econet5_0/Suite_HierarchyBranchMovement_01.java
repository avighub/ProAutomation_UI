package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.channelUserManagement.HierarchyBranchMovement;
import framework.features.common.Login;
import framework.pageObjects.channelUserManagement.HierarchyBranchMovementPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 12/10/2018.
 */
public class Suite_HierarchyBranchMovement_01 extends TestInit {

    OperatorUser hierarchyMover;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        try {
            hierarchyMover = DataFactory.getOperatorUserWithAccess("HRY_MOV");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }


    @Test(priority = 1, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void test1() {

        ExtentTest hierarchyMovementNegativeTest = pNode.createNode("TC_ECONET_0864", "To verify the channel member Hierarchy branch movement cant update if invalid details are entered.")
                .assignCategory(FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.NAVIN);

        try {

            Login.init(hierarchyMovementNegativeTest).login(hierarchyMover);

            User invalidUser = new User(Constants.WHOLESALER);


            HierarchyBranchMovementPage page = new HierarchyBranchMovementPage(hierarchyMovementNegativeTest);

            page.navHierarchyMovement();
            page.setMsisdnforBranchMovement(invalidUser.MSISDN);
            page.submitforBranchMovement();

            Assertion.verifyErrorMessageContain("channeluser.hierarchy.noOwner", "", hierarchyMovementNegativeTest);

        } catch (Exception e) {
            e.printStackTrace();
            markTestAsFailure(e, hierarchyMovementNegativeTest);
        }
    }


    /**
     * ToDO : This case is not complete as Test case is not clear
     * Case is disabled. Once this functionality is clear ,wl automate accordingly
     */
    @Test(priority = 1, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void test2() {

        ExtentTest hierarchyMovementNegativeTest = pNode.createNode("TC_ECONET_0864_1", "To verify the channel member Hierarchy branch movement cant update if same owner name and parent name selected.")
                .assignCategory(FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.NAVIN);

        try {

            Login.init(hierarchyMovementNegativeTest).login(hierarchyMover);

            User userWithCategory = DataFactory.getChannelUserWithCategory(Constants.RETAILER);

            User owner = DataFactory.getUserUsingMsisdn(MobiquityGUIQueries.getMsisdnUsingUserId(MobiquityGUIQueries.getOwnerIDOfUser(userWithCategory.MSISDN)));
            User parent = DataFactory.getUserUsingMsisdn(MobiquityGUIQueries.getMsisdnUsingUserId(MobiquityGUIQueries.getParentIDOfUser(userWithCategory.MSISDN)));

            userWithCategory.setOwnerUser(owner);
            userWithCategory.setParentUser(parent);

            startNegativeTestWithoutConfirm();

            HierarchyBranchMovement.init(hierarchyMovementNegativeTest).
                    enterFirstPageDetailsOnHierarchyMovementPage(userWithCategory).
                    enterSecondPageDetailsOnHierarchyMovementPage(userWithCategory, owner, parent);


        } catch (Exception e) {
            e.printStackTrace();
            markTestAsFailure(e, hierarchyMovementNegativeTest);
        }
    }


    //User parentUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

    @Test(priority = 3, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0202() {

        ExtentTest hierarchyMovementNegativeTest = pNode.createNode("TC_ECONET_0202", "To verify that System is able to configure to not allow hierarchy movement if a channel user has any balance available in any of its mWallet")
                .assignCategory(FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            Login.init(hierarchyMovementNegativeTest).login(hierarchyMover);

            User userWithCategory = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);

            User owner = DataFactory.getUserUsingMsisdn(MobiquityGUIQueries.getMsisdnUsingUserId(MobiquityGUIQueries.getOwnerIDOfUser(userWithCategory.MSISDN)));
            User parent = DataFactory.getUserUsingMsisdn(MobiquityGUIQueries.getMsisdnUsingUserId(MobiquityGUIQueries.getParentIDOfUser(userWithCategory.MSISDN)));

            userWithCategory.setOwnerUser(owner);
            userWithCategory.setParentUser(parent);

            startNegativeTest();

            HierarchyBranchMovement.init(hierarchyMovementNegativeTest).
                    enterFirstPageDetailsOnHierarchyMovementPage(userWithCategory).
                    enterSecondPageDetailsOnHierarchyMovementPage(userWithCategory, owner, parent, true);

            Assertion.verifyErrorMessageContain("channeluser.hierarchy.hasBalance", "Channel User Have some balance", hierarchyMovementNegativeTest);


        } catch (Exception e) {
            e.printStackTrace();
            markTestAsFailure(e, hierarchyMovementNegativeTest);
        }
    }


}
