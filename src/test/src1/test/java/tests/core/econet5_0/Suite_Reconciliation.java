package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.Promotion.Reconciliation;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Author;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 10/4/2018.
 */
public class Suite_Reconciliation extends TestInit {


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.RECONCILIATION})
    public void Test_01() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0403", "To verify that the System user should able to view Reconciliation screen.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.RECONCILIATION);
        t1.assignAuthor(Author.PRASHANT);
        try {
            OperatorUser user = DataFactory.getOperatorUserWithAccess("MN_REC");
            String provider = DataFactory.getDefaultProvider().ProviderId;
            Login.init(t1).login(user);
            Boolean isVisible = Reconciliation.init(t1)
                    .navReconciliation()
                    .selectProvider(provider)
                    .clickSubmit().viewReconciliation();

            Assertion.verifyEqual(isVisible, true, "view Reconciliation screen", t1, true);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


}
