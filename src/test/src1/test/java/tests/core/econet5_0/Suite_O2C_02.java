package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.ownerToChannel.O2CInitiation_Page1;
import framework.pageObjects.stockManagement.Reimbursement_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.Screenshot;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 6/18/2019.
 */
public class Suite_O2C_02 extends TestInit {

    @BeforeClass
    public void setup() {

    }


    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_9001() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_9001",
                "To Verify that the Network Admin/Channel Admin should be able to perform O2C using Reference number as a Stock ID only.")
                .assignCategory(FunctionalTag.ECONET_UAT_5_0);

        boolean isPrefChanged = false;

        try {

            if (!AppConfig.isLinkStockCreateWithO2C) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "Y");
                isPrefChanged = true;
            }

            Login.init(t1).loginAsOperatorUserWithRole(Roles.STOCK_INITIATION);

            String txnId = StockManagement.init(t1).
                    initiateAndApproveNetworkStock(defaultProvider.ProviderName, defaultBank.BankName, "100");

            User payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            TransactionManagement.init(t1).initiateAndApproveO2C(payee, "100", Constants.REMARKS, txnId);

            t1.pass("Network Admin is able to perform O2C using Stock Reference ID");

            ExtentTest t2 = pNode.createNode("TC_ECONET_9001_1", "To verify that On entering any random reference Number while performing O2C then error message should be displayed");

            startNegativeTest();

            TransactionManagement.init(t1).initiateO2C(payee, "100", Constants.REMARKS, DataFactory.getRandomNumberAsString(6));

            Assertion.verifyErrorMessageContain("error.stock.txn.id.invalid", "Verify Random Reference num error in O2C", t2);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            if (isPrefChanged) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("LINK_STOCK_CREATION_WITH_O2C", "N");
            }
        }

        Assertion.finalizeSoftAsserts();
    }


    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_9002() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_9002",
                "To verify that the Remarks field in ‘O2C transfer initiation’ &  ‘Stock reimbursement initiation’ should be marked as mandatory.")
                .assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {

            Login.init(t1).loginAsOperatorUserWithRole(Roles.OWNER_TO_CHANNEL_TRANSFER_INITIATE);

            O2CInitiation_Page1 page1 = new O2CInitiation_Page1(t1);

            page1.navigateToOwner2ChannelInitiationPage();

            if (page1.getRemarksLabelWebElement().getAttribute("class").equalsIgnoreCase("required")) {
                t1.pass("Remarks Field is Mandatory in O2C initiation..").addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
                Utils.highLightElement(page1.getRemarksLabelWebElement());
                Utils.captureScreen(t1);
            } else {
                t1.fail("Remarks Field is Not Mandatory in O2C initiation.").addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
            }

            Login.init(t1).loginAsOperatorUserWithRole(Roles.STOCK_REIMBURSEMENT_INITIATION);

            Reimbursement_page1 reimbursementPage1 = new Reimbursement_page1(t1);

            reimbursementPage1.navigateToLink();

            if (reimbursementPage1.getRemarkLabelWebElement().getAttribute("class").equalsIgnoreCase("required")) {
                t1.pass("Remarks Field is Mandatory in Reimbursement initiation..");
                Utils.highLightElement(reimbursementPage1.getRemarkLabelWebElement());
                Utils.captureScreen(t1);
            } else {
                t1.fail("Remarks Field is Not Mandatory in Reimbursement initiation.").addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

}
