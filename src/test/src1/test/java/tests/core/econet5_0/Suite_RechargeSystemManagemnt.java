package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.operatorManagement.OperatorManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.operatorManagement.OperatorManagementPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_RechargeSystemManagemnt extends TestInit {
    String operatorName, defaultValue;
    private OperatorUser usrAddOperator;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        Markup m = MarkupHelper.createLabel("Setup", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            defaultValue = null;
            operatorName = "AUTOOP" + DataFactory.getRandomNumber(4);
            usrAddOperator = DataFactory.getOperatorUserWithAccess("OPPADD");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0287() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0287",
                "To verify that the Network admin is able to Add operator through operator management module.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.SARASWATHI);

        try {

            //Add Operator
            defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MULTI_OPERATOR_SUPPORT");

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", "Y");


            OperatorManagement.init(t1)
                    .addRechargeOperator(operatorName, DataFactory.getDefaultProvider().ProviderId);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0288",
                    "To verify that the Network admin is able to Modify operator through operator management module.");
            t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);


            OperatorManagement.init(t2)
                    .modifyOperator(operatorName);

            ExtentTest t3 = pNode.createNode("TC_ECONET_0291",
                    "To verify that the Network admin is able to delete operator through operator management module.");
            t3.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
            OperatorManagement.init(t3)
                    .deleteOperator(operatorName);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", defaultValue);

        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0292() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0292",
                "To verify that the netadmin should able to define minimum and maximum amount to be defined for the flexi recharge.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MULTI_OPERATOR_SUPPORT");

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", "Y");

            OperatorManagement.init(t1)
                    .addRechargeOperatorWithFlexiRechargeOption(operatorName, DataFactory.getDefaultProvider().ProviderId);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", defaultValue);

        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0289() throws Exception {
        String defaultValue = null;

        ExtentTest t1 = pNode.createNode("TC_ECONET_0289",
                "To verify that if MULTI_OPERATOR_SUPPORT Preference is set at 'N' then network admin is able to add only one operator.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {
            //Add Operator

            defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MULTI_OPERATOR_SUPPORT");

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", "N");

            //getting OperatorID
            String operatorId = MobiquityGUIQueries.getOperatorId();

            if (operatorId == null) {
                OperatorManagement.init(t1)
                        .addRechargeOperator(operatorName, DataFactory.getDefaultProvider().ProviderId);
            }

            Login.init(t1).login(usrAddOperator);
            OperatorManagementPage page1 = OperatorManagementPage.init(t1);

            page1.navigateToAddOperatorLink();
            page1.setOperatorName(operatorName);
            page1.clickAdd();

            Assertion.verifyErrorMessageContain("operator.validation.MultiOperatorSupport",
                    "Only Single Operator support is allowed", t1);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", defaultValue);

        }
    }

    /**
     * TEST : TC_ECONET_0290
     * DESC: To verify that if MULTI_OPERATOR_SUPPORT Preference is set at 'Y' then network admin is able to add multiple operators.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0290() throws Exception {


        ExtentTest t1 = pNode.createNode("TC_ECONET_0290",
                "To verify that if MULTI_OPERATOR_SUPPORT Preference is set at 'Y' then network admin is able to add multiple operators.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {
            //Add Operator
            operatorName = "AUTOOP" + DataFactory.getRandomNumber(3);

            defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MULTI_OPERATOR_SUPPORT");

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", "Y");

            //getting OperatorID
            String operatorId = MobiquityGUIQueries.getOperatorId();

            if (operatorId == null) {
                OperatorManagement.init(t1)
                        .addRechargeOperator(operatorName, DataFactory.getDefaultProvider().ProviderId);
            }

            OperatorManagement.init(t1)
                    .addRechargeOperator(operatorName, DataFactory.getDefaultProvider().ProviderId);
            t1.pass("When Preference is set to Y,we are able to create more than single operator");

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", defaultValue);

            OperatorManagement.init(t1).deleteOperator(operatorName);

        }
    }
}

