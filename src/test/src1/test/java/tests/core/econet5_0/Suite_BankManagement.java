package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.bankMaster.DeleteBank_Page1;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by prashant.kumar on 8/17/2018.
 */
public class Suite_BankManagement extends TestInit {
    private OperatorUser netAdmin;
    private Bank bank, bank2;
    private SuperAdmin saDeleteBank;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {
            netAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            saDeleteBank = DataFactory.getSuperAdminWithAccess("BANK_DELETE");

            bank = new Bank(defaultProvider.ProviderId,
                    "TMPTB" + DataFactory.getRandomNumber(4),
                    null,
                    null,
                    true,
                    false);

            bank2 = new Bank(defaultProvider.ProviderId,
                    "TMPTB" + DataFactory.getRandomNumber(4),
                    null,
                    null,
                    true,
                    false);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0019() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0019",
                "To verify that the User other than Superadmin should not able to associate or deassociate services of bank.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PRASHANT);
        try {
            Login.init(t1).login(netAdmin);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("BANK_ALL", "");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0023() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0023", "To verify that the No modify bank functionality should be available on WEB.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);


        try {
            Login.init(t1).loginAsSuperAdmin("BANK_ADD");
            new DeleteBank_Page1(t1)
                    .navDeleteBank();

            Utils.captureScreen(t1);

            // todo - below logic is not proper [rarn - 5/28/2019]
            int size = DriverFactory.getDriver().findElements(By.partialLinkText("modify")).size();
            if (size == 0) {
                t1.pass("Modify Link not Found ");
            } else {
                List<WebElement> el = driver.findElements(By.partialLinkText("modify"));
                Boolean isFound = false;
                for (WebElement e : el) {
                    String link = e.getText();
                    if (link.equalsIgnoreCase("Modify Bank")) {
                        isFound = true;
                    }
                }
                if (isFound) {
                    Utils.captureScreen(t1);
                    t1.fail("Link found");
                } else {
                    t1.pass("Modify Link not Found ");
                    Utils.captureScreen(t1);
                }
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0025() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0025", "To verify that the Proper error message should get generated during Delete Bank if any subscriber is associated with the bank.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin(saDeleteBank);


            String bankName = DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId);
            DeleteBank_Page1 page = new DeleteBank_Page1(t1);
            page.navDeleteBank();
            page.selectBankForDeletion(bankName)
                    .clickSubmitPg1();
            Assertion.verifyErrorMessageContain("Bank.validation.bank.associated.Customer", "User Cannot delete the bank if it is associated with User", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * Disabling this case as it is automated incorrectly
     * //TODO Correct the Test Case
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0}, enabled = false)
    public void TC_ECONET_0494() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0494",
                "To verify that the While adding bank account to susbcriber on WEB ,subscriber " +
                        "will be given a choice of banks (in case of multiple banks), and mobiquity™ " +
                        "would display only those banks in the dropdown list for whom Trust A/c no. is defined in the system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin("MFSBMD");

            startNegativeTest();
            CurrencyProviderMapping.init(t1).deleteServiceProviderBankMappingNew(DataFactory.getDefaultProvider().ProviderId, defaultBank.BankName);
            Assertion.verifyErrorMessageContain("Default.Bank.Can.not.Deleted", "Group role it is associated", t1, defaultBank.BankName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0498() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0498", "To verify that the Admin User should be able to Link Bank Account of Subscriber via WEB." +
                "case of multiple banks), and mobiquity™ would display only those banks in the dropdown\n" +
                "/ list for whom Trust A/c no. is defined in the system.\"");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);

        try {
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0495() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0495", "To verify that the Super Admin should be able to enable bank services for a registered bank account." +
                "/ list for whom Trust A/c no. is defined in the system.\"");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1)
                    .loginAsSuperAdmin("OPT_BANK_ACC_ADD");

            if (bank.getCreated()) {
                CurrencyProviderMapping.init(t1)
                        .addServiceProviderBankAccount(bank.BankName, bank.PoolAccNum);
            } else {
                CurrencyProviderMapping.init(t1)
                        .addServiceProviderBankAccount(DataFactory.getDefaultBankNameForDefaultProvider(), bank.PoolAccNum);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test is Disabled because it is incorrectly automated
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0}, enabled = false)
    public void TC_ECONET_1158() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1158", "To verify that the Super Admin should be able to enable bank services for a registered bank account." +
                "/ list for whom Trust A/c no. is defined in the system.\"");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);


        Login.init(t1)
                .loginAsSuperAdmin("BANK_ADD");

        CurrencyProviderMapping.init(t1)
                .addBank(bank);


        CurrencyProviderMapping.init(t1)
                .addBank(bank2);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0888() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0888", "To verify that If modification is needed, then the particular bank needs to be deleted from system and then re-registered with modified details.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);
        try {
            Bank liqBank = new Bank(defaultProvider.ProviderId,
                    "TMPLIQ" + DataFactory.getRandomNumber(3),
                    null,
                    null,
                    true,
                    false);

            Login.init(t1).loginAsSuperAdmin("BANK_ADD");

            //Creating NonPartnerBank
            CurrencyProviderMapping.init(t1)
                    .addBank(liqBank);

            //Verifying if modify is allowed

            fl.contentFrame();
            driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
            fl.clickLink("BANK_ALL");
            try {
                driver.findElement(By.partialLinkText("Modify"));
                t1.fail("Superadmin able to modify bank Details");
            } catch (Exception e) {
                t1.pass("Superadmin not able to modify bank Details");
                Utils.captureScreen(t1);
            }

            //Deleting Bank
            CurrencyProviderMapping.init(t1)
                    .deleteBank(liqBank);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
    }


    @Test(groups = {FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1026() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1026", "To verify that the For each MMT Trust account it is pre-funded by Bank with following changes in Bank’s Mirror Trust Account " +
                "& Bank’s E-money Account." +
                "1) Bank’s mirror trust a/c in mobiquity will never be allowed to go in positive balance (above‘0’).");
        try {

            t1.assignCategory(FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0);

            List<String> banks = DataFactory.getAllBankNamesForUserCreation(GlobalData.defaultProvider.ProviderName);

            t1.info("Checking Bank's Balance in DB");
            for (String bankName : banks) {
                BigDecimal bankBalance = MobiquityGUIQueries.getBalanceBankWallet(bankName);

                if (bankBalance.floatValue() <= 0) {
                    t1.pass("Balance of Bank : " + bankName + " is Negative i.e Less than or Equal to 0");

                } else {
                    t1.fail("Balance is Positive i.e above 0");
                }
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0017() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0017",
                "TTo verify that the Superadmin should be able to view " +
                        "Add Bank Account number after logging into WEB.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PRASHANT);

        Login.init(t1).loginAsSuperAdmin("BANK_ADD");
        try {
            FunctionLibrary.init(t1).verifyLinkExists("BANK_ALL", "BANK_BANK_ADD");
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
    }

// Note - below tets is covered as part of Base Setup
//    @Test(groups = {FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0}, priority = 15)
//    public void TC_ECONET_0497() throws Exception {
//        try {
//            String bankName = "TEST" + DataFactory.getRandomNumber(3);
//
//            Bank bank = new Bank(providerName, bankName, Constants.BANK_TYPE_TRUST);
//
//            ExtentTest t1 = pNode.createNode("TC_ECONET_0497",
//                    "To verify that the For each bank addition in mobiquity, Each bank will maintain a MMT Trust Account at its end, and it will be prefunded by bank itself.\n" +
//                            "Mobiquity will maintain a Bank’s Mirror Trust Account & Bank’s E-money Account for each\n" +
//                            "bank and both Trust accounts will be in sync")
//                    .assignCategory(FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);
//
//            Login.init(t1).loginAsSuperAdmin("BANK_ADD");
//
//            CurrencyProviderMapping.init(t1).addBank(bank);
//
//            StockManagement.init(t1).
//                    initiateAndApproveNetworkStock(providerName, bank.BankName, Constants.STOCK_TRANSFER_AMOUNT);
//
//            BigDecimal postAmt = MobiquityGUIQueries.getBalanceBankWallet(bank.BankName);
//
//            BigDecimal preAmt = new BigDecimal("0").subtract(new BigDecimal(Constants.STOCK_TRANSFER_AMOUNT));
//
//            Assertion.verifyEqual(postAmt, preAmt, "Checking the Pre-funded Balance of Stock in Negative ", t1);
//
//        } catch (Exception e) {
//            markSetupAsFailure(e);
//        }
//        Assertion.finalizeSoftAsserts();
//    }
}





