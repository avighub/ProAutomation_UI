package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.bulkUserDeletion.BulkUserDeletion;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.HashMap;

import static framework.util.jigsaw.CommonOperations.getBalanceForOperatorUsersBasedOnUserId;

public class Suite_BulkUserDeletion extends TestInit {
    OperatorUser opt;
    User whs;
    private String unregPref, displayAllowedReg, modifyAllowedReg;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {

        ExtentTest s = pNode.createNode("Setup", "Setup Specific to this Script");

        try {
            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            MobiquityGUIQueries m = new MobiquityGUIQueries();
            unregPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_PUSH_REQUIRED_IN_DELETION");
            displayAllowedReg = MobiquityGUIQueries.fetchDisplayAllowedFromGUI("IS_PUSH_REQUIRED_IN_DELETION");
            modifyAllowedReg = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("IS_PUSH_REQUIRED_IN_DELETION");

            if (!unregPref.equalsIgnoreCase("FALSE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_PUSH_REQUIRED_IN_DELETION", "Y");
                SystemPreferenceManagement.init(s)
                        .updateSystemPreference("IS_PUSH_REQUIRED_IN_DELETION", "FALSE");
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0301_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0301_a", "To verify that the Network Admin is able to Delete the Channel User(Wholesaler) through Bulk User Deletion.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);
        try {

            whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);

            HashMap<String, String> users = new HashMap<>();
            users.put(whs.MSISDN, "CHANNEL");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(opt);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //TODO
            String whsStatus = MobiquityGUIQueries.getUserStatus(whs.MSISDN, Constants.WHOLESALER);
            DBAssertion.verifyDBAssertionEqual(whsStatus, "N", "Verification of user status", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0301_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0301_b", "To verify that the Network Admin is able to Delete the subscriber through Bulk User Deletion.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);
        try {
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);


            HashMap<String, String> users = new HashMap<>();
            users.put(subs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(opt);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            String subsStatus = MobiquityGUIQueries.getUserStatus(subs.MSISDN, Constants.SUBSCRIBER);
            DBAssertion.verifyDBAssertionEqual(subsStatus, "N", "Verification of user status", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    /**
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0302() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0302",
                "To verify that the Bulk user delete will be successful if user has amounts in secondary wallet " +
                        "then it will be transferred in primary wallet and account closure will be called on primary wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);
        try {
            OperatorUser opt = DataFactory.getOperatorUserWithAccess(Roles.BULK_USER_DELETION);

            User whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            //todo Uncomment
            ChannelUserManagement.init(t1).createChannelUser(whs, false);

            Login.init(t1).login(opt);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(whs,
                            Constants.COMMISSION_WALLET);

            UsrBalance preWhsBalance18 = MobiquityGUIQueries.getUserBalance(whs, Constants.COMMISSION_WALLET, null);
            int preWBalance18 = preWhsBalance18.Balance.intValue();
            t1.info("pre Savings club balance: " + preWBalance18);

            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int preWBalance = preWhsBalance.Balance.intValue();
            t1.info("pre Normal balance: " + preWBalance);
            HashMap<String, String> users = new HashMap<>();
            users.put(whs.MSISDN, "CHANNEL");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(opt);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            String whsStatus = MobiquityGUIQueries.getUserStatus(whs.MSISDN, Constants.WHOLESALER);
            DBAssertion.verifyDBAssertionEqual(whsStatus, "N", "Verification of user status", t1);

            UsrBalance postWhsBalance18 = MobiquityGUIQueries.getUserBalanceAfterAccountIsClosed(whs, Constants.COMMISSION_WALLET, null);
            int postWBalance18 = postWhsBalance18.Balance.intValue();
            t1.info("post Savings club balance: " + postWBalance18);

            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalanceAfterAccountIsClosed(whs, null, null);
            int postWBalance = postWhsBalance.Balance.intValue();
            t1.info("post Normal balance: " + postWBalance);

            Assertion.verifyEqual(postWBalance, 0, "Amount is transferred", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    //MON-8467
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0303() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0303", "To verify that the Any frozen funds is there in subscriber wallet then an intimation should be sent to the consumer that frozen funds  would be rolled-backed before going forward with the bulk user deletion process.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);
        try {
            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            User subs = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preBalance = preSubsBalance.Balance.intValue();
            int prefrozenbalance = preSubsBalance.frozenBalance.intValue();

            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, subs, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);


            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(subs, DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(3), "5", "test", Constants.NORMAL_WALLET);

            UsrBalance subsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int balanceAfterInit = subsBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = subsBalanceAfterInit.frozenBalance.intValue();
            System.out.println(frozenbalanceAfterInit + "fbal1");

            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + 5);
            t1.info("Expected: " + (prefrozenbalance + 5) + " Actual: " + frozenbalanceAfterInit);
            Thread.sleep(1000);
            Assert.assertEquals(balanceAfterInit, preBalance);

            HashMap<String, String> users = new HashMap<>();
            users.put(subs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(opt);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            String subsStatus = MobiquityGUIQueries.getUserStatus(subs.MSISDN, Constants.SUBSCRIBER);
            DBAssertion.verifyDBAssertionEqual(subsStatus, "N", "Verification of user status", t1);

            UsrBalance subsBalanceAfterInit1 = MobiquityGUIQueries.getUserBalanceAfterAccountIsClosed(subs, null, null);
            int frozenbalanceAfterInit1 = subsBalanceAfterInit1.frozenBalance.intValue();
            System.out.println(frozenbalanceAfterInit1 + "fbal");

            Assert.assertEquals(frozenbalanceAfterInit1, 0);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    //MON-8467
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0304() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0304", "To verify that the In case of any ambiguous or FIC transaction the operator would be given a warning through a message about the FIC funds of the subscriber/channel user. In case operator decides it can choose to still proceed with the deletion process.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);
        try {

            User subs = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, true);

            Login.init(t1).login(whs);

            ServiceCharge cashinService = new ServiceCharge(Services.CASHIN, whs, subs, null, null, null, null);
            TransferRuleManagement.init(t1)
                    .configureTransferRule(cashinService);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal("10"));

            String bankId = DataFactory.getDefaultBankIdForDefaultProvider();

            BigDecimal txnAmt = new BigDecimal(5);

            //initiate wallet to bank service
            SfmResponse response = Transactions.init(t1)
                    .walletToBankService(subs, txnAmt.toString(), bankId).verifyMessage("sfm.ambiguous.initiated");

            t1.info("Transaction ID: " + response.TransactionId);

            UsrBalance postUserCreationBalance = getBalanceForOperatorUsersBasedOnUserId(bankId);
            t1.info("FIC Pre Balance : " + postUserCreationBalance.FIC);

            UsrBalance userBalanceAfterInit = MobiquityGUIQueries.getUserBalance(subs, null, null);
            System.out.println(userBalanceAfterInit.Balance + "fbalA");

            HashMap<String, String> users = new HashMap<>();
            users.put(subs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(opt);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            String userStatus = MobiquityGUIQueries.getUserStatus(subs.MSISDN, Constants.SUBSCRIBER);
            DBAssertion.verifyDBAssertionEqual(userStatus, "N", "Verification of user status", t1);

            UsrBalance postUserCreationBalance1 = getBalanceForOperatorUsersBasedOnUserId(bankId);
            t1.info("FIC Post Balance : " + postUserCreationBalance1.FIC);

            UsrBalance userBalanceAfterInit1 = MobiquityGUIQueries.getUserBalanceAfterAccountIsClosed(subs, null, null);
            t1.info("User Balance After Account is CLosed :" + userBalanceAfterInit1.Balance);

            Assert.assertEquals(userBalanceAfterInit1.Balance.intValue(), 0);

            SMSReader.init(t1).verifyNotificationContain(subs.MSISDN, "", response.TransactionId);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }


    @AfterClass(alwaysRun = true)
    public void post_condition() throws Exception {
        ExtentTest teardown = pNode.createNode("post_condition", "Updating the preference to default value");
        /**
         * Change the system preference
         */
        MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_PUSH_REQUIRED_IN_DELETION", displayAllowedReg);
        MobiquityGUIQueries.updateModifyAllowed("IS_PUSH_REQUIRED_IN_DELETION", modifyAllowedReg);
        SystemPreferenceManagement.init(teardown)
                .updateSystemPreference("IS_PUSH_REQUIRED_IN_DELETION", unregPref);

    }


}
