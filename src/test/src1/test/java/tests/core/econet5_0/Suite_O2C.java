package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.channelEnquiry.ChannelEnquiry;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by prashant.kumar on 9/4/2018.
 */
public class Suite_O2C extends TestInit {
    private static OperatorUser payer, o2cInitiator, o2cApprover1;
    private static User payee, channelUser;
    private static ExtentTest setup, tearDown;
    private static String status, providerName;
    private static OperatorUser channelAdmin, optWithoutRoles, cce;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {


        try {
            payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            payer = new OperatorUser(Constants.NETWORK_ADMIN);
            o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");
            providerName = DataFactory.getDefaultProvider().ProviderName;
            channelUser = DataFactory.getChannelUserWithCategory("WHS");
            channelAdmin = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);

            //Group Role Specific Setup
            groupRoleSpecificSetup();

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    private void groupRoleSpecificSetup() {
        ExtentTest s1 = pNode.createNode("Setup", "Create a Network Admin with Specific Permission");


        try {
            String[] notRequiredRoles = {"O2C_INIT", "O2C_APP1", "O2C_ENQ", "T_RULES", "O2C_TRULES"};

            ArrayList<String> applicableRole = new ArrayList<>(Arrays.asList("CHECK_ALL"));
            WebGroupRole webGroupRole = new WebGroupRole(Constants.NETWORK_ADMIN, "AUTNWADM1", applicableRole, 1);

            Login.init(s1).loginAsSuperAdmin("GRP_ROL");
            GroupRoleManagement.init(s1)
                    .addWebGroupRole(webGroupRole);

            // now make sure that this role doesnt have notRequiredRoles
            GroupRoleManagement.init(s1).removeSpecificRole(webGroupRole, notRequiredRoles);

            // now create a network admin with this specific Role
            optWithoutRoles = new OperatorUser(Constants.NETWORK_ADMIN);
            optWithoutRoles.WebGroupRole = webGroupRole.RoleName;

            OperatorUserManagement.init(s1).createAdmin(optWithoutRoles);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    public void checkPre(User payer, User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0663() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0663", "To verify that User other than valid user can not initiate the O2C successfully if valid details are entered.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        Login.init(t1).login(channelUser);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("O2CTRF_ALL", "O2CTRF_O2CINT001");

        Login.init(t1).login(cce);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("O2CTRF_ALL", "O2CTRF_O2CINT001");

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0664() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0664", "To verify that User other than valid user can not approve the initiated O2C successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        Login.init(t1).login(channelUser);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("O2CTRF_ALL", "O2CTRF_O2CAPP01");

        Login.init(t1).login(cce);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("O2CTRF_ALL", "O2CTRF_O2CAPP01");

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0665() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0665", "To Verify that User other than valid user can't do O2C Enquiry.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        Login.init(t1).login(channelUser);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("MCHENQ_ALL", "MCHENQ_O2CENQ01");
        FunctionLibrary.init(t1).verifyLinkNotAvailable("MCHENQ_ALL", "MCHENQ_O2CENQ01");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0668() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0668", "To verify that User other than valid user is not able to define O2C transfer rule successfully through web.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        Login.init(t1).login(channelUser);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("TRULES_ALL", "TRULES_O2C_TR");

        Login.init(t1).login(channelAdmin);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("TRULES_ALL", "TRULES_O2C_TR");

        Login.init(t1).login(cce);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("TRULES_ALL", "TRULES_O2C_TR");

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0669() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0669", "To verify that the Valid user should able to perform O2C if system does breach the TCP configured in the system, while crediting the money in the primary wallet of the channel user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        User payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);


        try {
            Login.init(t1).login(o2cInitiator);
            String transID = TransactionManagement.init(t1).
                    initiateO2C(payee, Constants.O2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            if (transID != null) {
                Login.init(t1).login(o2cApprover1);
                TransactionManagement.init(t1).o2cApproval1(transID);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0706_A() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0706_A", "To verify that the O2C enquiry can be done using below parameters:\n" +
                "1.Using only Transaction ID");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        try {
            OperatorUser enqUsr = DataFactory.getOperatorUserWithAccess("O2C_ENQ");
            String transID = new MobiquityGUIQueries().dbGetLastTransID(Services.O2C);
            if (transID != null) {
                Login.init(t1).login(enqUsr);
                ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingTxnID(transID);
            } else {
                t1.skip("Skipping test case as Transaction ID is NULL");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0706_B() throws IOException {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0706_B", "To verify that the O2C enquiry can be done using below parameters:\n" +
                "1.Using MFS Provider Name, Payment Instrument, Wallet Type and MSISDN");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        try {

            User payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            ServiceCharge sCharge = new ServiceCharge(Services.O2C, o2cInitiator, payee, null, null, null, null);
            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);

            BigDecimal o2cAmount = MobiquityGUIQueries.getMaxTransferValue(sCharge.ServiceChargeName);
            o2cAmount = o2cAmount.subtract(BigDecimal.valueOf(1));
            Login.init(t1).login(o2cInitiator);
            String transID = TransactionManagement.init(t1).initiateO2C(payee,
                    o2cAmount.toString(), Constants.REMARKS, DataFactory.getRandomNumberAsString(6));
            if (transID != null) {
                TransactionManagement.init(t1).o2cApproval1(transID);
                //Utils.putThreadSleep(5000);
                Login.resetLoginStatus();
                Login.init(t1).loginAsOperatorUserWithRole(Roles.OPERATOR_TO_CHANNEL_TRANSFER_ENQUIRY);
                ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingMobileNumber(payee.MSISDN, transID);
            } else {
                t1.skip("Skipping test case as Transaction ID is NULL");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0706_C() throws IOException {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0706_C", "To verify that the O2C enquiry can be done using below parameters:\n" +
                "1.Using MFS Provider Name, Payment Instrument, Wallet Type, Date Range and Status of the transaction");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        String fromDate = new DateAndTime().getDate(-5);
        String toDate = new DateAndTime().getDate(0);
        try {
            OperatorUser enqUsr = DataFactory.getOperatorUserWithAccess("O2C_ENQ");

            OperatorUser payer = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            User payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);

            BigDecimal o2cAmount = MobiquityGUIQueries.getMaxTransferValue(sCharge.ServiceChargeName);
            o2cAmount = o2cAmount.subtract(BigDecimal.valueOf(1));
            String reference = DataFactory.getRandomString(6);
            Login.init(t1).login(payer);
            String transID = TransactionManagement.init(t1).initiateO2C(payee, o2cAmount.toString(), Constants.REMARKS, reference);
            if (transID != null) {
                TransactionManagement.init(t1).o2cApproval1(transID);
            }


            Login.init(t1).login(enqUsr);
            ChannelEnquiry.init(t1).
                    doO2CTransferEnquiryUsingDateRange(transID, fromDate, toDate, Constants.TXN_STATUS_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1008() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1008", "To verify that O2C transaction should not be successful if transaction amount is more than defined slab values(To value).");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        try {
            Login.init(t1).login(o2cInitiator);
            User user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            ServiceCharge obj = new ServiceCharge(Services.O2C, o2cInitiator, user, null, user.PaymentType, null, user.ProviderId);

            BigDecimal amt = MobiquityGUIQueries.getMaxTransferValue(obj.ServiceChargeName);

            if (amt == null) {
                /**
                 * This is the maximum amount
                 */
                amt = new BigDecimal(999999999);
            }
            BigDecimal one = new BigDecimal(2);

            amt = amt.add(one);

            System.out.println(amt);
            String walletName = DataFactory.getWalletName(Constants.NORMAL_WALLET);
            String providerId = DataFactory.getDefaultProvider().ProviderId;
            DataFactory.getDefaultProvider();
            startNegativeTest();
            TransactionManagement.init(t1).initiateO2CForSpecificWallet(user, walletName, providerId, "" + amt.toString(), null);
            Assertion.verifyErrorMessageContain("o2c.initiation.maxvalue.amt", "Assert Error message", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0}, priority = 2)
    public void TC_ECONET_1180() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1180 :O2C (Owner To Channel)", "To verify that network admin is able to Approve/Reject the add initiated service charge.");

        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0);
        t1.info("Approve/Reject the add initiated service charge.");
        try {
            Login.init(t1).login(o2cInitiator);
            ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
            status = ServiceChargeManagement.init(setup).checkServiceChargeStatus(sCharge);
            if (status.equals("Y")) {
                ServiceChargeManagement.init(t1).deleteServiceCharge(sCharge);
                ServiceChargeManagement.init(t1).initiateAndApproveServiceCharge(sCharge);
            }
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0357() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0357",
                "To verify that the Valid user (Network admin) should able to " +
                        "perform O2C if system doesn't breach the TCP configured in " +
                        "the system, while crediting the money in the primary wallet of the channel user.")
                .assignCategory(FunctionalTag.ECONET_UAT_5_0);
        try {

            User payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            //networkAdmin = DataFactory.getOperatorUserWithAccess("TCP_INSTRMENT");

            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(payee.DomainName, payee.CategoryName, payee.GradeName, providerName, "Normal");

            t1.info("TCP Profile is: " + insTcp.ProfileName);
           /* Login.init(t1).login(networkAdmin);
            TCPManagement.init(t1)
                    .editTcpForChannelUser(payee, l_tcpID, payee.DomainName, payee.DomainName, payee.GradeName, DataFactory.getDefaultProvider().ProviderName, "O2C Transfer", "9999");*/

            TransactionManagement.init(t1).initiateAndApproveO2C(payee, "999", Constants.REMARKS, DataFactory.getRandomString(6));


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }



}


