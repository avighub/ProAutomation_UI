package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.BulkMessageUpdate.BulkMessageUpdatePage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Nirupama MK on 29/08/2018.
 */
public class Suite_SMSConfigurations extends TestInit {
    OperatorUser opUsr;
    User chUser;
    String messageCodeStatic, messageCodeDynamic;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        opUsr = DataFactory.getOperatorUserWithAccess("SMS_CR");
        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        messageCodeStatic = "01043"; // this message code is fetched from messages_en.properties
        messageCodeDynamic = "10071"; // this message code is fetched from messages_en.properties
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0708() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0708", "To verify that the No user other than" +
                " network admin can Modify SMS Configurations.");

        ExtentTest t2 = pNode.createNode("TC_ECONET_1174", "Two Step subscriber registration : " +
                "To verify User other than Network Admin can not define the time period in the system Preference till " +
                "which a new subscriber remains in the pending state and does not respond to the " +
                "2 step subscriber registration Push message.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0);
        try {

            try {
                OperatorUser chAdm = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
                Login.init(t1).login(chAdm);

                driver.findElement(By.id("MPREF_ALL"));
                t1.fail("Preferences link is displayed for Channel Admin");

            } catch (Exception e) {
                Assertion.logAsPass("Preferences link is not displayed for Channel Admin as Expected", t1);
            }

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("MPREF_ALL"));
                t1.fail("Preferences link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Preferences link is not displayed for Channel User as Expected", t2);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t2);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0707() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0707", "To verify that After successful " +
                "update in SMS, new SMS should sent to users after performing corresponding transaction.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0);

        try {
            String newStaticText = "String For Testing Purpose";

            Login.init(t1).loginAsOperatorUserWithRole(Roles.SMS_CONFIGURATION);

            Preferences.init(t1)
                    .updateSMSConfigurationMessageText(Services.O2C, messageCodeStatic, newStaticText);

            // verify that newly set message is reflected
            String messageText = Preferences.init(t1)
                    .getSMSMessageText(Services.O2C, messageCodeStatic);

            Assertion.verifyEqual(messageText, newStaticText,
                    "Verify that the Newly set Static text is reflected properly", t1);

            String txnID = TransactionManagement.init(t1)
                    .initiateO2C(chUser, "10", Constants.REMARKS);

            TransactionManagement.init(t1)
                    .o2cApproval1(txnID, true);

            SMSReader.init(t1).verifyNotificationContain(chUser.MSISDN, newStaticText);

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        } finally {
            Preferences.init(t1).updateSMSConfigurationMessageText(Services.O2C, messageCodeStatic,
                    MessageReader.getMessage(messageCodeStatic, null));
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0710() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0710", "To verify that If SMS is modified " +
                "and if it doesn't match with the original keywords for SMS.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(opUsr);

            Preferences.init(t1)
                    .startNegativeTest()
                    .updateSMSConfigurationWithTransactionData(Services.O2C,
                            messageCodeDynamic, Constants.TRANSACTION_DATA);

            String actMsg = driver.switchTo().alert().getText();
            String expMsg = MessageReader.getMessage("sms.config.alert", null);

            Assertion.verifyContains(actMsg, expMsg,
                    "Transaction Data provided is not valid for current message", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }
    // boolean status = Utils.checkElementPresent("mobileNumberLabel", Constants.FIND_ELEMENT_BY_ID);
    //                Assertion.verifyEqual(status, true, "mobile_number Field", pNode);

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0709() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0709", "To verify that the Network admin has the " +
                "option to download the list of existing messages using a link provided on the web interface.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BULK_MESSAGE_UPDATE);

            BulkMessageUpdatePage page = new BulkMessageUpdatePage(t1);
            page.navigateToBulkMessageUpdate();

            boolean status = Utils.checkElementPresent("//img[contains(@src,'/CoreWeb/mjsp/common/images/test.gif')]", Constants.FIND_ELEMENT_BY_XPATH);
            Assertion.verifyEqual(status, true, "Download Link", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

}