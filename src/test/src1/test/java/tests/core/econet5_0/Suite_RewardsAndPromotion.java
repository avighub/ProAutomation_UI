package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.loyaltyManagement.LMS;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * created by krishan.chawla on 12/11/2018 for econet UAT
 */
public class Suite_RewardsAndPromotion extends TestInit {

    public String PromoName;
    public String fromDate;
    public String toDate;
    public String Type;
    private ExtentTest tearDown;
    private boolean isCreated = false;
    private OperatorUser promotionCreationNetworkAdmin, promotionModificationNetworkAdmin;
    private User chUser, chUser1, subs;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        PromoName = "LMS" + DataFactory.getRandomNumber(5);
        String dateFormat = "dd-MM-yyyy";
        String time = " 02:30:00 PM";
        fromDate = new DateAndTime().getDate(dateFormat, 3) + time;
        toDate = new DateAndTime().getDate(dateFormat, 21) + time;
        promotionCreationNetworkAdmin = DataFactory.getOperatorUserWithAccess("C_ROLE", Constants.NETWORK_ADMIN);
        promotionModificationNetworkAdmin = DataFactory.getOperatorUserWithAccess("MC_ROLE", Constants.NETWORK_ADMIN);

        tearDown = pNode.createNode("Tear Down");
        isCreated = false;
        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 2);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0091 : CREATE LMS PROFILE ", "To verify that the valid user can create promotion in system.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        Login.init(t1).login(promotionCreationNetworkAdmin);
        isCreated = LMS.init(t1).addLMSpromotion(PromoName, fromDate, toDate, Services.INTERNAT_SEND_MONEY);
        Login.init(tearDown).loginAsSuperAdmin("AC_ROLE");
        LMS.init(tearDown).approveLMS(PromoName);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void Test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0368 : Loyalty Management ", "To verify that the valid user can view promotion in system.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        if (isCreated) {
            Login.init(t1).login(promotionModificationNetworkAdmin);
            LMS.init(t1).ViewLMS(PromoName, fromDate, toDate);
        } else {
            t1.skip("Test cases skipp because profile is not created");
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void Test_03() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0094 : Loyalty Management ", "To verify that the valid user can modify promotion in system.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        if (isCreated) {
            Login.init(t1).login(promotionModificationNetworkAdmin);
            LMS.init(t1).modifyLMS(PromoName);
            Login.init(tearDown).loginAsSuperAdmin("AC_ROLE");
            LMS.init(tearDown).approveLMS(PromoName);
        } else {
            t1.skip("Test cases skipp because profile is not created");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0367() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0367 : Loyalty Management ", "To verify that channel user is able to perform loyalty balance enquiry for self and for other channel /subscriber users.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(chUser);

            LMS.init(t1).LMSchannelUserENQ(subs.MSISDN, Constants.USER_TYPE_SUBS);
            Utils.captureScreen(t1);
            t1.pass("User is able to view Loyalty balance for other Users");

            LMS.init(t1).LMSchannelSelfUserENQ(chUser.Password);
            Utils.captureScreen(t1);
            t1.pass("User is able to view Self Loyalty balance");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
