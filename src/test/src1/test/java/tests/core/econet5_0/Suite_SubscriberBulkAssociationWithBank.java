package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.File;

public class Suite_SubscriberBulkAssociationWithBank extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_Bulk_Association, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0570() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0570", "To verify that the nwAdmin should be able" +
                " to perform Subscriber bulk association with bank service.")

                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_Bulk_Association, FunctionalTag.ECONET_UAT_5_0);
        try {

            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            User sub_01 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(sub_01);

            String bankID = DataFactory.getDefaultBankId(DataFactory.getDefaultProvider().ProviderId);

            User sub_02 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(sub_02);

            Login.init(t1).login(netAdmin);
            String fileName = ChannelUserManagement.init(t1)
                    .generateBulkUserBankAssociationcsvFile(sub_01, sub_02, bankID);

            ChannelUserManagement.init(t1)
                    .initiateBulkUserBankAssociation(fileName);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0545", "To verify that operator is able to " +
                    "disassociate the banking services for required subscribers in bulk.");

            ExtentTest t3 = pNode.createNode("TC_ECONET_1087", "To verify that Operator user " +
                    "performing the operation should have the corresponding role assigned.");

            ChannelUserManagement.init(t2)
                    .bulkUserBankDisAssociation(fileName);

            Assertion.logAsPass("Operator user is having Corresponding role Assigned", t3);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_Bulk_Association, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1088() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1088", "To verify that Bulk uploaded file should " +
                "be in .csv format.")

                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_Bulk_Association, FunctionalTag.ECONET_SIT_5_0);
        try {
            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");

            String filePath = FilePath.dirFileUploads + "AutomationBulkBankAssociation" + DataFactory.getTimeStamp();
            String fileName = filePath + ".txt";

            File f = new File(fileName);
            f.createNewFile();

            Login.init(t1).login(netAdmin);

            ChannelUserManagement.init(t1)
                    .startNegativeTest()
                    .initiateBulkUserBankAssociation(fileName);

            String actual = Assertion.getErrorMessage();
            Assertion.verifyMessageContain(actual, "billUpload.file.error",
                    "Only csv files can be uploaded", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_Bulk_Association, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1089() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1133", "To verify that the network admin should able " +
                "to Download sheet of Bulk Association of Subscriber with Bank.")

                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_Bulk_Association, FunctionalTag.ECONET_SIT_5_0);
        try {

            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            Login.init(t1).login(netAdmin);

            AddChannelUser_pg1.init(t1).navBulkBankAssociation();
            AddChannelUser_pg1.init(t1).clickOnStartDownload();

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Verify NwAdmin can download sheet of Bulk Association of Subscriber with Bank ", t1);

            ExtentTest t2 = pNode.createNode("TC_ECONET_1089", "To verify that following fields are " +
                    "provided for each record in the uploaded file:" +
                    "1) Bank ID " +
                    "2) Provider ID " +
                    "3) MSISDN ");

            BulkChUserAndSubRegAndModPage.init(t2).verifyFieldInCsvFile(FilePath.fileBulkBankAssoc, "MSISDN*");
            BulkChUserAndSubRegAndModPage.init(t2).verifyFieldInCsvFile(FilePath.fileBulkBankAssoc, "Provider Id*");
            BulkChUserAndSubRegAndModPage.init(t2).verifyFieldInCsvFile(FilePath.fileBulkBankAssoc, "Bank Id*");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_Bulk_Association, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1090() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1151", "To verify that the channel user should " +
                "able to Add Bank Details of Subscriber via WEB.");

        User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub, true, true);

        ExtentTest t2 = pNode.createNode("TC_ECONET_1090", "To verify that File is not uploaded " +
                "successfully if details entered in Uploaded file is not correct.")

                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NMB, FunctionalTag.ECONET_SIT_5_0);
        try {

            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            Login.init(t2).login(netAdmin);

            Login.init(t2).login(netAdmin);
            String fileName = ChannelUserManagement.init(t2)
                    .generateBulkUserBankAssociationcsvFile(null, null, "");

            ChannelUserManagement.init(t2)
                    .startNegativeTest()
                    .bulkUserBankDisAssociation(fileName);

            Assertion.verifyErrorMessageContain("sub.bank.disassociation",
                    "Uploaded file is not correct ", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

}
