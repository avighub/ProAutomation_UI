package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_CustomerSelfRegistrationOnHandset extends TestInit {
    private User whs, sub;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void TC_ECONET_0294() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0294 ",
                "  To verify that valid channel user can upload the identity and address proof documents through web for the customers registered through the Mobile handset.").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.SUBSCRIBER_MANAGEMENT);
        try {

            //creating 1st self sbscriber through USSD
            sub = new User(Constants.SUBSCRIBER);
            TxnResponse result = Transactions.init(t1)
                    .selfRegistrationForSubscriberWithKinDetails(sub);
            Login.init(t1).login(whs);
            SubscriberManagement.init(t1).modifySubscriberRegisteredOnHandset(whs, sub, "Passport");
            SubscriberManagement.init(t1).modifyApprovalSubs(sub);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
