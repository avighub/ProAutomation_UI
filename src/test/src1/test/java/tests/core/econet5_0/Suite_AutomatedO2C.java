package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class Suite_AutomatedO2C extends TestInit {

    @Test(priority = 1)
    public void TC_API_ECONET_002() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_API_ECONET_002", "To Verify that Automated O2C" +
                "through Bank in case of no response from mobiquity, Bank can use transaction enquiry of" +
                " mobiquity to check the transaction status ");
        try {

            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            //create service charge and transfer rule for Automatic O2C
            ServiceCharge autoO2C = new ServiceCharge(Services.AUTO_O2C, opt, whs, null,
                    null, null, null);

            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(autoO2C);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(whs, Constants.NORMAL_WALLET, new BigDecimal("9"));

            String bankWalletNo = DataFactory.getBankId(DataFactory.getDefaultBankNameForDefaultProvider());

            //get the initial balance of channel user
            UsrBalance prebalance = MobiquityGUIQueries
                    .getUserBalance(whs, null, null);

            BigDecimal initUsrBalance = prebalance.Balance;
            t1.info("Initial user balance is " + initUsrBalance);

            //get the initial balance of IND04
            BigDecimal preBankBalance = MobiquityGUIQueries
                    .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankWalletNo);

            //perform Automatic O2C
            String txnID = Transactions.init(t1).automaticO2C(whs, "5",
                    DataFactory.getBankId(DataFactory.getDefaultBankNameForDefaultProvider()),
                    DataFactory.getRandomNumberAsString(3));

            //check user balance got credited
            UsrBalance postbalance = MobiquityGUIQueries.
                    getUserBalance(whs, null, null);

            BigDecimal currentUsrBalance = postbalance.Balance;

            ChannelUserManagement.init(t1).
                    checkIfUserBalanceisCredited(initUsrBalance, currentUsrBalance, false, "ChannelUser");

            //check IND04 got debited
            BigDecimal postBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankWalletNo);
            t1.info("Pre /Post  IND04 Balnce:  " + preBankBalance + " / " + postBankBalance);

            // bank balance is always negative and hence the comprison is inverse
            Assertion.verifyEqual((preBankBalance.compareTo(postBankBalance) > 0), true,
                    "Balance Got Debited", t1);

            Login.init(t1).login(opt);
            Enquiries.init(t1).transactionDetails(txnID);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
