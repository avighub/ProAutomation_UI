package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.blackListManagement.BlackListManagement;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Customer BlackList
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 3/09/2018
 */

public class Suite_CustomerBlackList extends TestInit {
    private static String blklMsisdn = null;
    private OperatorUser custblkl;
    private User chAddSubs;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        blklMsisdn = DataFactory.getAvailableMSISDN();
        chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.WHOLESALER);
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.ECONET_UAT_5_0}, enabled = false)
    public void TC_ECONET_0700() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0700",
                "To verify that the Blacklisted customer shouldn't get allowed to register on mobiquity system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.ECONET_UAT_5_0);

        try {

            String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn);

            custblkl = DataFactory.getOperatorUsersWithAccess("BLK_ABL").get(0);

            Login.init(t1).login(custblkl);

            BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

            User subsBlack = new User(Constants.SUBSCRIBER);
            subsBlack.setMSISDN(blklMsisdn);
            startNegativeTest();
            Login.init(t1).login(chAddSubs);
            SubscriberManagement.init(t1).addInitiateSubscriber(subsBlack);
            Assertion.verifyErrorMessageContain("systemparty.error.blacklist.msisdn.notallowed", "MSISDN is already Blacklisted", t1, blklMsisdn);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }
}