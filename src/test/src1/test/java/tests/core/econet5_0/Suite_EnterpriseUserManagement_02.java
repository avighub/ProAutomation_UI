package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkRechargeOtherCSV;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Suite_EnterpriseUserManagement_02 extends TestInit {
    private User entUser;
    private Biller offLineBiller;
    private OperatorUser opt, bulkPayAdm;
    private Map<String, String> RCoperator;
    private String operatorId;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {

            entUser = new User(Constants.ENTERPRISE);
            entUser.setEnterpriseLimit("5");
            bulkPayAdm = DataFactory.getBulkPayerWithAccess("SAL_AP1", entUser);
            ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(entUser, false);
            TransactionManagement.init(eSetup).makeSureChannelUserHasBalance(entUser);

            offLineBiller = BillerManagement.init(eSetup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            //getting OperatorID
            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            RCoperator = MobiquityGUIQueries.getOperatorDetails(DataFactory.getDefaultProvider().ProviderId);
            RCoperator.get("opt_id");

            ServiceCharge sCharge1 = new ServiceCharge(Services.BILL_PAYMENT, entUser, offLineBiller, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(sCharge1);

            User recharge = new User(Constants.ZEBRA_MER);
            ServiceCharge sCharge2 = new ServiceCharge(Services.Recharge_Others, entUser, recharge, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(sCharge2);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    // todo this test is a complete mess - need to rewrite
    @Test(priority = 1, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT}, enabled = false)
    public void TC_ECONET_0217() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0217", "To verify that if total amount " +
                "is greater than ‘Enterprise Limit for 2 level approval’ then the system will " +
                "proceed for Approval 2 stage.");

        try {
            BigDecimal txnAmount = new BigDecimal(10);
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //InitiatingBulkpayout
            Login.init(t1).login(entUser);
            List<BulkRechargeOtherCSV> entRechargeOtherList = new ArrayList<>();
            entRechargeOtherList.add(new BulkRechargeOtherCSV("1", operatorId, txnAmount.toString(),
                    whs.MSISDN, GlobalData.defaultProvider.ProviderId, "Remark Ok"));
/*

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateBulkRechargeOtherCsv(entRechargeOtherList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateBulkPaymentNew(Constants.BULK_PAYOUT_RECHARGE_OTHER_ENTERPRISE, fileName);
*/

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, "786768", txnAmount, true);

            EnterpriseManagement.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_BILL_PAY_ENTERPRISE, "786879", true, true);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
