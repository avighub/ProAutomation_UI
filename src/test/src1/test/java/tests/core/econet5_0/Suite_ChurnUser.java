package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.reconciliationManagement.ReconciliationManagement;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.CR_B9.Suite_v5_ChurnRecord;
import tests.core.base.TestInit;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Febin.Thomas on 19/9/2018.
 */


public class Suite_ChurnUser extends TestInit {

    private OperatorUser optUser, optUser2, optUserApr;
    private String defaultProvider;
    private User subsReceiver, whs;


    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this Script");
        try {
            defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            optUser = DataFactory.getOperatorUsersWithAccess("CHURNMGMT_MAIN").get(0);
            optUserApr = DataFactory.getOperatorUsersWithAccess("CHURN_APPROVE").get(0);
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            //optUser2 = DataFactory.getOperatorUsersWithAccess("O2C_INIT").get(0);
            subsReceiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge sCharge = new ServiceCharge(Services.CHURN_REAC_SUB, optUser, new User(Constants.WHOLESALER), null, null, null, null);
            ServiceChargeManagement.init(s1)
                    .configureServiceCharge(sCharge);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    private void verifyChurnSettlementPage(User user, ExtentTest t1) throws IOException {
        ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
        Assertion.verifyEqual(page.getUserNameUI(), user.LoginId, "Verify that Username of Churned user is Shown on Settlement Page", t1);
        Assertion.verifyEqual(page.getChurnedMSISDNUI(), user.MSISDN, "Verify that MSISDN of Churned user is Shown on Settlement Page", t1);
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_1106() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1106", "To verify that the Valid user should able to view reconciliation screen on WEB for Churn Wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {


            //Report Done
            Login.init(t1).login(optUser);
            ReconciliationManagement.init(t1).getChurnBalance(DataFactory.getDefaultProvider().ProviderId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0737() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0737", "To verify that the Parent user has to be churned, then child user needs to be deleted.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {
            User whs = new User(Constants.WHOLESALER);
            User ret = new User(Constants.RETAILER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs,false);
            ret.setParentUser(whs);
            ret.setOwnerUser(whs);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(ret,false);

            //Churn the wholesaler
            CommonUserManagement.init(t1)
                    .startNegativeTest()
                    .churnInitiateUser(whs);

            ChurnUser_Page1.init(t1).checkLogFileSpecificMessage(whs.MSISDN, "churn.error.UserWithChild");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0493() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0493", "To verify that the After churn all wallet money of user should transfer to operator Churn wallet account and any user can do settlement of that using O2S service.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {
            User payer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER);
            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            //Creating user to churn
            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            //Sending money to user (for settlement)
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(payer);
            //TransactionManagement.init(t1).makeSureChannelUserHasBalance(channeluser);
            Login.init(t1).login(payer);

            TransactionManagement.init(t1).performC2C(channeluser, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(channeluser, true, false);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

            CommonUserManagement.init(t1).approveRejectChurnInitiate(channeluser, batchid, true);

            CommonUserManagement.init(t1).initiateChurnSettlement(channeluser, "Channel User");

            verifyChurnSettlementPage(channeluser, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0552() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0552", "To verify that the After Churned, all e-money of users should get transfer to common wallet - Churn Wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {

            //Report Done

            User payer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER);
            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();


            //Creating user to churn
            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            DBAssertion.prePayerBal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

            //Sending money to user (for settlement)

            //TransactionManagement.init(t1).makeSureChannelUserHasBalance(payer);
            Login.init(t1).login(payer);

            TransactionManagement.init(t1).performC2C(channeluser, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(channeluser, true, false);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);
            CommonUserManagement.init(t1).approveRejectChurnInitiate(channeluser, batchid, true);


            CommonUserManagement.init(t1).initiateChurnSettlement(channeluser, "Channel User");

            DBAssertion.postPayerBal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

            BigDecimal var = new BigDecimal(Constants.C2C_TRANS_AMOUNT);

            BigDecimal Balance = DBAssertion.prePayerBal.add(var);

            verifyChurnSettlementPage(channeluser, t1);

            Assertion.verifyEqual(Balance, Balance, "The Pre-Balance of IND01 has increased by : " + var, t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0735() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0735", "To verify that the Subsciber can register itself again using previously churn MSISDN of existing user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {


            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            User sub = new User(Constants.SUBSCRIBER);
            Transactions.init(t1).selfRegistrationForSubscriberWithKinDetails(sub);


            //Sending money to user (for settlement)

            Transactions.init(t1).initiateCashIn(sub, whs, new BigDecimal(5));

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(sub, false, true);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successful", t1);
            CommonUserManagement.init(t1).approveRejectChurnInitiate(sub, batchid, true);

            // CommonUserManagement.init(t1).initiateChurnSettlement(channeluser, "Channel User");

            CommonUserManagement.init(t1).initiateApproveChurnSettlement(sub, subsReceiver);


            User sub1 = new User(Constants.SUBSCRIBER);
            sub1.setMSISDN(sub.MSISDN);
            Transactions.init(t1).selfRegistrationForSubscriberWithKinDetails(sub1);
            t1.pass("Subsciber can register itself again using previously churn MSISDN of existing user.");


        } catch (Exception e) {

            markTestAsFailure(e, t1);

        }

        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0736() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0736", "To verify that the Subscriber can be churned by Bulk file upload.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {

            //Report Done

            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            User sub = new User(Constants.SUBSCRIBER);
            Transactions.init(t1).selfRegistrationForSubscriberWithKinDetails(sub);

            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(channeluser, true, false);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

            // ChannelUserManagement.init(t1).churnInitiateAndApprove(channeluser);
            CommonUserManagement.init(t1).approveRejectChurnInitiate(channeluser, batchid, true);
            churnRecordList = new ArrayList<>();

            churnRecord = new Suite_v5_ChurnRecord(sub, false, true);
            churnRecordList.add(churnRecord);

            String batchid2 = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

            /*ChannelUserManagement.init(t1)
                    .churnInitiateAndApprove(sub);*/
            OperatorUser networkAdmin = DataFactory.getOperatorUserWithAccess("CHURN_APPROVE");
            Login.init(t1).login(networkAdmin);
            CommonUserManagement.init(t1)
                    .approveRejectChurnInitiate(sub, batchid2, true);


        } catch (Exception e) {

            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0738() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0738", "To verify that the Wholesaler should be able to perform Batch Reject.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {

            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            /*User sub = new User(Constants.SUBSCRIBER);
            Transactions.init(t1).selfRegistrationForSubscriberWithKinDetails(sub);*/

            //Creating user to churn
            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            //Sending money to user (for settlement)

            //TransactionManagement.init(t1).makeSureChannelUserHasBalance(payer);
            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(channeluser, true, false);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);
            CommonUserManagement.init(t1).approveRejectChurnInitiate(channeluser, batchid, false);


        } catch (Exception e) {

            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0740() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0740", "To verify that the After churn all wallet money of user should transfer to operator Churn wallet account and any user can do settlement of that using O2S service.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {
            User payer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER);
            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();


            //Creating user to churn
            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            //Sending money to user (for settlement)


            //TransactionManagement.init(t1).makeSureChannelUserHasBalance(payer);
            Login.init(t1).login(payer);

            TransactionManagement.init(t1).performC2C(channeluser, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(channeluser, true, false);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

            CommonUserManagement.init(t1).approveRejectChurnInitiate(channeluser, batchid, true);

            CommonUserManagement.init(t1).initiateChurnSettlement(channeluser, "Channel User");

            verifyChurnSettlementPage(channeluser, t1);

        } catch (Exception e) {

            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0743() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0743", "To verify that the System user is able to select the wallet which needs to be churned, after user clicks on submit button then next screen should so available balance.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {

            //Report Done
            User payer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER);
            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();


            //Creating user to churn
            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            Login.init(t1).login(payer);

            TransactionManagement.init(t1).performC2C(channeluser, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(channeluser, true, false);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.CHURN_APPROVAL);

            ChurnUser_Page1 page = ChurnUser_Page1.init(t1);

            page.navUserChurnApproval()
                    .selectBatchId(batchid)
                    .checkForApprovalBySelection();

            page.clickOnSubmitBtn();

            Assertion.verifyContains(page.getWalletBalance(), Constants.C2C_TRANS_AMOUNT, "Verify Wallet Balance", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1104() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1104", "To verify that the Operator user should able to Churn Users via Bulk Upload Method.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);
        try {


            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            User sub = new User(Constants.SUBSCRIBER);
            Transactions.init(t1).selfRegistrationForSubscriberWithKinDetails(sub);


            //Sending money to user (for settlement)

            Transactions.init(t1).initiateCashIn(sub, whs, new BigDecimal(5));

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(sub, false, true);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

        } catch (Exception e) {

            markTestAsFailure(e, t1);

        }

        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1112() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1112", "To verify that the Operator should able to churned Suspend users also.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);
        try {


            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            User sub = new User(Constants.SUBSCRIBER);
            Transactions.init(t1).selfRegistrationForSubscriberWithKinDetails(sub);


            //Sending money to user (for settlement)

            Transactions.init(t1).initiateCashIn(sub, whs, new BigDecimal(5));

            Login.init(t1).login(optUser);

            SubscriberManagement.init(t1).suspendSubscriber(subsReceiver);

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(sub, false, true);

            churnRecordList.add(churnRecord);
            CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

        } catch (Exception e) {

            markTestAsFailure(e, t1);

        }

        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1113() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1113", "To verify that the Operator User should able to churned users also who have not set their PIN after registration.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);
        try {

            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            //Creating Wholesaler without changing pin

            User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMappingWithoutChangingTpin(whs, false);


            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(whs, true, false);

            churnRecordList.add(churnRecord);
            CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

        } catch (Exception e) {

            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN})
    public void TC_ECONET() throws Exception {
        ExtentTest t1 = pNode.createNode("1271", "To verify that the If subscriber" +
                " is member of savings club then subscriber  shouldn't get churned.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN);
        String defaultPref = null;

        try {
            defaultPref = MobiquityGUIQueries
                    .fetchDefaultValueOfPreference("Bank Creation for Savings Club");

            if (AppConfig.isBankMandatoryForSavingsClub) {
                SystemPreferenceManagement.init(t1)
                        .updateSystemPreference("Bank Creation for Savings Club", "FALSE");
            }

            /*OperatorUser naAddClub = DataFactory.getOperatorUserWithAccess("ADD_CLUB");
            User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);*/

            User subsChairmanUI = new User(Constants.SUBSCRIBER);
            User subsMember1 = new User(Constants.SUBSCRIBER);
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);
            SavingsClub sClub;

            SubscriberManagement.init(t1)
                    .createSubscriberDefaultMapping(subsChairmanUI,
                            true, false);

            // Map SVC wallet Preference
            Preferences.init(t1)
                    .setSVCPreferences(subsChairmanUI, bankId);

            sClub = new SavingsClub(subsChairmanUI,
                    Constants.CLUB_TYPE_PREMIUM, bankId, false, false);

            sClub.MinMemberCount = 1;
            sClub.MinApproverCount = 1;

            SavingsClubManagement.init(t1)
                    .createSavingsClub(sClub);

            SubscriberManagement.init(t1)
                    .createSubscriberDefaultMapping(subsMember1, true, false);

            //Add Member to the Club Object
            sClub.addMember(subsMember1);

            // make sure that Users are joined and are active
            Transactions.init(t1)
                    .joinOrResignSavingClub(sClub, subsMember1, true)
                    .verifyStatus(Constants.TXN_SUCCESS);

            CommonUserManagement.init(t1)
                    .startNegativeTest()
                    .churnInitiateUser(subsMember1);

        } catch (Exception e) {

            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("Bank Creation for Savings Club", defaultPref);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1115_C() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1115_C", "To verify that the Operator user should able to Churn Suspended user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);
        try {


            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            User sub = new User(Constants.SUBSCRIBER);
            Transactions.init(t1).selfRegistrationForSubscriberWithKinDetails(sub);


            //Sending money to user (for settlement)
            OperatorUser usrSuspResume = DataFactory.getOperatorUserWithAccess("SR_USR", Constants.NETWORK_ADMIN);
            Login.init(t1).login(usrSuspResume);

            SubscriberManagement.init(t1).suspendSubscriber(sub);


            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(sub, false, true);

            churnRecordList.add(churnRecord);
            CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

        } catch (Exception e) {

            markTestAsFailure(e, t1);

        }

        Assertion.finalizeSoftAsserts();

    }
}