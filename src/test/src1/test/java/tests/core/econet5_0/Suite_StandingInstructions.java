package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_StandingInstructions extends TestInit {
    private User sub1, sub2, chUser;
    private OperatorUser optUser1;
    private ServiceCharge standingInstructionsService;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {

        ExtentTest setup = pNode.createNode("Pre-Condition,Configuring Non-Financial Service Charges");
        try {

            // fetch existing Users from App Data
            sub1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            sub2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUser1 = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);

            // Configuring non-financial services charge
            standingInstructionsService = new ServiceCharge(Services.ENABLE_STANDING_INSTRUCTIONS_BY_SUBS, sub1,
                    optUser1, null, null, null, null);

            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceCharge(standingInstructionsService);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.ECONET_UAT_5_0, FunctionalTag.STANDING_INS})

    public void TUNG48985() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0397 ", "To verify that the Customer " +
                "should able to pay by SI scheduled transactions method initiated by Self.");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.ECONET_UAT_5_0, FunctionalTag.STANDING_INS);
        try {
            Transactions.init(t1).enableStandardInstructionsBySubscriber(sub1, sub2, "20", "DAILY")
                    .verifyStatus(Constants.TXN_SUCCESS);

            Transactions.init(t1).viewListOfBeneficiaryForADSI(sub1, "SI")
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyBeneficiaryMsisdn(sub2.MSISDN)
                    .verifyDebitAmount("20.00");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @AfterClass(alwaysRun = true)
    public void post_Condition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Disable Standing instructions");
        try {

            Transactions.init(eSetup).disableStandardInstructionsBySubscriber(sub1, sub2)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyMessage("si.disable");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }
}