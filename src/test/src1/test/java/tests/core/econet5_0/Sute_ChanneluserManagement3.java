package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

//** created by Charan Teja from pvg Team**//

public class Sute_ChanneluserManagement3 extends TestInit {
    private static OperatorUser usrCreator, usrView, usrApprover, delChUser, delChUserApprover, suspendChUsr, suspendChUsrApprove, userModify, userModifyApprover, resumeChUsr, resumeChUsrApprove;
    private static OperatorUser bulkCreate, bulkApprove, OpUser, emailnotifier;
    private static ExtentTest tearDown;
    private User chUser, chUserRet;


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0871() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0871 ", "To verify that Each payment instrument will have its own grade, TCP & mobile Group Role assigned to it.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {

            User chuser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).initiateAndApproveChUser(chuser, false);
            CommonUserManagement.init(t1).changeFirstTimePassword(chuser);


        } catch (Exception e) {

        }
    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0852() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0852 ", "To Verify that after successful creation of channel user Each Channel User(Merchant Domain) when created will be assigned an Merchant code (or Agent code). This code can be used in transactions for single step transactions like Merchant Payment");
        ExtentTest t2 = pNode.createNode("TC_ECONET_0874 ", "To verify that If user clicks on submit without selecting any fields then no bank accounts are added");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {

            User chuser = new User(Constants.MERCHANT);
            ChannelUserManagement.init(t2).createChannelUserDefaultMapping(chuser, false);
            String agentCode = MobiquityGUIQueries.getAgentCode(chuser.MSISDN);
            t1.pass(agentCode + "has been generated while creating Merchant");

        } catch (Exception e) {

        }
    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0872() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0872 ", "To verify that By default, commission mWallet get auto associated with channel users during its creation");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);


        try {
            Login.init(t1).login(usrCreator);

            User chusr = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(chusr, false);
            String txn = TransactionManagement.init(t1)
                    .initiateO2CForSpecificWallet(chUser, DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName, GlobalData.defaultProvider.ProviderId,
                            Constants.MIN_O2C_AMOUNT, "5646");
            TransactionManagement.init(t1).o2cApproval1(txn);
            UsrBalance walletbalance = MobiquityGUIQueries.getUserBalance(chUser, DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletId, null);
            int walletal1 = walletbalance.Balance.intValue();


            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            e.printStackTrace();
            markTestAsFailure(e, t1);

        }
    }

    @Test(priority = 4, groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0877() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0877 ", "To verify that the mobiquity supports the following three document upload options:\n" +
                "o National Id Proof" +
                "o Address Proof" +
                "o Photo Id Proof");

        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            OperatorUser usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.NETWORK_ADMIN);
            Login.init(t1).login(usrCreator);
            AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(t1);

            pageOne.navAddChannelUser();

            boolean status = Utils.checkElementPresent("confirm2_confirmAddChannelUser_doc1", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "o National Id Proof", t1);

            status = Utils.checkElementPresent("confirm2_confirmAddChannelUser_doc2", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "o Address Proof", t1);

            status = Utils.checkElementPresent("confirm2_confirmAddChannelUser_doc3", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "o Photo Id Proof", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}






















