package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.ChangePasswordPage;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

public class Suite_AuthenticationManagement extends TestInit {

    private User whs;
    private OperatorUser opt;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "AuthenticationManagement");

        whs = new User(Constants.WHOLESALER);
        ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(whs, false);
        opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
    }

    public void checkPre(User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getWalletBalance(Constants.WALLET_101);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payee) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getWalletBalance(Constants.WALLET_101);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.LOGIN, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0771() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0771", "To verify that System user(Network Admin) is able to modify the login ID or password of another system user(Wholesaler) of the lower hierarchy.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.LOGIN, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            //Login as Network admin and change password for channel user by giving invalid password
            startNegativeTestWithoutConfirm();
            ChannelUserManagement.init(t1).modifyChannelUserLoginIdPassword(whs, whs.LoginId, "com135");
            Assertion.verifyErrorMessageContain("changePassword.error.ispasswordsComplex", "Password functionality check", t1, "6", "8");
            stopNegativeTestWithoutConfirm();

            //Login as Network admin and change the loginId & password for channel user
            ChannelUserManagement.init(t1).modifyChannelUserLoginIdPassword(whs, whs.LoginId, ConfigInput.userCreationPassword);
            ChannelUserManagement.init(t1).modifyUserApproval(whs);

            ChangePasswordPage changPwd = ChangePasswordPage.init(t1);
            FunctionLibrary fl = new FunctionLibrary(driver);

            Login.init(t1).login(whs.LoginId, ConfigInput.userCreationPassword);
            fl.contentFrame();

            //After changing the passowrd earlier it was asking for first time password rest but in the latest build this page is not displayed
           /* changPwd.setOldPassword(ConfigInput.userCreationPassword);
            changPwd.setNewPassword(whs.Password);
            changPwd.setConfirmPassword(whs.Password);
            Thread.sleep(3000);
            changPwd.clickSubmit();

            Assertion.assertMessageContain(changPwd.getMessageResetPwd(), "changePassword.label.success", "Change User Password", t1);
*/
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0617_0138() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0617", "To verify that the User account should " +
                "get locked after incorect attempts(no. of attempts configured in the system).");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            OperatorUser opt = new OperatorUser(Constants.NETWORK_ADMIN);
            OperatorUserManagement.init(t1).createAdmin(opt);

            Login.init(t1).performLogout();

            startNegativeTest();
            Login.init(t1).tryLogin(opt.LoginId, ConfigInput.userCreationPassword);
            Login.init(t1).tryLogin(opt.LoginId, ConfigInput.userCreationPassword);
            Login.init(t1).tryLogin(opt.LoginId, ConfigInput.userCreationPassword);

            Assertion.verifyErrorMessageContain("incorrect.password",
                    "User is blocked on invalid login attempt", t1);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0138", "To verify that if operator is barred" +
                    " then Operator will not be able to Login into his Web Portal. ");
            t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

            startNegativeTest();
            Login.init(t2).tryLogin(opt.LoginId, ConfigInput.defaultPass);
            Assertion.verifyErrorMessageContain("user.blocked", "User Blocked", t2);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0136() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0136", "To verify that if user is barred as sender then user will be able to receive financial transactions across any bearer.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User whs = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER,
                    Constants.BAR_AS_SENDER, null);

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(opt);
            // ChannelUserManagement.init(t1).barChannelUser(whs,Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_SENDER);

            checkPre(whs);

            TransactionManagement.init(t1).initiateAndApproveO2C(whs, Constants.MIN_O2C_AMOUNT, "Test");

            ChannelUserManagement.init(t1).unBarChannelUser(whs, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_SENDER);

            checkPost(whs);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCESS_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UTA_0130_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0130_a", "To verify that Network Admin can reset the password of Channel User.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCESS_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            //reset the password
            CommonUserManagement.init(t1).resetPasswordAsNetworkAdmin(whs);

            //fetch password from db
            String actualpassword = MobiquityGUIQueries.getMobiquityUserMessage(whs.MSISDN, "desc");

            /*//decrypt the password
            DesEncryptor d = new DesEncryptor();
            String decryptedPassword = d.decrypt(actualpassword);*/

            //verify  password
            Assertion.verifyContains(actualpassword, "Your password has been reinitialised to: 000000.Please Change your password before login.", "Check DB Message", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCESS_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UTA_0130_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0130_b", "To verify that Super admin can reset the password of Operator User.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCESS_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            OperatorUser opt = new OperatorUser(Constants.NETWORK_ADMIN);
            OperatorUserManagement.init(t1).createAdmin(opt);

            //reset the password
            CommonUserManagement.init(t1).resetPasswordAsSuperAdmin(opt);

            //fetch password from db
            String actualpassword = MobiquityGUIQueries.getMobiquityUserMessage(opt.MSISDN, "desc");

            /*//decrypt the password
            DesEncryptor d = new DesEncryptor();
            String decryptedPassword = d.decrypt(actualpassword);
*/
            //verify  password
            Assertion.verifyContains(actualpassword, "Your password has been reinitialised to: 000000.Please Change your password before login.", "Check DB Message", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.FORCED_PASSWORD_CHANGE_PROCEDURE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0131() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0131", "To verify that when the user is logged in for " +
                "first time then  the system generates the password and is sent to him via SMS then upon login, the system " +
                "will ask the user to reset his/her password as per his choice. ");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.FORCED_PASSWORD_CHANGE_PROCEDURE, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User whs_01 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).initiateAndApproveChUser(whs_01, false);

            //User whs_01 = DataFactory.getUserUsingMsisdn("7711132302");
            List<String> actualPwd = MobiquityGUIQueries.getMobiquityUserMessageList(whs_01.MSISDN, "desc");

            //decrypt the password
            DesEncryptor d = new DesEncryptor();

            for (String msg : actualPwd) {
                String password = d.decrypt(msg);
                if (password.contains(ConfigInput.userCreationPassword)) {
                    String pwd = password.split("Password:")[1].trim().split("\\s")[0];
                    Assertion.verifyEqual(pwd, ConfigInput.userCreationPassword, "Password sent in SMS", t1);
                    break;
                }
            }

            //CommonUserManagement.init(t1).changeFirstTimePassword(whs_01);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0775_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0775_a", "To verify that if channel user(Retailer) is barred as receiver then user is able to perform non- financial transactions across any bearer that includes service charge and commission as well.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User rt = new User(Constants.RETAILER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(rt, false);

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(rt);

            ChannelUserManagement.init(t1).barChannelUser(rt, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_RECIEVER);

            ServiceCharge balance = new ServiceCharge(Services.BALANCE_ENQUIRY, rt, opt,
                    null, null, null, null);

            balance.setNFSCServiceCharge("2");
            balance.setNFSCCommission("2");

            ServiceChargeManagement.init(t1).deleteNFSChargeForSpecificWallet(balance);

            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balance);

            //Non-Financial
            TxnResponse res1 = Transactions.init(t1).BalanceEnquiry(rt);
            res1.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0775_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0775_b", "To verify that if channel " +
                "user(Wholesaler) is barred as Sender then user is able to perform non- financial " +
                "transactions across any bearer if service charge is more than 0");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

           /* User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs,false);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs);

            Login.init(t1).login(opt);
            ChannelUserManagement.init(t1).barChannelUser(whs,Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_SENDER);
            */

            User whs = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs);

            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(opt);

            ServiceCharge balance = new ServiceCharge(Services.BALANCE_ENQUIRY, whs, opt,
                    null, null, null, null);

            ServiceChargeManagement.init(t1)
                    .deleteNFSChargeForSpecificWallet(balance);

            //define service charge with more than zero
            balance.setNFSCServiceCharge("2");
            balance.setNFSCCommission("2");
            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balance);

            //Non-Financial
            TxnResponse res1 = Transactions.init(t1).BalanceEnquiry(whs);
            res1.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0775_c() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0775_c", "To verify that if Subscriber is barred as receiver then user is able to perform non- financial transactions across any bearer that includes service charge and commission as well.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            Login.init(t1).login(opt);
            SubscriberManagement.init(t1).barSubscriber(subs, Constants.BAR_AS_RECIEVER);

            ServiceCharge balance = new ServiceCharge(Services.BALANCE_ENQUIRY, subs, opt,
                    null, null, null, null);

            balance.setNFSCServiceCharge("2");
            balance.setNFSCCommission("2");

            ServiceChargeManagement.init(t1).deleteNFSChargeForSpecificWallet(balance);

            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balance);

            checkPre(subs);

            //Non-Financial
            TxnResponse res1 = Transactions.init(t1).subscriberBalanceEnquiry(subs);
            res1.verifyStatus(Constants.TXN_SUCCESS);

            checkPost(subs);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

}
