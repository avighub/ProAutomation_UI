package tests.core.econet5_0;

import framework.entity.OperatorUser;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 9/25/2018.
 */
public class Suite_OperatorUseraManagement extends TestInit {
    private static OperatorUser netAdmin, bankAdmin;

    @BeforeClass(alwaysRun = true)
    public static void bc() throws Exception {
        netAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        bankAdmin = DataFactory.getOperatorUserWithCategory(Constants.BANK_ADMIN);
    }


    @Test(enabled = false, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0146_a() throws Exception {
       /* ExtentTest t1 = pNode.createNode("TC_ECONET_0146_a", "To verify that the No user other than superadmin can Modify  Operator User Network admin");
        Login.init(t1).login(DataFactory.getOperatorUserWithCategory(Constants.BANK_ADMIN));
        AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(t1);
        p1.NavigateNA();
        WebElement e = driver.findElement(By.id("add_addSystemParty_partyTypeId"));
        Select option = new Select(e);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("O2CTRF_ALL", "O2CTRF_O2CINT001");*/
    }

}
