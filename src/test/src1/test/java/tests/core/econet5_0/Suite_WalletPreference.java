package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.entity.WalletPreference;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 10/4/2018.
 */
public class Suite_WalletPreference extends TestInit {
    private User subscriber;
    private OperatorUser netAdmin;
    private WalletPreference wPref;

    @BeforeClass(alwaysRun = true)
    private void setup() throws Exception {
        subscriber = new User(Constants.SUBSCRIBER);
        netAdmin = DataFactory.getOperatorUserWithAccess("CAT_PREF");
        wPref = new WalletPreference(Constants.SUBSCRIBER,
                Constants.REGTYPE_SUBS_INIT_USSD,
                DataFactory.getDefaultWallet().WalletName, true,
                null, subscriber.GradeName);
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_PREFERENCES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0586() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0586", "To verify that the Valid User(Network admin) should be able to define all grades and wallet preferences for Non-Econet Subscribers.");

        try {
            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_PREFERENCES, FunctionalTag.ECONET_UAT_5_0);
            t1.assignAuthor(Author.PRASHANT);

            Login.init(t1).login(netAdmin);
            Preferences.init(t1).configureWalletPreferences(wPref);

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }

        //New TEST
        ExtentTest t2 = pNode.createNode("TC_ECONET_0587", "To verify that the Valid User(Wholesaler) should be able to Register Non-Econet Subscriber.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_PREFERENCES, FunctionalTag.ECONET_UAT_5_0);

        try {
            Transactions.init(t2).selfRegistrationForSubscriber(subscriber);

            String actualTCPID = MobiquityGUIQueries.getUserTCPName(subscriber);
            String actualGrade = MobiquityGUIQueries.dbGetUserGrade(subscriber);

            Assertion.verifyEqual(actualTCPID, wPref.TCP, "Verify Default TCP Applied Or not", t2, false);
            Assertion.verifyEqual(actualGrade, wPref.GradeName, "Verify Default GradeName Applied Or not", t2, false);

            String actualMobileGroupRole = MobiquityGUIQueries.dbGetSubsMobileGroupRole(subscriber);
            Assertion.verifyEqual(actualMobileGroupRole, wPref.mobileRole, "Verify Default Mobile Group  Applied Or not", t2, false);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.finalizeSoftAsserts();
        }

    }

}
