package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.walletPreference.SystemPreference_page1;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public class Suite_PinManagement extends TestInit {

    private OperatorUser usrCreator, usrApprover, opt;
    private User chAddSubs, chApproveSubs;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "PinManagement");

        usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");

        chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
        chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");

        opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

    }

    public void checkPre(User payer) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getWalletBalance(Constants.WALLET_103);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payer) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getWalletBalance(Constants.WALLET_103);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    /**
     * Removed Econet UAT Tag. This service is out of Scope
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT,FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_UAT_0614() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0614", "To verify that the Option of " +
                "‘Different PIN values for mPIN and tPIN per user’ is not checked, then user can set the tPIN" +
                " value same as mPIN.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("ENABLE_DIFF_TPIN_AND_MPIN");
            if (prefValue.equalsIgnoreCase("TRUE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("ENABLE_DIFF_TPIN_AND_MPIN", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("ENABLE_DIFF_TPIN_AND_MPIN", "FALSE");
            }

            User whs = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(whs);
            Login.init(t1).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(whs);

            TxnResponse res1 = Transactions.init(t1).changeChannelUserMPin(whs, ConfigInput.mPin);
            res1.verifyStatus(Constants.TXN_SUCCESS);
            TxnResponse res2 = Transactions.init(t1).changeChannelUserTPin(whs, ConfigInput.mPin);
            res2.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_SIT_0772_a() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0772_a", "To verify that the default PIN can be of a 4-digit numeric value for Channel user(Wholesaler)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            String defaultPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("USE_DEFAULT_PIN");
            String randomPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_RANDOM_PIN_ALLOW");

            if (defaultPin.equalsIgnoreCase("N")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("USE_DEFAULT_PIN", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("USE_DEFAULT_PIN", "Y");

            }
            if (randomPin.equalsIgnoreCase("Y")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_RANDOM_PIN_ALLOW", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("IS_RANDOM_PIN_ALLOW", "N");

            }

            User whs = new User(Constants.WHOLESALER);
            Login.init(pNode).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(whs);
            Login.init(pNode).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(whs);

            //User whs=DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //fetch password from db
            List<String> messages = MobiquityGUIQueries.getMobiquityUserMessageList(whs.MSISDN, "asc");

            //decrypt the password
            DesEncryptor d = new DesEncryptor();
            String decryptedPassword = d.decrypt(messages.get(0));
            String defaultMpin = decryptedPassword.split("mPIN: ")[1].split(" ")[0].trim();
            t1.info("Default MPIN is :" + defaultMpin);

            String decryptedPassword1 = d.decrypt(messages.get(1));
            String defaultTpin = decryptedPassword1.split("PIN:")[1].split(" ")[0].trim();
            t1.info("Default TPIN is :" + defaultTpin);

            //MPIN validation
            try {
                int mPin = Integer.parseInt(defaultMpin);
                t1.pass("Default Mpin has only numeric values");
            } catch (NumberFormatException e) {
                t1.fail("Default Mpin has non-numeric values");
            }

            char[] mpin = defaultMpin.toCharArray();
            Assertion.verifyEqual(mpin.length, 4, "Default Mpin is 4 digit numeric value", t1);

            //TPIN validation
            try {
                int tPin = Integer.parseInt(defaultTpin);
                t1.pass("Default Tpin has only numeric values");
            } catch (NumberFormatException e) {
                t1.fail("Default Tpin has non-numeric values");
            }

            char[] tpin = defaultTpin.toCharArray();
            Assertion.verifyEqual(tpin.length, 4, "Default Mpin is 4 digit numeric value", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_SIT_0772_b() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0772_b", "To verify that the default PIN can be of a 4-digit numeric value for Subscriber");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            String defaultPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("USE_DEFAULT_PIN");
            String randomPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_RANDOM_PIN_ALLOW");

            if (defaultPin.equalsIgnoreCase("N")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("USE_DEFAULT_PIN", "Y");
                SystemPreferenceManagement.init(t1).updateSystemPreference("USE_DEFAULT_PIN", "Y");
            }
            if (randomPin.equalsIgnoreCase("Y")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_RANDOM_PIN_ALLOW", "Y");
                SystemPreferenceManagement.init(t1).updateSystemPreference("IS_RANDOM_PIN_ALLOW", "N");
            }

            User subs = new User(Constants.SUBSCRIBER);
            Login.init(t1).login(chAddSubs);
            SubscriberManagement.init(t1).addSubscriber(subs, false);
            Login.init(t1).login(chApproveSubs);
            SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);
            Thread.sleep(2000);

            //fetch password from db
            List<String> messages = MobiquityGUIQueries.getAllMessageFromSentSMS(subs.MSISDN, "asc");

            //decrypt the password
            String decryptedPassword = Utils.decryptMessage(messages.get(0));

            String defaultMpin = decryptedPassword.split("mPin ")[1].split(" ")[0].trim();
            t1.info("Default MPIN is :" + defaultMpin);

            String decryptedPassword1 = Utils.decryptMessage(messages.get(1));
            String defaultTpin = decryptedPassword1.split("PIN:")[1].split(" ")[0].trim();
            t1.info("Default TPIN is :" + defaultTpin);

            //MPIN validation
            try {
                int mPin = Integer.parseInt(defaultMpin);
                t1.pass("Default Mpin has only numeric values: " + mPin);
            } catch (NumberFormatException e) {
                t1.fail("Default Mpin has non-numeric values");
            }

            char[] mpin = defaultMpin.toCharArray();
            Assertion.verifyEqual(mpin.length, 4, "Default Mpin is 4 digit numeric value", t1);

            //TPIN validation
            try {
                int tPin = Integer.parseInt(defaultTpin);
                t1.pass("Default Tpin has only numeric values : " + tPin);
            } catch (NumberFormatException e) {
                t1.fail("Default Tpin has non-numeric values");
            }

            char[] tpin = defaultTpin.toCharArray();
            Assertion.verifyEqual(tpin.length, 4, "Default Mpin is 4 digit numeric value", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_SIT_0772_c() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0772_c", "To verify that the system can be configured to generate random 4 digit PIN for Subscriber");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            String defaultPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("USE_DEFAULT_PIN");
            String randomPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_RANDOM_PIN_ALLOW");

            HashMap<String, String> pref = new HashMap<>();
            pref.put("USE_DEFAULT_PIN", "N");
            pref.put("IS_RANDOM_PIN_ALLOW", "Y");

            if (defaultPin.equalsIgnoreCase("Y") || randomPin.equalsIgnoreCase("N")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("USE_DEFAULT_PIN", "Y");
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_RANDOM_PIN_ALLOW", "Y");

                SystemPreferenceManagement.init(t1).updateMultipleSystemPreference(pref);
            }

            User subs = new User(Constants.SUBSCRIBER);
            Login.init(t1).login(chAddSubs);
            SubscriberManagement.init(t1).addSubscriber(subs, false);
            Login.init(t1).login(chApproveSubs);
            SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);
            Thread.sleep(2000);

            //fetch password from db
            List<String> messages = MobiquityGUIQueries.getAllMessageFromSentSMS(subs.MSISDN, "asc");

            //decrypt the password
            DesEncryptor d = new DesEncryptor();
            String decryptedPassword = d.decrypt(messages.get(0));
            String randomMpin = decryptedPassword.split("mPin ")[1].split(" ")[0].trim();
            t1.info("Random MPIN is :" + randomMpin);

            String decryptedPassword1 = d.decrypt(messages.get(1));
            String randomTpin = decryptedPassword1.split("PIN:")[1].split(" ")[0].trim();
            t1.info("Random TPIN is :" + randomTpin);

            //MPIN validation
            Assertion.verifyNotContains(randomMpin, "0000", "Random Mpin is not equal to default pin value", t1);

            try {
                int mPin = Integer.parseInt(randomMpin);
                t1.pass("Default Mpin has only numeric values");
            } catch (NumberFormatException e) {
                t1.fail("Default Mpin has non-numeric values");
            }

            char[] mpin = randomMpin.toCharArray();
            Assertion.verifyEqual(mpin.length, 4, "Default Mpin is 4 digit numeric value", t1);

            //TPIN validation

            Assertion.verifyNotContains(randomTpin, "0000", "Random Tpin is not equal to default pin value", t1);

            try {
                int tPin = Integer.parseInt(randomTpin);
                t1.pass("Default Tpin has only numeric values");
            } catch (NumberFormatException e) {
                t1.fail("Default Tpin has non-numeric values");
            }

            char[] tpin = randomTpin.toCharArray();
            Assertion.verifyEqual(tpin.length, 4, "Default Mpin is 4 digit numeric value", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {

            HashMap<String, String> pref = new HashMap<>();
            pref.put("USE_DEFAULT_PIN", "Y");
            pref.put("IS_RANDOM_PIN_ALLOW", "N");
            SystemPreferenceManagement.init(t1).updateMultipleSystemPreference(pref);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, enabled = false, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0772_d() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0772_d", "To verify that the system can be configured to generate random 4 digit PIN for channel user(Whoelsaler)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            String defaultPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("USE_DEFAULT_PIN");
            String randomPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_RANDOM_PIN_ALLOW");

            HashMap<String, String> pref = new HashMap<>();
            pref.put("USE_DEFAULT_PIN", "N");
            pref.put("IS_RANDOM_PIN_ALLOW", "Y");

            if (defaultPin.equalsIgnoreCase("Y") || randomPin.equalsIgnoreCase("N")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("USE_DEFAULT_PIN", "Y");
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_RANDOM_PIN_ALLOW", "Y");

                SystemPreferenceManagement.init(t1).updateMultipleSystemPreference(pref);
            }

            User whs = new User(Constants.WHOLESALER);
            Login.init(pNode).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(whs);
            Login.init(pNode).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(whs);

            //fetch password from db
            List<String> messages = MobiquityGUIQueries.getAllMessageFromSentSMS(whs.MSISDN, "asc");

            //decrypt the password
            DesEncryptor d = new DesEncryptor();
            String decryptedPassword = d.decrypt(messages.get(0));
            String randomMpin = decryptedPassword.split("mPIN: ")[1].split(" ")[0].trim();
            t1.info("Random MPIN is :" + randomMpin);

            String decryptedPassword1 = d.decrypt(messages.get(1));
            String randomTpin = decryptedPassword1.split("PIN:")[1].split(" ")[0].trim();
            t1.info("Random TPIN is :" + randomTpin);

            //MPIN validation
            Assertion.verifyNotContains(randomMpin, "0000", "Random Mpin is not equal to default pin value", t1);

            try {
                int mPin = Integer.parseInt(randomMpin);
                t1.pass("Default Mpin has only numeric values");
            } catch (NumberFormatException e) {
                t1.fail("Default Mpin has non-numeric values");
            }

            char[] mpin = randomMpin.toCharArray();
            Assertion.verifyEqual(mpin.length, 4, "Default Mpin is 4 digit numeric value", t1);

            //TPIN validation

            Assertion.verifyNotContains(randomTpin, "0000", "Random Tpin is not equal to default pin value", t1);

            try {
                int tPin = Integer.parseInt(randomTpin);
                t1.pass("Default Tpin has only numeric values");
            } catch (NumberFormatException e) {
                t1.fail("Default Tpin has non-numeric values");
            }

            char[] tpin = randomTpin.toCharArray();
            Assertion.verifyEqual(tpin.length, 4, "Default Mpin is 4 digit numeric value", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0773_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0773_a", "To verify that whenever channel user" +
                "(Wholesaler) is trying to access any financial or non-financial transaction where amount is getting " +
                "debited then user(Wholesaler) will be prompted for tPIN.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs);

            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //Financial
            SfmResponse res = Transactions.init(t1).initiateCashInwithInvalidTpin(subs, whs, "50", "");
            res.verifyMessage("tpin.missing.error");

            //Non-Financial
            TxnResponse res1 = Transactions.init(t1).BalanceEnquiryWithInvalidMpinTpin(whs, ConfigInput.mPin, "");
            res1.verifyMessage("sender.tpin.missing");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0773_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0773_a", "To verify that whenever Subscriber is " +
                "trying to access any financial or non-financial transaction where amount is getting debited then " +
                "subscriber will be prompted for tPIN.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs);

            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //Financial
            SfmResponse res = Transactions.init(t1).performCashOutWithInvalidSubsMpinTpin(subs, whs, new BigDecimal(5), ConfigInput.mPin, "");
            res.verifyMessage("tpin.missing.error");

            //Non-Financial
            TxnResponse res1 = Transactions.init(t1).subscriberBalanceEnquiryWithInvalidMpinTipn(subs, ConfigInput.mPin, "");
            res1.verifyMessage("sender.tpin.missing");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0133_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0133_a", "To verify that newly registered " +
                "channel user(Wholesaler) cannot perform any services via USSD, if pin is not changed even once .");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA).assignAuthor(Author.PUSHPALATHA);

        try {

            User whs = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(whs);
            Login.init(t1).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(whs);

            CommonUserManagement.init(t1)
                    .changeFirstTimePassword(whs);

            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //Perform transaction to get error for MPIN not changed after registration
            SfmResponse res = Transactions.init(t1).initiateCashInwithInvalidTpin(subs, whs, Constants.MIN_CASHIN_AMOUNT, ConfigInput.tPin);
            res.verifyMessage("003590");

            //Change channel users MPIN
            Transactions.init(t1).changeChannelUserMPin(whs);

            //Perform transaction to get error for TPIN not changed after registration
            SfmResponse res1 = Transactions.init(t1).initiateCashInwithInvalidTpin(subs, whs, Constants.MAX_CASHIN_AMOUNT, ConfigInput.tPin);
            res1.verifyMessage("tpin.not.changed");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0133_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0133_b", "To verify that newly registered " +
                "Subscriber cannot perform any services via USSD, if pin is not changed even once .");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User subs = new User(Constants.SUBSCRIBER);
            ConfigInput.changePin = false;
            SubscriberManagement.init(t1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subs, GlobalData.defaultWallet.WalletId, false);

            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //Perform transaction to get error for MPIN not changed after registration
            SfmResponse res = Transactions.init(t1).performCashOutWithInvalidSubsMpinTpin(subs, whs, new BigDecimal(Constants.MIN_CASHIN_AMOUNT), ConfigInput.mPin, ConfigInput.tPin);
            res.verifyMessage("003590");

            //Change Subscriber MPIN
            Transactions.init(t1).changeSubscriberMPin(subs);
/*

            //Perform transaction to get error for TPIN not changed after registration
            SfmResponse res1 = Transactions.init(t1).performCashOutWithInvalidSubsMpinTpin(subs, whs, new BigDecimal(Constants.MAX_CASHIN_AMOUNT), ConfigInput.mPin, ConfigInput.tPin);
            res1.verifyMessage("tpin.not.changed");
*/

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0613() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0613", "To verify that if 2nd level authentication" +
                " PIN per user is enabled then newly registered user will receive two PINs," +
                " namely mPIN and tPIN via two separate SMS.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            boolean pinEnabled = AppConfig.isTPinAuthentication;
            String defaultPin = AppConfig.defaultPin;

            if (pinEnabled) {

                User ssa = new User(Constants.SPECIAL_SUPER_AGENT);
                String[] payIdArr = DataFactory.getPayIdApplicableForSubs();

                CurrencyProviderMapping.init(pNode)
                        .mapWalletPreferencesUserRegistration(ssa, payIdArr);

                Login.init(t1).login(usrCreator);
                ChannelUserManagement.init(t1).addChannelUser(ssa);
                Login.init(t1).login(usrApprover);
                ChannelUserManagement.init(t1).approveChannelUser(ssa);

                List<String> messages = MobiquityGUIQueries.getAllMessageFromSentSMS(ssa.MSISDN, "asc");

                if (messages.size() == 0) {
                    Assertion.markAsFailure("Failed to fetch messages from DB, hence failing the test");
                } else {
                    //decrypt the password
                    DesEncryptor d = new DesEncryptor();
                    String mpin = d.decrypt(messages.get(0));
                    Assertion.verifyContains(mpin, "Channel User add initiated successfully. mPIN: " + defaultPin + " for newly added MFS Providers.", "Verification for MPIN SMS", t1);

                    String tpin = d.decrypt(messages.get(1));
                    Assertion.verifyContains(tpin, "Channel User add initiated successfully. Password:" + ConfigInput.userCreationPassword + " PIN:" + defaultPin + " Login Id:" + ssa.LoginId + "", "Verification for PIN SMS", t1);
                }
            } else {
                t1.warning("Set the preference 'ENABLE_TPIN_AUTHENTICATION' to true to get 2 pins for user");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }


    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT})
    public void TC_ECONET_0618_a_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0618_a", "To verify that following general rule" +
                " apply to Channel User’s PIN (applicable for both mPIN and tPIN): Should not be sequential i.e." +
                " no two consecutive digits should be sequential for example PIN like 2379, 9862 are not allowed.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        ExtentTest t2 = pNode.createNode("TC_ECONET_0618_b", "To verify that following general rule" +
                " apply to Channel User’s PIN (applicable for both mPIN and tPIN): Should not consist of any " +
                "duplicate number i.e. there should not be any repetitive number in the PIN for example PIN like" +
                " 1139, 2452 are not allowed.");
        t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            User whs = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(whs);
            Login.init(t1).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(whs);

            TxnResponse res = Transactions.init(t1).changeChannelUserMPin(whs, "2379");
            res.verifyMessage("pin.sequential.error");

            TxnResponse res1 = Transactions.init(t2).changeChannelUserMPin(whs, "1139");
            res1.verifyMessage("pin.repetitive.error");

            Transactions.init(t1).changeChannelUserMPin(whs, ConfigInput.mPin);

            startNegativeTest();
            TxnResponse res2 = Transactions.init(t1).changeChannelUserTPin(whs, "9862");
            res2.verifyMessage("pin.sequential.error");

            TxnResponse res3 = Transactions.init(t2).changeChannelUserTPin(whs, "2452");
            res3.verifyMessage("pin.repetitive.error");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT})
    public void TC_ECONET_0618_c_d() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0618_c", "To verify that following general rule apply to Subscriber’s PIN (applicable for both mPIN and tPIN): Should not be sequential i.e. no two consecutive digits should be sequential for example PIN like 2379, 9862 are not allowed.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        ExtentTest t2 = pNode.createNode("TC_ECONET_0618_d", "To verify that following general rule apply to Subscriber’s PIN (applicable for both mPIN and tPIN): Should not consist of any duplicate number i.e. there should not be any repetitive number in the PIN for example PIN like 1139, 2452 are not allowed.");
        t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            User subs = new User(Constants.SUBSCRIBER);
            Login.init(t1).login(chAddSubs);
            SubscriberManagement.init(t1).addSubscriber(subs, false);
            Login.init(t1).login(chApproveSubs);
            SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);
            Thread.sleep(2000);

            Transactions.init(t1).subscriberAcquisition(subs);

            TxnResponse res = Transactions.init(t1).changeSubscriberMPin(subs, "2379");
            res.verifyMessage("pin.sequential.error");

            TxnResponse res1 = Transactions.init(t2).changeSubscriberMPin(subs, "1139");
            res1.verifyMessage("pin.repetitive.error");

            Transactions.init(pNode).changeSubscriberMPin(subs, ConfigInput.mPin);

            TxnResponse res2 = Transactions.init(t1).changeSubscriberTPin(subs, "9862");
            res2.verifyMessage("pin.sequential.error");

            TxnResponse res3 = Transactions.init(t2).changeSubscriberTPin(subs, "2452");
            res3.verifyMessage("pin.repetitive.error");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0618() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0618_f", "To verify that following " +
                "general rules apply to user’s PIN (applicable for both mPIN and tPIN):Due to security reasons, " +
                "value of DEFAULT_PIN and DEFAULT_PASSWORD will not be displayed on preferences screen. However, " +
                "value of these preferences can be modified.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {


/*String pin = SystemPreferenceManagement.init(t1).getSystemPreferenceValue("DEFAULT_RESET_PIN");

            //Assertion.verifyContains(pin,null,"Default Reset Pin value is not displayed in preference page",t1);

            if(pin==""){

                t1.pass("Default Reset Pin value is not displayed in preference page");
            }
            else {
                t1.fail("Default Reset pin value is displayed in preference page");
            }
            String pwd = SystemPreferenceManagement.init(t1).getSystemPreferenceValue("DEFAULT_RESET_PWD");

            if(pwd==""){

                t1.pass("Default Reset password value is not displayed in preference page");
            }
            else {
                t1.fail("Default Reset password value is displayed in preference page");
            }*/


            String newDefaultPin = "1111";
            SystemPreferenceManagement.init(t1).updateSystemPreference("DEFAULT_RESET_PIN", "1111");

            User rt = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(rt);
            Login.init(t1).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(rt);

            List<String> messages = MobiquityGUIQueries.getAllMessageFromSentSMS(rt.MSISDN, "asc");

            if (messages.size() == 0) {
                Assertion.markAsFailure("Failed to fetch messages from DB, hence failing the test");
            } else {
                //decrypt the password
                DesEncryptor d = new DesEncryptor();
                String mpin = d.decrypt(messages.get(0));
                Assertion.verifyContains(mpin, "Channel User add initiated successfully. mPIN: " + newDefaultPin + " for newly added MFS Providers.", "Verification for MPIN SMS", t1);

                String tpin = d.decrypt(messages.get(1));
                Assertion.verifyContains(tpin, "Channel User add initiated successfully. Password:" + ConfigInput.userCreationPassword + " PIN:" + newDefaultPin + " Login Id:" + rt.LoginId + "", "Verification for PIN SMS", t1);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT})
    public void TC_ECONET_0618_e() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0618_e", "To verify that following general rule apply to Subscriber’s PIN (applicable for both mPIN and tPIN):  PIN_MIN_LENGTH and PIN_MAX_LENGTH would not be allowed to be modified but this will be displayed on preferences screen.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            String preference = "PIN_PWD_MIN_LENGTH";
            String preference1 = "PIN_PWD_MAX_LENGTH";

            String minLength = MobiquityGUIQueries.fetchmodifyAllowedFromGUI(preference);
            String maxLength = MobiquityGUIQueries.fetchmodifyAllowedFromGUI(preference1);

            DBAssertion.verifyDBAssertionEqual(minLength, "N", preference + " cannot be modified", t1);
            DBAssertion.verifyDBAssertionEqual(maxLength, "N", preference1 + " cannot be modified", t1);

            Login.init(t1).login(opt);
            SystemPreference_page1.init(t1).navigateToSystemPreferencePage();

            try {
                driver.findElement(By.xpath("//td[text()='" + preference + "']/..//td[last()]")).sendKeys("5");
                t1.fail(preference + " is editable");
            } catch (Exception e) {
                t1.pass(preference + " is non-editable");
            }

            try {
                driver.findElement(By.xpath("//td[text()='" + preference1 + "']/..//td[last()]")).sendKeys("5");
                t1.fail(preference1 + " is editable");
            } catch (Exception e1) {
                t1.pass(preference1 + " is non-editable");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0615() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0615", "To verify that the If factory " +
                "settings to set is ‘Different values for temporary mPIN and tPIN’.The temporary mPIN and tPIN " +
                "will be different if the factory setting option of ‘Different values for temporary mPIN and tPIN’ " +
                "is checked else both the PINs will have same value.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("ENABLE_DIFF_TPIN_AND_MPIN");
        try {

            if (prefValue.equalsIgnoreCase("FALSE")) {

                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("ENABLE_DIFF_TPIN_AND_MPIN", "Y");
                SystemPreferenceManagement.init(t1).updateSystemPreference("ENABLE_DIFF_TPIN_AND_MPIN", "TRUE");
            }

            User whs = new User(Constants.WHOLESALER);
           /* Login.init(pNode).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(whs);
            Login.init(pNode).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(whs);*/

            ChannelUserManagement.init(t1).initiateAndApproveChUser(whs, false);

            TxnResponse res1 = Transactions.init(t1).changeChannelUserMPin(whs, ConfigInput.mPin);
            res1.verifyStatus(Constants.TXN_SUCCESS);
            startNegativeTest();
            TxnResponse res2 = Transactions.init(t1).changeChannelUserTPin(whs, ConfigInput.mPin);
            res2.verifyMessage("000173");
            res2.verifyStatus("000173");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("ENABLE_DIFF_TPIN_AND_MPIN", prefValue);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 16, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0600A() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0600A", "To verify that the If factory " +
                "settings to set is ‘Different values for temporary mPIN and tPIN’.The temporary mPIN and tPIN " +
                "will be different if the factory setting option of ‘Different values for temporary mPIN and tPIN’ " +
                "is checked else both the PINs will have same value.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT).assignAuthor(Author.NIRUPAMA);

        String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("ENABLE_DIFF_TPIN_AND_MPIN");
        try {

            if (prefValue.equalsIgnoreCase("TRUE")) {

                MobiquityGUIQueries
                        .updateDisplayAndModifiedAllowedFromGUI("ENABLE_DIFF_TPIN_AND_MPIN", "Y");

                SystemPreferenceManagement.init(t1)
                        .updateSystemPreference("ENABLE_DIFF_TPIN_AND_MPIN", "FALSE");
            }

            User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).initiateAndApproveChUser(whs, false);

            TxnResponse res1 = Transactions.init(t1).changeChannelUserMPin(whs, ConfigInput.mPin);
            res1.verifyStatus(Constants.TXN_SUCCESS);

            TxnResponse res2 = Transactions.init(t1).changeChannelUserTPin(whs, ConfigInput.mPin);
            res2.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("ENABLE_DIFF_TPIN_AND_MPIN", prefValue);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0600B() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0600B", "To verify that the user is not able " +
                "to perform any transaction without pin change");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {

            User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).initiateAndApproveChUser(whs, false);

            //  service charges for non-Financial Transactions
            ServiceCharge chUserBal = new ServiceCharge(Services.BALANCE_ENQUIRY, whs, opt,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(chUserBal);

            //user shoult not be able to perform any transaction without changing PIN
            Transactions.init(t1)
                    .startNegativeTest()
                    .BalanceEnquiry(whs)
                    .verifyMessage("000680");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @BeforeClass(alwaysRun = true)
    public void postCondition() {
        ExtentTest teardown = pNode.createNode("teardown");
        try {
            SystemPreferenceManagement.init(teardown)
                    .updateSystemPreference("DEFAULT_RESET_PIN", "0000");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
