package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.util.common.Assertion;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by vandana.rattan on 2/20/2019.
 */
public class Suite_AlertOnLowBalance extends TestInit {
    private OperatorUser optNA;
    private User channelUser;


    /**
     * //TODO Complete test . Test case is not complete
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0}, enabled = false)
    public void TC_ECONET_0715() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0715 :To verify that the Mobiquity system should send alert on low balance " +
                "to distributor if their's agent wallet amount reached to that limit.")
                .assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {

            channelUser = new User(Constants.WHOLESALER);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.SYSTEM_PREFERENCES);

            Preferences.init(t1).modifySystemPreferences("WU_LOW_BALANCE_LIMIT1", "40000");
            Login.init(t1).login(channelUser);
            String messages = MobiquityGUIQueries.getMessageFromSentSMS(channelUser.MSISDN, "desc");
            System.out.println(messages);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
