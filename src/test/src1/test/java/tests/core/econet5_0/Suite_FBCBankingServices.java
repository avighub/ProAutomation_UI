package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_FBCBankingServices extends TestInit {

    private String prePrefValue, postPrefValue;

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.FBC_BANKING_SERVICES, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_SIT_1120() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1120",
                "To verify that the Valid User should able to Add FBC Bank under Bank Master Module.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.FBC_BANKING_SERVICES, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_POOL_ACC_REQ", "Y");

            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            Bank bank = new Bank(defaultProvider.ProviderId,
                    "FBCBANK" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    true,
                    false
            );

            CurrencyProviderMapping.init(t1)
                    .addBank(bank);

            CurrencyProviderMapping.init(t1)
                    .modifyServiceProviderBankMapping(defaultProvider.ProviderName, bank.BankName);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.FBC_BANKING_SERVICES, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1123() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1123",
                "To verify that the Valid user should able to Add Bank Details of Subscriber via WEB.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.FBC_BANKING_SERVICES, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");
            Bank bank = new Bank(defaultProvider.ProviderId,
                    "FBCBANK" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    true,
                    false
            );
            CurrencyProviderMapping.init(t1)
                    .addBank(bank);
            CurrencyProviderMapping.init(t1)
                    .modifyServiceProviderBankMapping(defaultProvider.ProviderName, bank.BankName);

            // add TCP and mobile role for new Bank

            //Create Subscriber with FBC Bank
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, true);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.FBC_BANKING_SERVICES, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_1142() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1142",
                "To verify that the Valid user should able to Add Ecobank Bank Details of Subscriber via WEB.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.FBC_BANKING_SERVICES, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");
            Bank bank = new Bank(defaultProvider.ProviderId,
                    "ECOBANK" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    true,
                    false
            );
            CurrencyProviderMapping.init(t1)
                    .addBank(bank);
            CurrencyProviderMapping.init(t1)
                    .modifyServiceProviderBankMapping(DataFactory.getDefaultProvider().ProviderName, bank.BankName);

            // add TCP and mobile role for new Bank

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
