package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TCPManagement;
import framework.pageObjects.tcp.ApprovalLimitReset;
import framework.pageObjects.tcp.InitiateLimitReset;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_LimitReset extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.LIMIT_RESET})
    public void TC_ECONET_1261() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1261", "To verify that the Admin user should " +
                "able to view new link 'Limit Extension' on web.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.LIMIT_RESET);
        try {
            OperatorUser optInitUser = DataFactory.getOperatorUserWithAccess("TCP_INITLIMITRESET");
            OperatorUser optApproveUser = DataFactory.getOperatorUserWithAccess("TCP_APPRLIMITRESET");

            try {
                Login.init(t1).login(optInitUser);
                InitiateLimitReset page = new InitiateLimitReset(t1);
                page.navInitiateLimitReset();
                Assertion.logAsPass("Initiate limit extension Is Available For NetworkAdmin", t1);

            } catch (Exception e) {
                t1.fail("Initiate limit extension Is not Available For NetworkAdmin");
            }

            try {

                Login.init(t1).login(optApproveUser);
                ApprovalLimitReset page = new ApprovalLimitReset(t1);
                page.navApprovalLimitReset();
                Assertion.logAsPass("Approve limit extension Is Available For NetworkAdmin", t1);

            } catch (Exception e) {
                t1.fail("Approve limit extension Is not Available For NetworkAdmin");
            }


        } catch (Exception e) {
            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.LIMIT_RESET})
    public void TC_ECONET_1262() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1262", "To verify that the " +
                "Admin user should able to initiate limit extension via web for a MSISDN.");

        ExtentTest t2 = pNode.createNode("TC_ECONET_1263", "To verify that the " +
                "Admin user should able to initiate limit extension via web for a MSISDN.");

        try {

            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            TCPManagement.init(t1).initiateLimitReset(sub, Constants.Category_SUBS, "Account Closure", "2x", "Both", "Weekly", "ALL");
            TCPManagement.init(t2).approveLimitReset(sub, Constants.Category_SUBS);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
