package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.pricingEngine.PricingEngine;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.ownerToChannel.O2CInitiation_Page1;
import framework.pageObjects.serviceCharge.NonFinancialServiceChargeCalculator_Page1;
import framework.pageObjects.serviceCharge.ServiceCharge_Pg2;
import framework.pageObjects.serviceCharge.ViewServiceCharge_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

import static framework.util.JsonPathOperation.delete;
import static framework.util.JsonPathOperation.set;
import static framework.util.jigsaw.JigsawOperations.performServiceRequestAndWaitForFailedForSyncAPI;

public class Suite_ServiceChargeAndCommissionMangement_01 extends TestInit {

    Wallet commWallet;
    CurrencyProvider defProvider;
    String defaultBankId;
    ServiceCharge sCharge;
    private OperatorUser netAdmin, usrSChargeCreator, usrSChargeApprover, channelAdmin;
    private User chUser, subs, merchant;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        usrSChargeCreator = DataFactory.getOperatorUserWithAccess("SVC_ADD");
        usrSChargeApprover = DataFactory.getOperatorUserWithAccess("SVC_APP");
        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        // merchant = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        commWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
        defProvider = DataFactory.getDefaultProvider();
        defaultBankId = DataFactory.getDefaultBankIdForDefaultProvider();
        channelAdmin = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
        netAdmin = DataFactory.getOperatorUserWithAccess("SVC_CALC", Constants.NETWORK_ADMIN);
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0089() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0089", "To verify that the Service charge deduction should be of one party only - Payer or Payee not Both.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {

            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
            ServiceCharge sCharge = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, opt, null,
                    null, null, null);


            Login.init(t1).login(usrSChargeCreator);

            ServiceChargeManagement.init(t1)
                    .addInitiateServiceCharge(sCharge);

            ServiceCharge_Pg2 pageTwo = ServiceCharge_Pg2.init(t1);

        /*
        Select Sender and Receiver Grade
         */
            Thread.sleep(5000);
            pageTwo.selectSenderGrade(sCharge.Payer.GradeName);
            pageTwo.selectReceiverGrade(sCharge.Payee.GradeName);
            Thread.sleep(2000);
            Select sel = new Select(driver.findElement(By.id("payingEntityID")));
            List<WebElement> options = sel.getOptions();
            for (int i = 1, j = 0; i < options.size(); i++, j++) {
                String opt1 = options.get(i).getText();
                Assert.assertNotEquals(opt1, "Both");
                continue;
            }
            t1.pass("Service Charge deduction is for one Party only");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * Test Case is disabled because it is not testing the actual functionality
     *
     * @throws Exception
     */
    @Test(enabled = false, priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0093() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0093",
                "To verify that the Commission amount value should credit to commission wallet of channel user only.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            UsrBalance channelUsrBalance = MobiquityGUIQueries.getUserBalance(chUser, commWallet.WalletId, null);
            Integer preBalance = channelUsrBalance.Balance.intValue();
            System.out.println(preBalance + "prebalance");

            Integer amount = 10;

            TransactionManagement.init(t1)
                    .performO2CforCommissionWallet(chUser, String.valueOf(amount), "Test");

            UsrBalance channelUsrPostBalance = MobiquityGUIQueries.getUserBalance(chUser, commWallet.WalletId, null);
            Integer postBalance = channelUsrPostBalance.Balance.intValue();
            System.out.println(postBalance + "postbalance");

            preBalance = preBalance + amount;

            Assert.assertEquals(postBalance, preBalance);
            t1.pass("Commission Amount is credited to Commission Wallet");

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0088() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0088", "To verify that the No commission amount will be credited to the distributor commission wallet if user don’t have commission wallet in system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG);
        try {
            //System Preference Update
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_COMMISSION_WALLET_REQUIRED", "N");

            chUser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(chUser, false);

            Login.init(t1).login(usrSChargeCreator);

            O2CInitiation_Page1 page1 = O2CInitiation_Page1.init(t1);

            page1.navigateToOwner2ChannelInitiationPage();
            page1.setMobileNumber(chUser.MSISDN);
            page1.selectProviderName(DataFactory.getDefaultProvider().ProviderId);

            Select sel = new Select(driver.findElement(By.id("otherWalletListId")));
            List<WebElement> sel1 = sel.getAllSelectedOptions();
            for (WebElement sel2 : sel1) {
                String wallet = sel2.toString();
                if (wallet.equalsIgnoreCase(Constants.COMMISSION_WALLET_STRING)) {
                    t1.fail("Even commission wallet is not available,commission option is available to transfer money");
                }
                t1.pass("No commission amount will be credited to the distributor commission wallet if user don’t have commission wallet in system.");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_COMMISSION_WALLET_REQUIRED", "Y");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0094() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("Creating Subscriber without Acquisation");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(chUser);

            User subs1 = new User(Constants.SUBSCRIBER);
            Login.init(t1).login(chUser);
            SubscriberManagement.init(t1).addSubscriber(subs1, true);
            Login.init(t1).login(chUser);
            SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs1);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0094", "To verify that the Subscriber should not able to perform any transaction if joining fee is not deducted in the system for subscriber.");
            startNegativeTest();
            TransactionManagement.init(t2).performCashIn(subs1, Constants.MIN_CASHOUT_AMOUNT);
            Assertion.verifyErrorMessageContain("subs.acquisation.pending", "Joining Fees is not paid", t2);

            ExtentTest t3 = pNode.createNode("TC_ECONET_0095", "To verify that the Subscriber should not able to perform Bank to Wallet transaction in case joining fee is not deducted in the system for subscriber.");
            ValidatableResponse res = performServiceRequestAndWaitForFailedForSyncAPI(Services.WALLET_TO_BANK,
                    set("serviceFlowId", ""),
                    set("transactionAmount", "5"),
                    set("initiator", "transactor"),
                    set("currency", "101"),
                    set("bearerCode", "USSD"),
                    set("language", "en"),
                    set("externalReferenceId", ""),
                    set("remarks", ""),
                    set("transactionMode", ""),
                    set("requestedServiceCode", ""),
                    delete("productOwnerCode"),
                    delete("productBrand"),
                    delete("productCategory"),
                    set("transactor.idType", "mobileNumber"),
                    set("transactor.productId", "12"),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.idValue", subs1.MSISDN),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.password", subs1.Password),
                    set("transactor.pin", ConfigInput.mPin),
                    set("transactor.txnPassword", null),
                    set("transactor.employeeId", null),
                    set("transactor.bankId", null),
                    set("transactor.bankAccountNumber", null),
                    delete("depositor"),
                    set("receiver.bankId", DataFactory.getDefaultBankIdForDefaultProvider()),
                    set("receiver.bankAccountNumber", subs1.MSISDN),
                    delete("receiver.idType"),
                    delete("receiver.idValue"),
                    delete("receiver.productId"),
                    delete("receiver.identificationNo"),
                    delete("receiver.pin"),
                    delete("receiver.mpin"),
                    delete("receiver.tpin"),
                    delete("receiver.password"),
                    delete("receiver.txnPassword"),
                    delete("receiver.partyId"));
            SfmResponse response1 = new SfmResponse(res, pNode);

            String mess = response1.Message;
            Assertion.assertEqual(mess, "Initiator is acquisition pending", "Initiator is acquisition pending", t3);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0895() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0895",
                "To verify that network admin cannot define Service Charge if From Amount is greater than To Amount in slab entry of Commission Setup.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            sCharge = new ServiceCharge(Services.TXN_CORRECTION, subs, merchant,
                    null, null, null, null);

            Login.init(t1).login(usrSChargeCreator);

            ServiceChargeManagement.init(t1)
                    .addInitiateServiceCharge(sCharge);
            sCharge.setCommissionFromRange("5000");
            startNegativeTest();
            ServiceChargeManagement.init(t1).setCommissionDetail(sCharge);
            Assertion.verifyErrorMessageContain("servicechargeprofile.validation.commstartend", "From Range Should not be greater than the To Range", t1, "1");

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0896() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0896",
                "To verify that network admin cannot define Service Charge if Payer Category is not selected in slab entry of Commission Setup.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            sCharge = new ServiceCharge(Services.TXN_CORRECTION, subs, merchant,
                    null, null, null, null);

            Login.init(t1).login(usrSChargeCreator);

            ServiceChargeManagement.init(t1)
                    .addInitiateServiceCharge(sCharge);
            startNegativeTest();
            ServiceCharge_Pg2 pageTwo = ServiceCharge_Pg2.init(t1);
            pageTwo.setCommissionFromRange(sCharge.CommissionFromRange);
            pageTwo.setCommissionToRange(Constants.SCHARGE_MAX_TXN_AMT);
            pageTwo.clickNextButton();

            Assertion.verifyErrorMessageContain("servicechargeprofile.validation.payerPayeenull", "Payer Category Or Payee Category cannot be empty", t1, "1");

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0897() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0897",
                "To verify that network admin can not view the details of the existing Service Charge if Service Charge Profile Set is not selected.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {

            Login.init(t1).login(usrSChargeCreator);

            ViewServiceCharge_Pg1 page = ViewServiceCharge_Pg1.init(t1);
            page.navAddServiceCharge();
            page.setNumberOfDays("1");
            page.clickSubmit();

            Assertion.verifyErrorMessageContain("servicechargeprofile.empty.serviceChargeSet", "Service Charge Profile is Required", t1);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0898() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0898",
                "To verify that network admin can not view the details of the existing Service Charge if mandatory Last Days field is left as blank or an invalid value is entered as an input.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {

            Login.init(t1).login(usrSChargeCreator);

            ViewServiceCharge_Pg1 page = ViewServiceCharge_Pg1.init(t1);
            page.navAddServiceCharge();
            page.selectServiceNameByIndex();
            page.setNumberOfDays("abc");
            page.clickSubmit();

            Assertion.verifyErrorMessageContain("servicechargeprofile.error.noofdaysnotnuemeric", "Last Days Should be Numeric", t1);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0900() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0900",
                "To verify that network admin cannot define Service Charge when Payer and Payee Category are same in Commission Setup");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            sCharge = new ServiceCharge(Services.TXN_CORRECTION, subs, merchant,
                    null, null, null, null);

            Login.init(t1).login(usrSChargeCreator);

            ServiceCharge_Pg2 pageTwo = ServiceCharge_Pg2.init(t1);

            ServiceChargeManagement.init(t1)
                    .addInitiateServiceCharge(sCharge);
            Thread.sleep(5000);
            pageTwo.selectSenderGrade(sCharge.Payer.GradeName);
            pageTwo.selectReceiverGrade(sCharge.Payee.GradeName);
            Thread.sleep(2000);

            /**
             * Determine the PayEntityCode
             */
            String payEntityCode, creditEntity, payerCommCatList, payeeCommCatList;

            payEntityCode = sCharge.Payee.CategoryName;
            pageTwo.selectPayingEntity(payEntityCode);
            // Set Credit Entity


            /**
             * Determine:
             *  Credit entity
             *  Payer Commission Category
             *  Payee Commission Category
             */

            payeeCommCatList = sCharge.Payer.ProviderName + "-Payer-" + sCharge.Payer.CategoryName + "-" + sCharge.Payer.PaymentType;
            creditEntity = sCharge.Payee.ProviderName;
            pageTwo.selectCreditedEntity(creditEntity);
            pageTwo.selectCommissionPayerCategory(payeeCommCatList);
            pageTwo.selectCommissionPayeeCategory(payeeCommCatList);
            pageTwo.clickNextButton();


            Assertion.verifyErrorMessageContain("servicechargeprofile.validation.payerPayeesame", "Payer Category Or Payee Category cannot be Same", t1, "1");

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0070() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0070",
                "To verify that network admin is able to Reject the add initiated service charge.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            sCharge = new ServiceCharge(Services.TXN_CORRECTION, subs, merchant,
                    null, null, null, null);

            Login.init(t1).login(usrSChargeCreator);
            TransferRuleManagement.init(t1).configureTransferRule(sCharge);

            ServiceChargeManagement.init(t1)
                    .addInitiateServiceCharge(sCharge)
                    .setCommissionDetail(sCharge)
                    .completeServiceChargeCreation(sCharge);

            Login.init(t1).login(usrSChargeApprover);
            ServiceChargeManagement.init(t1)
                    .rejectServiceCharge(sCharge);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0769() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0769",
                "To verify that user is able to specify service charge that a consumer has to pay (i.e. joining fee) to onboard into mobiquity Money. ");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            sCharge = new ServiceCharge(Services.ACQFEE, subs, usrSChargeApprover,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);

            t1.pass("Network Operator is able to define service charge for joining fee to onboard into mobiquity Money");


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0902() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0902",
                "To verify that Commission for new Subscriber addition will be applicable for cash in. ");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            sCharge = new ServiceCharge(Services.CASHIN, chUser, subs,
                    null, null, null, null);

            Login.init(t1).login(usrSChargeCreator);

            PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "COMMISSION", "mfs1.currency.code", "1");
            PricingEngine.init(t1).addApprovalForServiceCharge("Cash In", "COMMISSION", usrSChargeCreator.LoginId);


            t1.pass("Network Operator is able to define service charge for joining fee to onboard into mobiquity Money");


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0756() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0756",
                "To verify User other than network admin is not able to View Service charge calculator(Non Financial- charging)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {
            NonFinancialServiceChargeCalculator_Page1 page1 = NonFinancialServiceChargeCalculator_Page1.init(t1);
            ArrayList<OperatorUser> fields = new ArrayList<OperatorUser>();
            fields.add(netAdmin);
            fields.add(channelAdmin);

            int i;
            for (i = 0; i < fields.size(); i++) {
                Login.init(t1).login(fields.get(i));

                page1.navigateServiceChargeCalculator();

                t1.pass(fields.get(i).CategoryName + " has rights to login in to service charge calculator");
            }


            Login.init(t1).login(chUser);
            try {
                driver.findElement(By.id("NONFIN_ALL"));
                t1.fail(" Channel User have rights to login in to service charge calculator");
            } catch (Exception e) {
                t1.pass(" Channel User does not have rights to login in to service charge calculator");

            }
        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }
}





