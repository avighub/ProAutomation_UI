package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.common.Login;
import framework.features.systemManagement.GradeManagement;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TCPManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Grade Management
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 3/09/2018
 */

public class Suite_GradeManagement extends TestInit {

    private static SuperAdmin saAddGrade, saDeleteGrade;
    private static OperatorUser channeladmin, networkadmin;
    private static User channelUser;
    private static String gradeCode;
    private Grade grade;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            grade = new Grade(Constants.SUBSCRIBER);
            saAddGrade = DataFactory.getSuperAdminWithAccess("ADD_GRADES");
            saDeleteGrade = DataFactory.getSuperAdminWithAccess("DELETE_GRADES");
            channeladmin = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            networkadmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //Get all values and store in variable to reinitialize the object
            gradeCode = grade.getGradeCode();
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0050() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0050",
                "To verify that Valid Superadmin can create grade for Biller.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        Grade newGrade = null;
        try {

            Login.init(t1).loginAsSuperAdmin(saAddGrade);

            newGrade = new Grade(Constants.BILLER);

            GradeManagement.init(t1).addGrade(newGrade);
            t1.pass("SuperAdmin Able to Add Grade for External Companies");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            Login.init(t1).loginAsSuperAdmin(saDeleteGrade);
            GradeManagement.init(t1).deleteGrade(newGrade, true);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0051() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0051", "To verify that the Proper error message should get displayed on web if SUperadmin delete Grade which is already associated with TCP/Service Charge.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            Login.init(t1).loginAsSuperAdmin(saDeleteGrade);

            Grade newGrade = new Grade(Constants.WHOLESALER);
            newGrade.setGradeCode(Constants.GOLD_WHOLESALER);

            GradeManagement.init(t1).startNegativeTest().deleteGrade(newGrade, true);
            Assertion.verifyErrorMessageContain("tcp.exist.for.gradecode", "TCP Exists For GradeCode", t1);
            Assertion.verifyErrorMessageContain("servicechargeprofile.exist.for.gradecode", "Service Charge Profile Exists For GradeCode", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0052() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0052", "To verify that Users other than super admin should not be able to delete grade.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            ArrayList fields = new ArrayList();
            fields.add(channeladmin);
            fields.add(networkadmin);
            fields.add(channelUser);


            int i;
            for (i = 0; i < fields.size(); i++) {

                try {
                    Login.init(t1).login(fields.get(i));
                    driver.findElement(By.id("CHGRADES_ALL"));
                    t1.fail("Users other than super admin is be able to delete grade");

                } catch (Exception e) {
                    continue;

                }
            }
            t1.pass("Users other than super admin is not able to delete grade");
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0049() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0049", "To verify that the Proper error message should get displayed on web if Superadmin add Grade more than the configured count in system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);
        String pref = MobiquityGUIQueries.fetchDefaultValueOfPreference("MAX_GRADES_PER_CATEGORY");
        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference
                    ("MAX_GRADES_PER_CATEGORY", "1");

            Login.init(t1).loginAsSuperAdmin(saAddGrade);

            Grade newGrade = new Grade(Constants.WHOLESALER);

            GradeManagement.init(t1).startNegativeTest().addGrade(newGrade);
            Assertion.verifyErrorMessageContain("maximum.grades.count.reached", "Configured Count has been reached", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference
                    ("MAX_GRADES_PER_CATEGORY", pref);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0048() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0048", "\"Add Grade>> " +
                "To verify that SuperAdmin can create grade, if all fields in contains value upto their maximum length.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.SARASWATHI);

        Grade newGrade = null;
        String gradecode = null;
        try {

            Login.init(t1).loginAsSuperAdmin(saAddGrade);
            newGrade = new Grade(Constants.BILLER);

            //Setting the grade code more than maximum value
            String gradeCode = "NewGrade" + DataFactory.getRandomNumber(9);
            newGrade.setGradeCode(gradeCode);
            int gradeCodeLength = gradeCode.length();

            //Add Grade
            startNegativeTest();
            GradeManagement.init(t1).addGrade(newGrade);

            //Getting the entered gradecode
            String actual = Assertion.getActionMessage();
            gradecode = actual.split("Grade code:")[1].trim().split(",")[0].trim();
            int gradecodeLength = gradecode.length();

            //Asserting the length of both codes

            Assert.assertNotEquals(gradeCodeLength, gradecodeLength);
            t1.pass("User can't create grade with grade code whose length is more than maximum length");


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            Login.init(t1).loginAsSuperAdmin(saDeleteGrade);
            newGrade.setGradeCode(gradecode);
            GradeManagement.init(t1).deleteGrade(newGrade, true);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0718() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0718", "To verify that Users other than super admin should not be able to add grade.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            ArrayList fields = new ArrayList();
            fields.add(channeladmin);
            fields.add(networkadmin);
            fields.add(channelUser);


            int i;
            for (i = 0; i < fields.size(); i++) {

                try {
                    Login.init(t1).login(fields.get(i));
                    driver.findElement(By.id("CHGRADES_ALL"));
                    t1.fail("Users other than super admin is be able to add grade");

                } catch (Exception e) {
                    continue;

                }
            }
            t1.pass("Users other than super admin is not able to add grade");
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, enabled = false)
    public void TC_ECONET_0511() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0511", "To verify that , special few agent should be created by mobiquity user by creating a new grade.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {
            User chUser = new User(Constants.WHOLESALER);

            //Adding Grade
            saAddGrade = DataFactory.getSuperAdminWithAccess("ADD_GRADES");
            Login.init(t1).loginAsSuperAdmin(saAddGrade);
            Grade newGrade = new Grade(Constants.WHOLESALER);
            GradeManagement.init(t1).addGrade(newGrade);

            //Adding Group Role
            GroupRoleManagement.init(t1).
                    addMobileGroupRolesForSpecificGrade(chUser, newGrade.GradeName, Constants.PAYINST_WALLET_CONST_MOBILE_ROLE, DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletName);

            //Adding TCP
            InstrumentTCP tcp = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    newGrade.GradeName,
                    "WALLET", DataFactory.getDefaultWallet().WalletName);
            TCPManagement.init(t1).addInstrumentTCP(tcp);

            //Creating Users With New Grade
            chUser.setGradeName(newGrade.GradeName);


            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(chUser, false);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}



