package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.entity.WebGroupRole;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Category Management
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 10/09/2018
 */

public class Suite_Enquiry extends TestInit {
    private static User whs, cashoutUser, subs, whsEnq;
    private String trustBank;
    private OperatorUser net, channelAdmin, customerCareExecutive;
    private String trustBankId;
    private ExtentTest setupNode;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        setupNode = pNode.createNode("Setup");

        try {

            whs = DataFactory.getChannelUserWithCategory("WHS");
            whsEnq = DataFactory.getChannelUserWithAccess("ENQ_US", Constants.WHOLESALER);

            net = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            channelAdmin = DataFactory.getOperatorUserWithAccess("ENQ_US", Constants.CHANNEL_ADMIN);
            customerCareExecutive = DataFactory.getOperatorUserWithAccess("ENQ_US", Constants.CUSTOMER_CARE_EXE);

            trustBank = DataFactory.getDefaultBankNameForDefaultProvider();
            trustBankId = DataFactory.getBankId(trustBank);


            /*ServiceCharge b2W = new ServiceCharge(Services.BANK_TO_WALLET, whs, whs, trustBankId, null, null, null);
            TransferRuleManagement.init(setup).configureTransferRule(b2W);
*/
            cashoutUser = DataFactory.getChannelUserWithAccess("COUT_WEB");
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);

           /* ServiceCharge cashoutService = new ServiceCharge(Services.CASHOUT, subs, cashoutUser, null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(cashoutService);
*/
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }


    /**
     *
     * @param optNwadm
     * @param bcu
     * @param cce
     * @throws Exception
     */
    private void setupSpecificGroupRoleOperator(OperatorUser optNwadm,OperatorUser bcu,OperatorUser cce) throws Exception {
        ArrayList<String> applicableRole = new ArrayList<>(Arrays.asList("CHECK_ALL"));
        WebGroupRole webGroupRole = new WebGroupRole(Constants.NETWORK_ADMIN, "AUTNWADM1", applicableRole, 1);

        ArrayList<String> applicableRoleBCU = new ArrayList<>(Arrays.asList("CHECK_ALL"));
        WebGroupRole webGroupRoleBCU = new WebGroupRole(Constants.CHANNEL_ADMIN, "AUTBCU1", applicableRoleBCU, 1);

        ArrayList<String> applicableRoleCCE = new ArrayList<>(Arrays.asList("CHECK_ALL"));
        WebGroupRole webGroupRoleCCE = new WebGroupRole(Constants.CUSTOMER_CARE_EXE, "AUTCCE1", applicableRoleCCE, 1);

        Login.init(setupNode).loginAsSuperAdmin("GRP_ROL");
        GroupRoleManagement.init(setupNode)
                .addWebGroupRole(webGroupRole);

        GroupRoleManagement.init(setupNode)
                .addWebGroupRole(webGroupRoleBCU);

        GroupRoleManagement.init(setupNode)
                .addWebGroupRole(webGroupRoleCCE);

        optNwadm.WebGroupRole = webGroupRole.RoleName;
        bcu.WebGroupRole = webGroupRoleBCU.RoleName;
        cce.WebGroupRole = webGroupRoleCCE.RoleName;
        OperatorUserManagement.init(setupNode).createAdmin(optNwadm);
        OperatorUserManagement.init(setupNode).createOptUser(bcu,Constants.NETWORK_ADMIN,Constants.NETWORK_ADMIN);
        OperatorUserManagement.init(setupNode).createOptUser(cce,Constants.NETWORK_ADMIN,Constants.NETWORK_ADMIN);
    }


    private void setupSpecificGroupRoleChannelUser(User whs, User ret, User mer , User hmer) throws Exception {
        ArrayList<String> applicableRoleWHS = new ArrayList<>(Arrays.asList("CHECK_ALL"));
        WebGroupRole webGroupRole = new WebGroupRole(Constants.WHOLESALER, "AUTWHS1", applicableRoleWHS, 1);

        ArrayList<String> applicableRoleRET = new ArrayList<>(Arrays.asList("CHECK_ALL"));
        WebGroupRole webGroupRoleRET = new WebGroupRole(Constants.RETAILER, "AUTRET1", applicableRoleRET, 1);

        ArrayList<String> applicableRoleMER = new ArrayList<>(Arrays.asList("CHECK_ALL"));
        WebGroupRole webGroupRoleMER = new WebGroupRole(Constants.MERCHANT, "AUTMER1", applicableRoleMER, 1);

        ArrayList<String> applicableRoleHMER = new ArrayList<>(Arrays.asList("CHECK_ALL"));
        WebGroupRole webGroupRoleHMER = new WebGroupRole(Constants.HEAD_MERCHANT, "AUTHMER1", applicableRoleHMER, 1);

        Login.init(setupNode).loginAsSuperAdmin("GRP_ROL");
        GroupRoleManagement.init(setupNode)
                .addWebGroupRole(webGroupRole);

        GroupRoleManagement.init(setupNode)
                .addWebGroupRole(webGroupRoleRET);

        GroupRoleManagement.init(setupNode)
                .addWebGroupRole(webGroupRoleMER);

        GroupRoleManagement.init(setupNode)
                .addWebGroupRole(webGroupRoleHMER);

        whs.WebGroupRole = webGroupRole.RoleName;
        ret.WebGroupRole = webGroupRoleRET.RoleName;
        mer.WebGroupRole = webGroupRoleMER.RoleName;
        hmer.WebGroupRole = webGroupRoleHMER.RoleName;

        ChannelUserManagement.init(setupNode).createChannelUserDefaultMapping(whs,false);
        ChannelUserManagement.init(setupNode).createChannelUserDefaultMapping(ret,false);
        ChannelUserManagement.init(setupNode).createChannelUserDefaultMapping(mer,false);
        ChannelUserManagement.init(setupNode).createChannelUserDefaultMapping(hmer,false);

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0702_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0702_A",
                "To verify that the Netadmin should be able to do transaction details enquiry with Bank involved");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, trustBankId);
            String channaccount = AccountNum.get("ACCOUNT_NO");

            String trustAccNo = Utils.decryptMessage(channaccount);

            ServiceCharge b2W = new ServiceCharge(Services.BANK_TO_WALLET, whs, whs, trustBankId, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(b2W);

            SfmResponse res = Transactions.init(t1).bankToWalletService(whs, "2", trustBankId, trustAccNo);
            String serReqId = res.ServiceRequestId;
            String txnID = res.TransactionId;
            res.verifyMessage("bank.to.SVA.success", res.TransactionId);

            Transactions.init(t1).performEIGTransaction(serReqId, "true", Services.RESUME_BANK_TO_WALLET);

            Login.init(t1).login(net);
            Enquiries.init(t1).transactionDetails(txnID);

            Object[] expected = PageInit.fetchLabelTexts("//*[@id='txnAction_loadTxnDetails']/table/tbody/tr[2]/td");
            Object[] actual = UserFieldProperties.getLabels("enquiry.transaction.details2");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("Label '" + expected[i] + "' verified Successfully...");
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }

            String dtSlash = new SimpleDateFormat("yyyy/MM/dd").format(new Date());

            String maskAccNum = "XXXXXX" + trustAccNo.substring(6);

            String agentCode = MobiquityGUIQueries.getAgentCode(whs.MSISDN);
            String balance1 = MobiquityGUIQueries.getUserBalance(whs, null, null).Balance.toString();

            List<String> allFields = Arrays.asList(dtSlash, txnID, Constants.TXN_SUCCESS_STRING,
                    Services.BANK_TO_WALLET, whs.FirstName, maskAccNum, whs.FirstName, agentCode, balance1,
                    DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultWallet().WalletName);

            for (String fieldVal : allFields) {
                String elementText = "//form[@id='txnAction_loadTxnDetails']/table/tbody/tr[3]/td[contains(text(),'" + fieldVal + "')]";
                WebElement element = driver.findElement(By.xpath(elementText));
                Utils.highLightElement(element);
                Assertion.verifyEqual(element.getText(), fieldVal, "Verify Field", t1, true);
            }

            t1.info("All details are verified");


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0702_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0702_B",
                "To verify that the Network Admin should be able to do transaction details enquiry without bank");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            User chUsr = DataFactory.getChannelUserWithAccess(Roles.CASH_IN);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(chUsr);

            ServiceCharge cashinService = new ServiceCharge(Services.CASHIN, chUsr, subs, null, null, null, null);
            TransferRuleManagement.init(t1)
                    .configureTransferRule(cashinService);

            String tid = Transactions.init(t1).initiateCashIn(subs, chUsr, new BigDecimal("5")).TransactionId;

            Login.init(t1).loginAsOperatorUserWithRole(Roles.TRANSACTION_DETAILS);
            //DO Enquiries
            Enquiries.init(t1).transactionDetails(tid);

            Object[] expected = PageInit.fetchLabelTexts("//*[@id='txnAction_loadTxnDetails']/table/tbody/tr[2]/td");
            Object[] actual = UserFieldProperties.getLabels("enquiry.transaction.details1");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("Label " + expected[i] + " verified Successfully");
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }

            String dtSlash = new SimpleDateFormat("yyyy/MM/dd").format(new Date());

            List<String> allFields = Arrays.asList(tid,
                    "Transaction Success",
                    dtSlash,
                    Services.CASHIN,
                    subs.FirstName,
                    chUsr.FirstName,
                    DataFactory.getDefaultWallet().WalletName);

            for (String value : allFields) {
                String elementText = "//form[@id='txnAction_loadTxnDetails']/table/tbody/tr[3]/td[contains(text(),'" + value + "')]";
                WebElement elem = driver.findElement(By.xpath(elementText));
                Utils.highLightElement(elem);
                Assertion.verifyEqual(elem.getText(), value, "Verify Field Value", t1, true);
            }

            t1.info("All details are verified");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0703() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0703",
                "To verify that the Channel User should be able to do self balance enquiry");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI).assignAuthor(Author.SARASWATHI);

        try {
            Login.init(t1).login(whsEnq);
            Enquiries.init(t1).doSelfBalanceEnquiry(whsEnq);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0704_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0704_a",
                "To verify if the enquiry rights are as per the rights for Operator users:"+
                        "1.Network Admin (All types of Enquiry as mentioned)" +
                        "2.Channel Admin (All types of Enquiry as mentioned)" +
                        "3.Customer Care (All types of Enquiry as mentioned)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            OperatorUser optNwadm = new OperatorUser(Constants.NETWORK_ADMIN);
            OperatorUser bcu = new OperatorUser(Constants.CHANNEL_ADMIN);
            OperatorUser cce = new OperatorUser(Constants.CUSTOMER_CARE_EXE);

            setupSpecificGroupRoleOperator(optNwadm,bcu,cce);

            List<OperatorUser> userList = Arrays.asList(optNwadm,bcu,cce);

            for (OperatorUser user : userList){

                Login.init(t1).login(user);

                Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_ENQ_US"),
                        true,"Verify Channel/Subscriber Link",t1,true);

                Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_SR_PIN01"),
                        true,"Verify Reset Pin Link",t1,true);

                Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_TXN_DETAILS"),
                        true,"Verify Transaction Details Link",t1,true);

                Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_ENQ_LUS"),
                        true,"Verify Channel User/Subscriber Loyalty Link",t1,true);

                Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_CCE_ENQUIRY"),
                        true,"Verify Customer Care Executive Enquiry Link",t1,true);

                t1.pass(user.CategoryName + " has -> Channel/Subscriber, -> Reset Pin," +
                        " ->Transaction Details, ->'Channel User/Subscriber Loyalty', ->' Customer Care Executive Enquiry; rights");
            }
        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0704_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0704_b",
                "To verify if the enquiry rights are as per the rights for Superadmin users:\n" +
                        "1.Super Admin (Only find Transaction Details)"
        ).assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0);

        try {

            Login.init(t1).loginAsSuperAdmin(Roles.TRANSACTION_DETAILS);

            Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL", "ENQUIRY_TXN_DETAILS"),
                    true, "Verify Enquiry Link Available", t1, true);

            Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL", "ENQUIRY_ENQ_US"),
                    false, "Verify ENQUIRY_TXN_DETAILS Not available", t1);

            Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL", "ENQUIRY_SR_PIN01"),
                    false, "Verify ENQUIRY_SR_PIN01 Not available", t1);

            Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL", "ENQUIRY_ENQ_LUS"),
                    false, "Verify ENQUIRY_ENQ_LUS Not available", t1);

            Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL", "ENQUIRY_CCE_ENQUIRY"),
                    false, "Verify ENQUIRY_CCE_ENQUIRY Not available", t1);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0704_c() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0704_c",
                "To verify if the enquiry rights are as per the rights for Channel users:\n" +
                        "1.Wholesaler (All Rights)\n" +
                        "2.Retailer (All Rights)\n" +
                        "3.Head Merchant (All Rights)\n" +
                        "4.Merchant (All Rights)"
        ).assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {
            User whs = new User(Constants.WHOLESALER);
            User ret = new User(Constants.RETAILER);
            User mer = new User(Constants.MERCHANT);
            User hmer = new User(Constants.HEAD_MERCHANT);

            ret.setOwnerUser(whsEnq);
            ret.setParentUser(whsEnq);

            setupSpecificGroupRoleChannelUser(whs,ret,mer,hmer);

            List<User> userList = Arrays.asList(whs,ret,mer,hmer);

                for (User user : userList) {

                    Login.init(t1).login(user);

                    Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_ENQ_US"),
                            true,"Verify Enquiry Link Available",t1,true);

                    Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_ENQ_USC"),
                            true,"Verify Enquiry Link Available",t1,true);

                    Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_SR_PIN01"),
                            true,"Verify Reset Pin Link Available",t1,true);

                    Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_ENQ_LUS"),
                            true,"Verify Enquiry Link Available",t1,true);

                    Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL","ENQUIRY_ENQ_LUSC"),
                            true,"Verify Enquiry Link Available",t1,true);

                    t1.pass(user.CategoryName + " has Channel/Subscriber,Reset Pin,Self Balance,Channel User/Subscriber Loyalty,Loyalty Self Balance Enquiry rights");
                }

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    /* Note that in this case, select the netadmin who does not have reset pin and CCE access */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_1272() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1272",
                "To verify no user other than CCE should be able to change status of channel user and subscriber.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG).assignAuthor(Author.SARASWATHI);

        try {
            t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);

            Login.init(t1).login(net);
            FunctionLibrary.init(t1).clickLink("ENQUIRY_ALL");

            ArrayList<String> fields = new ArrayList<String>();
            fields.add("ENQUIRY_SR_PIN01");
            fields.add("ENQUIRY_CCE_ENQUIRY");

            int i;

            for (i = 0; i < fields.size(); i++) {
                try {
                    driver.findElement(By.id(fields.get(i)));
                    t1.fail("NetworkAdmin have reset pin/change status rights");
                    break;
                } catch (Exception e) {
                    continue;
                }
            }
            t1.pass("NetworkAdmin does not have reset pin/change status rights");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
