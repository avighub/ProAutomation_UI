package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by krishan.chawla on 23/10/2018.
 */
public class Suite_EmailConfiguration extends TestInit {

    /**
     * TEST : TC_ECONET_0712 :E-mail Notification Configuration
     * To verify that the No user other than network admin can E-mail Configurations.
     */
    @Test(priority = 1, groups = {FunctionalTag.EMAIL_NOTIFICATION, FunctionalTag.ECONET_UAT_5_0})
    public void emailConfigurationTC0712() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0712",
                "E-mail Notification Configuration: To verify that the No user other than network admin can E-mail Configurations.")
                .assignCategory(FunctionalTag.EMAIL_NOTIFICATION, FunctionalTag.ECONET_UAT_5_0);
        String serviceType = "Email Notification Configuration";

        User chnlUser = DataFactory.getAnyChannelUser();

        Login.init(t1).login(chnlUser);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("MPREF_ALL", "MPREF_SMS_CRDM");
    }
}
