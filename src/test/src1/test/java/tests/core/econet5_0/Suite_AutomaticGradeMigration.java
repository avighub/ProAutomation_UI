package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.*;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.gradeManagement.GradeAddPage;
import framework.pageObjects.walletPreference.WalletPreferences_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Suite_AutomaticGradeMigration extends TestInit {
    private static SuperAdmin saMapGrade;
    private User sub;

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1116() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1116",
                "To verify that existing mobiquity system maintains the following 3 tier grades:" +
                        "1. Banksubs, Econet staff, farmer\n" +
                        "2. Cardsubs\n" +
                        "3. Goldsubs");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            saMapGrade = DataFactory.getSuperAdminWithAccess("ADD_GRADES");
            Login.init(t1).loginAsSuperAdmin(saMapGrade);
            GradeAddPage gradeAddPage1 = new GradeAddPage(pNode);

            gradeAddPage1.navigateToLink();
            ArrayList grades = new ArrayList<>();
            grades.add("BANKSUBS");
            grades.add("CARDSUBS");
            grades.add("EconetStaff");
            grades.add("Farmer");
            grades.add("Gold Subscriber");

            try {

                for (int i = 0; i < grades.size(); i++) {
                    WebElement element = driver.findElement(By.xpath("//form[@id='grades_addGrades']//tbody/tr/td[4][.='" + grades.get(i) + "']"));
                    element.isDisplayed();
                    t1.pass("Mobiquity maitains the " + grades.get(i) + " grade for Subscriber");
                }
            } catch (Exception e) {
                t1.fail("Mobiquity does not maitains the default grade for subscriber");
            }

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(enabled = false, priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0554() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0554", "To verify that when subscriber is not " +
                "associated with banking/card services subscriber is associated with ‘Gold subs’ tier grade.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            //String walletName = DataFactory.getWalletName();

            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(sub);

            OperatorUser net = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            OperatorUser optBankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            String actualGrade = MobiquityGUIQueries.dbGetUserGrade(sub);

            /*//Set the KYC preference for Bank for Subscriber
            Login.init(t1).login(net);
            WalletPreference walletPref = new WalletPreference(Constants.SUBSCRIBER, "BANK LINKED USER",
                    Constants.PAYINST_WALLET_CONST, false, null, Constants.GRADE_BANKSUBS);

            walletPref.setPaymentInst(Constants.PAYINST_BANK_CONST);
            walletPref.setPreferenceType(Constants.PREFERENCE_TYPE_AUTOMATIC_GRADE_CHANGE);
            Preferences.init(t1).configureWalletPreferences(walletPref);
 */
            String custId = DataFactory.getRandomNumberAsString(6);
            String accNo = DataFactory.getRandomNumberAsString(9);
            String provider = DataFactory.getDefaultProvider().ProviderName;
            String bank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0).BankName;

            Login.init(t1).login(net);
            BankAccountAssociation.init(t1).initiateBankAccountRegistration(sub, provider, bank, custId, accNo, false);

            String gradeChange = MobiquityGUIQueries.dbGetUserGrade(sub);

            Assertion.verifyEqual(gradeChange, Constants.GRADE_BANKSUBS, "migrate the subscriber grade to tier 1 grade ‘Banksubs ’", t1, false);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(sub, provider, bank);

            gradeChange = MobiquityGUIQueries.dbGetUserGrade(sub);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(enabled = false, priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0555() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0555", "To verify that if tier 3 grade subscribers" +
                " gets associated with banking services, system should migrate the subscriber" +
                " grade to tier 1 grade ‘Banksubs’.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            SuperAdmin sm;
            Grade newGrade;

            newGrade = new Grade(Constants.SUBSCRIBER);
            sm = DataFactory.getSuperAdminWithAccess("ADD_GRADES");

            newGrade.setGradeCode("farmer");
            newGrade.setGradeName("farmer");

            Login.init(t1).loginAsSuperAdmin(sm);
            //fetch grade status, if it doesnt exist only then create newly
            List<String> status = MobiquityGUIQueries.fetchGradeStatus("farmer");

            if (!status.contains("Y"))
                GradeManagement.init(t1).addGrade(newGrade);

            else
                Assertion.logAsPass("Grade Code already exist", t1);

            //Fetch the TCP and MobileROle for grade 'farmer'
            ResultSet result = MobiquityGUIQueries.fetchTCPIdAndMobileGroupRoleCode(newGrade.GradeCode, DataFactory.getDefaultWallet().WalletId, "Wallet");

            while (result.next()) {
                //If mobile role doesnt exist for grade then create otherwise skip
                if (result.getString("GROUP_ROLE_CODE") == null) {
                    GroupRoleManagement.init(t1).addMobileGroupRolesForSpecificGrade(sub, newGrade.GradeName,
                            Constants.PAYINST_WALLET_CONST_MOBILE_ROLE,
                            DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletName);
                } else {
                    t1.info("Mobile role for grade " + newGrade.GradeName + " already exists in the system");
                }

                //If TCP doesnt exist for grade then create otherwise skip
                if (result.getString("PROFILE_ID") == null) {
                    InstrumentTCP tcp = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                            DataFactory.getDomainName(Constants.SUBSCRIBER),
                            DataFactory.getCategoryName(Constants.SUBSCRIBER),
                            newGrade.GradeName,
                            "WALLET", DataFactory.getDefaultWallet().WalletName);
                    TCPManagement.init(t1).addInstrumentTCP(tcp);
                } else {
                    t1.info("TCP for grade " + newGrade.GradeName + " already exists in the system");
                }
            }

            //Creating User With New Grade
            sub = new User(Constants.SUBSCRIBER, newGrade.GradeName);

            sub.setRegistrationType(Constants.REGTYPE_NO_KYC);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub, true, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * Working on this Test Cases
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0557() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0555", "To verify that if tier 3 grade subscribers" +
                " gets associated with banking services, system should migrate the subscriber" +
                " grade to tier 1 grade 'BankSubs'.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            sub = new User(Constants.SUBSCRIBER, DataFactory.getGradeName("SUBS"));

            Login.init(t1).loginAsSuperAdmin("CAT_PREF");

            configureWalletPreferencesForGradeMigration(sub);

            //Creating User With New Grade
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub, true, false);

            String bank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0).BankName;

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess(Roles.BANK_ACCOUNT_REGISTRATION));

            BankAccountAssociation.init(t1).initiateBankAccountRegistration(sub, defaultProvider.ProviderName, bank, DataFactory.getRandomNumberAsString(7), DataFactory.getRandomNumberAsString(7), false);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.ADD_MODIFY_DELETE_BANK_ACCOUNTS_APPROVAL);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(sub, defaultProvider.ProviderName, bank);

            String gradeChange = MobiquityGUIQueries.dbGetUserGrade(sub);

            Assertion.verifyEqual(gradeChange, Constants.GRADE_BANKSUBS, "migrate the subscriber grade to tier 1 grade ‘Banksubs ’", t1, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    private void configureWalletPreferencesForGradeMigration(User subs) throws IOException {
        try {

            WalletPreference wPref = new WalletPreference(Constants.SUBSCRIBER,
                    Constants.BNK_LNKD_USR,
                    DataFactory.getDefaultWallet().WalletName, true,
                    null, subs.GradeName);
            wPref.setPreferenceType(Constants.PREFERENCE_TYPE_AUTOMATIC_GRADE_CHANGE);

            Login.init(pNode).loginAsSuperAdmin("CAT_PREF");

            Preferences.init(pNode).
                    configureWalletPreferences(wPref);

            //Added by other user
            /*WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);
            page.navMapWalletPreferences();

            page.selectPreferenceType(Constants.PREFERENCE_TYPE_AUTOMATIC_GRADE_CHANGE);

            page.selectDomainName(subs.DomainName);
            page.selectCategoryName(subs.CategoryName);
            page.selectRegTypeByVal(Constants.BNK_LNKD_USR);

            page.selectProvider(DataFactory.getDefaultProvider().ProviderName);
            page.selectPaymentInstrument(Constants.PAYINST_WALLET_CONST);
            page.selectWalletType(Constants.NORMAL_WALLET_STRING);
            page.selectGrade(grade);
            page.selectMobileRoleByIndex();
            page.selectTCPByIndex();
            page.selectPrimaryAccount(true);

            page.clickNext();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("subscriber.category.preferences.success", "Map Wallet Preferences", pNode);
            }*/
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }
}
