package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_ConfigurationForNumberOfMSISDNForSingleNationalID extends TestInit {

    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SINGLE_NATIONAL_ID_CONFIRGURATION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_UAT_0504() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0504", "To verify that the Valid user should be able to add Subscribers in System having same Identification Number.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SINGLE_NATIONAL_ID_CONFIRGURATION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.PUSHPALATHA);

        try {
            //Create a subscriber
            //(Creating a new subcriber since based on a preference only the mentioned number of subcribers can be created with same identification number)
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            //Create another subcriber with same indentification number as of previous subscriber
            User subscriber = new User(Constants.SUBSCRIBER);
            subscriber.setExternalCode(subs.ExternalCode);

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subscriber, true, false);

            Assertion.verifyEqual(subs.ExternalCode, subscriber.ExternalCode, "Two Subscribers are created with same Identification Number", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SINGLE_NATIONAL_ID_CONFIRGURATION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_USSD_0220() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_USSD_0220", "To verify that the Subscriber has two different wallet and two different PIN's to access Ecocash USSD menu.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SINGLE_NATIONAL_ID_CONFIRGURATION, FunctionalTag.ECONET_UAT_5_0);

        try {

            ConfigInput.changePin = false;

            User sub01 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(sub01);

            //Create another subscriber with same indentification number as of previous subscriber
            User sub02 = new User(Constants.SUBSCRIBER);
            sub02.setExternalCode(sub01.ExternalCode);

            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(sub02);
            Assertion.verifyEqual(sub01.ExternalCode, sub02.ExternalCode, "Two Subscribers are created with same Identification Number", t1);

            //Subscribers should be having 2 different Wallet numbers
            String walletNum01 = MobiquityGUIQueries.fetchWalletNumber(sub01.MSISDN);
            String walletNum02 = MobiquityGUIQueries.fetchWalletNumber(sub02.MSISDN);

            Assertion.verifyNotEqual(walletNum01, walletNum02, "Subscribers with different Wallet Number", t1);
            //Subscribers should be having 2 different PIN numbers
            String pin1 = "9478";
            String pin2 = "2750";

            Transactions.init(t1).changeSubscriberMPin(sub01, pin1);
            Transactions.init(t1).changeSubscriberMPin(sub02, pin2);

            Assertion.verifyNotEqual(pin1, pin2, "Subscribers with different PIN Number", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void TC_USSD_022() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_USSD_0220", "To verify that the Subscriber has two different wallet and two different PIN's to access Ecocash USSD menu.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SINGLE_NATIONAL_ID_CONFIRGURATION, FunctionalTag.ECONET_UAT_5_0);

        String pin = MobiquityGUIQueries.dbFetchPassWordFromEmailQueue("7711703586");

        System.out.println("#### - PIN :" + pin);


    }

}
