package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.features.bulkServiceLevelThreshold.BulkServiceLevelThreshold;
import framework.util.common.Assertion;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;

/*
 * Created by Nirupama MK on 05/10/2018.
 */
public class Suite_BulkServiceLevelThresholdManagement extends TestInit {


    /**
     * Removed Econet Tag as it is de-scoped in Econet release
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SERVICE_THRESHOLD})
    public void TC_ECONET_0597() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0597", "To verify that the User can download " +
                "the current configuration file which details the set of profiles " +
                "(it can be different for each service type-sender grade-sender SVA type-receiver grade-receiver" +
                " SVA type combination) against which the minimum, maximum and multiples of transaction amount" +
                " has been defined.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SERVICE_THRESHOLD);
        try {
            ArrayList<String> header = new ArrayList<String>();
            header.add("Add(A) or Modify(M)*");
            header.add("Service Code*");
            header.add("Sender Grade Code*");
            header.add("Receiver Grade Code*");
            header.add("Profile Name*");
            header.add("Multiples of*");
            header.add("Minimum Transaction Amount*");
            header.add("Maximum Transaction Amount*");
            header.add("Sender Payment Method Type(WALLET/BANK)*");
            header.add("Receiver Payment Method Type(WALLET/BANK)*");
            header.add("Sender Account Type ID/ Bank ID*");
            header.add("Receiver Account Type ID/ Bank ID*");
            header.add("Sender Provider ID*");
            header.add("Receiver Provider ID*");

            BulkServiceLevelThreshold.init(t1).
                    downloadCurrentConfiguration();

            for (int i = 0; i < header.size() - 1; i++) {

                String val = ExcelUtil.ReadDataFromExcel(FilePath.fileCurrentConfigProfile, 0, i);
                Assertion.verifyEqual(val, header.get(i), "Verify Configuration file Contains '"
                        + val + "' as header", t1);

            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

}