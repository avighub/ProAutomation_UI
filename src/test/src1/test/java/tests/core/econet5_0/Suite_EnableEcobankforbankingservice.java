package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

//* created by CharanTeja Muthe** on 28-MAR *//

public class Suite_EnableEcobankforbankingservice extends TestInit {

    //declraing variable//
    private User chanlUser;
    private User subs;
    private String bank1;
    private User userCreator;
    private OperatorUser modifyuser;
    private CurrencyProvider defaultProvider;
    private OperatorUser usrApprover;
    private OperatorUser optUser, bankApprover;
    private String bank;
    private String custId, provider;
    private String accNo;
    private OperatorUser usrCreator;
    private String userApprover;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");
        bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
        modifyuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
        defaultProvider = DataFactory.getDefaultProvider();
        custId = DataFactory.getRandomNumberAsString(6);
        accNo = DataFactory.getRandomNumberAsString(9);
        provider = DataFactory.getDefaultProvider().ProviderName;
        bank = DataFactory.getDefaultBankName(DataFactory.getDefaultProvider().ProviderId);

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECOBANK_BANKING_SERVICES})
    public void TC_ECONET_1141() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1141 ", "To verify that the All pre-requisites should executed successfully after bank addition.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECOBANK_BANKING_SERVICES);

        try {
            //* taking created subscriber details from datafactory*//
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Login.init(t1).login(usrCreator);

            BankAccountAssociation.init(t1).initiateBankAccountRegistration(subs, provider, bank, custId, accNo, false);
            Thread.sleep(2000);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(pNode).approveAssociatedBanksForUser(subs, provider, bank);

            //*checking details from database Mbk_Cust_accounts*//

            String accountType = MobiquityGUIQueries.getBankAccountStatus(custId);

            DBAssertion.verifyDBAssertionEqual(accountType, "02", "Account Type is verified", t1);


        } catch (Exception e) {
        }


    }

}
