package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Geography;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.GeographyManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_GeographyManagement extends TestInit {

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0666() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0666",
                "To verify that Users(ChannelUser and ChannelAdmin) other " +
                        "than superadmin/network admin should not able to " +
                        "Add/Update/Delete/suspend zone successfully.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT,
                        FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            try {
                OperatorUser chAdm = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
                Login.init(t1).login(chAdm);
                driver.findElement(By.id("GEOMASTER_ALL"));
                t1.fail("Geo Management link is displayed for Channel Admin");
            } catch (Exception e) {
                t1.pass("Geo Management link is not displayed for Channel Admin");
            }
            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);
                driver.findElement(By.id("GEOMASTER_ALL"));
                t1.fail("Geo Management link is displayed for Channel User");
            } catch (Exception e) {
                t1.pass("Geo Management link is not displayed for Channel User");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0667() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0667", "To verify that the Proper error message should get generated on screen if user delete Zone/Area on web having association with users.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            Geography geo = MobiquityGUIQueries.getAutomationZoneAndArea();
            t1.info("Zone associated with user is: " + geo.ZoneName);
            t1.info("Area associated with user is: " + geo.AreaName);

            startNegativeTest();
            Login.init(t1).login(opt);
            GeographyManagement.init(t1).deleteZone(geo);

            Assertion.verifyErrorMessageContain("grphdomain.operation.msg.domainactive", "Zone associated with user cannot be deleted", t1);

            GeographyManagement.init(t1).deleteArea(geo);

            Assertion.verifyErrorMessageContain("grphdomain.operation.msg.activeuser", "Area associated with user cannot be deleted", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

}
