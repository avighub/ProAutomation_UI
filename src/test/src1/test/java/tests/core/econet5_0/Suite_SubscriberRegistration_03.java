package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Created by Nirupama MK on 26/09/2018.
 */
public class Suite_SubscriberRegistration_03 extends TestInit {
    private User chAddSubs;
    private OperatorUser nwAdmin, billerRegistor, categoryCreator;
    private String allowedSpecial;
    private String notAllowedSpecial;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        try {
            allowedSpecial = "' @-";
            notAllowedSpecial = "*%^!";
            chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.WHOLESALER);
            billerRegistor = DataFactory.getOperatorUserWithAccess("UTL_CREG");
            categoryCreator = DataFactory.getOperatorUserWithAccess("UTL_MCAT");
            nwAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /*
        TC_ECONET_0505b1
        Subscriber - lastName txt field should allow alpha numeric and special charecter
        TC_ECONET_0505b2
        Wholesaler - lastName txt field should allow alpha numeric and special charecter

        TC_ECONET_0505b3
        Merchant - lastName txt field should allow alpha numeric and special charecter
        TC_ECONET_0505a4
        Biller - BillerName txt field should allow alpha numeric and special charecter
      */

    @Test(priority = 1, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG,})
    public void TC_ECONET_0505b1() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0505b1", "To verify that the While adding " +
                "Subscriber in system following modifications in last name field should work. " +
                "b. alpha-numeric and special characters should be allowed." +
                "(e.g. comma, dot, apostrophe, space, forward slash, hyphen) ");

        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG);

        try {
            User sub4 = new User(Constants.SUBSCRIBER);
            sub4.setLastName("SUBS" + DataFactory.getRandomNumber(5) + allowedSpecial);

            startNegativeTest();

            Login.init(t1).login(chAddSubs);

            SubscriberManagement.init(t1).addInitiateSubscriber(sub4);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                    "Subscriber Last-Name Text Field should Accept Alpha Numeric and Special Characters", t1);

            sub4.setLastName("SUBS" + DataFactory.getRandomNumber(5) + notAllowedSpecial);
            SubscriberManagement.init(t1).addInitiateSubscriber(sub4);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), true,
                    "Subscriber Last-Name Text Field should Not accept " + notAllowedSpecial, t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0505b2() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0505b2", "To verify that the While adding " +
                "Channel User (wholesaler) in system following modifications in last name field should work." +
                "b. alpha-numeric and special characters should be allowed." +
                "(e.g. comma, dot, apostrophe, space, forward slash, hyphen) ");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            User chUser = new User(Constants.WHOLESALER);

            chUser.setLastName("WHS" + DataFactory.getRandomNumber(5) + allowedSpecial);
            Login.init(t1).login(nwAdmin);

            ChannelUserManagement.init(t1)
                    .initiateChannelUser(chUser);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                    "Wholesaler's Last-Name Text Field should Accept Alpha Numeric and Special Characters ", t1);

            chUser.setLastName("WHS" + DataFactory.getRandomNumber(5) + notAllowedSpecial);
            ConfigInput.isAssert = false;
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(chUser);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), true,
                    "Wholesaler's Last-Name Text Field should should Not accept " + notAllowedSpecial, t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG})
    public void TC_ECONET_0505b3() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0505b3", "To verify that the While adding " +
                "Merchant in system following modifications in last name field should work. " +
                "b. alpha-numeric and special characters should be allowed." +
                "(e.g. comma, dot, apostrophe, space, forward slash, hyphen) ");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG);

        try {
            User mercchant = new User(Constants.MERCHANT);

            mercchant.setLastName("MER" + DataFactory.getRandomNumber(5) + allowedSpecial);
            Login.init(t1).login(nwAdmin);

            ChannelUserManagement.init(t1)
                    .initiateChannelUser(mercchant);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                    "Merchant's Last-Name Text Field should Accept Alpha Numeric and Special Characters", t1);

            mercchant.setLastName("MER" + DataFactory.getRandomNumber(5) + notAllowedSpecial);

            startNegativeTest();
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(mercchant);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), true,
                    "Merchant's Last-Name Text Field should should Not accept " + notAllowedSpecial, t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG})
    public void TC_ECONET_0505a4() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0505a4", "To verify that the While adding Biller" +
                " in system following modifications in name field should work. " +
                "a. Number of characters can be accommodated by the'Biller Name' field to 50 characters. ");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG);

        try {
            String providerName = DataFactory.getDefaultProvider().ProviderName;

            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.setBillerName("BILER" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t1).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(t1).login(billerRegistor);

            ConfigInput.isConfirm = false;
            ConfigInput.isAssert = false;

            BillerManagement.init(t1).initiateBillerRegistration(biller);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Biller Name should Accept 50 Characters", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /*
        TC_ECONET_0506b1
        Subscriber - FirstName txt field should allow alpha numeric and special charecter
        TC_ECONET_0506b2
        Wholesaler - FirstName txt field should allow alpha numeric and special charecter
        TC_ECONET_0506b3
        Merchant - FirstName txt field should allow alpha numeric and special charecter
    */

    @Test(priority = 5, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG})
    public void TC_ECONET_0506b1() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0506b1", "To verify that the While adding " +
                "Subscriber in system following modifications in 'first name' field should work. " +
                "it should accept Alpha-numeric and special characters (e.g comma, dot, apostrophe, space," +
                " forward slash, hyphen) should be allowed.");

        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG);
        try {

            User subs = new User(Constants.SUBSCRIBER);

            subs.setFirstName("SUBS" + DataFactory.getRandomNumber(4) + allowedSpecial);
            Login.init(t1).login(chAddSubs);
            SubscriberManagement.init(t1).addInitiateSubscriber(subs);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                    "Subscriber's First-Name Text Field should Accept Alpha Numeric and Special Characters", t1);

            subs.setFirstName("SUBS" + DataFactory.getRandomNumber(4) + notAllowedSpecial);
            SubscriberManagement.init(t1).addInitiateSubscriber(subs);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), true,
                    "Subscriber's First-Name Text Field should should Not accept " + notAllowedSpecial, t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG})
    public void TC_ECONET_0506b2() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0506b2", "To verify that the While adding " +
                "Wholesaler in system following modifications in First name field should work. " +
                "alpha-numeric and special characters should be allowed." +
                "(e.g. comma, dot, apostrophe, space, forward slash, hyphen) ");

        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG);

        try {
            User whs = new User(Constants.WHOLESALER);

            whs.setFirstName("WHS" + DataFactory.getRandomNumber(4) + allowedSpecial);
            Login.init(t1).login(nwAdmin);

            ChannelUserManagement.init(t1)
                    .initiateChannelUser(whs);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                    "Wholesaler's First-Name Text Field should Accept Alpha Numeric and Special Characters", t1);

            whs.setFirstName("WHS" + DataFactory.getRandomNumber(4) + notAllowedSpecial);
            ConfigInput.isAssert = false;
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(whs);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), true,
                    "Wholesaler's First-Name Text Field should should should Not accept " + notAllowedSpecial, t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG})
    public void TC_ECONET_0506b3() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0506b3", "To verify that the While adding " +
                "Merchant in system following modifications in First name field should work. " +
                "alpha-numeric and special characters should be allowed." +
                "(e.g. comma, dot, apostrophe, space, forward slash, hyphen) ");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG);

        try {
            User mercchant = new User(Constants.MERCHANT);

            mercchant.setFirstName("MER" + DataFactory.getRandomNumber(5) + allowedSpecial);
            Login.init(t1).login(nwAdmin);

            ChannelUserManagement.init(t1)
                    .initiateChannelUser(mercchant);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                    "Merchant's First-Name Text Field should Accept Alpha Numeric and Special Characters", t1);

            mercchant.setFirstName("MER" + DataFactory.getRandomNumber(5) + notAllowedSpecial);
            Login.init(t1).login(nwAdmin);

            ConfigInput.isAssert = false;
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(mercchant);

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), true,
                    "Merchant's First-Name Text Field should Not accept " + notAllowedSpecial, t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
