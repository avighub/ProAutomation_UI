package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Domain;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.domainCategoryManagement.DomainManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_DomainManagement extends TestInit {

    private OperatorUser opt;
    private SuperAdmin saAddGrade;


    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        // ExtentTest t1 = pNode.createNode("SetUp", "Creation Of Domain");
        opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        saAddGrade = DataFactory.getSuperAdminWithAccess("ADD_GRADES");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_UAT_0039() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0039", "To verify that valid user should be able to add domain in the system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.PUSHPALATHA);
        try {

            Domain dom = new Domain();
            Login.init(t1).login(opt);
            DomainManagement.init(t1).addDomain(dom);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.DOMAIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_SIT_0759() throws Exception {

        ExtentTest t2 = pNode.createNode("TC_ECONET_0759", "To verify that Domain name and code must be unique in the system.");
        t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.DOMAIN_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            startNegativeTestWithoutConfirm();
            Login.init(t2).login(opt);
            Domain dom2 = new Domain();
            dom2.setDomainCode(whs.DomainCode);
            DomainManagement.init(t2).addDomain(dom2);
            Assertion.verifyErrorMessageContain("domainCode.already.exists", "Domain addition failure", t2);

            stopNegativeTestWithoutConfirm();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t2);
        }

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.DOMAIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0041() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0041", "To verify that the No option should be available on web to delete the domain in system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.DOMAIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);
        try {

            Login.init(t1).login(opt);

            try {
                fl.contentFrame();
                driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
                fl.clickLink("DOMMANAGE_ALL");
                driver.findElement(By.xpath("//a[contains(text(),'Delete Category')]"));
                t1.fail("Delete Domain link is displayed");

            } catch (Exception e) {
                t1.pass("Delete Domain link is not displayed");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.DOMAIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0040() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0040", "To verify that Only superadmin should be able to add domain in the system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.DOMAIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);
        try {

            Login.init(t1)
                    .loginAsSuperAdmin(saAddGrade);

            try {
                fl.contentFrame();
                driver.findElement(By.id("DOMMANAGE_ALL"));

                t1.fail("Domain link is displayed");

            } catch (Exception e) {
                t1.pass("Domain link is not displayed");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }


}
