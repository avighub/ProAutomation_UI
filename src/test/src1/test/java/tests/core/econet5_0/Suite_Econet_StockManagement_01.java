package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.WebGroupRole;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.stockManagement.StockApproval_Page1;
import framework.pageObjects.stockManagement.StockInitiation_Page1;
import framework.pageObjects.stockManagement.StockInitiation_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by vandana.rattan on 30-10-2018.
 */
public class Suite_Econet_StockManagement_01 extends TestInit {

    private OperatorUser stockInitiator, optAppNetworkStockL1, stockReimbUser, stockEAInitiator, stockEAApprover1, stockWithdrawer;
    private OperatorUser optWithoutRoles;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Create a Network Admin with Specific Permission");

        try {
            String[] notRequiredRoles = {"STOCK_REINIT", "STOCK_LIMIT", "STOCK_APP1", "STOCK_APP2", "STR_INIT", "STOCKTR_LIMIT", "STOCK_ENQ", "STK_LOY_WITHDRAW", "STK_WITHDRAW"};

            ArrayList<String> applicableRole = new ArrayList<>(Arrays.asList("CHECK_ALL"));
            WebGroupRole webGroupRole = new WebGroupRole(Constants.NETWORK_ADMIN, "AUTNWADM1", applicableRole, 1);

            Login.init(s1).loginAsSuperAdmin("GRP_ROL");
            GroupRoleManagement.init(s1)
                    .addWebGroupRole(webGroupRole);

            // now make sure that this role doesnt have notRequiredRoles
            GroupRoleManagement.init(s1).removeSpecificRole(webGroupRole, notRequiredRoles);

            // now create a network admin with this specific Role
            optWithoutRoles = new OperatorUser(Constants.NETWORK_ADMIN);
            optWithoutRoles.WebGroupRole = webGroupRole.RoleName;

            OperatorUserManagement.init(s1).createAdmin(optWithoutRoles);


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0349() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0349",
                "Stock Withdraw: To Verify stock cannot be initiated more than the MAX_TRF_AMT preference defined in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            stockInitiator = DataFactory.getOperatorUserWithAccess("STOCK_INIT", Constants.NETWORK_ADMIN);
            Login.init(t1).login(stockInitiator);

            BigDecimal stockAMt = new BigDecimal(MobiquityGUIQueries.fetchDefaultValueOfPreference("MAX_TRF_AMT"));

            stockAMt = stockAMt.divide(AppConfig.currencyFactor).add(BigDecimal.ONE);

            startNegativeTest();
            StockManagement.init(t1)
                    .initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                            DataFactory.getDefaultBankNameForDefaultProvider(), stockAMt.toString());

            Assertion.verifyErrorMessageContain("00031", "Assert Error message", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(groups = {FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0652() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0652 : Stock Withdraw", "To verify that User other than valid user can not set the Stock Limit.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_LMT");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0653() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0653 : Stock Approval-1", "To verify that User other than valid user can not successfully approve the stock at level-1 and request will close at this level when requested quantity is less than Approval limit 1.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_APP1");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0654() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0654 : Stock Approval-2", "To verify that User other than valid user can not successfully approve the stock at level 2.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_APP2");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0656() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0656 ", "To verify that User other than valid user is not able to Initiate Stock Transfer to EA through web.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STR_INIT");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0655() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0655: Stock Transfer Limit for  EA  ", "To verify that User other than valid user can not set the Stock Transfer Limit for EA.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STR_LMT");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0658() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0658: Stock Transfer EA Enquiry    ", "To verify that User other than valid user can't enquire the details of stock Transfer to EA..");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_ENQ");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0657() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0657: Stock Enquiry ", "To verify that User other than valid user can't enquire the Stocks through stock enquiry.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCKTR_ENQ");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0659() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0659 ", "To verify that the User other than valid user is not able to initiate the reimbursement through web.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_REMB");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0660() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0660 ", "To verify that the User other than valid user is not able to perform Stock withdrawl.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_WITHDRAWDM");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1223() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1223 ", "To verify that User other than valid user can not set the Stock Transfer Limit for EA").assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STR_LMT");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1224() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1224 ", "To verify that User other than valid user is not able to Initiate Stock Transfer to EA through web.").assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STR_INIT");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1225() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1225 ", "To verify that User other than valid user can't enquire the Stocks through stock enquiry.").assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_ENQ");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0661() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0661", "To verify that the User other than valid user is not able to perform Loyalty Stock Withdrawal.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0)
                .assignAuthor(Author.PRASHANT);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_LOY_WITHDRAWDM");
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0662() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0662", "To verify that the User other than valid user(to whom the role of stock management is not assigned) is not able to perform Stock withdrawl.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0)
                .assignAuthor(Author.PRASHANT);
        try {
            Login.init(t1).login(optWithoutRoles);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("STOCK_ALL", "STOCK_STK_WITHDRAWDM");
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0970() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0970", "To verify that Network Admin can initiate the stock with requested quantity having decimal value.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        String refNo = DataFactory.getRandomNumberAsString(7);

        try {
            stockInitiator = DataFactory.getOperatorUserWithAccess("STOCK_INIT", Constants.NETWORK_ADMIN);
            optAppNetworkStockL1 = DataFactory.getOperatorUserWithAccess("STOCK_APP1");
            Login.init(t1).login(stockInitiator);
            StockInitiation_Page1 page1 = StockInitiation_Page1.init(t1);
            page1.navigateToStockInitiationPage();
            page1.selectProviderName(DataFactory.getDefaultProvider().ProviderName);
            page1.selectBankName(DataFactory.getDefaultBankNameForDefaultProvider());
            page1.setReferenceNumber(refNo);
            page1.setStockAmount(Constants.STOCK_TRANSFERINDECIMAL1);
            page1.setRemarks(Constants.REMARKS);
            page1.clickSubmit();
            StockInitiation_Page2 page2 = new StockInitiation_Page2(t1);
            Assertion.verifyEqual(page2.getRequesterName(), stockInitiator.getFirstNameAndLastName(), "Verify Requester Name", t1, true);
            Assertion.verifyEqual(page2.getAmount(), Constants.STOCK_TRANSFERINDECIMAL1, "Verify Amount", t1, false);
            Assertion.verifyEqual(page2.getRemarks(), Constants.REMARKS, "Verify Remarks", t1, false);
            Assertion.verifyEqual(page2.getRefNo(), refNo, "Verify RefNo", t1, false);
            page2.clickConfirm();
            String txnId;
            String msg = Assertion.getActionMessage();
            if (ConfigInput.isCoreRelease) {
                txnId = msg.split("ID: ")[1].split(" ")[0];
            } else {
                txnId = msg.split("ID :")[1].split(" ")[0];
            }
            Assertion.verifyActionMessageContain("stock.initate.success", "Initiate Network Stock", t1, txnId);

            Login.init(t1).login(optAppNetworkStockL1);

            StockApproval_Page1 StockApproval_page1 = StockApproval_Page1.init(t1);
            StockApproval_page1.navigateToStockApproval1Page();
            String actualStatus = driver.findElement(By.xpath("//input[@value='" + txnId + "']/following::td[6]")).getText();
            Assertion.verifyEqual(actualStatus, Constants.Stock_Statuslevel1, "Assert Stock Current Status", t1, true);

            //  Assertion.verifyEqual(page2.getWallet(),DataFactory.getDefaultBankNameForDefaultProvider(),"Verify Bank",t1,false);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
