package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.pricingEngine.PricingEngine;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.jigsaw.CommonOperations.getAvailableBalanceForOperatorUsersBasedOnWalletNo;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : C2C
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 3/09/2018
 */

public class Suite_ChannelToChannelTransfer extends TestInit {

    private static OperatorUser usrSChargeSuspResume, networkAdmin;
    private static ServiceCharge sCharge;
    private User payer, payee, availablePayer, availablePayee;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        ExtentTest t1 = pNode.createNode("setup", "Create Users");

        payer = new User(Constants.WHOLESALER);
        payee = new User(Constants.WHOLESALER);

        /*//Adding Grade
        saAddGrade = DataFactory.getSuperAdminWithAccess("ADD_GRADES");
        Login.init(t1).loginAsSuperAdmin(saAddGrade);
        Grade newGrade = new Grade(Constants.WHOLESALER);
        GradeManagement.init(t1).addGrade(newGrade);

        //Adding Group Role
        GroupRoleManagement.init(t1).
                addMobileGroupRolesForSpecificGrade(payer, newGrade.GradeName, Constants.PAYINST_WALLET_CONST_MOBILE_ROLE, DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletName);

        //Adding TCP
        tcp = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDomainName(Constants.WHOLESALER),
                DataFactory.getCategoryName(Constants.WHOLESALER),
                newGrade.GradeName,
                "WALLET", DataFactory.getDefaultWallet().WalletName);
        TCPManagement.init(t1).addInstrumentTCP(tcp);

        //Creating Users With New Grade
        payer.setGradeName(newGrade.GradeName);
        payee.setGradeName(newGrade.GradeName);

        ChannelUserManagement.init(t1).createChannelUserDefaultMapping(payer, false);
        TransactionManagement.init(t1).makeSureChannelUserHasBalance(payer);

        ChannelUserManagement.init(t1).createChannelUserDefaultMapping(payee, false);
*/
        networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        availablePayer = DataFactory.getChannelUserWithAccess("C2C", 0);
        availablePayee = DataFactory.getChannelUserWithAccess("C2C", 1);
        usrSChargeSuspResume = DataFactory.getOperatorUsersWithAccess("SVC_SUS").get(0);

        sCharge = new ServiceCharge(Services.C2C, availablePayer, availablePayee, null, null, null, null);
        ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);

    }


    public void checkPre(User payerMSISDN, User payeeMSISDN) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payerMSISDN, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payeeMSISDN, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payerMSISDN, User payeeMSISDN) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payerMSISDN, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payeeMSISDN, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1011() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1011",
                "To verify that wholesaler can transfer the points to another channel user which are associated with the grades created manually.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        try {

            checkPre(payer, payee);
            //Login with valid user
            Login.init(t1).login(payer);

            TransactionManagement.init(t1).performC2C(payee, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            checkPost(payer, payee);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1012_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1012_a",
                "To verify that  wholesaler can not initiate C2C if service charge is not defined");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        try {

            BigDecimal initialServiceCharge = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int initialAmount = initialServiceCharge.intValue();

            //Deleting Service Charge
            Login.init(t1).login(networkAdmin);
            PricingEngine.init(t1).deleteServiceChargeForDefaultCurrency(sCharge, "Service");
            //Login with valid user
            Login.init(t1).login(availablePayer);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(availablePayer);
            TransactionManagement.init(t1).performC2C(availablePayee, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            BigDecimal finalServiceCharge = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int finalAmount = finalServiceCharge.intValue();

            Assert.assertEquals(initialAmount, finalAmount);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0072() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0072",
                "To verify network admin is able to suspend/resume service charge.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0);

        try {

            checkPre(availablePayer, availablePayee);
            Login.init(t1).login(usrSChargeSuspResume);
            ServiceChargeManagement.init(t1).suspendServiceCharge(sCharge);

            //to resume Service Charge
            Login.init(t1).login(usrSChargeSuspResume);
            ServiceChargeManagement.init(t1).resumeServiceCharge(sCharge);

            checkPost(availablePayer, availablePayee);

            //DBAssertion.verifyPrePostBalanceEqual(DBAssertion.prePayerBal, DBAssertion.postPayerBal,"Verify Balance Equal",t1);

            //DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            sCharge = new ServiceCharge(Services.C2C, availablePayer, availablePayee, null, null, null, null);
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1010_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1010_a",
                "To verify that C2C service will fail if the Sender is blocked");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        try {
            //Perform Transaction with Incorrect Mpin to block User

            Transactions.init(t1).BalanceEnquiryWithInvalidMpinTpin(availablePayer, "3222", ConfigInput.tPin).assertMessage("invc2c.label.transfer.confirm.fail.mpin");
            Transactions.init(t1).BalanceEnquiryWithInvalidMpinTpin(availablePayer, "3222", ConfigInput.tPin).assertMessage("invc2c.label.transfer.fail.nxtattempt.block");
            Transactions.init(t1).BalanceEnquiryWithInvalidMpinTpin(availablePayer, "3222", ConfigInput.tPin).assertMessage("invc2c.label.transfer.fail.block");

            Login.init(t1).login(availablePayer);
            startNegativeTest();
            TransactionManagement.init(t1).performC2C(availablePayee, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());
            Assertion.verifyErrorMessageContain("payer.barred.sender", "Verify Error Message", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            Login.init(t1).login(networkAdmin);
            availablePayer.setStatus(Constants.BAR_AS_SENDER);
            ChannelUserManagement.init(t1).unBarChannelUser(availablePayer, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_SENDER);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1010_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1010_a",
                "To verify that C2C service will fail if the receiver is blocked");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        try {
            Login.init(t1).login(networkAdmin);
            ChannelUserManagement.init(t1).barChannelUser(availablePayee, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_RECIEVER);

            Login.init(t1).login(availablePayer);
            startNegativeTest();
            TransactionManagement.init(t1).performC2C(availablePayee, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());
            Assertion.verifyErrorMessageContain("payee.C2C.barred.reciever", "Verify Error Message", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            Login.init(t1).login(networkAdmin);
            ChannelUserManagement.init(t1).unBarChannelUser(availablePayee, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_RECIEVER);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1184() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1184",
                "To verify that valid channel user is not able to perform C2C service successfully if invalid details are entered.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(networkAdmin);

            Login.init(t1).login(availablePayer);
            startNegativeTest();
            TransactionManagement.init(t1).performC2C(availablePayer, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());
            Assertion.verifyErrorMessageContain("sender.receiver.same", "Verify Error Message", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * //todo Need to update the If-Else Condition
     * Need discussion with DQA Team
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0573() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0573","To verify that the Merchants and agents should be able to perform " +
                "C2C in between without any service charge if parent user is same hierarchy.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try{

            User payer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER,Constants.RETAILER,0);
            User payee = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER,Constants.RETAILER,1);

            ServiceCharge sc = new ServiceCharge(Services.C2C,payer,payee,null,null,null,null);
            ServiceChargeManagement scMgmtObj = ServiceChargeManagement.init(t1);
            String scStatus = scMgmtObj.checkServiceChargeStatus(sc);
            t1.info("Service Charge Status is :=>" +scStatus);

            Login.init(t1).login(payer);
            if(scStatus.equalsIgnoreCase("N")){
                TransactionManagement.init(t1).performC2C(payee,"100",DataFactory.getRandomNumberAsString(6));
            }else if(scStatus.equalsIgnoreCase("Y")){
                scMgmtObj.deleteServiceChargeAllVersions(sc);
                TransactionManagement.init(t1).performC2C(payee,"100",DataFactory.getRandomNumberAsString(6));
            }else {
                t1.skip("Skipping the case as Service charge is already created and present in Suspended/Initiated state");
            }

        }catch (Exception e){
            markTestAsFailure(e,t1);
        }

        Assertion.finalizeSoftAsserts();

    }

}
