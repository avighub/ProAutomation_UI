package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.pricingEngine.PricingEngine;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.pricingEng.PricingEngine_Page1;
import framework.pageObjects.pricingEng.PricingEngine_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.jigsaw.CommonOperations.getAvailableBalanceForOperatorUsersBasedOnWalletNo;


public class Suite_PricingEngine extends TestInit {
    private static String[] serviceRuleValues;
    private static String[] saveDraftValues;
    private static String[] commissionRuleValues;
    private static OperatorUser OpUser;
    private Wallet commWallet;
    private OperatorUser netPricingEngAdd, netPricingEngApp;
    private User wholesaler, subscriber, wholesaler1;

    @BeforeClass(alwaysRun = true)
    public void preRequisite() throws Exception {
        try {
            netPricingEngAdd = DataFactory.getOperatorUserWithAccess(Roles.ADD_EDIT_DELETE_PRICING_POLICY_INITIATION, Constants.NETWORK_ADMIN);
            netPricingEngApp = DataFactory.getOperatorUserWithAccess(Roles.PRICING_POLICY_APPROVAL, Constants.NETWORK_ADMIN);
            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
            commWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
            wholesaler1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0096() throws Throwable {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0096",
                "To verify that network admin should be able to view pricing engine module on web.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            PricingEngine.init(t1)
                    .viewPricingEngine(false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0097() throws Throwable {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0097",
                "To verify that user should be redirected to a new page for pricing engine.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            PricingEngine.init(t1).viewPricingEngine(true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 3, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0098() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0098",
                "To verify that Network admin should be able to  " +
                        "add service charge through Pricing engine module successfully.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);

            serviceRuleValues = PricingEngine.init(t1).addServiceCharge(sCharge, "SERVICE");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 4, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0099() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0099",
                "To verify that network admin should be able to " +
                        "reorder the rule if the priority/ranking needs to be lowered.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {

            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);

            String[] serviceChargeName1 = PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "SERVICE", "ZWL", "1");
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addApprovalForServiceCharge("Cash In", "SERVICE", netPricingEngAdd.LoginId);

            ServiceCharge sCharge1 = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);
            String[] serviceChargeName2 = PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge1, "SERVICE", "ZWL", "3");
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addApprovalForServiceCharge("Cash In", "SERVICE", netPricingEngAdd.LoginId);

            PricingEngine.init(t1).reorderPricingRules(sCharge, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0100() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0100",
                "To verify that network admin should be able to Reorder the set of rules" +
                        " – This will change the relative priority of the rules")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);

            PricingEngine.init(t1).reorderPricingRules(sCharge, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0107() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0107a",
                "To verify that network admin should be able to make a rule inactive")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        /*ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber,
                null, null, null, null);*/
        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);

            PricingEngine.init(t1).changeStatus(sCharge, serviceRuleValues[0], "S");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        t1 = pNode.createNode("TC_ECONET_0107b",
                "To verify that network admin should be able to make a rule active")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).changeStatus(sCharge, serviceRuleValues[0], "A");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0110() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0110",
                "To verify that service charge policy should be " +
                        "segmented into three rule types : charge rule, waive rule, tax rule.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).checkSubMenusForServiceCharge(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0111() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0111",
                "To verify that commission policy should be " +
                        "segmented into two rule types : commission rule, tax rules.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).checkSubMenusForCommission(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0104() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0104",
                "To verify that user should also be able make " +
                        "commission profile with the same pricing engine module.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);

            PricingEngine.init(t1).addServiceCharge(sCharge, "COMMISSION");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 10, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0103() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0103",
                "To verify that network admin should be able to modify the existing rule," +
                        "i.e. changing the rule level settings or the statement or the conditions for executing the rule.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);

            String[] serviceChargeName1 = PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "SERVICE", "ZWL", "1");
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addApprovalForServiceCharge("Cash In", "SERVICE", netPricingEngAdd.LoginId);

            PricingEngine.init(t1).modifyCharge(sCharge, "SERVICE", serviceChargeName1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 11, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0105() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0105",
                "To verify that user should be able to calculate service charge " +
                        "using pricing calculator module within pricing engine.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.ACCESS_TO_CHARAGE_CALCULATOR);

            PricingEngine.init(t1).
                    pricingCalculator(sCharge, "SERVICE");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 12, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0106() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0106",
                "To verify that user should be able to save the newly made policy " +
                        "by clicking on the button save as draft on top right of the screen.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            saveDraftValues = PricingEngine.init(t1).saveDraftServiceCharge(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 13, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0121() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0121",
                "To verify that if the user has saved any policy as draft" +
                        " he should be able to see the saved draft.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            saveDraftValues = PricingEngine.init(t1).saveDraftServiceCharge(sCharge);
            Login.resetLoginStatus();
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).viewRule(sCharge, "SERVICE", saveDraftValues[0]);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 14, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0125() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0125",
                "To verify that user can easily create a duplicate service charge by clicking" +
                        " on the clone option available on the service charge already made.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            saveDraftValues = PricingEngine.init(t1).saveDraftServiceCharge(sCharge);

            Login.resetLoginStatus();
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).cloneThePolicy(sCharge, "SERVICE", saveDraftValues[0]);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 15, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0123() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0123",
                "To verify that user should be able to discard Policy Draft – When user chooses to discard the policy draft, " +
                        "the user will be able to see the latest approved /applied version of the policy with all its data")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0);
        User chnlUser, subs;
        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            String[] values = PricingEngine.init(t1).saveDraftServiceCharge(sCharge);
            PricingEngine_Page2.init(t1).discardPolicy();
            PricingEngine_Page2.init(t1).clickBtnToDiscard();
            boolean exist = PricingEngine_Page1.init(t1).isPolicyExist(values[0]);
            String actualMessage = String.valueOf(exist);
            Assertion.assertEqual(String.valueOf(exist), String.valueOf(false), "Verify Service Charge discarded ", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 16, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0085() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0085",
                "To verify that the Correct tax is credited in the correct wallet depending upon type of Transaction, service charge and commission.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {

            BigDecimal initialTaxAmount = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "INDTAX02");
            int initialAmount = initialTaxAmount.intValue();


            UsrBalance channelUsrBalance = MobiquityGUIQueries.getUserBalance(wholesaler, commWallet.WalletId, null);
            int preBalance = channelUsrBalance.Balance.intValue();

            ServiceCharge sCharge = new ServiceCharge(Services.C2C, wholesaler, wholesaler1, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);

            PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "SERVICE", "mfs1.currency.code", "2");
            PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "COMMISSION", "mfs1.currency.code", "1");
            PricingEngine.init(t1).addTax(sCharge, "1");
            //Perform Service

            ServiceChargeManagement.init(pNode)
                    .configureServiceCharge(sCharge);

            Login.init(t1).login(wholesaler);

            TransactionManagement.init(t1).performC2C(wholesaler1, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            BigDecimal finalTaxAmount = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "INDTAX02");
            int finalAmount = finalTaxAmount.intValue();

            UsrBalance channelUsrPostBalance = MobiquityGUIQueries.getUserBalance(wholesaler, commWallet.WalletId, null);
            int postBalance = channelUsrPostBalance.Balance.intValue();


            Assertion.assertEqual(initialAmount + 1, finalAmount, "Tax is Applied Successfully", t1);
            Assert.assertEquals(preBalance + 1, postBalance);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 17, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0108() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0108",
                "To verify that If none of rule conditions match across the set of rules in the policy," +
                        " then no charges should be calculated, and hence zero charges would apply.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);
        User chnlUser;
        User subsc;
        try {
            chnlUser = DataFactory.getChannelUserWithAccessAndGrade("CIN_WEB", "Gold Retailer");
            subsc = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            Login.init(t1).login(chnlUser);
            String transID = TransactionManagement.init(t1).
                    performCashIn(subsc, Constants.CASHIN_TRANS_AMOUNT, null);

            String expected = Constants.CASHIN_TRANS_AMOUNT;
            String actual = MobiquityDBAssertionQueries.getRequestedAmount(transID).toString();
            Assertion.assertEqual(actual, expected, "Verifying that service charge is not applied for the transaction.", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 18, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0109() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0109",
                "To verify that If for a particular request, multiple rule conditions are matching, only the highest rank rule " +
                        "should be checked and the charges should be calculated based on the statements defined for the rule.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);
        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "SERVICE", "INR", "4");

            sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            String[] servName = PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "SERVICE", "INR", "2");

            Login.init(t1).login(netPricingEngAdd);
            String[] value = PricingEngine.init(t1).pricingCalculator(sCharge, "SERVICE");
            Assertion.assertEqual(value[1], servName[0], "Verifying that the highest ranking rule is applicable.", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 19, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0115() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0115",
                "To verify that user is able to view the previous versions of the policy.  ")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);

            PricingEngine.init(t1).checkPreviousPolicyVersion(sCharge, "SERVICE");

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 20, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0122() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0122",
                "To verify that user should be able to revert to Last Saved Draft – When user chooses to revert to last saved, " +
                        "all the changes applied after saving the last draft is reverted. " +
                        "If no drafts have been created, then it reverts to the last approved/applied version of the policy.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            String[] servName = PricingEngine.init(t1).addServiceCharge(sCharge, "SERVICE");
            PricingEngine.init(t1).modifyCharge(sCharge, "SERVICE", servName, false);
            PricingEngine_Page2.init(t1).revertToLastSaved();
            PricingEngine_Page2.init(t1).clickBtnToRevert();
            String prevValue = PricingEngine_Page2.init(t1).getPricingFixedValue();
            Assertion.assertEqual(prevValue, "0.05", "Service Charge reverted to previous value.", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 21, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1196() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1196",
                "To verify that User other than Network admin should not able to view pricing engine module on web.");

        t1.assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        OpUser = DataFactory.getOptUserWithOutAccess(Roles.ADD_EDIT_DELETE_PRICING_POLICY_INITIATION, t1);

        Login.init(t1).login(OpUser);

        FunctionLibrary.init(t1).verifyLinkNotAvailable("SHULKA_ALL", "SHULKA");
    }

    /**
     * @throws Exception
     */
    @Test(priority = 22, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0118() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0118",
                "To verify that the Transaction should be successful where" +
                        " service charge tax is paid by charge payer.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);
        User chnlUser;
        User subsc;
        try {
            chnlUser = DataFactory.getChannelUserWithAccessAndGrade("CIN_WEB", DataFactory.getGradeName(Constants.GOLD_WHOLESALER));
            subsc = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "SERVICE", AppConfig.defaultCurrency, "3");
            PricingEngine.init(t1).addTax(sCharge, "1");
            Login.init(t1).login(chnlUser);
            String transID = TransactionManagement.init(t1).
                    performCashIn(subsc, Constants.CASHIN_TRANS_AMOUNT, null);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 23, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0119() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0119",
                "To verify that the Transaction should be successful where " +
                        "commission tax is paid by commission reciever.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);
        User chnlUser;
        User subsc;
        try {
            chnlUser = DataFactory.getChannelUserWithAccessAndGrade("CIN_WEB", DataFactory.getGradeName(Constants.GOLD_WHOLESALER));
            subsc = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addServiceCharge(sCharge, "COMMISSION");
            Login.init(t1).login(chnlUser);
            String transID = TransactionManagement.init(t1).
                    performCashIn(subsc, Constants.CASHIN_TRANS_AMOUNT, null);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 24, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0114() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0114",
                "Pricing Engine ->  Search for Rules" +
                        "To verify that user is able to search for rules from anywhere in the Pricing Engine. ")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {

            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).searchRules(sCharge, "SERVICE");
        } catch (Exception e) {
            //markTestAsFailure(e, t1);
            Assertion.raiseExceptionAndStop(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 25, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0116() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0116",
                "To verify that the Proper error should get displayed on web if user trying to add service charge which is " +
                        "already in approval stage for the same combination.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {

            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addServiceCharge(sCharge, "SERVICE");

            startNegativeTest();
            PricingEngine.init(t1).addServiceCharge(sCharge, "SERVICE");

            String expectedMessage = driver.findElement(By.xpath(".//*[@id='service-charge-policy']//section/ul/li")).getText();

            Assert.assertEquals(expectedMessage, "A policy for this service has already been submitted for approval. You can resubmit this policy for approval only if the earlier policy is rejected");
            t1.pass("Proper error message is dispayed if service charge initiated is already in approval stage for the same combination");

            PricingEngine.init(t1).addApprovalForServiceCharge("Cash In", "SERVICE", netPricingEngAdd.LoginId);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 26, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0124() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0124",
                "To verify that user should be able to " +
                        "reject the policy  pending for approval.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {

            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addServiceCharge(sCharge, "SERVICE");

            PricingEngine.init(t1).rejectServiceCharge("Cash In", "SERVICE", netPricingEngAdd.LoginId);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 27, groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0101() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0101",
                "To verify that network admin should be able to make a rule active or inactive – If the rule is inactive, " +
                        "then the rule conditions are not checked for when the Pricing Engine is asked to calculate the charges.\n")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);

        try {

            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);
            String[] serviceChargeName1 = PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "SERVICE", "USD", "1");
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addApprovalForServiceCharge("Cash In", "SERVICE", netPricingEngAdd.LoginId);

            ServiceCharge sCharge1 = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);
            String[] serviceChargeName2 = PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge1, "SERVICE", "USD", "3");
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addApprovalForServiceCharge("Cash In", "SERVICE", netPricingEngAdd.LoginId);

            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).changeStatus(sCharge, serviceChargeName2[0], "S");
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addApprovalForServiceCharge("Cash In", "SERVICE", netPricingEngAdd.LoginId);

            Login.init(t1).login(netPricingEngAdd);
            String[] values = PricingEngine.init(t1).pricingCalculatorCommon(sCharge, "SERVICE", "1.00");
            t1.info("Service charge amount: " + values[0] + " and Service charge name: " + values[1]);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0512() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0512",
                "To verify that the Valid user should be able to define service charge with TAX value with Fixed and Percentage amount.")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);
        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
            Login.init(t1).login(netPricingEngAdd);
            PricingEngine.init(t1).addServiceChargeTaxationRule(sCharge, "SERVICE");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * //TODO Complete the Test Case
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0}, invocationCount = 2, enabled = false)
    public void sameSession() throws Exception {
        ExtentTest t1 = pNode.createNode("Same Session",
                "To Do")
                .assignCategory(FunctionalTag.PRICING_ENGINE, FunctionalTag.ECONET_UAT_5_0);
        try {
            ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber,
                    null, null, null, null);

            Login.init(t1).login(netPricingEngAdd);

            //PricingEngine.init(t1).addServiceCharge(sCharge,"SERVICE");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
