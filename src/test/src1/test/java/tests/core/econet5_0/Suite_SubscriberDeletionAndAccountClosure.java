package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.Map;

public class Suite_SubscriberDeletionAndAccountClosure extends TestInit {

    private User chAddSubs, chApproveSubs, chUser;
    private OperatorUser optUser, bankApprover;
    private String prefValue, provider, bank, bank1, custId, accNo;
    private boolean pref;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        try {
            chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");

            chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("ACCOUNT_CLOSURE_TWO_STEP");
            boolean pref = AppConfig.isAccountClosureTwoStep;
            bankApprover = DataFactory.getOperatorUsersWithAccess("BNK_APR").get(0);
            provider = DataFactory.getDefaultProvider().ProviderName;
            bank = DataFactory.getDefaultBankName(DataFactory.getDefaultProvider().ProviderId);
            bank1 = DataFactory.getNonTrustBankName(DataFactory.getDefaultProvider().ProviderName);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();


    }

    @BeforeMethod(alwaysRun = true)
    public void generateCustIDandAccNO() throws Exception {

        custId = DataFactory.getRandomNumberAsString(6);
        accNo = DataFactory.getRandomNumberAsString(9);

    }

    /*  create subscriber with multiple wallets
        perform cashNI
        shlould not be able to delete Subscriber, "Only primary wallet can be deleted"
        Modify Subscriber to delete Non-primary wallets
        should be able to delete Subscriber
        perform DB Assertion
   */

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_DEL_ACC_CLOSURE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0808() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0808", "To verify that Account closure " +
                "can only be done once all non primary wallets have been removed (by modifying the customer).");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_DEL_ACC_CLOSURE, FunctionalTag.ECONET_SIT_5_0, Author.NIRUPAMA);
        try {

            if (pref) {
                SystemPreferenceManagement.init(t1).updateSystemPreference
                        ("ACCOUNT_CLOSURE_TWO_STEP", "N");
            }

            User subs = new User(Constants.SUBSCRIBER);
            Login.init(t1).login(chAddSubs);

            SubscriberManagement.isBankRequired = false;
            SubscriberManagement.init(t1)
                    .createSubscriber(subs);

            //Define Service Charge for Account Closure by Agent
            ServiceCharge accClosure = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs,
                    new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);

            ServiceChargeManagement.init(t1).
                    configureServiceCharge(accClosure);

            //Perform CashIn for the subscriber
            Login.init(t1).login(chUser);
            TransactionManagement.init(t1).performCashIn(subs, "4");

            ConfigInput.isConfirm = false;
            SubscriberManagement.init(t1)
                    .startNegativeTest()
                    .deleteSubscriber(subs, DataFactory.getDefaultProvider().ProviderId);

            Assertion.verifyErrorMessageContain("subs.error.AccountClosure1",
                    "Subscriber with non primary wallets cant be deleted", t1);

            //modify subscriber to delete Non-primary Wallets
            Login.init(t1).login(chUser);
            SubscriberManagement.init(t1)
                    .modifySubscriberToDeleteWallet(chUser, subs)
                    .modifyApprovalSubs(subs);

            // re-initialize, as in the same method we have already set to 'FALSE'
            ConfigInput.isConfirm = true;
            ConfigInput.isAssert = true;

            SubscriberManagement.init(t1)
                    .deleteSubscriber(subs, DataFactory.getDefaultProvider().ProviderId);

            String dbStatus = MobiquityGUIQueries
                    .getUserStatus(subs.MSISDN, subs.CategoryCode);

            Assertion.verifyEqual(dbStatus, "N", "Verify DB Status when Subscriber is Deleted.", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_DEL_ACC_CLOSURE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0807() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0807", "To verify that Customer can be deleted" +
                " even if he has not changed his PIN first time as well after registration.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_DEL_ACC_CLOSURE, FunctionalTag.ECONET_SIT_5_0);
        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference
                    ("ACCOUNT_CLOSURE_TWO_STEP", "N");

            User subs = new User(Constants.SUBSCRIBER);
            Login.init(t1).login(chAddSubs);

            SubscriberManagement.init(t1).addInitiateSubscriber(subs);
            CommonUserManagement.init(t1).assignWebGroupRole(subs);

            CommonUserManagement.init(t1).mapDefaultWalletPreferences(subs);
            AddChannelUser_pg5.init(t1).clickNext(subs.CategoryCode);

            AlertHandle.handleUnExpectedAlert(t1);
            AlertHandle.handleUnExpectedAlert(t1);

            CommonUserManagement.init(t1).verifyAndConfirmDetails(subs, false);

            Login.init(t1).login(chApproveSubs);
            SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);

            Transactions.init(t1).subscriberAcquisition(subs);

            //Perform Cashin for the subscriber
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal(4));

            // Delete the Subscriber
            SubscriberManagement.init(t1).
                    initiateDeleteSubscriber(subs, chUser, DataFactory.getDefaultProvider().ProviderId);

            String dbStatus = MobiquityGUIQueries
                    .getUserStatus(subs.MSISDN, subs.CategoryCode);

            Assertion.verifyEqual(dbStatus, "N", "Verify DB Status when Subscriber is Deleted.", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_DEL_ACC_CLOSURE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0806() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0806", "To verify that Customer can be deleted" +
                " even if he has not paid his joining fees.");

       /* ExtentTest t2 = pNode.createNode("TC_ECONET_0325", "To verify that Subscriber " +
                "Account Closure should be successful if Subscriber has not paid his joining fees");*/

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_DEL_ACC_CLOSURE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0);
        try {

            if (pref) {
                SystemPreferenceManagement.init(t1).updateSystemPreference
                        ("ACCOUNT_CLOSURE_TWO_STEP", "N");
            }

            User subs = new User(Constants.SUBSCRIBER);

            ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs,
                    new OperatorUser(Constants.OPERATOR), null,
                    null, null, null);

            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            Transactions.init(t1)
                    .selfRegistrationForSubscriberWithKinDetails(subs);

            Transactions.init(t1)
                    .initiateCashIn(subs, chUser, "5");

            String query = "select ACQUISITION_PAID from mtx_party_access where msisdn='" + subs.MSISDN + "'";

            String acqPayStatus = MobiquityGUIQueries.executeQueryAndReturnResult(query, "ACQUISITION_PAID");

            if (Constants.STATUS_DELETE.equalsIgnoreCase(acqPayStatus)) {
                SubscriberManagement.init(t1)
                        .deleteSubscriber(subs, DataFactory.getDefaultProvider().ProviderId);

                Assertion.verifyActionMessageContain("account.close.success", "Account Closure success", t1);

                String dbStatus = MobiquityGUIQueries
                        .getUserStatus(subs.MSISDN, subs.CategoryCode);

                Assertion.verifyEqual(dbStatus, "N", "Verify DB Status when Subscriber is Deleted.", t1);
            } else {
                t1.skip("Skipping this case as JOINING FEES has already been paid. Unable to test this functionality");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1027() throws Exception, MoneyException {


        ExtentTest t1 = pNode.createNode("TC_ECONET_1027", "To verify that the Admin User should " +
                "able to De-Link Bank Account of Subscriber via WEB.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0);
        try {

            String bankId = DataFactory.getBankId(DataFactory.getDefaultBankNameForDefaultProvider());

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1)
                    .createSubscriberDefaultMapping(subs, true, true);

            //fetch the bank account no for subscriber
            Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(subs, DataFactory.getDefaultProvider().ProviderId, bankId);
            String subaccount = AccountNum1.get("ACCOUNT_NO");

            //decrypt the account no.
            DesEncryptor d1 = new DesEncryptor();
            String decsubacc = d1.decrypt(subaccount);

            Login.init(t1).login(optUser);
            BankAccountAssociation.init(t1).initiateBankAccountDeRegistration(subs, provider, decsubacc);
            Thread.sleep(2000);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1).approveAssociatedBanksForUser(subs, provider, bank);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @AfterClass(alwaysRun = true)
    public void postcondition() throws Exception {

        Markup m = MarkupHelper.createLabel("Post-Condition,ReSetting preference", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            SystemPreferenceManagement.init(pNode).updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", prefValue);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }
}