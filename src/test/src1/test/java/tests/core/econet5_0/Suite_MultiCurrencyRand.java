//package tests.core.econet5_0;
//
//import com.aventstack.extentreports.ExtentTest;
//import framework.dataEntity.CurrencyProvider;
//import framework.entity.*;
//import framework.features.common.Login;
//import framework.features.stockManagement.StockManagement;
//import framework.features.systemManagement.*;
//import framework.features.userManagement.ChannelUserManagement;
//import framework.features.userManagement.CommonUserManagement;
//import framework.features.userManagement.SubscriberManagement;
//import framework.pageObjects.reconciliation.Reconciliation_Page;
//import framework.pageObjects.userManagement.AddChannelUser_pg4;
//import framework.pageObjects.userManagement.AddChannelUser_pg5;
//import framework.util.common.Assertion;
//import framework.util.common.DataFactory;
//import framework.util.common.DriverFactory;
//import framework.util.dbManagement.MobiquityGUIQueries;
//import framework.util.globalConstant.*;
//import framework.util.globalVars.ConfigInput;
//import framework.util.globalVars.FunctionalTag;
//import framework.util.globalVars.GlobalData;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.SkipException;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//import tests.core.base.TestInit;
//
//import java.lang.reflect.Array;
//import java.sql.ResultSet;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
////* For this class to be executed, RAND Currency and its corresponding providerId should be available in
//// SYS_SERVICE_PROVIDER table and status should be Y *//
//
//public class Suite_MultiCurrencyRand extends TestInit {
//
//    private static OperatorUser usrCreator, usrApprover;
//    private String bankName, providerName, providerId;
//
//    @BeforeClass(alwaysRun = true)
//    public void setup() throws Exception {
//
//        try {
//            DataFactory.getNonDefaultProvider();
//            if (DataFactory.getAllProviderNames().size() > 1) {
//                providerName =DataFactory.getNonDefaultProvider().ProviderName;
//                providerId = DataFactory.getNonDefaultProvider().ProviderId;
//                bankName = DataFactory.getAllBankNamesLinkedToProvider(providerName).get(0);
//            } else {
//                throw new SkipException("At least 2 Providers/Currencies are required for these Cases to run");
//            }
//
//        }catch (Exception e){
//            markSetupAsFailure(e);
//        }
//
//    }
//
//    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
//    public void TC_ECONET_1093() throws Exception {
//
//        ExtentTest t1 = pNode.createNode("TC_ECONET_1093", "To verify that the Account creation for New currency should be made from bank side.");
//        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0);
//
//        try {
//
//            Login.init(t1).loginAsSuperAdmin("BANK_ADD");
//            CurrencyProvider provider = GlobalData.currencyProvider.get(0);
//            Bank bank = new Bank(provider);
//            bank.setBankName(bankName);
//            bank.setProviderName(providerName);
//            CurrencyProviderMapping.init(t1)
//                    .addBank(bank);
//
//        } catch (Exception e) {
//            Assertion.raiseExceptionAndContinue(e, t1);
//        }
//    }
//
//    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
//    public void TC_ECONET_1094() throws Exception {
//
//        ExtentTest t1 = pNode.createNode("TC_ECONET_1094", "To verify that the Valid user should able to create new TCP for RAND currency.");
//        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0);
//
//        try {
//
//            //TCPManagement.init(t1).addInstrumentTCP(tcp);
//            //TCPManagement.init(t1).addInstrumentTCP(tcp1);
//            InstrumentTCP tcp2 = new InstrumentTCP(providerName,
//                    DataFactory.getDomainName(Constants.WHOLESALER),
//                    DataFactory.getCategoryName(Constants.WHOLESALER),
//                    DataFactory.getGradesForCategory(Constants.WHOLESALER).get(0).GradeName,
//                    "WALLET", DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName);
//            TCPManagement.init(t1).addInstrumentTCP(tcp2);
//        } catch (Exception e) {
//            Assertion.raiseExceptionAndContinue(e, t1);
//        }
//    }
//
//    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
//    public void TC_ECONET_1095() throws Exception {
//
//        ExtentTest t1 = pNode.createNode("TC_ECONET_1095", "To verify that the Valid user should able to create Service Charge, commission & Taxes for RAND currency.");
//        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0);
//
//        try {
//
//            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
//
//            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
//            ServiceCharge sCharge = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, opt, null,
//                    null, providerName, providerName);
//            sCharge.setServicePayeeProvider(providerName);
//            sCharge.setServicePayerProvider(providerName);
//
//            ServiceChargeManagement.init(t1)
//                    .configureServiceCharge(sCharge);
//        } catch (Exception e) {
//            Assertion.raiseExceptionAndContinue(e, t1);
//        }
//    }
//
//    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
//    public void TC_ECONET_1097() throws Exception {
//
//        ExtentTest t1 = pNode.createNode("TC_ECONET_1097", "To verify that the Valid user should able to perform Reimbursement with new currency RAND.");
//        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0);
//
//        try {
//
//
//            OperatorUser init = DataFactory.getOperatorUserWithAccess(Roles.STOCK_REIMBURSEMENT_INITIATION);
//            ServiceCharge sCharge = new ServiceCharge(Services.OPERATOR_WITHDRAW, init, init, null, null, null, null);
//            sCharge.setServicePayeeProvider(providerName);
//            sCharge.setServicePayerProvider(providerName);
//
//            TransferRuleManagement.init(t1).configureTransferRule(sCharge);
//
//            Login.init(t1).login(init);
//            String txnId = StockManagement.init(t1)
//                    .stockReimbursementInitiation(init, providerId, DataFactory.getRandomNumberAsString(5), "2", "test", null, false);
//            Login.init(t1).loginAsOperatorUserWithRole(Roles.STOCK_REIMBURSEMENT_APPROVAL);
//            StockManagement.init(t1).approveReimbursement(txnId);
//
//        } catch (Exception e) {
//            Assertion.raiseExceptionAndContinue(e, t1);
//        }
//    }
//
//
//    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
//    public void TC_ECONET_1098() throws Exception {
//
//        ExtentTest t1 = pNode.createNode("TC_ECONET_1098", "To verify that the New RAND currency should be displayed on Reconciliation screen as well & Valid user should able to view Reconciliation of RAND toore.");
//        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_SIT_5_0);
//
//        try {
//
//            Login.init(t1).loginAsOperatorUserWithRole(Roles.RECONCILIATION);
//
//            Reconciliation_Page reconPage =  new Reconciliation_Page(t1);
//            //  Navigate to Reciliation Page to check Reconciled Result
//            reconPage.navToReconciliationLink().setProviderByText(providerName).selectSubmit();
//
//        } catch (Exception e) {
//            Assertion.raiseExceptionAndContinue(e, t1);
//        }
//    }
//
//    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
//    public void TC_ECONET_0546() throws Exception {
//
//        ExtentTest t1 = pNode.createNode("TC_ECONET_0546", "To verify that the Valid user should be able to make registration process for Add other currency SVA.");
//        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0);
//
//        try {
//
//            CurrencyProvider obj = new CurrencyProvider(providerId,
//                    DataFactory.getNonDefaultProvider().Currency,
//                    bankName,
//                    true,
//                    true,
//                    false,
//                    false);
//
//            GlobalData.currencyProvider.add(obj);
//
//            User whs = new User(Constants.WHOLESALER);
//            whs.setProviderName(providerName);
//            Login.init(t1).loginAsOperatorUserWithRole(Roles.ADD_CHANNEL_USER);
//            ChannelUserManagement.init(t1).initiateChannelUser(whs).assignHierarchy(whs);
//
//            CommonUserManagement.init(t1)
//                    .assignWebGroupRole(whs);
//
//            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(t1);
//            ArrayList<String> array = new ArrayList<>();
//            array.add(DataFactory.getDefaultProvider().ProviderName);
//            array.add(providerName);
//
//            List<String> providerList = array;
//
//            String dWallet = DataFactory.getDefaultWallet().WalletName;
//
//            int counter = 0;
//            for (String provider : providerList) {
//                // select just one primary account per currency provider
//                if (counter != 0) {
//                    try {
//                        page4.clickAddMore(whs.CategoryCode);
//                    } catch (Exception e) {
//                        if (e.getMessage().contains("element is not attached")) {
//                            Thread.sleep(Constants.TWO_SECONDS);
//                            page4.clickAddMore(whs.CategoryCode);
//                        }
//                    }
//                }
//                // get instrument TCP
//                InstrumentTCP insTcp = DataFactory.getInstrumentTCP(whs.DomainName, whs.CategoryName, whs.GradeName, provider, dWallet);
//
//
//                page4.selectProvider(counter,provider);
//                page4.selectPaymentType(counter,dWallet);
//                page4.selectGrade(counter,whs.GradeName);
//
//                if (insTcp != null) {
//                    page4.selectInstrumentTcp(counter,insTcp.ProfileName);
//                } else {
//                    Select sel = new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected")));
//                    if (sel.getOptions().size() > 0)
//                        page4.selectInstrumentTcpByIndex(counter,0);
//                    else
//                        page4.selectInstrumentTcpByIndex(counter,1);
//
//                }
//                counter++;
//            }
//
//
//            page4.clickNext(whs.CategoryCode);
//
//            WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
//            AddChannelUser_pg5 page = AddChannelUser_pg5.init(t1);
//            page.clickNext(whs.CategoryCode);
//
//            wait.until(ExpectedConditions.alertIsPresent()).accept();
//            wait.until(ExpectedConditions.alertIsPresent()).accept();
//            page.completeUserCreation(whs, false);
//
//            Login.init(t1).loginAsOperatorUserWithRole(Roles.ADD_CHANNEL_USER_APPROVAL);
//            CommonUserManagement.init(t1).addInitiatedApproval(whs, true);
//
//        } catch (Exception e) {
//            Assertion.raiseExceptionAndContinue(e, t1);
//        } finally {
//            DataFactory.loadCurrencyProvider();
//        }
//    }
//}