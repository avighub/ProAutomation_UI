package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.Promotion.Reconciliation;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static framework.features.apiManagement.Transactions.getReconciliationBalance;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Escape Defect Reconciliation
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/27/2018.
 */

public class Suit_P1_Reconciliation extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() {
        ExtentTest P1_TC_057 = pNode.createNode("P1_TC_057", "To verify that there should not be reconciliation gap " +
                "and system should not throw There is a problem in application error.");
        try {
            OperatorUser user = DataFactory.getOperatorUserWithAccess("MN_REC");
            String provider = DataFactory.getDefaultProvider().ProviderId;
            Login.init(P1_TC_057).login(user);

            Reconciliation.init(P1_TC_057)
                    .navReconciliation()
                    .selectProvider(provider)
                    .clickSubmit();

            Assertion.verifyEqual(summeryOfBalance(), Float.toString(getReconciliationBalance()).replace("-", ""),
                    "Reconciliation Balance", P1_TC_057, true);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_057);
        }
        Assertion.finalizeSoftAsserts();
    }

    private String summeryOfBalance() {
        return driver.findElement(By.id("reconciliation_loadReconciliation_reconciliationMap_netBalance")).getText();
    }
}
