package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Create and Delete subscriber
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 3/15/2018.
 */
public class Suite_P1_EscapeDefect_TIGOHN extends TestInit {
    private User sub, whs;
    private OperatorUser optUsr;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        /*
        define transfer rule and service charge for account closure
         */
        ExtentTest eSetup = pNode.createNode("Setup", "Create Setup specific to the test");
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("T_RULES");
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //Login.init(eSetup).login(optUsr);

            ServiceCharge accountClosure = new ServiceCharge(Services.ACCOUNT_CLOSURE, new User(Constants.SUBSCRIBER), new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(accountClosure);

            ServiceChargeManagement.init(eSetup).configureServiceCharge(accountClosure);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.ESCAPEDEFECT})
    public void P1_TC_056() {
        /*
        create a subscriber by web
         */
        ExtentTest TC681 = pNode.createNode("P1_TC_056", "To verify that default message display while performing subscriber registration through Web or SSA.");
        try {
            Login.init(TC681).login(whs);
            sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(TC681).createSubscriberDefaultMapping(sub, true, false);

            //TransactionManagement.init(TC681).performCashIn(sub, "10", DataFactory.getDefaultProvider().ProviderName);
        } catch (Exception e) {
            markTestAsFailure(e, TC681);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_055() {
        ExtentTest TC680 = pNode.createNode("P1_TC_055", "To verify that valid user should be able to delete the subscriber from Web successfully.");
        try {
            /*
            delete a subscriber by web
            */
            Transactions.init(TC680).initiateCashIn(sub, whs, "5");
            SubscriberManagement.init(TC680)
                    .initiateDeleteSubscriber(sub, whs, DataFactory.getDefaultProvider().ProviderId);
        } catch (Exception e) {
            markTestAsFailure(e, TC680);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
