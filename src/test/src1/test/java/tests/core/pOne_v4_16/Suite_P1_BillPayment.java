package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.billerManagement.SubsBillerAssociation_pg1;
import framework.pageObjects.subscriberManagement.Subscriber_Notification_Registration_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Bill Payment Functionality Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/22/2018.
 */
public class Suite_P1_BillPayment extends TestInit {

    private OperatorUser optNotificationReg, naUtilBillReg, optCreator, optSuspendResume, optUpload;
    private User subShared;
    private Biller biller;
    private String providerId, defAccountNum;
    private ServiceCharge utility;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {
        /*
        Verify channel user has balance
        create utility registration service charge
        create a subscriber
         */

        ExtentTest eSuite = pNode.createNode("Setup", "Get Default Biller. " +
                "Make sure Channel User has Sufficient balance. " +
                "Make sure that Service Charge Utility Registration is configured." +
                "Create s Subscriber for Negative Testing.");

        try {
            optCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            optSuspendResume = DataFactory.getOperatorUserWithAccess("SR_USR");
            naUtilBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");
            optNotificationReg = DataFactory.getOperatorUserWithAccess("NOTF_REG");
            optUpload = DataFactory.getOperatorUserWithAccess("BILLUP");
            subShared = new User(Constants.SUBSCRIBER);
            providerId = DataFactory.getDefaultProvider().ProviderId;

            // get Default Biller


            biller = BillerManagement.init(eSuite).getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);


            // Create Subscriber for Negative Test
            // bank mapping is required as Default account number has to be used in further tests.
            SubscriberManagement.init(eSuite)
                    .createSubscriberDefaultMapping(subShared, true, true);

            defAccountNum = DataFactory.getRandomNumberAsString(5);

            // make sure leaf user has Balance
            TransactionManagement.init(eSuite)
                    .makeSureLeafUserHasBalance(subShared);

            //Defining service charge for Subscriber Biller Association
            utility = new ServiceCharge(Services.UTILITY_REGISTRATION, subShared, optCreator, null, null, null, null);

            ServiceChargeManagement.init(eSuite)
                    .configureNonFinancialServiceCharge(utility);

            utility.setNFSCServiceCharge("0");
            utility.setNFSCCommission("0");

            ServiceChargeManagement.init(eSuite)
                    .modifyNFSCInitAndApprove(utility, true);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.MONEY_SMOKE})
    public void TC405() throws Exception {
        /*login as operator user
        navigate Bill pay > bulk biller association
        add a active subscriber
         */
        ExtentTest TC405 = pNode.createNode("P1_TC_343",
                "To verify that valid user should be able to associate subscribers with the biller(Using BULK Biller Association), " +
                        "if all the valid details is entered.");

        try {
            Login.init(TC405).login(optUpload);

            String accNum = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subShared.MSISDN, accNum);
            BillerManagement.init(TC405)
                    .initiateBulkBillerRegistration(biller)
                    .downloadBulkRegistrationLogFile();

            String logMessage = BillerManagement.getLogEntry(accNum);
            Assertion.verifyContains(logMessage, MessageReader.getDynamicMessage("bulk.biller.association.success.message", subShared.MSISDN),
                    "Verify Subscriber Biller Association for accnum:" + accNum, TC405);
        } catch (Exception e) {
            markTestAsFailure(e, TC405);
        } finally {
            biller.removeAllBills();

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void P1_TC_169() throws Exception {
        /*
        login as operator user
        navigate to bill bay
        click on upload bill
        upload a file having invalid format
        validate error message
         */
        ExtentTest TC383 = pNode.createNode("P1_TC_169", "To verify that Bill upload is not successful with invalid Filename format.");
        try {
            Login.init(TC383).login(optUpload);

            BillerManagement.init(TC383)
                    .startNegativeTest()
                    .bulkBillerAssociationFileUpload(FilePath.uploadFile);

            Assertion.verifyErrorMessageContain("invalid.csv.file.format", "Invalid file format", TC383);
        } catch (Exception e) {
            markTestAsFailure(e, TC383);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void P1_TC_170() throws Exception {
        /*
        login as operator user
        navigate to bill bay
        click on subscriber biller association
        assign a subscriber with a biller
        navigate to delete subscriber biller assoiation and remove newly added subscriber from biller
         */

        ExtentTest TC388 = pNode.createNode("P1_TC_170", "To verify that system should be able to delete the association of the subscriber with the biller through web.");

        try {
            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            // add above bill to the Biller object
            biller.addBillForCustomer(subShared.MSISDN, billAccNumber);

            // Login as Operator user with Bill Registration Role
            Login.init(TC388)
                    .login(naUtilBillReg);

            // Initiate Subscriber Biller Association
            BillerManagement.init(TC388)
                    .initiateSubscriberBillerAssociation(biller);

            CustomerBill bill = biller.getAssociatedBill();
            BillerManagement.init(TC388)
                    .deleteSubsBillerAssociation(bill);


        } catch (Exception e) {
            markTestAsFailure(e, TC388);
        } finally {
            biller.removeAllBills();
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void P1_TC_330() throws Exception {
        /*
        login as operator user
        navigate to bill pay > notification management
        create a notification preference
        and create the same notofication preference one more time
        validate error message
         */
        ExtentTest TC389 = pNode.createNode("P1_TC_330", "To Verify that Notification Management Should not be successful " +
                "if we  enter details that already exist in the system.");
        try {

            BillNotification notification = new BillNotification("Demo Bill", Constants.BILL_NOTIFICATION_TYPE_GENERATE_DATE_AFTER, "100");
            BillerManagement.init(TC389).addBillerNotification(notification);

            BillerManagement.init(TC389)
                    .startNegativeTest()
                    .addBillerNotification(notification);

            Assertion.verifyErrorMessageContain("notification.notification.already.exists",
                    "Verify that Notification Management Should not be successful " +
                            "if we  enter details that already exist in the system", TC389);

            stopNegativeTest();
            BillerManagement.init(TC389)
                    .deleteBillerNotification(notification, "N");

        } catch (Exception e) {
            markTestAsFailure(e, TC389);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void TC390_398() throws Exception {
        /*
        login as operator user
        navigate to operator management > suspend/resume subscriber
        suspend a subscriber
        navigate to bill pay > subscriber biller association
        associate suspended subscriber with a biller
        validate error message
        resume the suspended subscriber
         */
        ExtentTest TC390 = pNode.createNode("P1_TC_331-P1_TC_332",
                "To verify that system should not able to associate suspended subscriber with the biller through web/ussd.");
        try {
            Login.init(TC390).login(optSuspendResume);
            SubscriberManagement.init(TC390).suspendSubscriber(subShared, Constants.USER_TYPE_SUBS);

            biller.addBillForCustomer(subShared.MSISDN, DataFactory.getRandomNumberAsString(5));

            Login.init(TC390).login(naUtilBillReg);
            SubsBillerAssociation_pg1.init(TC390)
                    .navAddBillerValidation()
                    .selectProvider(biller.ProviderName)
                    .setSubscriberMsisdn(subShared.MSISDN)
                    .clickOnSubmit();

            Assertion.verifyErrorMessageContain("mBanking.message.notRegister",
                    "Verify System should not able to associate suspended subscriber with the biller", TC390);

            //------------------------------------------------------
            ExtentTest TC398 = pNode.createNode("P1_TC_339",
                    "To Verify that valid user cannot add Subscriber Notification Registration when the subscriber is in suspended state");


            Login.init(TC390)
                    .login(optNotificationReg);
            Subscriber_Notification_Registration_Page1.init(TC398)
                    .subscriberBillNotificationRegistration(providerId, biller.BillerCode, "12345", subShared.MSISDN);

            Assertion.verifyErrorMessageContain("subs.message.notification.notactive",
                    "Verify Cannot add Subscriber Notification Registration when the subscriber is in suspended", TC398);

        } catch (Exception e) {
            markTestAsFailure(e, TC390);
        } finally {
            SubscriberManagement.init(TC390).resumeSubscriber(subShared, Constants.USER_TYPE_SUBS);
            biller.removeAllBills();
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void TC391() throws Exception {
        /*
        login as operator user
        navigate to subscriber management > notification registration
        add a subscriber who doesn't associate him with any biller
        validate error message
         */
        ExtentTest TC391 = pNode.createNode("TC391",
                "user can not add Subscriber Notification Registration if Subscriber is not associated with biller");
        try {
            Login.init(TC391).login(optNotificationReg);

            User sub391 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Subscriber_Notification_Registration_Page1.init(TC391)
                    .subscriberBillNotificationRegistration(providerId, biller.BillerCode, "12345", sub391.MSISDN);

            Assertion.verifyErrorMessageContain("notif.error.Subscriber.notRegistered",
                    "Verify Subscriber is not utility registered if Subscriber is not associated with biller", TC391);
        } catch (Exception e) {
            markTestAsFailure(e, TC391);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(priority = 7, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void TC395_401() throws Exception {
        /*
        login as operator user
        bar a subscriber as receiver
        associate that subscriber with a biller and validate error message
         */
        ExtentTest TC395 = pNode.createNode("P1_TC_336",
                "To verify that system should be able to associate Barred subscriber(Barred as Receiver) with the biller through web/ussd.");
        try {

            SubscriberManagement.init(TC395)
                    .barSubscriber(subShared, Constants.BAR_AS_RECIEVER);

            String billAccNumber = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subShared.MSISDN, billAccNumber);

            Login.init(TC395).login(naUtilBillReg);
            BillerManagement.init(TC395)
                    .initiateSubscriberBillerAssociation(biller);

            //-------------------------------------------------------------------
            ExtentTest TC401 = pNode.createNode("P1_TC_342",
                    "To Verify that valid user can add Subscriber Notification Registration when the subscriber is barred as Receiver");

            Login.init(TC401).login(optNotificationReg);
            Subscriber_Notification_Registration_Page1.init(TC401)
                    .subscriberBillNotificationRegistration(providerId, biller.BillerCode, defAccountNum, subShared.MSISDN);

            Assertion.verifyErrorMessageContain("notif.error.Subscriber.notRegistered", "Subscriber utility not registered", TC401);

        } catch (Exception e) {
            markTestAsFailure(e, TC395);
        } finally {
            SubscriberManagement.init(TC395).unBarSubscriber(subShared);
            biller.removeAllBills();
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 8, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void TC396_399_406() throws Exception
        /*
        login as operator user
        bar a subscriber as both
        associate that subscriber with a biller and validate error message
        unbar the subscriber
         */ {
        ExtentTest TC396 = pNode.createNode("P1_TC_337",
                "To verify that system should be able to associate Barred subscriber(Barred as Both) with the biller through web/ussd.");
        try {

            SubscriberManagement.init(TC396)
                    .barSubscriber(subShared, Constants.BAR_AS_BOTH);

            String billAccNumber = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subShared.MSISDN, billAccNumber);

            BillerManagement.init(TC396)
                    .initiateSubscriberBillerAssociation(biller);

            //------------------------------------------
            ExtentTest TC399 = pNode.createNode("P1_TC_340",
                    "To Verify that valid user cannot add Subscriber Notification Registration when the subscriber is barred as Both");

            Login.init(TC399)
                    .login(optNotificationReg);
            Subscriber_Notification_Registration_Page1.init(TC399)
                    .subscriberBillNotificationRegistration(providerId, biller.BillerCode, biller.BillerCode, subShared.MSISDN);
            Assertion.verifyErrorMessageContain("notif.error.Subscriber.notRegistered",
                    "Verify that valid user cannot add Subscriber Notification Registration when the subscriber is barred as Both", TC399);

            //------------------------------------------
            ExtentTest TC406 = pNode.createNode("P1_TC_344",
                    "To verify that valid user should  be able to associate  subscribers(Barred as Both)(Using BULK Biller Association) " +
                            "with the biller, if all the valid details is entered.");

            Login.init(TC406).login(optUpload);

            String accNum = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subShared.MSISDN, accNum);
            BillerManagement.init(TC406)
                    .initiateBulkBillerRegistration(biller)
                    .downloadBulkRegistrationLogFile();

            String logMessage = BillerManagement.getLogEntry(accNum);
            Assertion.verifyContains(logMessage, MessageReader.getDynamicMessage("bulk.biller.association.success.message", subShared.MSISDN),
                    "Verify Subscriber Biller Association for accnum:" + accNum, TC406);

        } catch (Exception e) {
            markTestAsFailure(e, TC396);
        } finally {
            SubscriberManagement.init(TC396).unBarSubscriber(subShared);
            biller.removeAllBills();
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 9, groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void TC397_400_407() throws Exception {
         /*
        login as operator user
        bar a subscriber as sender
        associate that subscriber with a biller and validate error message
        unbar the subscriber
         */
        ExtentTest TC397 = pNode.createNode("P1_TC_338",
                "To verify that system should able to associate Barred subscriber(Barred as Sender) with the biller through web/ussd.");
        try {
            SubscriberManagement.init(TC397)
                    .barSubscriber(subShared, Constants.BAR_AS_SENDER);

            String billAccNumber = DataFactory.getRandomNumberAsString(5);
            biller.removeAllBills();
            biller.addBillForCustomer(subShared.MSISDN, billAccNumber);

            BillerManagement.init(TC397)
                    .initiateSubscriberBillerAssociation(biller);

            //----------------------------------------
            ExtentTest TC400 = pNode.createNode("P1_TC_341",
                    "To Verify that valid user cannot add Subscriber Notification Registration when the subscriber is barred as sender");

            Login.init(TC400).login(optNotificationReg);
            Subscriber_Notification_Registration_Page1.init(TC400)
                    .subscriberBillNotificationRegistration(providerId, biller.BillerCode, defAccountNum, subShared.MSISDN);

            Assertion.verifyErrorMessageContain("notif.error.Subscriber.notRegistered",
                    "Verify that valid user cannot add Subscriber Notification Registration when the subscriber is barred as sender", TC400);

            //----------------------------------------
            ExtentTest TC407 = pNode.createNode("P1_TC_345", "To verify that valid user should  be able to associate  " +
                    "subscribers(Barred as Sender)(Using BULK Biller Association) with the biller, if all the valid details is entered.");

            Login.init(TC407).login(optUpload);

            String accNum = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subShared.MSISDN, accNum);

            BillerManagement.init(TC407)
                    .initiateBulkBillerRegistration(biller)
                    .downloadBulkRegistrationLogFile();

            String logMessage = BillerManagement.getLogEntry(accNum);
            Assertion.verifyContains(logMessage, MessageReader.getDynamicMessage("bulk.biller.association.success.message", subShared.MSISDN),
                    "Verify Subscriber Biller Association for accnum:" + accNum, TC407);

        } catch (Exception e) {
            markTestAsFailure(e, TC397);
        } finally {
            SubscriberManagement.init(TC397).unBarSubscriber(subShared);
            biller.removeAllBills();
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() {
        ExtentTest teardown = pNode.createNode("tear down");
        try {

            utility.setNFSCServiceCharge("0.1");
            utility.setNFSCCommission("0.1");

            ServiceChargeManagement.init(teardown)
                    .modifyNFSCInitAndApprove(utility, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

