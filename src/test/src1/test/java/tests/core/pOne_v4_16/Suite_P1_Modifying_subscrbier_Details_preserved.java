package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page2;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page3;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page4;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by gurudatta.praharaj on 6/15/2018.
 */


/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Subscriber Details Preserved
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/15/2018.
 */
public class Suite_P1_Modifying_subscrbier_Details_preserved extends TestInit {

    private User chUsr, sub;
    private ExtentTest P1_TC_079;

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test01() throws Exception {
        /*
        create a subscriber through ussd
        Login as Channel user
        initiate modify newly created subscriber
        validate Pin expiry message should not appear
        approve the modified initiated subscriber
         */
        P1_TC_079 = pNode.createNode("P1_TC_079", "Subscriber Should successfully modify Initiated");

        ExtentTest P1_TC_080_P1_TC_590 = pNode.createNode("P1_TC_080_P1_TC_590", "To verify that any valid user can modify the " +
                "subscriber details which is registered  through subscriber self registration.");
        try {
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            sub = new User(Constants.SUBSCRIBER);

            Transactions.init(P1_TC_079)
                    .selfRegistrationForSubscriberWithKinDetails(sub);

            Login.init(P1_TC_079).login(chUsr);

            SubscriberManagement.init(P1_TC_079)
                    .initiateSubscriberModification(sub);
            completeModificationSub(sub);

            Assertion.verifyActionMessageContain("subs.modify.message.initiateBy",
                    "Subscriber modified initiated", P1_TC_079, chUsr.FirstName, chUsr.LastName);

            SubscriberManagement.init(P1_TC_080_P1_TC_590)
                    .modifyApprovalSubs(sub);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_079);
        }
    }


    private void completeModificationSub(User user) {
        ModifySubscriber_page2.init(P1_TC_079)
                .setNamePrefix()
                .setIdType()
                .setIdNumber()
                .clickOnNextPg2();

        ModifySubscriber_page3.init(P1_TC_079)
                .selectWebGroupRole(user.WebGroupRole);

        ModifySubscriber_page3.init(P1_TC_079)
                .nextPage();

        ModifySubscriber_page4.init(P1_TC_079)
                .clickSubmitPg4()
                .clickOnConfirmPg4()
                .nextFinalConfirm_click();
    }
}
