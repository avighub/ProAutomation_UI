package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.Geography;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.GeographyManagement;
import framework.pageObjects.Geo_managment.Geography_ModifyPage1;
import framework.pageObjects.Geo_managment.Geography_Page1;
import framework.pageObjects.Geo_managment.Geography_Page2;
import framework.pageObjects.Geo_managment.Geography_Page3;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify various Fields and Buttons in Geography Management (6-P1 Cases)
 * Author Name      : Jyoti Katiyar
 * Created Date     : 08/02/2018
 */
public class Suite_P1_ZoneManagement extends TestInit {

    private OperatorUser NetworkAdm;

    @BeforeClass(alwaysRun = true)
    private void preRun() throws Exception {
        NetworkAdm = DataFactory.getOperatorUserWithAccess("VIEWGRPHDOMAIN");
    }

    @Test(priority = 1, groups = {FunctionalTag.ZONE_MANAGEMENT})
    public void Test_01() throws Exception {

        ExtentTest t1 = pNode.createNode("TC038", "Field Validation: To verify that Super Admin or Network Admin should be able" +
                " to add Zone in system")
                .assignCategory(FunctionalTag.GEOGRAPHY_MANAGEMENT);

        //login as Channel user
        Login.init(t1).login(NetworkAdm);

        //navigate to geography management page select Zone and submit

        Geography_Page1 page1 = new Geography_Page1(t1);
        page1.navigateToLink()
                .selectgeo("Zone")   //Domain type is selected as Zone
                .clicksubmit();
        Geography_Page2 page2 = Geography_Page2.init(t1);

        //verify all buttons exists

        page2.verifyIfBottonExists("Add");
        page2.verifyIfBottonExists("Update");
        page2.verifyIfBottonExists("Delete");
        page2.verifyIfBottonExists("Back");

        Geography_Page3 page3 = new Geography_Page3(t1);
        page3.add()
                .adddvalues("North", "Delhi", "Gurugram", "H.Q")
                .addgeo()
                .confirmbutton();
        Assertion.verifyActionMessageContain("grphdomain.addgrphdomain.msg.addsuccess", "Geo Domain added", t1);
    }

    @Test(priority = 2, groups = {FunctionalTag.ZONE_MANAGEMENT})
    public void Test_02() throws Exception {

        ExtentTest t2 = pNode.createNode("TC043", "Field Validation: To verify that Super Admin or Network Admin should be able to " +
                "modify Zone in system")
                .assignCategory(FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t2).login(NetworkAdm);

        //navigate to geography management page select Zone and submit

        Geography_Page1 page1 = new Geography_Page1(t2);
        page1.navigateToLink()
                .selectgeo("Zone")   //Domain type is selected as Zone
                .clicksubmit();
        Geography_Page2 page2 = Geography_Page2.init(t2);
        Geography_ModifyPage1 page4 = new Geography_ModifyPage1(t2);
        //select the radio button for the Zone you want to modfiy

        page2.selectRadio("Delhi");
        //click on update button

        page4.clickUpdateButton();

        //modify the Geographical domain name, Geographical  domain short name, Status and Description

        page4.setDomName("Delhi 1");
        page4.setDomShortName("Gurugram1");
        page4.setGeoDesc("H.Q Mahindra Comviva");
        page4.selectStatus("Suspended");

        //After modification click on update button

        page4.clickModify();

        //Verify Update and back button exist or not
        page2.verifyIfBottonExists("Confirm");
        page2.verifyIfBottonExists("Cancel");
        page2.verifyIfBottonExists("Back");
    }

    @Test(priority = 3, enabled = false, groups = {FunctionalTag.ZONE_MANAGEMENT})
    public void Test_03() throws Exception {

        ExtentTest t3 = pNode.createNode("TC046", "To verify that Super Admin or Network Admin should be able " +
                "to delete Zone in system")
                .assignCategory(FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t3).login(NetworkAdm);

        //navigate to geography management page select Zone and submit

        Geography_Page1 page1 = new Geography_Page1(t3);
        page1.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit();

        //modify the Geographical domain name which you want to delete

        Geography_Page2 page2 = Geography_Page2.init(t3);
        page2.selectRadio("Delhi");
        page2.clickDeleteButton();
        Thread.sleep(3000);
        Assertion.verifyActionMessageContain("grphdomain.deletegrphdomain.msg.deletesuccess", "Geo Domain Deleted", t3);
    }

    @Test(priority = 4, groups = {FunctionalTag.ZONE_MANAGEMENT})
    public void Test_04() throws Exception {
        Geography geo;
        geo = new Geography();
        ExtentTest t4 = pNode.createNode("TC054", "To verify that Super Admin or Network Admin should be able" +
                " to modify Area in system")
                .assignCategory(FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t4).login(NetworkAdm);

        //navigate to geography management page select Zone and submit
//
//        Geography_Page1 page1 = Geography_Page1.init(t4);
//        page1.navigateToLink()
//                .selectgeo("Area")
//                .clicksubmit();
//
//        GeographyManagement page=GeographyManagement.init(t4);
//        page.modifyArea(geo,"Active");

        GeographyManagement.init(t4).addZone(geo);
        GeographyManagement.init(t4).addArea(geo);
        String zonename = geo.ZoneName;
        String areaname = geo.AreaName;

        System.out.println(zonename);
        System.out.println(areaname);

        Markup m = MarkupHelper.createLabel("modifyarea", ExtentColor.BROWN);
        t4.info(m);

        new Geography_Page1(t4)
                .navigateToLink()
                .selectgeo("Area")
                .clicksubmit()
                .selectzone(geo.ZoneName);

        //Verify Add,Update,Delete and back button exist or not
        Geography_Page2 page2 = Geography_Page2.init(t4);
        page2.verifyIfBottonExists("Add");
        page2.verifyIfBottonExists("Update");
        page2.verifyIfBottonExists("Delete");
        page2.verifyIfBottonExists("Back");

        //modify the status of created Area

        Geography_ModifyPage1 page1 = new Geography_ModifyPage1(t4)
                .selectgeo(geo.AreaName)
                .clickUpdateButton()
                .selectStatus("Suspended");
        page1.clickModify()
                .clickConfirmButton();

        Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Area is Modified", t4);
    }

    @Test(priority = 5, groups = {FunctionalTag.ZONE_MANAGEMENT})
    public void Test_05() throws Exception {
        Geography geo;
        geo = new Geography();
        ExtentTest t5 = pNode.createNode("TC057", "To verify that Super Admin or Network Admin should be able" +
                " to delete Area in system")
                .assignCategory(FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t5).login(NetworkAdm);

        //navigate to geography management page and create Zone and Area

        GeographyManagement.init(t5).addZone(geo);
        GeographyManagement.init(t5).addArea(geo);
        String zonename = geo.ZoneName;
        String areaname = geo.AreaName;

        Markup m = MarkupHelper.createLabel("deletearea", ExtentColor.BROWN);
        t5.info(m);

        new Geography_Page1(t5)
                .navigateToLink()
                .selectgeo("Area")
                .clicksubmit()
                .selectzone(geo.ZoneName);

        //Verify Add,Update,Delete and back button exist or not
        ExtentTest t6 = pNode.createNode("TC059", "To verify that Super Admin or Network Admin should be able" +
                " to delete Area in system");
        Geography_Page2 page2 = Geography_Page2.init(t6);
        page2.verifyIfBottonExists("Add");
        page2.verifyIfBottonExists("Update");
        page2.verifyIfBottonExists("Delete");
        page2.verifyIfBottonExists("Back");

        //Delete the created Area

        new Geography_Page1(t6)
                .navigateToLink()
                .selectgeo("Area")
                .clicksubmit()
                .selectzone(geo.ZoneName);

        Geography_Page2.init(t6)
                .selectRadio(geo.AreaName)
                .clickDeleteButton();
        // AlertHandle.acceptAlert(t5);
        Assertion.verifyActionMessageContain("grphdomain.deletegrphdomain.msg.deletesuccess", "Area is Deleted", t6
        );
    }


}