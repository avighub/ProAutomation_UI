package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.pageObjects.PageInit;
import framework.pageObjects.gradeManagement.GradeModify_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.UserFieldProperties;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Modify Grade Header and button Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 3/15/2018.
 */
public class Suite_P1_ModifyGrade extends TestInit {

    @Test(priority = 1)
    public void Test_01() {
        /*
        login as super admin
        navigate to modify grade
        validate table headers
        validate modify and confirm button
         */
        ExtentTest TC064 = pNode.createNode("TC064",
                "Verify Modify Grade Page");

        try {
            SuperAdmin sm = DataFactory.getSuperAdminWithAccess("MODIFY_GRADES");
            Login.init(TC064).loginAsSuperAdmin(sm);

            GradeModify_Page1 page = GradeModify_Page1.init(TC064);
            page.navigateToLink();

            Object[] expectedHeaders = PageInit.fetchLabelTexts("//table[@class = 'wwFormTableC']/tbody/tr[1]/td");
            Object[] actualHeaders = UserFieldProperties.getLabels("modify.grade");

            if (Arrays.equals(expectedHeaders, actualHeaders)) {
                for (int i = 1; i < expectedHeaders.length; i++) {
                    Assertion.verifyEqual(expectedHeaders[i], actualHeaders[i], "Headers Verified Successfully", TC064);
                }
            } else {
                TC064.fail("Headers Verification Failed");
            }
            Utils.captureScreen(TC064);

            boolean modifyButton = Utils.checkElementPresent("modifyGrades_modify_submit", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(modifyButton, true, "Modify Button", TC064, true);

            page.selectuser("GRT");
            page.modifyButtonClick();

            boolean confirmButton = Utils.checkElementPresent(".//input[@value='Confirm']", Constants.FIND_ELEMENT_BY_XPATH);
            Assertion.verifyEqual(confirmButton, true, "Confirm Button", TC064, true);

        } catch (Exception e) {
            markTestAsFailure(e, TC064);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
