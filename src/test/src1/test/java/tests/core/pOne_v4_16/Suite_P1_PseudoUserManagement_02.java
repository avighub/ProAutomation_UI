package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.PseudoUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.PseudoUserManagement;
import framework.pageObjects.userManagement.ModifyPseudoRoles_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Pseudo User
 * Author Name      : Gurudatta Praharaj
 * Date             : 7/4/2018..
 */
public class Suite_P1_PseudoUserManagement_02 extends TestInit {
    private User chUser;
    private PseudoUser pseudoUser;
    private OperatorUser chAdm;
    private String providerID, role, tcp;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");
        try {
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            chAdm = DataFactory.getOperatorUserWithAccess("PSEUDO_MOD2");
            pseudoUser = new PseudoUser();
            providerID = DataFactory.getDefaultProvider().ProviderId;
            role = "pseudo" + DataFactory.getRandomNumberAsString(3);
            tcp = "pseudoTcp" + DataFactory.getRandomNumberAsString(3);

            pseudoUser.setParentUser(chUser);
            PseudoUserManagement.init(setup)
                    .createCompletePseudoUser(pseudoUser);

            Login.init(setup).login(chUser);
            PseudoUserManagement.init(setup)
                    .addUpdatePseudoTcp(pseudoUser, providerID, Constants.NORMAL_WALLET, tcp, tcp);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() throws Exception {
        ExtentTest P1_TC_193 = pNode.createNode("P1_TC_193",
                "To verify that the valid user should able to Modify Pseudo User.");

        /*
        Login as Channel User
        Navigate to Pseudo User
        Modify User & validate success message
         */
        try {
            PseudoUserManagement.init(P1_TC_193)
                    .modifyPseudoUser(chUser, pseudoUser, pseudoUser.PseudoCategoryCode);

            Assertion.verifyActionMessageContain("PSEUDO_MODIFY_INITIATE",
                    "Pseudo User Modify Initiation Successfully", pNode, pseudoUser.MSISDN);

            PseudoUserManagement.init(P1_TC_193)
                    .approvePseudoUserModification(chAdm, pseudoUser);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_193);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT})
    public void Test_02() throws Exception {
        ExtentTest P1_TC_194 = pNode.createNode("P1_TC_194",
                "To verify that the valid user should able to modify pseudo users group roles.");
        /*
        Login as Channel User
        Navigate to Pseudo User Modify Role
        Modify a Role and validate success message
         */
        try {
            Login.init(P1_TC_194).login(chUser);
            ModifyPseudoRoles_page1.init(P1_TC_194)
                    .navToModifyPseudoRoles().setMsisdn(pseudoUser.MSISDN)
                    .setMFSProvider(providerID).selectWallet(Constants.NORMAL_WALLET)
                    .setRoleCode(role).setRoleName(role)
                    .clickAddButton().clickConfirm();

            Assertion.verifyActionMessageContain("pseudo.roles.success", "Pseudo Role Updated Successfully", P1_TC_194);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_194);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT})
    public void Test_03() throws Exception {
        ExtentTest P1_TC_195 = pNode.createNode("P1_TC_195",
                "To verify that the valid user should able to modify pseudo users TCP.");
        /*
        Login as Channel User
        Navigate to Pseudo User Modify TCP
        Modify a TCP and validate success message
         */
        try {
            Login.init(P1_TC_195).login(chUser);

            PseudoUserManagement.init(P1_TC_195)
                    .addUpdatePseudoTcp(pseudoUser, providerID, Constants.NORMAL_WALLET, tcp, tcp);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_195);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT})
    public void Test_04() throws Exception {
        ExtentTest P1_TC_599 = pNode.createNode("P1_TC_599",
                "To verify that the valid user should able to Delete Pseudo User.");
        /*
        Login as Channel User
        Navigate to Delete User Modify TCP
        Delete Pseudo user and validate success message

         */
        try {
            Login.init(P1_TC_599).login(chUser);

            PseudoUserManagement.init(P1_TC_599)
                    .initiateDeletePseudoUser(pseudoUser)
                    .approveDeletePseudoUser(pseudoUser, true);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_599);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
