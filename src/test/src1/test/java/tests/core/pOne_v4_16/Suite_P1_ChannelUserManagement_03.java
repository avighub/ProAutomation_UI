package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bulkUserRegnAndModification.BulkUserRegistrationAndModification;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that active network admin can reject\n"+
 *                    " the add initiated Transfer Rules between the same domains.
 * Author Name      : Nirupama mk
 * Created Date     : 19/02/2018
 */

public class Suite_P1_ChannelUserManagement_03 extends TestInit {

    private User whs_278_279;
    private OperatorUser optUser, optSuspendResume, o2cInitiator, usrDelete;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        optSuspendResume = DataFactory.getOperatorUserWithAccess("PTY_SCU");
        optUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
        usrDelete = DataFactory.getOperatorUserWithAccess("PTY_DCU");
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_505() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_505", " To Verify that Channel admin can reject" +
                " suspend initiate of channel user");
        try {
            t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);

            whs_278_279 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 3);

            Login.init(t1).login(optSuspendResume);
            ChannelUserManagement.init(t1)
                    .initiateSuspendChannelUser(whs_278_279)
                    .approveSuspendChannelUser(whs_278_279, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC279() throws Exception {

        ExtentTest t2 = pNode.createNode("P1_TC_506", " To verify that channel admin can \n" +
                " reject initiate resume of an suspended Parent channel user");
        try {
            User whs_278_279 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t2)
                    .createChannelUserDefaultMapping(whs_278_279, false);

            Login.init(t2).login(optSuspendResume);

            //Suspend Channel User
            ChannelUserManagement.init(t2)
                    .initiateSuspendChannelUser(whs_278_279)
                    .approveSuspendChannelUser(whs_278_279, true);

            // Initiate resume and then reject resume
            ChannelUserManagement.init(t2)
                    .resumeInitiateChannelUser(whs_278_279)
                    .resumeChannelUserApproval(whs_278_279, false);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    //Descoped
    @Test(priority = 3, enabled = false, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC282() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_508",
                " To verify that system can initiate users in bulk");

        Login.init(pNode).login(optUser);

        User whs282 = new User(Constants.WHOLESALER);
        // To generate CSV File
        String filename = BulkUserRegistrationAndModification.init(t1).
                generateBulkUserRegnCsvFile(whs282, Constants.BULK_USER_STATUS_ADD, whs282.MSISDN);

        // Initiate bulk user registration
        String id = BulkUserRegistrationAndModification.init(t1)
                .initiateBulkUserRegistration(filename);

        ExtentTest t2 = pNode.createNode("P1_TC_150",
                " To verify that system can reject the initiated user in bulk");

        Login.init(pNode).login(optUser);

        // reject Initiated bulk user registration
        BulkUserRegistrationAndModification.init(t2)
                .approveBulkRegnAndModn(id, false);
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_325() throws Exception {

        ExtentTest t4 = pNode.createNode("P1_TC_325", "To verify that channel admin can't delete a" +
                " channel user having pending transactions in application");

        t4.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        try {
            User whs291 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t4)
                    .createChannelUserDefaultMapping(whs291, false);

            Login.init(t4).login(o2cInitiator);
            TransactionManagement.init(t4)
                    .initiateO2C(whs291, "5", "Remark Auto");

            Login.init(t4).login(usrDelete);
            ChannelUserManagement.init(t4)
                    .startNegativeTest()
                    .initiateChannelUserDelete(whs291);

            Assertion.verifyErrorMessageContain("not.delete.channel.existing.transaction",
                    "verify that channel admin can't delete a channel user " +
                            "having pending transactions in application", t4);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}