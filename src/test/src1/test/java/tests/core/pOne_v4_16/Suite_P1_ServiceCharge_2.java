package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.pageObjects.serviceCharge.ServiceCharge_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Network Admin should be able to set a
 * new service charge and commission Profile for a particular service.
 * Author Name      : Nirupama mk
 * Created Date     : 27/02/2018
 */

public class Suite_P1_ServiceCharge_2 extends TestInit {
    OperatorUser opt, usrSChargeCreator;
    User subs, chUser;
    ServiceCharge sCharge;

    @BeforeClass(alwaysRun = true)
    public void beforeTest() throws Exception {
        ExtentTest eSetup = pNode.createNode("Setup", "Delete the service charge all versions as part of pre requisite to run this suite");
        try {
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            opt = DataFactory.getOperatorUserWithAccess("SVC_ADD");
            usrSChargeCreator = DataFactory.getOperatorUserWithAccess("SVC_ADD");
            sCharge = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, subs, chUser,
                    null, null, null, null);

            ServiceChargeManagement.init(eSetup)
                    .deleteServiceChargeAllVersions(sCharge);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.SERVICE_CHARGE_MANAGEMENT})
    public void P1_TC_417() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_417", " To verify that Network Admin should be able to" +
                "set a new service charge and commission Profile for a particular service.");

        try {
            Login.init(t1).login(opt);
            ServiceCharge_Pg1 page = ServiceCharge_Pg1.init(t1);

            page.navAddServiceCharge();
            Assertion.verifyEqual(fl.elementIsDisplayed(page.profileName), true,
                    "Verify that User is successfully navigated to Add Servic Charge Page", t1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.SERVICE_CHARGE_MANAGEMENT})
    public void P1_TC_418_419() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_418", " To verify that Network Admin should be able to" +
                " set a new service charge and commission Profile for a particular service.");

        ExtentTest t2 = pNode.createNode("P1_TC_419", " To verify that Network Admin should be able to" +
                " set a new service charge and commission Profile for a particular service.");

        try {
            // user should be able to fill first page to go to next page
            Login.init(t1).login(usrSChargeCreator);
            ServiceChargeManagement.init(t1)
                    .addInitiateServiceCharge(sCharge);

            /**
             * sub test 2
             */
            ServiceChargeManagement.init(t2)
                    .addInitiateServiceCharge(sCharge)
                    .setCommissionDetail(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            ServiceChargeManagement.init(pNode)
                    .configureServiceCharge(sCharge);
            Assertion.finalizeSoftAsserts();
        }
    }
}
