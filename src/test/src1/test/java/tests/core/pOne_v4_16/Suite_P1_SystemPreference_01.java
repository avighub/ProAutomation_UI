package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.walletPreference.SystemPreference_page1;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: System Preference Functionality Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/15/2018.
 */
public class Suite_P1_SystemPreference_01 extends TestInit {
    private String feqExtCodeNSubs, clientSite;

    private OperatorUser optUsr;
    private ExtentTest TC002, TC003, TC004, TC005;
    private FunctionLibrary fl;

    @FindBy(css = "input[value='FREQ_EXT_CODE_N_SUBS'][type='checkbox']")
    private WebElement checkFreqExtCode;

    @FindBy(css = "input[value='CLIENT_SITE'][type='checkbox']")
    private WebElement checkClientSite;

    @FindBy(className = "wwFormTableC")
    private WebElement webTable;


    @BeforeClass(alwaysRun = true)
    public void preRun() throws Exception {
        feqExtCodeNSubs = MobiquityGUIQueries.fetchDefaultValueOfPreference("FREQ_EXT_CODE_N_SUBS");
        clientSite = MobiquityGUIQueries.fetchDefaultValueOfPreference("CLIENT_SITE");
        optUsr = DataFactory.getOperatorUserWithAccess("PREF001");

        TC002 = pNode.createNode("P1_TC_018",
                "System will show a list of System Preferences on the screen");

        TC003 = pNode.createNode("P1_TC_388",
                "Verify that the System Preference list can be modified by the operator. " +
                        "System will allow you to modify the value in the text box");

        TC004 = pNode.createNode("P1_TC_389",
                "Verify thatSystem will show you the list of System Preferences " +
                        "which has been modified and will ask to Submit the list as a confirmation");

        TC005 = pNode.createNode("P1_TC_390",
                "Verify that System Preference is successfully updated.");

        PageFactory.initElements(driver, this);
        fl = new FunctionLibrary(DriverFactory.getDriver());
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.SYSTEM_PREFERENCES})
    public void Test_01() throws Exception {
        String sFrequency = "2";
        String sClientSite = "www.dummy.com";
        try {
            /**
             * Sub Test TC002
             */
            Login.init(TC002).login(optUsr);

            SystemPreference_page1 page = SystemPreference_page1.init(TC002);
            page.navigateToSystemPreferencePage();

            if (fl.elementIsDisplayed(checkFreqExtCode) && fl.elementIsDisplayed(checkClientSite)) {
                TC002.pass("Preference List is Shown, " +
                        "checked for two of the preferences - Frequecy Of External Code & CLIENT_SITE");
            } else {
                TC002.fail("Failed to verify that Preference List is shown");
                Utils.captureScreen(TC002);
                Assert.fail("Failed to verify Preference Page- exit Test");
            }

            /**
             * Subtest TC003
             */
            Login.init(TC003).login(optUsr);
            SystemPreference_page1 pTC003 = SystemPreference_page1.init(TC003);
            pTC003.navigateToSystemPreferencePage();
            // verify that once check corresponding to a preference is clicked the field becomes enabled

            WebElement element = driver.findElement(By.xpath("//input[@value='FREQ_EXT_CODE_N_SUBS']/ancestor::tr[1]/td/input[@type='text']"));
            WebElement element2 = driver.findElement(By.xpath("//input[@value='CLIENT_SITE']/ancestor::tr[1]/td/input[@type='text']"));
            checkFreqExtCode.click();
            TC003.info("Clicked on Check box corresponding to FREQ_EXT_CODE_N_SUBS");

            if (element.isEnabled()) {
                TC003.pass("Verified that the text box for FREQ_EXT_CODE_N_SUBS is editable after checking the correcponding check box");
            } else {
                TC003.fail("Failed to verify editable state of text box for FREQ_EXT_CODE_N_SUBS");
                Utils.captureScreen(TC003);
            }

            /**
             * Sub Test TC004
             */
            SystemPreference_page1 pTC004 = SystemPreference_page1.init(TC004);
            element.clear();
            element.sendKeys(sFrequency);
            TC004.info("Set the value for FREQ_EXT_CODE_N_SUBS = '" + sFrequency + "'");

            checkClientSite.click(); // set the second preference as well
            element2.clear();
            element2.sendKeys(sClientSite);
            TC004.info("Set the value for CLIENT_SITE = '" + sClientSite + "'");

            pTC004.clickSubmit();
            // verify that both the updated preferences are available
            if (isStringPresentInWebTable("Frequecy Of External Code") &&
                    isStringPresentInWebTable("CLIENT_SITE")) {
                TC004.pass("Updated Preferences are available ready for update");
            } else {
                TC004.fail("Updated Preferences are Not available ready for update");
            }
            Utils.captureScreen(TC004);

            /**
             * Sub Test TC005
             */
            SystemPreference_page1 pTC005 = SystemPreference_page1.init(TC005);
            pTC005.clickPreferenceModifySubmit();

            Assertion.verifyActionMessageContain("mBanking.message.success.preferences.update",
                    "Successfully Updated Preference", TC005);
        } catch (Exception e) {
            markTestAsFailure(e, TC005);
        } finally {
            SystemPreferenceManagement.init(TC005)
                    .updateSystemPreference("FREQ_EXT_CODE_N_SUBS", feqExtCodeNSubs)
                    .updateSystemPreference("CLIENT_SITE", clientSite);
        }
    }

    private boolean isStringPresentInWebTable(String text) {
        return webTable.findElements(By.xpath(".//tr/td[contains(text(), '" + text + "')]")).size() > 0;
    }
}
