package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Escape Defect Duplicate MSISDN
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/25/2018.
 */
public class Suite_P1_EscapeDefect_Duplicate_Msisdn extends TestInit {

    private User channelUser, subscriber;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("Initiating Test");
        try {

            channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            subscriber = new User(Constants.SUBSCRIBER);

            SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "N");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() throws Exception {
        ExtentTest P1_TC_062 = pNode.createNode("P1_TC_062", "System should not allow the agent MSISDN " +
                "to be registered as a Subscriber");
        /*
        create a new subscriber with the same MSISDN in
        which already a channel user is present in system
        validate error message
         */
        try {
            subscriber.setMSISDN(channelUser.MSISDN);

            startNegativeTest();
            Transactions.init(P1_TC_062)
                    .selfRegistrationForSubscriberWithKinDetails(subscriber)
                    .assertMessage("same.user.maisdn")
                    .assertStatus("00256");
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_062);
        }
        Assertion.finalizeSoftAsserts();
    }
}
