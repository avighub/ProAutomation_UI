package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.bulkUserRegnAndModification.BulkUserRegnAndModification_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that channel admin can initiate special super agent  creation
 * Author Name      : Nirupama mk
 * Created Date     : 09/02/2018
 * Duplicate
 */

public class Suite_P1_ChannelUserManagement_02 extends TestInit {

    private OperatorUser optUser;
    private User chUser, chUser2;
    private UserFieldProperties fields;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        optUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
    }

    @Test(priority = 1)
    public void test_01() throws Exception {

        ExtentTest t1 = pNode.createNode("TC286", " Bulk User Registration>> Bulk User Registration " +
                "To Verify that system give proper message when user click on submit button without " +
                " upload any bulk file.");

        Login.init(pNode).login(optUser);

        BulkUserRegnAndModification_Page1 page = new BulkUserRegnAndModification_Page1(pNode);

        page.navigateToBulkUsrRegnAndModifn();

        page.clickSubmit();

        Assertion.verifyErrorMessageContain("bulkupload.error.fileNotUploaded",
                "Verify that system give proper message when user click on submit button without \" +\n" +
                        "                \" upload any bulk file", t1);
    }


    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void test_02() {

        ExtentTest t2 = pNode.createNode("TC292", "To verify that system proceeds to the " +
                " Channel User Registration-Assign Wallet page when atleast one Group Role is selected " +
                "and clicked on Next Button.");
        try {
            chUser2 = new User(Constants.WHOLESALER);

            Login.init(t2).login(optUser);

            ChannelUserManagement.init(t2).initiateChannelUser(chUser2);

            ChannelUserManagement.init(t2)
                    .assignHierarchy(chUser2);

            CommonUserManagement.init(t2)
                    .assignWebGroupRole(chUser2);

            CommonUserManagement.init(t2)
                    .verifyWalletPageDetails(chUser2);

            CommonUserManagement.init(t2)
                    .mapDefaultWalletPreferences(chUser2);
            Thread.sleep(2000);

            ExtentTest t3 = pNode.createNode("P1_TC_509", " Channel user Management >> \n" +
                    "Modify channel user>>To verify that One Bank per MFS provider account has to be\n" +
                    " set as the Primary Account.");

            CommonUserManagement.init(t3)
                    .verifyBankPageDetails(chUser2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void test_04() throws Exception {

        ExtentTest P1_TC_101 = pNode.createNode("P1_TC_101", "To verify that channel user registration " +
                "assign Wallet page should open with all valid details.");

        Login.init(P1_TC_101).login(optUser);

        ChannelUserManagement.init(P1_TC_101)
                .initiateChannelUserModification(chUser);

        CommonUserManagement.init(P1_TC_101)
                .navigateAndSelectWebGroupRole(chUser);

        // .verifyWalletPageDetails(chUser);
        //   System.out.println(chUsr.MSISDN +chUsr.LoginId+" :: "+ chUsr.FirstName +" :: "+optUser.LoginId);

        boolean status = Utils.checkElementPresent("0", Constants.FIND_ELEMENT_BY_ID);
        Assertion.verifyEqual(status, true, "MFS Provider Field", P1_TC_101);

        status = Utils.checkElementPresent("walletTypeID0", Constants.FIND_ELEMENT_BY_ID);
        Assertion.verifyEqual(status, true, "Wallet Type Field", P1_TC_101);

        Utils.checkElementPresent("grade0", Constants.FIND_ELEMENT_BY_ID);
        Assertion.verifyEqual(status, true, "Wallet Grade Field", P1_TC_101);

        Utils.checkElementPresent("walletRoles0", Constants.FIND_ELEMENT_BY_ID);
        Assertion.verifyEqual(status, true, "Mobile Group Role Wallet Field", P1_TC_101);

        Utils.checkElementPresent("tcp0", Constants.FIND_ELEMENT_BY_ID);
        Assertion.verifyEqual(status, true, "TCP Profile Name", P1_TC_101);

        Utils.checkElementPresent("uniWalNum0", Constants.FIND_ELEMENT_BY_ID);
        Assertion.verifyEqual(status, true, "Unique Payment Instrumnet Number Field", P1_TC_101);

        Utils.checkElementPresent("add1_addChannelUser_counterList_0__primaryAccountSelected", Constants.FIND_ELEMENT_BY_ID);
        Assertion.verifyEqual(status, true, "Primary Account Field", P1_TC_101);

        Utils.checkElementPresent("add1_addChannelUser_counterList_0__statusSelected", Constants.FIND_ELEMENT_BY_ID);
        Assertion.verifyEqual(status, true, "Status Field", P1_TC_101);

        Utils.captureScreen(P1_TC_101);
    }

    @Test(priority = 5, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void test_05() throws Exception {

        ExtentTest t5 = pNode.createNode("TC291", " To verify that channel admin can't delete a\n" +
                " channel user having pending transactions in application");
        try {
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            OperatorUser channelAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            Login.init(t5).login(channelAdmin);

            OperatorUser deletor = DataFactory.getOperatorUserWithAccess("PTY_DCU");

            Login.init(t5).login(deletor);

            ChannelUserManagement.init(t5)
                    .startNegativeTest()
                    .initiateChannelUserDelete(chUser);

            Assertion.verifyErrorMessageContain("some.wallets.have.balance", "cannot delete", t5);

        } catch (Exception e) {
            //e.printStackTrace();
            markTestAsFailure(e, t5);
        }
    }

    @Test(priority = 6)
    public void test_06() throws Exception {

        ExtentTest t6 = pNode.createNode("TC291", " To verify that channel admin can't delete a\n" +
                " channel user having pending transactions in application");

        Login.init(t6).login(optUser);
        chUser = new User(Constants.WHOLESALER);

        ChannelUserManagement.init(t6).
                createChannelUserDefaultMapping(chUser, true);

        Login.init(t6).login(optUser);
        TransactionManagement.init(t6)
                .initiateO2C(chUser, "50", "Remark Auto");

        //Delete Initiate Channel User
        ChannelUserManagement.init(t6)
                .startNegativeTest()
                .initiateChannelUserDelete(chUser);

        Assertion.verifyErrorMessageContain("not.delete.channel.existing.transaction",
                "Verify user can't be delete initiated after o2c is initiated", t6);
    }
}