package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.entity.WalletPreference;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.reconciliation.Reconciliation_Page;
import framework.pageObjects.walletPreference.WalletPreferences_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;

public class Suite_P1_WalletPreference extends TestInit {

    private UserFieldProperties fields;

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.WALLET_PREFERENCES})
    public void TC023() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_399", "Verify Configuring Wallet Preferences.");
        try {

            WalletPreference wPref = new WalletPreference(Constants.SUBSCRIBER,
                    Constants.REGTYPE_NO_KYC,
                    DataFactory.getDefaultWallet().WalletName, true,
                    null, null);

            Login.init(t1).loginAsSuperAdmin("CAT_PREF");

            Preferences.init(t1)
                    .configureWalletPreferences(wPref);

            ExtentTest t2 = pNode.createNode("P1_TC_022",
                    "Verify that creating new wallet preference will override the earlier mapped preference.");

            //System will delete the earlier Wallet Preference setting for the domain&
            // category and will save the newly set wallet preference
            WalletPreference wPref1 = new WalletPreference(Constants.SUBSCRIBER,
                    Constants.REGTYPE_NO_KYC,
                    DataFactory.getDefaultWallet().WalletName, true,
                    null, null);

            Login.init(t2).loginAsSuperAdmin("CAT_PREF");

            Preferences.init(t2)
                    .configureWalletPreferences(wPref1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void TC682() throws Exception {
        ExtentTest t2 = pNode.createNode("TC682",
                "To verify that there should not be" +
                        " reconciliation gap and system should not throw There " +
                        "is a problem in application error.");

        try {
            User channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //  O2C transaction
            OperatorUser o2cinit = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            Login.init(t2).login(o2cinit);
            String txnid = TransactionManagement.init(t2).initiateO2C(channeluser, "50", "1234");
            TransactionManagement.init(t2).o2cApproval1(txnid);

            Reconciliation_Page reconPage = new Reconciliation_Page(t2);
            //  Navigate to Reciliation Page to check Reconciled Result
            reconPage.navToReconciliationPage();
            reconPage.validateFieldsInReconcilationScreen();

            verifyIfLabelExist(UserFieldProperties.getField("reconciliation.success.result"), t2);
            Utils.captureScreen(t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    private void verifyIfLabelExist(String labelName, ExtentTest t2) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.xpath("//form[@id='reconciliation_loadReconciliation']//td[@class='tableft']")).isDisplayed()) {
            t2.pass("Successfuuly verified the Label for element - " + labelName);
        } else {
            t2.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t2);
        }
    }


    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.WALLET_PREFERENCES})
    public void TC021() throws Exception {
        ExtentTest t3 = pNode.createNode("P1_TC_398", "Verify Wallet preferences page. System shows the field which needs to be entered");

        SuperAdmin opUser = DataFactory.getSuperAdminWithAccess("CAT_PREF");

        try {
            Login.init(t3).loginAsSuperAdmin(opUser);
            WalletPreferences_Pg1.init(t3).navMapWalletPreferences();

            verifyIfLabelExists(UserFieldProperties.getField("walletPref.domain"), t3);
            verifyIfLabelExists(UserFieldProperties.getField("walletPref.cat"), t3);
            verifyIfLabelExists(UserFieldProperties.getField("walletPref.reg"), t3);

            verifyIfLabel(UserFieldProperties.getField("walletPref.mfs"), t3);
            verifyIfLabel(UserFieldProperties.getField("walletPref.pay"), t3);
            verifyIfLabel(UserFieldProperties.getField("walletPref.wallet"), t3);
            verifyIfLabel(UserFieldProperties.getField("walletPref.grade"), t3);
            verifyIfLabel(UserFieldProperties.getField("walletPref.mobile"), t3);
            verifyIfLabel(UserFieldProperties.getField("walletPref.tcp"), t3);
            verifyIfLabel(UserFieldProperties.getField("walletPref.primary"), t3);
            verifyIfLabel(UserFieldProperties.getField("walletPref.delete"), t3);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        Utils.captureScreen(t3);
    }

    private void verifyIfLabelExists(String labelName, ExtentTest t3) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.xpath("//label[contains(text(),'" + labelName + "')]")).isDisplayed()) {
            t3.pass("Successfully verified the Label for element - " + labelName);
        } else {
            t3.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t3);
        }
    }

    private void verifyIfLabel(String labelName, ExtentTest t3) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.xpath("//b[contains(text(),'" + labelName + "')]")).isDisplayed()) {
            t3.pass("Successfully verified the Label for element - " + labelName);
        } else {
            t3.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t3);
        }
    }
}
