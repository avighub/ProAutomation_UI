package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Geography;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.GeographyManagement;
import framework.util.common.DataFactory;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that valid user can initiate modification of an existing Channel user
 * Author Name      : Nirupama mk
 * Created Date     : 16/02/2018
 */
public class Suite_P1_GeographyMgnt extends TestInit {
    private Geography geo;
    private OperatorUser opt;

    @BeforeClass(alwaysRun = true)
    public void beforeTest() throws Exception {

        Geography geo = new Geography();
        geo = new Geography();
        opt = DataFactory.getOperatorUserWithAccess("VIEWGRPHDOMAIN");
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void test_01() throws Exception {

        ExtentTest t1 = pNode.createNode("TC037", " To verify that the valid user should able to Add Pseudo Category.");

        //Add a new Zone

        Login.init(t1).login(opt);

        Geography geo = new Geography();

        GeographyManagement.init(t1).addZone(geo);

        //Fetch the newly added zone name
        String zonename = geo.ZoneName;

        ExtentTest t2 = pNode.createNode("TC012", " Modify Zone To verify that valid user is able to modify Zone");

        GeographyManagement.init(t1).modifyZone(geo, "Active");

    }
}
