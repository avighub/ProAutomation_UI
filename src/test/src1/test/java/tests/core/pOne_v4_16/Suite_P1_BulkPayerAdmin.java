package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_P1_BulkPayerAdmin extends TestInit {

    /*
     * Company Name     : Comviva Technologies Ltd.
     * Application Name : Mobiquity 4.6.0
     * Objective        : P1: Bulk Payer Admin
     * Author Name      : Gurudatta Praharaj
     * Date             : 6/20/2018.
     */

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void Test_01() {
        /*
        Login As Enterprise User
        Navigate to Bulk Payer Admin Management
        Initiate & Approve a new Bulk Payer Admin
        validate success message
        Initiate Modify & Approve the newly created Bulk Payer Admin
        Validate Success a
         */
        ExtentTest P1_TC_593 = pNode.createNode("P1_TC_593", "To check if bulk payer modification can be done successfully");
        try {
            User enUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            Login.init(P1_TC_593).login(enUser);

            OperatorUser bulkPayAdm = new OperatorUser(Constants.BULK_PAYER_ADMIN);
            OperatorUserManagement.init(P1_TC_593)
                    .initiateOperatorUser(bulkPayAdm)
                    .approveOperatorUser(bulkPayAdm);

            ExtentTest P1_TC_594 = pNode.createNode("P1_TC_594", "To check if bulk payer modification can be approved");

            OperatorUserManagement.init(P1_TC_594)
                    .initModifyOrDeleteOperator(bulkPayAdm, true);

            OperatorUserManagement.init(P1_TC_594)
                    .approveModifyOperatorUser(bulkPayAdm, true);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_593);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
