package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.PseudoUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.PseudoUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that the valid user should able to Add Pseudo Category.
 * Author Name      : Nirupama mk
 * Created Date     : 13/02/2018
 */

public class Suite_P1_PseudoUserManagement_01 extends TestInit {
    private User channeluser;
    private OperatorUser chnlAdmin, modifyApprover;
    private String categoryName;
    private String catName;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            chnlAdmin = DataFactory.getOperatorUserWithAccess("PSEUDO_ADD2", Constants.CHANNEL_ADMIN);
            channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE,
            FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.MONEY_SMOKE})
    public void TC755() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_597", "To verify that the valid user should able to Add Pseudo Category.");

        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            PseudoUserManagement.init(t1).initiateAndApprovePseudoCategory(channeluser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM,
            FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.MONEY_SMOKE})
    public void TC756_757() throws Exception {

        ExtentTest t3 = pNode.createNode("TC_ECONET_0255", "To verify that the valid user should able to add Pseudo User.");
        t3.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            PseudoUser pseudoUser = new PseudoUser();
            pseudoUser.setParentUser(channeluser);
            PseudoUserManagement.init(t3).createCompletePseudoUser(pseudoUser);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0254() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0254", "To verify that the valid user(Channel Admin) should able to Modify Pseudo Category.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        catName = PseudoUserManagement.init(t1).UpdatePseudoCategory(channeluser);
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 5, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT,
            FunctionalTag.MONEY_SMOKE})
    public void P1_TC_600() throws Exception {

        ExtentTest t5 = pNode.createNode("P1_TC_600", "To verify that Pseudo User Should be able to do Cashin ");
        try {
            channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            TransactionManagement.init(t5).makeSureChannelUserHasBalance(channeluser);

            //Checking initial Balance
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            BigDecimal initialUserBalance = balance.Balance;

            //Creating PseudoUser
            PseudoUser pseudoUser = new PseudoUser();
            pseudoUser.setParentUser(channeluser);
            PseudoUserManagement.init(t5).createCompletePseudoUser(pseudoUser);
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //Performing Transaction
            Transactions.init(t5).initiateCashInthroughPseudoUser(subs, pseudoUser, new BigDecimal(10));

            //Checking final balance
            balance = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            BigDecimal currentUserBalance = balance.Balance;

            t5.info("Initial Balance: " + initialUserBalance + " : Current Balance: " + currentUserBalance);
            BigDecimal amount = initialUserBalance.subtract(currentUserBalance);

            Assertion.verifyAccountIsDebited(initialUserBalance, currentUserBalance, amount,
                    "Account Is Debited With Service Charge", t5);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0797() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0797", "To verify that the valid user(Channel User) should able to delete Pseudo Category.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        PseudoUserManagement.init(t1).DeletePseudoUserCategory(channeluser);
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 7, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0798() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0798", "To verify that the valid user should able to modify Pseudo User.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        PseudoUser pseudoUser = new PseudoUser();
        PseudoUserManagement.init(t1).createModifyPseudoUser(pseudoUser, channeluser);
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 8, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0799() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0799", "To verify that the valid user should able to Delete Pseudo User.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        PseudoUser pseudoUser = new PseudoUser();
        PseudoUserManagement.init(t1).PseudoUserDeletion(pseudoUser, channeluser);
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0800() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0800", "To verify that the valid user(Channel admin) should able to modify pseudo users group roles.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        PseudoUser pseudoUser = new PseudoUser();
        pseudoUser.setParentUser(channeluser);
        PseudoUserManagement.init(t1).createandUpdatePseudoGroupRole(pseudoUser, channeluser);
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0801() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0801", "To verify that the valid user should able to modify pseudo users TCP.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        PseudoUser pseudoUser = new PseudoUser();
        PseudoUserManagement.init(t1).createandUpdatePseudoTCP(pseudoUser, channeluser);
        Assertion.finalizeSoftAsserts();
    }
}

