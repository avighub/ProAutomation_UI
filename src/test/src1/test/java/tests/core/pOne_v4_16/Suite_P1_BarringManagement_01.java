package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_P1_BarringManagement_01 extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.PSEUDO_USER_MANAGEMENT})
    public void P1_TC_127_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_127_1",
                "Verify that a Channel admin will not be able to Unbar another user if the user is of same or upper level hierarchy.")
                .assignCategory(FunctionalTag.BARRING_MANAGEMENT);
        try {
            // get A barred Operator User
            String parentCategory = DataFactory.getParentCategoryCode(Constants.CHANNEL_ADMIN);
            OperatorUser barChAdm = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.CHANNEL_ADMIN);
            OperatorUser barNwAdm = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);

            // get Operator User with same hierarchy and try to unbar the user
            OperatorUser validChAdm = DataFactory.getOperatorUserWithAccess("PTY_BLKL", Constants.CHANNEL_ADMIN);

            Login.init(t1).login(validChAdm);

            startNegativeTest();
            CommonUserManagement.init(t1).unBarOperatorUser(barChAdm);

            Assertion.verifyErrorMessageContain("can.only.unbar.subscriberchannel.categusers",
                    "Verify that a Channel admin will not be able to Unbar Same hierarchy Level User", t1, barChAdm.FirstName);

            CommonUserManagement.init(t1).unBarOperatorUser(barNwAdm);

            Assertion.verifyErrorMessageContain("can.only.unbar.subscriberchannel.categusers",
                    "Verify that a Channel admin will not be able to Unbar higher hierarchy level User", t1, barNwAdm.FirstName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();

        }
    }
}
