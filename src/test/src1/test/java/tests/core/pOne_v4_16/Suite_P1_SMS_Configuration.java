package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.preferences.NotificationConfiguration_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;
import java.util.NoSuchElementException;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Network Admin should be able to understand how to modify\n" +
 " an existing SMS in the system through WEB
 * Author Name      : Nirupama mk / Rahul Rana
 * Created Date     : 12/02/2018
 */
public class Suite_P1_SMS_Configuration extends TestInit {

    private OperatorUser opUsr;
    private String messageCodeStatic, messageCodeDynamic;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        opUsr = DataFactory.getOperatorUserWithAccess("SMS_CR");
        messageCodeStatic = "01043"; // this message code is fetched from messages_en.properties
        messageCodeDynamic = "10071"; // this message code is fetched from messages_en.properties
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_084() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_084",
                "Verify  SMS Configuration page. Fields and available actions.");

        Login.init(t1).login(opUsr);
        NotificationConfiguration_pg1 page = new NotificationConfiguration_pg1(t1);
        try {
            page.navigateToSMSConfig();
            page.clickSmsConfigRadio();
            Utils.captureScreen(t1);

            Utils.putThreadSleep(5000);
            Assertion.verifyEqual(page.SMSRadio.isDisplayed(), true,
                    "Verify that an option to check radio SMS is available", t1);

            Assertion.verifyEqual(page.serviceCode.isDisplayed(), true,
                    "Verify that an option to Select Service Code is available", t1);

            Assertion.verifyEqual(page.messageCode.isDisplayed(), true,
                    "Verify that an option to select message code is available", t1);

            Assertion.verifyEqual(page.languageDdown.isDisplayed(), true,
                    "Verify that an option to select language is available", t1);

            Assertion.verifyEqual(page.transactionDataCode.isDisplayed(), true,
                    "Verify that an option to Select transaction Data Code is available", t1);

            Assertion.verifyEqual(page.addButton.isDisplayed(), true,
                    "Verify that an option to add Transaction Data Code is available as an ADD Button", t1);

            Assertion.verifyEqual(page.messageTextArea.isDisplayed(), true,
                    "Verify that Message Text area is available", t1);

            Assertion.verifyEqual(page.submitButton.isDisplayed(), true,
                    "Verify that an option to submit any changes in SMS configuration page is available", t1);

        } catch (NoSuchElementException e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.SMS_CONFIGURATION})
    public void P1_TC_023() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_023",
                "Verify that in selecting the message code, the message section shows the respective message.");
        try {
            Login.init(t1).login(opUsr);
            String messageText = Preferences.init(t1)
                    .getSMSMessageText(Services.O2C, messageCodeStatic);

            Assertion.verifyMessageContain(messageText,
                    messageCodeStatic,
                    "Verify that the Message Text is populated for the respective message code",
                    t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0})

    public void P1_TC_024() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_024", "Verify modifying existing SMS to a static text.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0);

        String newStaticText = "String For Testing Purpose";
        Login.init(t1).login(opUsr);

        try {
            Preferences.init(t1)
                    .updateSMSConfigurationMessageText(Services.O2C, messageCodeStatic, newStaticText);

            // verify that newly set message is reflected
            String messageText = Preferences.init(t1)
                    .getSMSMessageText(Services.O2C, messageCodeStatic);

            Assertion.verifyEqual(messageText,
                    newStaticText,
                    "Verify that the Newly set Static text is reflected properly",
                    t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Preferences.init(t1).updateSMSConfigurationMessageText(Services.O2C, messageCodeStatic,
                    MessageReader.getMessage(messageCodeStatic, null));
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.SMS_CONFIGURATION})
    public void P1_TC_025() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_025",
                "Verify modifying existing SMS to a dynamic text. Verify adding dynamic transaction code data");

        Login.init(t1).login(opUsr);

        // Pre Check
        String messageTextDynamic = Preferences.init(t1)
                .getSMSMessageText(Services.O2C, messageCodeDynamic);
        Assertion.verifyEqual(messageTextDynamic.trim(),
                MessageReader.getMessage(messageCodeDynamic, null).trim(),
                "Pre Check, verify that message text is displayed correct for the respective message Code",
                t1);

        try {
            NotificationConfiguration_pg1 page1 = new NotificationConfiguration_pg1(t1);

            page1.navigateToSMSConfig();
            page1.selectServiceByValue(Services.O2C);
            page1.selectMessageByValue(messageCodeDynamic);
            page1.selectLanguage();

            // get the options from the transaction data Code select
            List<String> transactionDataCode = page1.getTransactionDataCode();

            // select each code and add to message text area as dynamic Message
            page1.setMessageText(messageTextDynamic);
            for (String code : transactionDataCode) {
                page1.selectTransactionCodeByValue(code);
                page1.clickAddButton();
                messageTextDynamic += code; // added code to the expected string
            }
            page1.clickSubmit();

            Assertion.verifyActionMessageContain("sms.cr.message.update.success",
                    "Verify Updating Text Message for SMS Configuration using dynamic transaction Data", t1, messageCodeDynamic);

            // now fetch the updated new message
            String messageNewText = Preferences.init(t1)
                    .getSMSMessageText(Services.O2C, messageCodeDynamic);

            // this new Message should be equal to processed messageText
            Assertion.verifyEqual(messageNewText, messageTextDynamic,
                    "Verify that the Newly set Dynamic text is reflected properly", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Preferences.init(t1).updateSMSConfigurationWithTransactionData(Services.O2C, messageCodeDynamic,
                    MessageReader.getMessage(messageCodeDynamic, null));

        }
        Assertion.finalizeSoftAsserts();
    }


}