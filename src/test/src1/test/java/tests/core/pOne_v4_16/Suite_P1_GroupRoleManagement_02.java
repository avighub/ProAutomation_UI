package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.GradeDB;
import framework.entity.MobileGroupRole;
import framework.entity.WebGroupRole;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.pageObjects.groupRole.GroupRole_Page1;
import framework.pageObjects.groupRole.GroupRole_Page2;
import framework.pageObjects.groupRole.GroupRole_Page3;
import framework.pageObjects.groupRole.GroupRole_Page4;
import framework.util.common.*;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify various Fields and Buttons in Group Role Management (14-P1 Cases)
 * Author Name      : Jyoti Katiyar
 * Created Date     : 01/02/2018
 */

public class Suite_P1_GroupRoleManagement_02 extends TestInit {
    private WebGroupRole webGroupRole;
    private List<String> applicableRole;
    private WebDriverWait wait;
    private FunctionLibrary fl;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        applicableRole = new ArrayList<>(Arrays.asList("SUBSADDAP", "SUBSADD"));
        webGroupRole = new WebGroupRole(Constants.WHOLESALER, "DummyTestRole" + DataFactory.getRandomNumber(3), applicableRole, 1);
        wait = new WebDriverWait(DriverFactory.getDriver(), 10);
        fl = new FunctionLibrary(DriverFactory.getDriver());
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.GROUP_ROLE_MANAGEMENT})
    public void P1_TC_391_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_391_1", "Verify Add Group Role Page, options to select domain, category and role type");

        try {
            Login.init(t1)
                    .loginAsSuperAdmin("GRP_ROL");

            GroupRole_Page1 page = GroupRole_Page1.init(t1);
            page.navAddWebGroupRole();

            Utils.captureScreen(t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page.DomainName), true, "Verify Domain Name Field is available", t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page.CategoryName), true, "Verify Category Name Field is available", t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page.GradeName), true, "Verify Grade Name Field is available", t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page.RadioMobileGroupRole), true, "Verify option for creating Mobile Role is shown", t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page.RadioWebGroupRole), true, "Verify option for creating Web Group Role is shown", t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page.Submit), true, "Verify option Submitting the form is shown", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

        ExtentTest t2 = pNode.createNode("P1_TC_392_1", "Verify an option to check all roles is available when creating a web role");
        try {
            Login.init(t2)
                    .loginAsSuperAdmin("GRP_ROL");
            GroupRoleManagement.init(t2)
                    .openWebRolePage(webGroupRole.CategoryCode);
            GroupRole_Page2.init(t2)
                    .initiateAddGroupRole();

            Utils.captureScreen(t2);

            Assertion.verifyEqual(fl.elementIsDisplayed(GroupRole_Page4.init(t2).SelectAllRoles),
                    true,
                    "Verify that Select all role option is available", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

        ExtentTest t3 = pNode.createNode("P1_TC_392_3",
                "Verify while adding mobile role, user can select the provider, payment instrument and instrument type.");
        try {
            // try adding mobile role for Category Wholesaler
            List<GradeDB> grades = DataFactory.getGradesForCategory(Constants.WHOLESALER);
            MobileGroupRole mobGroupRole = new MobileGroupRole(Constants.WHOLESALER, grades.get(0).GradeName);

            Login.init(t3)
                    .loginAsSuperAdmin("GRP_ROL");

            GroupRole_Page1 page = GroupRole_Page1.init(t3);
            GroupRole_Page2 pageTwo = GroupRole_Page2.init(t3);
            GroupRole_Page3 pageThree = GroupRole_Page3.init(t3);

            // initiate adding mobile role
            page.navAddWebGroupRole();
            page.selectDomainName(mobGroupRole.DomainName);
            page.selectCategoryName(mobGroupRole.CategoryName);
            page.selectGradeName(mobGroupRole.GradeName);
            page.checkMobileGroupRole();
            page.submit();

            pageTwo.initiateAddGroupRole();

            // get all available provider list from UI
            List<String> providersUI = pageThree.getAllCurrencyProviders();

            String providerName = DataFactory.getAllProviderNames().get(0);
            String paymentInstrument = DataFactory.getAllPaymentInstruments(providerName).get(0);
            // verify provider is available in UI
            Assertion.verifyListContains(providersUI, providerName,
                    "Verify Provider:" + providerName + " is available in UI for selection", t3);

            // select the provider and check for payment instruments
            pageThree.selectCurrencyProvider(providerName);
            List<String> payInstrumentList = pageThree.getAllPaymentInstrument();
            // verify payment instrument is available in UI
            Assertion.verifyListContains(payInstrumentList, "Wallet",
                    "Verify PayInstrument: Wallet is available in UI for selection", t3);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 2, groups = {FunctionalTag.GROUP_ROLE_MANAGEMENT})
    public void TC007() throws Exception {
        ExtentTest t1 = pNode.createNode("TC007", "Verify Add Group role Page.");

        try {
            Login.init(t1)
                    .loginAsSuperAdmin("GRP_ROL");

            GroupRoleManagement.init(t1).openWebRolePage(webGroupRole.CategoryCode);
            GroupRole_Page2 page2 = GroupRole_Page2.init(t1);

            //verify whether all the labels the displayed in UI or not
            page2.verifyIfLabelExists(UserFieldProperties.getField("groupRole.Domain"), t1);
            page2.verifyIfLabelExists(UserFieldProperties.getField("groupRole.Category"), t1);
            page2.verifyIfLabelExists(UserFieldProperties.getField("groupRole.RoleName"), t1);
            page2.verifyIfLabelExists(UserFieldProperties.getField("groupRole.RoleCode"), t1);
            page2.verifyIfLabelExists(UserFieldProperties.getField("groupRole.RoleStatus"), t1);

            //verify all the Buttons are visible or not

            page2.verifyIfButtonExists("Back", t1);
            page2.verifyIfButtonExists("Add ", t1);
            page2.verifyIfButtonExists("Update", t1);
            page2.verifyIfButtonExists("View details", t1);
            page2.verifyIfButtonExists("Duplicate", t1);
            page2.verifyIfButtonExists("Change Default", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.GROUP_ROLE_MANAGEMENT})
    public void P1_TC_393_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_393_1",
                "Add group role and verify that newly added group role is shown in the list of Group Roles for that particular Domain and category.");
        try {
            Login.init(t1)
                    .loginAsSuperAdmin("GRP_ROL");

            GroupRoleManagement.init(t1)
                    .addWebGroupRole(webGroupRole);

            // verify that Newly added web role is available
            if (webGroupRole.isGroupRoleCreated()) {
                GroupRole_Page1.init(t1)
                        .navAddWebGroupRole()
                        .selectDomainName(webGroupRole.DomainName)
                        .selectCategoryName(webGroupRole.CategoryName)
                        .checkWebGroupRole();

                GroupRole_Page1.init(t1)
                        .submit();

                Assertion.verifyEqual(GroupRole_Page2.init(t1).isRoleExists(webGroupRole.RoleName),
                        true,
                        "Verify that newly added webGroup Role:" + webGroupRole.RoleName + " is available for respective Domain and Category",
                        t1, true);

            } else {
                t1.fail("Failed to add Group role");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.GROUP_ROLE_MANAGEMENT})
    public void TC011() throws Exception {
        ExtentTest t1 = pNode.createNode("TC011", "Modify Group role, Verify Group Role Modify Page");


        Login.init(t1)
                .loginAsSuperAdmin("GRP_ROL");

        try {
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            GroupRoleManagement.init(t1).openWebRolePage(webGroupRole.CategoryCode);
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            GroupRole_Page2 page2 = GroupRole_Page2.init(t1);

            boolean domainName = Utils.checkElementPresent("(//label[text() = 'Wholesaler'])[1]", Constants.FIND_ELEMENT_BY_XPATH);
            boolean categoryName = Utils.checkElementPresent("(//label[text() = 'Wholesaler'])[2]", Constants.FIND_ELEMENT_BY_XPATH);
            boolean groupRoleName = Utils.checkElementPresent("(//td[contains(text(),'whsWRole01')])[1]", Constants.FIND_ELEMENT_BY_XPATH);
            boolean groupRoleCode = Utils.checkElementPresent("(//td[contains(text(),'whsWRole01')])[2]", Constants.FIND_ELEMENT_BY_XPATH);
            boolean groupRoleStatus = Utils.checkElementPresent("//td[@class = 'tabcol']/div[contains(text(),'Active')]", Constants.FIND_ELEMENT_BY_XPATH);

            boolean backButton = Utils.checkElementPresent("grouprole_forward_back", Constants.FIND_ELEMENT_BY_ID);
            boolean addButton = Utils.checkElementPresent("grouprole_forward_button_add", Constants.FIND_ELEMENT_BY_ID);
            boolean update = Utils.checkElementPresent("grouprole_forward_button_update", Constants.FIND_ELEMENT_BY_ID);
            boolean viewDetails = Utils.checkElementPresent("grouprole_forward_button_view", Constants.FIND_ELEMENT_BY_ID);
            boolean deleteButton = Utils.checkElementPresent("//input[@value = 'Delete']", Constants.FIND_ELEMENT_BY_XPATH);
            boolean duplicateButton = Utils.checkElementPresent("grouprole_forward_button_duplicate", Constants.FIND_ELEMENT_BY_ID);
            boolean changeDefault = Utils.checkElementPresent("grouprole_forward_button_makeDefault", Constants.FIND_ELEMENT_BY_ID);

            //verify whether all the labels the displayed in UI or not

            Assertion.verifyEqual(domainName, true, "Domain Name Available", t1);
            Assertion.verifyEqual(categoryName, true, "Category Name Available", t1);
            Assertion.verifyEqual(groupRoleName, true, "Group Role Name Available", t1);
            Assertion.verifyEqual(groupRoleCode, true, "Group Role Code Available", t1);
            Assertion.verifyEqual(groupRoleStatus, true, "Group Role Status Available", t1);

            //verify all the Buttons are visible or not

            Assertion.verifyEqual(backButton, true, "Back Button Available", t1);
            Assertion.verifyEqual(addButton, true, "Add Button Available", t1);
            Assertion.verifyEqual(update, true, "Update Button Available", t1);
            Assertion.verifyEqual(viewDetails, true, "View Details Button Available", t1);
            Assertion.verifyEqual(deleteButton, true, "Delete Button Available", t1);
            Assertion.verifyEqual(duplicateButton, true, "Duplicate Button Available", t1);
            Assertion.verifyEqual(changeDefault, true, "Change Default Button Available", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("TC013", "System will modify the Group Role and will display a successful update " +
                "message to the User.");

        // below code will update the existing web Role
        GroupRoleManagement.init(t2)
                .addWebGroupRole(webGroupRole);
    }


    @Test(priority = 5, groups = {FunctionalTag.P1CORE, FunctionalTag.GROUP_ROLE_MANAGEMENT})
    public void P1_TC_019() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_019", "Verify Update Group role Page for any existing Group role");

        try {
            Login.init(t1)
                    .loginAsSuperAdmin("GRP_ROL");

            GroupRoleManagement.init(t1)
                    .addWebGroupRole(webGroupRole);

            // Navigate to the Update group role page and verify the same
            GroupRoleManagement.init(t1)
                    .openWebRolePage(webGroupRole.CategoryCode);

            GroupRoleManagement.init(t1)
                    .verifyUpdateGroupRolePage(webGroupRole);

            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, dependsOnMethods = "P1_TC_019", groups = {FunctionalTag.P1CORE, FunctionalTag.GROUP_ROLE_MANAGEMENT})
    public void P1_TC_020() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_020",
                "Verify View Group Role Page for any existing Group role");

        Login.init(t1)
                .loginAsSuperAdmin("GRP_ROL");

        try {
            GroupRoleManagement.init(t1)
                    .viewWebGroupRoleDetails(webGroupRole);

            GroupRole_Page2 page2 = GroupRole_Page2.init(t1);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            boolean groupCode = Utils.checkElementPresent("//input[@name='groupRoleCode' and @disabled = 'disabled']", Constants.FIND_ELEMENT_BY_XPATH);
            boolean groupName = Utils.checkElementPresent("//input[@name='groupRoleName' and @disabled = 'disabled']", Constants.FIND_ELEMENT_BY_XPATH);

            Assertion.verifyEqual(groupCode, true, "Group Role Code Is Disabled", t1);
            Assertion.verifyEqual(groupName, true, "Group Role Name Is Disabled", t1, true);

//            page2.verifyIfLabelDisabledOrEnabled("groupRoleCode", t1);
//            page2.verifyIfLabelDisabledOrEnabled("groupRoleName", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, dependsOnMethods = "P1_TC_019", groups = {FunctionalTag.P1CORE, FunctionalTag.GROUP_ROLE_MANAGEMENT})
    public void P1_TC_304() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_304", "To verify that while Super Admin or Network Admin try" +
                " to Delete group role in the system,System asks for confirmation of deletion.")
                .assignCategory(FunctionalTag.GROUP_ROLE_MANAGEMENT);

        ExtentTest t2 = pNode.createNode("TC017",
                "Verify Admin should not be able to delete a group role assigned to any user in the System");

        WebGroupRole baseRole = DataFactory.getWebRoleFromRnR(Constants.WHOLESALER);

        Login.init(t1)
                .loginAsSuperAdmin("GRP_ROL");

        try {
            GroupRoleManagement.init(t1)
                    .openWebRolePage(baseRole.CategoryCode);

            GroupRole_Page2 pageTwo = GroupRole_Page2.init(t1);

            pageTwo.clickOnDeleteWithoutAlertHandle();
            if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
                t1.pass("Successfully verified that Confirmation of delete appears when trying to delete a web Role");
                DriverFactory.getDriver().switchTo().alert().accept();
                GroupRoleManagement.init(t1)
                        .startNegativeTest()
                        .deleteWebGroupRoleDetails(baseRole);

                Assertion.verifyErrorMessageContain("grouprole.deleteUsedGroupRole.Error",
                        "Verify Admin should not be able to delete a group role assigned to any user in the System", t1);
            } else {
                t1.fail("Failed to verify that Confirmation of delete appears when trying to delete a web Role");
                t2.skip("Failed to verify Group Role Delete");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            GroupRoleManagement.init(t2)
                    .deleteWebGroupRoleDetails(webGroupRole);
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void teardown() throws Exception {
        ExtentTest teardown = pNode.createNode("teardown", "Concluding Test");
        try {
            GroupRoleManagement.init(teardown)
                    .deleteWebGroupRoleDetails(webGroupRole);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}






