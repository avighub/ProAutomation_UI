package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.pageObjects.nonFinancialServiceCharge.NFSCApprove_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Approve Service Charge Fields and Functionality Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/5/2018.
 */
public class Suite_P1_ApproveServiceCharge extends TestInit {
    private OperatorUser optApprover;
    private User subUser;
    private ServiceCharge sChargeBALENQ;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Delete all the Service Charge versions. " +
                "Add Initiate the service charge for Approval Tests.");
        try {

            optApprover = DataFactory.getOperatorUserWithAccess("CHARGENON_APP");
            subUser = new User(Constants.SUBSCRIBER);
            sChargeBALENQ = new ServiceCharge(Services.BALANCE_ENQUIRY, subUser, optApprover,
                    null, null, null, null);

            ServiceChargeManagement.init(eSetup)
                    .configureNonFinancialServiceCharge(sChargeBALENQ);

            Login.init(eSetup).login(optApprover);

            ServiceChargeManagement.init(eSetup)
                    .modifyNFSChargeInitiate(sChargeBALENQ);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.SERVICE_CHARGE_MANAGEMENT})
    public void P1_TC_422_423() throws Exception {
        /*
        navigate to non financial service charge approval page
        validate the newly created NFS should be available for approval
        validate Accept and Reject Buttons
        validate Success message depending on approve or reject button
         */

        ExtentTest P1_TC_422 = pNode.createNode("P1_TC_422", "Application will open the page and will show the list of " +
                "Profile Ids created and pending for approval and rejection");
        try {

            NFSCApprove_pg1 page = NFSCApprove_pg1.init(P1_TC_422);

            page.navApproveNFSC();
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            List<String> services = page.getListOfServicesPendingForApproval();

            Assertion.verifyListContains(services, sChargeBALENQ.ServiceChargeName,
                    "Verify " + sChargeBALENQ.ServiceChargeName + " Is Available On Approval Page", P1_TC_422);

            Assertion.verifyEqual(page.getStatusOfTheServicePendingForApproval(sChargeBALENQ.ServiceChargeName), "Update Initiated",
                    "Status Of The Service", P1_TC_422, true);

            ExtentTest P1_TC_423 = pNode.createNode("P1_TC_423", " Application will show the summary of the Service Charge Profile " +
                    "along with the button to Approve/Reject the profile");

            page.verifyApproveAndRejectButtonDisplayForTheService(sChargeBALENQ.ServiceChargeName, P1_TC_423);

            ExtentTest P1_TC_424 = pNode.createNode("P1_TC_424", "Application will activate the profile if Approve button is " +
                    "clicked and show the confirmation message to the user");

            sChargeBALENQ.setIsNFSC();

            ServiceChargeManagement.init(P1_TC_424)
                    .approveServiceCharge(sChargeBALENQ, Constants.SERVICE_CHARGE_APPROVE_CREATE);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_422);
        }
        Assertion.finalizeSoftAsserts();
    }
}
