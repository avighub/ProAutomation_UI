package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by gurudatta.praharaj on 3/23/2018.
 */
public class Suite_P1_StockManagement extends TestInit {

    private OperatorUser stock;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("Initiating Test");
        try {
            stock = DataFactory.getOperatorUserWithAccess("STR_INIT");
            Login.init(setup).login(stock);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void Test_01() throws Exception {
        /*
        login as network admin
        navigate to stock management
        initiate a stock with decimal value.
         */
        ExtentTest TC411 = pNode.createNode("TC411", "To verify that Network Admin can initiate the stock with requested quantity having decimal value.");
        try {
            StockManagement.init(TC411)
                    .initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "10.01");
        } catch (Exception e) {
            markTestAsFailure(e, TC411);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() throws Exception {
        /*
        login as network admin
        navigate to Suit_IMT initiation
        initiate Suit_IMT stock with decimal value.
        and approve at lvl 1
         */
        ExtentTest TC434_TC435 = pNode.createNode("TC434_TC435", "To verify that Network Admin can initiate the Suit_IMT with requested quantity having decimal value. .");
        try {
            String txnID = StockManagement.init(TC434_TC435)
                    .initiateIMT("12345", "10.50", "test");

            ExtentTest TC436 = pNode.createNode("TC436", " To verify that Network  Admin can successfully approve the Suit_IMT at level-1 and request will close at this level when requested quantity is less than Approval limit 1.");
            startNegativeTest();
            StockManagement.init(TC436).approveIMTStockL1(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, TC434_TC435);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.STOCK_MANAGEMENT})
    public void Test_03() throws Exception {
        /*
        Login as network admin
        initiate Suit_IMT
        approve at both the levels
         */
        ExtentTest P1_TC_201 = pNode.createNode("P1_TC_201", "To verify that Network  Admin can successfully approve the Suit_IMT at level-1 and request will go for Second Approval when requested quantity is more than Approval limit 1.");
        try {
            StockManagement.init(P1_TC_201)
                    .initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "10.01");

            String txnID = StockManagement.init(P1_TC_201)
                    .initiateIMT(DataFactory.getRandomNumberAsString(5), "100", "test");

            StockManagement.init(P1_TC_201)
                    .approveIMTStockL1(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_201);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4)
    public void Test_04() throws Exception {
         /*
        login as network admin
        initiate Suit_IMT
        reject at lvl 1
        validate success message
         */
        ExtentTest TC438 = pNode.createNode("To verify that Network Admin can Reject the Initiated Suit_IMT at level 1");
        try {
            String txnID = StockManagement.init(TC438).initiateIMT("12345", "10", "test");
            StockManagement.init(TC438).rejectIMTStockLevel1(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, TC438);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5)
    public void Test_05() throws Exception {
        /*
        login as network admin
        initiate Suit_IMT
        approve at lvl 1
        reject at lvl 2
        validate success message
         */
        ExtentTest TC440 = pNode.createNode("TC440", " To verify that Network Admin can Reject the Approved 1 at level 2");
        try {
            String txnID = StockManagement.init(TC440).initiateIMT("12345", "10", "test");
            ConfigInput.isAssert = false;
            StockManagement.init(TC440).approveIMTStockL1(txnID);
            StockManagement.init(TC440).rejectIMTStockLevel2(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, TC440);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
