package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Suit_IMT Stock Initiation and approval
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/20/2018.
 */
public class Suite_P1_IMT extends TestInit {

    @Test(priority = 1)
    public void TC449() throws Exception {
        /*
            login as operator user
            initiate Suit_IMT stock and approve
         */
        ExtentTest TC449 = pNode.createNode("TC449", "To verify that the for each Suit_IMT partner Etisalat hub) new ‘Category’ will be created in the system.");
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("STOCK_INIT");
            Login.init(TC449).login(optUsr);

            String txnID = StockManagement.init(TC449).initiateIMT("12345", "10", "abcd");
            System.out.println(txnID);
            StockManagement.init(TC449).approveIMTStockL1(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, TC449);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
