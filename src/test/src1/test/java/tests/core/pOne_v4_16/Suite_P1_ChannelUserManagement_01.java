package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that channel admin can initiate special super agent  creation
 * Author Name      : Nirupama mk
 * Created Date     : 06/02/2018
 */
public class Suite_P1_ChannelUserManagement_01 extends TestInit {
    private OperatorUser optAddUser, optModify, optModifyApp, delChUser;
    private User chUser, ssa304, parent304_277;
    private UserFieldProperties fields;
    private String pCategory;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Get Parent Category for SSA and create Parent User");
        try {
            optAddUser = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);
            optModify = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            optModifyApp = DataFactory.getOperatorUserWithAccess("PTY_MCHAPP");
            delChUser = DataFactory.getOperatorUserWithAccess("PTY_DCU", Constants.CHANNEL_ADMIN);


            pCategory = DataFactory.getParentCategoryCode(Constants.SPECIAL_SUPER_AGENT);
            parent304_277 = new User(pCategory);

            ChannelUserManagement.init(setup)
                    .createChannelUserDefaultMapping(parent304_277, false);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC304() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_153", " Channel user Management >> Add channel user " +
                "To verify that channel admin can initiate special super agent  creation.");
        try {

            ssa304 = new User(Constants.SPECIAL_SUPER_AGENT);
            ssa304.setParentUser(parent304_277);

            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(ssa304, false);

//            SMSReader.init(t1).verifyNotificationContain(ssa304.MSISDN, "channeluser.message.sms");

            if (ssa304.isPwdReset == "Y") {
                t1.pass("Successfully Initiated and created a SSA User");
            } else {
                t1.fail("Failed to Initiate Create SSA User");
                Utils.captureScreen(t1);
            }

            ExtentTest t2 = pNode.createNode("P1_TC_320", " Channel user Management >> Delete channel user Approval \n" +
                    "To verify that channel admin can Not reject initiate delete of Parent channel user");

            if (ssa304.isPwdReset == "Y") {
                Login.init(t2).login(delChUser);
                ChannelUserManagement.init(t2)
                        .startNegativeTest()
                        .initiateChannelUserDelete(parent304_277);
                Assertion.verifyErrorMessageContain("channel.error.childExist", "Verify Parent User cannot be crated for SSA user", t2);
            } else {
                t2.skip("Skip delete of Parent User as test TC304 has Failed");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT}, enabled = false)
    //covered in P1_TC_153
    public void P1_TC_381() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_381", " Channel user Management >> Add channel user \n" +
                "To verify that channel admin can initiate special super agent  creation.");

        User ssa = new User(Constants.SPECIAL_SUPER_AGENT);

        ChannelUserManagement.init(t1).createChannelUserDefaultMapping(ssa, false);
        SMSReader.init(t1).verifyNotificationContain(ssa.MSISDN, "channeluser.message.sms");

    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC269() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_502", " Channel user Management >> Add channel user \n" +
                "To verify that all the selectable fields on the Channel User Registration-Assign hierarchy page:\n" +
                "Selectable fields should appear in following sequence:-" +
                "Domain Name, User Category, Owner Name(First name+Last name), Parent Category, Parent First Name (Search Field)\n" +
                "Geography Management");
        try {
            Login.init(t1).login(optAddUser);

            User ssa269 = new User(Constants.SPECIAL_SUPER_AGENT);

            ChannelUserManagement.init(t1)
                    .initiateChannelUser(ssa269);

            boolean domain = Utils.checkElementPresent("domainCodeList", Constants.FIND_ELEMENT_BY_ID);
            boolean category = Utils.checkElementPresent("categoryCodeList", Constants.FIND_ELEMENT_BY_ID);
            boolean ownerName = Utils.checkElementPresent("ownerId", Constants.FIND_ELEMENT_BY_ID);
            boolean parentCategory = Utils.checkElementPresent("parentId", Constants.FIND_ELEMENT_BY_ID);
            boolean firstName = Utils.checkElementPresent("userNameId", Constants.FIND_ELEMENT_BY_ID);
            boolean parentName = Utils.checkElementPresent("parentNameId", Constants.FIND_ELEMENT_BY_ID);
            boolean zone = Utils.checkElementPresent("zoneId", Constants.FIND_ELEMENT_BY_ID);
            boolean geography = Utils.checkElementPresent("geoId", Constants.FIND_ELEMENT_BY_ID);

            Assertion.verifyEqual(domain, true, "Select Domain Dropdown Is Present", t1);
            Assertion.verifyEqual(category, true, "Select Category Dropdown Is Present", t1);
            Assertion.verifyEqual(ownerName, true, "Select Owner Name Dropdown Is Present", t1);
            Assertion.verifyEqual(parentCategory, true, "Select Parent Category Dropdown Is Present", t1);
            Assertion.verifyEqual(firstName, true, " First Name Text box Is Present", t1);
            Assertion.verifyEqual(parentName, true, " Select Parent Name dropdown Is Present", t1);
            Assertion.verifyEqual(zone, true, " Select Zone Dropdown Is Present", t1);
            Assertion.verifyEqual(geography, true, "Select Geography Dropdown Is Present", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC276() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_504", " Channel user Management >> Modify channel user approval \n" +
                "To verify that Channel admin can reject modification request of an Channel user");

        try {

//            User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);

            Login.init(t1).login(optModify);
//            ssa304.LastName = "DummyLast";
            ChannelUserManagement.init(t1)
                    .initiateChannelUserModification(parent304_277);
//            ModifyChannelUser_pg1.init(t1).setLastName(ssa304.LastName);
            ChannelUserManagement.init(t1).completeChannelUserModification();

            Login.init(t1).login(delChUser);
            ChannelUserManagement.init(t1)
                    .approveRejectModifyChannelUser(parent304_277, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_585() throws Exception {
        ExtentTest t6 = pNode.createNode("P1_TC_585", " To verify that valid user should not be able to \n" +
                " add same MSISDN as multiple users.");
        try {
            // fetch existing user from App Data
            User chUser1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            // Set the chUser1 User's MSISDN to chUser2
            User chUser2 = new User(Constants.WHOLESALER);
            chUser2.MSISDN = chUser1.MSISDN;

            Login.init(t6)
                    .login(optAddUser);

            ChannelUserManagement.init(t6)
                    .startNegativeTest()
                    .initiateChannelUser(chUser2);

            Assertion.verifyErrorMessageContain("luser.validation.partyAccessMsisdnidExists",
                    "The given MSISDN is already registered in the system as different user", t6, chUser2.MSISDN);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}