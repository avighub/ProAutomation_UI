package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.serviceCharge.ModifyNewSubscriberCommRule;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: View New Subscriber Commission Rule
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/14/2018.
 */

public class Suite_P1_AddServiceCharge extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() {
        /*
        Login As Operator User
        Navigate To Service Charge > Modify New Sub comm Rule
        select a service and click view button
        verify if Subscriber Commission Rule Is Is Present Or Not
         */
        ExtentTest P1_TC_032 = pNode.createNode("P1_TC_032", "To verify network admin is able to " +
                "view New Subscriber Commission Rule");
        try {
            String subCommRuleId = MobiquityGUIQueries.getSubscriberCommissionRuleId();
            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("SVC_SUBCOMRULE");
            Login.init(P1_TC_032).login(optUsr);

            String actualSubCommRule = ModifyNewSubscriberCommRule.init(P1_TC_032)
                    .navToModifyNewSubCommRule()
                    .selectService(subCommRuleId)
                    .viewDetails().getSubCommRule();

            String expSubCommRule = MobiquityGUIQueries.getSubscriberCommissionRuleName(subCommRuleId);

            Assertion.assertEqual(expSubCommRule, actualSubCommRule,
                    "Subscriber Commission Rule", P1_TC_032, true);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_032);
        }
        Assertion.finalizeSoftAsserts();
    }
}
