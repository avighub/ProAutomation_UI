package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.CurrencyProvider;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.pageObjects.mfsProviderWalletTypeMaster.AddWallet_Page1;
import framework.pageObjects.mfsProviderWalletTypeMaster.ModifyDeleteWallet_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that the valid user should able to Add Pseudo Category.
 * Author Name      : Nirupama mk
 * Created Date     : 28/02/2018
 */

public class Suite_P1_MFSproviderWalletTypeMaster extends TestInit {

    private SuperAdmin saAddWallet, saModWallet;
    private CurrencyProvider providerOne;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {
            saAddWallet = DataFactory.getSuperAdminWithAccess("MFSWTM01");
            saModWallet = DataFactory.getSuperAdminWithAccess("MFSMD");

            providerOne = DataFactory.getDefaultProvider();

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.MFS_PROVIDER_WALLET})
    public void P1_TC_116() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_116", " MFS Provider Wallet Type Master:" +
                "To verify that Wallet Type cannot be linked if Default Wallet Type selected from drop down list" +
                " should not be checked from Wallet Type List.");
        try {
            Login.init(t1).loginAsSuperAdmin(saAddWallet);

            // Negative Testing : add Wallet Without Selecting any Wallet Type

            AddWallet_Page1 page1 = AddWallet_Page1.init(t1);

            page1.navigateToMFSProviderWalletTypeMaster();
            page1.selectProviderName(providerOne.ProviderName);
            page1.selectDefaultWalletByText(DataFactory.getAllWallet().get(0).WalletName);

            page1.Submit.click();

            Assertion.verifyErrorMessageContain("pls.select.oneWallet.Type",
                    "select any wallet type", t1);


            ExtentTest t2 = pNode.createNode("P1_TC_454", " Modify MFS Provider Wallet Types:\n" +
                    "To verify that list of services against each Wallet Types should display in Wallet Service selection window.");

            CurrencyProviderMapping.init(t2).modifyMFSProviderWalletMapping(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getAllWallet().get(0).WalletName);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.MFS_PROVIDER_WALLET})
    public void test_02() throws Exception {

        ExtentTest t3 = pNode.createNode("P1_TC_445", " Modify MFS Provider Wallet Types\n" +
                "To verify that atleast one service should be selected against each Wallet Type for \n" +
                "successful Modification of Wallet Type for MFS Provider");
        try {
            Login.init(t3).loginAsSuperAdmin(saModWallet);

            // Negative Testing : Whiling Modifying, dont select any Service

            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("modify MFS Provider Wallet Mapping", ExtentColor.BLUE);
            t3.info(m); // Method Start Marker

            ModifyDeleteWallet_Page1 page1 = new ModifyDeleteWallet_Page1(t3);

            page1.navigateToModifyDeleteWallet();
            driver.findElement(By.xpath(".//*[text()='" + DataFactory.getDefaultProvider().ProviderName + "']")).click();
            page1.clickOnmodify1();

            WebElement walletCheck = driver.findElement(By.
                    xpath("//td[contains(text(),'" + DataFactory.getAllWallet().get(0).WalletName + "')]//following::input[1]"));

            if (!walletCheck.isSelected() && walletCheck.isEnabled()) {
                walletCheck.click();
                t3.info("Select the Pay Instrument Type: " + DataFactory.getAllWallet().get(0).WalletName);
            }

            page1.clickOnModify2();
            page1.checkAllServices();
            page1.checkAllServices(); // click twice to deselect all services
            page1.submitModification();

            Assertion.verifyErrorMessageContain("Please.select.atleast.one.service.par.wallet",
                    "Select atleast one service", t3);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.MFS_PROVIDER_WALLET})
    public void P1_TC_457() throws Exception {

        ExtentTest t4 = pNode.createNode("P1_TC_457", " Delete MFS Provider Wallet Types:\n" +
                "To verify that a valid user can not delete linked default Wallet Types from a MFS provider");
        try {
            Login.init(t4).loginAsSuperAdmin(saModWallet);

            startNegativeTest();
            CurrencyProviderMapping.init(t4)
                    .deleteMFSProviderWalletMapping(providerOne.ProviderName, DataFactory.getAllWallet().get(0).WalletName);

            Assertion.verifyErrorMessageContain("Default.Wallet.Can.not.Deleted", "Default Wallet Can Not Deleted", pNode);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
