package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.tcp.EditTCP;
import framework.pageObjects.transactionCorrection.TxnCorrectionInitiation_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import framework.util.jigsaw.JigsawOperations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setTxnProperty;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Transaction Correction
 * Author Name      : Rahul Rana & Gurudatta Praharaj
 * Date             : 6/26/2018.
 */
public class Suite_P1_TransactionCorrection_01 extends TestInit {

    private User usrRetailer, p2pReceiver, subs_01, usrWhs, subs_02;
    private OperatorUser initTxnCorrection, appTxnCorrection;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {

        ExtentTest eSetup = pNode.createNode("Setup", "Make sure that the Transaction correction service charge " +
                "is configured in the system. Make sure that Channel user has sufficient Balance");
        try {
            // users from app data
            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();

            initTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRECTION");
            appTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRAPP");
            usrWhs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            usrRetailer = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            subs_01 = new User(Constants.SUBSCRIBER);
            subs_02 = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(eSetup).createDefaultSubscriberUsingAPI(subs_01);
            SubscriberManagement.init(eSetup).createDefaultSubscriberUsingAPI(subs_02);

            // Service charge object
            ServiceCharge cashIn = new ServiceCharge(Services.TXN_CORRECTION, subs_01, usrWhs, null, null, null, null);

            ServiceCharge sChargeInvC2C = new ServiceCharge(Services.INVERSEC2C, usrRetailer, usrWhs, null, null, null, null);
            ServiceCharge sChargeInvC2CRev = new ServiceCharge(Services.TXN_CORRECTION, usrWhs, usrRetailer, null, null, null, null);

            ServiceCharge c2c = new ServiceCharge(Services.C2C, usrWhs, usrRetailer, null, null, null, null);
            ServiceCharge c2cRev = new ServiceCharge(Services.TXN_CORRECTION, usrRetailer, usrWhs, null, null, null, null);

            ServiceCharge CashOut = new ServiceCharge(Services.CASHOUT, subs_01, usrWhs, null, null, null, null);
            ServiceCharge cashOutRev = new ServiceCharge(Services.TXN_CORRECTION, usrWhs, subs_01, null, null, null, null);

            ServiceCharge p2p = new ServiceCharge(Services.P2PNONREG, subs_01, initTxnCorrection, null, null, null, null);
            ServiceCharge p2pRev = new ServiceCharge(Services.TXN_CORRECTION, subs_01, initTxnCorrection, null, null, null, null);
            ServiceCharge p2pRev1 = new ServiceCharge(Services.TXN_CORRECTION, initTxnCorrection, subs_01, null, null, null, null);

            ServiceCharge cashInOth = new ServiceCharge(Services.CASH_IN_OTHERS1, usrWhs, subs_01, null, null, null, null);
            ServiceCharge cashInOthRev = new ServiceCharge(Services.TXN_CORRECTION, subs_01, usrWhs, null, null, null, null);

            LinkedList<ServiceCharge> transRules = new LinkedList<>();
            transRules.addAll(Arrays.asList(cashIn, sChargeInvC2C, sChargeInvC2CRev, c2c, c2cRev,
                    CashOut, cashOutRev, p2p, p2pRev, p2pRev1, cashInOth, cashInOthRev));

            for (ServiceCharge rules : transRules) {
                TransferRuleManagement.init(eSetup).configureTransferRule(rules);
            }

            SystemPreferenceManagement.init(eSetup)
                    .updateSystemPreference("TXN_CORR_SERVICETYPE",
                            "'MERCHPAY','P2P','CASHIN','CASHOUT','C2C','INVC2C','CASHINOTH','CASHINOTHR','ESCROWTRF'");

            //For P2P:NON-REG
            SystemPreferenceManagement.init(eSetup)
                    .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", GlobalData.defaultWallet.WalletId);

            TransactionManagement.init(eSetup)
                    .makeSureChannelUserHasBalance(usrRetailer);

            TransactionManagement.init(eSetup)
                    .makeSureChannelUserHasBalance(usrWhs);

            TransactionManagement.init(eSetup)
                    .makeSureLeafUserHasBalance(subs_01);

            TransactionManagement.init(eSetup)
                    .makeSureLeafUserHasBalance(subs_02);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.TRANSACTIONS_CORRECTION})
    public void P1_TC_109() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_109",
                "Verify Transaction Correction details under the Initiation Page.");

        t1.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", Services.CASHIN));

        Login.init(t1)
                .login(usrWhs);

        String txnId = Transactions.init(t1)
                .initiateCashIn(subs_01, usrWhs, new BigDecimal("1")).TransactionId;

        if (txnId == null) {
            markTestAsFailure("Txn Id is null, exiting the test", t1);
        }

        t1.info("Transaction Id: " + txnId);

        try {
            Login.init(t1).login(initTxnCorrection);
            TxnCorrectionInitiation_page1 page = TxnCorrectionInitiation_page1.init(t1);
            page.navigateToTxnCorrection()
                    .enterTransactionID(txnId)
                    .clickSubmit();
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            /*
             * Verify the Data
             */
            Assertion.verifyEqual(page.getTransactionIdFromUI(), txnId, "Verify Transaction Id is Shown correct", t1);
            Assertion.verifyEqual(page.getServicTypeFromUI(), Services.CASHIN, "Verify Service type is Shown correct", t1);
            Assertion.verifyEqual(page.getTransferValueFromUI(), "1", "Verify transferred amount is Shown correct", t1);
            Assertion.verifyEqual(page.getPayerMsisdnFromUI(), usrWhs.MSISDN, "Verify Payer MSISDN is Shown correct", t1);
            Assertion.verifyEqual(page.getPayeeMsisdnFromUI(), subs_01.MSISDN, "Verify Payee MSISDN is Shown correct", t1);
            Utils.captureScreen(t1);

            Assertion.finalizeSoftAsserts();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.TRANSACTIONS_CORRECTION})
    public void P1_TC_180() throws Exception {
        ExtentTest t2 = pNode.createNode("P1_TC_180",
                "To verify that system should successfully perform transaction correction for Inverse C2C.");

//        GroupRoleManagement.init(t2)
//                .addOrRemoveSpecificMobileRole(usrRetailer, "Normal", "Inverse C2C Sender",
//                        DataFactory.getDefaultProvider().ProviderName, true);
//
//        GroupRoleManagement.init(t2)
//                .addOrRemoveSpecificMobileRole(usrRetailer, "Normal", "Inverse C2C Receiver",
//                        DataFactory.getDefaultProvider().ProviderName, true);

        t2.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", Services.INVERSEC2C));

        String invC2cAmount = "1";

        Login.init(t2)
                .login(usrWhs);

        String txnID = TransactionManagement.init(t2)
                .inverseC2C(usrRetailer, invC2cAmount);

        try {
            String reqID = MobiquityGUIQueries.dbGetServiceRequestId(txnID);
            JigsawOperations.setUseServiceInBody(false);
            Transactions.init(t2)
                    .inversec2cConfirmation(usrRetailer, reqID, true);

        } finally {

            TransactionCorrection.init(t2)
                    .initAndApproveTxnCorrection(txnID, false, false);

            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.TRANSACTIONS_CORRECTION})
    public void P1_TC_181() throws Exception {
        ExtentTest t3 = pNode.createNode("P1_TC_181",
                "Transaction correction rejection. Verify that an initiate transaction correction can be rejected successfully");

        t3.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", Services.CASHIN));
        try {
            Login.init(t3)
                    .login(usrWhs);

            String txnId = Transactions.init(t3)
                    .initiateCashIn(subs_01, usrWhs, new BigDecimal("1")).TransactionId;

            if (txnId == null) {
                markTestAsFailure("Txn Id is null, exiting the test", t3);
            }

            String txnCorrectionID = TransactionCorrection.init(t3)
                    .initiateTxnCorrection(txnId, false, false);

            TransactionCorrection.init(t3)
                    .approveOrRejectTxnCorrection(txnCorrectionID, false);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE})
    public void P1_TC_179() throws Exception {
        ExtentTest t4 = pNode.createNode("P1_TC_179", "To verify that system should successfully perform transaction correction for C2C.");

        t4.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", Services.C2C));
        try {
            Login.init(t4).login(usrWhs);

            String txnID = TransactionManagement.init(t4)
                    .performC2C(usrRetailer, "1", DataFactory.getRandomNumberAsString(5));

            Login.init(t4).login(initTxnCorrection);

            String serviceId = TransactionCorrection.init(t4)
                    .initiateTxnCorrection(txnID, false, false);

            Login.init(t4).login(appTxnCorrection);

            TransactionCorrection.init(t4)
                    .approveOrRejectTxnCorrection(serviceId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.P1CORE})
    public void P1_TC_362() throws Exception {
        ExtentTest t5 = pNode.createNode("P1_TC_362", "To verify that valid  user cannot initiate a  " +
                "Successful P2P Non reg revert transaction when user click on Reject button , through transaction correction  " +
                "when both users belong to same MFS provider.");
        try {
            t5.info("Set txn properties : txn.reversal.allowed.services");
            setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
            setTxnProperty(of("txn.reversal.allowed.services", Services.P2PNONREG));

            p2pReceiver = new User(Constants.SUBSCRIBER);

            String txnID = Transactions.init(t5)
                    .p2pNonRegTransaction(subs_01, p2pReceiver, new BigDecimal("1")).TransactionId;

            Login.init(t5).login(initTxnCorrection);

            String txnCorrId = TransactionCorrection.init(t5)
                    .initiateTxnCorrection(txnID, false, false);

            Login.init(t5).login(appTxnCorrection);

            TransactionCorrection.init(t5)
                    .approveOrRejectTxnCorrection(txnCorrId, false);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.P1CORE}, enabled = false)
    //txn correction is not possible from 4.16 for merchant payment
    public void P1_TC_543() throws Exception {
        ExtentTest t6 = pNode.createNode("P1_TC_543", "To verify that transaction correction " +
                "should be successful for merchant payment.");
        try {
            t6.info("Set txn properties : txn.reversal.allowed.services");
            setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
            setTxnProperty(of("txn.reversal.allowed.services", Services.MERCHANT_PAY));

            User merchant = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);
            User merPayReceiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            String serviceID = Transactions.init(t6)
                    .initiateMerchantPayment(merchant, merPayReceiver, "1").ServiceRequestId;

            String txnID = Transactions.init(t6)
                    .resumeAmbiguousTransaction(Services.RESUME_MERCHANT_PAY, serviceID, "true").TransactionId;

            Login.init(t6).login(initTxnCorrection);

            String txnCorrId = TransactionCorrection.init(t6)
                    .initiateTxnCorrection(txnID, false, false);

            Login.init(t6).login(appTxnCorrection);

            TransactionCorrection.init(t6)
                    .approveOrRejectTxnCorrection(txnCorrId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t6);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 7, groups = {FunctionalTag.P1CORE})
    public void P1_TC_544() throws Exception {
        ExtentTest t7 = pNode.createNode("P1_TC_544", "To verify that transaction correction should be successful for Cash In to other service.");
        try {

            String txnID = Transactions.init(t7)
                    .initiateCashInOthers(subs_02, usrWhs, subs_01, "1").TransactionId;

            String serviceID = Transactions.init(t7)
                    .initiateTransactionReversalByOperator(initTxnCorrection, txnID,
                            Constants.NORMAL_WALLET, Services.CASH_IN_OTHERS1,
                            "false", "false").ServiceRequestId;

            Transactions.init(t7)
                    .approveTransactionReversal(serviceID, appTxnCorrection);

        } catch (Exception e) {
            markTestAsFailure(e, t7);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 8, groups = {FunctionalTag.P1CORE})
    public void P1_TC_547_P1_TC_082() throws Exception {
        ExtentTest t8 = pNode.createNode("P1_TC_547_P1_TC_082", "To verify that transaction correction service " +
                "is not able to approve if sender is barred after initiate the transaction correction.");

        try {
            User chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            User sub1 = new User(Constants.SUBSCRIBER);
            User sub2 = new User(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal("1");

            SubscriberManagement.init(t8)
                    .createDefaultSubscriberUsingAPI(sub1);

            Transactions.init(t8)
                    .initiateCashIn(sub1, chUsr, new BigDecimal("10"));

            String txnID = Transactions.init(t8)
                    .p2pNonRegTransaction(sub1, sub2, txnAmount).TransactionId;

            String serviceID = Transactions.init(t8)
                    .initiateTransactionReversalByOperator(initTxnCorrection, txnID,
                            Constants.NORMAL_WALLET, Services.P2PNONREG,
                            "false", "false").ServiceRequestId;

            Login.init(t8).login(chUsr);

            SubscriberManagement.init(t8)
                    .barSubscriber(sub1, Constants.BAR_AS_BOTH);

            startNegativeTest();
            Transactions.init(t8)
                    .approveTransactionReversal(serviceID, appTxnCorrection)
                    .assertMessage("payee.barred.both", sub1.MSISDN);

            stopNegativeTest();
            ChannelUserManagement.init(t8)
                    .unBarChannelUser(sub1, Constants.USER_TYPE_SUBS);

            ExtentTest t9 = pNode.createNode("P1_TC_548", "To verify that transaction correction service is not " +
                    "able to approve if receiver is suspended after initiate the transaction correction.");

            txnID = Transactions.init(t9)
                    .p2pNonRegTransaction(sub1, sub2, txnAmount).TransactionId;

            Login.init(t9).login(initTxnCorrection);

            serviceID = Transactions.init(t9)
                    .initiateTransactionReversalByOperator(initTxnCorrection, txnID,
                            Constants.NORMAL_WALLET, Services.P2PNONREG,
                            "false", "false").ServiceRequestId;

            OperatorUser suspendUsr = DataFactory.getOperatorUserWithAccess("SR_USR");
            Login.init(t9).login(suspendUsr);

            SubscriberManagement.init(t9)
                    .suspendSubscriber(sub1, Constants.USER_TYPE_SUBS);

            startNegativeTest();
            Transactions.init(t9)
                    .approveTransactionReversal(serviceID, appTxnCorrection)
                    .assertMessage("user.suspend", t9);

            stopNegativeTest();
            SubscriberManagement.init(t9)
                    .resumeSubscriber(sub1, Constants.USER_TYPE_SUBS);

            ExtentTest t10 = pNode.createNode("P1_TC_549", "To verify that valid  user can initiate a  " +
                    "Successful transaction correction for Cash in when user click on Confirm button , " +
                    "through transaction correction  when both users belong to same MFS provider.");

            txnID = Transactions.init(t10)
                    .initiateCashIn(sub1, chUsr, txnAmount).TransactionId;

            serviceID = Transactions.init(t10)
                    .initiateTransactionReversalByOperator(initTxnCorrection, txnID,
                            Constants.NORMAL_WALLET, Services.CASHIN,
                            "true", "true").ServiceRequestId;

            ExtentTest t11 = pNode.createNode("P1_TC_551", "To verify that valid user can approve " +
                    "the initiated CashIn transaction correction i.e [ amount + tax on amount + service charge + " +
                    "tax on service charge + commission + tax on commission ] record");

            Transactions.init(t11)
                    .approveTransactionReversal(serviceID, appTxnCorrection);

            ExtentTest t12 = pNode.createNode("P1_TC_552", "To verify that valid  user can initiate a " +
                    "Successful transaction correction for  Cash out  when user click on Confirm button , " +
                    "through transaction correction  when both users belong to same MFS provider.");

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            serviceID = Transactions.init(t12)
                    .initiateCashOut(sub1, chUsr, txnAmount).ServiceRequestId;
            try {
                txnID = Transactions.init(t12)
                        .cashoutApprovalBysubs(sub1, serviceID).TransactionId;
            } finally {

                serviceID = Transactions.init(t12)
                        .initiateTransactionReversalByOperator(initTxnCorrection, txnID,
                                Constants.NORMAL_WALLET, Services.CASHOUT,
                                "true", "true").ServiceRequestId;

                ExtentTest t13 = pNode.createNode("P1_TC_554", "Transaction Correction Approval:\n" +
                        "To verify that valid user can approve the initiated CashOut transaction correction i.e [ amount + " +
                        "tax on amount + service charge + tax on service charge + commission + tax on commission ] record");

                Transactions.init(t13)
                        .approveTransactionReversal(serviceID, appTxnCorrection);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t8);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    private void updateTCPforInvC2c(ExtentTest test) throws Exception {
        Login.init(test).login(initTxnCorrection);
        EditTCP pageOne = EditTCP.init(test);

        pageOne.navAddInstrumentTcp();
        driver.findElement(By.xpath("//table[@id='myTable']/tbody/tr[@class='list']/td[.='Wholesaler - Retailer']/following-sibling::td[.='Normal']/following-sibling::td[.='Gold Retailer']/following-sibling::td[4]")).click();
        pageOne.clickedit();

        WebElement element = driver.findElement(By.xpath("(//tr[td[text()= 'Inverse C2C Transfer']])[1]/following-sibling::tr[4]/td[3]/input"));
        element.clear();
        element.sendKeys("99999");
        Utils.putThreadSleep(4000);
        WebElement element1 = driver.findElement(By.xpath("(//tr[td[text()= 'Inverse C2C Transfer']])[2]/following-sibling::tr[4]/td[3]/input"));
        element1.clear();
        element1.sendKeys("99999");

        pageOne.clickNext();
        pageOne.clickconfirm();
        pageOne.navInstrumentTcpApproval();
        pageOne.clickdetails();
        pageOne.clickonApprove();
    }
}
