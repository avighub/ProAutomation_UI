package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.master.Master;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.userManagement.ModifyChannelUser_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that valid user can initiate modification of an existing Channel user
 * Author Name      : Nirupama mk
 * Created Date     : 27/02/2018
 */

public class Suite_P1_ChannelUserManagement_05 extends TestInit {
    private OperatorUser optModify, optModifyApp, optSuspend;
    private User whs715_716;

    @BeforeClass(alwaysRun = true)
    private void setup() throws Exception {

        optModify = DataFactory.getOperatorUserWithAccess("PTY_MCU");
        optModifyApp = DataFactory.getOperatorUserWithAccess("PTY_MCHAPP");
        optSuspend = DataFactory.getOperatorUserWithAccess("PTY_SCU", Constants.CHANNEL_ADMIN);

    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC715() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_075", " Channel user Management >> Modify channel user " +
                "To verify that valid user can initiate modification of an existing Channel user");
        try {
            whs715_716 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(whs715_716, false);

            Login.init(t1).login(optModify);
            ChannelUserManagement.init(t1)
                    .initiateChannelUserModification(whs715_716);
            ModifyChannelUser_pg1.init(t1).setLastName("DummyLast");
            ChannelUserManagement.init(t1).completeChannelUserModification();

            Assertion.verifyActionMessageContain("channeluser.modify.approval", "Verify that valid user can initiate modification of an existing Channel user", t1, whs715_716.FirstName, whs715_716.LastName);
            Assertion.finalizeSoftAsserts();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            whs715_716.LastName = "DummyLast";
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC716() throws Exception {
        ExtentTest t2 = pNode.createNode("P1_TC_076", " Channel user Management >> Modify channel user" +
                " approval To verify that valid user can approve modification request of an Channel user");
        try {
            Login.init(t2).login(optModifyApp);

            User chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            ChannelUserManagement.init(t2).modifyChannelUserWithNoChange(chUsr)
                    .modifyUserApproval(chUsr);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void test_02() throws Exception {

        ExtentTest t2 = pNode.createNode("P1_TC_060", " Channel user Management >> Suspend user initiate");
        try {
            Login.init(t2).login(optSuspend);
            ChannelUserManagement.init(t2)
                    .initiateSuspendChannelUser(whs715_716);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
    }

    @Test(priority = 4)
    public void TC285() throws Exception {

        ExtentTest t3 = pNode.createNode("TC285", " To Verify that after successful creation of" +
                " channel user Each Channel User(Merchant Domain) when created will be assigned an Merchant code" +
                " (or Agent code). This code can be used in transactions for single step transactions like Merchant Payment");
        String txAmount = "10";
        User mer285 = new User(Constants.MERCHANT);
        mer285.setMerchantType("OFFLINE");

        ChannelUserManagement.init(t3).createChannelUserDefaultMapping(mer285, false);

        User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        TransactionManagement.init(t3).makeSureLeafUserHasBalance(subs);

        ServiceCharge merchPay = new ServiceCharge(Services.MERCHANT_PAY, subs, mer285, null, null, null, null);
        TransferRuleManagement.init(t3).configureTransferRule(merchPay);

        Transactions.init(t3)
                .initiateMerchantPayment(mer285, subs, txAmount)
                .verifyMessage("api.merchantpay.completed", txAmount, subs.MSISDN, mer285.MSISDN);
    }

    @Test(priority = 5)
    public void test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("TC116", " To verify Service Provider is able  to send a" +
                " customized message to the end user ");
        OperatorUser optUser = DataFactory.getOperatorUserWithAccess("MNT_STS");
        Login.init(t4).login(optUser);

        Master.init(t4).changeNetworkStatus("Network is Suspended");
    }
}