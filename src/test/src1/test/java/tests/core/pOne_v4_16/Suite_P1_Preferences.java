package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.pageObjects.walletPreference.SystemPreference_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Preference
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/28/2018.
 */
public class Suite_P1_Preferences extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() {
        ExtentTest P1_TC_387 = pNode.createNode("P1_TC_387", "To verify Preference section is available " +
                "with the Super Admin where following roles are available under preference\n" +
                "System Preference , Group Role Management , Wallet preference ");
        try {
            /*
            Login as super admin maker
            Click on preferences
            Validate all sub links under preferences
             */
            SuperAdmin sAdm = DataFactory.getSuperAdminWithAccess("PREF001");
            Login.init(P1_TC_387).loginAsSuperAdmin(sAdm);

            SystemPreference_page1.init(P1_TC_387)
                    .navigateToSystemPreferencePage();

            boolean viewWalletPreferenceLink = Utils.checkElementPresent("MPREF_KYC001", Constants.FIND_ELEMENT_BY_ID);
            boolean systemPreference = Utils.checkElementPresent("MPREF_PREF001", Constants.FIND_ELEMENT_BY_ID);
            boolean viewGroupRoles = Utils.checkElementPresent("MPREF_GRP_ROL_VIEW", Constants.FIND_ELEMENT_BY_ID);
            boolean walletPreference = Utils.checkElementPresent("MPREF_CAT_PREFDM", Constants.FIND_ELEMENT_BY_ID);

            Assertion.verifyEqual(viewWalletPreferenceLink, true, "View Wallet Preference Link Is Available", P1_TC_387);
            Assertion.verifyEqual(systemPreference, true, "System Preference Link is Available", P1_TC_387);
            Assertion.verifyEqual(viewGroupRoles, true, "View Group Role Link Is Available", P1_TC_387);
            Assertion.verifyEqual(walletPreference, true, "Wallet Preference Link Is Available", P1_TC_387, true);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_387);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
