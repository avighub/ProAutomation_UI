package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.RechargeOperator;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.kpi.KPIManagement;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: KPI Current Date & Date Range Records Validations
 * Author Name      : Gurudatta Praharaj
 * Date             : 7/06/2018.
 */
public class Suite_P1_KPI extends TestInit {

    private OperatorUser optUsr;
    private String providerID, fromDate, toDate, operatorId;
    private User sub, newSub, merchant, wholesaler, rechargeReceiver;
    private ServiceCharge selfRechargeSub, balEnq, last5txn, p2p;
    private BigDecimal txnAmnt;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("KPIR");
            providerID = DataFactory.getDefaultProvider().ProviderId;
            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            newSub = new User(Constants.SUBSCRIBER);
            rechargeReceiver = new User(Constants.ZEBRA_MER);
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            fromDate = new DateAndTime().getDate(-7);
            toDate = new DateAndTime().getDate(0);
            operatorId = MobiquityGUIQueries.getOperatorId();
            if (operatorId == null) {
                Login.init(eSetup).login(optUsr);
                RechargeOperator newOperator = new RechargeOperator(providerID);
                OperatorUserManagement.init(eSetup).addRechargeOperator(newOperator);
                operatorId = MobiquityGUIQueries.getOperatorId();
            }

            selfRechargeSub = new ServiceCharge(Services.SELF_RECHARGE, sub, rechargeReceiver, null, null, null, null);

            balEnq = new ServiceCharge(Services.BALANCE_ENQUIRY, newSub, optUsr, null, null, null, null);

            last5txn = new ServiceCharge(Services.LAST_N_TRANSACTION, newSub, optUsr, null, null, null, null);

            p2p = new ServiceCharge(Services.P2PNONREG, newSub, optUsr, null, null, null, null);

            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(selfRechargeSub);

            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(p2p);

            ServiceChargeManagement.init(eSetup)
                    .configureNonFinancialServiceCharge(last5txn);

            SystemPreferenceManagement.init(eSetup)
                    .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", GlobalData.defaultWallet.WalletId);

            SystemPreferenceManagement.init(eSetup)
                    .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "N");

            TransactionManagement.init(eSetup)
                    .makeSureChannelUserHasBalance(wholesaler);

            TransactionManagement.init(eSetup)
                    .makeSureLeafUserHasBalance(sub);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    private double kpiCurrentDateRecords(String kpiID, ExtentTest test) throws Exception {
        Login.init(test).login(optUsr);
        KPIManagement.init(test)
                .performKPIUsingCurrentDate(providerID);

        Thread.sleep(Constants.MAX_WAIT_TIME);
        double currentDateRecord = KPIManagement.init(test).getKpiValueFromUI(kpiID);
        return currentDateRecord;
    }

    private double kpiDateRangeRecord(String kpiID, ExtentTest test) throws Exception {
        Login.init(test).login(optUsr);
        KPIManagement.init(test).performKPIUsingDateRange(providerID, fromDate, toDate);

        Thread.sleep(Constants.MAX_WAIT_TIME);
        double dateRangeRecord = KPIManagement.init(test)
                .getKpiValueFromUI(kpiID);

        return dateRangeRecord;
    }

    private void validateKpiResults(double actualValue, double expectedValue, String aboutTc, ExtentTest test) throws Exception {
        if (actualValue < expectedValue) {
            Assertion.verifyNotEqual("Total Record Before Performing Operation: " + actualValue, "Total Record After Performing Operation: " + expectedValue, aboutTc, test);

        } else {
            test.fail("Pre Value: " + actualValue + " is Equals To Post Value: " + expectedValue);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {

        ExtentTest P1_TC_205 = pNode.createNode("P1_TC_205",
                "Verify that after adding a new subscriber the current count in KPI page for 'Number of " +
                        "New Subscriber' is incremented by at least 1");

        ExtentTest P1_TC_206 = pNode.createNode("P1_TC_206",
                "Verify that after adding a new subscriber the current count in KPI page for 'Total Number of " +
                        "New Subscriber' is incremented by at least 1");

        ExtentTest P1_TC_238 = pNode.createNode("P1_TC_238", "Verify that after adding a new subscriber " +
                "the current count in KPI page for 'Number of New Subscriber' for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_239 = pNode.createNode("P1_TC_239", "Verify that after adding a new subscriber " +
                "the current count in KPI page for 'Total Number of Subscriber' for past 7 days is incremented by at least 1");
        try {
            double preNumOfNewSubPast7Days = kpiDateRangeRecord("num.of.new.customers.id", P1_TC_238);

            double preTotalNumberOfSUbsPast7Days = kpiDateRangeRecord("total.number.of.subscribers.id", P1_TC_239);

            double preNewSubsValue = kpiCurrentDateRecords("num.of.new.customers.id", P1_TC_205);

            double preTotalSubsValue = kpiCurrentDateRecords("total.number.of.subscribers.id", P1_TC_206);

            Transactions.init(P1_TC_205)
                    .selfRegistrationForSubscriberWithKinDetails(newSub);

            double postNumOfNewSubPast7Days = kpiDateRangeRecord("num.of.new.customers.id", P1_TC_238);

            double postTotalNumberOfSUbsPast7Days = kpiDateRangeRecord("total.number.of.subscribers.id", P1_TC_239);

            double postNewSubsValue = kpiCurrentDateRecords("num.of.new.customers.id", P1_TC_205);

            double postTotalSubsValue = kpiCurrentDateRecords("total.number.of.subscribers.id", P1_TC_206);

            validateKpiResults(preNewSubsValue, postNewSubsValue, "Number Of New Subscribers", P1_TC_205);
            validateKpiResults(preTotalSubsValue, postTotalSubsValue, "Total Number Of New Subscribers", P1_TC_206);

            validateKpiResults(preNumOfNewSubPast7Days, postNumOfNewSubPast7Days, "Number Of New Subs In Past 7 Days", P1_TC_238);
            validateKpiResults(preTotalNumberOfSUbsPast7Days, postTotalNumberOfSUbsPast7Days, "Total Number Of Subs In Past 7 Days", P1_TC_239);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_205);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE})
    public void Test_02() throws Exception {
        ExtentTest P1_TC_219 = pNode.createNode("P1_TC_219", "Verify that after preforming one " +
                "failed cash in transaction the current count in KPI page for number of failed cash in transaction is incremented by at least 1");

        ExtentTest P1_TC_252 = pNode.createNode("P1_TC_252", "Verify that after preforming one " +
                "failed cash in transaction the current count in KPI page for number of failed cash in transaction for past 7 days is incremented by at least 1");

        double preTotalFailedCashInValuePast7days = kpiDateRangeRecord("total.number.of.failed.cashin.transactions.id", P1_TC_252);
        double preTotalFailedCashInValue = kpiCurrentDateRecords("total.number.of.failed.cashin.transactions.id", P1_TC_219);

        try {
            startNegativeTest();
            Transactions.init(P1_TC_219)
                    .initiateCashIn(sub, wholesaler, new BigDecimal("0"));
        } finally {
            double postTotalFailedCashInValuePast7days = kpiDateRangeRecord("total.number.of.failed.cashin.transactions.id", P1_TC_252);
            double postTotalFailedCasInValue = kpiCurrentDateRecords("total.number.of.failed.cashin.transactions.id", P1_TC_219);

            validateKpiResults(preTotalFailedCashInValue, postTotalFailedCasInValue, "Total Failed CashIn", P1_TC_219);
            validateKpiResults(preTotalFailedCashInValuePast7days, postTotalFailedCashInValuePast7days, "Total Failed CashIn", P1_TC_252);
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE})
    public void Test_03() throws Exception {
        ExtentTest P1_TC_207 = pNode.createNode("P1_TC_207", "Verify that after adding a new merchant the current count in KPI page for 'Number of " +
                "New merchant' is incremented by at least 1");

        ExtentTest P1_TC_208 = pNode.createNode("P1_TC_208", "Verify that after adding a new merchant the current count in KPI page for 'Total Number of " +
                "New merchant' is incremented by at least 1");

        ExtentTest P1_TC_240 = pNode.createNode("P1_TC_240", "Verify that after adding a new merchant the current count in KPI page for 'Number of " +
                "New merchant' for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_241 = pNode.createNode("P1_TC_241", "Verify that after adding a new merchant the current count in KPI page for 'Total Number of " +
                "New merchant' for past 7 days is incremented by at least 1");

        try {
            double preTotalNumOfNewMerPast7Days = kpiDateRangeRecord("total.number.of.merchant.id", P1_TC_241);

            double preNumOfNewMerPast7Days = kpiDateRangeRecord("number.of.new.merchant.id", P1_TC_240);

            double totalPreMerValue = kpiCurrentDateRecords("total.number.of.merchant.id", P1_TC_208);

            double preNewMerValue = kpiCurrentDateRecords("number.of.new.merchant.id", P1_TC_207);

            merchant = new User(Constants.MERCHANT);
            ChannelUserManagement.init(P1_TC_207)
                    .createChannelUserDefaultMapping(merchant, false);

            double postNumOfNewMerPast7Days = kpiDateRangeRecord("number.of.new.merchant.id", P1_TC_240);

            double postTotalNumOfNewMerPast7Days = kpiDateRangeRecord("total.number.of.merchant.id", P1_TC_241);

            double postNewMerValue = kpiCurrentDateRecords("number.of.new.merchant.id", P1_TC_207);

            double totalPostMerValue = kpiCurrentDateRecords("total.number.of.merchant.id", P1_TC_208);

            validateKpiResults(preNewMerValue, postNewMerValue, "Number Of New Merchants", P1_TC_207);
            validateKpiResults(totalPreMerValue, totalPostMerValue, "Total Number Of New Merchants", P1_TC_208);
            validateKpiResults(preTotalNumOfNewMerPast7Days, postTotalNumOfNewMerPast7Days, "Total Number Of New Merchants In past 7 days", P1_TC_241);
            validateKpiResults(preNumOfNewMerPast7Days, postNumOfNewMerPast7Days, "Number Of new merchants in past 7 days", P1_TC_240);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_207);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE}) //depends on test_01
    public void Test_04() throws Exception {
        ExtentTest P1_TC_211 = pNode.createNode("P1_TC_211", "Verify that after preforming one " +
                "transaction the current count in KPI page for amount of transaction is incremented by at least 1");

        ExtentTest P1_TC_212 = pNode.createNode("P1_TC_212", "Verify that after preforming one " +
                "transaction the current count in KPI page for total number of transaction is incremented by at least 1");

        ExtentTest P1_TC_217 = pNode.createNode("P1_TC_217", "Verify that after preforming one " +
                "cash in transaction the current count in KPI page for total number of cash in transaction is incremented by at least 1");

        ExtentTest P1_TC_218 = pNode.createNode("P1_TC_218", "Verify that after preforming one " +
                "successful cash in transaction the current count in KPI page for total number of successful cash in transaction is incremented by at least 1");

        ExtentTest P1_TC_244 = pNode.createNode("P1_TC_244", "Verify that after preforming one " +
                "transaction the current count in KPI page for amount of transaction for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_245 = pNode.createNode("P1_TC_245", "Verify that after preforming one " +
                "transaction the current count in KPI page for total number of transaction for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_250 = pNode.createNode("P1_TC_250", "Verify that after preforming one " +
                "cash in transaction the current count in KPI page for total number of cash in transaction for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_251 = pNode.createNode("P1_TC_251", "Verify that after preforming one " +
                "successful cash in transaction the current count in KPI page for total number of successful cash in transaction for past 7 days is incremented by at least 1");

        try {
            double preTotalAmountOfTxnPast7Days = kpiDateRangeRecord("total.amount.transaction.id", P1_TC_244);

            double preTotalNumOfTxnPast7Days = kpiDateRangeRecord("total.amount.transaction.id", P1_TC_245);

            double preTotalNumOfCashInTxnPast7Days = kpiDateRangeRecord("total.number.of.cashin.transaction.id", P1_TC_250);

            double preTotalNumOfSuccessCashInTxnPast7Days = kpiDateRangeRecord("total.number.of.success.cashin.transactions.id", P1_TC_251);


            double preAmountValue = kpiCurrentDateRecords("total.amount.transaction.id", P1_TC_211);

            double preTotalNumOfTransactionValue = kpiCurrentDateRecords("total.number.of.transaction.id", P1_TC_211);

            double preTotalCashInValue = kpiCurrentDateRecords("total.number.of.cashin.transaction.id", P1_TC_217);

            double preTotalSuccessCashInValue = kpiCurrentDateRecords("total.number.of.success.cashin.transactions.id", P1_TC_218);

            Transactions.init(P1_TC_211)
                    .initiateCashIn(newSub, wholesaler, "5");

            double postTotalAmountOfTxnPast7Days = kpiDateRangeRecord("total.amount.transaction.id", P1_TC_244);

            double postTotalNumOfTxnPast7Days = kpiDateRangeRecord("total.amount.transaction.id", P1_TC_245);

            double postTotalNumOfCashInTxnPast7Days = kpiDateRangeRecord("total.number.of.cashin.transaction.id", P1_TC_250);

            double postTotalNumOfSuccessCashInTxnPast7Days = kpiDateRangeRecord("total.number.of.success.cashin.transactions.id", P1_TC_251);

            double postAmountValue = kpiCurrentDateRecords("total.amount.transaction.id", P1_TC_211);

            double postTotalNumOfTransactionValue = kpiCurrentDateRecords("total.number.of.transaction.id", P1_TC_211);

            double postTotalCashInValue = kpiCurrentDateRecords("total.number.of.cashin.transaction.id", P1_TC_217);

            double postTotalSuccessCashInValue = kpiCurrentDateRecords("total.number.of.success.cashin.transactions.id", P1_TC_218);

            validateKpiResults(preTotalAmountOfTxnPast7Days, postTotalAmountOfTxnPast7Days, "Total Amount Of Transactions Past 7 Days", P1_TC_244);
            validateKpiResults(preTotalNumOfTxnPast7Days, postTotalNumOfTxnPast7Days, "Total Number Of Transactions Past 7 Days", P1_TC_245);
            validateKpiResults(preTotalNumOfCashInTxnPast7Days, postTotalNumOfCashInTxnPast7Days, "Total Number Of Cash in Transactions Past 7 Days", P1_TC_250);
            validateKpiResults(preTotalNumOfSuccessCashInTxnPast7Days, postTotalNumOfSuccessCashInTxnPast7Days, "Total Number Of Successful Cash in Transactions Past 7 Days", P1_TC_251);

            validateKpiResults(preAmountValue, postAmountValue, "Total Amount Of Transactions", P1_TC_211);
            validateKpiResults(preTotalNumOfTransactionValue, postTotalNumOfTransactionValue, "Total Number Of Transactions", P1_TC_212);
            validateKpiResults(preTotalCashInValue, postTotalCashInValue, "Total number of Cash In Transactions", P1_TC_217);
            validateKpiResults(preTotalSuccessCashInValue, postTotalSuccessCashInValue, "Total number of success Cash In transactions", P1_TC_218);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_211);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.P1CORE})
    public void Test_05() throws Exception {
        ExtentTest P1_TC_220 = pNode.createNode("P1_TC_220", "Verify that after preforming one " +
                "P2P transaction the current count in KPI page for total number of P2P transaction is incremented by at least 1");

        ExtentTest P1_TC_222 = pNode.createNode("P1_TC_222", "Verify that after preforming one successful" +
                "P2P transaction the current count in KPI page for total number of successful P2P transaction is incremented by at least 1");

        ExtentTest P1_TC_253 = pNode.createNode("P1_TC_253", "Verify that after preforming one " +
                "P2P transaction the current count in KPI page for total number of P2P transaction for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_255 = pNode.createNode("P1_TC_255", "Verify that after preforming one successful" +
                "P2P transaction the current count in KPI page for total number of successful P2P transaction is incremented by at least 1");
        try {
            double preTotalp2pTxnPast7Days = kpiDateRangeRecord("total.number.of.p2p.transactions.id", P1_TC_253);

            double preTotalSuccessp2pTxnPast7Days = kpiDateRangeRecord("total.number.of.success.p2p.transactions", P1_TC_255);

            double preTotalp2pTransaction = kpiCurrentDateRecords("total.number.of.p2p.transactions.id", P1_TC_220);

            double preTotalp2pSuccessTransaction = kpiCurrentDateRecords("total.number.of.success.p2p.transactions", P1_TC_222);

            User unRegSubs = new User(Constants.SUBSCRIBER);

            txnAmnt = new BigDecimal("1");

            Transactions.init(P1_TC_220)
                    .p2pNonRegTransaction(sub, unRegSubs, txnAmnt);

            double postTotalp2pTxnPast7Days = kpiDateRangeRecord("total.number.of.p2p.transactions.id", P1_TC_253);

            double postTotalSuccessp2pTxnPast7Days = kpiDateRangeRecord("total.number.of.success.p2p.transactions", P1_TC_255);

            double postTotalp2pTransaction = kpiCurrentDateRecords("total.number.of.p2p.transactions.id", P1_TC_220);

            double postTotalp2pSuccessTransaction = kpiCurrentDateRecords("total.number.of.success.p2p.transactions", P1_TC_222);

            validateKpiResults(preTotalp2pTxnPast7Days, postTotalp2pTxnPast7Days, "Total Number Of P2P Transactions in past 7 days", P1_TC_253);
            validateKpiResults(preTotalSuccessp2pTxnPast7Days, postTotalSuccessp2pTxnPast7Days, "Total Number Of success P2P Transactions in past 7 days", P1_TC_255);

            validateKpiResults(preTotalp2pSuccessTransaction, postTotalp2pSuccessTransaction, "Total Number Of Successful P2P Transactions", P1_TC_222);
            validateKpiResults(preTotalp2pTransaction, postTotalp2pTransaction, "Total Number Of P2P Transactions", P1_TC_220);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_220);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.P1CORE}) //depends on test_01
    public void Test_06() throws Exception {
        ExtentTest P1_TC_209 = pNode.createNode("P1_TC_209", "Verify that after deleting a subscriber " +
                "the current count in KPI page for 'Total Number of Deleted Subscriber' is incremented by at least 1");

        ExtentTest P1_TC_242 = pNode.createNode("P1_TC_242", "Verify that after deleting a subscriber " +
                "the current count in KPI page for 'Total Number of Deleted Subscriber' for past 7 days is incremented by at least 1");
        try {
            double preTotalNumOfDeletedSubsPast7Days = kpiDateRangeRecord("total.number.of.deleted.subs.id", P1_TC_242);

            double preValue = kpiCurrentDateRecords("total.number.of.deleted.subs.id", P1_TC_209);

            Transactions.init(P1_TC_209)
                    .initiateCashIn(newSub, wholesaler, "5");

            Login.init(P1_TC_209).login(wholesaler);

            ConfigInput.isAssert = false;
            SubscriberManagement.init(P1_TC_209)
                    .deleteSubscriber(newSub, providerID);

            Assertion.verifyActionMessageContain("account.close.success", "Account Closure success", P1_TC_209);

            double postTotalNumOfDeletedSubsPast7Days = kpiDateRangeRecord("total.number.of.deleted.subs.id", P1_TC_242);

            double postValue = kpiCurrentDateRecords("total.number.of.deleted.subs.id", P1_TC_209);

            validateKpiResults(preTotalNumOfDeletedSubsPast7Days, postTotalNumOfDeletedSubsPast7Days, "Total Number Of Deleted Subscribers for past 7 days", P1_TC_242);
            validateKpiResults(preValue, postValue, "Total Number Of Deleted Subscribers", P1_TC_209);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_209);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 7, groups = {FunctionalTag.P1CORE}) //depends on Test_03
    public void Test_07() throws Exception {

        ExtentTest P1_TC_210 = pNode.createNode("P1_TC_210", "Verify that after deleting a merchant " +
                "the current count in KPI page for 'Total Number of Deleted merchant' is incremented by at least 1");

        ExtentTest P1_TC_243 = pNode.createNode("P1_TC_243", "Verify that after deleting a merchant " +
                "the current count in KPI page for 'Total Number of Deleted merchant' for past 7 days is incremented by at least 1");
        try {
            double preTotalNumberOfDeletedMerPast7Days = kpiDateRangeRecord("total.number.of.deleted.merchant.id", P1_TC_243);
            double preValue = kpiCurrentDateRecords("total.number.of.deleted.merchant.id", P1_TC_210);

            ChannelUserManagement.init(P1_TC_210)
                    .deleteChannelUser(merchant);

            double postTotalNumberOfDeletedMerPast7Days = kpiDateRangeRecord("total.number.of.deleted.merchant.id", P1_TC_243);
            double postValue = kpiCurrentDateRecords("total.number.of.deleted.merchant.id", P1_TC_210);

            validateKpiResults(preTotalNumberOfDeletedMerPast7Days, postTotalNumberOfDeletedMerPast7Days, "Total Number Of Deleted Merchants for past 7 days ", P1_TC_243);
            validateKpiResults(preValue, postValue, "Total Number Of Deleted Merchants", P1_TC_210);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_210);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 8, groups = {FunctionalTag.P1CORE})
    public void Test_08() throws Exception {

        ExtentTest P1_TC_223 = pNode.createNode("P1_TC_223", "Verify that after preforming one cash out " +
                " transaction the current count in KPI page for total number of  cash out transaction is incremented by at least 1");

        ExtentTest P1_TC_225 = pNode.createNode("P1_TC_225", "Verify that after preforming one successful cash out " +
                " transaction the current count in KPI page for total number of successful cash out transaction is incremented by at least 1");

        ExtentTest P1_TC_256 = pNode.createNode("P1_TC_256", "Verify that after preforming one cash out " +
                " transaction the current count in KPI page for total number of cash out transaction for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_258 = pNode.createNode("P1_TC_258", "Verify that after preforming one successful cash out " +
                " transaction the current count in KPI page for total number of successful cash out transaction for past 7 days is incremented by at least 1");

        double preTotalNumOfCashOutTxnPast7days = kpiDateRangeRecord("total.number.of.cashout.transactions", P1_TC_223);

        double preTotalNumOfSuccessCashoutTxnPast7Days = kpiDateRangeRecord("total.number.of.successful.cashout.transactions", P1_TC_258);

        double preTotalNumOfCashOutTransactions = kpiCurrentDateRecords("total.number.of.cashout.transactions", P1_TC_256);

        double preTotalSuccessCashOutTransactions = kpiCurrentDateRecords("total.number.of.successful.cashout.transactions", P1_TC_225);

        txnAmnt = new BigDecimal("1");
        try {
            String serviceID = Transactions.init(P1_TC_223)
                    .performCashOut(sub, wholesaler, txnAmnt).ServiceRequestId;

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            Transactions.init(P1_TC_223)
                    .cashoutApprovalBysubs(sub, serviceID);
        } finally {

            double postTotalNumOfCashOutTxnPast7days = kpiDateRangeRecord("total.number.of.cashout.transactions", P1_TC_256);

            double postTotalNumOfSuccessCashoutTxnPast7Days = kpiDateRangeRecord("total.number.of.successful.cashout.transactions", P1_TC_258);

            double postTotalCashOutTransactions = kpiCurrentDateRecords("total.number.of.cashout.transactions", P1_TC_223);

            double postTotalSuccessCashOutTransactions = kpiCurrentDateRecords("total.number.of.successful.cashout.transactions", P1_TC_225);

            validateKpiResults(preTotalNumOfSuccessCashoutTxnPast7Days, postTotalNumOfSuccessCashoutTxnPast7Days, "Total Number Of Successful Cash out Transactions", P1_TC_258);

            validateKpiResults(preTotalNumOfCashOutTxnPast7days, postTotalNumOfCashOutTxnPast7days, "Total Number Of Cash out Transactions", P1_TC_256);

            validateKpiResults(preTotalSuccessCashOutTransactions, postTotalSuccessCashOutTransactions, "Total Number Of Successful Cash out Transactions", P1_TC_225);

            validateKpiResults(preTotalNumOfCashOutTransactions, postTotalCashOutTransactions, "Total Number Of Cash out Transactions", P1_TC_223);
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 9, groups = {FunctionalTag.P1CORE})
    public void Test_09() throws Exception {
        ExtentTest P1_TC_224 = pNode.createNode("P1_TC_224", "Verify that after preforming one failed cash out " +
                "transaction the current count in KPI page for total number of failed cash out transaction is incremented by at least 1");

        ExtentTest P1_TC_257 = pNode.createNode("P1_TC_257", "Verify that after preforming one failed cash out " +
                "transaction the current count in KPI page for total number of failed cash out transaction for past 7 days is incremented by at least 1");

        double preTotalFailedCashOutTxnPast7Days = kpiDateRangeRecord("total.number.of.failed.cashout.transactions", P1_TC_257);
        double preTotalFailedCashOutTransactions = kpiCurrentDateRecords("total.number.of.failed.cashout.transactions", P1_TC_224);

        txnAmnt = new BigDecimal("0");

        try {
            String serviceID = Transactions.init(P1_TC_224)
                    .performCashOut(sub, wholesaler, txnAmnt).ServiceRequestId;

//            startNegativeTest();
//            Transactions.init(P1_TC_224)
//                    .cashoutApprovalBysubs(sub, serviceID);
        } finally {
            double postTotalFailedCashOutTxnPast7Days = kpiDateRangeRecord("total.number.of.failed.cashout.transactions", P1_TC_257);

            double postTotalFailedCashOutTransactions = kpiCurrentDateRecords("total.number.of.failed.cashout.transactions", P1_TC_224);

            validateKpiResults(preTotalFailedCashOutTxnPast7Days, postTotalFailedCashOutTxnPast7Days, "Total Number Of Failed Cash out Transactions for past 7 days", P1_TC_257);
            validateKpiResults(preTotalFailedCashOutTransactions, postTotalFailedCashOutTransactions, "Total Number Of Failed Cash out Transactions", P1_TC_224);
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 10, groups = {FunctionalTag.P1CORE})
    public void Test_10() throws Exception {
        ExtentTest P1_TC_226 = pNode.createNode("P1_TC_226", "Verify that after preforming one balance enquiry " +
                "transaction the current count in KPI page for total number of balance enquiry transaction is incremented by at least 1");

        ExtentTest P1_TC_228 = pNode.createNode("P1_TC_228", "Verify that after preforming one successful balance enquiry " +
                "transaction the current count in KPI page for total number of balance successful enquiry transaction is incremented by at least 1");

        ExtentTest P1_TC_259 = pNode.createNode("P1_TC_259", "Verify that after preforming one balance enquiry " +
                "transaction the current count in KPI page for total number of balance enquiry transaction for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_261 = pNode.createNode("P1_TC_261", "Verify that after preforming one successful balance enquiry " +
                "transaction the current count in KPI page for total number of balance successful enquiry transaction for past 7 days is incremented by at least 1");
        try {
            ServiceChargeManagement.init(P1_TC_226)
                    .configureNonFinancialServiceCharge(balEnq);

            double preTotalNumOfBalEnqTxnPast7Days = kpiDateRangeRecord("total.number.of.balance.enquiry.transactions", P1_TC_259);

            double preTotalNumOfSuccessBalEnqTxnPast7Days = kpiDateRangeRecord("total.number.of.success.balance.enquiry.transactions", P1_TC_261);

            double preTotalNumOfBalEnq = kpiCurrentDateRecords("total.number.of.balance.enquiry.transactions", P1_TC_226);

            double preTotalNumOfSuccessBalEnq = kpiCurrentDateRecords("total.number.of.success.balance.enquiry.transactions", P1_TC_228);

            Transactions.init(P1_TC_226)
                    .subscriberBalanceEnquiry(sub);

            double postTotalNumOfBalEnqTxnPast7Days = kpiDateRangeRecord("total.number.of.balance.enquiry.transactions", P1_TC_259);

            double postTotalNumOfSuccessBalEnqTxnPast7Days = kpiDateRangeRecord("total.number.of.success.balance.enquiry.transactions", P1_TC_261);

            double postTotalNumOfBalEnq = kpiCurrentDateRecords("total.number.of.balance.enquiry.transactions", P1_TC_226);

            double postTotalNumOfSuccessBalEnq = kpiCurrentDateRecords("total.number.of.success.balance.enquiry.transactions", P1_TC_228);

            validateKpiResults(preTotalNumOfBalEnqTxnPast7Days, postTotalNumOfBalEnqTxnPast7Days, "Total Number Of Balance Enquiry", P1_TC_259);
            validateKpiResults(preTotalNumOfSuccessBalEnqTxnPast7Days, postTotalNumOfSuccessBalEnqTxnPast7Days, "Total Number Of Balance Enquiry", P1_TC_261);

            validateKpiResults(preTotalNumOfBalEnq, postTotalNumOfBalEnq, "Total Number Of Balance Enquiry", P1_TC_226);
            validateKpiResults(preTotalNumOfSuccessBalEnq, postTotalNumOfSuccessBalEnq, "Total Number Of Successful Balance Enquiry", P1_TC_228);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_226);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 11, groups = {FunctionalTag.P1CORE})
    public void Test_11() throws Exception {
        ExtentTest P1_TC_227 = pNode.createNode("P1_TC_227", "Verify that after preforming one failed balance enquiry " +
                "transaction the current count in KPI page for number of balance failed enquiry transaction is incremented by at least 1");

        ExtentTest P1_TC_260 = pNode.createNode("P1_TC_260", "Verify that after preforming one failed balance enquiry " +
                "transaction the current count in KPI page for number of balance failed enquiry transaction for past 7 days is incremented by at least 1");

        double preTotalNumOfFailBalEnqPast7days = kpiDateRangeRecord("total.number.of.failed.balance.enquiry.transactions", P1_TC_260);
        double preTotalNumOfFailBalEnq = kpiCurrentDateRecords("total.number.of.failed.balance.enquiry.transactions", P1_TC_227);

        ServiceChargeManagement.init(P1_TC_227).configureNonFinancialServiceCharge(balEnq)
                .deleteNFSChargeAllVersions(balEnq);

        try {
            Transactions.init(P1_TC_227)
                    .subscriberBalanceEnquiry(sub);
        } finally {

            double postTotalNumOfFailBalEnqPast7days = kpiDateRangeRecord("total.number.of.failed.balance.enquiry.transactions", P1_TC_260);

            double postTotalNumOfFailBalEnq = kpiCurrentDateRecords("total.number.of.failed.balance.enquiry.transactions", P1_TC_227);

            validateKpiResults(preTotalNumOfFailBalEnqPast7days, postTotalNumOfFailBalEnqPast7days, "Total Number Of Failed Balance Enquiry", P1_TC_260);
            validateKpiResults(preTotalNumOfFailBalEnq, postTotalNumOfFailBalEnq, "Total Number Of Failed Balance Enquiry", P1_TC_227);
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 12, groups = {FunctionalTag.P1CORE})
    public void Test_12() throws Exception {

        ExtentTest P1_TC_229 = pNode.createNode("P1_TC_229", "Verify that after preforming last 5 transactions " +
                "transaction the current count in KPI page for number of last 5 transactions is incremented by at least 1");

        ExtentTest P1_TC_231 = pNode.createNode("P1_TC_231", "Verify that after one preforming one successful last 5 transactions " +
                "transaction the current count in KPI page for Total number of successful last 5 transactions is incremented by at least 1");

        ExtentTest P1_TC_262 = pNode.createNode("P1_TC_262", "Verify that after preforming last 5 transactions " +
                "transaction the current count in KPI page for number of last 5 transactions for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_264 = pNode.createNode("P1_TC_264", "Verify that after one preforming one successful last 5 transactions " +
                "transaction the current count in KPI page for Total number of successful last 5 transactions for past 7 days is incremented by at least 1");
        try {
            double preTotalNumOfLast5TxnPast7Days = kpiDateRangeRecord("total.number.of.last5.transactions", P1_TC_262);

            double preTotalNumOfSuccessLast5TxnPast7Days = kpiDateRangeRecord("total.number.of.successful.last5.transactions", P1_TC_264);

            double preTotalLast5Txn = kpiCurrentDateRecords("total.number.of.last5.transactions", P1_TC_229);

            double preTotalSuccessLast5Txn = kpiCurrentDateRecords("total.number.of.successful.last5.transactions", P1_TC_231);

            Transactions.init(P1_TC_229).Last_n_Transaction(sub.MSISDN);

            double postTotalNumOfLast5TxnPast7Days = kpiDateRangeRecord("total.number.of.last5.transactions", P1_TC_262);

            double postTotalNumOfSuccessLast5TxnPast7Days = kpiDateRangeRecord("total.number.of.successful.last5.transactions", P1_TC_264);

            double postTotalLast5Txn = kpiCurrentDateRecords("total.number.of.last5.transactions", P1_TC_229);

            double postTotalSuccessLast5Txn = kpiCurrentDateRecords("total.number.of.successful.last5.transactions", P1_TC_231);

            validateKpiResults(preTotalNumOfLast5TxnPast7Days, postTotalNumOfLast5TxnPast7Days, "Total Number Of Last 5 Transactions past 7 days", P1_TC_262);
            validateKpiResults(preTotalNumOfSuccessLast5TxnPast7Days, postTotalNumOfSuccessLast5TxnPast7Days, "Total Number Of Last 5 Transactions past 7 days", P1_TC_264);

            validateKpiResults(preTotalLast5Txn, postTotalLast5Txn, "Total Number Of Last 5 Transactions", P1_TC_229);
            validateKpiResults(preTotalSuccessLast5Txn, postTotalSuccessLast5Txn, "Total Number Of Last 5 Transactions", P1_TC_231);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_229);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 13, groups = {FunctionalTag.P1CORE})
    public void Test_13() throws Exception {
        ExtentTest P1_TC_330 = pNode.createNode("P1_TC_330", "Verify that after one preforming one failed last 5 transactions " +
                "transaction the current count in KPI page for Total number of failed last 5 transactions is incremented by at least 1");

        ExtentTest P1_TC_263 = pNode.createNode("P1_TC_263", "Verify that after one preforming one failed last 5 transactions " +
                "transaction the current count in KPI page for Total number of failed last 5 transactions for past 7 days is incremented by at least 1");

        double preTotalFailedLast5TxnPast7Days = kpiDateRangeRecord("total.number.of.failed.last5.transactions", P1_TC_263);
        double preTotalLast5Txn = kpiCurrentDateRecords("total.number.of.failed.last5.transactions", P1_TC_330);

        ServiceChargeManagement.init(P1_TC_330).configureNonFinancialServiceCharge(last5txn)
                .deleteNFSChargeAllVersions(last5txn);

        try {
            Transactions.init(P1_TC_330).Last_n_Transaction(sub.MSISDN);
        } finally {
            double postTotalFailedLast5TxnPast7Days = kpiDateRangeRecord("total.number.of.failed.last5.transactions", P1_TC_263);

            double postTotalLast5Txn = kpiCurrentDateRecords("total.number.of.failed.last5.transactions", P1_TC_330);

            validateKpiResults(preTotalFailedLast5TxnPast7Days, postTotalFailedLast5TxnPast7Days, "Total Number of Failed last 5 transactions", P1_TC_263);
            validateKpiResults(preTotalLast5Txn, postTotalLast5Txn, "Total Number of Failed last 5 transactions", P1_TC_330);
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 14, groups = {FunctionalTag.P1CORE})
    public void Test_14() throws Exception {
        ExtentTest P1_TC_214 = pNode.createNode("P1_TC_214", "Verify that after one preforming one failed top up " +
                "transaction the current count in KPI page for Total number of failed top up transactions is incremented by at least 1");

        ExtentTest P1_TC_247 = pNode.createNode("P1_TC_247", "Verify that after one preforming one failed top up " +
                "transaction the current count in KPI page for Total number of failed top up transactions for past 7 days is incremented by at least 1");

        double preFailedTopUpRecord = kpiCurrentDateRecords("total.number.of.failed.top.up.transactions.id", P1_TC_214);
        double preFailedTopUpRecordPast7Days = kpiDateRangeRecord("total.number.of.failed.top.up.transactions.id", P1_TC_247);

        try {
            startNegativeTest();
            Transactions.init(P1_TC_214)
                    .subscriberSelfRecharge(sub, "0", operatorId);
        } finally {
            double postFailedTopUpRecord = kpiCurrentDateRecords("total.number.of.failed.top.up.transactions.id", P1_TC_214);
            double postFailedTopUpRecordPast7Days = kpiDateRangeRecord("total.number.of.failed.top.up.transactions.id", P1_TC_247);

            validateKpiResults(preFailedTopUpRecord, postFailedTopUpRecord, "Total Number Of Failed Topup Transaction", P1_TC_214);
            validateKpiResults(preFailedTopUpRecordPast7Days, postFailedTopUpRecordPast7Days, "Total Number Of Failed Topup Transaction for past 7 days", P1_TC_247);
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 15, groups = {FunctionalTag.P1CORE})
    public void Test_15() throws Exception {

        ExtentTest P1_TC_213 = pNode.createNode("P1_TC_213", "To verify that after performing a top up transaction" +
                "the current count in KPI page for total number of top up transactions is incremented by at least 1");

        ExtentTest P1_TC_215 = pNode.createNode("P1_TC_215", "To verify that after performing a successful top up transaction" +
                "the current count in KPI page for total number of successful top up transactions is incremented by at least 1");

        ExtentTest P1_TC_246 = pNode.createNode("P1_TC_246", "To verify that after performing a top up transaction" +
                "the current count in KPI page for total number of top up transactions for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_248 = pNode.createNode("P1_TC_248", "To verify that after performing a successful top up transaction" +
                "the current count in KPI page for total number of successful top up transactions for past 7 days is incremented by at least 1");

        double preTotalNumOfTopUpTxn = kpiCurrentDateRecords("total.number.of.topup.transactions.id", P1_TC_213);
        double preTotalNumOfSuccessTopUpTxn = kpiCurrentDateRecords("total.number.of.successful.top.up.transactions.id", P1_TC_215);

        double preTotalNumOfTopUpTxnPast7Days = kpiDateRangeRecord("total.number.of.topup.transactions.id", P1_TC_246);
        double preTotalNumOfSuccessTopUpTxnPast7Days = kpiDateRangeRecord("total.number.of.successful.top.up.transactions.id", P1_TC_248);
        try {
            String serviceReqId = Transactions.init(P1_TC_213)
                    .subscriberSelfRecharge(sub, "1", operatorId).ServiceRequestId;

            Transactions.init(P1_TC_213)
                    .resumeAmbiguousTransaction(Services.SELF_RECHARGE_RESUME, serviceReqId, "true");
        } finally {
            double postTotalNumOfTopUpTxn = kpiCurrentDateRecords("total.number.of.topup.transactions.id", P1_TC_213);
            double postTotalNumOfSuccessTopUpTxn = kpiCurrentDateRecords("total.number.of.successful.top.up.transactions.id", P1_TC_215);

            double postTotalNumOfTopUpTxnPast7Days = kpiDateRangeRecord("total.number.of.topup.transactions.id", P1_TC_246);
            double postTotalNumOfSuccessTopUpTxnPast7Days = kpiDateRangeRecord("total.number.of.successful.top.up.transactions.id", P1_TC_248);

            validateKpiResults(preTotalNumOfTopUpTxn, postTotalNumOfTopUpTxn, "Total Number Of Top up Transactions", P1_TC_213);
            validateKpiResults(preTotalNumOfSuccessTopUpTxn, postTotalNumOfSuccessTopUpTxn, "Total Number Of Successful Top up Transactions", P1_TC_215);

            validateKpiResults(preTotalNumOfTopUpTxnPast7Days, postTotalNumOfTopUpTxnPast7Days, "Total Number Of Top up Transactions For pst 7 Days", P1_TC_246);
            validateKpiResults(preTotalNumOfSuccessTopUpTxnPast7Days, postTotalNumOfSuccessTopUpTxnPast7Days, "Total Number Of Successful Top up Transactions For pst 7 Days", P1_TC_248);
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 16, groups = {FunctionalTag.P1CORE})
    public void Test_16() throws Exception {
        ExtentTest P1_TC_221 = pNode.createNode("P1_TC_221", "To verify that after performing a failed P2P transaction" +
                "the current count in KPI page for total number of failed P2P transactions is incremented by at least 1");

        ExtentTest P1_TC_254 = pNode.createNode("P1_TC_254", "To verify that after performing a failed P2P transaction" +
                "the current count in KPI page for total number of failed P2P transactions for past 7 days is incremented by at least 1");

        double preTotalFailP2PTxn = kpiCurrentDateRecords("total.number.of.failed.p2p.transactions.id", P1_TC_221);
        double preTotalFailP2PTxnPast7Days = kpiDateRangeRecord("total.number.of.failed.p2p.transactions.id", P1_TC_254);

        User receiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        try {
            startNegativeTest();
            Transactions.init(P1_TC_221)
                    .performP2PTransfer(sub, receiver, "0");
        } finally {
            double postTotalFailP2PTxn = kpiCurrentDateRecords("total.number.of.failed.p2p.transactions.id", P1_TC_221);
            double postTotalFailP2PTxnPast7Days = kpiDateRangeRecord("total.number.of.failed.p2p.transactions.id", P1_TC_254);

            validateKpiResults(preTotalFailP2PTxn, postTotalFailP2PTxn, "Total Number Of Failed P2P Transactions", P1_TC_221);
            validateKpiResults(preTotalFailP2PTxnPast7Days, postTotalFailP2PTxnPast7Days, "Total Number Of Failed P2P Transactions Past 7 Days", P1_TC_254);
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 17, groups = {FunctionalTag.P1CORE})
    public void Test_17() throws Exception {
        ExtentTest P1_TC_235 = pNode.createNode("P1_TC_235", "To verify that after performing a service in which commission amount is defined" +
                "the current count in KPI page for Total number of Commission Transfer Transactions is incremented by at least 1");

        ExtentTest P1_TC_237 = pNode.createNode("P1_TC_237", "To verify that after performing a successful service in which commission amount is defined" +
                "the current count in KPI page for Total number of successful Commission Transfer Transactions is incremented by at least 1");

        ExtentTest P1_TC_267 = pNode.createNode("P1_TC_267", "To verify that after performing a service in which commission amount is defined" +
                "the current count in KPI page for Total number of Commission Transfer Transactions for past 7 days is incremented by at least 1");

        ExtentTest P1_TC_269 = pNode.createNode("P1_TC_269", "To verify that after performing a successful service in which commission amount is defined" +
                "the current count in KPI page for Total number of successful Commission Transfer Transactions is incremented by at least 1");
        try {
            double preTotalCommTxn = kpiCurrentDateRecords("total.number.of.Commission.Transfer.Transactions", P1_TC_235);
            double preTotalSuccessCommTxn = kpiCurrentDateRecords("total.number.of.success.Commission.Transfer.Transactions", P1_TC_237);

            double preTotalCommTxnPast7Days = kpiDateRangeRecord("total.number.of.Commission.Transfer.Transactions", P1_TC_267);
            double preTotalSuccessCommTxnPast7Days = kpiDateRangeRecord("total.number.of.success.Commission.Transfer.Transactions", P1_TC_269);

            User newSub = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(P1_TC_235)
                    .createDefaultSubscriberUsingAPI(newSub);

            Transactions.init(P1_TC_235)
                    .initiateCashIn(newSub, wholesaler, "5");

            Login.init(P1_TC_235).login(wholesaler);

            startNegativeTest();
            SubscriberManagement.init(P1_TC_235)
                    .deleteSubscriber(newSub, providerID);

            Assertion.verifyActionMessageContain("account.close.success", "Account Closure success", P1_TC_235);

            double postTotalCommTxn = kpiCurrentDateRecords("total.number.of.Commission.Transfer.Transactions", P1_TC_235);
            double postTotalSuccessCommTxn = kpiCurrentDateRecords("total.number.of.success.Commission.Transfer.Transactions", P1_TC_237);

            double postTotalCommTxnPast7Days = kpiDateRangeRecord("total.number.of.Commission.Transfer.Transactions", P1_TC_267);
            double postTotalSuccessCommTxnPast7Days = kpiDateRangeRecord("total.number.of.success.Commission.Transfer.Transactions", P1_TC_269);

            validateKpiResults(preTotalCommTxn, postTotalCommTxn, "Total Number Of Commission Transfer Transactions", P1_TC_235);
            validateKpiResults(preTotalSuccessCommTxn, postTotalSuccessCommTxn, "Total Number Of Successful Commission Transfer Transactions", P1_TC_237);
            validateKpiResults(preTotalCommTxnPast7Days, postTotalCommTxnPast7Days, "Total Number Of Commission Transfer Transactions Past 7 Days", P1_TC_267);
            validateKpiResults(preTotalSuccessCommTxnPast7Days, postTotalSuccessCommTxnPast7Days, "Total Number Of Successful Commission Transfer Transactions Past 7 Days", P1_TC_269);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_235);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tTeardown = pNode.createNode("Teardown", "After Class Methods for Current Suite");
        try {
            ServiceChargeManagement.init(tTeardown)
                    .configureNonFinancialServiceCharge(balEnq);

            ServiceChargeManagement.init(tTeardown)
                    .configureNonFinancialServiceCharge(last5txn);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
