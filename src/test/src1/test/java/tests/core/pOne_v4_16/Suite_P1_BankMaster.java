package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.bankMaster.AddServiceProviderBankAccounts1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Bank Master Functionality Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/19/2018.
 */
public class Suite_P1_BankMaster extends TestInit {
    private CurrencyProvider defaultProvider;

    @BeforeClass(alwaysRun = true)
    public void beforeRun() {
        defaultProvider = DataFactory.getDefaultProvider();
    }

    /**
     * Duplicate
     *
     * @throws Exception
     */
    @Test(priority = 1, enabled = false, groups = {FunctionalTag.P1CORE, FunctionalTag.BANK_MASTER})
    public void Test_01() throws Exception {
        /*
        login as super admin maker
        Navigate to Add service provider to bank accounts under bank master
        select a bank and add account number
        validate success message
        navigate to modify service provider to bank accounts under bank master
        modify the details
        validate success message
         */

        ExtentTest TC124 = pNode.createNode("TC124", "To verify that superadmin can 'Add Bank Accounts' under selected bank.");
        try {
            String accNum = RandomStringUtils.randomNumeric(5);


            Login.init(TC124).loginAsSuperAdmin("OPT_BANK_ACC_ADD");
            CurrencyProviderMapping.init(TC124)
                    .addServiceProviderBankAccount(defaultProvider.Bank.BankName, accNum);

        } catch (Exception e) {
            markTestAsFailure(e, TC124);
        }

        ExtentTest TC121 = pNode.createNode("TC121", "To verify that superadmin can 'Modify Bank Accounts' under selected bank.");
        try {
            String accNum = RandomStringUtils.randomNumeric(5);


            Login.init(TC124).loginAsSuperAdmin("OPT_BANK_ACC_ADD");
            CurrencyProviderMapping.init(TC124)
                    .addServiceProviderBankAccount(defaultProvider.Bank.BankName, accNum);


            Login.init(TC124).loginAsSuperAdmin("OPT_BANK_ACC_MOD");
            CurrencyProviderMapping.init(TC121)
                    .modifyServiceProviderBankAccount(defaultProvider.Bank.BankName, accNum);
        } catch (Exception e) {
            markTestAsFailure(e, TC121);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Duplicate
     *
     * @throws Exception
     */
    @Test(priority = 2, enabled = false, groups = {FunctionalTag.P1CORE, FunctionalTag.BANK_MASTER})
    public void Test_02() throws Exception {
        ExtentTest TC123 = pNode.createNode("TC123", "To verify that superadmin can add more account options when clicked on 'Add More' under selected bank.");
        try {
            /*
            Login as super admin maker
            Navigate to Add service provider to bank accounts under bank master
            click on add more
            verify new field to enter new bank account number is displayed
             */
            Login.init(TC123).loginAsSuperAdmin("OPT_BANK_ACC_ADD");

            defaultProvider = DataFactory.getDefaultProvider();
            AddServiceProviderBankAccounts1 page = new AddServiceProviderBankAccounts1(TC123);

            page.navAddBank();
            page.setBankName(defaultProvider.Bank.BankName);
            page.clickOnBankSubmit();
            page.clickOnAddMore();
            Thread.sleep(2000);
            boolean isFieldToEnterNewBankDetailsExist = page.isFieldToEnterNewBankDetails();

            Assertion.verifyEqual(isFieldToEnterNewBankDetailsExist, true,
                    "Field to add a new bank account is available after clicking add more button ", TC123);
        } catch (Exception e) {
            markTestAsFailure(e, TC123);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.BANK_MASTER, FunctionalTag.CRITICAL_CASES_TAG})
    public void Test_03() throws Exception {
        /*
        login as super admin checker
        set is pool acc num required to NO
        add a bank with out pool account number
        validate success message
        delete the newly created bank
        validate success message
        set is pool acc num required back to Yes
         */
        ExtentTest extent03 = pNode.createNode("P1_TC_445_P1_TC_446_P1_TC_449", "To verify that the Super admin can add same bank (without Pool account number) with all the details which was deleted from system.");
        SystemPreferenceManagement.init(extent03)
                .updateSystemPreference("IS_POOL_ACC_REQ", "N");
        try {
            SuperAdmin sc = DataFactory.getSuperAdminWithAccess("BANK_ADD");
            Login.init(extent03).loginAsSuperAdmin(sc);

            Bank bank = new Bank(defaultProvider.ProviderId,
                    "TMPBK" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    true,
                    false);
            CurrencyProviderMapping.init(extent03)
                    .addBank(bank);

            CurrencyProviderMapping.init(extent03).deleteBank(bank.BankName);

        } catch (Exception e) {
            markTestAsFailure(e, extent03);
        } finally {
            SystemPreferenceManagement.init(extent03)
                    .updateSystemPreference("IS_POOL_ACC_REQ", "Y");
        }


    }

    /**
     * Duplicate
     *
     * @throws Exception
     */
    @Test(priority = 4, enabled = false)
    public void Test_04() throws Exception {
        /*
        login as super admin checker
        add a bank with invalid pool account number
        validate error message.
         */
        ExtentTest TC120 = pNode.createNode("TC120", "To verify that Super admin can not add the Bank if Pool account number is invalid");

        try {
            SuperAdmin sc = DataFactory.getSuperAdminWithAccess("BANK_ADD");
            Login.init(TC120).loginAsSuperAdmin(sc);
            Bank bank = new Bank(defaultProvider.ProviderId,
                    "TMPBK" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    true,
                    false);

            bank.setPoolAccNum("abcd");
            ConfigInput.isAssert = false;
            startNegativeTest();
            CurrencyProviderMapping.init(TC120)
                    .addBank(bank);

            Assertion.verifyErrorMessageContain("invalid.pool.account.number", "invaid pool account number", TC120);
        } catch (Exception e) {
            markTestAsFailure(e, TC120);
        }
        Assertion.finalizeSoftAsserts();
    }
}
