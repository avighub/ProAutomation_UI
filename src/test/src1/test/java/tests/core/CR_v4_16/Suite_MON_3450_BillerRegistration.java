package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.dataEntity.Wallet;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * "MON-3450: Biller Registration, Add Approve Validation Module, Subscriber Association Module (Bulk, API, UI Modify and Delete), Api Call module to be written in Selenium.
 * 1. Subscriber biller association through web
 * 2. Subscriber biller association by retailer
 * 3. Bulk subscriber association
 * 4. Delete subscriber biller association through web
 * 5. Delete/Modify subscriber biller association by retailer"
 */

/**
 * Created by rahul.rana on 8/30/2017.
 */
public class Suite_MON_3450_BillerRegistration extends TestInit {
    private OperatorUser naUtilBillReg, naBulkBillerAssoc;
    private Biller biller;
    private User subscriber, retailer, retailer2, wholesaler;
    private Wallet defaultWallet;

    /**
     * Test             :       SETUP
     * <p>
     * Description      :       Setup Specific to this Suite!
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest tSetup = pNode.createNode("Setup", "Setup Specific to test Biller Registration. " +
                "Make Sure that the service charge UTILITY_REGISTRATION, RETAILER_BILLPAY & DELETE_ASSOCIATION are configured.");

        try {
            // Variables
            defaultWallet = DataFactory.getDefaultWallet();
            naUtilBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");
            naBulkBillerAssoc = DataFactory.getOperatorUserWithAccess("UTL_BLKBILASSOC");

            subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            retailer = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            retailer2 = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            OperatorUser operator = new OperatorUser(Constants.OPERATOR); // for creating NFSC
            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            // make sure that no bill are associated with the Biller Object
            biller = BillerManagement.init(tSetup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.removeAllBills();

            // Configure Service Charge for UTILITY_REGISTRATION
            ServiceCharge sCharge = new ServiceCharge(Services.UTILITY_REGISTRATION, subscriber, operator, null, null, null, null);
            ServiceChargeManagement.init(tSetup)
                    .configureNonFinancialServiceCharge(sCharge);

            // Configure Non Financial Service Charge for RETAILER_BILLPAY
            ServiceCharge sCharge1 = new ServiceCharge(Services.RETAILER_BILLPAY, subscriber, operator, null, null, null, null);
            ServiceChargeManagement.init(tSetup)
                    .configureNonFinancialServiceCharge(sCharge1);

            // Configure Non Financial Service Charge for DELETE_SUBSCRIBER_BILLER_ASSOCIATION
            ServiceCharge delSubsReg = new ServiceCharge(Services.DELETE_ASSOCIATION, subscriber, naUtilBillReg, null, null, null, null);
            ServiceChargeManagement.init(tSetup).configureNonFinancialServiceCharge(delSubsReg);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test             :       TEST 01
     * <p>
     * Description      :       WEB: Perform and Validate Subscriber Biller Association
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-3450-01",
                "WEB : Perform and Validate Subscriber Biller Association");

        try {
            // get a random Bill Account Number
            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            // add above bill to the Biller object
            biller.addBillForCustomer(subscriber.MSISDN, billAccNumber);

            // Login as Operator user with Bill Registration Role
            Login.init(t1)
                    .login(naUtilBillReg);

            // Initiate Subscriber Biller Association
            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test             :       TEST 02 & TEST 03 & TEST04
     * <p>
     * Description      :       API: Perform and Validate Subscriber Biller Association by Retailer
     * API: Subscriber Biller Association by Retailer - Approval
     * API: Verify Already associated Bill Could not be re-processed
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_02_03_04() throws Exception {
        ExtentTest t2 = pNode.createNode("TEST 02 API", "Perform and Validate Subscriber Biller Association by Retailer");

        try {
            // get a random Bill Number and associate with the biller
            String billAccNumber = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subscriber.MSISDN, billAccNumber);
            CustomerBill newBill = biller.getNonAssociatedBill();

            // Perform Bill association by retailer
            TxnResponse response1 = Transactions.init(t2)
                    .subsBillerRegistrationByRetailer(retailer, newBill, defaultWallet.WalletId)
                    .assertStatus(Constants.TXN_SUCCESS);

            /**
             * Sub Test
             * Perform Biller Approval for the test performed TEST 02
             */
            ExtentTest t3 = pNode.createNode("TEST 03 API", "Subscriber Biller Association by Retailer - Approval");

            Transactions.init(t3)
                    .subsBillerAssociationApproval(subscriber.MSISDN, response1.TxnId)
                    .assertStatus(Constants.TXN_SUCCESS);

            // as the bill is successfully Approve, set the bill's status as isAlreadyAssociated
            newBill.setIsAssociated();


            /**
             * Sub Test
             * Verify that Already associated and approved bill can't be processed
             */
            ExtentTest t4 = pNode.createNode("TEST 04 API", "Verify Already associated Bill Could not be re-processed");

            Transactions.init(t4)
                    .subsBillerRegistrationByRetailer(retailer, newBill, defaultWallet.WalletId)
                    .assertStatus("00597")
                    .assertMessage("txn.subscriber.already.registered.with.company");
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test             :       TEST 06
     * <p>
     * Description      :       Delete Biller Association
     *
     * @throws Exception
     */
    @Test(priority = 2, dependsOnMethods = {"Test_02_03_04"})
    public void Test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("TEST 05", "Delete Biller Association");

        try {
            CustomerBill bill = biller.getAssociatedBill();
            BillerManagement.init(t5)
                    .deleteSubsBillerAssociation(bill);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test             :       TEST 05
     * <p>
     * Description      :       UI: Bulk Subscriber Association, Associate multiple bills and perfomm Bulk association.
     * Verify the Upload logs for success
     * Verify if Already registered, re- registration is not possible
     *
     * @throws Exception
     */
    @Test(priority = 4)
    public void Test_06() throws Exception {
        ExtentTest t6 = pNode.createNode("TEST 06", "Bulk Subscriber Association, " +
                "Associate multiple bills and perform Bulk association. Verify the Upload logs for success");

        try {
            String billAcc_01 = DataFactory.getRandomNumberAsString(5);
            String billAcc_02 = DataFactory.getRandomNumberAsString(6);

            // Add Customer Bills
            biller.addBillForCustomer(subscriber.MSISDN, billAcc_01);
            biller.addBillForCustomer(subscriber.MSISDN, billAcc_02);


            Login.init(t6).login(naBulkBillerAssoc);
            BillerManagement.init(t6)
                    .initiateBulkBillerRegistration(biller);

            BillerManagement.init(t6)
                    .downloadBulkRegistrationLogFile();

            /**
             * For all the Bill associated with the Biller Object
             * Validate biller registration
             * If bills from previous test are also considered then registration must fails with message
             * However, there must be successful registration for billAcc_01 &  billAcc_02
             */
            for (CustomerBill bill : biller.BillList) {
                String actual = BillerManagement.getLogEntry(bill.BillAccNum);
                String expected = null;
                if (bill.isAlreadyAssociated) {
                    // verify that billAccount could not be Registered
                    expected = MessageReader.getMessage("subscriber.already.registered.with.company.ui", null);
                } else {
                    // verify for success message
                    expected = MessageReader.getDynamicMessage("bulk.biller.association.success.message", bill.CustomerMsisdn);
                }
                Assertion.verifyContains(actual, expected, "Verify Log", t6);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test             :       TEST 07
     * <p>
     * Description      :       API: Delete Subscriber Biller Registration
     *
     * @throws Exception
     */
    @Test(priority = 5)
    public void Test_07() throws Exception {
        ExtentTest t7 = pNode.createNode("TEST 07 API", "Delete Subscriber Biller Registration by Retailer");

        try {
            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t7).createDefaultSubscriberUsingAPI(sub);

            Transactions.init(t7).initiateCashIn(sub, wholesaler, new BigDecimal("10"));
            /**
             * Make sure a bill is already registered
             */
            String billAccNumber = DataFactory.getRandomNumberAsString(5);
        /*biller.addBillForCustomer(sub.MSISDN, billAccNumber);
        CustomerBill newBill = biller.getNonAssociatedBill();*/

            CustomerBill newBill = new CustomerBill(biller, sub.MSISDN, billAccNumber, "Test Bill");

            // Perform Bill association by retailer
            TxnResponse response = Transactions.init(t7)
                    .subsBillerRegistrationByRetailer(retailer2, newBill, defaultWallet.WalletId)
                    .verifyStatus(Constants.TXN_SUCCESS);

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            //Approve subscriber Biller association
            Transactions.init(t7).subsBillerAssociationApproval(newBill.CustomerMsisdn, response.TxnId);

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            // Delete Subscriber Biller registration
            Transactions.init(t7)
                    .delBillerRegistrationByRetailer(retailer, newBill, defaultWallet.WalletId)
                    .assertStatus(Constants.TXN_SUCCESS);
        } catch (Exception e) {
            markTestAsFailure(e, t7);
        }
        Assertion.finalizeSoftAsserts();
    }


}
