package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.Wallet;
import framework.entity.SavingsClub;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.Preferences;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suit_MON_7906_SingleStepCreation extends TestInit {

    private String bankId;
    private User subsChairman, member1;
    private SavingsClub sClub;
    private CurrencyProvider providerOne;
    private Wallet walletOne;

    @BeforeClass
    public void setup() {
        ExtentTest s1 = pNode.createNode("Setup", "Setep specific for this test");
        try {
            bankId = DataFactory.getBankId(GlobalData.defaultBankName);
            providerOne = GlobalData.defaultProvider;
            walletOne = GlobalData.defaultWallet;
            subsChairman = new User(Constants.SUBSCRIBER);
            member1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            // Map SVC wallet Preference
            Preferences.init(s1)
                    .setSVCPreferences(subsChairman, bankId);

            SubscriberManagement.init(s1)
                    .createSubscriberDefaultMapping(subsChairman, true, true);

            // Savings Club object, with default Member count and Approved Count : 1, 1
            sClub = new SavingsClub(subsChairman, Constants.CLUB_TYPE_PREMIUM, bankId, false, false);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, enabled = true)
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test 01", "Verify that Created Saving Club by subscriber should be created with single step.")
                .assignCategory(FunctionalTag.SAVING_CLUB);
        try {
            // crete savings club using API
            Transactions.init(t1)
                    .createSavingClubBySubscriberWithSingleStep(sClub, providerOne.ProviderId, walletOne.WalletId);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}