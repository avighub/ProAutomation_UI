package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page2;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Gurudatta Praharaj on 12/4/2017.
 */
public class Suite_MON_3360_Single_ID_Multiple_Msisdn extends TestInit {

    private User subscriber1, subscriber2,
            existingSubscriber1, existingSubscriber2, channelUser;

    private OperatorUser optUsr;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("FREQ_EXT_CODE_N_SUBS", "1");

           /* SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "N");*/

            existingSubscriber1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            existingSubscriber2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");

            TransactionManagement.init(setup)
                    .makeSureChannelUserHasBalance(channelUser);

//            CurrencyProviderMapping.init(setup)
//                    .mapPrimaryWalletPreference(existingSubscriber1);

            ServiceCharge acqFee = new ServiceCharge(Services.ACQFEE, existingSubscriber1,
                    optUsr, null, null, null, null);

            ServiceChargeManagement.init(setup)
                    .configureServiceCharge(acqFee);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @BeforeMethod(alwaysRun = true)
    public void PreTest() throws Exception {
        Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

        subscriber1 = new User(Constants.SUBSCRIBER);
        subscriber2 = new User(Constants.SUBSCRIBER);
    }

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest test = pNode.createNode("Test_01", "Verify that N no of subscribers can be associated " +
                "with the same identification number through Subscriber USSD Self Registration ");
        try {
            Transactions.init(test)
                    .selfRegistrationForSubscriberWithKinDetails(subscriber1);

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            subscriber2.setExternalCode(subscriber1.ExternalCode);

            startNegativeTest();
            Transactions.init(test)
                    .selfRegistrationForSubscriberWithKinDetails(subscriber2)
                    .verifyStatus("90004").verifyMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("Test_02", "Verify that N no of subscribers can be associated " +
                "with the same identification number through Subscriber Registration by channel user through USSD ");
        try {
            Transactions.init(test)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subscriber1);

            Transactions.init(test)
                    .subscriberAcquisition(subscriber1);

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            subscriber2.setExternalCode(subscriber1.ExternalCode);

            startNegativeTest();
            Transactions.init(test)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subscriber2)
                    .verifyStatus("90004").verifyMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("Test_03", "Verify that N no of subscribers can be associated " +
                "with the same identification number through Customer Registration from AS400");

        try {
            Transactions.init(test)
                    .selfRegistrationForSubscriberWithKinDetails(subscriber1);

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            subscriber2.setExternalCode(subscriber1.ExternalCode);

            startNegativeTest();
            Transactions.init(test).subRegistrationByAS400(subscriber2)
                    .verifyStatus("90004").verifyMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4)
    public void Test_04() {
        ExtentTest test = pNode.createNode("Test_04", "Verify that N no of subscribers can be associated " +
                "with the same identification number through subscriber registration by channel user through web");
        try {
            Login.init(test).login(channelUser);
            subscriber1.setExternalCode(existingSubscriber1.ExternalCode);

            startNegativeTest();
            SubscriberManagement.init(test)
                    .addInitiateSubscriber(subscriber1);

            Assertion.verifyErrorMessageContain("subscriber.same.identification.number",
                    "Duplicate Identification Number", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void Test_05() {
        ExtentTest test = pNode.createNode("Test_05", "Verify that N no of subscribers can be associated " +
                "with the same identification number through bulk subscriber registration");
        try {

            Login.init(test).login(optUsr);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(test);

            page.navBulkChUserAndSubsciberRegistrationAndModification()
                    .SelectSubscriberRegistrationService();

            subscriber2.setExternalCode(subscriber1.ExternalCode);
            SubscriberManagement.init(test)
                    .writeDataToBulkSubsRegistrationCsv(subscriber1,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            false)
                    .writeDataToBulkSubsRegistrationCsv(subscriber2,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            true);

            String batchId = page.uploadFile(FilePath.fileBulkSubReg).submitCsv()
                    .uploadFile(FilePath.fileBulkSubReg).submitCsv().getBatchId();

            page.navBulkRegistrationAndModificationMyBatchesPage()
                    .ClickOnBatchSubmit().downloadLogFileUsingBatchId(batchId)
                    .verifyErrorMessageInLogFile("bulk.external.error.msg", batchId, test, subscriber1.ExternalCode);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6)
    public void Test_06() {
        ExtentTest test = pNode.createNode("Test_06", "Verify that N no of subscribers can be modified " +
                "with the same identification number through modify subscriber by channel user through web");
        try {
            SubscriberManagement.init(test)
                    .initiateSubscriberModification(existingSubscriber2);

            ModifySubscriber_page2.init(test)
                    .externalCode_SetText(existingSubscriber1.ExternalCode).clickOnNextPg2();

            Assertion.verifyErrorMessageContain("subscriber.same.identification.number",
                    "Duplicate External Code", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7)
    public void Test_07() {
        ExtentTest test = pNode.createNode("Test_07", "Verify that system display proper error message " +
                "when (N+1) subscriber tries to register through Subscriber USSD Self Registration (CUSTREG) " +
                "in the system with the same identification number for which N no of subscribers is already present");
        try {
            subscriber1.setExternalCode(existingSubscriber1.ExternalCode);

            startNegativeTest();
            Transactions.init(test)
                    .selfRegistrationForSubscriberWithKinDetails(subscriber1)
                    .verifyStatus("90004").verifyMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8)
    public void Test_08() {
        ExtentTest test = pNode.createNode("Test_08", "Verify that system display proper error message " +
                "when (N+1) subscriber tries to register through Subscriber Registration by channel user through USSD");
        try {
            subscriber1.setExternalCode(existingSubscriber1.ExternalCode);

            startNegativeTest();
            Transactions.init(test)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subscriber1)
                    .verifyStatus("90004").verifyMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9)
    public void Test_09() {
        ExtentTest test = pNode.createNode("Test_09", "Verify that system display proper error message " +
                "when (X+1) subscriber tries to register through Customer Registration from AS400 (REGST) in the system " +
                "with the same identification number for which X no of subscribers is already present");
        try {
            subscriber1.setExternalCode(existingSubscriber1.ExternalCode);

            startNegativeTest();
            Transactions.init(test).subRegistrationByAS400(subscriber1)
                    .verifyStatus("90004").verifyMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10)
    public void Test_10() {
        ExtentTest test = pNode.createNode("Test_10", "Verify that system display proper error message " +
                "when (X+1) subscriber tries to register subscriber registration by channel user through web in the system with the same " +
                "identification number for which X no of subscribers is already present");
        try {
            Login.init(test).login(channelUser);
            subscriber1.setExternalCode(existingSubscriber1.ExternalCode);

            startNegativeTest();
            SubscriberManagement.init(test)
                    .addInitiateSubscriber(subscriber1);

            Assertion.verifyErrorMessageContain("subscriber.same.identification.number",
                    "Duplicate Identification Number", test);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11)
    public void Test_11() {
        ExtentTest test = pNode.createNode("Test_11", "Verify that system display proper error message " +
                "when (X+1) subscriber tries to register through bulk subscriber registration in the system with the same " +
                "identification number for which X no of subscribers is already present ");
        try {
            Login.init(test).login(optUsr);

            subscriber1.setExternalCode(existingSubscriber1.ExternalCode);

            SubscriberManagement.init(test)
                    .writeDataToBulkSubsRegistrationCsv(subscriber1,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            false);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(test);

            String batchId = page.navBulkChUserAndSubsciberRegistrationAndModification()
                    .SelectSubscriberRegistrationService()
                    .uploadFile(FilePath.fileBulkSubReg).submitCsv().getBatchId();

            page.navBulkRegistrationAndModificationMyBatchesPage()
                    .ClickOnBatchSubmit().downloadLogFileUsingBatchId(batchId)
                    .verifyErrorMessageInLogFile("bulk.identificationnum.error.msg", batchId, test, subscriber1.ExternalCode);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 12)
    public void Test_12() {
        ExtentTest test = pNode.createNode("Test_12", "Verify that system display proper error message " +
                "when (X+1) subscriber tries to register subscriber registration by channel user through web in the " +
                "system with the same identification number for which X no of subscribers is already present");

        try {
            Login.init(test).login(channelUser);
            SubscriberManagement.init(test)
                    .initiateSubscriberModification(existingSubscriber2);

            ModifySubscriber_page2.init(test)
                    .externalCode_SetText(existingSubscriber1.ExternalCode).clickOnNextPg2();

            Assertion.verifyErrorMessageContain("subscriber.same.identification.number", "Duplicate External Code", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 13)
    public void Test_13() {
        ExtentTest test = pNode.createNode("Test_13", "Verify that Check button in the Add Subscriber page " +
                "displays proper error message when channel user tries to register (X+1) subscriber with the same identification number " +
                "for which X no of subscribers is already present");
        try {
            Login.init(test).login(channelUser);

            subscriber1.setExternalCode(existingSubscriber1.ExternalCode);
            String actual = AddSubscriber_Page1.init(test)
                    .navAddSubscriber()
                    .setIdentificationNumber(subscriber1.ExternalCode)
                    .clickCheckIdentification()
                    .getMsgTextDuplicateIdeIdentificationNumExist();

            Assertion.verifyMessageContain(actual, "subscriber.same.identification.number",
                    "Duplicate External Code", test);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 14)
    public void Test_14() {
        ExtentTest test = pNode.createNode("Test_14", "Verify that Check button in the Modify Subscriber page " +
                "displays proper error message when channel user tries to modify (X+1) subscriber with the same identification number " +
                "for which X no of subscribers is already present");
        try {
            Login.init(test).login(channelUser);

            ModifySubscriber_page1.init(test).navSubscriberModification()
                    .setMSISDN(existingSubscriber1.MSISDN)
                    .clickOnSubmitPg1();

            String actual = AddSubscriber_Page1.init(test)
                    .setIdentificationNumber(existingSubscriber2.ExternalCode)
                    .clickCheckIdentification()
                    .getMsgTextDuplicateIdeIdentificationNumExist();

            Assertion.verifyMessageContain(actual, "subscriber.same.identification.number",
                    "Duplicate External Code", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() {
        ExtentTest teardown = pNode.createNode("TearDown", "Concluding Test");
        try {
            SystemPreferenceManagement.init(teardown)
                    .updateSystemPreference("FREQ_EXT_CODE_N_SUBS", "2");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }
}
