package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.savingsClubManagement.AddClub_Page1;
import framework.pageObjects.savingsClubManagement.ViewClub_Page1;
import framework.pageObjects.subscriberManagement.SubscriberInformation_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/*
MON-3440
ADD CLUB:
    Pre-req - Done
    1       - Done
    2       - Partial,  ADD_MAKER_REQ_CLUB set to 'N'
    3       - OPEN
    4       - Done
    5       - Done
    6       - Done
    7       - Done [USSD]
    8       - Done [USSD]
    9       - Done
    10      - Done
    11      - OPEN
    12      - OPEN
    13      - Done
    14      - OPEN - IS_BANK_MANDATORY_FOR_SAVING_CLUB = 'N'
    15      - Done
    16,17,18,19,21 - Done
    22      - Done
    23      - Done
    24      - Dones
    TODO - Check Bank Accounts associated with Chairman, once the club is approved!
 */
public class Suite_MON_3440_AddSavingClub extends TestInit {
    private User subsChairmanUI, subsChairmanAPI;
    private SavingsClub sClubUI, sClubAPI;
    private String bankId;
    private OperatorUser naAddClub, naAppClub, naViewClub;
    private CurrencyProvider defaultProvider;
    private Wallet defaultWallet;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Create Chairman User for UI and API Test. " +
                "Create Saving club Objects.");
        s1.assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // get Default Provider
            defaultProvider = DataFactory.getDefaultProvider();
            defaultWallet = DataFactory.getDefaultWallet();
            bankId = DataFactory.getBankId(GlobalData.defaultBankName);

            // Operator Users with Specific Permissions
            naAddClub = DataFactory.getOperatorUserWithAccess("ADD_CLUB");
            naAppClub = DataFactory.getOperatorUserWithAccess("APP_CLUB");
            naViewClub = DataFactory.getOperatorUserWithAccess("VIEW_CLUB");

            // create subscriber Chairman user Objects
            subsChairmanUI = new User(Constants.SUBSCRIBER);
            subsChairmanAPI = new User(Constants.SUBSCRIBER);

            // Map SVC wallet Preference
            Preferences.init(s1)
                    .setSVCPreferences(subsChairmanUI, bankId);

            /*
            Create Subscribers with Default Mapping
             */
            SubscriberManagement.init(s1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsChairmanUI, defaultWallet.WalletId, true);

            SubscriberManagement.init(s1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsChairmanAPI, defaultWallet.WalletId, true);

            // Savings Club object, with default Member count and Approved Count : 1, 1
            sClubUI = new SavingsClub(subsChairmanUI, Constants.CLUB_TYPE_PREMIUM, bankId, false, false);

            // savings club object for API test, Preferences used for Member and approved Count
            sClubAPI = new SavingsClub(subsChairmanAPI, Constants.CLUB_TYPE_BASIC, bankId, true, true);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verify that Minimum Number of " +
     * approver can not be greater than Minimum Number of members
     *
     * @throws Exception
     */
    @Test(priority = 0)
    public void test_10() throws Exception {
        ExtentTest t10 = pNode.createNode("Test 10", " Verify that Minimum Number of " +
                "approver can not be greater than Minimum Number of members ");
        t10.assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SavingsClub sClub_10 = new SavingsClub(subsChairmanUI,
                    Constants.CLUB_TYPE_BASIC, bankId, true, true);
            sClub_10.MinApproverCount = 2; // Set Min Approver Count less than the Min Member Count
            sClub_10.MinMemberCount = 1;

            Login.init(t10)
                    .login(naAddClub);

            SavingsClubManagement.init(t10)
                    .startNegativeTest()
                    .initiateSavingsClub(sClub_10);

            Assertion.verifyErrorMessageContain("savingclub.error.min.approver.greater.than.min.member",
                    "Verify that the Minimum number of Approver can't be greater than Min Num of Member", t10);
        } catch (Exception e) {
            markTestAsFailure(e, t10);
        }


        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify that Minimum Number of
     * approver can not be greater than Minimum Number of members
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void test_11() throws Exception {
        ExtentTest t11 = pNode.createNode("Test 11", "Verify that Maximum Number of " +
                "approver can not be greater than Maximum Number of members ");
        t11.assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SavingsClub sClub_11 = new SavingsClub(subsChairmanAPI, Constants.CLUB_TYPE_BASIC, bankId, true, true);
            sClub_11.MaxApproverCount = 2;  // set the Max Approve greater than Max member
            sClub_11.MaxMemberCount = 1;
            sClub_11.MinApproverCount = 1;
            sClub_11.MinMemberCount = 1;

            Login.init(t11)
                    .login(naAddClub);

            SavingsClubManagement.init(t11)
                    .startNegativeTest()
                    .expectErrorB4Confirm()
                    .initiateSavingsClub(sClub_11);

            Assertion.verifyErrorMessageContain("savingclub.error.max.approver.greater.than.max.member",
                    "Verify that the Maximum number of Approver can't be greater than Maximum Num of Member", t11);
        } catch (Exception e) {
            markTestAsFailure(e, t11);
        }


        Assertion.finalizeSoftAsserts();
    }

    /**
     * Reject a Savings Club Approval
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.ECONET_UAT_5_0})
    public void test_01_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 01", "Reject a Savings Club Approval");
        t1.assignCategory(FunctionalTag.SAVING_CLUB);

        try {
// Create Savings Club
            Login.init(t1).login(naAddClub);

            SavingsClubManagement.init(t1)
                    .initiateSavingsClub(sClubUI);

            /**
             * SubTest
             * Check That the Club Status Is set to Inactive,
             */
            ExtentTest t2 = pNode.createNode("TEST 02", "Verify That Current Status is Approval Initiated").assignCategory(FunctionalTag.ECONET_UAT_5_0);

            Login.init(t1).login(naViewClub);

            String status = ViewClub_Page1.init(t2)
                    .navViewSavingClub()
                    .getClubCurrentStatus(sClubUI.CMMsisdn);

            Assertion.verifyEqual(status, Constants.APPROVAL_INITIATED, "Verify That Current Status is Approval Initiated", t2);
            t2.addScreenCaptureFromPath(ScreenShot.TakeScreenshot()); // add screen shot for reference

            // Reject Savings Club
            Login.init(t1).login(naAppClub);

            SavingsClubManagement.init(t1)
                    .rejectSavingsClub(sClubUI);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify That Savings Club with Same rejected Details Could be re Created
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("TEST 03", "Verify That Savings Club " +
                "with Same rejected Details Could be re Created");

        t3.assignCategory(FunctionalTag.SAVING_CLUB, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SMOKE);

        try {
            SavingsClubManagement.init(t3)
                    .createSavingsClub(sClubUI);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify That Savings Club with Same rejected Details Could be re Created
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void test_04() throws Exception {
        ExtentTest t3 = pNode.createNode("TEST 04", "Verify that the user can be a chairman of " +
                "a single club only at a given point of time");
        t3.assignCategory(FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SIT_5_0);

        /*
        Try to Create a Savings Club with the same User as Chairman
        As one Savings club is already created this operation must Fail
         */
        SavingsClub sClub_04 = new SavingsClub(subsChairmanUI, Constants.CLUB_TYPE_BASIC, bankId, false, false);

        try {
            Login.init(t3)
                    .login(naAddClub);

            SavingsClubManagement.init(t3)
                    .startNegativeTest()
                    .initiateSavingsClub(sClub_04);

            Assertion.verifyErrorMessageContain("saving.club.admin.already.associated",
                    "verify That user can be a chairman of a single club", t3);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }


        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify that a new wallet for savingclub is associated with the chairman of the club after club is approved
     * Verify that Account is created for the Club at the Bank Side
     *
     * @throws Exception
     */
    @Test(priority = 5)
    public void test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("TEST 05", "Verify that a new wallet for savingclub " +
                "is associated with the chairman of the club after club is approved &" +
                "verify that Account is created for the Club at the Bank Side");
        t5.assignCategory(FunctionalTag.SAVING_CLUB);

        User usrViewSubs = DataFactory.getChannelUserWithAccess("SUBS_INF", 0);

        String sClubWallet = DataFactory.getWalletName("18");
        try {
            Login.init(t5)
                    .login(usrViewSubs);

            new SubscriberInformation_Page1(t5)
                    .navigateToSubsInfo()
                    .selectProvider(defaultProvider.ProviderId)
                    .selectPaymentInstrument("WALLET")
                    .selectWalletType(sClubWallet)
                    .setMSISDN(subsChairmanUI.MSISDN)
                    .clickSubmit();

            // Verify that the link for Viewing user Info is available
            // if wallet was not added, in such case there will be a message
            if (DriverFactory.getDriver().findElements(By.linkText("Subscriber Information")).size() > 0) {
                t5.pass("Successfully verified that the Wallet SAVINGS CLUB is added automatically");
            } else {
                t5.fail("Failed to verify that the Wallet SAVINGS CLUB is added automatically with the Chairman");
                t5.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            }
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }


        /**
         * TODO  - Archived code
         * As the test has to be only performed with Subscriber
         * These codes could be used to view and Verify channel User
         */
        /*// View the User
        ChannelUserManagement.init(t5)
                .viewChannelUser(subsChairmanUI);

        if (DriverFactory.getDriver()
                .findElements(By.xpath("//tr/td[contains(text(),'" + sClubWallet + "')]/ancestor::tr[1]")).size() > 0) {

            t5.pass("Successfully verified that SAVINGCLUB Wallet is now associated with the User");

            String temTxt = DriverFactory.getDriver()
                    .findElement(By.xpath("//tr/td[contains(text(),'" + sClubWallet + "')]/ancestor::tr[1]")).getText();

            // other Validation
            Assertion.verifyContains(temTxt, sClubUI.CMGradeName, "Verify Chairman Grade name is Associated", t5);
            Assertion.verifyContains(temTxt, sClubUI.ClubWalletTCP, "Verify Club Wallet TCP is Associated", t5);
            Assertion.verifyContains(temTxt, sClubUI.ClubWalletMobileGrpRole, "Verify Club Wallet Mobile Group Role is Associated", t5);
        } else {
            t5.fail("SAVINGCLUB Wallet is not associated with the User");
            Utils.scrollToBottomOfPage();
            t5.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
        }

        // verify that Account is created for the Club at the Bank Side
        if (AppConfig.isBankMandatoryForSavingsClub) {
            String clubId = MobiquityGUIQueries.getSavingClubId(sClubUI.ClubName);

            if (DriverFactory.getDriver()
                    .findElements(By.xpath("//tr/td[contains(text(),'" + clubId + "')]/ancestor::tr[1]")).size() > 0) {
                t5.pass("Successfully verified that Account is created for the Club at the Bank Side");

                String temTxt = DriverFactory.getDriver()
                        .findElement(By.xpath("//tr/td[contains(text(),'" + clubId + "')]/ancestor::tr[1]")).getText();

                // verify that the Account correspons to the Bank linked to the Saving Club
                Assertion.verifyContains(temTxt, sClubUI.BankName, "Verify Bank Name ", t5);
                Assertion.verifyContains(temTxt, sClubUI.CMMsisdn, "Verify Account Number", t5);
            } else {
                t5.fail("Account is NOT created for the Club at the Bank Side");
                Utils.scrollToBottomOfPage();
                t5.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
            }
        } else {
            t5.info("Bank is not mandatory, please check the Preference - IS_BANK_MANDATORY_FOR_SAVING_CLUB");
        }*/

        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verify that created club ID should start with the value set in CLUB_ID_START_WITH system preference
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_06() throws Exception {
        ExtentTest t6 = pNode.createNode("TEST 06", "Verify that created club ID should " +
                "start with the value set in CLUB_ID_START_WITH system preference");
        t6.assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            String clubId = MobiquityGUIQueries.getSavingClubId(sClubUI.ClubName);

            // get the Prefix from The Club Id fetched from the DB
            String prefixFromDb = clubId.substring(0, AppConfig.savingClubIdPrefix.length());

            Assertion.verifyEqual(prefixFromDb, AppConfig.savingClubIdPrefix,
                    "Verify That the Club ID start with value set in CLUB_ID_START_WITH system preference", t6);

        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }

        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verify that created club ID should start with the value set in CLUB_ID_START_WITH system preference
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_07() throws Exception {
        ExtentTest t7 = pNode.createNode("TEST 07", "Verify Default Max and Min number of Approver and Members");
        t7.assignCategory(FunctionalTag.SAVING_CLUB);
        Login.init(t7)
                .login(naAddClub);

        try {
            AddClub_Page1 page = AddClub_Page1.init(t7);
            page.navigateToAddClubPage();

            String minMemberCountUI = page.getMinMemberCount();
            Assertion.verifyEqual(minMemberCountUI, AppConfig.defaultMinNumMember,
                    "Tally Min Member from Preference to the UI default", t7);

            String maxMemberCountUI = page.getMaxMemberCount();
            Assertion.verifyEqual(maxMemberCountUI, AppConfig.defaultMaxNumMember,
                    "Tally Max Member from Preference to the UI default", t7);

            String minApprovedCountUI = page.getMinApproverCount();
            Assertion.verifyEqual(minApprovedCountUI, AppConfig.defaultMinNumApprover,
                    "Tally Min Approved from Preference to the UI default", t7);

            String maxApprovedCountUI = page.getMaxApproverCount();
            Assertion.verifyEqual(maxApprovedCountUI, AppConfig.defaultMaxNumApprover,
                    "Tally Max Approved from Preference to the UI default", t7);
        } catch (Exception e) {
            markTestAsFailure(e, t7);
        }

        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verify that any registered user " +
     * is able to create (add initiate) a club through USSD service
     *
     * @throws Exception
     */
    @Test(priority = 8)
    public void test_08() throws Exception {
        ExtentTest t8 = pNode.createNode("TEST 08", "Verify that any registered user " +
                "is able to create (add initiate) a club through USSD service");
        t8.assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            sClubAPI = new SavingsClub(subsChairmanAPI, Constants.CLUB_TYPE_BASIC, bankId, true, true);

            Transactions.init(t8)
                    .addSavingClub(sClubAPI)
                    .verifyStatus(Constants.TXN_SUCCESS);
        } catch (Exception e) {
            markTestAsFailure(e, t8);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify that a user can be a chairman of a single club only at a given point of time
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void test_09() throws Exception {
        ExtentTest t9 = pNode.createNode("TEST 09 API", "Verify that a user can be a chairman of a single club only at a given point of time ");
        t9.assignCategory(FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SIT_5_0);
        try {
            sClubAPI = new SavingsClub(subsChairmanAPI, Constants.CLUB_TYPE_BASIC, bankId, true, true);

            Transactions.init(t9)
                    .addSavingClub(sClubAPI)
                    .verifyStatus("CMALASSO")
                    .verifyMessage("saving.club.admin.already.associated");
        } catch (Exception e) {
            markTestAsFailure(e, t9);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Debugging techniques:
     *
     * After Creating and User, write the user to xlsx file
     * eg. subsChairman.writeDataToExcel();
     *
     * Now if you are running second time u can fetch the user easily by providing its msisdn
     * to get the msisdn refer ChannelUser.xlsx sheet
     *
     * subsChairman = DataFactory.getUserUsingMsisdn("<MSISDN>")
     *
     *
     * Same goes for a Saving Club
     * sClubToJoin.writeDataToExcel(); ->> write to xlsx
     *
     * use below code to fetch the Saving club from TempSavingClu.xlsx
     * sClubToJoin = DataFactory.getSavingClubFromAppdata()
     */


}