package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by ravindra.dumpa on 12/22/2017.
 */
public class Suite_MON_3351_Email_Pwd_Enterprise_Random_Pwd_Y extends TestInit {
    OperatorUser usrCanChangePref, usrCanAddChUsr;
    String resetpwdValue = "000000";

    /**
     * Verifying random password when Enterprise on boarded using email notification
     *
     * @throws Throwable
     */
    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Set random password preference to Y");
        SystemPreferenceManagement.init(s1)
                .updateSystemPreference("IS_RANDOM_PASS_ALLOW", "Y");
        SystemPreferenceManagement.init(s1)
                .updateSystemPreference("DEFAULT_RESET_PWD", resetpwdValue);
        usrCanChangePref = DataFactory.getOperatorUserWithAccess("PREF001");
        usrCanAddChUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
    }

    /**
     * Test_01
     * Verifying random password when Enterprise on boarded using email notification
     *
     * @throws Throwable
     */
    @Test(priority = 1)
    public void Test_01() throws Throwable {
        ExtentTest t1 = pNode.createNode("Test_01", "Verifying random password when Enterprise on boarded");
        /**
         * Login as operator user
         * Create enterprise
         * fetch password from db and decrypt
         * verify the random password
         */
        //Login as operator user
        Login.init(t1).login(usrCanAddChUsr);

        //create enterprise
        User enterprise = new User(Constants.ENTERPRISE);
        ChannelUserManagement.init(t1).createChannelUserforRandomPassword(enterprise);

        //fetch password from db
        MobiquityGUIQueries m = new MobiquityGUIQueries();
        String actualpassword = MobiquityGUIQueries.dbFetchPassWordFromEmailQueue(enterprise.MSISDN);

        //decrypt the password
        DesEncryptor d = new DesEncryptor();
        String decryptedPassword = d.decrypt(actualpassword);

        //verify the random password
        Assertion.assertNotEqual(decryptedPassword, ConfigInput.userCreationPassword, "Verify email sent", t1);
    }

    /**
     * Test_02
     * Verifying random password when Enterprise reset the password using email notification
     *
     * @throws Throwable
     */
    @Test(priority = 2)
    public void Test_02() throws Throwable {
        ExtentTest t2 = pNode.createNode("Test_02", "Verify random password when Enterprise reset the password");
        /**
         * Login as operator user
         * Create enterprise
         * reset the password
         * fetch password from db and decrypt
         * verify the random password
         */
        //login as operator user
        Login.init(t2).login(usrCanAddChUsr);

        //Create enterprise
        User enterprise = new User(Constants.ENTERPRISE);
        ChannelUserManagement.init(t2).createChannelUserforRandomPassword(enterprise);

        //reset the password
        CommonUserManagement.init(t2).resetPasswordAsSuperAdmin(enterprise);

        //fetch password from db
        String actualpassword = MobiquityGUIQueries.dbFetchResetPassWordFromEmailQueue(enterprise.MSISDN);

        //decrypt the password
        DesEncryptor d = new DesEncryptor();
        String decryptedPassword = d.decrypt(actualpassword);

        //verify reset random password
        Assertion.assertNotEqual(decryptedPassword, resetpwdValue, "verify password", t2);
    }

    /**
     * Test_03
     * Verifying random password when merchant on boarded using email notification
     *
     * @throws Throwable
     */
    @Test(priority = 3)
    public void Test_03() throws Throwable {
        ExtentTest t3 = pNode.createNode("Test_03", "Verifying random password when merchant on boarded");
        /**
         * Login as operator user
         * Create merchant
         * fetch password from db and decrypt
         * verify the random password
         */
        //Login as operator user
        Login.init(t3).login(usrCanAddChUsr);

        //create head merchant
        User hmerchant = new User(Constants.HEAD_MERCHANT);
        ChannelUserManagement.init(t3).createChannelUserforRandomPassword(hmerchant);


        //create merchant
        User merchant = new User(Constants.MERCHANT);
        ChannelUserManagement.init(t3).createChannelUserforRandomPassword(merchant);

        //fetch password from db
        MobiquityGUIQueries m = new MobiquityGUIQueries();
        String actualpassword = MobiquityGUIQueries.dbFetchPassWordFromEmailQueue(merchant.MSISDN);

        //decrypt the password
        DesEncryptor d = new DesEncryptor();
        String decryptedPassword = d.decrypt(actualpassword);

        //verify the random password
        Assertion.assertNotEqual(decryptedPassword, ConfigInput.userCreationPassword, "Verify email sent", t3);
    }

    /**
     * Test_04
     * Verifying random password when merchant reset the password using email notification
     *
     * @throws Throwable
     */
    @Test(priority = 4)
    public void Test_04() throws Throwable {
        ExtentTest t4 = pNode.createNode("Test_04", "Verify random password when Merchant reset the password");
        /**
         * Login as operator user
         * Create merchant
         * reset the password
         * fetch password from db and decrypt
         * verify the random password
         */
        //login as operator user
        Login.init(t4).login(usrCanAddChUsr);

        //Create merchant
        User merchant = new User(Constants.MERCHANT);
        ChannelUserManagement.init(t4).createChannelUserforRandomPassword(merchant);

        //reset the password
        CommonUserManagement.init(t4).resetPasswordAsSuperAdmin(merchant);

        //fetch password from db
        MobiquityGUIQueries m = new MobiquityGUIQueries();
        String actualpassword = MobiquityGUIQueries.dbFetchResetPassWordFromEmailQueue(merchant.MSISDN);

        //decrypt the password
        DesEncryptor d = new DesEncryptor();
        String decryptedPassword = d.decrypt(actualpassword);

        //verify reset random password
        Assertion.assertNotEqual(decryptedPassword, resetpwdValue, "verify password", t4);
    }

    @AfterClass(alwaysRun = true)
    public void Test_05() throws Exception {
        ExtentTest f1 = pNode.createNode("Test_05", "Changing random password preference");
        SystemPreferenceManagement.init(f1)
                .updateSystemPreference("IS_RANDOM_PASS_ALLOW", "N");
    }
}
