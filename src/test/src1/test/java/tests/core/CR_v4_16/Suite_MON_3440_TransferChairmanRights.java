package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_MON_3440_TransferChairmanRights extends TestInit {

    private User subsChairman, subsMember1, subsMember2;
    private SavingsClub sClub;
    private OperatorUser naTrfClubAdm, naTrfClubAdmApp;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        // Extent Test
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific for Testing Transfer Chairman Rights. " +
                "Make Sure that Service Charge CLUB DEPOSIT is configured. Create Chairman User and also create the Saving Club for testing")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // get Default Provider
            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);

            // Operator Users from AppData
            naTrfClubAdm = DataFactory.getOperatorUsersWithAccess("TRF_CLUB_ADM").get(0);
            naTrfClubAdmApp = DataFactory.getOperatorUsersWithAccess("APPTRF_CLUB_ADM").get(0);

            // User Objects
            subsChairman = new User(Constants.SUBSCRIBER);
            subsMember1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            subsMember2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

            // Map SVC wallet Preference
            Preferences.init(s1)
                    .setSVCPreferences(subsChairman, bankId);

            // Savings Club object
            sClub = new SavingsClub(subsChairman, Constants.CLUB_TYPE_PREMIUM, defaultBank.BankID, false, false);

            // set Min and Max number of Member and Approver
            sClub.setMemberCount(2, 3);
            sClub.setApproverCount(2, 2);

            /*
            Add Member to the Club Object
             */
            sClub.addMember(subsMember1);
            sClub.addMember(subsMember2);

            // ServiceCharge Object
            ServiceCharge cDeposit = new ServiceCharge(Services.CLUB_DEPOSIT, subsMember1, subsChairman, null, "18", null, null);

            /**
             P R E   R E Q U I S I T E    C R E A T I O N
             */

            //Create Subscribers with Default Mapping
            SubscriberManagement.init(s1)
                    .createSubscriberDefaultMapping(subsChairman, true, true);

            //Configure Service Charge For Club Deposit
            ServiceChargeManagement.init(s1)
                    .configureServiceCharge(cDeposit);

            // Create Saving Club
            SavingsClubManagement.init(s1)
                    .createSavingsClub(sClub);

            // make sure that Users are joined and are active
            for (User member : sClub.getMembers()) {
                // Join Member 1 and make SVA deposit

                if (!member.MSISDN.equals(sClub.CMMsisdn)) {
                    Transactions.init(s1)
                            .joinOrResignSavingClub(sClub, member, true)
                            .verifyStatus(Constants.TXN_SUCCESS);

                    Transactions.init(s1)
                            .depositSavingClub(sClub, member, AppConfig.minSVCDepositAmount.toString())
                            .verifyStatus(Constants.TXN_SUCCESS);
                }
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void transferRightTests() throws Exception {

        /**
         * Test 1
         * Verify Error, when the New Chairman is not registered
         */
        ExtentTest t1 = pNode.createNode("Test 01", "Negative Test, Verify Error, when the New Chairman is not registered")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SavingsClubManagement.init(t1)
                    .startNegativeTest()
                    .initiateChairManTransfer(sClub.ClubId,
                            "12365478", subsMember1.MSISDN, Constants.CM_TRANSFER_REASON_RESIGNED, false);
            Assertion.verifyErrorMessageContain("savingclub.chairman.not.registered", "Verify Error, when the New Chairman is not registered", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


        /**
         * Test 2
         * Verify Error, when the Club Member is not registered
         */
        ExtentTest t2 = pNode.createNode("Test 02", "Negative Test, Verify Error, when the Club Member is not registered")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SavingsClubManagement.init(t2)
                    .startNegativeTest()
                    .initiateChairManTransfer(sClub.ClubId,
                            subsMember1.MSISDN, "12365478", Constants.CM_TRANSFER_REASON_RESIGNED, false);
            Assertion.verifyErrorMessageContain("savingclub.club.member.not.registered", "Verify Error, when the Club Member is not registered", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }


        /**
         * Test 3
         * Verify Error, when New Chairman MSISDN and Club member MSISDN are Same
         */
        ExtentTest t3 = pNode.createNode("Test 03", "Negative Test, Verify that New Chairman MSISDN" +
                " and Club member MSISDN should not be same")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SavingsClubManagement.init(t3)
                    .startNegativeTest()
                    .initiateChairManTransfer(sClub.ClubId,
                            subsMember1.MSISDN, subsMember1.MSISDN, Constants.CM_TRANSFER_REASON_RESIGNED, false);
            Assertion.verifyErrorMessageContain("savingclub.chairman.member.are.same", "Verify Error, when New Chairman MSISDN and Club member MSISDN are same", t3);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }


    }

}
