package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by shubham.kumar1
 * <p>
 * <p>
 * Use Case: MON-3380
 * <p>
 * To verify that network admin should be able to perform O2C transaction from UI
 * To verify the service charge and commission charge displayed during the O2C initiation should same as what is defined in service charge and commission policy
 * To verify that O2C transaction approval should not be successful and transaction should fail with appropriate error code & error message if receiver is:
 * 1. delete initiated after the O2C initiation
 * 2. churn initiated after the O2C initiation
 * To verify that O2C transaction approval should not be successful and transaction should fail with appropriate error code & error message if receiver account is:
 * 1. suspend initiated after the O2C initiation
 * 2. delete initiated after the O2C initiation
 */

public class Suite_MON_3380_O2CTransactions extends TestInit {

    private User newWholesaler, wholesaler;
    private OperatorUser channelAdmin, o2cInitiator;
    private String providerName;
    private String txnId;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest t = pNode.createNode("Setup", "Setup function for Suite_MON_3380_O2CTransactions");

        try {
            providerName = DataFactory.getDefaultProvider().ProviderName;
            o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            channelAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            newWholesaler = new User(Constants.WHOLESALER);
            OperatorUser payer = new OperatorUser(Constants.NETWORK_ADMIN);

            ChannelUserManagement.init(t).
                    createChannelUserDefaultMapping(newWholesaler, true);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1)
    public void deleteChannelUserWithO2CInitiated() throws Exception {

        ExtentTest t2 = pNode.createNode("TEST 02", "To verify that O2C transaction approval should not be successful" +
                "and transaction should fail with appropriate error code & error message if receiver is delete initiated after the O2C initiation");
        try {
            Login.init(t2).login(o2cInitiator);
            txnId = TransactionManagement.init(t2)
                    .initiateO2CWithProvider(newWholesaler, providerName, "5");
            if (txnId != null) {
                t2.pass("O2C initiation successful");
                //Delete Initiate Channel User
                ChannelUserManagement.init(t2)
                        .startNegativeTest()
                        .initiateChannelUserDelete(newWholesaler);

                Assertion.verifyErrorMessageContain("not.delete.channel.existing.transaction", "Verify user can't be delete initiated after o2c is initiated", t2);
            } else {
                t2.fail("couldn't initiate O2C");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 3, dependsOnMethods = "deleteChannelUserWithO2CInitiated")
    public void churnChannelUserWithO2CInitiated() throws Exception {

        ExtentTest t3 = pNode.createNode("TEST 03", "To verify that O2C transaction approval should not be successful and transaction " +
                "should fail with appropriate error code & error message if receiver is churn initiated after the O2C initiation");

        try {
            //Churn Initiate Channel User
            CommonUserManagement.init(t3)
                    .startNegativeTest()
                    .churnInitiateUser(newWholesaler);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 4, dependsOnMethods = "deleteChannelUserWithO2CInitiated", enabled = false)
    public void test_04() throws Exception {

        ExtentTest t4 = pNode.createNode("TEST 04", "To verify that O2C transaction approval should not be successful " +
                "and transaction should fail with appropriate error code & error message if receiver account is Modify initiated after the O2C initiation");

        try {
            // modify Initiate a Channel user O2c Is already initiated
            ChannelUserManagement.init(t4)
                    .modifyChannelUserWithNoChange(newWholesaler);

            // now try to Approve O2C
            startNegativeTest();
            TransactionManagement.init(t4)
                    .o2cApproval1(txnId, true);
            Assertion.verifyErrorMessageContain("not.delete.channel.existing.transaction", "Verify user can't be Churn Initiated after o2c is initiated", t4);

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        } finally {
            stopNegativeTest();

            ChannelUserManagement.init(t4)
                    .approveRejectModifyChannelUser(newWholesaler, true);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, dependsOnMethods = "deleteChannelUserWithO2CInitiated", enabled = false)
    public void test_05() throws Exception {

        ExtentTest t5 = pNode.createNode("TEST 05", "To verify that O2C transaction approval should not be successful " +
                "and transaction should fail with appropriate error code & error message if receiver account is delete initiated after the O2C initiation");


        try {
            // modify Initiate a Channel user O2c Is already initiated
            ChannelUserManagement.init(t5)
                    .initiateChannelUserDelete(newWholesaler);

            // now try to Approve O2C
            startNegativeTest();
            TransactionManagement.init(t5)
                    .o2cApproval1(txnId, true);
            Assertion.verifyErrorMessageContain("not.delete.channel.existing.transaction", "Verify user can't be Churn Initiated after o2c is initiated", t5);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        } finally {
            stopNegativeTest();

            ChannelUserManagement.init(t5)
                    .approveRejectDeleteChannelUser(newWholesaler, false);
        }
        Assertion.finalizeSoftAsserts();
    }


}