package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.savingsClubManagement.ViewClub_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.HashMap;

/**
 * Modify Club: MON-3440
 * -----------------------------------
 * JIRA     STATUS          TEST CASE
 * -----------------------------------
 * 1       Done
 * 2       Done
 * 3       Done
 * 4       Done
 * 5       Done
 * NOTE - for Debugging Techniques used for this script Scroll to Bottom
 */
public class Suite_MON_3440_JoinSavingClub extends TestInit {

    private SavingsClub sClubToJoin;
    private OperatorUser naViewClub;
    private User subsChairman, subsMember1, subsMember2, subsMember3, subsMember4;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        /**
         * O B J E C T S
         */

        try {
            ExtentTest eSetup = pNode.createNode("Setup", "Fetch Base Set Users. " +
                    "Create Chairman User. " +
                    "Make sure Club Deposit service charge is configured." +
                    "Create Savings club with appropriate configuration.");

            // get App Data
            CurrencyProvider defaultProvider = GlobalData.defaultProvider;
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);
            naViewClub = DataFactory.getOperatorUsersWithAccess("VIEW_CLUB").get(0);
            subsMember1 = new User(Constants.SUBSCRIBER);
            subsMember2 = new User(Constants.SUBSCRIBER);
            subsMember3 = new User(Constants.SUBSCRIBER);
            subsMember4 = new User(Constants.SUBSCRIBER);
            subsChairman = new User(Constants.SUBSCRIBER);

            // Map SVC wallet Preference
            Preferences.init(eSetup)
                    .setSVCPreferences(subsChairman, bankId);

            SubscriberManagement.init(eSetup)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsMember1, defaultWallet.WalletId, true);
            SubscriberManagement.init(eSetup)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsMember2, defaultWallet.WalletId, true);
            SubscriberManagement.init(eSetup)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsMember3, defaultWallet.WalletId, true);
            SubscriberManagement.init(eSetup)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsMember4, defaultWallet.WalletId, true);
            SubscriberManagement.init(eSetup)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsChairman, defaultWallet.WalletId, true);

            // Savings Club object
            sClubToJoin = new SavingsClub(subsChairman,
                    Constants.CLUB_TYPE_PREMIUM,
                    DataFactory.getDefaultBankIdForDefaultProvider(),
                    false,
                    false);

            // Set the Member and Approver Count
            sClubToJoin.setMemberCount(2, 4);
            sClubToJoin.setApproverCount(2, 3);

            /*
            Add Members to the Club object
            Adding 2 members as min member count is set to 2 ;  sClubToJoin.setMemberCount(2, 3);
             */
            sClubToJoin.addMember(subsMember1);
            sClubToJoin.addMember(subsMember2);

            // ServiceCharge Object
            ServiceCharge cDeposit = new ServiceCharge(Services.CLUB_DEPOSIT,
                    subsMember1,
                    subsChairman,
                    null,
                    DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId,
                    null,
                    null);

            /**
             * P R E   R E Q U I S I T E    C R E A T I O N
             */
            ServiceChargeManagement.init(eSetup)
                    .configureServiceCharge(cDeposit);

            // Create Saving Club
            SavingsClubManagement.init(eSetup)
                    .createSavingsClub(sClubToJoin);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify that Created Saving Club Status is AI
     *
     * @throws Exception
     */
    @Test(priority = 0, enabled = true, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-3440-01", "Verify that Created Saving Club Status is Approved")
                .assignCategory(FunctionalTag.SAVING_CLUB);
        try {
            Login.init(t1).login(naViewClub);

            String status = ViewClub_Page1.init(t1)
                    .navViewSavingClub()
                    .getClubCurrentStatus(sClubToJoin.CMMsisdn);

            Assertion.verifyEqual(status, Constants.APPROVED, "Verify That Current Status is Approval Initiated", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 02
     * Join Minimum number of members and verify the Message Notification
     * TODO - Verify Notification for SVC Transfer - JIRA-3968
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test 02", "Verify that Members can join the savings Club," +
                " Notification must be sent to the Members")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            for (User member : sClubToJoin.getMembers()) {
                // Join Saving Club
                if (!member.MSISDN.equals(sClubToJoin.CMMsisdn)) {
                    Transactions.init(t2)
                            .joinOrResignSavingClub(sClubToJoin, member, true)
                            .verifyStatus(Constants.TXN_SUCCESS)
                            .verifyMessage("savingclub.member.join.success", sClubToJoin.ClubName, sClubToJoin.ClubId);

                    SMSReader.init(t2)
                            .verifyNotificationContain(member.MSISDN, "savingclub.member.join.success", sClubToJoin.ClubName, sClubToJoin.ClubId);
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 03
     * Verify that the Club Status is Now active as mininum number of member have joined
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_03() throws Exception {
        ExtentTest t2 = pNode.createNode("Test 03", "Verify that the Club Status is now Active");
        t2.assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            Login.init(t2).login(naViewClub);

            String status = ViewClub_Page1.init(t2)
                    .navViewSavingClub()
                    .getClubCurrentStatus(sClubToJoin.CMMsisdn);

            Assertion.verifyEqual(status, Constants.STATUS_CLUB_ACTIVE, "Verify That Current Status is Active for - " + sClubToJoin.CMMsisdn, t2);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 04
     * Perform SVC Transfer and Verify that Now The members are Active
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test 04", "Verify that User is not active unless depositing Min SVA amount");

        t4.assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            for (User member : sClubToJoin.getMembers()) {
                // verify the Pre SVA deposit User Status
                String status = getMemberCurrentStatus(sClubToJoin.ClubId, member.MSISDN, t4).get("status");
                Assertion.verifyEqual(status, Constants.APPROVAL_INITIATED, "Verify Status is Activation Initiated", t4);

                // Member 1 Joins the Club run the API
                Transactions.init(t4)
                        .depositSavingClub(sClubToJoin, member, AppConfig.minSVCDepositAmount.toString())
                        .verifyStatus(Constants.TXN_SUCCESS)
                        .verifyMessage("savingclub.deposit.confirmation");

                // verify the Notification message
                SMSReader.init(t4)
                        .verifyNotificationContain(member.MSISDN, "savingclub.deposit.confirmation.msg");

                // verify that user is now active
                HashMap<String, String> postSVADepositStatus = getMemberCurrentStatus(sClubToJoin.ClubId, member.MSISDN, t4);
                Assertion.verifyEqual(postSVADepositStatus.get("status"), Constants.STATUS_CLUB_ACTIVE, "Verify Status is Currently Active", t4);

                // Verify that the Approver Status is Also 'Y'
                Assertion.verifyEqual(postSVADepositStatus.get("approver"), Constants.STATUS_CLUB_ACTIVE, "Verify that the User is Approver", t4);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }

        Assertion.finalizeSoftAsserts();
    }


    /**
     * Note -  in the setup the Max number of member is set to 3 and max approver is set to 2
     * Adding one more member now, will not be automatically be the approver
     */
    @Test(priority = 5, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("Test 05", "Verify that joined members will be the approver of the club till members count reaches the " +
                "number set for Maximum Number of approvers while creating club");
        t5.assignCategory(FunctionalTag.SAVING_CLUB);

        try {

            Transactions.init(t5)
                    .joinOrResignSavingClub(sClubToJoin, subsMember3, true)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyMessage("savingclub.member.join.success", sClubToJoin.ClubName, sClubToJoin.ClubId);

            HashMap<String, String> postSVADepositStatus = getMemberCurrentStatus(sClubToJoin.ClubId, subsMember3.MSISDN, t5);

            Assertion.verifyEqual(postSVADepositStatus.get("approver"), "N", "Verify that the User is NOT Approver", t5);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Note -  in the setup the Max number of member is set to 3 and max approver is set to 2
     * Adding one more member now, will not be automatically be the approver
     */
    @Test(priority = 6)
    public void test_06() throws Exception {
        ExtentTest t5 = pNode.createNode("Test 06", "Add Member More that the Max member, Verify Error Message");
        t5.assignCategory(FunctionalTag.SAVING_CLUB);

        try {

            Transactions.init(t5)
                    .joinOrResignSavingClub(sClubToJoin, subsMember4, true)
                    .verifyStatus("9999208")
                    .verifyMessage("savingclub.max.member.reached", sClubToJoin.ClubName, sClubToJoin.ClubId);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }

        Assertion.finalizeSoftAsserts();
    }


    /**
     * Get Members current Status
     *
     * @param clubId
     * @param memberMsisdn
     * @param chNode
     * @return
     * @throws Exception
     */
    private HashMap<String, String> getMemberCurrentStatus(String clubId, String memberMsisdn, ExtentTest chNode) throws Exception {
        pNode.info("Get getUserCurrentStatus for msisdn - " + memberMsisdn + " and Club id - " + clubId);
        naViewClub = DataFactory.getOperatorUsersWithAccess("VIEW_CLUB").get(0);
        Login.init(chNode).login(naViewClub);
        return ViewClub_Page1.init(chNode)
                .navViewSavingClub()
                .clickOnViewClub(clubId)
                .getUserCurrentStatus(memberMsisdn);
    }

    /**
     * Debugging techniques:
     *
     * After Creating and User, write the user to xlsx file
     * eg. subsChairman.writeDataToExcel();
     *
     * Now if you are running second time u can fetch the user easily by providing its msisdn
     * to get the msisdn refer ChannelUser.xlsx sheet
     *
     * subsChairman = DataFactory.getUserUsingMsisdn("<MSISDN>")
     *
     *
     * Same goes for a Saving Club
     * sClubToJoin.writeDataToExcel(); ->> write to xlsx
     *
     * use below code to fetch the Saving club from TempSavingClu.xlsx
     * sClubToJoin = DataFactory.getSavingClubFromAppdata()
     */

}