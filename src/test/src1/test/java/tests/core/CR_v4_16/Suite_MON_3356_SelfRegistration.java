package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by rahul.rawat on 9/13/2017.
 * <p>
 * MON-3356- Assign different grades to customers getting on-board through self registration process
 */
public class Suite_MON_3356_SelfRegistration extends TestInit {
    private User subs;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        ExtentTest t1 = pNode.createNode("Setup", "Setup Specific for this script");
        try {
            OperatorUser operator = new OperatorUser(Constants.OPERATOR);
            subs = new User(Constants.SUBSCRIBER);

            // Configure the Service Charge Acquisition fee
            ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs, operator, null, null, null, null);
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            String[] payIdArr = DataFactory.getPayIdApplicableForSubs();
            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(subs, payIdArr);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * 1.Create ServiceCharge for joining fee(New Subscriber Commission Rule link in UI)
     * 2.Create ServiceCharge for acquisition fee
     * 3.Create a wallet preference from UI for [ Subscriber  with Non_KYC]
     * 4.Self Subscriber registration from API
     */
    @Test(priority = 1)
    public void verifyThatAbleAssignDifferentGradesToSubscriberDuringSelfRegistration() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01", "Assign different grades to customers getting on-board through self registration process." +
                "Verify grade Count is Equal to Wallet Grade For User");

        try {
            ArrayList<String> paymentTypePerference = new ArrayList<String>();
            ArrayList<String> paymentTypeForUser = new ArrayList<String>();

            //Fetch the wallet Number for SUBS with NoKyc
            ResultSet gradeCount = MobiquityGUIQueries.fetchDetailsForKycModeAndGradeCode("NO_KYC", Constants.SUBSCRIBER);
            while (gradeCount.next()) {
                paymentTypePerference.add(gradeCount.getString("PAYMENT_TYPE_ID"));
            }

            //Self-subscriber registration through API
       /* Transactions.init(t1)
                .selfRegistrationForSubscriber(subs);*/

            //Fetch the wallet of subscriber On-boarded with
            ResultSet walletGradeForUser = MobiquityGUIQueries.fetchDetailsOfWalletsForUser(subs.MSISDN);
            while (walletGradeForUser.next()) {
                paymentTypeForUser.add(walletGradeForUser.getString("PAYMENT_TYPE_ID"));
            }

            Utils.captureScreen(t1);
            Assert.assertEquals(gradeCount.getRow(), walletGradeForUser.getRow());
            Assertion.verifyEqual(gradeCount.getRow(), walletGradeForUser.getRow(),
                    "Verify grade Count is Equal to Wallet Grade For User", t1);

            if (paymentTypePerference.size() == paymentTypeForUser.size()) {
                Assert.assertTrue(paymentTypeForUser.containsAll(paymentTypePerference));
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

}
