package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.BulkBankAssociation_Pg;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by ravindra.dumpa on 12/1/2017.
 */
public class Suite_MON_3341_Bulk_Reg_Bank_Services extends TestInit {

    private OperatorUser usrCanDoBnkAssoci;

    @BeforeClass
    public void setUp() throws Exception {
        ExtentTest s1 = pNode.createNode("setUp:");
        usrCanDoBnkAssoci = DataFactory.getOperatorUsersWithAccess("SUBS_BNK_ASSOC").get(0);
    }

    /**
     * Test_01
     * Verifying Bulk bank association using csv file
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void Test_01() throws Exception {

        ExtentTest t1 = pNode.createNode("Test_01:Verify Bulk Bank association using csv file");
        /**
         * update the preference IS_USERTYPE_ALLOWED to Y
         * create subscriber with out bank association
         * login as operator
         * navigate to bulk bank association and download csv file
         * fill the bulk bank details in csv and upload and submit
         * verify successful message
         */
        //update the preference IS_USERTYPE_ALLOWED to Y
        SystemPreferenceManagement.init(t1)
                .updateSystemPreference("IS_USERTYPE_ALLOWED", "Y");


        //Create subscriber with Bank association
        User sub = new User(Constants.SUBSCRIBER);
        SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub, true, false);

        //login as operator user
        Login.init(t1).login(usrCanDoBnkAssoci);

        //navigate bulk bank association page and download csv file
        BulkBankAssociation_Pg.init(t1).navBulkBankAssociation_Pg();
        BulkBankAssociation_Pg.clickOnStartDownload();

        //fill the details in csv file upload and submit
        ChannelUserManagement.init(t1).writeBulkUserBankAssociationDetailsInCSV(sub, FilePath.fileBulkBankAssoc);
        BulkBankAssociation_Pg.init(t1).uploadFile(FilePath.fileBulkBankAssoc);
        BulkBankAssociation_Pg.init(t1).submitCsv();

        //verify successful message
        Assertion.verifyActionMessageContain("bulk.bank.associ.successful", "Verify bulk bank association", t1);
    }

    /**
     * Test_02
     * Verifying banks in list of codes
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void Test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test_02:Verify banks in list of codes");
        /**
         * update the preference IS_USERTYPE_ALLOWED to N
         * login as operator
         * navigate to bulk bank association and download csv file
         * fill the bulk bank details in csv and upload and submit
         * verify successful message
         */
        //update the preference IS_USERTYPE_ALLOWED to N
        SystemPreferenceManagement.init(t2)
                .updateSystemPreference("IS_USERTYPE_ALLOWED", "N");

        //login as operator user
        Login.init(t2).login(usrCanDoBnkAssoci);

        //navigate to bulk bank association page and verify list of codes
        BulkBankAssociation_Pg.init(t2)
                .navBulkBankAssociation_Pg()
                .clickOnListOfCodes()
                .verifyListOfCodesPage();
    }

    /**
     * Test_03
     * Verifying usertype is  mandatory when preference is Y
     *
     * @throws Exception
     */
    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_03:Verifying usertype is mandatory when preference is Y");
        /**
         * update the preference IS_USERTYPE_ALLOWED to Y
         * login as operator
         * navigate to bulk bank association and download csv file
         * download csv file
         * verify usertype
         */
        //update the preference IS_USERTYPE_ALLOWED to Y
        SystemPreferenceManagement.init(t3)
                .updateSystemPreference("IS_USERTYPE_ALLOWED", "Y");

        //login as operator
        Login.init(t3).login(usrCanDoBnkAssoci);

        //navigate bulk bank asscociation page and download csv file
        BulkBankAssociation_Pg.init(t3).navBulkBankAssociation_Pg();
        BulkBankAssociation_Pg.clickOnStartDownload();

        //verify in csv file
        BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile(FilePath.fileBulkBankAssoc, "User Type*");


    }

    /**
     * Test_04
     * Verifying usertype is not mandatory when preference is N
     *
     * @throws Exception
     */
    @Test(priority = 4)
    public void Test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_04:Verify usertype is not mandatory when preference is N");
        /**
         * update the preference IS_USERTYPE_ALLOWED to N
         * login as operator
         * navigate to bulk bank association and download csv file
         * download csv file
         * verify usertype
         */
        //update the preference IS_USERTYPE_ALLOWED to N
        SystemPreferenceManagement.init(t4)
                .updateSystemPreference("IS_USERTYPE_ALLOWED", "N");

        //login as operator
        Login.init(t4).login(usrCanDoBnkAssoci);

        //navigate bulk bank asscociation page and download csv file
        BulkBankAssociation_Pg.init(t4).navBulkBankAssociation_Pg();
        BulkBankAssociation_Pg.clickOnStartDownload();

        //verify usertype is not mandatory
        try {
            BulkChUserAndSubRegAndModPage.init(t4).verifyFieldInCsvFile(FilePath.fileBulkBankAssocEconet, "User Type*");
        } catch (Exception e) {
            t4.info("usertype is not mandatory");
        }

    }

    /**
     * Test_05
     * Verifying bulk bank associ of user having linked with bank
     *
     * @throws Exception
     */
    @Test(priority = 5)
    public void Test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("Test_05:Verifying bulk bank associ of user already linked with bank");
        /**
         * update the preference IS_USERTYPE_ALLOWED to N
         * create subscriber with bank association
         * login as operator
         * navigate to bulk bank association and download csv file
         * fill the bulk bank details in csv and upload and submit
         * verify successful message
         */
        //update the preference IS_USERTYPE_ALLOWED to N
        SystemPreferenceManagement.init(t5)
                .updateSystemPreference("IS_USERTYPE_ALLOWED", "N");

        //create subscriber with Bank details
        User sub = new User(Constants.SUBSCRIBER);
        SubscriberManagement.init(t5).createSubscriber(sub);

        //login as operator
        Login.init(t5).login(usrCanDoBnkAssoci);

        //navigate to bulk bank association and download csv file
        BulkBankAssociation_Pg.init(t5).navBulkBankAssociation_Pg();
        BulkBankAssociation_Pg.clickOnStartDownload();

        //write details in csv and upload
        ChannelUserManagement.init(t5).writeBulkUserBankAssociationDetailsInCSV(sub, FilePath.fileBulkBankAssoc);
        BulkBankAssociation_Pg.init(t5).uploadFile(FilePath.fileBulkBankAssoc);
        BulkBankAssociation_Pg.init(t5).submitCsv();

        //verify error message
        Assertion.verifyEqual(Assertion.isErrorInPage(t5), true, "verified error message", t5);

    }

    /**
     * Test_06
     * Verifying bulk bank associ with no usertype
     *
     * @throws Exception
     */
    @Test(priority = 6)
    public void Test_06() throws Exception {
        ExtentTest t5 = pNode.createNode("Test_06:Verify bulk bank associ with no usertype");
        /**
         * update the preference IS_USERTYPE_ALLOWED to N
         * create subscriber with bank association
         * login as operator
         * navigate to bulk bank association and download csv file
         * fill the bulk bank details in csv and upload and submit
         * verify successful message
         */
        //update the preference IS_USERTYPE_ALLOWED to N
        SystemPreferenceManagement.init(t5)
                .updateSystemPreference("IS_USERTYPE_ALLOWED", "N");

        //create subscriber without bank association
        User sub = new User(Constants.SUBSCRIBER);
        SubscriberManagement.init(t5).createSubscriberDefaultMapping(sub, true, false);

        //Login as Operator
        Login.init(t5).login(usrCanDoBnkAssoci);

        //navigate to page and download csv
        BulkBankAssociation_Pg.init(t5).navBulkBankAssociation_Pg();
        BulkBankAssociation_Pg.clickOnStartDownload();

        //fill the details in csv and upload
        ChannelUserManagement.init(t5).writeBulkUserBankAssociationDetailsInCSV(sub, FilePath.fileBulkBankAssocEconet);
        BulkBankAssociation_Pg.init(t5).uploadFile(FilePath.fileBulkBankAssocEconet);
        BulkBankAssociation_Pg.init(t5).submitCsv();

        //verify there is no error message
        Assertion.verifyEqual(Assertion.isErrorInPage(t5), false, "no error message", t5);

    }

}
