package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.KinUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

/**
 * Created by ravindra.dumpa on 10/12/2017.
 */
public class Suite_MON_3451_Subscriber_Registration_DOB_Enhancement extends TestInit {

    private User usrCanAddSubs;
    private int agelimit;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest t1 = pNode.createNode("SetUp", "Setup Specific for testing Subscribe DOB enhancement MON 3451." +
                " Update the System preference CUST_AGE_LIMIT." +
                "Check for the system preference is Age LImit required and based on the result update the preference IS_AGE_LIMIT_REQUIRE = Y");
        MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();
        Map<String, String> mtxprefMap = dbHandler.dbGetMtxPreferences();
        agelimit = Integer.parseInt(mtxprefMap.get("CUST_AGE_LIMIT"));
        if (!AppConfig.isAgeLimitIsRequired) {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_AGE_LIMIT_REQUIRE", "Y");
        } else {
            t1.info("Age Limit is not required");
        }

        usrCanAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.WHOLESALER);
    }

    /**
     * User age is less than age limit and kin details are not provided, expect an error
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01", "Subscriber registration with dob enhancement with below age");

        Login.init(t1)
                .login(usrCanAddSubs);

        User subsUser = new User(Constants.SUBSCRIBER);

        // now for a invalid dta date you can run like below
        subsUser.setDateOfBirth(new DateAndTime().getDate(-((365 * agelimit) - 5)));//TODO caliculate leaf years
        SubscriberManagement.init(t1)
                .startNegativeTest()
                .addInitiateSubscriber(subsUser);

        Assertion.verifyErrorMessageContain("subs.error.custageislessthan.limit", "Description", t1);
    }

    /**
     * Kin user is greater than age limit, no error
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void Test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test_02", "Subscriber registration with dob enhancement with above age");

        Login.init(t2)
                .login(usrCanAddSubs);

        User subsUser = new User(Constants.SUBSCRIBER);
        subsUser.setDateOfBirth(new DateAndTime().getDate(-((365 * agelimit) + 5)));//TODO caliculate leaf years

        SubscriberManagement.init(t2)
                .addInitiateSubscriber(subsUser);


        Assertion.verifyEqual(Assertion.isErrorInPage(t2), false,
                "Verify if Date of Birth is more than 10 years, Kin details are not required", t2);
    }

    /**
     * User is less than age limit, and kin details are provided, no error(functionality changed
     *
     * @throws Exception
     */
    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_03", "Subscriber registration with dob enhancement with below age and kin details");

        // create a user object and set the date of birth less than age limit
        User subsUser = new User(Constants.SUBSCRIBER);
        subsUser.setDateOfBirth(new DateAndTime().getDate(-((365 * agelimit) - 5)));//TODO caliculate leaf years

        // assign a kin user
        subsUser.setKinUser(new KinUser());

        // Login As user
        Login.init(t3)
                .login(usrCanAddSubs);

        SubscriberManagement.init(t3)
                .addInitiateSubscriber(subsUser);

        Assertion.verifyEqual(Assertion.isErrorInPage(t3), false,
                "Verify After Providing KinDetails Subscriber registration can be proceeded!", t3);

    }

    @Test(priority = 4)
    public void Test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_04", "Subscriber registration with dob enhancement with below age using api");
        /**
         * Create a subscriber user object
         * set date of birth to less than age limit
         * create a kin user object and assign it to the subscriber
         * hit the API, Error message should be Customer age is below permitted age.
         */
        User subsUser_04 = new User(Constants.SUBSCRIBER);
        subsUser_04.setDateOfBirth(new DateAndTime().getDate(-((365 * agelimit) - 5)));//TODO caliculate leaf years

        KinUser neKinUser = new KinUser();
        subsUser_04.setKinUser(neKinUser);

        //self registering subscriber with kin details
        //Verifying status of the subscriber registration
        Transactions.init(t4).startNegativeTest()
                .selfRegistrationForSubscriberWithKinDetails(subsUser_04)
                .assertStatus("770155");

    }

    @Test(priority = 5)
    public void Test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("Test_05", "Subscriber registration with dob enhancement with below age age with kin details using api");

        // create a user object
        User subsUser_05 = new User(Constants.SUBSCRIBER);
        subsUser_05.setDateOfBirth(new DateAndTime().getDate(-((365 * agelimit) - 5)));//TODO calculate leaf years

        //self registering subscriber with out kin details
        Transactions.init(t5).startNegativeTest()
                .selfRegistrationForSubscriberWithKinDetails(subsUser_05)
                .assertStatus("770155");

        //result.assertMessage("dob.enhancement.reg.succes",t4);
    }

    @Test(priority = 6)
    public void Test_06() throws Exception {
        ExtentTest t6 = pNode.createNode("Test_06", "Subscriber registration with dob " +
                "enhancement with below age with kin details using api");
        /**
         * Create a subscriber user object
         * set date of birth to less than age limit
         * create a kin user object and assign it to the subscriber
         * hhit the API, registration should be successful
         */
        User subsUser_06 = new User(Constants.SUBSCRIBER);
        subsUser_06.setDateOfBirth(new DateAndTime().getDate(-((365 * agelimit) - 5)));//TODO caliculate leaf years
        KinUser neKinUser = new KinUser();
        subsUser_06.setKinUser(neKinUser);

        //self registering subscriber with kin details
        Transactions.init(t6).startNegativeTest()
                .selfRegistrationForSubscriberWithKinDetails(subsUser_06)
                .assertStatus("770155");
        /*because of functionality change checking KIn details are not manadatory even age is less than age limit
        result.assertStatus(Constants.TXN_SUCCESS, t6);
        MobiquityGUIQueries m = new MobiquityGUIQueries();
        Assertion.assertEqual(m.checkMsisdnExistMobiquity(subsUser_06.MSISDN),true,"Verify Subscriber registered in db",t6);*/

    }
}
