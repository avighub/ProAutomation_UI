package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Automation team.
 * This is mandatory script and must be executed before running saving club scripts
 */
public class MON_3440_SavingClubSetPreference extends TestInit {

    @Test(priority = 1)
    public void preRequisite_SVC() throws Exception {
        String bankId = DataFactory.getBankId(GlobalData.defaultBankName);
       /*
        Set SVC Preferences as prerequisite for Savings Club Management
         */
        if (ConfigInput.isCoreRelease) {
            ExtentTest s1 = pNode.createNode("MON-3440-01",
                    "Set Wallet Preference Based on the AppConfig.isBankMandatoryForSavingsClub")
                    .assignCategory(FunctionalTag.SAVING_CLUB);

            // Update the System Preference CLUB_BANK_ID
            if (!(AppConfig.clubBankId.contains(bankId))) {
                SystemPreferenceManagement.init(s1).updateSystemPreference("CLUB_BANK_ID", bankId);
            }

            User subs = new User(Constants.SUBSCRIBER);
            // Map Wallet preference for Saving club
            CurrencyProviderMapping.init(s1)
                    .mapSVCWalletPreference(subs, bankId);


        } else {
            ExtentTest s1 = pNode.createNode("MON-3440-01", "Set SVC Preferences update SVC Grade Preferences Table {DB}");
            s1.assignCategory(FunctionalTag.SAVING_CLUB);
            User subsChairmanUI = new User(Constants.SUBSCRIBER);
            SavingsClubManagement.init(s1)
                    .setSVCPreferencePrerequisite(subsChairmanUI, DataFactory.getDefaultBankIdForDefaultProvider());
        }

    }
}
