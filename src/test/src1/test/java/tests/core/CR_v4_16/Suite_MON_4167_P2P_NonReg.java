package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.jigsaw.CommonOperations;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.JsonPathOperation.*;
import static framework.util.jigsaw.CommonOperations.getBalanceForOperatorUsersBasedOnUserId;
import static framework.util.jigsaw.CommonOperations.setUMSProperties;
import static framework.util.jigsaw.JigsawOperations.*;
import static framework.util.jigsaw.ServiceRequestContracts.serviceRequestDetails;
import static org.hamcrest.Matchers.is;

public class Suite_MON_4167_P2P_NonReg extends TestInit {
    private User subscriber, new_subs, subsDeleteInitiator, agentPartner;
    private String serviceCode;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest tSetup = pNode.createNode("Setup", "Setup specific to test P2P Non registration MON 4167. " +
                "Make sure that Service charge P2PNONREG is configured. " +
                "Make sure taht the Service charge COUTBYCODE is configured. " +
                "Delete all existing pricing policies to avoid ambiguity. " +
                "Update System preference DOMESTIC_REMIT_WALLET_PAYID.");
        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        new_subs = new User(Constants.SUBSCRIBER);
        agentPartner = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        subsDeleteInitiator = DataFactory.getChannelUserWithAccess("SUBSDEL", 1);

        OperatorUser operator = new OperatorUser(Constants.OPERATOR); // for creating NFSC

        //Make sure agent partner involved in test case Has Enough Balance
        TransactionManagement.init(tSetup)
                .makeSureLeafUserHasBalance(subscriber);

        //P2PNONREG transafer rule validation
        ServiceCharge tRule = new ServiceCharge(Services.P2PNONREG, subscriber, operator, null, null, null, null);
        TransferRuleManagement.init(tSetup)
                .configureTransferRule(tRule);

        //COUTBYCODE transafer rule validation
        tRule = new ServiceCharge(Services.COUTBYCODE, operator, agentPartner, null, null, null, null);
        TransferRuleManagement.init(tSetup)
                .configureTransferRule(tRule);

        CommonOperations.deleteAllPricingPolicies();

        // Set the Preference Value to 'Y'
        MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("DOMESTIC_REMIT_WALLET_PAYID", "Y");
        SystemPreferenceManagement.init(tSetup)
                .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", "12");

        //ALLOW UNKNOWN PARTY IN THE NETWORK
        setUMSProperties(of(
                "ignoreUnknownPartyNetwork", true
        ));


    }

    @AfterClass(alwaysRun = true)
    public void afterEnd() throws Exception {
        //DIS-ALLOW UNKNOWN PARTY IN THE NETWORK
        setUMSProperties(of(
                "ignoreUnknownPartyNetwork", false
        ));

    }

    @Test(priority = 1)
    public void MON_4167_TEST1() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_4167_TEST1",
                " To verify that P2P transfer to an unregistered user should be successful and when the user is registered in the system his account should be" +
                        " credited with the same amount. Also within this process IND02 wallet should be synchronous and agent cash out should give an error");

        try {
            BigDecimal txnAmt = new BigDecimal(2);

            //check the balance in IND02 wallet
            UsrBalance initialBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");

            //perform a p2p non reg transaction
            SfmResponse response = Transactions.init(t1)
                    .p2pNonRegTransaction(subscriber, new_subs, txnAmt);

            String passCode = response.getSerialNumber();

            //check the balance in IND02 wallet
            UsrBalance postP2PBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");

            //assert that IND02 wallet is credited
            BigDecimal amount = initialBalanceIND02.Balance.subtract(postP2PBalanceIND02.Balance);
            Assertion.verifyAccountIsCredited(initialBalanceIND02.Balance, postP2PBalanceIND02.Balance, amount, "verify that IND02 wallet is credited", t1);
            //Assertion.verifyEqual(initialBalanceIND02.Balance, postP2PBalanceIND02.Balance.subtract(txnAmt), "verify that IND02 wallet is credited", t1);

            //create a subscriber
            SubscriberManagement.init(t1)
                    .createDefaultSubscriberUsingAPI(new_subs);

            //check the balance in IND02 wallet
            UsrBalance postUserCreationBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");

            //assert that IND02 wallet is debited i.e. same as initial value
            Assertion.verifyEqual(initialBalanceIND02.Balance, postUserCreationBalanceIND02.Balance, "verify that IND02 wallet balance is debited", t1);

            UsrBalance subsBalance = MobiquityGUIQueries.getUserBalance(new_subs, null, null);

            //assert that subscriber balace is same as txnAmount
            Assertion.verifyEqual(subsBalance.Balance, txnAmt, "verify that subscriber balance is same as txnAmount", t1);

            // TODO move this to Transaction and add Message /Status Validation
            //perform agent cash out using passcode it should give an error
            ValidatableResponse response2 = performServiceRequestAndWaitForFailedForSyncAPI("COUTBYCODE",
                    set("transactor.idValue", agentPartner.MSISDN),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.productId", Constants.NORMAL_WALLET), // TODO use DataFactory to get Default wallet ID
                    delete("receiver"),
                    delete("depositor"),
                    set("initiator", "transactor"),
                    set("transactionAmount", txnAmt),
                    put("$", "withdrawer", serviceRequestDetails(
                            set("idType", "mobileNumber"),
                            set("idValue", new_subs.MSISDN),
                            set("tpin", ConfigInput.tPin),
                            set("mpin", ConfigInput.mPin),
                            set("productId", Constants.NORMAL_WALLET),
                            set("identificationNo", new_subs.ExternalCode),
                            delete("productId"),
                            delete("identificationNo"),
                            delete("password"),
                            delete("txnPassword"),
                            delete("encryptedPassword"),
                            delete("employeeId"),
                            set("passcode", passCode)
                    ))
            );
            SfmResponse res = new SfmResponse(response2, t1);
            res.verifyMessage("unknown.notPresent");

            //validateHasErrorCode(response2, "unknown.notPresent");
            // TODO

            //this cash in is required as there may be service charge defined on subscriber deletion
            Login.init(t1).login(agentPartner);

            TransactionManagement.init(t1)
                    .performCashIn(new_subs, "5", null);

        /*
        TearDown delete the User
         */
            SubscriberManagement.init(t1)
                    .initiateDeleteSubscriber(new_subs, subsDeleteInitiator, DataFactory.getDefaultProvider().ProviderId);

            Assertion.verifyActionMessageContain("account.close.success", "subscriber deleted", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2)
    public void MON_4167_TEST2() throws Exception {
        ExtentTest t2 = pNode.createNode("MON_4167_TEST2", " To verify that P2P transfer to an unregistered user should be successful and the user should be able" +
                " to perform agent cash-out. Also within this process IND02 wallet should be synchronous and on the creation of " +
                "subscriber his wallet balance should be 0");


        try {
            BigDecimal txnAmt = new BigDecimal(2);

            new_subs = new User(Constants.SUBSCRIBER);

            //check the balance in IND02 wallet
            UsrBalance initialBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");

            //perform a p2p non reg transaction
            SfmResponse response = Transactions.init(t2)
                    .p2pNonRegTransaction(subscriber, new_subs, txnAmt);

            String passCode = response.getSerialNumber();

            //check the balance in IND02 wallet
            UsrBalance postP2PBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");

            //assert that IND02 wallet is credited
            BigDecimal amount = initialBalanceIND02.Balance.subtract(postP2PBalanceIND02.Balance);
            Assertion.verifyAccountIsCredited(initialBalanceIND02.Balance, postP2PBalanceIND02.Balance, amount, "verify that IND02 wallet is credited", t2);
            //Assertion.verifyEqual(initialBalanceIND02.Balance, postP2PBalanceIND02.Balance.subtract(BigDecimal.valueOf(2)), "verify that IND02 wallet is credited", t2);

            //perform a agent cashout
            ValidatableResponse response2 = performServiceRequestAndWaitForSuccessForSyncAPI("COUTBYCODE",
                    set("transactor.idValue", agentPartner.MSISDN),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.productId", "12"),
                    delete("receiver"),
                    delete("depositor"),
                    set("initiator", "transactor"),
                    set("transactionAmount", txnAmt),
                    put("$", "withdrawer", serviceRequestDetails(
                            set("idType", "mobileNumber"),
                            set("idValue", new_subs.MSISDN),
                            set("tpin", ConfigInput.tPin),
                            set("mpin", ConfigInput.mPin),
                            set("productId", "12"),
                            set("identificationNo", new_subs.ExternalCode),
                            delete("productId"),
                            delete("identificationNo"),
                            delete("password"),
                            delete("txnPassword"),
                            delete("encryptedPassword"),
                            delete("employeeId"),
                            set("passcode", passCode)
                    ))
            ).body("txnStatus", is("TI"));

            JsonPath responsePath2 = response2.extract().jsonPath();

            String cashOutServiceRequestId = responsePath2.getString("serviceRequestId");

            ValidatableResponse response3 = performServiceRequestResumeAndWaitForSuccessForSyncAPI("COUTBYCODE",
                    set("resumeServiceRequestId", cashOutServiceRequestId),
                    set("bearerCode", "IVR"),
                    set("firstTransactionId", response.TransactionId),
                    set("party.idValue", agentPartner.MSISDN),
                    set("party.tpin", ConfigInput.tPin),
                    set("party.mpin", ConfigInput.mPin)
            ).body("txnStatus", is("TS"));


            //check the balance in IND02 wallet
            UsrBalance postUserCreationBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");

            //assert that IND02 wallet is debited i.e. same as initial value
            Assertion.verifyEqual(initialBalanceIND02.Balance, postUserCreationBalanceIND02.Balance, "verify that IND02 wallet is debited i.e. same as initial value", t2);

            //create a subscriber
            SubscriberManagement.init(t2)
                    .createSubscriberDefaultMapping(new_subs, true, false);


            UsrBalance subsBalance = MobiquityGUIQueries.getUserBalance(new_subs, null, null);

            //assert that subscriber balace is 0
            Assertion.verifyEqual(subsBalance.Balance, new BigDecimal(0), "verify that subscriber balace is 0", t2);

            //this cash in is required as there may be service charge defined on subscriber deletion
            Login.init(t2).login(agentPartner);

            TransactionManagement.init(t2)
                    .performCashIn(new_subs, "5", null);

            //delete  the subcriber
            SubscriberManagement.init(t2)
                    .initiateDeleteSubscriber(new_subs, subsDeleteInitiator, DataFactory.getDefaultProvider().ProviderId);
            Assertion.verifyActionMessageContain("account.close.success", "subscriber deleted", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();

    }

}
