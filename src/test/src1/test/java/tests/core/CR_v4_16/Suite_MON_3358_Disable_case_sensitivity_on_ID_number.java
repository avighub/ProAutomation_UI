package tests.core.CR_v4_16;


import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by ravindra.dumpa on 11/15/2017.
 */
public class Suite_MON_3358_Disable_case_sensitivity_on_ID_number extends TestInit {

    private static String domain, category, regType, provider, grade, wallet;
    private OperatorUser usrCanAddChUser, usrCanAddBulkUser, o2cInitiator;
    private User usrCanAddsubs, chUser, subs, subsDeleteInitiator, channelUser, subsnew, subs_11;
    private String subBatchId, batchId;
    private User subsUser_01;

    /**
     * Changing the FREQ_EXT_CODE_N_SUBS preference value to 1
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {

        ExtentTest tSetup = pNode.createNode("Setup", "Get users fro the test. " +
                "Update system preference FREQ_EXT_CODE_N_SUBS. " +
                "Map primary wallet preference for SUBS category, required when creating leaf user via API. " +
                "Make Sure Acquisition free Service charge is configured, from SUBS to OPT");
        /**
         * Change the system preference FREQ_EXT_CODE_N_SUBS to 1
         */
        try {
            usrCanAddChUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            usrCanAddsubs = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.WHOLESALER);
            usrCanAddBulkUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            subsDeleteInitiator = DataFactory.getChannelUserWithAccess("SUBSDEL");
            channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            subsnew = new User(Constants.SUBSCRIBER);
            subs_11 = new User(Constants.SUBSCRIBER);

            OperatorUser optuser = DataFactory.getOperatorUserWithAccess("PREF001");
            Login.init(tSetup).login(optuser);

            MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("FREQ_EXT_CODE_N_SUBS", "Y");
            SystemPreferenceManagement.init(tSetup)
                    .updateSystemPreference("FREQ_EXT_CODE_N_SUBS", "1");

            CurrencyProviderMapping.init(tSetup)
                    .mapPrimaryWalletPreference(subsnew);

            ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subsnew, new OperatorUser(Constants.OPERATOR), null, null, null, null);

            // Configure the Service Charge
            ServiceChargeManagement.init(tSetup)
                    .configureServiceCharge(sCharge);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }

    }

    /**
     * Creating channel user 1
     *
     * @throws Exception
     */

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01", "Creating channel user with alpha numeric external code");
        /**
         * create channel user
         */
        // Create Channel user with alpha numeric external code
        chUser = new User(Constants.WHOLESALER);
        chUser.setExternalCode(chUser.MSISDN + "ABCD");
        ChannelUserManagement.init(t1).createChannelUser(chUser);
    }

    /**
     * Creating channel user with same external code(Identification number) of channel user 1 to check case sensitivity
     *
     * @throws Exception
     */
    @Test(priority = 2, dependsOnMethods = "Test_01")
    public void Test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test_02", "Verifying case sensitivity on ID num while Channel User registration");
        /**
         * login as operator user
         * add channel user with existed Id with case sensitivity
         * verify the error message
         */
        //Login as operator user

        try {
            Login.init(t2)
                    .login(usrCanAddChUser);
            //Create channel user with case sensitive external code of channel user 1
            User chUser1 = new User(Constants.WHOLESALER);
            chUser1.setExternalCode(chUser.ExternalCode.toLowerCase());

            //initiate channel user
            ConfigInput.isAssert = false;
            ChannelUserManagement.init(t2).initiateChannelUser(chUser1);


            //verify error message
            Assertion.verifyEqual(Assertion.isErrorInPage(t2), true,
                    "Verify subscriber with ID num which already existed! ", t2);
            ConfigInput.isAssert = true;
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Creating Subscriber 1
     *
     * @throws Exception
     */

    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_03", "Creating subscriber with alpha numeric external code");
        /*
         * Create subscriber
         */

        try {
            // Create Channel user with alpha numeric external code
            subs = new User(Constants.SUBSCRIBER);
            subs.setExternalCode(subs.MSISDN + "ABCD");
            SubscriberManagement.init(t3).createSubscriberDefaultMapping(subs, true, false);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Creating subscriber with same external code(Identification number) of subscriber 1 to check case sensitivity
     *
     * @throws Exception
     */

    @Test(priority = 4, dependsOnMethods = "Test_03")
    public void Test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_04", "Verifying case sensitivity on ID num while sub registration");
//        /**
//         * login as channel user
//         * add subscriber with existed Id with case sensitivity
//         * verify the error message
//         */

        try {
            //login as channel user
            Login.init(t4)
                    .login(usrCanAddsubs);
            //Create subscriber with case sensitive external code of channel user 1
            User subs1 = new User(Constants.SUBSCRIBER);
            subs1.setExternalCode(subs.ExternalCode.toLowerCase());

            //initiate subscriber
            SubscriberManagement.init(t4).addInitiateSubscriber(subs1);

            //verify Error message
            Assertion.verifyEqual(Assertion.isErrorInPage(t4), true,
                    "Verify subscriber with ID num which already existed! ", t4);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verifying case sensivity on external code(Identification number) while bulk channel registration
     *
     * @throws Exception
     */

    @Test(priority = 5)
    public void Test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("Test_05", "Verifying case sensitivity on ID number while Bulk Channel User Registration");
        /*
         * Login as Operator user
         * Create Subscriber Object
         * Create one Subscriber object with cas sensitivity of above subscriber object
         * Goto bulk channel and subscriber registration page
         * Select the service
         * write the data in csv file
         * Upload csv file
         * go to batch page and verify logs
         */
        try {
            //login as operator user
            Login.init(t5).login(usrCanAddBulkUser);

            //create user wholesaler object with alphabet external code
            User user = new User(Constants.WHOLESALER);
            user.setExternalCode(user.MSISDN + "ABCD");

            //create one more wholesaler object with case sensitive external code of above wholesaler
            User user1 = new User(Constants.WHOLESALER);
            user1.setExternalCode(user.ExternalCode.toLowerCase());

            //go to bulk ch user and sub registration
            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t5);
            page.navBulkChUserAndSubsciberRegistrationAndModification();

            //select the service and fill details in csv
            page.SelectChannelUserRegistrationService();
            ChannelUserManagement.init(t5)
                    .writeBulkChUserRegistrationDetailsInCSV(FilePath.fileBulkChUserReg,
                            user,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            true,
                            false)
                    .writeBulkChUserRegistrationDetailsInCSV(FilePath.fileBulkChUserReg,
                            user1,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            true,
                            true);

            //upload and submit the file
            page.uploadFile(FilePath.fileBulkChUserReg);
            page.submitCsv();

            //get the batch and goto batches page
            String batchId_05 = page.getBatchId();
            page.navBulkRegistrationAndModificationMyBatchesPage();
            page.ClickOnBatchSubmit();

            //verify the logs
            BulkChUserAndSubRegAndModPage.init(t5)
                    .downloadLogFileUsingBatchId(batchId_05)
                    //.verifyErrorMessageInLogFile(user1.ExternalCode, batchId_05, t5);
                    .verifyErrorMessageInLogFile("bulk.external.duplicate.msg", batchId_05, t5, user.ExternalCode);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verifying case sensivity on external code(Identification number) while bulk subscriber registration
     *
     * @throws Exception
     */

    @Test(priority = 6)
    public void Test_06() throws Exception {
        ExtentTest t6 = pNode.createNode("Test_06", "Verifying case sensitivity on ID number while Bulk Subscriber Registration");
        /**
         * Login as Operator user
         * Create Subscriber Object
         * Create one subscriber with external code with case sensitive external code other subscriber
         * Change some fields as empty
         * Select the service
         * write the data in csv file
         * Upload csv file
         * Go to My batches page and verify log file
         */
        try {
            //login as operator user
            Login.init(t6).login(usrCanAddBulkUser);

            //create subscriber object with alphabet external code
            User user = new User(Constants.SUBSCRIBER);
            user.setExternalCode(user.MSISDN + "ABCD");

            //create one more subscriber object with case sensitive external code of above subscriber
            User user1 = new User(Constants.SUBSCRIBER);
            user1.setExternalCode(user.ExternalCode.toLowerCase());

            //go to bulk ch user and sub registration
            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t6);
            page.navBulkChUserAndSubsciberRegistrationAndModification();

            //select the service and fill details in csv
            page.SelectSubscriberRegistrationService();

            SubscriberManagement.init(t6)
                    .writeDataToBulkSubsRegistrationCsv(user,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            false)
                    .writeDataToBulkSubsRegistrationCsv(user1,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            true);

            //upload and submit the file
            page.uploadFile(FilePath.fileBulkSubReg);
            page.submitCsv();

            //get the batch and goto batches page
            String subBatchId = page.getBatchId();
            page.navBulkRegistrationAndModificationMyBatchesPage();
            page.ClickOnBatchSubmit();

            //verify the logs
            BulkChUserAndSubRegAndModPage.init(t6)
                    .downloadLogFileUsingBatchId(subBatchId)
                    .verifyErrorMessageInLogFile("bulk.external.error.msg", subBatchId, t6, user.ExternalCode);
        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verifying case sensivity on external code(Identification number) while self subscriber registration
     *
     * @throws Exception
     */
    @Test(priority = 7)
    public void Test_07() throws Exception {
        ExtentTest t7 = pNode.createNode("Test_07", "Verifying case sensitivity on ID number while Self Subscriber Registration through USSD");
        /**
         * Create a subscriber user object with alpha numeric external code
         * Create one more subscriber with case sensitive external code of other subscriber
         * Register subscriber by self
         * hit the API, 1st sub registration should be successful
         * 2nd subscriber should be un successful
         */
        try {
            //create subscriber object with alphabet external code
            User subsUser_07 = new User(Constants.SUBSCRIBER);
            subsUser_07.setExternalCode(subsUser_07.MSISDN + "ABCD");

            //self registering subscriber with kin details
            TxnResponse result = Transactions.init(t7)
                    .selfRegistrationForSubscriberWithKinDetails(subsUser_07)
                    //Verifying status of the subscriber registration
                    .assertStatus(Constants.TXN_SUCCESS);

            //verifying msisdn in db
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            Assertion.assertEqual(MobiquityGUIQueries.checkMsisdnExistMobiquity(subsUser_07.MSISDN), true, "Verify Subscriber registered in db", t7);

            //create one more subscriber object with case sensitive external code of above subscriber
            User subsUser1_07 = new User(Constants.SUBSCRIBER);
            subsUser1_07.setExternalCode(subsUser_07.ExternalCode.toLowerCase());

            //self registering subscriber
            ConfigInput.isAssert = false;
            TxnResponse result1 = Transactions.init(t7)
                    .selfRegistrationForSubscriberWithKinDetails(subsUser1_07)
                    //Verifying status of the subscriber registration and verifying message as well
                    .assertStatus("90004")
                    .assertMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, t7);
        }
        ConfigInput.isAssert = true;
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verifying case sensitivity on external code(Identification number) while subscriber registration by As400
     *
     * @throws Exception
     */
    @Test(priority = 8)
    public void Test_08() throws Exception {
        ExtentTest t8 = pNode.createNode("Test_08", "Verifying case sensitivity on external code  while Customer Registration from AS400 ");
        try {
            /**
             * Create a subscriber user object with alpha numeric external code
             * Create one more subscriber with case sensitive external code of other subscriber
             * Register subscriber by AS400
             * hit the API, 1st sub registration should be successful
             * 2nd subscriber should be un successful
             */
            //create subscriber object with alphabet external code
            User subsUser_08 = new User(Constants.SUBSCRIBER);
            subsUser_08.setExternalCode(subsUser_08.MSISDN + "ABCD");

            //subscriber registration by As400
            TxnResponse result = Transactions.init(t8)
                    .subRegistrationByAS400(subsUser_08)
                    //Verifying status of the subscriber registration
                    .assertStatus(Constants.TXN_SUCCESS);

            //create one more subscriber object with case sensitive external code of above subscriber
            User subsUser1_8 = new User(Constants.SUBSCRIBER);
            subsUser1_8.setExternalCode(subsUser_08.ExternalCode.toLowerCase());

            //subscriber registration by AS400
            ConfigInput.isAssert = false;
            TxnResponse result1 = Transactions.init(t8)
                    .subRegistrationByAS400(subsUser1_8)
                    //Verifying status of the subscriber registration and verifying message as well
                    .assertStatus("90004")
                    .assertMessage("duplicate.externalcode");

            ConfigInput.isAssert = false;

        } catch (Exception e) {
            markTestAsFailure(e, t8);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verifying case sensivity on external code(Identification number) while subscriber registration by Channel user
     *
     * @throws Exception
     */
    @Test(priority = 9)
    public void Test_09() throws Exception {
        /**
         * Create a subscriber user object with alpha numeric external code
         * Create one more subscriber with case sensitive external code of other subscriber
         * Register subscriber by Channel user
         * hit the API, 1st sub registration should be successful
         * 2nd subscriber should be un successful
         */

        ExtentTest t9 = pNode.createNode("Test_09", "case sensitivity on ID number while sub registration by channel user  ");
        try {
            //create subscriber object with alphanumeric external code
            User subsUser_09 = new User(Constants.SUBSCRIBER);
            subsUser_09.setExternalCode(subsUser_09.MSISDN + "ABCD");

            //subscriber registration by Channel user
            TxnResponse result = Transactions.init(t9)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subsUser_09)
                    //Verifying status of the subscriber registration
                    .assertStatus(Constants.TXN_SUCCESS);

            //create one more subscriber object with case sensitive external code of above subscriber
            User subsUser1_09 = new User(Constants.SUBSCRIBER);
            subsUser1_09.setExternalCode(subsUser_09.ExternalCode.toLowerCase());

            //subscriber registration By Channel user
            startNegativeTest();
            TxnResponse result1 = Transactions.init(t9)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subsUser1_09)
                    //Verifying status of the subscriber registration and verifying message as well
                    .assertStatus("90004")
                    .assertMessage("duplicate.externalcode");
        } catch (Exception e) {
            markTestAsFailure(e, t9);
        }

        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verifying case sensivity on external code(Identification number) while sub registration with External code of Churned ch user
     *
     * @throws Exception
     */
    @Test(priority = 10)
    public void Test_10() throws Exception {
        ExtentTest t10 = pNode.createNode("Test_10", "case sensitivity on ID number while sub registration with churned user ID  ");
        /**
         * Create a Subscriber object with alpha numeric external code
         * Create one more Subscriber with case sensitive external code of other deleted Subscriber
         * Verify the Error message
         */
        try {
            //create Subscriber object with alphabet external code
            User subs_10 = CommonUserManagement.init(t10).getChurnedUser(Constants.SUBSCRIBER, null);

            //Create Subscriber with case sensitive external code of deleted Subscriber
            User subs1 = new User(Constants.SUBSCRIBER);
            subs1.setExternalCode(subs_10.ExternalCode);

            //try to create subscriber
            SubscriberManagement.init(t10).createSubscriber(subs1);

            //verify there is no Error message
            Assertion.verifyEqual(Assertion.isErrorInPage(t10), false,
                    "Verify subscriber with ID num which already churned user ID! ", t10);
        } catch (Exception e) {
            markTestAsFailure(e, t10);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verifying case sensivity on external code(Identification number) while sub registration with External code of deleted sub
     *
     * @throws Exception
     */
    @Test(priority = 11)
    public void Test_11() throws Exception {
        String defaultProvider = DataFactory.getDefaultProvider().ProviderName;

        ExtentTest t11 = pNode.createNode("Test_11", "case sensitivity on ID number while sub registration with deleted user ID ");
        /**
         * Create a Subscriber object with alpha numeric external code
         * Create one more Subscriber with case sensitive external code of other deleted Subscriber
         * Verify the there is no Error message
         */

        try {
            //create Subscriber object with alphabet external code
            subs_11.setExternalCode(subs_11.MSISDN + "ABCD");

            //create subscriber
            SubscriberManagement.init(t11).createDefaultSubscriberUsingAPI(subs_11);

            Login.init(t11).login(channelUser);

            Login.init(pNode).login(o2cInitiator);

            TransactionManagement.init(t11).initiateAndApproveO2CWithProvider(channelUser, defaultProvider, "200");

            Login.init(t11).login(channelUser);

            //perform cash in to subscriber 01
            TransactionManagement.init(t11).performCashIn(subs_11, "10", defaultProvider);

            //delete subcriber by agent
            SubscriberManagement.init(t11)
                    .initiateDeleteSubscriber(subs_11, subsDeleteInitiator, DataFactory.getDefaultProvider().ProviderId);
            //Create Subscriber with case sensitive external code of deleted Subscriber

            subsnew.setExternalCode(subs_11.ExternalCode);

            //try to create subscriber
            SubscriberManagement.init(t11).createSubscriberDefaultMapping(subsnew, true, false); // todo
            //Verify Error message
            Assertion.verifyEqual(Assertion.isErrorInPage(t11), false,
                    "Verify subscriber creation with delete state subs external code ", t11);
        } catch (Exception e) {
            markTestAsFailure(e, t11);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Changing the FREQ_EXT_CODE_N_SUBS preference value to 2
     *
     * @throws Exception
     */

    @AfterClass(alwaysRun = true)
    public void Test_12() throws Exception {
        ExtentTest f1 = pNode.createNode("Test_12", "Updating the preference to default value");
//        /**
//         * Change the system preference FREQ_EXT_CODE_N_SUBS to 2
//         */
        MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("FREQ_EXT_CODE_N_SUBS", "Y");
        SystemPreferenceManagement.init(f1)
                .updateSystemPreference("FREQ_EXT_CODE_N_SUBS", "2");

    }

}
