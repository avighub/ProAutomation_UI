/*
package tests.core.changeRequestAutomation_v5;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.SystemBankAccount;
import framework.entity.User;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.apiManagement.Transactions;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.jigsaw.commonOperations;
import org.junit.Ignore;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.jigsaw.commonOperations.getBalanceForOperatorUsersBasedOnUserId;

@Ignore("BUG: MON-4168")
public class Suite_MON_2246_DebitServiceCharge extends TestInit {

    private SystemBankAccount receiverWallet;
    private String senderWallet;
    private User channelUserWHS, subs;
    private int txnAmt = 10;
    private int serviceChargeFixAmt = 10;

    */
/**
 * Setup
 *
 * @throws Exception
 *//*

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest setup = pNode.createNode("Setup Specific for this Test");
        CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();

        if (defaultProvider != null) {
            senderWallet = "IND03";
            receiverWallet = new SystemBankAccount(defaultProvider.BankName, "IND04");
        }

        channelUserWHS = DataFactory.getChannelUserList(Constants.WHOLESALER).get(0);
        subs = DataFactory.getChannelUserList(Constants.SUBSCRIBER).get(0);

        // Delete all pricing policy Service Charge
        CommonOperations.deleteAllPricingPolicies();

        // Make sure Users have enough Balance
        TransactionManagement.init(setup)
                .makeSureChannelUserHasBalance(channelUserWHS);
    }

    @Test(priority = 1)
    public void downloadAmbiguousTransactionFileAndUploadAsSuccess() throws Exception {
        ExtentTest node = pNode.createNode("TEST 08", "To verify that when transaction is ambiguous admin should be able to download" +
                " the ambiguous transactions from the UI and in the downloaded file along with transaction ID" +
                " numeric transaction also should be available in the downloaded file");

        UsrBalance initialBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId(senderWallet);
        UsrBalance initialBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);


        */
/*      Cash In to Ensure IND03 has enough balance      *//*

        Transactions transactions = Transactions.init(node);
        transactions.defineServiceCharge(serviceChargeFixAmt, 0);
        transactions.initiateCashIn(subs, channelUserWHS, txnAmt);

        UsrBalance preIND03Balance = getBalanceForOperatorUsersBasedOnUserId(senderWallet);
        Assertion.verifyEqual(preIND03Balance.Balance, initialBalanceIND03.Balance.add(BigDecimal.valueOf(serviceChargeFixAmt)), "Verify Amt credited to IND03 Wallet", node);

        */
/*      Initiate Debit service charge, Transaction Status Should Go to Ambiguous         *//*

        SfmResponse response = transactions
                .performDebitServiceCharge(senderWallet, receiverWallet);


        UsrBalance ambiguousBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId(senderWallet);
        UsrBalance ambiguousBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        */
/*      Verify Balances in Ambiguous State      *//*

        AmbiguousTransactionManagement ambiguousTransactionManagement = AmbiguousTransactionManagement.init(node);
        ambiguousTransactionManagement
                .initiateAmbiguousBalanceValidation(preIND03Balance, initialBalanceIND04, ambiguousBalanceIND03, ambiguousBalanceIND04, preIND03Balance.Balance);

        */
/*
 * Download the ambiguous Transaction File
 *//*

        ambiguousTransactionManagement
                .downloadAmbiguousTxSheet(receiverWallet)
                .verifyAmbiguousTransactionFile(response, senderWallet, receiverWallet.BankId);

        */
/*      Update the Excel and Upload the File      *//*

        ExtentTest node1 = pNode.createNode("TEST 09(a)", "success transactions the FIC amount in the receiver account should be moved to available balance and there should be no reconciliation mismatch");

        */
/*      Set the Transaction as Pass         *//*

        ambiguousTransactionManagement = AmbiguousTransactionManagement.init(node1);
        ambiguousTransactionManagement.setTransactionStatus(response, true)
                .uploadUpatedAmbiguousTxnFile(response, receiverWallet);

        Thread.sleep(180000); // wait for the Ambiguous Transaction to Get effective and processed

        */
/*        Check Balance & Db Status         *//*

        ambiguousTransactionManagement
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

        UsrBalance postBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId(senderWallet);
        UsrBalance postBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        ambiguousTransactionManagement
                .initiatePostProcessBalanceValidation(true, preIND03Balance, initialBalanceIND04, postBalanceIND03, postBalanceIND04, preIND03Balance.Balance);
    }

    @Test(priority = 2)
    public void downloadAmbiguousTransactionFileAndUploadAsFailure() throws Exception {
        ExtentTest node = pNode.createNode("TEST 09(b)", "For failed transactions the FIC in the receiver account should be moved back to sender available balance and there should be no reconciliation mismatch");

        CommonOperations.deleteAllPricingPolicies();

        UsrBalance initialBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId(senderWallet);
        UsrBalance initialBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        */
/*      Cash In to Ensure IND03 has enough balance      *//*

        Transactions transactions = Transactions.init(node);
        transactions.defineServiceCharge(serviceChargeFixAmt, 0);
        transactions.initiateCashIn(subs, channelUserWHS, txnAmt);

        UsrBalance preIND03Balance = getBalanceForOperatorUsersBasedOnUserId(senderWallet);
        Assertion.verifyEqual(preIND03Balance.Balance, initialBalanceIND03.Balance.add(BigDecimal.valueOf(serviceChargeFixAmt)), "Verify Amt credited to IND03 Wallet", node);

        */
/*      Initiate Debit service charge, Transaction Status Should Go to Ambiguous         *//*

        SfmResponse response = transactions
                .performDebitServiceCharge(senderWallet, receiverWallet);


        UsrBalance ambiguousBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId(senderWallet);
        UsrBalance ambiguousBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        */
/*      Verify Balances in Ambiguous State      *//*

        AmbiguousTransactionManagement ambiguousTransactionManagement = AmbiguousTransactionManagement.init(node);
        ambiguousTransactionManagement
                .initiateAmbiguousBalanceValidation(preIND03Balance, initialBalanceIND04, ambiguousBalanceIND03, ambiguousBalanceIND04, preIND03Balance.Balance);

        */
/*
 * Download the ambiguous Transaction File
 *//*

        ambiguousTransactionManagement.downloadAmbiguousTxSheet(receiverWallet)
                .verifyAmbiguousTransactionFile(response, senderWallet, receiverWallet.BankId)
                .setTransactionStatus(response, false)//Set the Transaction as Fail
                .uploadUpatedAmbiguousTxnFile(response, receiverWallet);

        Thread.sleep(180000); // wait for the Ambiguous Transaction to Get effective and processed

        */
/*        Check Balance & Db Status         *//*

        ambiguousTransactionManagement.dbVerifyAmbiguousIsProcessed(response.TransactionId, false);

        UsrBalance postBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId(senderWallet);
        UsrBalance postBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        ambiguousTransactionManagement
                .initiatePostProcessBalanceValidation(false, preIND03Balance, initialBalanceIND04, postBalanceIND03, postBalanceIND04, preIND03Balance.Balance);
    }
}*/
