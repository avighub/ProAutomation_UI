package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.jigsaw.CommonOperations;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by shubham.kumar1
 * <p>
 * Use Case: MON-3279
 * <p>
 * To verify that If transaction is moved to ambiguous the admin should be able to download this ambiguous transaction and upload as:
 * 1. successful transaction after which amount in receiver FIC should be moved to available balance
 * 2. failed transaction after which amount in receiver FIC should be moved back to sender available balance
 **/

public class Suite_MON_3279_AgentAssistedMerchantPayment extends TestInit {
    private User merchantPartner, agentPartner, unregisteredCustomer;
    private Wallet defaultWallet;

    /**
     * Setup
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest testNode = pNode.createNode("Setup", "Make sure Channel User has balance. Configure Transfer rule AGENT_INITIATED_MERCHPAY. Delete all pricing policies");

        try {
            defaultWallet = DataFactory.getDefaultWallet();
            merchantPartner = DataFactory.getChannelUserWithCategory(Constants.HEAD_MERCHANT);
            agentPartner = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            unregisteredCustomer = new User(Constants.SUBSCRIBER);
            //setting the third party response to be one minute
            MobiquityGUIQueries.updateRetryIntervalForThirdParty("1", testNode);

            //Make sure agent partner involved in test case Has Enough Balance
            TransactionManagement.init(testNode)
                    .makeSureChannelUserHasBalance(agentPartner);

            ServiceCharge tRule = new ServiceCharge(Services.AGENT_INITIATED_MERCHPAY, agentPartner, merchantPartner, null, null, null, null);
            TransferRuleManagement.init(testNode)
                    .configureTransferRule(tRule);

            // Delete all pricing policy S Charge
            CommonOperations.deleteAllPricingPolicies();
        } catch (Exception e) {
            markSetupAsFailure(e);
        }

    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TS() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 36", " To verify that If transaction is moved to ambiguous the admin should be able to" +
                " download this ambiguous transaction and upload as successful transaction after which amount in receiver FIC should be moved to available balance");

        try {
            /**
             * Initiate merchant Payment, Transaction Status Should Go to Ambiguous
             */
            UsrBalance agentPreBal = MobiquityGUIQueries.getUserBalance(agentPartner, null, null);
            UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(merchantPartner, null, null);
            BigDecimal amountTx = new BigDecimal(2);

            //doing a transaction for the unregisterd consumer
            SfmResponse response = Transactions.init(t1)
                    .initiateMerchantPayOut(agentPartner, unregisteredCustomer, merchantPartner, amountTx.toString(), defaultWallet.WalletId);

            //checking for the transaction status to be in ambigious state
            ValidatableResponse validatableResponse = response.validatableResponse;
            validatableResponse.assertThat()
                    .body("txnStatus", Matchers.equalTo("TA"));


            /**
             * Check Balance
             */
            UsrBalance agentPostBal = MobiquityGUIQueries.getUserBalance(agentPartner, null, null);
            UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(merchantPartner, null, null);
            AmbiguousTransactionManagement.init(t1).
                    initiateAmbiguousBalanceValidation(agentPreBal, merPreBal, agentPostBal, merPostBal, amountTx);

            AmbiguousTransactionManagement.init(t1)
                    .downloadAmbiguousTxSheet(merchantPartner)
                    .verifyAmbiguousTransactionFile(response, agentPartner.MSISDN, merchantPartner.MSISDN);

            /**
             * Update the Excel and Upload the File
             */
            ExtentTest t2 = pNode.createNode("MON-3279-a", "TEST-AMBIGUOUS_TRANSACTION_SET_STATUS_TS. Success transactions the FIC amount in the receiver account should me moved\n" +
                    " to available balance and there should be no reconciliation mismatch");

            /**
             * Set the Transaction as Pass
             */
            AmbiguousTransactionManagement.init(t2)
                    .setTransactionStatus(response, true)
                    .uploadUpatedAmbiguousTxnFile(response, merchantPartner);

            Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

            /**
             * Check Balance & Db Status
             */
            AmbiguousTransactionManagement.init(t2)
                    .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

            UsrBalance agentPostProcessBal = MobiquityGUIQueries.getUserBalance(agentPartner, null, null);
            UsrBalance merPostProcessBal = MobiquityGUIQueries.getUserBalance(merchantPartner, null, null);
            AmbiguousTransactionManagement.init(t2).
                    initiatePostProcessBalanceValidation(true, agentPreBal, merPreBal, agentPostProcessBal, merPostProcessBal, amountTx);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }


    @Test(priority = 2)
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TF() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 37", " To verify that If transaction is moved to ambiguous the admin should be able to" +
                " download this ambiguous transaction and upload as failed transaction after which amount in receiver FIC should be moved back to sender available balance");

        try {
            /**
             * Initiate merchant Payment, Transaction Status Should Go to Ambiguous
             */
            UsrBalance agentPreBal = MobiquityGUIQueries.getUserBalance(agentPartner, null, null);
            UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(merchantPartner, null, null);
            BigDecimal amountTx = new BigDecimal(2);

            //doing a transaction for the unregisterd consumer
            SfmResponse response = Transactions.init(t1)
                    .initiateMerchantPayOut(agentPartner, unregisteredCustomer, merchantPartner, amountTx.toString(), defaultWallet.WalletId);

            //checking that the transaction status is ambigious after merchant payment
            ValidatableResponse validatableResponse = response.validatableResponse;
            validatableResponse.assertThat()
                    .body("txnStatus", Matchers.equalTo("TA"));

            /**
             * Check Balance
             */
            UsrBalance agentPostBal = MobiquityGUIQueries.getUserBalance(agentPartner, null, null);
            UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(merchantPartner, null, null);
            AmbiguousTransactionManagement.init(t1).
                    initiateAmbiguousBalanceValidation(agentPreBal, merPreBal, agentPostBal, merPostBal, amountTx);

            AmbiguousTransactionManagement.init(t1)
                    .downloadAmbiguousTxSheet(merchantPartner)
                    .verifyAmbiguousTransactionFile(response, agentPartner.MSISDN, merchantPartner.MSISDN);

            /**
             * Update the Excel and Upload the File
             */
            ExtentTest t2 = pNode.createNode("MON-3279-b", "Failed transactions the FIC amount in the receiver account should be moved\n" +
                    " back to sender available balance and there should be no reconciliation mismatch");

            /**
             * Set the Transaction as Fail
             */
            AmbiguousTransactionManagement.init(t2)
                    .setTransactionStatus(response, false)
                    .uploadUpatedAmbiguousTxnFile(response, merchantPartner);

            Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

            /**
             * Check Balance & Db Status
             */
            AmbiguousTransactionManagement.init(t2)
                    .dbVerifyAmbiguousIsProcessed(response.TransactionId, false);

            UsrBalance agentPostProcessBal = MobiquityGUIQueries.getUserBalance(agentPartner, null, null);
            UsrBalance merPostProcessBal = MobiquityGUIQueries.getUserBalance(merchantPartner, null, null);
            AmbiguousTransactionManagement.init(t2).
                    initiatePostProcessBalanceValidation(false, agentPreBal, merPreBal, agentPostProcessBal, merPostProcessBal, amountTx);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

}