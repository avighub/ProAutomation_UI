package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.TxnResponse;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Create two SavingClub
 * Create a Subscriber and Join the same in both the Clubs
 * run Generic API to list of clubs associated with a subscriber-
 */
public class Suite_MON_3441_SavingClubGenericApi extends TestInit {

    private User subsMember1, subsChairman1, subsChairman2;
    private SavingsClub sClub_3441_01, sClub_3441_02;
    private CurrencyProvider defaultProvider;
    private Wallet defaultWallet;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        // Extent Test
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific for Testing Saving Club Generic API." +
                "Create Two Saving club and assign appropriate number of approvers and memebers.")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            BigDecimal minCahIn = AppConfig.minSVCDepositAmount.add(new BigDecimal(25));

            // get Default Provider
            defaultProvider = DataFactory.getDefaultProvider();
            defaultWallet = DataFactory.getDefaultWallet();
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);

            // User Objects and initializing
            subsChairman1 = new User(Constants.SUBSCRIBER);
            subsChairman2 = new User(Constants.SUBSCRIBER);
            subsMember1 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(s1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsChairman1, defaultWallet.WalletId, true);
            SubscriberManagement.init(s1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsChairman2, defaultWallet.WalletId, true);
            SubscriberManagement.init(s1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subsMember1, defaultWallet.WalletId, true);


            TransactionManagement.init(s1)
                    .makeSureLeafUserHasBalance(subsChairman1, minCahIn)
                    .makeSureLeafUserHasBalance(subsChairman2, minCahIn)
                    .makeSureLeafUserHasBalance(subsMember1, minCahIn);

            // Map SVC wallet Preference
            Preferences.init(s1)
                    .setSVCPreferences(subsChairman1, bankId);

            // Savings Club object
            sClub_3441_01 = new SavingsClub(subsChairman1, Constants.CLUB_TYPE_PREMIUM, defaultProvider.Bank.BankID, false, false);
            sClub_3441_02 = new SavingsClub(subsChairman2, Constants.CLUB_TYPE_PREMIUM, defaultProvider.Bank.BankID, false, false);

            /*
            Set the Member and Approver Count for both the savings Club
            As the Min member count for Club1 is 1, after the SubsMember1 join the status of club shud be Active
            Club2 required minimum 3 members to be active, to test if Club is not active then i must now show
            for the member in test 01
             */
            sClub_3441_01.setMemberCount(2, 3);
            sClub_3441_01.setApproverCount(2, 3);

            sClub_3441_02.setMemberCount(2, 3);
            sClub_3441_02.setApproverCount(2, 3);

            // Add Member to the Club Object
            sClub_3441_01.addMember(subsMember1);
            sClub_3441_02.addMember(subsMember1);

            // Create Saving Club
            SavingsClubManagement.init(s1)
                    .createSavingsClub(sClub_3441_01)
                    .createSavingsClub(sClub_3441_02);

            // Join Subscriber to Club1
            Transactions.init(s1)
                    .joinOrResignSavingClub(sClub_3441_01, subsMember1, true)
                    .verifyStatus(Constants.TXN_SUCCESS);

            // join subscriber to Club2
            TxnResponse response2 = Transactions.init(s1)
                    .joinOrResignSavingClub(sClub_3441_02, subsMember1, true)
                    .verifyStatus(Constants.TXN_SUCCESS);


            Transactions.init(s1)
                    .depositSavingClub(sClub_3441_01, subsMember1, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS);
            Transactions.init(s1)
                    .depositSavingClub(sClub_3441_01, subsChairman1, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS);
//            Transactions.init(s1)
//                    .depositSavingClub(sClub_3441_01, subsChairman2, AppConfig.minSVCDepositAmount.toString())
//                    .verifyStatus(Constants.TXN_SUCCESS);


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test 01
     * Get SVC List linked to a msisdn
     */
    @Test(priority = 1)
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test 01", "Get SVC List linked to a msisdn")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        TxnResponse response = Transactions.init(t1)
                .getSVCList(subsMember1.MSISDN, defaultProvider.ProviderId)
                .verifyStatus(Constants.TXN_SUCCESS);

        // verify that the Club 1 is shown in the Message Response
        Assertion.verifyContains(response.getMessage(), sClub_3441_01.ClubId, "Verify Active Club Id is Shown", t1, false);
        Assertion.verifyContains(response.getMessage(), sClub_3441_01.ClubName, "Verify Active Club name is Shown", t1, false);

        // Verify Club 2 Details are not shown as it is still Inactive
        Assertion.verifyContains(response.getMessage(), sClub_3441_02.ClubId, "Verify Inactive Club Id is Also Shown", t1, false);
        Assertion.verifyContains(response.getMessage(), sClub_3441_02.ClubName, "Verify Inactive Club name is Also Shown", t1, false);

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Make the second Club as Active and get the SVC list linked with the Member
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test 02", "Get SVC List linked to a msisdn")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        // join another user and make the Club active
        User member2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
        Transactions.init(t2)
                .joinOrResignSavingClub(sClub_3441_02, member2, true)
                .verifyStatus(Constants.TXN_SUCCESS);

        // get the SVC list linked to the member
        TxnResponse response = Transactions.init(t2)
                .getSVCList(subsMember1.MSISDN, defaultProvider.ProviderId)
                .verifyStatus(Constants.TXN_SUCCESS);

        // verify that the Club 1 is shown in the Message Response
        Assertion.verifyContains(response.getMessage(), sClub_3441_01.ClubId, "Verify Active Club Id is Shown", t2, false);
        Assertion.verifyContains(response.getMessage(), sClub_3441_01.ClubName, "Verify Active Club name is Shown", t2, false);

        /*
        as the Club is now active its details must be shown for the SVC linked with the Member1
         */
        Assertion.verifyContains(response.getMessage(), sClub_3441_02.ClubId, "Verify Active Club Id is Shown", t2, false);
        Assertion.verifyContains(response.getMessage(), sClub_3441_02.ClubName, "Verify Active Club Name is Shown", t2, false);

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Get Wallet Details
     *
     * @throws Exception enable = false, as this was just for debug purpose,
     */
    @Test(priority = 3, enabled = false)
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test 03", "Get SVC List")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        TxnResponse response = Transactions.init(t3)
                .getSVCList(subsMember1.MSISDN, defaultProvider.ProviderId);

    }

    /**
     * Get Wallet Details
     *
     * @throws Exception
     */
    @Test(priority = 4)
    public void test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test 04", "Get Wallet details for the Saving Club member")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        TxnResponse response = Transactions.init(t4)
                .getSVCWalletDetails(sClub_3441_01.ClubId, subsMember1.MSISDN);

        Assertion.verifyEqual(response.SVCBalance, "0", "Get the wallet details, and also verify the SVC balance associated, must be 0, as no transaction is done yet", t4);

        /*
        Deposit a minimum SVA amount
        check the wallet balance, it must be greater than 0 and < = Min SVC amount
         */
        Transactions.init(t4)
                .depositSavingClub(sClub_3441_01, subsMember1, AppConfig.minSVCDepositAmount.toString());

        TxnResponse response1 = Transactions.init(t4)
                .getSVCWalletDetails(sClub_3441_01.ClubId, subsMember1.MSISDN);

        BigDecimal currentBal = new BigDecimal(response1.SVCBalance);
        t4.info("Current SVC Balance: " + currentBal);
        if (currentBal.compareTo(BigDecimal.valueOf(0)) >= 0 && currentBal.compareTo(AppConfig.minSVCDepositAmount) <= 0) {
            t4.pass("Verifies successfully that the current balance is reflected in SVC Wallet Details");
        } else {
            t4.fail("Failed to verify that the current balance is reflected in the SVC Wallet Details");
        }
    }

    /**
     * Initiate Saving Club deletion
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("Test 05", "Initiate Delete Saving Club").assignCategory(FunctionalTag.ECONET_SIT_5_0);
        OperatorUser naDelClub = DataFactory.getOperatorUsersWithAccess("DEL_CLUB").get(0);

        Login.init(t5)
                .login(naDelClub);

        SavingsClubManagement.init(t5)
                .initiateClubDeletion(sClub_3441_02.ClubId);
    }

    /**
     * Approve Saving Club deletion
     *
     * @throws Exception
     */
    @Test(priority = 6)
    public void test_06() throws Exception {
        ExtentTest t6 = pNode.createNode("Test 06", "Approve Delete Saving Club");
        OperatorUser naDelClubApp = DataFactory.getOperatorUsersWithAccess("DELAPP_CLUB").get(0);

        Login.init(t6)
                .login(naDelClubApp);

        SavingsClubManagement.init(t6)
                .approveClubDeletion(sClub_3441_02.ClubId);
    }

    /**
     * Get SVC List linked to a msisdn, Inactive Club Should not be available
     * As Club 2 is deleted in previous Tests, it must not be available in the list
     *
     * @throws Exception
     */
    @Test(priority = 7)
    public void test_07() throws Exception {
        ExtentTest t7 = pNode.createNode("Test 07", "Get SVC List linked to a msisdn, Inactive Club Should not be available")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        TxnResponse response = Transactions.init(t7)
                .getSVCList(subsMember1.MSISDN, defaultProvider.ProviderId)
                .verifyStatus(Constants.TXN_SUCCESS);

        // verify that the Club 1 is shown in the Message Response
        Assertion.verifyContains(response.getMessage(), sClub_3441_01.ClubId, "Verify Active Club Id is Shown", t7, false);
        Assertion.verifyContains(response.getMessage(), sClub_3441_01.ClubName, "Verify Active Club name is Shown", t7, false);

        // Verify Club 2 Details are not shown as it is still Inactive
        Assertion.verifyNotContains(response.getMessage(), sClub_3441_02.ClubId, "Verify Inactive Club Id is Not Shown", t7, false);
        Assertion.verifyNotContains(response.getMessage(), sClub_3441_02.ClubName, "Verify Inactive Club name is Not Shown", t7, false);

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8)
    public void test_08() throws Exception {
        ExtentTest t8 = pNode.createNode("Test 08", "Verify that withdraw transaction performed by a club member comes in the pending transactions for a club approver.")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            TxnResponse response = Transactions.init(t8)
                    .svcWithdrawCash(sClub_3441_01.ClubId, subsMember1.MSISDN, "10")
                    .verifyStatus(Constants.TXN_SUCCESS);
            String txnId = response.TxnId;

            //check pending transactions API and look for the performed transaction's ID
            TxnResponse pendingTxnResp = Transactions.init(t8)
                    .svcGetPendingTransactions(sClub_3441_01, subsMember1.MSISDN, defaultWallet.WalletId)
                    .verifyStatus(Constants.TXN_SUCCESS);
            String responseMessage = pendingTxnResp.getMessage();
            List<String> pendingTxnsList = new ArrayList<String>(Arrays.asList(responseMessage.split("~")));
            Assertion.verifyEqual(pendingTxnsList.contains(txnId), true, "Verify that the performed transaction comes in the list of pending transactions", t8, true);

        } catch (Exception e) {
            markTestAsFailure(e, t8);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9)
    public void test_09() throws Exception {
        ExtentTest t9 = pNode.createNode("Test 09", "Verify that bank to wallet transaction performed by a club member comes in the pending transactions for a club approver.")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            TxnResponse response = Transactions.init(t9)
                    .SVCbankToWallet(sClub_3441_01, subsChairman1.MSISDN, "10")
                    .verifyStatus(Constants.TXN_SUCCESS);
            String txnId = response.TxnId;

            //check pending transactions API and look for the performed transaction's ID
            TxnResponse pendingTxnResp = Transactions.init(t9)
                    .svcGetPendingTransactions(sClub_3441_01, subsMember1.MSISDN, "18")
                    .verifyStatus(Constants.TXN_SUCCESS);
            String responseMessage = pendingTxnResp.getMessage();
            List<String> pendingTxnsList = new ArrayList<String>(Arrays.asList(responseMessage.split("~")));
            Assertion.verifyEqual(pendingTxnsList.contains(txnId), true, "Verify that the performed transaction comes in the list of pending transactions", t9, true);

        } catch (Exception e) {
            markTestAsFailure(e, t9);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10)
    public void test_10() throws Exception {
        ExtentTest t10 = pNode.createNode("Test 10", "Verify that wallet to bank transaction performed by a club member comes in the pending transactions for a club approver.")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            TxnResponse response = Transactions.init(t10)
                    .SVCWalletToBank(sClub_3441_01, subsChairman1.MSISDN, "10")
                    .verifyStatus(Constants.TXN_SUCCESS);
            String txnId = response.TxnId;

            //check pending transactions API and look for the performed transaction's ID
            TxnResponse pendingTxnResp = Transactions.init(t10)
                    .svcGetPendingTransactions(sClub_3441_01, subsMember1.MSISDN, "18")
                    .verifyStatus(Constants.TXN_SUCCESS);
            String responseMessage = pendingTxnResp.getMessage();
            List<String> pendingTxnsList = new ArrayList<String>(Arrays.asList(responseMessage.split("~")));
            Assertion.verifyEqual(pendingTxnsList.contains(txnId), true, "Verify that the performed transaction comes in the list of pending transactions", t10, true);

        } catch (Exception e) {
            markTestAsFailure(e, t10);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 12)
    public void test_12() throws Exception {
        ExtentTest t12 = pNode.createNode("Test 12", "Verify the response returned by the SVC Bank Mini Statement Enquiry API")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            TxnResponse response = Transactions.init(t12)
                    .depositSavingClub(sClub_3441_01, subsMember1, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS);
            String txnId = response.TxnId;

            TxnResponse response2 = Transactions.init(t12)
                    .svcMiniStatement(sClub_3441_01, subsMember1.MSISDN, "18", "")
                    .verifyStatus(Constants.TXN_SUCCESS);
            String response2Message = response2.getMessage();
            Assertion.verifyEqual(response2Message.contains(txnId), true, "Verify that the response message of SVC Mini Statement contains the performed transaction", t12, true);
        } catch (Exception e) {
            markTestAsFailure(e, t12);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 13)
    public void test_13() throws Exception {
        ExtentTest t13 = pNode.createNode("Test 13", "Verify that adding a new savings club is always a one step process and gets successfully created irrespective of the preference ADD_MAKER_REQ_CLUB")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        User sc1, sc2;
        SavingsClub s1, s2;

        try {
            //create chairman users
            sc1 = new User(Constants.SUBSCRIBER);
            sc2 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t13)
                    .createSubscriberWithSpecificPayIdUsingAPI(sc1, defaultWallet.WalletId, true);
            SubscriberManagement.init(t13)
                    .createSubscriberWithSpecificPayIdUsingAPI(sc2, defaultWallet.WalletId, true);

            s1 = new SavingsClub(sc1, Constants.CLUB_TYPE_PREMIUM, defaultBank.BankID, false, false);
            s1.setMemberCount(2, 3);
            s1.setApproverCount(2, 3);
            s1.addMember(subsMember1);
            SystemPreferenceManagement.init(t13).updateSystemPreference("ADD_MAKER_REQ_CLUB", "Y");
            SavingsClubManagement.init(t13).createSavingsClub(s1);

            s2 = new SavingsClub(sc2, Constants.CLUB_TYPE_PREMIUM, defaultBank.BankID, false, false);
            s2.setMemberCount(2, 3);
            s2.setApproverCount(2, 3);
            s2.addMember(subsMember1);
            SystemPreferenceManagement.init(t13).updateSystemPreference("ADD_MAKER_REQ_CLUB", "N");
            SavingsClubManagement.init(t13).createSavingsClub(s2);

        } catch (Exception e) {
            markTestAsFailure(e, t13);
        }
        Assertion.finalizeSoftAsserts();
    }

}
