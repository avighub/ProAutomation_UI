package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.TxnResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_MON_3440_SVCWithdrawal extends TestInit {

    private User subsChairman, subsMember1, subsMember2;
    private SavingsClub sClub;
    private OperatorUser naTrfClubAdm, naTrfClubAdmApp;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        // Extent Test
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific for Testing SVC Withdrawal. " +
                "Make sure that Service Charge CLUB WITHDRAWAL is Configured. " +
                "Create a chairman user and also create Saving club for testing")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {

            // get Default Provider
            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);

            // Operator Users from AppData
            naTrfClubAdm = DataFactory.getOperatorUserWithAccess("TRF_CLUB_ADM");
            naTrfClubAdmApp = DataFactory.getOperatorUserWithAccess("APPTRF_CLUB_ADM");

            // User Objects
            subsChairman = new User(Constants.SUBSCRIBER);
            subsMember1 = DataFactory.getChannelUserWithCategoryAndGradeCode(Constants.SUBSCRIBER, Constants.GOLD_SUBSCRIBER, 0);
            subsMember2 = DataFactory.getChannelUserWithCategoryAndGradeCode(Constants.SUBSCRIBER, Constants.GOLD_SUBSCRIBER, 1);

            TransactionManagement.init(pNode).makeSureLeafUserHasBalance(subsMember1);
            TransactionManagement.init(pNode).makeSureLeafUserHasBalance(subsMember2);


            // Map SVC wallet Preference
            Preferences.init(s1)
                    .setSVCPreferences(subsChairman, bankId);

            // Savings Club object
            sClub = new SavingsClub(subsChairman, Constants.CLUB_TYPE_PREMIUM, defaultBank.BankID, false, false);

            // set Min and Max number of Member and Approver
            sClub.setMemberCount(2, 3);
            sClub.setApproverCount(2, 2);

        /*
        Add Member to the Club Object
        as member1 is the 2nd to be added after chairman, set the flag is approver for the same
         */
            sClub.addMember(subsMember1);
            subsMember1.setAsSvaApprover();
            sClub.addMember(subsMember2);

            // ServiceCharge Object
            ServiceCharge sWithdraw = new ServiceCharge(
                    Services.CLUB_WITHDRAW,
                    subsChairman,
                    subsMember2,
                    DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId,
                    null,
                    null,
                    null
            );

            // ServiceCharge Object
            ServiceCharge cDeposit = new ServiceCharge(Services.CLUB_DEPOSIT,
                    subsMember1,
                    subsChairman,
                    null,
                    DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId,
                    null,
                    null);

            /**
             P R E   R E Q U I S I T E    C R E A T I O N
             */

            //Create Subscribers with Default Mapping
            SubscriberManagement.init(s1)
                    .createSubscriberDefaultMapping(subsChairman, true, true);

            //Configure Service Charge For Club Deposit
            ServiceChargeManagement.init(s1)
                    .configureServiceCharge(sWithdraw);
            ServiceChargeManagement.init(s1)
                    .configureServiceCharge(cDeposit);

            // Create Saving Club
            SavingsClubManagement.init(s1)
                    .createSavingsClub(sClub);

            // make sure that Users are joined and are active
            for (User member : sClub.getMembers()) {
                // Join Member 1 and make SVA deposit
                if (!member.MSISDN.equals(sClub.CMMsisdn)) {
                    Transactions.init(s1)
                            .joinOrResignSavingClub(sClub, member, true)
                            .verifyStatus(Constants.TXN_SUCCESS);
                }
            }

            // Perform deposit, this loop is separated from the above
            // as member can not depoist unless club is active
            for (User member : sClub.getMembers()) {
                Transactions.init(s1)
                        .depositSavingClub(sClub, member, AppConfig.minSVCDepositAmount.toString())
                        .verifyStatus(Constants.TXN_SUCCESS);
            }


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void testWithdrawal() throws Exception {
        ExtentTest t1 = pNode.createNode("Test1", "Test Initiate SVC withdraw")
                .assignCategory(FunctionalTag.SAVING_CLUB);
        String svcWithdrawAmount = "4";
        TxnResponse response = null;
        UsrBalance subPreBal = null;
        try {
            subPreBal = MobiquityGUIQueries.getUserBalance(subsMember1, null, null);

            // Verify that SVC Withdrawal is successfully Initiated
            response = Transactions.init(t1)
                    .svcWithdrawCash(sClub.ClubId, subsMember1.MSISDN, svcWithdrawAmount)
                    .verifyMessage("savingclub.withdraw.initiated",
                            DataFactory.getDefaultProvider().Currency,
                            svcWithdrawAmount,
                            subsMember1.LoginId
                    );
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        /*
        SubTest 1
         */
        ExtentTest t2 = pNode.createNode("Test2", "Verify that The Chairman gets notification on SVC Withdrawal initiation")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SMSReader.init(t2).verifyRecentNotification(subsChairman.MSISDN,
                    "savingclub.withdraw.initiated",
                    DataFactory.getDefaultProvider().Currency,
                    svcWithdrawAmount,
                    subsMember1.LoginId
            );
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


        /*
        Sub test
        Verify Approving SVC withdrawal
         */
        ExtentTest t3 = pNode.createNode("Test3", "Verify that Approving SVC Withdraw")
                .assignCategory(FunctionalTag.SAVING_CLUB);
        UsrBalance subPostBal = null;
        try {
            for (User member : sClub.getMembers()) {
                if (member.isSvaApprover) {
                    Transactions.init(t3)
                            .svcApproverWithdraw(sClub.ClubId, member.MSISDN, response.TxnId)
                            .verifyStatus(Constants.TXN_SUCCESS);
                }
            }

            // Get The post Approval balance
            subPostBal = MobiquityGUIQueries.getUserBalance(subsMember1, null, null);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        /*
        Sub test
        Verify Approving SVC withdrawal
         */
        ExtentTest t4 = pNode.createNode("Test4", "Verify that User Balance is Credited")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            t4.info("PreBalance: " + subPreBal.Balance + " / PostBalance: " + subPostBal.Balance);
            if (subPreBal.Balance.compareTo(subPostBal.Balance) < 0) {
                t4.pass("The Member Balance is Credited after SVC Withdrawal");
            } else {
                t4.fail("Member's Balance is not Credited");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * testWithdrawNegativeCases
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void testWithdrawNegativeCases() throws Exception {
        ExtentTest t5 = pNode.createNode("Test5", "Verify Withdraw is not allowed for amount 0")
                .assignCategory(FunctionalTag.SAVING_CLUB);
        ExtentTest t6 = pNode.createNode("Test6", "Verify Withdraw is not allowed for amount more than Club Balance")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            Transactions.init(t5)
                    .svcWithdrawCash(sClub.ClubId, subsMember1.MSISDN, "0")
                    .verifyMessage("savingclub.withdraw.min.allowed");

            Transactions.init(t6)
                    .svcWithdrawCash(sClub.ClubId, subsMember1.MSISDN, "9999")
                    .verifyMessage("savingclub.withdraw.max.allowed");
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test Mini Statement API
     *
     * @throws Exception
     */
    @Test(priority = 3, enabled = false)
    public void testMiniStatementAPI() throws Exception {
        ExtentTest t7 = pNode.createNode("Test7", "Verify the Mini Statement")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        Transactions.init(t7)
                .svcGetMiniStatement(sClub)
                .verifyStatus(Constants.TXN_SUCCESS);

        // TODO - Data Validation


    }


}
