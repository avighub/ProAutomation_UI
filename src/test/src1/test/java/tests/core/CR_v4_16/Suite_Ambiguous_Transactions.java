package tests.core.CR_v4_16;

/**
 * T E S T   S C E N A R I O
 * <p>
 * 1. To verify that when transaction is ambiguous admin should be able to
 * download the ambiguous transactions from the UI and in the downloaded file
 * along with transaction ID numeric transaction also should be available in the downloaded file
 * <p>
 * 2. To verify that when transaction is ambiguous state,
 * admin should admin should be able to download the ambiguous transactions
 * and upload as either success or failure and after the successful upload for
 * <p>
 * a)  success transactions the FIC amount in the receiver account should me moved
 * to available balance and there should be no reconciliation mismatch
 * b)  For failed transactions the FIC in the receiver account should be
 * moved back to sender available balance and there should be no reconciliation mismatch
 * <p>
 * 3. DB Assertions
 * select RETRY_INTERVAL from THIRD_PARTY_PROPERTIES, if interval is more than 2 min, then fail the test
 * as the Transaction status will only updated after this retry interval
 * and waiting for longer time duration is not preferred in automation
 * <p>
 * select * from MTX_TRANSACTION_HEADER (Status and CheckReconciliation status once Roll Back Is Done)
 * <p>
 * MTX_AMBIGUOUS_TXN
 * MTX_AMBIGUOUS_TXN_DETAILS
 */

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.jigsaw.CommonOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;


/**
 * Created by rahul.rana on 8/31/2017.
 * <p>
 * TODO
 * 1 - UI validation for CheckReconciliation
 */
public class Suite_Ambiguous_Transactions extends TestInit {

    CurrencyProvider defaultProvider;
    private User wholesaler, merchant, subs, subsDeleteInitiate, subsDeleteInitiator;
    private String amountTx;
    private BigDecimal amountTxLong;
    private Wallet defaultWallet;

    /**
     * Setup
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest setup = pNode.createNode("SETUP", "Setup Specific for this Test");

        defaultProvider = DataFactory.getDefaultProvider();
        defaultWallet = DataFactory.getDefaultWallet();
        wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        merchant = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        subsDeleteInitiator = DataFactory.getChannelUserWithAccess("SUBSDEL");
        amountTx = "2";
        amountTxLong = new BigDecimal(amountTx);

        subsDeleteInitiate = new User(Constants.SUBSCRIBER); // for Delete initiate and churn related case
        SubscriberManagement.init(setup)
                .createDefaultSubscriberUsingAPI(subsDeleteInitiate);

        // Make sure Users have enough Balance
        TransactionManagement.init(setup)
                .makeSureChannelUserHasBalance(merchant)
                .makeSureChannelUserHasBalance(wholesaler)
                .makeSureLeafUserHasBalance(subs)
                .makeSureLeafUserHasBalance(subsDeleteInitiate);

        // Configure Transfer Rule for Merchant Pay
        ServiceCharge tRuleMerchantPay = new ServiceCharge(Services.MERCHANT_PAY, subs, merchant, null, null, null, null);
        TransferRuleManagement.init(setup)
                .configureTransferRule(tRuleMerchantPay);

        // Delete all pricing policy Service Charge
        CommonOperations.deleteAllPricingPolicies();
    }

    /**
     * Test Ambiguous Transaction with Set Status as 'TS' : SUCCESS
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TS() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 01", "To verify that when transaction is ambiguous admin should be able to " +
                " download the ambiguous transactions from the UI and in the downloaded file " +
                " along with transaction ID numeric transaction also should be available in the downloaded file");

        /**
         * Initiate merchant Payment, Transaction Status Should Go to Ambiguous
         */
        UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);

        SfmResponse response = Transactions.init(t1)
                .initiateMerchantPayment(merchant, subs, amountTx);

        /**
         * Check Balance
         */
        UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);
        AmbiguousTransactionManagement.init(t1).
                initiateAmbiguousBalanceValidation(subPreBal, merPreBal, subPostBal, merPostBal, amountTxLong);

        /**
         * Download the ambiguous Transaction File
         * Verify the Ambiguous Transaction File
         */
        AmbiguousTransactionManagement.init(t1)
                .downloadAmbiguousTxSheet(merchant)
                .verifyAmbiguousTransactionFile(response, subs.MSISDN, merchant.MSISDN);

        /**
         * Update the Excel and Upload the File
         */
        ExtentTest t2 = pNode.createNode("TEST 02", "success transactions the FIC amount in the receiver account should me moved\n" +
                " to available balance and there should be no reconciliation mismatch");

        /**
         * Set the Transaction as Pass
         */
        AmbiguousTransactionManagement.init(t2)
                .setTransactionStatus(response, true)
                .uploadUpatedAmbiguousTxnFile(response, merchant);

        Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

        /**
         * Check Balance & Db Status
         */
        AmbiguousTransactionManagement.init(t2)
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

        UsrBalance subPostProcessBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostProcessBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);
        AmbiguousTransactionManagement.init(t2).
                initiatePostProcessBalanceValidation(true, subPreBal, merPreBal, subPostProcessBal, merPostProcessBal, amountTxLong);
    }

    /**
     * Test Ambiguous Transaction with Set Status as 'TF' FAILURE
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TF() throws Exception {
        ExtentTest t3 = pNode.createNode("TEST 03", "Failed transactions the FIC amount in the receiver account should me moved\n" +
                " to available balance and there should be no reconciliation mismatch");

        /**
         * Initiate merchant Payment, Transaction Status Should Go to Ambiguous
         */
        UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);

        SfmResponse response = Transactions.init(t3)
                .initiateMerchantPayment(merchant, subs, amountTx);

        /**
         * Check Balance
         */
        UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);
        AmbiguousTransactionManagement.init(t3)
                .initiateAmbiguousBalanceValidation(subPreBal, merPreBal, subPostBal, merPostBal, amountTxLong);

        AmbiguousTransactionManagement.init(t3)
                .downloadAmbiguousTxSheet(merchant)
                .verifyAmbiguousTransactionFile(response, subs.MSISDN, merchant.MSISDN)
                .setTransactionStatus(response, false)
                .uploadUpatedAmbiguousTxnFile(response, merchant);

        Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

        /**
         * Check Balance and DB Process Status
         */
        AmbiguousTransactionManagement.init(t3)
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, false);

        UsrBalance subPostProcessBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostProcessBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);
        AmbiguousTransactionManagement.init(t3)
                .initiatePostProcessBalanceValidation(false, subPreBal, merPreBal, subPostProcessBal, merPostProcessBal, amountTxLong);
    }

    /**
     * Test Payee Delete initiate if it has Pending Transactions
     *
     * @throws Exception
     */
    @Test(priority = 3)
    public void TEST_03() throws Exception {
        ExtentTest t4 = pNode.createNode("TEST 04", "To verify that Payee could't be delete initiate if it has Pending Transaction");

        SfmResponse response = null;
        try {
            response = Transactions.init(t4)
                    .initiateMerchantPayment(merchant, subsDeleteInitiate, amountTx);

            /*
            Verify that User Could Not be Delete Initiated as it has Transaction In Pending state
             */
            SubscriberManagement.init(t4)
                    .startNegativeTest()
                    .initiateDeleteSubscriber(subsDeleteInitiate, subsDeleteInitiator, defaultProvider.ProviderId);

            Assertion.verifyErrorMessageContain("subs.error.pending.txn.exists", "Verify Payee Could not be Delete Initiate", t4);

        } finally {
            SfmResponse responseResume = Transactions.init(t4)
                    .resumeMerchantPayment(response, Services.RESUME_MERCHANT_PAY);
        }
    }

    @Test(priority = 4, enabled = false)
    public void INITIATE_MERCHANT_PAY_OUT() throws Exception {
        ExtentTest t4 = pNode.createNode("TEST 04", "Description is still pending");

        User party = new User(Constants.SUBSCRIBER); // this will act as a user not in the system
        /**
         * Initiate merchant Pay Out, Transaction Status Should Go to Ambiguous
         */
        UsrBalance whsPreBal = MobiquityGUIQueries.getUserBalance(wholesaler, null, null);
        UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);

        SfmResponse response = Transactions.init(t4)
                .initiateMerchantPayOut(merchant,
                        party,
                        wholesaler,
                        amountTx,
                        defaultWallet.WalletId);

        /**
         * Check Balance
         */
        UsrBalance whsPostBal = MobiquityGUIQueries.getUserBalance(wholesaler, null, null);
        UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);
        //initiateAmbiguousBalanceValidation(subPreBal, merPreBal, subPostBal, merPostBal, t3);
    }

    @Test(priority = 1)
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TS_Recharge_Others() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 01", "To verify that when transaction is ambiguous admin should be able to " +
                " download the ambiguous transactions from the UI and in the downloaded file " +
                " along with transaction ID numeric transaction also should be available in the downloaded file");

        /**
         * Initiate merchant Payment, Transaction Status Should Go to Ambiguous
         */
        UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);

        SfmResponse response = Transactions.init(t1)
                .initiateMerchantPayment(merchant, subs, amountTx);

        /**
         * Check Balance
         */
        UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);
        AmbiguousTransactionManagement.init(t1).
                initiateAmbiguousBalanceValidation(subPreBal, merPreBal, subPostBal, merPostBal, amountTxLong);

        /**
         * Download the ambiguous Transaction File
         * Verify the Ambiguous Transaction File
         */
        AmbiguousTransactionManagement.init(t1)
                .downloadAmbiguousTxSheet(merchant)
                .verifyAmbiguousTransactionFile(response, subs.MSISDN, merchant.MSISDN);

        /**
         * Update the Excel and Upload the File
         */
        ExtentTest t2 = pNode.createNode("TEST 02", "success transactions the FIC amount in the receiver account should me moved\n" +
                " to available balance and there should be no reconciliation mismatch");

        /**
         * Set the Transaction as Pass
         */
        AmbiguousTransactionManagement.init(t2)
                .setTransactionStatus(response, true)
                .uploadUpatedAmbiguousTxnFile(response, merchant);

        Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

        /**
         * Check Balance & Db Status
         */
        AmbiguousTransactionManagement.init(t2)
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

        UsrBalance subPostProcessBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostProcessBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);
        AmbiguousTransactionManagement.init(t2).
                initiatePostProcessBalanceValidation(true, subPreBal, merPreBal, subPostProcessBal, merPostProcessBal, amountTxLong);
    }


}
