package tests.core.CR_v4_16;

/**
 * T E S T   S C E N A R I O
 * <p>
 * 1. To verify that when transaction is ambiguous admin should be able to
 * download the ambiguous transactions from the UI and in the downloaded file
 * along with transaction ID numeric transaction also should be available in the downloaded file
 * <p>
 * 2. To verify that when transaction is ambiguous state,
 * admin should admin should be able to download the ambiguous transactions
 * and upload as either success or failure and after the successful upload for
 * a)  success transactions the FIC amount in the receiver account should me moved
 * to available balance and there should be no reconciliation mismatch
 * b)  For failed transactions the FIC in the receiver account should be
 * moved back to sender available balance and there should be no reconciliation mismatch
 * <p>
 * 3. DB Assertions
 * select RETRY_INTERVAL from THIRD_PARTY_PROPERTIES, if interval is more than 2 min, then fail the test
 * as the Transaction status will only updated after this retry interval
 * and waiting for longer time duration is not preferred in automation.
 * select * from MTX_TRANSACTION_HEADER (Status and CheckReconciliation status once Roll Back Is Done)
 * MTX_AMBIGUOUS_TXN
 * MTX_AMBIGUOUS_TXN_DETAILS
 */

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.jigsaw.CommonOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by rahul.rana on 9/21/2017.
 */
public class DemoApiAutomation extends TestInit {
    private User merchant, subs;
    private String amountTx;
    private BigDecimal amountTxLong;

    /**
     * Setup
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest setup = pNode.createNode("SETUP", "Make Sure Merchant and Subscriber has sufficient Balance. " +
                "Make sure that the Merchant Pay Transfer rule is configured. " +
                "Delete all existing Pricing policy. ");

        CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
        Wallet defaultWallet = DataFactory.getDefaultWallet();

        merchant = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        amountTx = "2";
        amountTxLong = new BigDecimal(amountTx);

        // Make sure Users have enough Balance
        TransactionManagement.init(setup)
                .makeSureChannelUserHasBalance(merchant)
                .makeSureLeafUserHasBalance(subs);

        // Configure Transfer Rule for Merchant Pay
        ServiceCharge tRuleMerchantPay = new ServiceCharge(Services.MERCHANT_PAY, subs, merchant, null, null, null, null);
        TransferRuleManagement.init(setup)
                .configureTransferRule(tRuleMerchantPay);

        // Delete all pricing policy Service Charge
        CommonOperations.deleteAllPricingPolicies();
    }

    /**
     * Test Ambiguous Transaction with Set Status as 'TS' : SUCCESS
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TS() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 01", "To verify that when transaction is ambiguous " +
                "admin should be able to download the ambiguous transactions from the UI and in the " +
                "downloaded file along with transaction ID numeric transaction also should be available in the downloaded file");

        /**
         Initiate merchant Payment, Transaction Status Should Go to Ambiguous
         */
        UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);

        SfmResponse response = Transactions.init(t1)
                .initiateMerchantPayment(merchant, subs, amountTx)
                .assertStatus("SUCCEEDED");

        /**
         Check Balance
         */
        UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);

        AmbiguousTransactionManagement.init(t1)
                .initiateAmbiguousBalanceValidation(subPreBal, merPreBal, subPostBal, merPostBal, amountTxLong);

        /**
         Download the ambiguous Transaction File
         Verify the Ambiguous File
         */
        AmbiguousTransactionManagement.init(t1)
                .downloadAmbiguousTxSheet(merchant)
                .verifyAmbiguousTransactionFile(response, subs.MSISDN, merchant.MSISDN);

        /**
         Update the Excel and Upload the File`
         */
        ExtentTest t2 = pNode.createNode("TEST 02", "success transactions the FIC amount in the receiver account should me moved\n" +
                " to available balance and there should be no reconciliation mismatch");

        /**
         Set the Transaction as Pass
         */
        AmbiguousTransactionManagement.init(t2)
                .setTransactionStatus(response, true)
                .uploadUpatedAmbiguousTxnFile(response, merchant);

        Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

        /**
         Check Balance & Db Status
         */
        AmbiguousTransactionManagement.init(t2)
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

        UsrBalance subPostProcessBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostProcessBal = MobiquityGUIQueries.getUserBalance(merchant, null, null);

        AmbiguousTransactionManagement.init(t1)
                .initiatePostProcessBalanceValidation(true, subPreBal, merPreBal, subPostProcessBal, merPostProcessBal, amountTxLong);
    }

}
