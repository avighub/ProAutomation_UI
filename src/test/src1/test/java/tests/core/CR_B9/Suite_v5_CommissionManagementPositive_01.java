package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.commissionDisbursement.CommissionDisbursement;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.commissionDisbursement.Commission_Withdrawal_Page1;
import framework.pageObjects.commissionDisbursement.Commission_Withdrawal_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Commission Management(Positive)User Story MON-4441
 * Author Name      : Jyoti Katiyar
 * Created Date     : 21/03/2018
 */

public class Suite_v5_CommissionManagementPositive_01 extends TestInit {

    private User wholesaler;
    private OperatorUser OptUser2, networkAdmin;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Get Required Base Set Users. " +
                "Creating Wholesaler. " +
                "Defining transfer rule and perform O2C");
        try {
            OptUser2 = DataFactory.getOperatorUsersWithAccess("O2C_INIT").get(0);
            networkAdmin = DataFactory.getOperatorUsersWithAccess("COMMDISINITIATE").get(0);

            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

           /* ChannelUserManagement.init(eSetup)
                    .createChannelUserDefaultMapping(wholesaler, true);*/

            //Login as networkadmin to set transfer limit and define O2C
            Login.init(eSetup).login(OptUser2);
            TransferRuleManagement.init(eSetup).setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                    wholesaler.DomainName,
                    wholesaler.CategoryName,
                    Constants.PAYINST_WALLET_CONST, DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName);

            //Perform O2C to commission wallet
            String txn = TransactionManagement.init(eSetup)
                    .initiateO2CForSpecificWallet(wholesaler,
                            DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName,
                            GlobalData.defaultProvider.ProviderId,
                            Constants.MIN_O2C_AMOUNT, "5646");
            TransactionManagement.init(eSetup).o2cApproval1(txn);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


      /*To verify that in the existing the commission disbursement page "Export" button is renamed to
     " Export for Commission Disbursement*/

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void ExportButtonIsRenamedToExportforCommissionDisbursement_4937() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4937", "To verify that in the existing the commission disbursement page \"Export\" button is renamed to\n" +
                "\" Export for Commission Disbursement");
        try {
            //login with network admin
            Login.init(t1).login(networkAdmin);

            //Navigate to Commission Management Page 1
            Commission_Withdrawal_Page1.init(t1)
                    .NavigateToLink()
                    .SelectProvider()
                    .SelectDomain(wholesaler.DomainName)
                    .SelectCategory(wholesaler.CategoryName)
                    .ClickOnSubmit();

            //Navigate to Commission Management Page 2 and verify the "Export CSV for Commission Disbursement" button
            Commission_Withdrawal_Page2.init(t1).verifyIfBottonExists("Export CSV for Commission Withdrawal", t1);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /*To verify that menu name is change from " Commission Disbursement Process" to " Commission Management"*/

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void CommissionDisbursementProcessToCommissionManagement_4938() throws Exception {
        ExtentTest t2 = pNode.createNode("MON-4938", "To verify that menu name is change from " +
                "Commission Disbursement Process to Commission Management");
        try {

            //login with network admin
            Login.init(t2).login(networkAdmin);

            //Navigate to Commission Management Page

            Commission_Withdrawal_Page1.init(t2)
                    .VerifyCommissionManagementLink(t2);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    /*To verify that breadcrumbs and form heading in the screens after clicking on " Commission Management"
    should be " Commission Management"*/

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void BreadcrumbsAndFormHeading_4939() throws Exception {
        ExtentTest t3 = pNode.createNode("MON-4939", "To verify that breadcrumbs and form heading " +
                "in the screens after clicking on Commission Management");

        try {

            //Login as NetworkAdmin
            Login.init(t3).login(networkAdmin);

            //Navigate to Commission Management Page 1 and Verify the breadcrumb and form heading in commission management page 1
            Commission_Withdrawal_Page1.init(t3).NavigateToLink()
                    .VerifyBreadcrumbs(t3, "1")
                    .VerifyFormHeading(t3);

            //Navigate to commission management page 2
            Commission_Withdrawal_Page1.init(t3)
                    .SelectProvider()
                    .SelectDomain(wholesaler.DomainName)
                    .SelectCategory(wholesaler.CategoryName)
                    .ClickOnSubmit();

            //Verify the breadcrumb in commission management page 2
            Commission_Withdrawal_Page1.init(t3).NavigateToLink()
                    .VerifyBreadcrumbs(t3, "2");

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();
    }

    /*To verify that along with " Export for Commission Disbursement" button, "Export for Commission Withdrawal"
    button should be displayed in the UI"*/

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void ExportForCommissionWithdrawalButtonShouldBeVisible_4940() throws Exception {
        ExtentTest t4 = pNode.createNode("MON-4940", "To verify that along with Export for Commission Disbursement " +
                "button, Export for Commission Withdrawal button should be displayed in the UI");
        try {

            //login with network admin
            Login.init(t4).login(networkAdmin);

            //Navigate to Commission Management Page 1

            Commission_Withdrawal_Page1.init(t4)
                    .NavigateToLink()
                    .SelectProvider()
                    .SelectDomain(wholesaler.DomainName)
                    .SelectCategory(wholesaler.CategoryName)
                    .ClickOnSubmit();


            //Navigate to Commission Management Page 2 and verify the "Export CSV for Commission Disbursement" button
            Commission_Withdrawal_Page2.init(t4)
                    .verifyIfBottonExists("Export CSV for Commission Withdrawal", t4);

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();
    }

    /*To verify that on selecting the users and clicking on Export for Commission Withdrawal" button
    should download the .csv file with selected users*/

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void ClickingOnExportForCommissionWithdrawalButtonToDownloadTheCSV_4941_4942() throws Exception {
        ExtentTest t5 = pNode.createNode("MON-4941", "To verify that on selecting the users and" +
                " clicking on Export for Commission Withdrawal button should download the .csv file with selected users");

        try {

            //Login as NetworkAdmin
            Login.init(t5).login(networkAdmin);

            CommissionDisbursement.init(t5)
                    .exportCommission_Withdrawal(wholesaler);

            String defaultProvider = GlobalData.defaultProvider.ProviderId;
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(wholesaler, "13", null);
            List status = new ArrayList<String>();
            status.add(0, defaultProvider);
            status.add(1, wholesaler.MSISDN);
            status.add(2, balance.Balance.intValue());

            ExtentTest t6 = pNode.createNode("MON-4942", "Dowloaded .csv file contains channel user " +
                    "mobile number, amount in commission wallet, Remarks as column header");
            CommissionDisbursement.init(t6)
                    .VerifyCommissionWithdrawalCSVFile(status);

        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();
    }

    /*To verify that the overridden amount in the UI should be downloaded to .csv file*/
    //@Ignore //Defect MON-5075
    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void OverriddenAmountInTheUIShouldNotBeDownloaded_4944() throws Exception {

        ExtentTest t7 = pNode.createNode("MON-4944", "To verify that the overridden amount" +
                " in the UI should not be downloaded to .csv file");
        try {
            //Login as NetworkAdmin
            Login.init(t7).login(networkAdmin);

            //Navigate to Commission Management Page 1
            Commission_Withdrawal_Page1.init(t7)
                    .NavigateToLink()
                    .SelectProvider()
                    .SelectDomain(wholesaler.DomainName)
                    .SelectCategory(wholesaler.CategoryName)
                    .ClickOnSubmit();

            //Over write the commission amount
            String changedCommisionAmount = "10.0";
            String actualCommissionAmount = Commission_Withdrawal_Page2.init(t7).overrideCommissionAmount(wholesaler.MSISDN, changedCommisionAmount);
            System.out.println(actualCommissionAmount);
            //Download the commission Withdrawal CSV file for a selected user
            Commission_Withdrawal_Page2.init(t7).downloadCommissionWithdrawal().getCommissionWithdrawalFile();

            //Create a arraylist and store the value to be verified from csv file
            //new CommissionDisbursement(t7).exportCommission_Withdrawal(wholesaler);
            String defaultProvider = DataFactory.getDefaultProvider().ProviderId;
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(wholesaler, "13", null);
            List status = new ArrayList<String>();
            status.add(0, defaultProvider);
            status.add(1, wholesaler.MSISDN);
            status.add(2, changedCommisionAmount);
            CommissionDisbursement.init(t7).VerifyCommissionWithdrawalCSVFile(status);

            if (actualCommissionAmount.equals(changedCommisionAmount)) {
                t7.fail("The Commission amount in the Commission withdrawal csv is not changed");
            } else {
                t7.pass("The Commission amount in the Commission withdrawal csv is overridden i.e changed");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t7);
        }
        Assertion.finalizeSoftAsserts();
    }
}
