package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Subscriber should be able to change his default currency
 * Author Name      : Nirupama MK
 * Created Date     : 21/05/2018
 */
public class Suite_v5_ChangeTheDefaultCurrency_03 extends TestInit {

    /*  MON_5876
        create a User(chnl/sub)
        Run the API to fetch the User Information
        Verify that the response from API also contains the Default_Currency, which is same as specified as default

        MON_5878
        create a User(chnl/sub) with default provider
        Run API to Change the Default Currency Code
        Verify the Different Error Msg when Same or Different currency code is provided

     */
    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void MON_5876() throws Exception {

        ExtentTest MON_5876 = pNode.createNode("MON-5876", "Verify that 'USERINFOREQ' API should " +
                "return the default currency of the user as part of response");

        try {

            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(MON_5876).createDefaultSubscriberUsingAPI(sub);

            TxnResponse res = Transactions.init(MON_5876).userInfo(sub, Constants.CUSTOMER_REIMB);

            String currencyCodeFromAPIresponse = res.getResponse().extract().jsonPath()
                    .getString("COMMAND.DETAIL.DEFAULT_CURRENCY");

            String defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(sub);

            Assertion.assertEqual(currencyCodeFromAPIresponse, defaultCurrencyCodeOfUser,
                    "User Information Contains 'Currency Code'", MON_5876);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5876);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_5878() throws Exception {

        ExtentTest MON_5878 = pNode.createNode("MON-5878", "Verify error message when " +
                "Subscriber / channel user will select a different currency and submit.");

        try {
            User subscriber = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(MON_5878).createDefaultSubscriberUsingAPI(subscriber);

            //fetch the List of Currencies Associated with user
            TxnResponse response = Transactions.init(MON_5878)
                    .fetchListOfCurrenciesAssociatedWithUser(subscriber.MSISDN, Constants.CUSTOMER_REIMB)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .assertMessage("userenq.user.success");

            String currencyAssociatedWithUser = response.getResponse().extract().jsonPath()
                    .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

            MON_5878.info("Currency associated with user - " + currencyAssociatedWithUser);

            //Fetch the default currency of user
            String defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(subscriber);

            List<String> CurrencyListFromSystem = MobiquityGUIQueries.fetchListOfCurrencyCode();

            for (String currencyCode : CurrencyListFromSystem) {

                String providerID = MobiquityGUIQueries.getProviderIdAssociatedWithCurrency(currencyCode);

                if (!currencyCode.equals(defaultCurrencyCodeOfUser) & (!(currencyCode.equals(currencyAssociatedWithUser)))) {

                    Transactions.init(MON_5878)
                            .startNegativeTest()

                            //error msg when same currency code is provided
                            .changeCurrencyCode(subscriber, currencyCode, providerID)
                            .assertMessage("CDC001");
                    break;
                }
            }

            for (String currencyCode : CurrencyListFromSystem) {

                if (!currencyCode.equals(defaultCurrencyCodeOfUser) & (!(currencyCode.equals(currencyAssociatedWithUser)))) {

                    Transactions.init(MON_5878)
                            .startNegativeTest()

                            //error msg when same currency code is provided
                            .changeCurrencyCode(subscriber, currencyCode, DataFactory.getDefaultProvider().ProviderId)
                            .assertMessage("CDC002");
                    break;
                }
            }

        } catch (Exception e) {
            markTestAsFailure(e, MON_5878);
        }
        Assertion.finalizeSoftAsserts();
    }
}