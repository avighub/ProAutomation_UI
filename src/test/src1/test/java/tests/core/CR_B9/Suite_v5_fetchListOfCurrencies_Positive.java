package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;
import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : API to fetch list of currencies associated with user
 * Author Name      : Nirupama MK
 * Created Date     : 21/05/2018
 */

public class Suite_v5_fetchListOfCurrencies_Positive extends TestInit {

    private User channeluser, subscriber;
    private TxnResponse response, response1;
    private String defaultPrefValue;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            subscriber = new User(Constants.SUBSCRIBER);
            CurrencyProviderMapping.init(eSetup)
                    .mapPrimaryWalletPreference(subscriber);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void MON_5717() throws Exception {

        /*
            MON_5717
            fetch list of currencies for the existing Channel User
            validate success message

            MON_5718
            fetch list of currencies for the existing Subscriber
            validate success message

            MON_5728
            channelUser currency details are displayed,
            when user is registered as both ChannelUser and Subscriber
            while running the API, even if "user role" is not specified,
            by default, Channel user details are displayed
            validate success message

         */

        ExtentTest MON_5717 = pNode.createNode("MON-5717",
                "Verify that the currencies associated with the channel user are displayed in the response");

        try {
            channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            response = Transactions.init(MON_5717)
                    .fetchListOfCurrenciesAssociatedWithUser(channeluser.MSISDN, Constants.CHANNEL_REIMB)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .assertMessage("userenq.user.success");

            List<String> currencyList = MobiquityGUIQueries.fetchListOfCurrencyCode();

            Boolean isResponseHavingListOfCurrencies = response.getResponse().extract().jsonPath()
                    .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY").contains(",");

            if (isResponseHavingListOfCurrencies) {

                int res = response.getResponse().extract().jsonPath()
                        .getList("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY").size();

                for (int i = 0; i < res; i++) {
                    String currencyListfromAPI = response.getResponse().extract().jsonPath()
                            .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY[" + i + "]");

                    if (currencyList.contains(currencyListfromAPI)) {
                        MON_5717.pass("list of currencies associated with user - " + currencyListfromAPI);

                    } else {
                        MON_5717.fail("list of currencies are not available");
                        Assert.fail();
                    }
                }

            } else {
                String currencyFromAPI = response.getResponse().extract().jsonPath()
                        .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

                if (currencyList.contains(currencyFromAPI)) {
                    MON_5717.pass("Only one Currency is Associated with user - " + currencyFromAPI);

                } else {
                    MON_5717.fail("list of currencies are not available");
                    Assert.fail();
                }
            }

            ExtentTest MON_5718 = pNode.createNode("MON_5718",
                    "Verify that the Subscriber currencies  are displayed in the response");

            subscriber = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(MON_5717).createDefaultSubscriberUsingAPI(subscriber);

            response1 = Transactions.init(MON_5718)
                    .fetchListOfCurrenciesAssociatedWithUser(subscriber.MSISDN, Constants.CUSTOMER_REIMB)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .assertMessage("userenq.user.success");

            String currencyfromApiResponse = response1.getResponse().extract().jsonPath()
                    .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

            if (currencyList.contains(currencyfromApiResponse)) {
                MON_5718.pass("Currency associated with user" + currencyfromApiResponse);

            } else {

                MON_5718.fail("Currency List not available");
                Assert.fail();
            }

        } catch (Exception e) {
            markTestAsFailure(e, MON_5717);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_5728() throws Exception {

        ExtentTest MON_5728 = pNode.createNode("MON-5728",
                "when user is enquired for currencies for a user who is both Subscriber and " +
                        " channel user By default channel user currencies should be  displayed.");
        try {

            String msisdn, userType;
            User chUser;

            List<String> currencyList = MobiquityGUIQueries.fetchListOfCurrencyCode();
            Map<String, String> userAsBoth = MobiquityGUIQueries.fetchUserWhoIsBothSubsAndChanlUser();

            if (userAsBoth != null) {

                msisdn = userAsBoth.get("MSISDN");
                userType = userAsBoth.get("UserType");

                response = Transactions.init(MON_5728)
                        .fetchListOfCurrenciesAssociatedWithUser(msisdn, "")
                        .verifyStatus(Constants.TXN_SUCCESS)
                        .assertMessage("userenq.user.success");

                Boolean isResponseHavingListOfCurrencies = response.getResponse().extract().jsonPath()
                        .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY").contains(",");

                if (isResponseHavingListOfCurrencies) {

                    int res = response.getResponse().extract().jsonPath()
                            .getList("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY").size();

                    for (int i = 0; i < res; i++) {
                        String currencyListfromAPI = response.getResponse().extract().jsonPath()
                                .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY[" + i + "]");

                        if (currencyList.contains(currencyListfromAPI)) {
                            MON_5728.pass("list of currencies associated with user - " + currencyListfromAPI);

                        } else {
                            MON_5728.fail("list of currencies are not available");
                            Assert.fail();
                        }
                    }

                } else {
                    String currencyFromAPI = response.getResponse().extract().jsonPath()
                            .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

                    Assertion.verifyContains(String.valueOf(currencyList), currencyFromAPI,
                            "Only one Currency is Associated with user ", MON_5728);

                }

            } else {

                defaultPrefValue = MobiquityGUIQueries
                        .fetchDefaultValueOfPreference("CHANNEL_USER_AS_SUBS_ALLOWED");

                SystemPreferenceManagement.init(MON_5728)
                        .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");

                //create user who is both channel user and subscriber
                chUser = new User(Constants.WHOLESALER);
                ChannelUserManagement.init(MON_5728).createChannelUser(chUser, false);

                subscriber = new User(Constants.SUBSCRIBER);
                subscriber.setMSISDN(chUser.MSISDN);

                SubscriberManagement.init(MON_5728).createDefaultSubscriberUsingAPI(subscriber);

                response = Transactions.init(MON_5728)
                        .fetchListOfCurrenciesAssociatedWithUser(subscriber.MSISDN, "")
                        .verifyStatus(Constants.TXN_SUCCESS)
                        .assertMessage("userenq.user.success");

                Boolean isResponseHavingListOfCurrencies = response.getResponse().extract().jsonPath()
                        .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY").contains(",");

                if (isResponseHavingListOfCurrencies) {

                    int res = response.getResponse().extract().jsonPath()
                            .getList("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY").size();

                    for (int i = 0; i < res; i++) {
                        String currencyListfromAPI = response.getResponse().extract().jsonPath()
                                .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY[" + i + "]");

                        if (currencyList.contains(currencyListfromAPI)) {
                            MON_5728.pass("list of currencies associated with user - " + currencyListfromAPI);

                        } else {
                            MON_5728.fail("list of currencies are not available");
                            Assert.fail();
                        }
                    }

                } else {
                    String currencyFromAPI = response.getResponse().extract().jsonPath()
                            .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

                    Assertion.verifyContains(String.valueOf(currencyList), currencyFromAPI,
                            "Only one Currency is Associated with user ", MON_5728);
                }

                SystemPreferenceManagement.init(MON_5728)
                        .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", defaultPrefValue);
            }

        } catch (Exception e) {
            markTestAsFailure(e, MON_5728);

        }
    }
}