package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkBillPaymentCSV;
import framework.dataEntity.BulkRechargeOtherCSV;
import framework.dataEntity.UsrBalance;
import framework.entity.RechargeOperator;
import framework.entity.*;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_BankAccountRegistrationWEB_Negative_MON_4443
 * Author Name      : Pushpalatha | reworked [rahul rana, sep 29, 2018]
 * Created Date     : 18/07/2018
 */

public class Suite_v5_BulkBillPaymentForEnterprisers_MON_3501 extends TestInit {

    private User entUser;
    private Biller offLineBiller, onlineBiller;
    private OperatorUser bulkUser1, bulkUser2, opt;
    private RechargeOperator RCoperator;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            TransactionManagement.init(eSetup).makeSureChannelUserHasBalance(entUser);

            offLineBiller = BillerManagement.init(eSetup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            //getting OperatorID
            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            RCoperator = OperatorUserManagement.init(eSetup)
                    .getRechargeOperator(defaultProvider.ProviderId);

            ServiceCharge sCharge1 = new ServiceCharge(Services.BILL_PAYMENT, entUser, offLineBiller, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(sCharge1);

            User recharge = new User(Constants.ZEBRA_MER);
            ServiceCharge sCharge2 = new ServiceCharge(Services.Recharge_Others, entUser, recharge, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(sCharge2);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.SYSTEM_TEST})
    public void verifyThatBulkOnlineBillPaymentIsSuccessfulForEnterprise() throws Exception {

        ExtentTest t1 = pNode.createNode("SC01",
                "Verify that bulk online bill payment is successful for Enterprise");

        try {
            BigDecimal txnAmount = new BigDecimal(Constants.MAX_TRANSACTION_ALLOWED).add(new BigDecimal(1));
            onlineBiller = BillerManagement.init(t1)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            UsrBalance preEntBalance = MobiquityGUIQueries.getUserBalance(entUser, null, null);
            UsrBalance preBillerBalance = MobiquityGUIQueries.getBillerBalance(onlineBiller.BillerCode);

            // Downlaod and update the Csv File with Single entry
            List<BulkBillPaymentCSV> entBillPayList = new ArrayList<>();
            entBillPayList.add(new BulkBillPaymentCSV("1", onlineBiller.BillerCode, txnAmount.toString(),
                    DataFactory.getRandomNumberAsString(6), GlobalData.defaultProvider.ProviderId, "Remark Ok"));

            //InitiatingBulkpayout
            Login.init(t1).login(entUser);
            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateBulkBillPaymentCsv(entBillPayList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_BILL_PAY_ENTERPRISE, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, true);

            // verify Dashboard page for successful Transaction
            EnterpriseManagement.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_BILL_PAY_ENTERPRISE, batchId, true, true);

            //Enterprise
            UsrBalance postEntBalance = MobiquityGUIQueries.getUserBalance(entUser, null, null);
            UsrBalance postBillerBalance = MobiquityGUIQueries.getBillerBalance(onlineBiller.BillerCode);

            Thread.sleep(2000);
            Assertion.verifyAccountIsDebited(preEntBalance.Balance, postEntBalance.Balance, txnAmount,
                    "Verify that enterprise User Balance is Debited", t1);
            Assertion.verifyAccountIsCredited(preBillerBalance.Balance, postBillerBalance.Balance, txnAmount,
                    "Verify that Biller Balance is credited", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.SYSTEM_TEST})
    public void verifyThatBulkBillPaymentIsSuccessfulForEnterprise() throws Exception {
        ExtentTest t1 = pNode.createNode("SC02",
                "Verify that bulk off-lineline bill payment is successful for Enterprise");

        try {
            BigDecimal txnAmount = new BigDecimal(10);
            UsrBalance preEntBalance = MobiquityGUIQueries.getUserBalance(entUser, null, null);
            UsrBalance preBillerBalance = MobiquityGUIQueries.getBillerBalance(offLineBiller.BillerCode);

            //InitiatingBulkpayout
            Login.init(t1).login(entUser);
            List<BulkBillPaymentCSV> entBillPayList = new ArrayList<>();
            entBillPayList.add(new BulkBillPaymentCSV("1", offLineBiller.BillerCode, txnAmount.toString(),
                    DataFactory.getRandomNumberAsString(6), GlobalData.defaultProvider.ProviderId, "Remark Ok"));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateBulkBillPaymentCsv(entBillPayList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_BILL_PAY_ENTERPRISE, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, true);

            EnterpriseManagement.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_BILL_PAY_ENTERPRISE, batchId, true, true);

            UsrBalance postEntBalance = MobiquityGUIQueries.getUserBalance(entUser, null, null);
            UsrBalance postBillerBalance = MobiquityGUIQueries.getBillerBalance(offLineBiller.BillerCode);

            Thread.sleep(2000);
            Assertion.verifyAccountIsDebited(preEntBalance.Balance, postEntBalance.Balance, txnAmount,
                    "Verify that enterprise User Balance is Debited", t1);
            Assertion.verifyAccountIsCredited(preBillerBalance.Balance, postBillerBalance.Balance, txnAmount,
                    "Verify that Biller Balance is credited", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0, FunctionalTag.SYSTEM_TEST})
    public void verifyThatBulkRechargeOthersIsSuccessfulForEnterprise() throws Exception {

        ExtentTest t1 = pNode.createNode("SC03",
                "Verify that bulk recharge others is successful for Enterprise");
        try {
            BigDecimal txnAmount = new BigDecimal(10);
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            UsrBalance preEntBalance = MobiquityGUIQueries.getUserBalance(entUser, null, null);
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            UsrBalance preRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);

            //InitiatingBulkpayout
            Login.init(t1).login(entUser);
            List<BulkRechargeOtherCSV> entRechargeOtherList = new ArrayList<>();
            entRechargeOtherList.add(new BulkRechargeOtherCSV("1", RCoperator.OperatorId, txnAmount.toString(),
                    whs.MSISDN, GlobalData.defaultProvider.ProviderId, "Remark Ok"));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateBulkRechargeOtherCsv(entRechargeOtherList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_RECHARGE_OTHER_ENTERPRISE, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, true);

            UsrBalance postEntBalance = MobiquityGUIQueries.getUserBalance(entUser, null, null);
            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            UsrBalance postRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);

            Assertion.verifyAccountIsDebited(preEntBalance.Balance, postEntBalance.Balance, txnAmount,
                    "Verify that enterprise User Balance is Debited", t1);
            Assertion.verifyAccountIsCredited(preRCOptBalance.FIC, postRCOptBalance.FIC, txnAmount,
                    "Verify that Operator FIC Balance is credited ", t1);
            /*Assertion.verifyAccountIsNotAffected(preWhsBalance.Balance, postWhsBalance.Balance,
                    "Verify that Wholesaler Balance is credited", t1);*/

            //TODO transaction need to be Resumed - further balance calculation is required
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
