package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.stockManagement.StockApproval_Page1;
import framework.pageObjects.stockManagement.StockLimit_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class Suite_v5_StockCreation extends TestInit {

    public static BigDecimal Stock_TransferLimit;
    public static BigDecimal Stock_TransferLevel1;
    public static BigDecimal Stock_TransferLevel2;
    private User sub, chan;
    private String defaultProviderName;
    private OperatorUser stockInitiator, stockApproval, opt, stockApproval2, stockLimiter;

    @BeforeClass(alwaysRun = true)
    public void prerequist() throws Exception {
        try {
            defaultProviderName = DataFactory.getDefaultProvider().ProviderName;
            stockInitiator = DataFactory.getOperatorUsersWithAccess("STOCK_INIT").get(0);
            stockApproval = DataFactory.getOperatorUsersWithAccess("STOCK_APP1").get(0);
            stockApproval2 = DataFactory.getOperatorUsersWithAccess("STOCK_APP2").get(0);
            stockLimiter = DataFactory.getOperatorUsersWithAccess("STOCK_LIMIT").get(0);
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            chan = DataFactory.getAnyChannelUser();


            ExtentTest setup = pNode.createNode("Setup", "Setuo Specific to this suite. " +
                    "Select Start and End Date. " +
                    "Set stock initation limit. ");
            String fromDate = new DateAndTime().getDate(-30);
            String toDate = new DateAndTime().getDate(0);


            //Operator user login,We are taking first Operator user because other operator user will initiate n approve.
            OperatorUser opt = DataFactory.getOperatorUsersWithAccess("STOCK_LIMIT").get(0);
            Login.init(setup).login(opt);

           /* //Adding stock limit
            StockManagement stockManagement = StockManagement.init(setup);
            stockManagement.addNetworkStockLimit(defaultProviderName, Constants.Stock_TransferLimit);*/

            //Get Stock Limit from StockLimit Page1 from UI
            StockLimit_Page1 page1 = StockLimit_Page1.init(setup);
            page1.navigateToStockLimitPage();
            Stock_TransferLimit = page1.getStockLimitUI();

            //Set Stock Limit level 1 and Stock Limit level 2
            Stock_TransferLevel2 = Stock_TransferLimit.add(new BigDecimal(1));
            Stock_TransferLevel1 = Stock_TransferLimit.subtract(new BigDecimal(1));
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void networkAdminAbleToInitiateStock() {
        ExtentTest t1 = pNode.createNode("MON_5561_01", "verify network admin able to do stock initiation and able to approve stock at stock approval-1.");
        try {
            StockManagement.init(t1).initiateAndApproveNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_AMOUNT);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void onceStockIsApprovedByNetadminSameDetailsShouldNotBeAvailableInApprovalList() {
        ExtentTest t1 = pNode.createNode("MON_5561_02", "verify once stock initiation is approved,same details should not availble in approval list.");
        try {
            Login.init(t1).login(stockInitiator);
            String txnId = StockManagement.init(t1).
                    initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_AMOUNT);
            StockManagement.init(t1).approveNetworkStockL1(txnId);

            Login.init(t1).login(stockApproval);
            StockApproval_Page1 page1 = StockApproval_Page1.init(t1);
            page1.navigateToStockApproval1Page();
            try {
                WebElement element = driver.findElement(By.xpath(".//*[@value='" + txnId + "']"));
                t1.fail("Still transaction id is available for Approval");
            } catch (Exception e) {
                t1.pass("Transaction id is not available for Approval");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void networkAdminAbleToInitiateStockAndApprovedByTwoNetworkAdminIfAmountIsHigherThan() {
        ExtentTest t1 = pNode.createNode("MON_5561_03", "verify network admin able to do stock initiation and able to approve stock at stock approval-1 and stock approval-2.");
        try {

            StockManagement.init(t1).
                    initiateAndApproveNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                            DataFactory.getDefaultBankNameForDefaultProvider(),
                            Stock_TransferLevel2.toString());
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void verifyThatRefNoFieldIsNumericOnly() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5561_04",
                "Verify that Ref. No. field is numeric only and system should display proper error message when data entered in Ref No is non-numeric");
        try {
            //Enter special character for Ref No
            StockManagement.init(t1).
                    initStockTransferWithInvalidDetails(DataFactory.getDefaultProvider().ProviderName,
                            DataFactory.getDefaultBankNameForDefaultProvider(),
                            Stock_TransferLevel2.toString(), "***");
            Assertion.verifyErrorMessageContain("stockInitiation.special.refNo", "Reference Number Validation", t1);

            //Leave Ref No field as blank
            StockManagement.init(t1).
                    initStockTransferWithInvalidDetails(DataFactory.getDefaultProvider().ProviderName,
                            DataFactory.getDefaultBankNameForDefaultProvider(),
                            Stock_TransferLevel2.toString(), "");
            Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "Reference Number Validation", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void verifyThatRequestedAmountFieldIsNumericOnly() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5561_05",
                "Verify that Requested Amount field is numeric only and system should display proper error message when data entered in Requested Amount is non-numeric");

        try {
            String refNo = DataFactory.getRandomNumberAsString(5);

            //Enter special character for Req Amount
            StockManagement.init(t1).
                    initStockTransferWithInvalidDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "@#$%", refNo);
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Requested Amount non numeric Validation", t1);

            Thread.sleep(1000);

            //Enter alphabets for Req Amount
            StockManagement.init(t1).
                    initStockTransferWithInvalidDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "asdeq", refNo);
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Requested Amount non numeric Validation", t1);

            Thread.sleep(1000);

            //Enter alphaNumeric for Req Amount
            StockManagement.init(t1).
                    initStockTransferWithInvalidDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "34er", refNo);
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Requested Amount non numeric Validation", t1);

            Thread.sleep(1000);

            //Enter Requested Amount value less than 0
            StockManagement.init(t1).
                    initStockTransferWithInvalidDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "-45", refNo);
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Requested Amount less than 0 Validation", t1);

            Thread.sleep(1000);

            //Enter Requested Amount value equal to 0
            StockManagement.init(t1).
                    initStockTransferWithInvalidDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "0", refNo);
            Assertion.verifyErrorMessageContain("stock.error.requestedQuantity.greaterThanZero", "Requested Amount equal to 0 Validation", t1);

            Thread.sleep(1000);

            //Decimal values validation
            StockManagement.init(t1).
                    initStockTransferWithInvalidDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "2.23456", refNo);
            Assertion.verifyErrorMessageContain("escrowInitiation.decimal.validation", "Requested Amount Decimal value validation", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0, FunctionalTag.DESCOPED_5dot0})
    public void verifyThatNetworkAdminIsnotabletoinitiateStockWhenHeIsBarredInTheSystem() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5561_06",
                "Verify that network admin is not able to initiate Stock transfer when he is barred in the system and system should display proper error message");
        try {
            //As per MON-6816, when user is barred, he will not be able to login
            OperatorUser net = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);

            Login.init(t1).startNegativeTest().tryLogin(net.LoginId, net.Password);
            Assertion.verifyErrorMessageContain("00036", "User is not able to initiate Stock", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
