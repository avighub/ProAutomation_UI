package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.pageObjects.stockManagement.StockApproval_Page1;
import framework.pageObjects.stockManagement.StockLimit_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

//

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Stock Initation and Approval (Negative) User Story MON-3744
 * Author Name      : Jyoti Katiyar
 * Created Date     : 10/04/2018
 */

public class StockInitiationAndApprovalNegative extends TestInit {
    private BigDecimal Stock_TransferLimit;
    private BigDecimal Stock_TransferLevel1;
    private BigDecimal Stock_TransferLevel2;
    private OperatorUser opt;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest setup = pNode.createNode("Pre-conditions for Stock Initiation And Approval", "Get Required Base Set Users. " +
                "Set stock initiation limit. ");

        try {

            //Operator user login ,We are taking first Operator user because other operator user will initiate n approve.
            OperatorUser opt = DataFactory.getOperatorUsersWithAccess("STOCK_LIMIT").get(0);
            Login.init(setup).login(opt);
            setup.info("Stock Limit set by:" + opt.LoginId);

            /*//Adding stock limit
            String defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            StockManagement stockManagement = StockManagement.init(setup);
            stockManagement.addNetworkStockLimit(defaultProvider, Constants.Stock_TransferLimit);*/

            //Get Stock Limit from StockLimit Page1
            StockLimit_Page1 page1 = StockLimit_Page1.init(setup);
            page1.navigateToStockLimitPage();
            Stock_TransferLimit = StockLimit_Page1.init(setup).getStockLimitUI();

            //Set Stock Limit level 1 and Stock Limit level 2
            Stock_TransferLevel2 = Stock_TransferLimit.add(new BigDecimal(1));
            Stock_TransferLevel1 = Stock_TransferLimit.subtract(new BigDecimal(1));
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /* To verify that the users who has initiated the stock should not be able to approve it level-1 */
    @Test(priority = 1, enabled = true)
    public void StockInitiatorShouldNotApproveAtLevel1_TC01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_07", "To verify that the users who has " +
                "initiated the stock should not be able to approve it level-1");
        try {
            //login with operator user having Group role for adding channel user
            opt = DataFactory.getOperatorUsersWithAccess("STR_INIT").get(0);
            Login.init(t1).login(opt);

            StockManagement stockManagement = StockManagement.init(t1);

            //Initiate Stock
            t1.info("Stock Initiated set by:" + opt.LoginId);
            String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultBankNameForDefaultProvider(),
                    Stock_TransferLevel2.toString());

            //Approval 1
            ConfigInput.isAssert = false;
            opt = DataFactory.getOperatorUsersWithAccess("STR_INIT").get(0);
            Login.init(t1).login(opt);
            stockManagement.verifyStockApprovalLevel1(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /* To verify that the users who has initiated the stock should not be able to approve it level-2*/
    @Test(priority = 2, enabled = true)
    public void StockInitiatorShouldNotApproveAtLevel2_TC02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test_08", "To verify that the users who has " +
                "initiated the stock should not be able to approve it level2");
        try {
            //login with operator user having Group role for adding channel user
            opt = DataFactory.getOperatorUsersWithAccess("STR_INIT").get(0);
            Login.init(t2).login(opt);

            StockManagement stockManagement = StockManagement.init(t2);

            //Initiate Stock
            String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultBankNameForDefaultProvider(),
                    Stock_TransferLevel2.toString());
            System.out.println(txnID);

            stockManagement.verifyStockApprovalLevel2(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();

    }

    /* To verify that the users who has approved at level-1 should not be able to approve at level-2 */
    @Test(priority = 3, enabled = true)
    public void ApproverAtLevel1ShouldNotApproveAtLevel1_TC03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_09", "To verify that the users who has " +
                "approved at level1 should not be able to approve at level2 ");
        try {
            //login with operator user having Group role for adding channel user
            opt = DataFactory.getOperatorUsersWithAccess("STR_INIT").get(0);
            Login.init(t3).login(opt);

            StockManagement stockManagement = StockManagement.init(t3);

            //Initiate Stock
            String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultBankNameForDefaultProvider(),
                    Stock_TransferLevel2.toString());

            //Approval 1
            ConfigInput.isAssert = false;
            //stockManagement.stockApprovalLevel1(txnID);
            OperatorUser stockApprover1 = DataFactory.getOperatorUsersWithAccess("STOCKTR_APP1").get(0);

            Login.init(t3).login(stockApprover1);
            StockApproval_Page1 page1 = StockApproval_Page1.init(t3);
            page1.navigateToStockApproval1Page();
            page1.selectRadioButton(txnID);
            page1.clickSubmit();
            page1.verifyAmount();

            if (ConfigInput.isConfirm)
                page1.clickApprove();
            Thread.sleep(2000);
            if (ConfigInput.isAssert) {
                String actualMessage = Assertion.getActionMessage();
                if (actualMessage.contains(MessageReader.getMessage("stock.approval.secondLevelNeeded", null))) {
                    t3.pass("Successfully Approved Level 1, Level 2 Approval Is Required!");

                } else {
                    Assertion.verifyActionMessageContain("stock.approve.success", "Approve Network Stock", t3, txnID);

                }
            }

            //Login with the same operator user who approved at level-1
            OperatorUser stockApprover2 = DataFactory.getOperatorUsersWithAccess("STOCKTR_APP1").get(0);
            Login.init(t3).login(stockApprover2);

            stockManagement.verifyStockApprovalLevel2(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();

    }
}







