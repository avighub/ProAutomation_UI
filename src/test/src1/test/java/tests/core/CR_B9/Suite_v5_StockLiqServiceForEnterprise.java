package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_v5_StockLiqServiceForEnterprise extends TestInit {

    private OperatorUser usrCreator, bankApprover, optuser, baruser;
    private User enterprsie;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            BankAccountAssociation.init(eSetup).createLiquidationBanks();

            enterprsie = new User(Constants.ENTERPRISE);

            ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(enterprsie, true);
            TransactionManagement.init(eSetup).makeSureChannelUserHasBalance(enterprsie);


            optuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");

            ServiceCharge stockliq = new ServiceCharge
                    (Services.STOCK_LIQUIDATION_SERVICE, enterprsie, optuser, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(stockliq);

            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            baruser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceAfterTransactorStatusIsChanged() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4780_1", "Verify that appropriate error message & error code is displayed if transactor status is changed during initiation of transaction.");

        try {
            //Create Operator User
            OperatorUser operatorUser = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(enterprsie, batchId);
            //Getting Liquidation Id
            String liqId = MobiquityGUIQueries.getLiquidationId(enterprsie.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));
            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationServiceforFail(enterprsie, operatorUser, liqId, Bankid);
            response.verifyMessage("transactor.barred.as.sender.and.receiver");
            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t1).stockLiquidationServiceforFail(enterprsie, operatorUser, liqId, Bankid);
            response1.verifyMessage("transactor.barred.as.sender.and.receiver");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceaftersenderIsBarredAsReceiver() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_14", "Verify that during initiation of transaction and transaction should be successfull if sender is barred as receiver.");

        try {
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(enterprsie, batchId);
            //Getting Liquidation Id
            String liqId = MobiquityGUIQueries.getLiquidationId(enterprsie.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));
            //Bar the User as Receiver
            Login.init(t1).login(baruser);
            ChannelUserManagement.init(t1).barChannelUser(enterprsie, Constants.USER_TYPE_CHANNEL, Constants.BAR_AS_RECIEVER);
            //Perform Stock Liquidation
            SfmResponse response1 = Transactions.init(t1).stockLiquidationService(enterprsie, baruser, liqId, Bankid);
            String status = response1.Status;
            //Unbar the User
            ChannelUserManagement.init(t1).unBarChannelUser(enterprsie, "CH_USERS", "Receiver");
            Assert.assertEquals(status, "SUCCEEDED");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceAfterSenderStatusIsSuspendInitiateAndSuspended() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4780_3", "Verify that appropriate error message & error code is displayed if sender is suspendinitiate/suspended during initiation of transaction.");

        try {
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(enterprsie, batchId);
            //Getting Liquidation Id

            String liqId = MobiquityGUIQueries.getLiquidationId(enterprsie.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));

            Login.init(t1).login(baruser);
            //Sender is suspend Initiated
            ChannelUserManagement.init(t1).suspendInitiateChannelUser(enterprsie);
            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationService(enterprsie, baruser, liqId, Bankid);
            String status = response.Status;
            Assert.assertEquals(status, "SUCCEEDED");
            Login.init(t1).login(baruser);
            //Sender is suspended
            ChannelUserManagement.init(t1).suspendChannelUserApproval(enterprsie);
            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t1).stockLiquidationServiceforFail(enterprsie, baruser, liqId, Bankid);
            String errormessage1 = response1.Message;
            Assert.assertEquals(errormessage1, "Sender has been suspended");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}
