package tests.core.CR_B9;

import framework.entity.User;

public class Suite_v5_ChurnRecord {
    private User user;
    private boolean churnChnl;
    private boolean churnSubs;

    public Suite_v5_ChurnRecord(User user, boolean churnChnl, boolean churnSubs) {
        this.user = user;
        this.churnChnl = churnChnl;
        this.churnSubs = churnSubs;
    }

    public User getUser() {
        return user;
    }

    public boolean isChurnChnl() {
        return churnChnl;
    }

    public boolean isChurnSubs() {
        return churnSubs;
    }
}
