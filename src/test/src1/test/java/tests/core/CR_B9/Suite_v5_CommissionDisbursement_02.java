package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page1;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.List;

public class Suite_v5_CommissionDisbursement_02 extends TestInit {

    private User usrWhs, usrRt;
    private Wallet commWallet;
    private BigDecimal minBalance;
    private OperatorUser usrOperator;
    private CurrencyProvider providerOne;

    @BeforeClass(alwaysRun = true)
    public void setupTest() throws Exception {
        ExtentTest t1 = pNode.createNode("Setup", "Setup Specific to this Suite");
        try {
            commWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
            providerOne = DataFactory.getDefaultProvider();
            minBalance = new BigDecimal("100");
            usrWhs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            usrRt = DataFactory.getChannelUserWithCategory(Constants.RETAILER);

            usrOperator = DataFactory.getOperatorUserWithAccess("COMMDISINITIATE", Constants.NETWORK_ADMIN);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(usrWhs, commWallet.WalletId, minBalance);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void MON_4231() throws Exception {

        ExtentTest t1 = pNode.createNode("MON-4231(b)",
                "To verify the commission disbursement should list the user with " +
                        "balance more than minimum amount entered in filter criteria");
        try {
            //get pre Balance of the User
            UsrBalance channelUsrBalance = MobiquityGUIQueries
                    .getUserBalance(usrWhs, commWallet.WalletId, providerOne.ProviderName);

            int preBalance = channelUsrBalance.Balance.intValue();
            int amt = preBalance + 1; // Adding 1 to make requested balance more than what user already has

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(usrOperator);

            new Commission_Disbursement_Page1(t1)
                    .navigateToLink()
                    .selectProvider(providerOne.ProviderName)
                    .SelectDomain(usrWhs.DomainName)
                    .SelectCategory(usrWhs.CategoryName);

            new Commission_Disbursement_Page1(t1)
                    .enterMinAmount(Integer.toString(amt))
                    .clickOnSubmit();
            //get the all the users from table
            List<String> userList = new Commission_Disbursement_Page2(t1).getAllUsers();

            //To verify the commission disbursement should list the user with balance more than minimum amount
            Assertion.verifyListNotContains(userList, usrWhs.LoginId,
                    "User not found since minimum amt entered is greater than user balance", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void test_2() throws Exception {

        ExtentTest t1 = pNode.createNode("MON-4232",
                "To verify the commission disbursement should list the user who belongs only to selected are & zone");

        try {

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(usrOperator);

            new Commission_Disbursement_Page1(t1).navigateToLink()
                    .selectProvider(providerOne.ProviderName)
                    .SelectDomain(usrWhs.DomainName)
                    .SelectCategory(usrWhs.CategoryName)
                    .selectZone(usrWhs.Geography.ZoneName)//will work fine once the channel user is created through baseSetup
                    .selectArea(usrWhs.Geography.AreaName)//will work fine once the channel user is created through baseSetup
                    .clickOnSubmit();

            //get the all the users from table
            List<String> userList = new Commission_Disbursement_Page2(t1).getAllUsers();

            //verify commission disbursement should display the user who belongs only to selected area & zone
            Assertion.verifyListContains(userList, usrWhs.LoginId,
                    "user who belongs to selected zone & area is displayed", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
