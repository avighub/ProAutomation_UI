package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : 4202
 * Author Name      : PushpaLatha
 * Created Date     : 25/07/2018
 */
public class Suite_v5_MON_4202 extends TestInit {

    private User whs;
    private String trustBank, nonTrustBank, provider, walletName;
    private OperatorUser net, bankApprover;
    private String trustBankId, nonTrustBankId;
    private MobiquityGUIQueries m;

    @BeforeClass()
    public void pre_condition() throws Exception {

        ExtentTest t1 = pNode.createNode("Setup",
                "Create users and service charge for the scripts");

        try {
            net = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            provider = DataFactory.getDefaultProvider().ProviderName;
            trustBank = DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName);
            nonTrustBank = DataFactory.getNonTrustBankName(DataFactory.getDefaultProvider().ProviderName);
            bankApprover = DataFactory.getOperatorUsersWithAccess("BNK_APR").get(0);
            walletName = DataFactory.getDefaultWallet().WalletName;
            trustBankId = DataFactory.getBankId(trustBank);
            nonTrustBankId = DataFactory.getBankId(nonTrustBank);
            m = new MobiquityGUIQueries();

            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANKING_SERVICES_FOR_TRUST_BANK");
            if (prefValue.equalsIgnoreCase("FALSE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("BANKING_SERVICES_FOR_TRUST_BANK", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            }

            whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, true);

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs);

            String defaultProvider = DataFactory.getDefaultProvider().ProviderName;

            String bank1 = DataFactory.getBankId(DataFactory.getNonTrustBankName(defaultProvider));
            String bank2 = DataFactory.getBankId(DataFactory.getTrustBankName(defaultProvider));

            SuperAdmin supradm1 = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            String encrptdPwd = MobiquityGUIQueries.dbGetEncryptedPwd(supradm1.LoginId);
            SfmResponse response = Transactions.init(t1).initiateEscrowToEscrowTransfer(supradm1, bank1, bank2, encrptdPwd, "100");

            String serviceReqId = response.ServiceRequestId;
            SuperAdmin supradc = DataFactory.getSuperAdminWithAccess("PTY_ASUA");

            SfmResponse appRes = Transactions.init(t1).escrowToEscrowTransferApproval(supradc, serviceReqId);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void verifyThatChannelUserIsAbleToPerformBanktoSvaWhenPreferenceIsTrue() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("SC01", "Verify that channel user is able to perform Bank to SVA transfer for trust banks when 'Banking Services for Trust Bank' is set to True");

        try {
            //Setting preference BANKING_SERVICES_FOR_TRUST_BANK to TRUE
            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANKING_SERVICES_FOR_TRUST_BANK");
            if (prefValue.equalsIgnoreCase("FALSE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("BANKING_SERVICES_FOR_TRUST_BANK", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            }

            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, trustBankId);
            String channaccount = AccountNum.get("ACCOUNT_NO");
            //decrypt the account no.
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(channaccount);

            ServiceCharge b2W = new ServiceCharge(Services.BANK_TO_WALLET, whs, whs, trustBankId, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(b2W);

            SfmResponse res = Transactions.init(t1).bankToWalletService(whs, "2", trustBankId, trustAccNo);
            String serReqId = res.ServiceRequestId;
            res.verifyMessage("bank.to.SVA.success", res.TransactionId);

            Transactions.init(t1).performEIGTransaction(serReqId, "true", Services.RESUME_BANK_TO_WALLET);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("SC02", "Verify that channel user is able to perform Bank to SVA transfer for non trust banks when 'Banking Services for Trust Bank' is set to True");

        try {

            Map<String, String> AccountNum1 = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, nonTrustBankId);
            String channaccount1 = AccountNum1.get("ACCOUNT_NO");

            //decrypt the account no.
            DesEncryptor d1 = new DesEncryptor();
            String nonTrustAccNo = d1.decrypt(channaccount1);

            ServiceCharge b2W1 = new ServiceCharge(Services.BANK_TO_WALLET, whs, whs, nonTrustBankId, null, null, null);
            TransferRuleManagement.init(t2).configureTransferRule(b2W1);

            SfmResponse res1 = Transactions.init(t2).bankToWalletService(whs, "2", nonTrustBankId, nonTrustAccNo);
            String serReqId1 = res1.ServiceRequestId;
            res1.verifyMessage("bank.to.SVA.success", res1.TransactionId);

            Transactions.init(t2).performEIGTransaction(serReqId1, "true", Services.RESUME_BANK_TO_WALLET);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void verifyThatChannelUserIsAbleToPerformSVAtoBankWhenPreferenceIsTrue() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("SC03", "Verify that channel user is able to perform SVA to Bank transfer for trust banks when 'Banking Services for Trust Bank' is set to True");

        try {
            //Setting preference BANKING_SERVICES_FOR_TRUST_BANK to TRUE
            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANKING_SERVICES_FOR_TRUST_BANK");
            if (prefValue.equalsIgnoreCase("FALSE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("BANKING_SERVICES_FOR_TRUST_BANK", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            }

            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, trustBankId);
            String channaccount = AccountNum.get("ACCOUNT_NO");
            //decrypt the account no.
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(channaccount);

            ServiceCharge sCharge = new ServiceCharge(Services.SVA_TO_BANK, whs, whs, null, trustBankId, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(sCharge);

            Login.init(t1).login(whs);
            String transID = TransactionManagement.init(t1).initiateSVAtoBank(whs, Constants.SVA_TRANSFER_NOW, provider, walletName, trustBank, Constants.SVA_AMOUNT, trustAccNo);

            String reqID = MobiquityGUIQueries.dbGetServiceRequestId(transID);

            Transactions.init(t1).performEIGTransaction(reqID, "true", Services.RESUME_WALLET_TO_BANK);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        ExtentTest t2 = pNode.createNode("SC04", "Verify that channel user is able to perform SVA to Bank transfer for non trust banks when 'Banking Services for Trust Bank' is set to True");

        try {
            //Non Trust Bank
            Map<String, String> AccountNum1 = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, nonTrustBankId);
            String channaccount1 = AccountNum1.get("ACCOUNT_NO");
            //decrypt the account no.
            DesEncryptor d1 = new DesEncryptor();
            String nonTrustAccNo = d1.decrypt(channaccount1);

            ServiceCharge sCharge1 = new ServiceCharge(Services.SVA_TO_BANK, whs, whs, null, nonTrustBankId, null, null);

            TransferRuleManagement.init(t2)
                    .configureTransferRule(sCharge1);

            Login.init(t1).login(whs);
            String transID1 = TransactionManagement.init(t2).initiateSVAtoBank(whs, Constants.SVA_TRANSFER_NOW, provider, walletName, nonTrustBank, Constants.SVA_AMOUNT, nonTrustAccNo);

            String reqID1 = MobiquityGUIQueries.dbGetServiceRequestId(transID1);

            Transactions.init(t2).performEIGTransaction(reqID1, "true", Services.RESUME_WALLET_TO_BANK);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void verifyThatChannelUserIsAbleToPerformStockLiquidationServiceWhenPreferenceIsTrue() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("SC05", "Verify that channel user is able to perform Stock Liquidation service for trust banks when 'Banking Services for Trust Bank' is set to True");

        try {
            //Setting preference BANKING_SERVICES_FOR_TRUST_BANK to TRUE
            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANKING_SERVICES_FOR_TRUST_BANK");
            if (prefValue.equalsIgnoreCase("FALSE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("BANKING_SERVICES_FOR_TRUST_BANK", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            }


            /***********FOR TRUST BANK*******************/

            ServiceCharge sCharge = new ServiceCharge(Services.STOCK_LIQUIDATION_SERVICE, whs, net, null, trustBankId, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(sCharge);

            //Generate Liquidation Id
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(whs, batchId);

            //Get the generated Liquidation Id
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String liqId = MobiquityGUIQueries.getLiquidationId(whs.MSISDN, batchId);

            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationService(whs, net, liqId, trustBankId);
            response.verifyStatus(Constants.TXN_STATUS_SUCCEEDED);

            Transactions.init(t1).performEIGTransaction(response.ServiceRequestId, "true", Services.RESUME_STOCK_LIQUIDATION_SERVICE);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        ExtentTest t2 = pNode.createNode("SC06", "Verify that channel user is able to perform Stock Liquidation service for non trust banks when 'Banking Services for Trust Bank' is set to True");
        try {

            ServiceCharge sCharge1 = new ServiceCharge(Services.STOCK_LIQUIDATION_SERVICE, whs, net, null, nonTrustBankId, null, null);

            TransferRuleManagement.init(t2)
                    .configureTransferRule(sCharge1);

            //Generate Liquidation Id
            String batchId1 = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t2).stockLiquidationGeneration(whs, batchId1);

            //Get the generated Liquidation Id
            String liqId1 = MobiquityGUIQueries.getLiquidationId(whs.MSISDN, batchId1);

            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t2).stockLiquidationService(whs, net, liqId1, nonTrustBankId);
            response1.verifyStatus(Constants.TXN_STATUS_SUCCEEDED);

            Transactions.init(t2).performEIGTransaction(response1.ServiceRequestId, "true", Services.RESUME_STOCK_LIQUIDATION_SERVICE);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void verifyThatChannelUserIsAbleToPerformBanktoSvaWhenPreferenceIsFalse() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("SC07", "Verify that channel user is not able to perform Bank to SVA transfer for trust banks when 'Banking Services for Trust Bank' is set to False");

        try {
            //Setting preference BANKING_SERVICES_FOR_TRUST_BANK to TRUE
            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANKING_SERVICES_FOR_TRUST_BANK");
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "FALSE");

            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, trustBankId);
            String channaccount = AccountNum.get("ACCOUNT_NO");
            //decrypt the account no.
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(channaccount);

            ServiceCharge b2W = new ServiceCharge(Services.BANK_TO_WALLET, whs, whs, trustBankId, null, null, null);
            TransferRuleManagement.init(pNode).configureTransferRule(b2W);

            SfmResponse res = Transactions.init(t1).bankToWalletService(whs, "2", trustBankId, trustAccNo, false);
            res.verifyMessage("allowed.only.for.partner.bank");

            ExtentTest t2 = pNode.createNode("SC08", "Verify that channel user is able to perform Bank to SVA transfer for non trust banks when 'Banking Services for Trust Bank' is set to False");
            Map<String, String> AccountNum1 = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, nonTrustBankId);
            String channaccount1 = AccountNum1.get("ACCOUNT_NO");
            //decrypt the account no.
            DesEncryptor d1 = new DesEncryptor();
            String nonTrustAccNo = d1.decrypt(channaccount1);

            ServiceCharge b2W1 = new ServiceCharge(Services.BANK_TO_WALLET, whs, whs, nonTrustBankId, null, null, null);
            TransferRuleManagement.init(t2).configureTransferRule(b2W1);

            SfmResponse res1 = Transactions.init(t2).bankToWalletService(whs, "2", nonTrustBankId, nonTrustAccNo);
            String serReqId1 = res1.ServiceRequestId;
            res1.verifyMessage("bank.to.SVA.success", res1.TransactionId);

            Transactions.init(t2).performEIGTransaction(serReqId1, "true", Services.RESUME_BANK_TO_WALLET);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void verifyThatChannelUserIsAbleToPerformSVAtoBankWhenPreferenceIsFalse() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("SC09", "Verify that channel user is not able to perform SVA to Bank transfer for trust when 'Banking Services for Trust Bank' is set to False");

        try {
            //Setting preference BANKING_SERVICES_FOR_TRUST_BANK to TRUE
            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANKING_SERVICES_FOR_TRUST_BANK");
            if (prefValue.equalsIgnoreCase("TRUE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("BANKING_SERVICES_FOR_TRUST_BANK", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "FALSE");
            }

            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, trustBankId);
            String channaccount = AccountNum.get("ACCOUNT_NO");
            //decrypt the account no.
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(channaccount);

            ServiceCharge sCharge = new ServiceCharge(Services.SVA_TO_BANK, whs, whs, null, trustBankId, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(sCharge);

            Login.init(t1).login(whs);
            startNegativeTest();
            TransactionManagement.init(t1).initiateSVAtoBank(whs, Constants.SVA_TRANSFER_NOW, provider, walletName, trustBank, Constants.SVA_AMOUNT, trustAccNo);
            Assertion.verifyErrorMessageContain("allowed.only.for.partner.bank", "Not allowed for trust banks", t1);
            stopNegativeTest();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        ExtentTest t2 = pNode.createNode("SC10", "Verify that channel user is able to perform SVA to Bank transfer for non trust banks when 'Banking Services for Trust Bank' is set to False");
        try {
            //Non Trust Bank
            Map<String, String> AccountNum1 = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, nonTrustBankId);
            String channaccount1 = AccountNum1.get("ACCOUNT_NO");
            //decrypt the account no.
            DesEncryptor d1 = new DesEncryptor();
            String nonTrustAccNo = d1.decrypt(channaccount1);

            ServiceCharge sCharge1 = new ServiceCharge(Services.SVA_TO_BANK, whs, whs, null, nonTrustBankId, null, null);

            TransferRuleManagement.init(t2)
                    .configureTransferRule(sCharge1);

            Login.init(t2).login(whs);
            String transID1 = TransactionManagement.init(t2).initiateSVAtoBank(whs, Constants.SVA_TRANSFER_NOW, provider, walletName, nonTrustBank, Constants.SVA_AMOUNT, nonTrustAccNo);

            String reqID1 = MobiquityGUIQueries.dbGetServiceRequestId(transID1);

            Transactions.init(t2).performEIGTransaction(reqID1, "true", Services.RESUME_WALLET_TO_BANK);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANKING_SERVICES_FOR_TRUST_BANK");
            if (prefValue.equalsIgnoreCase("FALSE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("BANKING_SERVICES_FOR_TRUST_BANK", "Y");

                SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            }
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void verifyThatChannelUserIsAbleToPerformStockLiquidationServiceWhenPreferenceIsFalse() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("SC11", "Verify that channel user is not able to perform Stock Liquidation transfer for trust banks when 'Banking Services for Trust Bank' is set to False");

        try {
            //Setting preference BANKING_SERVICES_FOR_TRUST_BANK to TRUE
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "FALSE");

            /***********FOR TRUST BANK*******************/

            //Generate Liquidation Id
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(whs, batchId);

            //Get the generated Liquidation Id
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String liqId = MobiquityGUIQueries.getLiquidationId(whs.MSISDN, batchId);

            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationServiceforFail(whs, net, liqId, trustBankId);
            response.verifyMessage("allowed.only.for.partner.bank", "Not allowed for Non partner banks");

            ExtentTest t2 = pNode.createNode("SC12", "Verify that channel user is able to perform Stock Liquidation transfer for non trust banks when 'Banking Services for Trust Bank' is set to False");

            //Generate Liquidation Id
            String batchId1 = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t2).stockLiquidationGeneration(whs, batchId1);

            //Get the generated Liquidation Id
            String liqId1 = MobiquityGUIQueries.getLiquidationId(whs.MSISDN, batchId1);

            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t2).stockLiquidationService(whs, net, liqId1, nonTrustBankId);
            response1.verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            response1.verifyMessage("stock.liquidation.ambiguous", response1.TransactionId);

            Transactions.init(t2).performEIGTransaction(response1.ServiceRequestId, "true", Services.RESUME_STOCK_LIQUIDATION_SERVICE);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
        }
        Assertion.finalizeSoftAsserts();
    }

}
