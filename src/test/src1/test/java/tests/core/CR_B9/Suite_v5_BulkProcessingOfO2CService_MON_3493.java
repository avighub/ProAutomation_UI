package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import com.google.common.collect.ImmutableMap;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.RechargeOperator;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.bulkPayoutTool.BulkPayoutInitiate_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setTxnConfig;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_BankAccountRegistrationWEB_Negative_MON_4443
 * Author Name      : Pushpalatha
 * Created Date     : 18/07/2018
 */

public class Suite_v5_BulkProcessingOfO2CService_MON_3493 extends TestInit {

    private ArrayList<String> errors = new ArrayList<String>();
    private RechargeOperator RCoperator;
    private ArrayList<String> transactionIds = new ArrayList<String>();
    private String prefValue;
    private User enterp;
    private OperatorUser opt, optUser, optUserApp;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {

            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("CURRENCY_FACTOR");

            enterp = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            if (enterp == null) {
                enterp = new User(Constants.ENTERPRISE);
                ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(enterp, false);
            }

            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            optUser = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            optUserApp = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE");


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void verifyThatO2CIsSuccessfulThroughBulkPayoutTool() throws Exception {
        ExtentTest t1 = pNode.createNode("SC01",
                "Verify that O2C is successful through Bulk Payout tool");

        try {
            //fetch the previous balance of users
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(enterp, null, null);
            int preWBalance = preWhsBalance.Balance.intValue();

            //fetch the previous balance of operator
            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            String fileName = BulkPayoutTool.init(t1).generateFileForO2C(DataFactory.getDefaultProvider().ProviderId, DataFactory.getDefaultWallet().WalletId, enterp, "3");

            Login.init(pNode).login(opt);

            //Initiate bulk payout for Stock Reimbursement
            String id = BulkPayoutTool.init(t1).initiateNewBulkPayout("O2C transfer", fileName);
            Thread.sleep(5000);

            //Login with user who has bulk payout approve access
            Login.init(t1).login(optUserApp);

            //Approve bulk payout
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout("O2C transfer", id, true);

            //check for the status in BulkPayout Dashboard screen
            Login.init(t1).login(optUserApp);
            String file = BulkPayoutTool.init(t1).verifyBulkPayoutDashboard("O2C transfer", id, true, true);

            ArrayList<String> idValue = new ArrayList<String>();
            idValue.add(enterp.MSISDN);
            idValue.add("8856458758");

            for (int i = 0; i < idValue.size(); i++) {
                String error = BulkPayoutTool.init(t1).checkLog(i, idValue.get(i), file);
                errors.add(error);
            }

            Assert.assertEquals(errors.get(1), "Receiver is invalid");

            //fetch the previous balance of users
            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalance(enterp, null, null);
            int postWBalance = postWhsBalance.Balance.intValue();

            //fetch the previous balance of operator
            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue() - 3);
            Assert.assertEquals(postWBalance, preWBalance + 3);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void verifyThatBulkProcessingForO2CIsNotSuccessfulWhenMandatoryFieldsAreNotEntered() throws Exception {
        ExtentTest t1 = pNode.createNode("SC02",
                "Verify that bulk processing for O2C is not successful when mandatory fields are not entered");

        try {
            String fileName = BulkPayoutTool.init(t1).generateErrorFileForO2C(DataFactory.getDefaultProvider().ProviderId, DataFactory.getDefaultWallet().WalletId, enterp, "3");

            Login.init(t1).login(opt);

            //Initiate bulk payout for Stock Reimbursement
            String id = BulkPayoutTool.init(t1).startNegativeTest().initiateNewBulkPayout("O2C transfer", fileName);
            Thread.sleep(5000);
            String actual = BulkPayoutInitiate_page1.init(t1).verfiy_Errormessage();

            Assertion.verifyMessageContain(actual, "bulk.payout.initiate.failed", "Bulk payout initiate failed", t1);

            errors = BulkPayoutInitiate_page1.init(t1).downloadErrorStatusFile(t1);

            Assert.assertEquals(errors.get(0), "Receiver SVA Type ID is mandatory and should be numeric");
            Assert.assertEquals(errors.get(1), "Payment Type is mandatory and should be one of the possible value(Cheque|Demand Draft|Cash|Others) in case sensitive format.");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, enabled = false, groups = {FunctionalTag.v5_0})
    public void verifyThatReversalIsSuccessfulThroughBulkPayoutTool() throws Exception {
        ExtentTest t1 = pNode.createNode("SC03",
                "Verify that reversal is successful through bulk payout tool");
        try {
            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.O2C + "," + Services.CASHIN + "," + Services.Recharge_Others + "," + Services.AgentAssistedBillPayment));
            setTxnConfig(of(
                    "txn.reversal.allowed.upto.seconds", "3600000"
            ));

            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            List<OperatorUser> opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN);

            //getting OperatorID
            RCoperator = OperatorUserManagement.init(t1)
                    .getRechargeOperator(defaultProvider.ProviderId);

            Biller biller = BillerManagement.init(t1).getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);
            Biller onlineBiller = BillerManagement.init(t1).getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            User rechargeReceiver = new User(Constants.ZEBRA_MER);

            //O2C
            SfmResponse o2cResponse = Transactions.init(t1).initiateO2C(opt.get(0), whs, "50", DataFactory.getDefaultWallet().WalletId, defaultProvider.ProviderId);
            SfmResponse o2cResponse1 = Transactions.init(t1).approveO2C(opt.get(1), o2cResponse.ServiceRequestId);
            Transactions.init(t1).approveO2C(opt.get(2), o2cResponse1.ServiceRequestId);
            transactionIds.add(o2cResponse.TransactionId);
            Thread.sleep(1000);

            //Cashin
            SfmResponse cashinResponse = Transactions.init(pNode).initiateCashIn(subs, whs, "5");
            transactionIds.add(cashinResponse.TransactionId);
            Thread.sleep(1000);

            //Recharge Others
            //User rechargeReceiver = new User(Constants.ZEBRA_MER);
            SfmResponse rcResponse = Transactions.init(pNode).rechargeMobilebychanneluser(opt.get(0), whs, rechargeReceiver, 20, RCoperator.OperatorId);
            //Moving transaction from ambiguous to TS
            Transactions.init(pNode).performEIGTransaction(rcResponse.ServiceRequestId, "true", Services.Resume_Recharge_Others);
            transactionIds.add(rcResponse.TransactionId);
            Thread.sleep(1000);

            //Offline Bill Payment
            String accNo = DataFactory.getRandomNumberAsString(5);
            ServiceCharge adhocBill = new ServiceCharge(Services.AgentAssistedBillPayment, whs, biller, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(adhocBill);
            SfmResponse offlineBillResponse = Transactions.init(t1).initiateOfflineBillPaymentForAdhoc(biller, whs, "6", accNo);
            transactionIds.add(offlineBillResponse.TransactionId);
            Thread.sleep(1000);

            //Online Bill payment
            SfmResponse onlineBillResponse = Transactions.init(t1).initiateOfflineBillPaymentForAdhoc(onlineBiller, whs, "6", accNo);
            Transactions.init(pNode).performEIGTransaction(onlineBillResponse.ServiceRequestId, "true", Services.RESUME_BILLPAY_ADHOC);
            transactionIds.add(onlineBillResponse.TransactionId);
            Thread.sleep(1000);

            //Define transfer rule for revrsal of
            //O2C
            ServiceCharge o2c = new ServiceCharge(Services.TXN_CORRECTION, opt.get(0), whs, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(o2c);
            //cashin
            ServiceCharge cashIn = new ServiceCharge(Services.TXN_CORRECTION, subs, whs, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(cashIn);
            //RechargeOthers
            ServiceCharge rc = new ServiceCharge(Services.TXN_CORRECTION, rechargeReceiver, whs, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(rc);
            //BillPayment
            ServiceCharge billPay = new ServiceCharge(Services.TXN_CORRECTION, biller, whs, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(billPay);

            //Generate Bulk payout file for reversal
            String fileName = BulkPayoutTool.init(t1).generateFileForReversal(transactionIds, "False", "False", "False");

            Login.init(pNode).login(opt.get(0));

            //Initiate bulk payout for Stock Reimbursement
            String id = BulkPayoutTool.init(t1).initiateNewBulkPayout("Transaction Reversal", fileName);
            Thread.sleep(5000);

            //Login with user who has bulk payout approve access
            Login.init(t1).loginAsSuperAdmin("BULK_APPROVE");

            //Approve bulk payout
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout("Transaction Reversal", id, true);

            //check for the status in BulkPayout Dashboard screen
            Login.init(t1).loginAsSuperAdmin("BULK_APPROVE");
            String file = BulkPayoutTool.init(t1).verifyBulkPayoutDashboard("Transaction Reversal", id, true, true);

            for (int i = 0; i < transactionIds.size(); i++) {
                BulkPayoutTool.init(t1).checkLog(i, transactionIds.get(i), file);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}
