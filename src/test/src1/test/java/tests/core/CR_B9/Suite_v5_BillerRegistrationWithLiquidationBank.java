/*
package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.Liquidation;
import framework.entity.OperatorUser;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.billerManagement.BillerApproveInitiate_pg1;
import framework.pageObjects.billerManagement.BillerRegisteration_pg2;
import framework.pageObjects.billerManagement.BillerRegistrationPage1;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.AddModifyApproveBank;
import framework.pageObjects.userManagement.LiquidationDetailsApprovalPage;
import framework.pageObjects.userManagement.LiquidationDetailsConfirmPage;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;

*/
/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Modify ChannelUser with LiquidationDetails
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 13/04/2018
 *//*


public class Suite_v5_BillerRegistrationWithLiquidationBank extends TestInit {
    private String isBankReqPref, displayAllowedReg, modifyAllowedReg, providerName;
    private OperatorUser billerRegistor, bankApprover, billerRegApprovar;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "BankId must be Requried to Create User With Liquidation Details");

        BankAccountAssociation.init(eSetup).createLiquidationBanks();

        providerName = DataFactory.getDefaultProvider().ProviderName;
        billerRegistor = DataFactory.getOperatorUserWithAccess("UTL_CREG");
        bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
        billerRegApprovar = DataFactory.getOperatorUserWithAccess("UTL_CAPP");

        //Setting Preference
        isBankReqPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_BANK_REQUIRED");
        displayAllowedReg = MobiquityGUIQueries.fetchDisplayAllowedOnGUI("IS_BANK_REQUIRED");
        modifyAllowedReg = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("IS_BANK_REQUIRED");


        if (displayAllowedReg.equalsIgnoreCase("N") || modifyAllowedReg.equalsIgnoreCase("N")) {
            MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_BANK_REQUIRED", "Y");
        }

        if (!isBankReqPref.equalsIgnoreCase("true")) {
            SystemPreferenceManagement.init(eSetup).updateSystemPreference("IS_BANK_REQUIRED", "TRUE");
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void createBillerWithLiquidationDetails() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4772_1",
                "To verify that Biller is created with Liquidation Details");
        try {
            //Creating a biller
            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            BillerManagement.init(t1).createBillerWithLiquidationBank(biller);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(t1)
                    .approveAllAssociatedLiquidationBanksForBiller(biller);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void liquidationBankButtonShouldBeAvailablewhenIS_BANK_REQUIREDpreferenceisTrue() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4772_2/3",
                ".Verify liquidation bank details(Add liquidation bank button) should be populated in Bill Pay-->>Biller Registration-->>Linked bank page when IS_BANK_REQUIRED preference is True ");
        try {
            //Creating a biller

            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            Login.init(t1).login(billerRegistor);


            BillerRegisteration_pg2 page1 = new BillerRegisteration_pg2(t1);

            startNegativeTestWithoutConfirm();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller);

            Assertion.verifyEqual(page1.isLiquidationFieldDisplayed(), true,
                    "Verify that the Liquidation Fields are displayed", pNode, true);

            page1.liquidationclick();

            Object[] expected = PageInit.fetchLabelTexts("(//tbody[@id='liquidationDetails']/tr[1]/td/b)[position()<7]");
            Object[] actual = UserFieldProperties.getLabels("liquidation.details");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("label Validated Successfully: " + expected[i]);
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void billerWithMultipleLiquidationDetailsWithSameMFSProvideNotAllowed() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4772_7",
                "To verify that Biller is not created with same MFS Provider");

        //Creating a biller
        try {
            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            Login.init(t1).login(billerRegistor);


            BillerRegisteration_pg2 page1 = new BillerRegisteration_pg2(t1);
            AddChannelUser_pg5 page2 = new AddChannelUser_pg5(t1);

            startNegativeTestWithoutConfirm();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller);

            */
/*
 * Map all the provides
 *//*



            for (int i = 0; i < 2; i++) {


                page1.liquidationclick();

                WebElement liqaccountNum = driver.findElement(By.name("liquidationBankAccountNo1[" + i + "]"));
                WebElement liqbranchnam = driver.findElement(By.name("liquidationBranchName1[" + i + "]"));
                WebElement liqaccountholdernam = driver.findElement(By.name("liquidationAccHolderName1[" + i + "]"));
                WebElement freqtradingnam = driver.findElement(By.name("liquidationTradingName1[" + i + "]"));

                // UI elements

                new Select(driver.findElement(By.id("liquidationProvider" + i + ""))).selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
                new Select(driver.findElement(By.id("liquidationBankListId" + i + ""))).selectByVisibleText(DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));
                Liquidation liq = new Liquidation();
                liqaccountNum.sendKeys(liq.LiquidationBankAccountNumber);
                liqbranchnam.sendKeys(liq.LiquidationBankBranchName);
                liqaccountholdernam.sendKeys(liq.AccountHolderName);
                freqtradingnam.sendKeys(liq.FrequencyTradingName);

            }
            page2.selectLiquidationFrequency(biller.LiquidationFrequency, biller.LiquidationDay);
            page1.clickButtonSubmit();
            AlertHandle.acceptAlert(t1);
            AlertHandle.acceptAlert(t1);
            Assertion.verifyErrorMessageContain("LiquidationBank.one.permfsprovider", "Only one Bank per mfs provider is allowed", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void liquidationdetailsinConfirmAndApprovepage() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4772_9",
                "To Verify Liquidation bank details should be populated in Confirm page.");
        try {
            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            Login.init(t1).login(billerRegistor);

            BillerRegisteration_pg2 page1 = new BillerRegisteration_pg2(t1);
            BillerRegistrationPage1 page2 = new BillerRegistrationPage1(t1);

            startNegativeTestWithoutConfirm();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller);
            page1.addBankForBiller(biller);
            page1.clickButtonSubmit();
            AlertHandle.acceptAlert(t1);
            AlertHandle.acceptAlert(t1);
            //Verify Details in Confirm Page
            String liquidationBankAccountNumber = biller.getLiquidationBankAccountNumber();
            String liquidationBankBranchName = biller.getLiquidationBankBranchName();
            String accountHolderName = biller.getAccountHolderName();
            String frequencyTradingName = biller.getFrequencyTradingName();

            //Verify Details In Confirm Page

            LiquidationDetailsConfirmPage liqCon = new LiquidationDetailsConfirmPage(t1);
            liqCon.verifyHeadersDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), liquidationBankAccountNumber, liquidationBankBranchName, accountHolderName, frequencyTradingName);

            page2.clickButtonSubmit();

            Login.init(t1).login(billerRegApprovar);

            BillerApproveInitiate_pg1 page = new BillerApproveInitiate_pg1(t1);
            page.navBillerApproveInitiate();
            page.clickOnApproveOrReject(biller.BillerCode, true);

            LiquidationDetailsApprovalPage liqApp = new LiquidationDetailsApprovalPage(t1);
            liqApp.verifyHeadersDetailsInBillerApprovalPage(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), liquidationBankAccountNumber, liquidationBankBranchName, accountHolderName, frequencyTradingName);
            t1.pass("Fields Are Validated");

            page.clickApproveBiller();
            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1).approveAllAssociatedLiquidationBanksForBiller(biller);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void channeladminrejectsbankonapproveBankpage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4772_15", " Verify Channel Admin able to reject the liquidation Bank account");

        try {
            //Biller creation
            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            BillerManagement.init(t1).createBillerWithLiquidationBank(biller);


            Login.init(t1).login(bankApprover);
            //Reject Bank
            AddModifyApproveBank pageOne = AddModifyApproveBank.init(t1);
            pageOne.navAddModifyDeleteBank();
            pageOne.selectProvider(DataFactory.getDefaultProvider().ProviderName);
            pageOne.selectBank(DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));
            pageOne.setFromDate();
            pageOne.setToDate();
            pageOne.clickSubmit();
            pageOne.checkBanksForUser(biller.LoginId);
            pageOne.reject();

            Assertion.verifyActionMessageContain("Bank.reject.message", "Bank has been rejected", t1, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));
            String status = MobiquityGUIQueries.getStatusOfLiquidationBankForBiller(biller.BillerCode);
            String status1 = MobiquityGUIQueries.getBankAccountStatusWithLoginId(biller.LoginId);

            DBAssertion.verifyDBAssertionEqual(status, "N", "Verify Bank Status", t1);
            DBAssertion.verifyDBAssertionEqual(status1, "N", "Verify Bank Status", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void liquidationBankButtonShouldNotBeAvailablewhenIS_BANK_REQUIREDpreferenceisFalse() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4772_3",
                ".Verify liquidation bank details(Add liquidation bank button) should be populated in Bill Pay-->>Biller Registration-->>Linked bank page when IS_BANK_REQUIRED preference is True ");

        //Creating a biller
        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_BANK_REQUIRED", "FALSE");

            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            Login.init(t1).login(billerRegistor);


            BillerRegisteration_pg2 page1 = new BillerRegisteration_pg2(t1);

            startNegativeTestWithoutConfirm();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller);

            Assertion.verifyEqual(page1.isLiquidationFieldDisplayed(), false,
                    "Verify that the Liquidation Fields Should not be displayed, " +
                            "as preference IS_BANK_REQUIRED is Set o False", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).
                    updateSystemPreference("IS_BANK_REQUIRED", "TRUE");
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {

        ExtentTest tTeardown = pNode.createNode("Teardown", "After Class Methods for Current Suite");

        SystemPreferenceManagement.init(tTeardown).updateSystemPreference("IS_BANK_REQUIRED", isBankReqPref);

        AppConfig.init(); // re-initialize the appconfig

    }

}







*/
