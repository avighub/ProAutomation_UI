package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.bankMaster.AddBank_Page1;
import framework.pageObjects.mfsProviderBankTypeMaster.ModifyDeleteBank_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : SuperAdmin should be able to Add NonPartnerBank
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 19/02/2018
 */

public class Suite_v5_SuperAdminAddingNonPartnerBank extends TestInit {

    private Bank nonPartnerBank;

    @BeforeClass(alwaysRun = true)
    public void setup() throws IOException {
        List<CurrencyProvider> providerWithLiqBank = DataFactory.getProviderWithLiquidationBank();

    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void createnonPartnerBank() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4090_1",
                "Verify super admin(checker) able to add non-partner bank(Liquidation bank) type");

        nonPartnerBank = new Bank(defaultProvider.ProviderId,
                "NPB" + DataFactory.getRandomNumberAsString(3),
                null,
                null,
                false,
                true);

        try {
            // login as superadmin with Add bank Rights
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");
            //Creating NonPartnerBank
            CurrencyProviderMapping.init(t1)
                    .addBank(nonPartnerBank);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void nonPresenceOfNonPartnerBankInBankTypeMaster() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_4090_3",
                "Verify Non-partner bank(Liquidation Bank) should not be populated in " +
                        "MFS Provider Bank Type Master>>add bank ");

        try {

            // login as superadmin with Add bank Rights
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            //Verifying presence of NonpartnerBank in BankTypeMaster
            AddBank_Page1 page2 = AddBank_Page1.init(t1);
            page2.navMFSProviderBankTypeMaster();
            List<WebElement> bankelements = driver.findElements(By.xpath("(//form[@id='MfsBankMapping_inputBank']/table/tbody//td[1])[position()>=4 and position()<last()]"));

            for (WebElement bankname1 : bankelements) {
                String bankname1a = bankname1.getText();
                if (bankname1a.equals(nonPartnerBank.BankName)) {
                    t1.fail("Liquidation Bank Name is available");
                }
            }
            t1.pass("BankName is not present in AddTypemaster");

            //Verifying presence of NonpartnerBank in ModifyTypeMaster
            ExtentTest t2 = pNode.createNode("MON_4090_4", "Verify Non-partner bank(Liquidation Bank) should not be populated in MFS Provider Bank Type Master>>modify bank ");

            ModifyDeleteBank_Page1 page = new ModifyDeleteBank_Page1(t2);
            page.navigateToModifyDeleteBank();
            driver.findElement(By.xpath(".//*[text()='" + nonPartnerBank.ProviderName + "']")).click();
            page.initiateModify();

            List<WebElement> bankelements1 = driver.findElements(By.xpath("(//form[@id='MfsBankMapping_modifyBankMapping']/table/tbody//td[1])[position()>=3 and position()<last()]"));

            for (WebElement bankname2 : bankelements1) {
                String bankname2a = bankname2.getText();
                if (bankname2a.equals(nonPartnerBank.BankName)) {
                    Assert.fail();
                }
            }
            t2.pass("BankName is not present in ModifyTypemaster");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0}, enabled = false)
    public void bankIdAppearsWithoutEntering() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4090_6",
                "Verify While adding Non-partner bank type," +
                        "System should generated Bank Id when you don't enter Bank id value if preference IS_BANKID_REQ is N");
        Bank bank_06 = null;
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_BANKID_REQ", "N");

            bank_06 = new Bank(defaultProvider.ProviderId,
                    "TMPNB" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    false,
                    true);

            // login as superadmin with Add bank Rights
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");
            CurrencyProviderMapping.init(t1)
                    .addBank(bank_06);

            String bankId = MobiquityGUIQueries.getBankId(bank_06);

            Assertion.verifyEqual((bankId != null), true, "Verify that Bank Id is generated By itself", t1);
            t1.info("Bank id is:" + bankId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {

            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_BANKID_REQ", "Y");

            Login.init(t1)
                    .loginAsSuperAdmin("BANK_DELETE");

            CurrencyProviderMapping.init(t1)
                    .deleteBank(bank_06.BankName);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void bankIdMandatory() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4090_7", "Verify While adding Non-partner bank type,entering the bank id value is mandatory when preference IS_BANKID_REQ is Y and proper error message is thrown when not entered");

        Bank bank_07 = null;
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_BANKID_REQ", "Y");

            //Creating NonPartnerBank
            bank_07 = new Bank(defaultProvider.ProviderId,
                    "TMPNB" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    false,
                    true);

            // set the bank Id as empty
            bank_07.BankID = "";

            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            startNegativeTest();
            CurrencyProviderMapping.init(t1)
                    .addBank(bank_07);

            //Verifying if  Bank Id is Mandatory
            Assertion.verifyErrorMessageContain("Bank.id.mandatory", "Verify that when IS_BANKID_REQ is set to 'Y' bank id is Mandatory", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void disableOfPoolAccountAndCBSField() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4090_8",
                "Verify While adding Non-partner bank type,Pool account details(pool account number,pool account type and CBS type) should be disabled.");
        try {
            //Creating NonPartnerBank
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            AddBank_Page1 page1 = AddBank_Page1.init(t1);

            page1.navAddBank();
            page1.selectProviderName(nonPartnerBank.ProviderName);
            page1.setBankName(nonPartnerBank.BankName);
            page1.selectBankType(nonPartnerBank.TrustType);

            Assertion.verifyEqual(page1.isdisppoolaccounttype(), false, "Verify that Pool account details are not available", t1, true);
            Assertion.verifyEqual(page1.isdispcbstype(), false, "Verify that CBS Type account details are not available", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.v5_0})
    public void createSamenonPartnerBankForDifferentMFSProvider() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4090_2",
                "Verify that same bank name should not be allowed to add for different mfs providers");

        try {
            nonPartnerBank.setProviderId("101", "MFS2");

            startNegativeTest();

            CurrencyProviderMapping.init(t1)
                    .addBank(nonPartnerBank);

            Assertion.verifyErrorMessageContain("bankName.alreay.Exist", "Verify that same bank name should not be allowed to add for different mfs providers", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void deletenonPartnerBank() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4090_5", "Verify super admin(checker) able to delete non-partner bank(Liquidation bank) type");

        try {
            CurrencyProviderMapping.init(t1).deleteBank(nonPartnerBank.BankName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}
















