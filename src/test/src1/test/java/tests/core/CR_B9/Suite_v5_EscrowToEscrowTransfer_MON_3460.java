package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.escrowToEscrowTransfer.EscrowToEscrowTransfer;
import framework.features.pricingEngine.PricingEngine;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.escrowToEscrowTransfer.escrowToEscrowTransferInitiation_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_EscrowToEscrowTransfer_MON_3460
 * Author Name      : Pushpalatha
 * Created Date     : 4/02/2018
 */

public class Suite_v5_EscrowToEscrowTransfer_MON_3460 extends TestInit {

    private String bank1, bank2, refNo;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        try {
            bank1 = DataFactory.getNonTrustBankName(defaultProvider.ProviderName);
            bank2 = DataFactory.getTrustBankName(defaultProvider.ProviderName);
            refNo = DataFactory.getRandomNumberAsString(5);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void verifyThatSuperAdminMakerOrNetworkAdminIsAbleToInitiateEscrowtoEscrowTransfer() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC09",
                "Verify that superadmin (maker) or network admin is able to initiate Escrow to Escrow Transfer successfully between trust & non-trust bank accounts and vice versa and success message should display");

        try {

            //Super Admin maker initiating Escrow to Escrow Transfer
            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            EscrowToEscrowTransfer.init(t1)
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            //Network Admin initiating Escrow to Escrow Transfer
            OperatorUser netAdm = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(netAdm);
            EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void EscrowToEscrowTransferInitiationScreenFieldValidation() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC1_SC2",
                "Verify that superadmin (maker) and network admin is having Escrow to Escrow Transfer Initiation link permission under Stock Management link");
        try {
            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            EscrowToEscrowTransfer.init(t1).checkInitiateFields();

            //Initiate Escrow to Escrow by not entering Requested Amount field
            EscrowToEscrowTransfer.init(t1)
                    .startNegativeTest()
                    .expectErrorB4Confirm()
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, "");

            Assertion.verifyErrorMessageContain("stockInitiation.blank.requestedQuantity", "Requested Amount Validation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void verifyThatBanksAssociatedWithSelectedMFSProviderIsDisplayedInInitiateScreen() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC3_SC5",
                "Verify that when particular MFS Provider is selected respected banks (trust & non-trust) associated with the provider is displayed in the drop down list");
        try {

            //Get the list Trust & Non Trust banks for particular MFS provider from DB
            List<String> bank = MobiquityGUIQueries.getListOfBankForSpecificProvider("101", "bank_name", "'TRUST','NTRUST'");
            Collections.sort(bank);

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            escrowToEscrowTransferInitiation_Page1.init(t1).navigateToEscrowToEscrowTransferInitiationPage();

            //Get the list Trust & Non Trust banks for particular MFS provider from EscrowToEscrowTransfer Initiation page
            List<String> fromOptions = escrowToEscrowTransferInitiation_Page1.init(t1).listOfFromBank();
            List<String> toOptions = escrowToEscrowTransferInitiation_Page1.init(t1).listOfToBank();
            Collections.sort(fromOptions);
            Collections.sort(toOptions);

            //Compare the banks fetched from DB are displayed in application in From and To bank fields
            Assert.assertEquals(bank, fromOptions);
            t1.info("Trust & non-trust banks associated with the provider is displayed in the from list");
            Assert.assertEquals(bank, toOptions);
            t1.info("Trust & non-trust banks associated with the provider is displayed in the to list");

            //Initiate EscrowToEscrowTransfer by selecting same bank in from and To bank fields
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank1, refNo, Constants.MIN_TRANSACTION_AMT);

            Assertion.verifyErrorMessageContain("escrowInitiation.same.senderAndReceiver", "Sender and reciever banks validation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void verifyThatProperBalanceOfSelectedBanksAreDisplayedInInitiateScreen() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC4",
                "Verify that proper bank's available balance is displayed on bank selection while Escrow to Escrow Transfer Initiation");
        try {

            String bank1WalletNo = DataFactory.getBankId(bank1);
            String bank2WalletNo = DataFactory.getBankId(bank2);

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            HashMap<String, BigDecimal> bMap = EscrowToEscrowTransfer.init(t1)
                    .fetchFromAndToBalance(defaultProvider.ProviderName, bank1, bank2);

            BigDecimal fbal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank1WalletNo);
            BigDecimal tbal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank2WalletNo);

            Assertion.verifyEqual(bMap.get("fromBalance"), fbal.multiply(new BigDecimal(-1)), "Verify From Balance is same as available balance of Bank1", t1);
            Assertion.verifyEqual(bMap.get("toBalance"), tbal.multiply(new BigDecimal(-1)), "Verify To Balance is same as available balance of Bank2", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void verifyThatRefNoFieldIsNumericOnly() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC6",
                "Verify that Ref. No. field is numeric only and system should display proper error message when data entered in Ref No is non-numeric");
        try {
            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            //Enter special character for Ref No
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, "***", Constants.MIN_TRANSACTION_AMT);
            Assertion.verifyErrorMessageContain("stockInitiation.special.refNo", "Reference Number Validation", t1);
            Thread.sleep(3000);

            //Leave Ref No field as blank
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, "", Constants.MIN_TRANSACTION_AMT);
            Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "Reference Number Validation", t1);
            Thread.sleep(3000);

            //Enter alphanumeric for Ref No
            EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, "test123", Constants.MIN_TRANSACTION_AMT);
            Thread.sleep(3000);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void verifyThatRequestedAmountFieldIsNumericOnly() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC7",
                "Verify that Requested Amount field is numeric only and system should display proper error message when data entered in Requested Amount is non-numeric");
        try {
            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            //Enter special character for Ref No
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, "abc");
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Requested Amount non numeric Validation", t1);

            Thread.sleep(3000);

            //Enter alphabets for Ref No
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, "%^&*");
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Requested Amount non numeric Validation", t1);

            Thread.sleep(3000);

            //Enter alphaNumeric for Ref No
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, "12test");
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Requested Amount non numeric Validation", t1);

            Thread.sleep(3000);

            //Enter Requested Amount value less than 0
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, "-3");
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Requested Amount less than 0 Validation", t1);

            Thread.sleep(3000);

            //Enter Requested Amount value equal to 0
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, "0");
            Assertion.verifyErrorMessageContain("stock.error.requestedQuantity.greaterThanZero", "Requested Amount equal to 0 Validation", t1);

            Thread.sleep(3000);

            //Decimal values validation
            EscrowToEscrowTransfer.init(t1).startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, "2.23456");
            Assertion.verifyErrorMessageContain("escrowInitiation.decimal.validation", "Requested Amount Decimal value validation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void verifyThatProperDetailsAreDisplayedInConfirmationScreenAsPerDataSelectedInInitiateScreen() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC8",
                " Verify that proper details should display in the confirmation screen as data selected in the initiation page while Escrow to Escrow Transfer Initiation");
        try {
            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            //Enter special character for Ref No
            HashMap<String, BigDecimal> bMap = EscrowToEscrowTransfer.init(t1)
                    .fetchFromAndToBalance(defaultProvider.ProviderName, bank1, bank2);

            EscrowToEscrowTransfer.init(t1)
                    .startNegativeTest()
                    .expectErrorB4Confirm()
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            EscrowToEscrowTransfer.init(t1)
                    .verifyEscroToEscrowConfirmScreen(bank1, bank2, bMap.get("fromBalance"), bMap.get("toBalance"), refNo, Constants.MIN_TRANSACTION_AMT);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void verifyThatOnClickOfBackButtonUserIsNavigatedToInitiateScreen() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC10",
                "Verify that on click of Cancel button in the confirmation page, user is navigate to initiation screen with the entered / selected values displayed");
        try {
            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            EscrowToEscrowTransfer.init(t1).confirmScreenBackbuttonValidation(defaultProvider.ProviderName, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.v5_0})
    public void verifyThatTrustBankAccountsBalanceIsAlwaysNegativeWhilePerformingEscrowToEscrowTransfer() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460",
                "Verify that bank account balance is always negative while performing escrow to escrow transfer");
        try {
            String bank2 = null;
            List<String> bankWithoutBalance = MobiquityGUIQueries.getListOfBankForSpecificProvider("101", "bank_name", "'TRUST','NTRUST'");
            int flag = 0;
            for (int i = 0; i < bankWithoutBalance.size(); i++) {
                if (bankWithoutBalance.get(i).contains("BANKWITHOUTBALANCE")) {
                    flag = 1;
                    bank2 = bankWithoutBalance.get(i);
                    break;
                }
            }
            if (flag == 0) {
                Login.init(t1)
                        .loginAsSuperAdmin("BANK_ADD");
                CurrencyProvider provider = GlobalData.currencyProvider.get(0);
                bank2 = "BANKWITHOUTBALANCE" + DataFactory.getRandomNumberAsString(2);
                Bank bank = new Bank(defaultProvider.ProviderId,
                        Constants.TEMP_BANK_TRUST,
                        null,
                        null,
                        true,
                        false);

                bank.setBankName(bank2);
                CurrencyProviderMapping.init(t1)
                        .addBank(bank);
            }

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            EscrowToEscrowTransfer.init(t1).startNegativeTest().initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            Assertion.verifyActionMessageContain("escrow.initiation.party.barred", "Receiver bank balance cannot be positive", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    // descoped as Barred user cannot login
    @Test(priority = 10, groups = {FunctionalTag.v5_0}, enabled = false)
    public void verifyThatNetworkAdminIsnotabletoinitiateEscrowToEscrowTransferWhenHeIsBarredInTheSystem() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_01",
                "Verify that network admin is not able to initiate Escrow to Escrow transfer when he is barred in the system and system should display proper error message");
        try {
            OperatorUser optBarredNWADM = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);

            Login.init(t1).login(optBarredNWADM);
            startNegativeTest();
            EscrowToEscrowTransfer.init(t1)
                    .initiateEscrowToEscrowTransfer(defaultProvider.ProviderName, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            Assertion.verifyActionMessageContain("escrow.initiation.party.barred", "Escrow Initiate not successful", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11, groups = {FunctionalTag.v5_0})
    public void MON_3378_ServiceChargeAndTransferRuleNotRequired() throws Exception {
        ExtentTest t1 = pNode.createNode("SC09",
                "Verify that transfer rule & service charge and commission not required while performing Stock Transfer between Escrow Accounts transaction");
        try {
            //Check for Transfer Rule
            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            ServiceCharge escrow = new ServiceCharge(Services.ESCROW_TO_ESCROW, opt, whs, null, null, null, null);
            TransferRuleManagement.init(t1).checkServiceInTransferRule(escrow);

            //Check for Service charge
            boolean sValue = PricingEngine.init(t1).checkServiceInPricingEnginePage("SERVICE CHARGE", "Stock Transfer between Escrow Accounts");
            if (sValue == false) {
                t1.pass("Stock Transfer between Escrow Accounts is not displayed in Service charge page");
            } else {
                t1.fail("Stock Transfer between Escrow Accounts is displayed in Service charge page");
                Assert.fail();
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
