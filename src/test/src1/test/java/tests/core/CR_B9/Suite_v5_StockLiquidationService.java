package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.pricingEng.PricingEngine_Page1;
import framework.pageObjects.pricingEng.PricingEngine_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Modify ChannelUser with LiquidationDetails
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/04/2018
 */

public class Suite_v5_StockLiquidationService extends TestInit {

    private User channeluser;
    private OperatorUser optuser, bankApprover, baruser;


    @BeforeClass(alwaysRun = true)
    public void initsetup() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            //Create channel user

            channeluser = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(eSetup).
                    createChannelUserDefaultMapping(channeluser, true);

            TransactionManagement.init(eSetup).
                    makeSureChannelUserHasBalance(channeluser, new BigDecimal(299));

            optuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            baruser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");

            ServiceCharge stockliq = new ServiceCharge
                    (Services.STOCK_LIQUIDATION_SERVICE, channeluser, optuser, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(stockliq);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0}, enabled = false)//MON-6606
    public void stockLiquidationServicePayerType() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_1",
                "Verify while defining stock liquidation service charge," +
                        "charge payer should be only sender(pricing engine-->who pays drop down)");

        try {
            //Login as Networkadmin to define Service Charge
            OperatorUser usrSChargeCreator = DataFactory.getOperatorUserWithAccess("SVC_ADD");
            Login.init(t1).login(usrSChargeCreator);
            PricingEngine_Page1 page1 = new PricingEngine_Page1(t1);
            page1.navToPricingEngine();
            PricingEngine_Page2 page2 = new PricingEngine_Page2(t1);
            //Select Service Type Stock Liquidation Service
            page1.selectServiceType("Stock Liquidation Service");
            Thread.sleep(4000);
            page2.navToAddService();
            WebElement element = driver.findElement(By.id("charge-rule-0-charge-statement-0-charge-payer"));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].click();", element);
            Select sel = new Select(driver.findElement(By.id("charge-rule-0-charge-statement-0-charge-payer")));
            Thread.sleep(4000);
            sel.selectByVisibleText("Sender");
            //Verifying Only Sender Option is available in dropdown
            List<WebElement> options = sel.getOptions();
            for (int i = 1; i < options.size(); i++) {
                String opt = options.get(i).getText();
                Assert.assertEquals(false, opt.contentEquals("Reciever"));
                Assert.assertEquals(false, opt.contentEquals("Sender Parent"));
                Assert.assertEquals(false, opt.contentEquals("Receiver Parent"));
                Assert.assertEquals(false, opt.contentEquals("Sender Owner"));
                Assert.assertEquals(false, opt.contentEquals("Receiver Owner"));
            }


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceAfterSuspendInitiateAndSuspendBank() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_22AND23",
                " Verify that appropriate error message & error code is displayed " +
                        "if sender's liquidation bank suspend initiated " +
                        "and Suspended during initiation of transaction ");

        try {
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);

            Transactions.init(t1).stockLiquidationGeneration(channeluser, batchId);

            //Getting Liquidation Id
            String liqId = MobiquityGUIQueries.getLiquidationId(channeluser.MSISDN, batchId);

            String Bankid = MobiquityGUIQueries.
                    getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));


            Login.init(t1).login(optuser);
            //Modifying the status of Liquidation Bank
            ChannelUserManagement.init(t1).initiateChannelUserModification(channeluser);
            CommonUserManagement.init(t1).
                    navModifyToBankMapping(channeluser).modifyExistingStatusOfLiquidationBank(channeluser, "SUSPEND");

            ChannelUserManagement.init(t1).
                    approveChannelUserModification(channeluser);
            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).
                    stockLiquidationServiceforFail(channeluser, optuser, liqId, Bankid);
            response.verifyMessage("006671");

            Login.init(t1).login(bankApprover);

            //Approving the status of Liquidation Bank
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(channeluser, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));

            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t1).
                    stockLiquidationServiceforFail(channeluser, optuser, liqId, Bankid);

            response1.verifyMessage("receiver.bank.account.suspended");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceafterDeleteInitiateanddeletedBank() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_24AND25", " Verify that appropriate error message & error code is displayed if sender's liquidation bank delete initiated and deleted during initiation of transaction ");

        try {
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(channeluser, batchId);
            //Getting Liquidation Id

            String liqId = MobiquityGUIQueries.getLiquidationId(channeluser.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId
                    (DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));
            Login.init(t1).login(optuser);
            //Modifying the status of Liquidation Bank
            ChannelUserManagement.init(t1).initiateChannelUserModification(channeluser);
            CommonUserManagement.init(t1).
                    navModifyToBankMapping(channeluser).
                    modifyExistingStatusOfLiquidationBank(channeluser, "DELETE");
            ChannelUserManagement.init(t1).
                    approveChannelUserModification(channeluser);
            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).
                    stockLiquidationServiceforFail(channeluser, optuser, liqId, Bankid);

            response.verifyMessage("006671");

            Login.init(t1).login(bankApprover);
            //Approving the status of Liquidation Bank
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(channeluser, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));

            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t1).stockLiquidationServiceforFail(channeluser, optuser, liqId, Bankid);

            response1.verifyMessage("006671");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceaftersenderIsBarredAsReceiver() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_14", "Verify that during initiation of transaction and transaction should be successfull if sender is barred as receiver.");

        try {
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(channeluser, batchId);
            //Getting Liquidation Id

            String liqId = MobiquityGUIQueries.getLiquidationId(channeluser.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));
            OperatorUser baruser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");
            //Bar the User as Receiver
            Login.init(t1).login(baruser);
            ChannelUserManagement.init(t1).
                    barChannelUser(channeluser, Constants.USER_TYPE_CHANNEL, Constants.BAR_AS_RECIEVER);
            //Perform Stock Liquidation
            SfmResponse response1 = Transactions.init(t1).
                    stockLiquidationService(channeluser, baruser, liqId, Bankid);
            String status = response1.Status;
            //Unbar the User
            ChannelUserManagement.init(t1).
                    unBarChannelUser(channeluser, "CH_USERS", "Receiver");
            Assert.assertEquals(status, "SUCCEEDED");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceAfterSenderStatusIsSuspendInitiateAndSuspended() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_16/17", "Verify that appropriate error message & error code is displayed if sender is suspendinitiate/suspended during initiation of transaction.");

        try {
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(channeluser, batchId);
            //Getting Liquidation Id
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String liqId = MobiquityGUIQueries.getLiquidationId(channeluser.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));
            OperatorUser baruser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");
            Login.init(t1).login(baruser);
            //Sender is suspend Initiated
            ChannelUserManagement.init(t1).suspendInitiateChannelUser(channeluser);
            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationService(channeluser, baruser, liqId, Bankid);
            Login.init(t1).login(baruser);
            //Sender is suspended
            ChannelUserManagement.init(t1).suspendChannelUserApproval(channeluser);
            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t1).
                    stockLiquidationServiceforFail(channeluser, baruser, liqId, Bankid);

            ChannelUserManagement.init(t1).resumeChannelUser(channeluser);
            response1.verifyMessage("sender.suspended");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceAfterTransactorStatusIsChanged() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_18/19", "Verify that appropriate error message & error code is displayed if transactor status is changed during initiation of transaction.");

        try {
            //Create Operator User
            OperatorUser operatorUser = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(channeluser, batchId);
            //Getting Liquidation Id
            String liqId = MobiquityGUIQueries.getLiquidationId(channeluser.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));
            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationServiceforFail(channeluser, operatorUser, liqId, Bankid);
            response.verifyMessage("transactor.barred.as.sender.and.receiver");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServicewithoutdefiningTransferRule() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_38", "Verify Stock Liquidation transaction should not be successful,if you don't define transfer rule(channel user-operator) for stock liquidation.");

        try {
            //Create Merchant
            User channeluser = new User(Constants.MERCHANT);
            ChannelUserManagement.init(t1).
                    createChannelUserDefaultMapping(channeluser, true);

            TransactionManagement.init(t1).
                    makeSureChannelUserHasBalance(channeluser);
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(channeluser, batchId);
            //Getting Liquidation Id

            String liqId = MobiquityGUIQueries.getLiquidationId(channeluser.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId
                    (DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));

            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t1).stockLiquidationServiceforFail(channeluser, optuser, liqId, Bankid);
            response1.verifyMessage("5001");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceAfterTransactorIsDeleteInitiatedAndDeleted() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4093_20/21", "20.Verify that appropriate error message & error code is displayed if transactor is delete initiated/deleted during initiation of transaction\n");

        try {
            //Create Operator User
            OperatorUser operatorUser = new OperatorUser(Constants.NETWORK_ADMIN);
            OperatorUserManagement.init(t1).createAdmin(operatorUser);
            SuperAdmin saMaker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            SuperAdmin saChecker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
            Login.init(t1).loginAsSuperAdmin(saMaker);
            //Delete Initiate Operator
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(operatorUser, false);
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(channeluser, batchId);
            //Getting Liquidation Id

            String liqId = MobiquityGUIQueries.getLiquidationId(channeluser.MSISDN, batchId);
            String Bankid = MobiquityGUIQueries.getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));
            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationServiceforFail(channeluser, operatorUser, liqId, Bankid);

            response.verifyMessage("transactor.delete.initiated");

            //Delete Operator
            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(operatorUser, true);
            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t1).stockLiquidationServiceforFail(channeluser, operatorUser, liqId, Bankid);
            response1.verifyMessage("transactor.notRegistered");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}

