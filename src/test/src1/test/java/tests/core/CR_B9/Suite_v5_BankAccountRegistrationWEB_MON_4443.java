package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.bankAccountAssociation.BankAccountRegistration_Pg1;
import framework.pageObjects.bankAccountAssociation.BankAccountRegistration_Pg2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static framework.util.common.DataFactory.getRandomNumberAsString;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_BankAccountRegistrationWEB_MON_4443
 * Author Name      : Pushpalatha
 * Created Date     : 1/07/2018
 */

public class Suite_v5_BankAccountRegistrationWEB_MON_4443 extends TestInit {

    private OperatorUser optNetAdmin, optBankApprover, optModifyuser;
    private User whs, subs, whs1;
    private String custId, accNo;
    private CurrencyProvider defaultProvider;
    private Bank trustBank, nonTrustBank;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            SystemPreferenceManagement.init(eSetup).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            SystemPreferenceManagement.init(eSetup).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            defaultProvider = DataFactory.getDefaultProvider();
            trustBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            optModifyuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            whs1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER); // only used for UI check
            optNetAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            optBankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");

            whs = new User(Constants.WHOLESALER);
            subs = new User(Constants.SUBSCRIBER);

            // map wallet preferences for channel User
            CurrencyProviderMapping.init(eSetup)
                    .mapWalletPreferencesUserRegistration(whs, DataFactory.getPayIdApplicableForChannelUser());

            CurrencyProviderMapping.init(eSetup)
                    .mapWalletPreferencesUserRegistration(subs, DataFactory.getPayIdApplicableForSubs());

            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(subs);
            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(whs);

            ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(whs, false);
            SubscriberManagement.init(eSetup).createSubscriberDefaultMapping(subs, true, false);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @BeforeMethod(alwaysRun = true)
    public void generateCustIDandAccNO() throws Exception {
        custId = DataFactory.getRandomNumberAsString(6);
        accNo = DataFactory.getRandomNumberAsString(9);
    }


    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void SC09() throws Exception {

        ExtentTest t1 = pNode.createNode("SC09",
                "Verify that if there is no other bank account associated with the user," +
                        " mark the associated account as Primary account, otherwise mark it as non-primary");
        try {
            //Setting preference AUTO_BANK_ACC_LINK_MATCH_ID to N
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1).
                    initiateBankAccountRegistration(subs, defaultProvider.ProviderName, trustBank.BankName, custId, accNo, false);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(subs, defaultProvider.ProviderName, trustBank.BankName);

            HashMap<String, String> value = BankAccountAssociation.init(t1)
                    .modifySubscriberPageBankCheck(subs, whs1, accNo);

            Assert.assertEquals(value.get("LinkedBank"), trustBank.BankName);
            t1.pass("Selected bank is displayed as Linked Bank");
            Assert.assertEquals(value.get("CustomerId"), custId);
            t1.pass("Selected customer Id is displayed for the user");
            Assert.assertEquals(value.get("AccountNumber"), accNo);
            t1.pass("Selected Account Number is displayed for the user");

            if (value.get("PrimaryAccount").equalsIgnoreCase("Yes")) {
                t1.pass("Registered Bank account is marked as Primary");
            } else {
                t1.fail("Registered Bank account is not marked as Primary");
                Assert.fail();
            }
            Thread.sleep(2000);

            String acc = getRandomNumberAsString(8);

            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(subs, defaultProvider.ProviderName, nonTrustBank.BankName, getRandomNumberAsString(6), acc, false);
            Thread.sleep(2000);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(subs, defaultProvider.ProviderName, nonTrustBank.BankName);

            HashMap<String, String> value1 = BankAccountAssociation.init(t1)
                    .modifySubscriberPageBankCheck(subs, whs1, acc);
            Utils.captureScreen(t1);
            if (value1.get("PrimaryAccount").equalsIgnoreCase("No")) {
                t1.pass("Registered Bank account is not marked as Primary");
            } else {
                t1.fail("Registered Bank account is marked as Primary");
                Assert.fail();

            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "SC09")
    public void verifyThatNonTrustBankAreOnlyDisplayedWhenBankingServicesForTrustBankPreferenceIsSetToFalse() throws Exception {

        ExtentTest t1 = pNode.createNode("SC05",
                "Verify that only Non-Trust bank(s) should populated as per provider selection in " +
                        "Bank Account Registration page when system preference BANK_ACC_LINKING_VIA_MSISDN = Y " +
                        "& BANKING_SERVICES_FOR_TRUST_BANK = FALSE ");

        try {
            //Setting preference BANK_ACC_LINKING_VIA_MSISDN to Y
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            //Setting preference BANKING_SERVICES_FOR_TRUST_BANK to False
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "FALSE");

            BankAccountRegistration_Pg1 page1 = new BankAccountRegistration_Pg1(t1);
            BankAccountRegistration_Pg2 page2 = new BankAccountRegistration_Pg2(t1);

            //Associate Bank with the user
            Login.init(t1).login(optNetAdmin);
            page1.navigateToLink();
            page1.selectProvider(defaultProvider.ProviderName);
            page1.selectUserType(subs.CategoryCode);
            page1.setMSISDNPrefN(subs.MSISDN);
            page1.clickSubmitBtnPrefN();
            page2.clickAddMoreBtn();
            List<String> actual = page2.getLinkedBankList();
            Assertion.verifyListContains(actual, nonTrustBank.BankName, "Verify Non Trust Banks are Shown in the Linked Bank List when BANKING_SERVICES_FOR_TRUST_BANK = 'false'", t1);
            Assertion.verifyListNotContains(actual, trustBank.BankName, "Verify Trust Banks is NOT Shown in the Linked Bank List when BANKING_SERVICES_FOR_TRUST_BANK = 'false'", t1);

            // reset the Preference as it might affect other tests
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");

            // set the BANK_ACC_LINKING_VIA_MSISDN to N and check if Still the non trust banks are shown and
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            Login.init(t1).login(optNetAdmin);
            page1.navigateToLink();
            page1.selectProvider(defaultProvider.ProviderName);
            page1.selectUserType(subs.CategoryCode);
            page1.setMSISDNPrefN(subs.MSISDN);
            page1.clickSubmitBtnPrefN();
            page2.clickAddMoreBtn();
            actual = page2.getLinkedBankList();
            Assertion.verifyListContains(actual, nonTrustBank.BankName, "Verify Non Trust Banks are Shown in the Linked Bank List " +
                    "when BANK_ACC_LINKING_VIA_MSISDN = 'N' and BANKING_SERVICES_FOR_TRUST_BANK = 'true", t1);
            Assertion.verifyListContains(actual, trustBank.BankName, "Verify Trust Banks is Shown in the Linked Bank List " +
                    "when BANK_ACC_LINKING_VIA_MSISDN = 'N' and BANKING_SERVICES_FOR_TRUST_BANK = 'true'", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "SC09")
    public void verifyThatTrustAndNonTrustBankAreDisplayedWhenBankingServicesForTrustBankPreferenceIsSetToTrue() throws Exception {

        ExtentTest t1 = pNode.createNode("SC06",
                "Verify that Trust & Non-Trust bank(s) should populated as per provider " +
                        "selection in Bank Account Registration page when system preference" +
                        " BANK_ACC_LINKING_VIA_MSISDN = Y & BANKING_SERVICES_FOR_TRUST_BANK = TRUE ");
        try {
            //Setting preference AUTO_BANK_ACC_LINK_MATCH_ID to Y
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");

            //Associate Bank with the user
            BankAccountRegistration_Pg1 page1 = new BankAccountRegistration_Pg1(t1);
            BankAccountRegistration_Pg2 page2 = new BankAccountRegistration_Pg2(t1);

            Login.init(t1).login(optNetAdmin);
            page1.navigateToLink();
            page1.selectProvider(defaultProvider.ProviderName);
            page1.selectUserType(subs.CategoryCode);
            page1.setMSISDNPrefN(subs.MSISDN);
            page1.clickSubmitBtnPrefN();
            page2.clickAddMoreBtn();
            List<String> actual = page2.getLinkedBankList();
            Collections.sort(actual);

            Assertion.verifyListContains(actual, nonTrustBank.BankName,
                    "Verify Non Trust Banks is Shown in the Linked Bank List " +
                            "when BANK_ACC_LINKING_VIA_MSISDN = 'Y' and BANKING_SERVICES_FOR_TRUST_BANK = 'true'", t1);

            Assertion.verifyListContains(actual, trustBank.BankName,
                    "Verify Trust Banks is Shown in the Linked Bank List " +
                            "when BANK_ACC_LINKING_VIA_MSISDN = 'Y' and BANKING_SERVICES_FOR_TRUST_BANK = 'true'", t1);

            //Set the preference BANK_ACC_LINKING_VIA_MSISDN as N
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            Login.init(t1).login(optNetAdmin);
            page1.navigateToLink();
            page1.selectProvider(defaultProvider.ProviderName);
            page1.selectUserType(subs.CategoryCode);
            page1.setMSISDNPrefN(subs.MSISDN);
            page1.clickSubmitBtnPrefN();
            page2.clickAddMoreBtn();
            actual = page2.getLinkedBankList();
            Collections.sort(actual);
            Assertion.verifyListContains(actual, nonTrustBank.BankName,
                    "Verify Non Trust Banks is Shown in the Linked Bank List " +
                            "when BANK_ACC_LINKING_VIA_MSISDN = 'N' and BANKING_SERVICES_FOR_TRUST_BANK = 'true'", t1);

            Assertion.verifyListContains(actual, trustBank.BankName,
                    "Verify Trust Banks is Shown in the Linked Bank List " +
                            "when BANK_ACC_LINKING_VIA_MSISDN = 'N' and BANKING_SERVICES_FOR_TRUST_BANK = 'true'", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
        }
        Assertion.finalizeSoftAsserts();

    }

    // script has some serious logic issue. cannot be included in suite
    @Test(priority = 4, groups = {FunctionalTag.v5_0}, enabled = false)
    public void verifyThatNetworkAdminIsAbleToAssociateBankToSubscriberWhenPreferenceIsSetToY() throws Exception {

        ExtentTest t1 = pNode.createNode("SC01", "Verify that when system preference BANK_ACC_LINKING_VIA_MSISDN = Y, " +
                "then below fields should display in Bank Account Association > Bank Account Registration page and network " +
                "admin is able to associate bank account with user when KYC preference is defined for bank and customer ID and bank account number same as the user's mobile number");

        try {

            //Setting preference AUTO_BANK_ACC_LINK_MATCH_ID to Y
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            //Check the first page feilds


            BankAccountRegistration_Pg1 page1 = new BankAccountRegistration_Pg1(t1);
          /*
            Login.init(t1).login(optNetAdmin);
            page1.navigateToLink();
            Utils.captureScreen(t1);

            Object[] expectedData = UserFieldProperties.getLabels("BankAcc.Reg");
            Object[] actualData = PageInit.fetchLabelTexts("//form[@id='bankAdd_displayBankAccountRegistration']/table/tbody/tr/td/label");

            try {
                Assert.assertEquals(actualData, expectedData);
                t1.pass("Fields are displayed as expected");
            } catch (Exception e) {
                t1.fail("Fields are not displayed as expected");
            }*/

            //Associate Bank with the user
            Login.init(t1).login(optNetAdmin);
            ArrayList<String> accDetails = BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(subs, defaultProvider.ProviderName, trustBank.BankName, subs.MSISDN, subs.MSISDN, true);

            Assert.assertEquals(accDetails.get(0), subs.MSISDN);
            t1.pass("MSISDN is set as customer Id for the user");
            Assert.assertEquals(accDetails.get(1), subs.MSISDN);
            t1.pass("MSISDN is set as Account Number for the user");

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1).approveAssociatedBanksForUser(subs, defaultProvider.ProviderName, trustBank.BankName);

            HashMap<String, String> value = BankAccountAssociation.init(t1).modifySubscriberPageBankCheck(subs, whs, subs.MSISDN);

            Assert.assertEquals(value.get("LinkedBank"), trustBank.BankName);
            t1.pass("Selected bank is displayed as Linked Bank");
            Assert.assertEquals(value.get("CustomerId"), subs.MSISDN);
            t1.pass("MSISDN is set as customer Id for the user");
            Assert.assertEquals(value.get("AccountNumber"), subs.MSISDN);
            t1.pass("MSISDN is set as Account Number for the user");

            Login.init(t1).login(optNetAdmin);
            page1.navigateToLink();
            page1.setMSISDNPrefN(subs.MSISDN);
            page1.selectUserType(subs.CategoryCode);
            page1.clickSubmitBtnPrefN();
            Utils.captureScreen(t1);

            Assertion.verifyErrorMessageContain("bank.account.registration.already.associated",
                    "Same bank should can not be mapped with the same user", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void verifyThatNetworkAdminIsAbleToAssociateBankToWholesalerWhenPreferenceIsSetToY() throws Exception {

        ExtentTest t1 = pNode.createNode("SC02", "Verify that when system preference BANK_ACC_LINKING_VIA_MSISDN = Y, " +
                "then below fields should display in Bank Account Association > Bank Account Registration page and network " +
                "admin is able to associate bank account with wholesaler when KYC preference is defined for bank and customer " +
                "ID and bank account number same as the user's mobile number");
        try {
            //Setting preference AUTO_BANK_ACC_LINK_MATCH_ID to Y
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            //Associate Bank with the user
            Login.init(t1).login(optNetAdmin);
            ArrayList<String> accDetails = BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(whs, defaultProvider.ProviderName, trustBank.BankName, whs.MSISDN, whs.MSISDN, true);

            Assert.assertEquals(accDetails.get(0), whs.MSISDN);
            t1.pass("MSISDN is set as customer Id for the user");
            Assert.assertEquals(accDetails.get(1), whs.MSISDN);
            t1.pass("MSISDN is set as Account Number for the user");

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(whs, defaultProvider.ProviderName, trustBank.BankName);

            HashMap<String, String> value = BankAccountAssociation.init(t1)
                    .viewChannelUserBankCheck(whs, whs.MSISDN);

            Assert.assertEquals(value.get("LinkedBank"), trustBank.BankName);
            t1.pass("Selected bank is displayed as Linked Bank");
            Assert.assertEquals(value.get("CustomerId"), whs.MSISDN);
            t1.pass("MSISDN is set as customer Id for the user");
            Assert.assertEquals(value.get("AccountNumber"), whs.MSISDN);
            t1.pass("MSISDN is set as Account Number for the user");

            Login.init(t1).login(optNetAdmin);
            BankAccountRegistration_Pg1 page1 = new BankAccountRegistration_Pg1(t1);
            page1.navigateToLink();
            page1.setMSISDNPrefN(whs.MSISDN);
            page1.selectUserType(whs.CategoryCode);
            page1.clickSubmitBtnPrefN();
            Utils.captureScreen(t1);

            Assertion.verifyErrorMessageContain("bank.account.registration.already.associated", "Same bank should not be added", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 6, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void verifyThatNetworkAdminIsAbleToAssociateBankWhenPreferenceIsSetToNAndChannelAdminIsAbleToReject() throws Exception {

        ExtentTest t1 = pNode.createNode("SC07", "Verify that channel admin is able to reject the associate bank account(s) with user from Channel User Management > Add/Modify/Delete Bank Accounts Approval ");
        try {
            //Setting preference AUTO_BANK_ACC_LINK_MATCH_ID to N
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
            //Associate Bank with the user
            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(subs, defaultProvider.ProviderName, trustBank.BankName, custId, accNo, false);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .rejectAssociatedBanksForUser(subs, defaultProvider.ProviderName, trustBank.BankName, custId);


            HashMap<String, String> value = BankAccountAssociation.init(t1)
                    .modifySubscriberPageBankCheck(subs, whs1, accNo);

            Utils.captureScreen(t1);
            if (value.get("AccountNumber") == null) {
                t1.pass("Bank is not added");
            } else {
                t1.fail("Bank is added");
                Assert.fail();
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


}
