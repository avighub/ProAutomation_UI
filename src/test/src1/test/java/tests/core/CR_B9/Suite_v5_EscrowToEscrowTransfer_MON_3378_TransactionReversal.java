package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.junit.Ignore;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setTxnConfig;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_EscrowToEscrowTransfer_MON_3378_TransactionReversal
 * Author Name      : Pushpalatha
 * Created Date     : 4/02/2018
 */

@Ignore("BUG-4870")
public class Suite_v5_EscrowToEscrowTransfer_MON_3378_TransactionReversal extends TestInit {

    private String TxnprefValue, DR_CRprefValue;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            setTxnConfig(of("txn.reversal.allowed.services", "ESCROWTRF"));

            TxnprefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("TXN_CORR_SERVICETYPE");
            DR_CRprefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("DR_CR_TXN_CORR");

            if (!(TxnprefValue.contains("ESCROWTRF")))
                SystemPreferenceManagement.init(eSetup).updateSystemPreference("TXN_CORR_SERVICETYPE", TxnprefValue + ",'ESCROWTRF'");
            if (!(DR_CRprefValue.contains("ESCROWTRF")))
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("DR_CR_TXN_CORR", "Y");

            SystemPreferenceManagement.init(eSetup).updateSystemPreference("DR_CR_TXN_CORR", DR_CRprefValue + ",'ESCROWTRF'");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {

        ExtentTest tTeardown = pNode.createNode("Teardown", "After Class Methods for Current Suite");
        try {
            SystemPreferenceManagement.init(tTeardown).updateSystemPreference("TXN_CORR_SERVICETYPE", TxnprefValue);
            SystemPreferenceManagement.init(tTeardown).updateSystemPreference("DR_CR_TXN_CORR", DR_CRprefValue);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void MON_3378_transactionCorrectionNotPossibleForE2E_byAPI() throws Exception {

        ExtentTest t1 = pNode.createNode("SC14_USSD",
                "Verify that Stock Transfer between Escrow Accounts transaction reversal is not possible in the system even though service type is mentioned in TXN_CORR_SERVICETYPE and DR_CR_TXN_CORR system preference and txn.reversal.allowed.services property in cloud config");
        try {
            String defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            String bank1 = DataFactory.getBankId(DataFactory.getNonTrustBankName(defaultProvider));
            String bank2 = DataFactory.getBankId(DataFactory.getTrustBankName(defaultProvider));

            SuperAdmin supradm1 = DataFactory.getSuperAdminWithAccess("PTY_ASU");

            SfmResponse response = Transactions.init(t1).initiateEscrowToEscrowTransfer(supradm1, bank1, bank2, "7491aa9ed9c62112361e2f7cf83a6e6011074cfb1c823296bb41511470322af5", "2");

            String serviceReqId = response.ServiceRequestId;

            SuperAdmin supradc = DataFactory.getSuperAdminWithAccess("PTY_ASUA");

            SfmResponse appRes = Transactions.init(t1).escrowToEscrowTransferApproval(supradc, serviceReqId);

            String txnId = appRes.TransactionId;

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            SfmResponse res = Transactions.init(t1).initiateTransactionReversalByOperator(opt, "EE180710.1757.A00046", "12", Services.ESCROW_TO_ESCROW);
            res.verifyMessage("transaction.notFound");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_3378_transactionCorrectionNotPossibleForE2E_byWEB() throws Exception {

        ExtentTest t1 = pNode.createNode("SC14_WEB",
                "Verify that Stock Transfer between Escrow Accounts transaction reversal is not possible in the system even though service type is mentioned in TXN_CORR_SERVICETYPE and DR_CR_TXN_CORR system preference and txn.reversal.allowed.services property in cloud config");
        try {
            String defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            String bank1 = DataFactory.getBankId(DataFactory.getNonTrustBankName(defaultProvider));
            String bank2 = DataFactory.getBankId(DataFactory.getTrustBankName(defaultProvider));

            SuperAdmin supradm1 = DataFactory.getSuperAdminWithAccess("PTY_ASU");

            SfmResponse response = Transactions.init(t1).initiateEscrowToEscrowTransfer(supradm1, bank1, bank2, "7491aa9ed9c62112361e2f7cf83a6e6011074cfb1c823296bb41511470322af5", "2");//7491aa9ed9c62112361e2f7cf83a6e6011074cfb1c823296bb41511470322af5

            String serviceReqId = response.ServiceRequestId;

            SuperAdmin supradc = DataFactory.getSuperAdminWithAccess("PTY_ASUA");

            SfmResponse appRes = Transactions.init(t1).escrowToEscrowTransferApproval(supradc, serviceReqId);

            String txnId = appRes.TransactionId;

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            Login.init(t1).login(opt);

            startNegativeTest();

            TransactionCorrection.init(t1).initiateTxnCorrection(txnId, true, true);

            Assertion.verifyErrorMessageContain("transaction.notFound", "Reversal not possible for escrow to escrow", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

}
