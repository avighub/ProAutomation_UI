package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : ChurnManagement test cases
 * Author Name      : Shivakumar G
 * Created Date     : 19/04/2018
 */

public class Suite_v5_ChurnManagement extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.MONEY_SMOKE})
    public void mon_4810_01() {

        ExtentTest t1 = pNode.createNode("MON-4810-01", "churn_channeluser and Subscriber").assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);
        try {

            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            User sub = new User(Constants.SUBSCRIBER);
            Transactions.init(t1).selfRegistrationForSubscriberWithKinDetails(sub);

            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            Suite_v5_ChurnRecord churnRecord = new Suite_v5_ChurnRecord(channeluser, true, false);

            churnRecordList.add(churnRecord);
            String batchid = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

            // ChannelUserManagement.init(t1).churnInitiateAndApprove(channeluser);
            CommonUserManagement.init(t1).approveRejectChurnInitiate(channeluser, batchid, true);
            churnRecordList = new ArrayList<>();

            churnRecord = new Suite_v5_ChurnRecord(sub, false, true);
            churnRecordList.add(churnRecord);

            String batchid2 = CommonUserManagement.init(t1).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t1).checkTotalFiledCount1();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t1);

            /*ChannelUserManagement.init(t1)
                    .churnInitiateAndApprove(sub);*/
            OperatorUser networkAdmin = DataFactory.getOperatorUserWithAccess("CHURN_APPROVE");
            Login.init(t1).login(networkAdmin);
            CommonUserManagement.init(t1)
                    .approveRejectChurnInitiate(sub, batchid2, true);


        } catch (Exception e) {

            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void churnsubscriber() throws Exception {

        ExtentTest t2 = pNode.createNode("MON-4810",
                "Try to verify Channel user as Subscriber and vice versa");
        try {

            List<Suite_v5_ChurnRecord> churnRecordList = new ArrayList<>();

            Suite_v5_ChurnRecord churnRecord;

            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t2).createChannelUserDefaultMapping(channeluser, false);

            churnRecord = new Suite_v5_ChurnRecord(channeluser, false, true);
            churnRecordList.add(churnRecord);

            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t2).createSubscriberDefaultMapping(sub, true, false);

            churnRecord = new Suite_v5_ChurnRecord(sub, true, false);
            churnRecordList.add(churnRecord);

            CommonUserManagement.init(t2).churnUsers(churnRecordList);
            ChurnUser_Page1.init(t2).checkTotalFiledCount2();

            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfull", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }
}

