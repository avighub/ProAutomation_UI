package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.TxnResponse;
import framework.entity.Bank;
import framework.entity.User;
import framework.features.apiManagement.OldTxnOperations;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.TransactionType;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static framework.util.JsonPathOperation.delete;
import static framework.util.JsonPathOperation.set;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_AutoDeRegistrationOfBankAccount_SUBUser
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 13/04/2018
 */
public class Suite_v5_AutoDeRegistrationOfBankAccount_SUBUser extends TestInit {
    private CurrencyProvider defaultProvider;
    private Bank defaultBank, nonTrustBank;
    private User subsUser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            defaultProvider = DataFactory.getDefaultProvider();
            defaultBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);

            subsUser = new User(Constants.SUBSCRIBER);
            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(subsUser);
            SubscriberManagement.init(eSetup).createSubscriberDefaultMapping(subsUser, true, false);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void deRegisterBankForChannelUserThroughAPI() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5799_SUB_01",
                "verify channel user/customer able to de register bank account with " +
                        "BKDREG api with new enhancements.");
        String primaryAccountNum = DataFactory.getRandomNumberAsString(8);

        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
            // add primary Bank
            Transactions.init(t1)
                    .bankRegistration(subsUser, defaultBank, Constants.CUSTOMER_REIMB, primaryAccountNum, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            // add 2nd bank which could be deRegistration
            String accNumber = DataFactory.getRandomNumberAsString(8);
            Transactions.init(t1)
                    .bankRegistration(subsUser, nonTrustBank, Constants.CUSTOMER_REIMB, accNumber, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            Transactions.init(t1)
                    .bankDeAssociation(subsUser, nonTrustBank, accNumber);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON_5799_SUB_02",
                "Verify de registration of primary bank Linked to the subscriber");
        try {
            TxnResponse res = Transactions.init(t2)
                    .bankDeAssociation(subsUser, defaultBank, primaryAccountNum);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void verifyUserRoleTagShouldBeOptionalForCustomer() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5799_6",
                "verify existed customer able to de-link bank with BKDREG,without providing UserRole as Customer. ");
        try {
            String accNumber = DataFactory.getRandomNumberAsString(9);
            Transactions.init(t1)
                    .bankRegistration(subsUser, nonTrustBank, Constants.CUSTOMER_REIMB, accNumber, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_DEREGISTRATION,
                    set("COMMAND.MSISDN", subsUser.MSISDN),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", subsUser.FirstName),
                    set("COMMAND.LNAME", subsUser.LastName),
                    set("COMMAND.IDNO", subsUser.ExternalCode),
                    delete("COMMAND.USERROLE"),
                    set("COMMAND.BANKACCNUMBER", accNumber)
            );
            t1.info(operation.bodyString);
            TxnResponse res = operation.postOldTxnURLForHSB(t1);
            Assertion.verifyEqual(res.isTxnSuccessful(Constants.TXN_SUCCESS), true,
                    "Verify Successfully Deregister Bank when for a customer and verified that user role is not mandatory", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11, groups = {FunctionalTag.v5_0})
    public void deRegisterBankForChannelUserWhenBANK_ACC_LINKING_VIA_MSISDNIsYByProvidingAccountNumber() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("MON_5799_8",
                "verify channel user able to de register bank account by providing BANK account number," +
                        "if BANK_ACC_LINKING_VIA_MSISDN preference is Y.");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
            Transactions.init(t1)
                    .bankRegistration(subsUser, nonTrustBank, Constants.CUSTOMER_REIMB, subsUser.MSISDN, subsUser.MSISDN)
                    .verifyStatus(Constants.TXN_SUCCESS);

            Transactions.init(t1)
                    .bankDeAssociation(subsUser, nonTrustBank, subsUser.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 12, groups = {FunctionalTag.v5_0})
    public void invalidIDNOWhenAUTO_BANK_ACC_LINK_MATCH_IDIsY() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5799_11",
                "verify system should throw error proper message when identification number is not associated" +
                        " with msisdn when AUTO_BANK_ACC_LINK_MATCH_ID preference is Y.");
        try {

            String accNum = DataFactory.getRandomNumberAsString(8);
            Transactions.init(t1)
                    .bankRegistration(subsUser, nonTrustBank, Constants.CUSTOMER_REIMB, accNum, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_DEREGISTRATION,
                    set("COMMAND.MSISDN", subsUser.MSISDN),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", subsUser.FirstName),
                    set("COMMAND.LNAME", subsUser.LastName),
                    set("COMMAND.IDNO", subsUser.ExternalCode),
                    set("COMMAND.BANKACCNUMBER", accNum)
            );
            t1.info(operation.bodyString);

            TxnResponse res = operation.postOldTxnURLForHSB(t1);
            Assertion.verifyMessageContain(res.Message, "identification.number.notmatch.mobilenumber",
                    "Verify that Customer ID/ Bank Account num is mandatory when associating Bank to Customer", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
