/*DESCOPED
package tests.core.systemCases_417.LiquidationBank;

import com.aventstack.extentreports.ExtentTest;
import com.jayway.restassured.path.json.JsonPath;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.billerManagement.BillerApproveModify_pg1;
import framework.pageObjects.billerManagement.BillerModification_Page1;
import framework.pageObjects.billerManagement.BillerModification_Page2;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.pageObjects.userManagement.LiquidationDetailsApprovalPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import org.apache.tools.ant.taskdefs.SQLExec;
import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static framework.util.globalConstant.Constants.BILL_SERVICE_LEVEL_ADHOC;
import static framework.util.globalConstant.Constants.BILL_SERVICE_LEVEL_PREMIUM;
*/
/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Modify ChannelUser with LiquidationDetails
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/05/2018
 *//*



public class Suite_ModifyBillerWithLiquidationDetails extends TestInit {
    String unregPref, displayAllowedReg, modifyAllowedReg, providerName;
    OperatorUser billerRegistor, bankApprover, billerRegApprovar, billerRegModifier, categoryCreator;
    Biller billerWithoutLiq, biller;
    Liquidation liq;


    @BeforeClass
    public void initsetup() throws Exception {

        ExtentTest t1 = pNode.createNode("SetBankPreference", "BankId must be Requried to Create User With Liquidation Details");

        List<String> banks = MobiquityGUIQueries.getListOfBankForLiquidation();
        int flag = 0, flag1 = 0, flag2 = 0;
        String bankname = "AXIS", bankname1 = "AXIS2", bankname2 = "AXIS1";
        for (int i = 0; i < banks.size(); i++) {
            if (banks.get(i).equalsIgnoreCase(bankname)) {
                flag = 1;
            } else if (banks.get(i).equalsIgnoreCase(bankname2)) {
                flag1 = 1;
            } else if (banks.get(i).equalsIgnoreCase(bankname1)) {
                flag2 = 1;
            } else {
                t1.info("Banks are not available");
            }

        }

        if (flag == 0) {
            Bank bank = new Bank(GlobalData.currencyProvider.get(0));
            bank.setBankName(bankname);
            CurrencyProviderMapping.init(t1).addLiquidationBank(bank);
        }

        if (flag2 == 0) {
            Bank bank1 = new Bank(GlobalData.currencyProvider.get(0));
            bank1.setBankName(bankname1);
            CurrencyProviderMapping.init(t1).addLiquidationBank(bank1);
        }

        if (flag1 == 0) {
            Bank bank2 = new Bank(GlobalData.currencyProvider.get(2));
            bank2.setBankName(bankname2);
            CurrencyProviderMapping.init(t1).addLiquidationBank(bank2);
        }

        providerName = DataFactory.getDefaultProvider().ProviderName;
        billerRegistor = DataFactory.getOperatorUserWithAccess("UTL_CREG");
        bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
        billerRegApprovar = DataFactory.getOperatorUserWithAccess("UTL_CAPP");
        billerRegModifier = DataFactory.getOperatorUserWithAccess("UTL_CMOD");
        categoryCreator = DataFactory.getOperatorUserWithAccess("UTL_MCAT");
        //Setting Preference
        MobiquityGUIQueries m = new MobiquityGUIQueries();
        unregPref = m.fetchDefaultValueOfPreference("IS_BANK_REQUIRED");
        displayAllowedReg = MobiquityGUIQueries.fetchDisplayAllowedFromGUI("IS_BANK_REQUIRED");
        modifyAllowedReg = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("IS_BANK_REQUIRED");
        if (unregPref.equalsIgnoreCase("False")) {

            if (displayAllowedReg.equalsIgnoreCase("N") || modifyAllowedReg.equalsIgnoreCase("N")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_BANK_REQUIRED", "Y");
            }

            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_BANK_REQUIRED", "TRUE");

        }

        //Biller creation
        biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

        Login.init(pNode).login(categoryCreator);
        BillerManagement.init(t1).addBillerCategory();

        Login.init(pNode).login(billerRegistor);
        liq=new Liquidation();
        BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller,liq,"Daily",null,null,null);
        Login.init(pNode).login(billerRegApprovar);
        BillerManagement.init(t1).initiateApproveBiller(biller, true);

        Login.init(t1).login(bankApprover);

        CommonUserManagement.init(pNode)
                .approveDefaultAssociatedLiquidationBanksforBiller(biller);
        biller.writeDataToExcel();
        //biller = DataFactory.getBillerFromAppdata();

    }

    @Test

    public void billerwithliquibankfieldsinModifyPage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4781_1", "Verify Add liquidation bank checkbox should be populated in Modify channel user-->>Linked Bank Page ");

        try {

            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_BANK_REQUIRED", "FALSE");

            billerWithoutLiq = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);
            BillerManagement.init(t1).createBiller(billerWithoutLiq);

            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_BANK_REQUIRED", "TRUE");


            Login.init(pNode).login(billerRegModifier);

            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);
            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);
            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
            BillerApproveModify_pg1 page3=new BillerApproveModify_pg1(pNode);

            page1.navBillerModification();
            page1.selectBillerToModify(billerWithoutLiq.BillerCode);
            page1.clickUpdateButton();

            page2.clickNextButton();

            WebElement liquidationBank = driver.findElement(By.name("action:modifyMerchant_addMoreLiquidationBankMod"));

            if (liquidationBank.isDisplayed()) {
                t1.info("Liquidation Field is Displayed");
            } else {
                Assert.fail("Liquidation field is not displayed");
            }
            liquidationBank.click();
            ArrayList fields = new ArrayList();
            fields.add("MFS Provider");
            fields.add("Liquidation Bank Name");
            fields.add("Liquidation Bank Account Number");
            fields.add("Liquidation Bank Branch Name");
            fields.add("Account Holder Name");
            fields.add("Frequency Trading Name");


            for (int i = 0, j = 1; i < fields.size(); i++, j++) {
                String elementText = "//tbody[@id='liquidationDetails']/tr[1]/td[" + j + "]/b[contains(text(),'" + fields.get(i) + "')]";
                WebElement elem = driver.findElement(By.xpath(elementText));
                boolean isElementExist = elem.isDisplayed();
                Assert.assertTrue(isElementExist);
                t1.info(fields.get(i) + " is Present");

                AddChannelUser_pg5 page = new AddChannelUser_pg5(t1);
                page.frequencyFieldsPresent("Daily");
                page.frequencyFieldsPresent("Weekly");
                page.frequencyFieldsPresent("Monthly");
            }
                new Select(driver.findElement(By.id("liquidationProvider0"))).selectByVisibleText(defaultProvider.ProviderName);
                pNode.info("Select Provider - " + defaultProvider.ProviderName);
                Thread.sleep(1000);

                new Select(driver.findElement(By.id("liquidationBankListId0"))).selectByVisibleText("AXIS");
                pNode.info("AXIS Bank is selected");
                Thread.sleep(2000);
                Liquidation liq1 = new Liquidation();
                page5.adddvalues(liq1.LiquidationBankAccountNumber, liq1.LiquidationBankBranchName, liq1.AccountHolderName, liq1.FrequencyTradingName);

                driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='Daily']")).click();
                pNode.info("Daily is selected");
                Thread.sleep(3000);

                page3.clickButtonSubmitForNewUser();
                Thread.sleep(3000);
                page3.clickConfirmButton();

                BillerManagement.init(t1).modifyBillerApproval(billerWithoutLiq, true);
                Login.init(t1).login(bankApprover);
                CommonUserManagement.init(pNode)
                        .approveDefaultAssociatedLiquidationBanksforBiller(billerWithoutLiq);




        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test

    public void liquidationFieldsDisplayedinModifyPage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4781_3", "Verify After approving biller with liquidation bank,Liquidation bank details should be populated in modify biller user page.");

        try {
            // Modifying Channel User
            Login.init(t1).login(billerRegModifier);
            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);

            page1.navBillerModification();
            Thread.sleep(3000);
            page1.selectBillerToModify(biller.BillerCode);
            page1.clickUpdateButton();

            page2.clickNextButton();

            //Liquidation Fields Details Displaying in ModifyPage
            LiquidationDetailsApprovalPage page = new LiquidationDetailsApprovalPage(t1);
            page.verifyHeadersDetailsinModifyPage("MFS1", "AXIS", liq.LiquidationBankAccountNumber, liq.LiquidationBankBranchName, liq.AccountHolderName, liq.FrequencyTradingName);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 1)
    public void operatorUserabletoModifyOnlyLiqFreqDetails() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4781_4", " Verify After approving add channel user with liquidation bank,Operator users not able to modify Liquidation bank details other than frequency type in modify channel user page.");

        try {

            // Modifying Channel User
            Login.init(t1).login(billerRegModifier);
            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);

            page1.navBillerModification();
            page1.selectBillerToModify(biller.BillerCode);
            page1.clickUpdateButton();
            page2.clickNextButton();
            CommonUserManagement.init(t1).modifyExistingFrequencyForLiquidationBankForBiller("Weekly", "Monday", null, null);

            BillerManagement.init(t1).modifyBillerApproval(biller, true);

        } catch (Exception e) {
            e.printStackTrace();
        }}

        @Test(priority = 2)
        public void operatorUsernotabletoModifyLiqdetails () throws Exception {


            ExtentTest t1 = pNode.createNode("MON_4781_5", " Verify After approving biller with liquidation bank,Operator users not able to modify Liquidation bank details other than frequency type in modify Biller page. ");

            try {

                // Modifying Channel User
                Login.init(t1).login(billerRegModifier);
                BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
                BillerModification_Page2 page2 = new BillerModification_Page2(pNode);



                //Verifying Operator User Not Able To Modify Liq Freq Details
                page1.navBillerModification();
                page1.selectBillerToModify(biller.BillerCode);
                page1.clickUpdateButton();
                page2.clickNextButton();
                LiquidationDetailsApprovalPage page = new LiquidationDetailsApprovalPage(t1);
                page.verifyHeadersDetailsinModifyPageareDisabled();


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    @Test(priority = 3)//getting failed
    public void liquidationDetailsGettingDisplayedOnModifyApprovalpage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4781_6", "Verify After modify biller with liquidation bank,Liquidation bank details should be populated in modify biller approval page.");
        try {
            biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            Login.init(pNode).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(pNode).login(billerRegistor);
            Liquidation liq1=new Liquidation();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller,liq1,"Daily",null,null,null);
            Login.init(pNode).login(billerRegApprovar);
            BillerManagement.init(t1).initiateApproveBiller(biller, true);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller);

            Login.init(t1).login(billerRegModifier);
            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);

            page1.navBillerModification();
            page1.selectBillerToModify(biller.BillerCode);
            page1.clickUpdateButton();
            page2.clickNextButton();
            CommonUserManagement.init(t1).modifyExistingFrequencyForLiquidationBankForBiller("Weekly", "Monday", null, null);

            BillerApproveModify_pg1 page = new BillerApproveModify_pg1(pNode);


            page.navBillerApproveInitiate();
            page.clickOnApproveOrReject(biller.BillerCode, true);
            //Verifying Details Present Or Not
            LiquidationDetailsApprovalPage page3 = new LiquidationDetailsApprovalPage(t1);
            page3.verifyHeadersDetails("MFS1", "AXIS", liq1.LiquidationBankAccountNumber, liq1.LiquidationBankBranchName, liq1.AccountHolderName, liq1.FrequencyTradingName);
            page.clickApproveBiller();




        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 7)
    public void operatorUserAbletoSuspendLiquidationBank() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4781_6", "Verify Admin user can able to suspend Liquidation bank account details of biller.");
        try {
             Biller biller1 = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            Login.init(pNode).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(pNode).login(billerRegistor);
            Liquidation liq1=new Liquidation();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller1,liq1,"Daily",null,null,null);
            Login.init(pNode).login(billerRegApprovar);
            BillerManagement.init(t1).initiateApproveBiller(biller1, true);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1);

            Login.init(t1).login(billerRegModifier);
            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);

            page1.navBillerModification();
            page1.selectBillerToModify(biller1.BillerCode);
            page1.clickUpdateButton();
            page2.clickNextButton();
            CommonUserManagement.init(t1).modifyExistingStatusOfLiquidationBankForBiller("SUSPEND");

            BillerApproveModify_pg1 page = new BillerApproveModify_pg1(pNode);
            page.navBillerApproveInitiate();
            page.clickOnApproveOrReject(biller1.BillerCode, true);
            //Verifying Details Present Or Not
            page.clickApproveBiller();

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 8)
    public void operatorUserAbletoDeleteLiquidationBank() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4781_6", "Verify Admin user can able to delete Liquidation bank account details of biller.");
        try {
           Biller biller1 = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            Login.init(pNode).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(pNode).login(billerRegistor);
            Liquidation liq1=new Liquidation();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller1,liq1,"Daily",null,null,null);
            Login.init(pNode).login(billerRegApprovar);
            BillerManagement.init(t1).initiateApproveBiller(biller1, true);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1);

            Login.init(t1).login(billerRegModifier);
            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);

            page1.navBillerModification();
            page1.selectBillerToModify(biller1.BillerCode);
            page1.clickUpdateButton();
            page2.clickNextButton();
            CommonUserManagement.init(t1).modifyExistingStatusOfLiquidationBankForBiller("DELETE");

            BillerApproveModify_pg1 page = new BillerApproveModify_pg1(pNode);
            page.navBillerApproveInitiate();
            page.clickOnApproveOrReject(biller1.BillerCode, true);
            //Verifying Details Present Or Not
            page.clickApproveBiller();

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 9)
    public void operatorUserAbletoRejectSuspendLiquidationBank() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4781_6", "Verify Admin user can able to reject suspend Liquidation bank account details of biller.");
        try {
            Biller biller1 = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            Login.init(pNode).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(pNode).login(billerRegistor);
            Liquidation liq1=new Liquidation();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller1,liq1,"Daily",null,null,null);
            Login.init(pNode).login(billerRegApprovar);
            BillerManagement.init(t1).initiateApproveBiller(biller1, true);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1);

            Login.init(t1).login(billerRegModifier);
            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);

            page1.navBillerModification();
            page1.selectBillerToModify(biller1.BillerCode);
            page1.clickUpdateButton();
            page2.clickNextButton();
            CommonUserManagement.init(t1).modifyExistingStatusOfLiquidationBankForBiller("SUSPEND");

            BillerApproveModify_pg1 page = new BillerApproveModify_pg1(pNode);
            page.navBillerApproveInitiate();
            page.clickOnApproveOrReject(biller1.BillerCode, true);
            //Verifying Details Present Or Not
            page.clickApproveBiller();

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1,true);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 10)
    public void operatorUserAbletoRejectDeleteLiquidationBank() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4781_6", "Verify Admin user can able to reject delete Liquidation bank account details of biller.");
        try {
            //Create Biller
            Biller biller1 = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, BILL_SERVICE_LEVEL_ADHOC);
            biller1.setBillerType(Constants.BILLER_BILL_PAYEE);

            Login.init(pNode).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(pNode).login(billerRegistor);
            Liquidation liq1=new Liquidation();
            BillerManagement.init(t1).initiateBillerRegistrationWithLiquidationBankDetails(biller1,liq1,"Daily",null,null,null);
            Login.init(pNode).login(billerRegApprovar);
            BillerManagement.init(t1).initiateApproveBiller(biller1, true);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1);
            //Modify Biller
            Login.init(t1).login(billerRegModifier);
            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);

            page1.navBillerModification();
            page1.selectBillerToModify(biller1.BillerCode);
            page1.clickUpdateButton();
            page2.clickNextButton();
            CommonUserManagement.init(t1).modifyExistingStatusOfLiquidationBankForBiller("DELETE");

            BillerApproveModify_pg1 page = new BillerApproveModify_pg1(pNode);
            page.navBillerApproveInitiate();
            page.clickOnApproveOrReject(biller1.BillerCode, true);
            //Verifying Details Present Or Not
            page.clickApproveBiller();

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1,true);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 11)
    public void operatorUserAbletoAddOnlyOneLiquidationBankPerMFSProvider() throws Exception {

        try {

            //Modify Biller
            ExtentTest t1 = pNode.createNode("MON_4781_11", ".Verify each user can set up only 1 liquidation bank per MFS Provider including liquidation banks which biller is already associated.");

            Login.init(t1).login(billerRegModifier);
            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);


            page1.navBillerModification();
            page1.selectBillerToModify(biller.BillerCode);
            page1.clickUpdateButton();
            page2.clickNextButton();
            Thread.sleep(3000);
            driver.findElement(By.name("action:modifyMerchant_addMoreLiquidationBankMod")).click();
            //Adding Second Bank
            WebElement liqaccountNum = driver.findElement(By.name("liquidationBankAccountNo1[1]"));
            WebElement liqbranchnam = driver.findElement(By.name("liquidationBranchName1[1]"));
            WebElement liqaccountholdernam = driver.findElement(By.name("liquidationAccHolderName1[1]"));
            WebElement freqtradingnam = driver.findElement(By.name("liquidationTradingName1[1]"));
            new Select(driver.findElement(By.id("liquidationProvider1"))).selectByVisibleText("MFS1");
            Thread.sleep(1000);
            new Select(driver.findElement(By.id("liquidationBankListId1"))).selectByVisibleText("AXIS2");

            Liquidation liq1 = new Liquidation();
            liqaccountNum.sendKeys(liq1.LiquidationBankAccountNumber);
            liqbranchnam.sendKeys(liq1.LiquidationBankBranchName);
            liqaccountholdernam.sendKeys(liq1.AccountHolderName);
            freqtradingnam.sendKeys(liq1.FrequencyTradingName);
            driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='Daily']")).click();
            Thread.sleep(3000);
            page2.clickSubmitButtonAfterChanges();
            Assertion.verifyErrorMessageContain("LiquidationBank.one.permfsprovider", "Only one Bank per mfs provider is allowed", t1);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 10)
    public void generateAPIForSuspendInitiateAndSuspendedBiller() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4781_12", "Verify if biller is suspend initiate liquidation id is generated and if suspended liquidation id is not generated");
        try {

            //Create Biller
            Biller biller1 = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, BILL_SERVICE_LEVEL_ADHOC);
            biller1.setBillerType(Constants.BILLER_BILL_PAYEE);

            BillerManagement.init(t1).createBillerWithLiquidationBankId(biller1,"Daily",null,null,null);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveDefaultAssociatedLiquidationBanksforBiller(biller1);
            biller1.writeDataToExcel();
            //Biller biller1 = DataFactory.getBillerFromAppdata();

            //Initiate Payment to Biller
            User whs=DataFactory.getChannelUserWithCategory("WHS");
            //TransactionManagement.init(t1).initiateAndApproveO2C(whs,"150","testing");
            String accNo = DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).initiateOfflineBillPaymentForAdhoc(biller1,whs,"100",accNo);
            BillerManagement.init(t1).suspendInitiateBiller(biller1);
            Transactions.init(t1).stockLiquidationGenerationForBiller(biller1);
            BillerManagement.init(t1).suspendApproveBiller(biller1);
            SfmResponse response=Transactions.init(t1).stockLiquidationGenerationForBillerForFail(biller1);
            String errorresponse = response.Message;
            Assert.assertEquals(errorresponse,"Initiator has been suspended" );

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    }



*/
