package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.features.pricingEngine.PricingEngine;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Commisssion Settlement Defect MON-5455
 * Author Name      : Jyoti Katiyar
 * Created Date     : 04/05/2018
 */

public class CommissionSettlementDefect extends TestInit {
    private SoftAssert softAssert;

    /*Commission Settlement service is displayed in pricing engine UI for commission policy,
    service charge policy,pricing calculator*/
    /*
     *  Defect MON-5455
     *  So it will fail,once it is fixed the
     *  T.S will pass
     *
     * */
    @Test(enabled = false)
    public void CommissionSettlementServiceDefect_5455() throws Exception {
        softAssert = new SoftAssert();
        ExtentTest t1 = pNode.createNode("MON_5455",
                "Verify Commission Settlement Service is not displaying in " +
                        "Pricing Engine UI for service charge policy,commission policy and pricing calculator ");

        //Login as a network admin > navigate to Pricing engine page > Select "Servie Charge" > Search for commission Settlement service
        boolean sValue = PricingEngine.init(t1).checkServiceInPricingEnginePage("SERVICE CHARGE", "COMMISSION SETTLEMENT");
        if (sValue) {
            t1.fail("COMMISSION SETTLEMENT is displayed in Pricing Engine UI for SERVICE CHARGE");
            softAssert.fail("COMMISSION SETTLEMENT is displayed in Pricing Engine UI for SERVICE CHARGE");
        } else {
            t1.pass("COMMISSION SETTLEMENT is NOT displayed in Pricing Engine UI for SERVICE CHARGE");
        }

        //Login as a network admin > navigate to Pricing engine page > Select "COMMISSION" > Search for commission Settlement service
        boolean cValue = PricingEngine.init(t1).checkServiceInPricingEnginePage("COMMISSION", "COMMISSION SETTLEMENT");
        if (cValue) {
            t1.fail("COMMISSION SETTLEMENT is displayed in Pricing Engine UI for COMMISSION");
            softAssert.fail("COMMISSION SETTLEMENT is displayed in Pricing Engine UI for COMMISSION");

        } else {
            t1.pass("COMMISSION SETTLEMENT is NOT displayed in Pricing Engine UI for  COMMISSION");
        }

        //Login as a network admin > navigate to Pricing engine page > Select "Pricing Calculator" > Search for commission Settlement service
        boolean pValue = PricingEngine.init(t1).checkServiceInPricingEnginePage("Pricing Calculator", "COMMISSION SETTLEMENT");
        if (pValue) {
            t1.fail("COMMISSION SETTLEMENT is displayed in Pricing Engine UI for Pricing Calculator");
            softAssert.fail("COMMISSION SETTLEMENT is displayed in Pricing Engine UI for Pricing Calculator");
        } else {
            t1.pass("COMMISSION SETTLEMENT is NOT displayed in Pricing Engine UI for Pricing Calculator");
        }
        softAssert.assertAll();
    }
}
