package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TCPManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.tcp.ApprovalLimitReset;
import framework.pageObjects.tcp.InitiateLimitReset;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Modify ChannelUser with LiquidationDetails
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 13/07/2018
 */

public class Suite_LimitResetCR extends TestInit {

    private SuperAdmin sAdmin;
    private OperatorUser optInitUser, optApproveUser;
    private User channelUser, subscriber;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        sAdmin = DataFactory.getSuperAdminWithAccess("GRP_ROL");
        optInitUser = DataFactory.getOperatorUserWithAccess("TCP_INITLIMITRESET");
        optApproveUser = DataFactory.getOperatorUserWithAccess("TCP_APPRLIMITRESET");
        channelUser = DataFactory.getChannelUserWithCategory("WHS");
        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void webroleInitiatelimitextensionIsAvailableForNetworkAdmin() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_2",
                "Verify To initiate the user’s threshold limit extension a new web role 'Initiate limit extension' should be created and assigned to admin user.");

        try {
            //Login As Network Admin

            Login.init(t1).login(optInitUser);
            InitiateLimitReset page = new InitiateLimitReset(t1);
            page.navInitiateLimitReset();
            t1.info("Initiate limit extension Is Available For NetworkAdmin");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void extensionOfThresholdLimitByNetworkAdminForChannelUser() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_1", "Verify Extension of threshold limit should be a maker-checker process executed by Admin users.");

        try {
            //Login As Network Admin

            TCPManagement.init(t1).initiateLimitReset(channelUser, "Channel User", Services.O2C, "2x", "Both", "Weekly", "ALL");
            TCPManagement.init(t1).approveLimitReset(channelUser, "Channel User");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void limitExtensionShouldBeAllowedForBothCustomerAndChannelUsers() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_3", "Verify Limit extension should be allowed for customers and channel users.");

        try {
            //Login As Network Admin
            Login.init(t1).login(optInitUser);

            ArrayList fields = new ArrayList();
            fields.add("Subscriber");
            fields.add("Channel User");
            InitiateLimitReset page = new InitiateLimitReset(t1);
            page.navInitiateLimitReset();
            Select sel = new Select(driver.findElement(By.id("enumId")));
            List<WebElement> options = sel.getOptions();
            for (int i = 1, j = 0; i < options.size(); i++, j++) {
                String opt = options.get(i).getText();
                Assert.assertEquals(opt, fields.get(j));
                t1.info(fields.get(j) + " userType is Present");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void allServicesAndForAllBearerShouldBeAllowedForLimitExtensions() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_4/6", "Verify System should allow to extern the limit of any service type or all services/all bearer");

        try {
            //Login As Network Admin

            Login.init(t1).login(optInitUser);

            InitiateLimitReset page = new InitiateLimitReset(t1);
            page.navInitiateLimitReset();
            page.selectUserType("Channel User");
            page.enterMobilerNumber(channelUser.MSISDN);
            page.selectAllService();
            page.selectExtension("2x");
            page.selectLimit("Both");
            page.selectFrequency("Daily");
            page.selectBearer("ALL");

            page.selectExpiryDate(4);
            page.clickOnSubmit();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("tcp.limit.reset.initiate", "Limit Reset Initiated", pNode);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void limitExtensionShouldBeAvailableForBoth_Count_Amount() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_5", "Verify Limit extension should be allowed on 'Amount' or 'Count' or both.");

        try {
            //Login As Network Admin

            Login.init(t1).login(optInitUser);

            ArrayList fields = new ArrayList();
            fields.add("Both");
            fields.add("Count");
            fields.add("Amount");
            InitiateLimitReset page = new InitiateLimitReset(t1);
            page.navInitiateLimitReset();

            Select sel = new Select(driver.findElement(By.id("tcpInitiateLimitReset_submitList()_limitType")));
            List<WebElement> options = sel.getOptions();
            for (int i = 1, j = 0; i < options.size(); i++, j++) {
                String opt = options.get(i).getText();
                Assert.assertEquals(opt, fields.get(j));
                t1.info(fields.get(j) + " option is Present");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void limitExtensionShouldBeAvailableForDaily_Weekly_Monthly() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_7", ".Verify Limit extension should be allowed for any threshold duration like daily, weekly or monthly");

        try {
            //Login As Network Admin

            Login.init(t1).login(optInitUser);

            ArrayList fields = new ArrayList();
            fields.add("Daily");
            fields.add("Weekly");
            fields.add("Monthly");
            InitiateLimitReset page = new InitiateLimitReset(t1);
            page.navInitiateLimitReset();

            Select sel = new Select(driver.findElement(By.id("liquidationFrequency")));
            List<WebElement> options = sel.getOptions();
            for (int i = 1, j = 0; i < options.size(); i++, j++) {
                String opt = options.get(i).getText();
                Assert.assertEquals(opt, fields.get(j));
                t1.info(fields.get(j) + " frequency is Present");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void requiredInputParametersShouldBeAvailableForLimitExtension() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4900_8", ".Verify Below should be required input parameters for limit extension:\n" +
                "      a.UserType(channel user and subscriber)\n" +
                "      b.User’s mobile number\n" +
                "c.Service type (drop down list with ‘All’ option)\n" +
                "      d.Limit type (Drop down list with ‘Count’, ‘Amount’ & Both option)\n" +
                "      e.Bearer type (USSD/Mobile app & ‘All’ option)\n" +
                "      f.Limit duration (Daily, Weekly, Monthly)\n" +
                "      g.Extension (2x, 3x....5x)( extension will be applicable on existing assigned limit not on consumed limit) ");

        try {
            //Login As Network Admin

            Login.init(t1).login(optInitUser);
            InitiateLimitReset page = new InitiateLimitReset(t1);
            page.navInitiateLimitReset();

            Object[] expected = PageInit.fetchLabelTexts("//table[@class = 'wwFormTableC']/tbody/tr/td[1]/label");
            Object[] actual = UserFieldProperties.getLabels("initiate.limit.reset");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("label Validated Successfully: " + expected[i]);
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void requiredInputParametersShouldBeAvailableForApprovalLimitExtension() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4900_11", "Verify based on below input parameters,Admin user should provide the input details and submit the request\n" +
                "    a.From date\n" +
                "    b.To date\n" +
                "c.UserType\n" +
                "    d.Mobile number ");

        try {
            //Login As Network Admin

            Login.init(t1).login(optInitUser);
            ApprovalLimitReset page = new ApprovalLimitReset(t1);
            page.navApprovalLimitReset();

            Object[] expected = PageInit.fetchLabelTexts("//table[@class = 'wwFormTableC']/tbody/tr/td[1]/label");
            Object[] actual = UserFieldProperties.getLabels("approval.limit.reset");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("label Validated Successfully: " + expected[i]);
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

    }

    @Test(priority = 9, groups = {FunctionalTag.v5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void listOfInitiatedLimitExtensionTransactionIsAvailableBasedOnGivenInputParameters() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_12", "Verify list of initiated limit extension records should be displayed based on given input parameters with radio button option ");

        try {
            //Login As Network Admin

            TCPManagement.init(t1).initiateLimitReset(channelUser, "Channel User", Services.O2C, "2x", "Both", "Weekly", "ALL");
            Login.init(t1).login(optApproveUser);
            ApprovalLimitReset page = new ApprovalLimitReset(t1);
            page.navApprovalLimitReset();
            page.selectFromDate(-1);
            page.selectToDate(-1);
            page.selectUserType("Channel User");
            page.enterMobilerNumber(channelUser.MSISDN);
            page.clickOnSubmit();
            WebElement element = driver.findElement(By.xpath(".//*[@id='tcpApprovalLimitReset_submitConfirm()']/table/tbody/tr[2]/td"));
            String actual = element.getText();
            Assert.assertEquals(actual, "No transactions available currently for approval at this level");
            t1.pass("Transactions Is not availble for given input");
            TCPManagement.init(t1).approveLimitReset(channelUser, "Channel User");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 10, groups = {FunctionalTag.v5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void systemShouldDisplayExsistingAssignedThresholdLimitsConsumedLimitsRequestedExtensionLimit() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_12", "Verify list of initiated limit extension records should be displayed based on given input parameters with radio button option ");

        try {
            //Login As Network Admin

            TCPManagement.init(t1).initiateLimitReset(channelUser, "Channel User", Services.O2C, "2x", "Both", "Weekly", "ALL");
            Login.init(t1).login(optApproveUser);
            ApprovalLimitReset page = new ApprovalLimitReset(t1);
            page.navApprovalLimitReset();
            page.selectFromDate(0);
            page.selectToDate(0);
            page.selectUserType("Channel User");
            page.enterMobilerNumber(channelUser.MSISDN);
            page.clickOnSubmit();
            page.clickOnCheckBox(channelUser.LoginId);
            page.clickOnConfirm();
            Object[] expected = PageInit.fetchLabelTexts(".//*[@id='tcpApprovalLimitReset_approve()']/table/tbody/tr[4]|(.//*[@id='tcpApprovalLimitReset_approve()']/table/tbody/tr)[position() mod 2=1 and position()>5 and position()<last()]");
            Object[] actual = UserFieldProperties.getLabels("approval.limit.fields");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("label Validated Successfully: " + expected[i]);
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }


            page.clickOnApprove();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 11, groups = {FunctionalTag.v5_0})
    public void errorMessageShouldGetDisplayedIfSelectedServiceIsNotAvailableInThresholdCount() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900", "Verify error message should be displayed if service is not available in threshold count");
        try {
            //Login As Network Admin
            Login.init(t1).login(optInitUser);
            InitiateLimitReset pageOne = InitiateLimitReset.init(pNode);


            pageOne.navInitiateLimitReset();
            pageOne.selectUserType("Channel User");
            pageOne.enterMobilerNumber(channelUser.MSISDN);
            pageOne.serviceType.click();
            Thread.sleep(2000);
            Object[] expected = PageInit.fetchLabelTexts("//span[@class = 'dojoComboBoxOptions']/div", "resultvalue");
            List<Object> expectedList = Arrays.asList(expected);
            ArrayList<Object> ex = new ArrayList<>(expectedList);
            ArrayList<String> expectedListString = (ArrayList<String>) (ArrayList<?>) ex;
            List<String> serviceType = MobiquityGUIQueries.getServiceTypeFromThresholdCount(channelUser.MSISDN);


            for (int j = 0; j < serviceType.size(); j++) {

                for (int i = 1; i < expectedListString.size(); i++) {

                    if ((expectedListString.get(i).equalsIgnoreCase(serviceType.get(j)))) {
                        expectedListString.remove(i);
                    }
                }
            }

            pageOne.selectServiceName(expectedListString.get(1));
            pageOne.selectExtension("2x");
            pageOne.selectLimit("Both");
            pageOne.selectFrequency("Daily");
            pageOne.selectBearer("WEB");

            pageOne.selectExpiryDate(4);
            pageOne.clickOnSubmit();
            Assertion.verifyErrorMessageContain("transfer.controldetails.notavailable", "Service has not been done by user", t1);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 12, groups = {FunctionalTag.v5_0})
    public void errorMessageShouldGetDisplayedIfSelectedBearerIsNotAvailableInThresholdCount() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900", "Verify error message should be displayed if bearer is not available in threshold count");
        try {
            //Login As Network Admin
            Login.init(t1).login(optInitUser);
            InitiateLimitReset pageOne = InitiateLimitReset.init(pNode);


            pageOne.navInitiateLimitReset();
            pageOne.selectUserType("Channel User");
            pageOne.enterMobilerNumber(channelUser.MSISDN);
            pageOne.selectServiceName(Services.O2C);
            pageOne.selectExtension("2x");
            pageOne.selectLimit("Both");
            pageOne.selectFrequency("Daily");
            Select sel = new Select(driver.findElement(By.id("bearerId")));
            List<WebElement> options = sel.getOptions();
            List<String> serviceType = MobiquityGUIQueries.getBearerTypeFromThresholdCount(channelUser.MSISDN);


            for (int j = 0; j < serviceType.size(); j++) {

                for (int i = 1; i < options.size(); i++) {

                    if ((options.get(i).getText().equalsIgnoreCase(serviceType.get(j)))) {
                        options.remove(i);
                    }
                }
            }


            pageOne.selectBearer(options.get(1).getText());

            pageOne.selectExpiryDate(4);
            pageOne.clickOnSubmit();
            Assertion.verifyErrorMessageContain("transfer.controldetails.notavailable", "Service has not been done by user", t1);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 13, groups = {FunctionalTag.v5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void extensionOfThresholdLimitUpdatedInSystem() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_15", "Verify User approves the extension, requested limits of user will be updated in the system ");

        try {
            //Login As Network Admin

            TCPManagement.init(t1).initiateLimitReset(channelUser, "Channel User", Services.O2C, "3x", "Both", "Weekly", "ALL");
            TCPManagement.init(t1).approveLimitReset(channelUser, "Channel User");
            Map<String, String> dbTRule = MobiquityGUIQueries.dbGetExtensionValue(channelUser.MSISDN, Services.O2C, "2", "ALL");
            String ext_Status = dbTRule.get("Status");
            String ext_Count = dbTRule.get("COUNT");
            String ext_Amount = dbTRule.get("AMOUNT");

            Assert.assertEquals(ext_Status, "TS");
            Assert.assertEquals(ext_Count, "3");
            Assert.assertEquals(ext_Amount, "3");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 14, groups = {FunctionalTag.v5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void rejectionOfThresholdLimitByNetworkAdmin() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_16", "Verify On rejection, limit extension should be rejected ");

        try {
            //Login As Network Admin

            TCPManagement.init(t1).initiateLimitReset(channelUser, "Channel User", Services.O2C, "2x", "Both", "Weekly", "ALL");
            TCPManagement.init(t1).approveLimitReset(channelUser, "Channel User", false);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 15, groups = {FunctionalTag.v5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void errorMessageShouldDisplayWhenInvalidMobileNumberIsGiven() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_19", "Verify System should throw proper error message when we enter unknown Msisdn. ");

        try {
            //Login As Network Admin
            startNegativeTest();

            TCPManagement.init(t1).initiateLimitReset(channelUser, "Subscriber", Services.O2C, "2x", "Both", "Weekly", "ALL");
            Assertion.verifyErrorMessageContain("tcp.invalid.mobilenumber", "Invalid User Mobile Number", t1);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test(priority = 16, groups = {FunctionalTag.v5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void extensionOfThresholdLimitByNetworkAdminForSubscriber() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4900_1", "Verify Extension of threshold limit should be a maker-checker process executed by Admin users.");

        try {
            //Login As Network Admin

            TCPManagement.init(t1).initiateLimitReset(subscriber, "Subscriber", Services.CASHIN, "2x", "Both", "Weekly", "ALL");
            TCPManagement.init(t1).approveLimitReset(subscriber, "Subscriber");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
