package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.TxnResponse;
import framework.entity.Bank;
import framework.entity.User;
import framework.features.apiManagement.OldTxnOperations;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.TransactionType;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

import static framework.util.JsonPathOperation.delete;
import static framework.util.JsonPathOperation.set;
import static framework.util.common.DataFactory.getDefaultBankId;
import static framework.util.globalConstant.Constants.SAVINGS_ACCOUNT_TYPENO;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_AutoRegistrationOfBankingService
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 13/04/2018
 */

public class Suite_v5_AutoRegistrationOfBankingService extends TestInit {

    private User whsUser, subsUser;
    private CurrencyProvider defaultProvider;
    private Bank trustBank, nonTrustBank;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            whsUser = new User(Constants.WHOLESALER);
            subsUser = new User(Constants.SUBSCRIBER);
            defaultProvider = DataFactory.getDefaultProvider();
            trustBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);

            // Mapp Bank account link Preference
            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(subsUser);
            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(whsUser);

            // create users
            SubscriberManagement.init(eSetup).createSubscriberDefaultMapping(subsUser, true, false);
            ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(whsUser, false);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void createBankBankForChannelUserThroughAPI() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_1",
                "verify channel user/customer able to link bank with BANKREG api with new enhancements(user role,identification number tags).");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "Y");
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
            Transactions.init(t1)
                    .bankRegistration(whsUser, trustBank, Constants.CHANNEL_REIMB, whsUser.MSISDN, whsUser.MSISDN)
                    .verifyStatus(Constants.TXN_SUCCESS);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void errorMessageWhilecreatingBankForChannelUserWhenAUTO_BANK_ACC_LINK_MATCH_IDIsYWithoutIDNO() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_3", "verify Id number is mandatory when AUTO_BANK_ACC_LINK_MATCH_ID is Y ");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "Y");

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                    set("COMMAND.MSISDN", whsUser.MSISDN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", whsUser.FirstName),
                    set("COMMAND.LNAME", whsUser.LastName),
                    set("COMMAND.BANKACCNUMBER", DataFactory.getRandomNumberAsString(8)),
                    set("COMMAND.CUSTOMERID", DataFactory.getRandomNumberAsString(8)),
                    set("COMMAND.USERROLE", Constants.CHANNEL_REIMB),
                    delete("COMMAND.IDNO") // delete IDNO

            );
            t1.info(operation.bodyString);
            operation.postOldTxnURLForHSB(t1)
                    .verifyMessage("subs.externalCode.required");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void createBankForChannelUserWhenAUTO_BANK_ACC_LINK_MATCH_IDIsN() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_2",
                "verify Id number is optional when AUTO_BANK_ACC_LINK_MATCH_ID is N ");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "N");

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                    set("COMMAND.MSISDN", whsUser.MSISDN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", whsUser.FirstName),
                    set("COMMAND.LNAME", whsUser.LastName),
                    set("COMMAND.BANKACCNUMBER", DataFactory.getRandomNumberAsString(8)),
                    set("COMMAND.CUSTOMERID", DataFactory.getRandomNumberAsString(8)),
                    set("COMMAND.USERROLE", Constants.CHANNEL_REIMB),
                    delete("COMMAND.IDNO") // delete IDNO
            );
            t1.info(operation.bodyString);
            TxnResponse res = operation.postOldTxnURLForHSB(t1);
            Assertion.verifyEqual(res.isTxnSuccessful(Constants.TXN_SUCCESS), true,
                    "Verify that when AUTO_BANK_ACC_LINK_MATCH_ID is set to N, bank account can be linked without providing details of the IDNO", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "Y");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void verifyUserRoletagshouldbeoptionalWhileCreatingBank() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_4", "verify UserRole tag should be optional");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "N");

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                    set("COMMAND.MSISDN", subsUser.MSISDN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    set("COMMAND.PROVIDER", trustBank.ProviderId),
                    set("COMMAND.BANKID", trustBank.BankID),
                    set("COMMAND.BANKNAME", trustBank.BankName),
                    set("COMMAND.IDNO", subsUser.ExternalCode),
                    set("COMMAND.FNAME", subsUser.FirstName),
                    set("COMMAND.LNAME", subsUser.LastName),
                    set("COMMAND.BANKACCNUMBER", DataFactory.getRandomNumberAsString(8)),
                    set("COMMAND.CUSTOMERID", DataFactory.getRandomNumberAsString(8)),
                    delete("COMMAND.USERROLE") // delete USERROLE

            );
            t1.info(operation.bodyString);
            TxnResponse res = operation.postOldTxnURLForHSB(t1);
            Assertion.verifyEqual(res.isTxnSuccessful(Constants.TXN_SUCCESS), true,
                    "Verify that when AUTO_BANK_ACC_LINK_MATCH_ID is set to N, " +
                            "bank account can be linked without providing details of the USERROLE", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "Y");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM})
    public void createBankForExsitingCustomerThroughAPI() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_5",
                "verify existed customer able to link bank with BANKREG,by providing UserRole as Customer.");
        try {
            User sub01 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub01, true, false);
            TxnResponse res = Transactions.init(t1)
                    .bankRegistration(sub01, trustBank, Constants.CUSTOMER_REIMB, null, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            Assertion.verifyEqual(res.isTxnSuccessful(Constants.TXN_SUCCESS), true,
                    "Verify that verify existed customer able to link bank with BANKREG,by providing UserRole as Customer. ", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void verifyUserRoleTagShouldBeOptionalForCustomer() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_6",
                "verify existed customer able to link bank with BANKREG,without providing UserRole as Customer. ");
        try {

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                    set("COMMAND.MSISDN", subsUser.MSISDN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.IDNO", subsUser.ExternalCode),
                    set("COMMAND.FNAME", subsUser.FirstName),
                    set("COMMAND.LNAME", subsUser.LastName),
                    set("COMMAND.BANKACCNUMBER", DataFactory.getRandomNumberAsString(8)),
                    set("COMMAND.CUSTOMERID", DataFactory.getRandomNumberAsString(8)),
                    delete("COMMAND.USERROLE") // delete USERROLE

            );
            t1.info(operation.bodyString);
            TxnResponse res = operation.postOldTxnURLForHSB(t1);
            Assertion.verifyEqual(res.isTxnSuccessful(Constants.TXN_SUCCESS), true,
                    "verify existed customer able to link bank with BANKREG,without providing UserRole as Customer.", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void createBankForChannelUserWhenBANK_ACC_LINKING_VIA_MSISDNIsN() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_7", "verify when BANK_ACC_LINKING_VIA_MSISDN preference is N:" +
                "Then Bank Account Number, Customer ID will be mandatory ");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            User sub02 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub02, true, false);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                    set("COMMAND.MSISDN", sub02.MSISDN),
                    set("COMMAND.PROVIDER", trustBank.ProviderId),
                    set("COMMAND.BANKID", trustBank.BankID),
                    set("COMMAND.BANKNAME", trustBank.BankName),
                    set("COMMAND.FNAME", sub02.FirstName),
                    set("COMMAND.LNAME", sub02.LastName),
                    set("COMMAND.IDNO", sub02.ExternalCode),
                    set("COMMAND.ACCOUNTTYPE", "01"),
                    set("COMMAND.CUSTOMERID", ""),
                    set("COMMAND.BANKACCNUMBER", "")
            );
            t1.info(operation.bodyString);
            TxnResponse res = operation.postOldTxnURLForHSB(t1);

            Assertion.verifyMessageContain(res.Message, "mBanking.error.custidrequired",
                    "Verify that Customer ID/ Bank Account num is mandatroy when associating Bank to Customer", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
        }
        Assertion.finalizeSoftAsserts();
    }

    User whsUserS1;

    @Test(priority = 9, groups = {FunctionalTag.v5_0})
    public void createBankForChannelUserWhenBANK_ACC_LINKING_VIA_MSISDNIsY() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("MON_4831_8", "verify when BANK_ACC_LINKING_VIA_MSISDN preference is Y:" +
                "Then Bank Account Number, Customer ID will be optional,If Account type is not present, then save it as Savings by default");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
            whsUserS1 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whsUserS1, false);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                    set("COMMAND.MSISDN", whsUserS1.MSISDN),
                    set("COMMAND.PROVIDER", trustBank.ProviderId),
                    set("COMMAND.BANKID", trustBank.BankID),
                    set("COMMAND.BANKNAME", trustBank.BankName),
                    set("COMMAND.FNAME", whsUserS1.FirstName),
                    set("COMMAND.LNAME", whsUserS1.LastName),
                    set("COMMAND.IDNO", whsUserS1.ExternalCode),
                    delete("COMMAND.BANKACCNUMBER"),
                    delete("COMMAND.ACCOUNTTYPE"),
                    delete("COMMAND.CUSTOMERID")

            );
            t1.info(operation.bodyString);
            operation.postOldTxnURLForHSB(t1)
                    .assertStatus(Constants.TXN_SUCCESS);
            Map<String, String> accountInfo = MobiquityGUIQueries
                    .dbGetAccountDetails(whsUserS1, defaultProvider.ProviderId, getDefaultBankId(defaultProvider.ProviderId));
            String channaccount = accountInfo.get("ACCOUNT_NO");
            DesEncryptor d = new DesEncryptor();
            String decchanacc = d.decrypt(channaccount);

            String customerId = accountInfo.get("CUST_ID");
            String accountType = accountInfo.get("ACCOUNT_TYPE");

            Assert.assertEquals(decchanacc, whsUserS1.MSISDN);
            Assert.assertEquals(customerId, whsUserS1.MSISDN);
            Assert.assertEquals(accountType, SAVINGS_ACCOUNT_TYPENO);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.v5_0})
    public void invalidIDNOWhenAUTO_BANK_ACC_LINK_MATCH_IDIsY() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_9",
                "verify system should throw error message when pass identification " +
                        "number which not associated with msisdn, when AUTO_BANK_ACC_LINK_MATCH_ID preference is Y.");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "Y");
            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                    set("COMMAND.MSISDN", whsUserS1.MSISDN),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", whsUserS1.FirstName),
                    set("COMMAND.LNAME", whsUserS1.LastName),
                    set("COMMAND.IDNO", whsUserS1.ExternalCode),
                    set("COMMAND.ACCOUNTTYPE", "01"),
                    set("COMMAND.BANKACCNUMBER", subsUser.MSISDN),
                    set("COMMAND.CUSTOMERID", subsUser.MSISDN)

            );
            t1.info(operation.bodyString);

            operation.postOldTxnURLForHSB(t1)
                    .verifyMessage("identification.number.notmatch.mobilenumber");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11, groups = {FunctionalTag.v5_0})
    public void createBankForExsitingChannelUserThroughAPIForAvailableProviders() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_13",
                "verify channel user/customer able to link bank with BANKREG api with new enhancements for multi currency(mfs1,mfs2). ");
        try {
            User sub04 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub04, true, false);
            for (String provider : DataFactory.getAllProviderNames()) {
                Bank bnk = DataFactory.getAllTrustBanksLinkedToProvider(provider).get(0);
                Transactions.init(t1)
                        .bankRegistration(sub04, bnk, Constants.CUSTOMER_REIMB, null, null)
                        .verifyStatus(Constants.TXN_SUCCESS);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 12, groups = {FunctionalTag.v5_0})
    public void createBankForUserWhoIsRegisteredAsBothChannelUserAndSubscriberThroughAPIWithNoUserRole() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4831_16/17/18",
                "Verify that bank registration should be successful for channel " +
                        "user when one MSISDN is registered as both for subsUser and channel " +
                        "user in the system when system preference CHANNEL_USER_AS_SUBS_ALLOWED = Y," +
                        "if UserRole is not passed/Channel/Customer.");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");

            User whs05 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs05, false);

            t1.info("Channel user: " + whs05.LoginId + "'s MSISDN is linked to new Subscriber");

            User subs05 = new User(Constants.SUBSCRIBER);
            subs05.setMSISDN(whs05.MSISDN);

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs05, true, false);

            Transactions.init(t1).bankRegistration(subs05, trustBank, Constants.CUSTOMER_REIMB, null, null);
            Transactions.init(t1).bankRegistration(whs05, trustBank, Constants.CHANNEL_REIMB, null, null);
            Transactions.init(t1).bankRegistration(subs05, nonTrustBank, Constants.CUSTOMER_REIMB, null, null);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "N");
        }
        Assertion.finalizeSoftAsserts();

    }
}
