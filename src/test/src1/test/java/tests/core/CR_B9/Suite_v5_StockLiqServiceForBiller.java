package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.SfmResponse;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.common.DataFactory.getRandomNumber;

public class Suite_v5_StockLiqServiceForBiller extends TestInit {

    private Biller biller;
    private String providerName;
    private OperatorUser billerRegistor, bankApprover, optuser, baruser;
    private User whs;
    private Bank defaultBank;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {

            providerName = DataFactory.getDefaultProvider().ProviderName;
            defaultBank = DataFactory.getAllTrustBanksLinkedToProvider(providerName).get(0);
            billerRegistor = DataFactory.getOperatorUserWithAccess("UTL_CREG");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            baruser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");
            optuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);


            SystemPreferenceManagement.init(eSetup).updateSystemPreference("IS_BANK_REQUIRED", "TRUE");

            biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);
            biller.setBillerType(Constants.BILLER_BILL_PAYEE);

            //Creating a biller
            BillerManagement.init(eSetup).createBillerWithLiquidationBank(biller, true);

            Login.init(eSetup).login(bankApprover);

            CommonUserManagement.init(eSetup)
                    .approveAllAssociatedLiquidationBanksForBiller(biller);

            TransactionManagement.init(eSetup)
                    .makeSureChannelUserHasBalance(whs);

            ServiceCharge billPayment = new ServiceCharge(Services.AgentAssistedBillPayment, whs, biller, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(billPayment);

            String accNo = DataFactory.getRandomNumberAsString(5);
            Transactions.init(eSetup).initiateOfflineBillPaymentForAdhoc(biller, whs, "500", accNo);

            ServiceCharge stockliq = new ServiceCharge(Services.STOCK_LIQUIDATION_SERVICE, biller, optuser, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(stockliq);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceAfterTransactorStatusIsChanged() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4781_5",
                "Verify that appropriate error message & error code is displayed " +
                        "if transactor status is changed during initiation of transaction.");

        try {
            //Create Operator User
            OperatorUser operatorUser = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);

            //Generating Stock Liquidation
            String batchId = "BA." + getRandomNumber(8);
            Transactions.init(t1).stockLiquidationGenerationForBiller(biller, batchId);

            //Getting Liquidation Id
            String liqId = MobiquityGUIQueries.getLiquidationIdForBiller(biller.BillerCode, batchId);

            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1)
                    .stockLiquidationServiceforBillerFail(biller, operatorUser, liqId, defaultBank.BankID)
                    .verifyMessage("transactor.barred.as.sender.and.receiver");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceaftersenderIsBarredAsReceiver() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4781_1",
                "Verify that during initiation of transaction and transaction should be successfull if sender is barred as receiver.");

        try {
            //Generating Stock Liquidation
            String batchId = "BA." + getRandomNumber(8);
            Transactions.init(t1).stockLiquidationGenerationForBiller(biller, batchId);

            //Getting Liquidation Id
            String liqId = MobiquityGUIQueries.getLiquidationIdForBiller(biller.BillerCode, batchId);

            //Bar the User as Receiver
            BillerManagement.init(t1)
                    .barBiller(biller, Constants.USER_TYPE_BILLER, Constants.BAR_AS_RECIEVER);

            //Perform Stock Liquidation
            Transactions.init(t1)
                    .stockLiquidationServiceForBiller(biller, baruser, liqId, defaultBank.BankID)
                    .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            //Unbar the User
            if (biller.isBarred()) {
                BillerManagement.init(t1)
                        .unBarBiller(biller, Constants.USER_TYPE_BILLER, Constants.BAR_AS_RECIEVER);
            }
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void stockLiquidationServiceAfterBillerStatusIsSuspendInitiateAndSuspended() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4781_3",
                "Verify that Stock Liquidation service is success if the Biller is Suspended Initiated");

        ExtentTest t2 = pNode.createNode("MON_4781_3_a",
                "Verify that Stock Liquidation service fails if the Biller is Suspended");

        ExtentTest t3 = pNode.createNode("MON_4781_2",
                "Verify Failure in generatin liquidation id when Biller is Suspended.");
        try {
            //Generation of Liquidation Id
            String batchId = "BA." + getRandomNumber(8);
            Transactions.init(t1).stockLiquidationGenerationForBiller(biller, batchId);

            //Suspend Initiate Biller
            BillerManagement.init(t1).suspendInitiateBiller(biller);
            //Getting Liquidation Id

            String liqId = MobiquityGUIQueries.getLiquidationIdForBiller(biller.BillerCode, batchId);

            //Performing Stock Liquidation Service
            Transactions.init(t1).stockLiquidationServiceForBiller(biller, billerRegistor, liqId, defaultBank.BankID)
                    .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);

            String batchId1 = "BA." + getRandomNumber(8);
            Transactions.init(t2).stockLiquidationGenerationForBiller(biller, batchId1);

            //Getting Liquidation Id
            String liqId1 = MobiquityGUIQueries.getLiquidationIdForBiller(biller.BillerCode, batchId1);

            //Suspend the Biller Biller
            BillerManagement.init(t2).suspendApproveBiller(biller);
            Transactions.init(t2).stockLiquidationServiceforBillerFail(biller, billerRegistor, liqId1, defaultBank.BankID)
                    .verifyMessage("sender.suspended");

            //Stock Liquidation Generation
            Transactions.init(t3).stockLiquidationGenerationForBillerForFail(biller)
                    .verifyMessage("888001");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            if (biller.isSuspended()) {
                Markup m = MarkupHelper.createLabel("TearDown", ExtentColor.PINK);
                pNode.info(m); // Method Start Marker
                BillerManagement.init(t1).initiateActivateBiller(biller);
                BillerManagement.init(t1).approveActivateBiller(biller);
            }
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "stockLiquidationServiceAfterBillerStatusIsSuspendInitiateAndSuspended")
    public void eigCallAfterBillerStatusIsSuspended() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4781_3",
                "Verify if biller is Suspended After Stock Liquidation Service,Service is getting passed");
        try {
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);
            //get the initial balance of IND04
            BigDecimal preBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankId);

            //Generation of Liquidation Id
            String batchId = "BA." + getRandomNumber(8);
            Transactions.init(t1).stockLiquidationGenerationForBiller(biller, batchId);
            //Getting Liquidation Id

            String liqId = MobiquityGUIQueries.getLiquidationIdForBiller(biller.BillerCode, batchId);

            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationServiceForBiller(biller, billerRegistor, liqId, bankId);
            String serviceId = response.ServiceRequestId;

            //Suspend Initiate Biller
            BillerManagement.init(t1).suspendInitiateBiller(biller);
            BillerManagement.init(t1).suspendApproveBiller(biller);

            Transactions.init(t1).performEIGTransactionforFail(serviceId, "true", Services.RESUME_STOCK_LIQUIDATION_SERVICE);

            //check IND04 got debited
            BigDecimal postBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankId);
            Assertion.verifyEqual((preBankBalance.compareTo(postBankBalance) < 0), true, "Verify that Bank Balance is Credited", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            if (biller.isSuspended()) {
                BillerManagement.init(t1).initiateActivateBiller(biller);
                BillerManagement.init(t1).approveActivateBiller(biller);
            }
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void eigCallAfterBillerStatusIsBlockedAsReceiver() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4781_4",
                "Verify if biller is Blocked As Reciever After Stock Liquidation Service,Service is getting Passed");
        try {
            //get the initial balance of IND04
            BigDecimal preBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, defaultBank.BankID);
            t1.info("Initial balance of IND04 is " + preBankBalance);
            //Generation of Liquidation Id

            String batchId = "BA." + getRandomNumber(8);
            Transactions.init(t1)
                    .stockLiquidationGenerationForBiller(biller, batchId);

            //Getting Liquidation Id
            String liqId = MobiquityGUIQueries.getLiquidationIdForBiller(biller.BillerCode, batchId);

            //Performing Stock Liquidation Service
            String serviceId = Transactions.init(t1)
                    .stockLiquidationServiceForBiller(biller, billerRegistor, liqId, defaultBank.BankID)
                    .verifyStatus(Constants.TXN_STATUS_SUCCEEDED)
                    .getServiceRequestId();


            //Bar Biller
            BillerManagement.init(t1).barBiller(biller, Constants.USER_TYPE_BILLER, Constants.BAR_AS_RECIEVER);

            Transactions.init(t1).performEIGTransaction(serviceId, "true", Services.RESUME_STOCK_LIQUIDATION_SERVICE);

            //check IND04 got debited
            BigDecimal postBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, defaultBank.BankID);
            t1.info("current balance of IND04 is " + postBankBalance);

            Assertion.verifyEqual((preBankBalance.compareTo(postBankBalance) < 0), true, "Verify that Bank Balance is Credited", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            //Unbar the User
            if (biller.isBarred()) {
                BillerManagement.init(t1)
                        .unBarBiller(biller, Constants.USER_TYPE_BILLER, Constants.BAR_AS_RECIEVER);
            }
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 5, groups = {FunctionalTag.v5_0}, enabled = false)
    public void stockLiquidationServiceAfterTransactorIsDeleteInitiatedAndDeleted() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4781_6",
                "Verify that appropriate error message & error code is displayed if transactor" +
                        " is delete initiated/deleted during initiation of transaction");

        try {
            //Create Operator User
            OperatorUser operatorUser = new OperatorUser(Constants.NETWORK_ADMIN);
            OperatorUserManagement.init(t1).createAdmin(operatorUser);
            SuperAdmin saMaker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            SuperAdmin saChecker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
            Login.init(t1).loginAsSuperAdmin(saMaker);
            //Delete Initiate Operator
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(operatorUser, false);
            //Generating Stock Liquidation
            String batchId = "BA." + getRandomNumber(8);
            Transactions.init(t1).stockLiquidationGenerationForBiller(biller, batchId);
            //Getting Liquidation Id

            String liqId = MobiquityGUIQueries.getLiquidationIdForBiller(biller.BillerCode, batchId);
            String Bankid = MobiquityGUIQueries.getBankId(DataFactory.getTrustBankName("MFS1"));
            //Performing Stock Liquidation Service
            SfmResponse response = Transactions.init(t1).stockLiquidationServiceforBillerFail(biller, operatorUser, liqId, Bankid);

            response.verifyMessage("transactor.delete.initiated");

            //Delete Operator
            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(operatorUser, true);
            //Performing Stock Liquidation Service
            SfmResponse response1 = Transactions.init(t1).stockLiquidationServiceforBillerFail(biller, operatorUser, liqId, Bankid);
            response1.verifyMessage("transactor.notRegistered");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @AfterClass(alwaysRun = true)
    public void resetPreference() throws Exception {
        ExtentTest tTeardown = pNode.createNode("Teardown", "After Class Methods for Current Suite");

        try {
            SystemPreferenceManagement.init(tTeardown).updateSystemPreference("IS_BANK_REQUIRED", "FALSE");
        } catch (Exception e) {
            markTestAsFailure(e, tTeardown);
        }
        Assertion.finalizeSoftAsserts();

    }
}

