package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.escrowToEscrowTransfer.EscrowToEscrowTransfer;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_EscrowToEscrowTransfer_MON_4467
 * Author Name      : Pushpalatha
 * Created Date     : 4/02/2018
 */

public class Suite_v5_EscrowToEscrowTransfer_MON_4467 extends TestInit {

    private String bank1, bank2, refNo, defaultProvider;
    private OperatorUser net;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {

            defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            bank1 = DataFactory.getNonTrustBankName(defaultProvider);
            bank2 = DataFactory.getTrustBankName(defaultProvider);
            refNo = DataFactory.getRandomNumberAsString(5);
            net = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void EscrowToEscrowAprrovalPageFieldValidation() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4467_SC1_SC2",
                "Verify that superadmin (checker) and network admin having Escrow to Escrow Transfer Approval link permission under Stock Management link");
        try {

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            String txnId1 = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            OperatorUser netAdmn = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            Login.init(t1).login(netAdmn);
            EscrowToEscrowTransfer.init(t1).checkApprovalFields(txnId1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void verifyInApprovalScreenEscrowTEscrowTransfersPendingForApprovalButNotInitiatedByTheSameUserIsDisplayed() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4467_SC3",
                "Verify that on clicking Escrow to Escrow Transfer Approval link, all Escrow to Escrow Transfers pending for approval, but not initiated by the same user who has logged in should be listed");
        try {

            //Initiated by network admin and approval by Super Admin
            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");

            String txnId = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            OperatorUser netAdmn = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(netAdmn);

            String userId = MobiquityGUIQueries.getUserId(netAdmn.LoginId);

            //Network Admin
            EscrowToEscrowTransfer.init(t1).listOfInitiatedTxnValidation(txnId, userId);

            //Initiated by network admin and approval by Super Admin
            Login.init(t1).login(netAdmn);
            String txnId1 = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            //Super Admin
            SuperAdmin supradc = DataFactory.getSuperAdminWithAccess("ESCROW_APP");
            String userId1 = MobiquityGUIQueries.getUserId(supradc.LoginId);
            Login.init(t1).loginAsSuperAdmin("ESCROW_APP");
            EscrowToEscrowTransfer.init(t1).listOfInitiatedTxnValidation(txnId1, userId1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void verifythatAfterApprovalOrRejectionParticularRecordShouldNotDisplay() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4467_SC6",
                "Verify that after approval/ rejection of initiated Escrow to Escrow Transfer particular record(s) should not display in the screen");
        try {

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            String txnId = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            OperatorUser netAdmn = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(netAdmn);
            EscrowToEscrowTransfer.init(t1).approveEscrowToEscrowTransfer(String.valueOf(txnId));

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            String txnId2 = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            Login.init(t1).login(netAdmn);
            EscrowToEscrowTransfer.init(t1).rejectEscrowToEscrowTransfer(String.valueOf(txnId2), "testing");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void verifyThatSuperAdminCheckerOrNetworkAdminIsAbleToApproveTheInitiatedEscrowtoEscrowTransfer() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4467_SC4_SC7",
                "4.Verify that superadmin (checker) or network admin is able to approve the initiated Escrow to Escrow Transfer and proper success message should display" +
                        "7.Verify that upon Escrow to Escrow Transfer approval proper credit debit should happen from the respective banks");
        try {

            BigDecimal amount = new BigDecimal(5);
            String bank1WalletNo = DataFactory.getBankId(bank1);
            String bank2WalletNo = DataFactory.getBankId(bank2);

            BigDecimal prePayerbal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank1WalletNo);
            BigDecimal prePayeebal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank2WalletNo);

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            String txnId1 = EscrowToEscrowTransfer.init(t1)
                    .initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, amount.toString());

            Login.init(t1).loginAsSuperAdmin("ESCROW_APP");
            EscrowToEscrowTransfer.init(t1)
                    .approveEscrowToEscrowTransfer(String.valueOf(txnId1));

            BigDecimal postPayerbal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank1WalletNo);
            BigDecimal postPayeebal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank2WalletNo);

            Assertion.verifyAccountIsDebited(prePayerbal, postPayerbal, amount, "Verify That Payer Balance is Debited", t1);
            Assertion.verifyAccountIsCredited(prePayeebal, postPayeebal, amount, "Verify That Payee Balance is Credited", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void verifyThatSuperAdminCheckerOrNetworkAdminIsAbleToRejectTheInitiatedEscrowtoEscrowTransfer() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4467_SC5_SC7",
                "5.Verify that superadmin (checker) or network admin is able to reject the initiated Escrow to Escrow Transfer and proper message should display %n" +
                        "7.Verify that upon Escrow to Escrow Transfer approval proper credit debit should happen from the respective banks");
        try {

            String bank1WalletNo = DataFactory.getBankId(bank1);
            String bank2WalletNo = DataFactory.getBankId(bank2);

            BigDecimal prePayerbal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank1WalletNo);
            t1.info("Pre Payer balance fetched from DB " + prePayerbal);
            BigDecimal prePayeebal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank2WalletNo);
            t1.info("Pre Payee balance fetched from DB " + prePayeebal);

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            String txnId2 = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            OperatorUser netAdmn = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(netAdmn);
            EscrowToEscrowTransfer.init(t1)
                    .rejectEscrowToEscrowTransfer(String.valueOf(txnId2), "test");

            BigDecimal postPayerbal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank1WalletNo);
            t1.info("Post Payer balance fetched from DB " + postPayerbal);
            BigDecimal postPayeebal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank2WalletNo);
            t1.info("Post Payee balance fetched from DB " + postPayeebal);

            Assertion.verifyEqual(prePayerbal.compareTo(postPayerbal) == 0, true, "Verify that Post payer Balance is Not Debited", t1);
            Assertion.verifyEqual(prePayeebal.compareTo(postPayeebal) == 0, true, "Verify that Post payee Balance is Not Credited", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void verifyThatProperErrorMessageIsDisplayedOnRejectingaEscrowtoEscrowTransferWithoutEnteringRemarks() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4467_SC9",
                "Verify that proper error message is displayed on rejecting a Escrow to Escrow Transfer without entering remarks");
        try {

            String bank1WalletNo = DataFactory.getBankId(bank1);
            String bank2WalletNo = DataFactory.getBankId(bank2);

            BigDecimal prePayerbal = MobiquityGUIQueries
                    .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank1WalletNo);
            t1.info("Pre Payer balance fetched from DB " + prePayerbal);
            BigDecimal prePayeebal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bank2WalletNo);
            t1.info("Pre Payee balance fetched from DB " + prePayeebal);

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            String txnId2 = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            Login.init(t1).loginAsSuperAdmin("ESCROW_APP");
            EscrowToEscrowTransfer.init(t1)
                    .startNegativeTest().rejectEscrowToEscrowTransfer(String.valueOf(txnId2), "");

            Assertion.verifyErrorMessageContain("escrow.approval.rejection.remarks.required", "Reject without remarks", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void verifyThatNetworkAdminIsnotabletoApproveEscrowToEscrowTransferWhenHeIsBarredInTheSystem() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_3460_SC8",
                "Verify that network admin is not able to initiate Escrow to Escrow transfer when he is barred in the system and system should display proper error message");
        try {

            Login.init(t1).login(net);
            String txnId1 = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            Transactions.init(t1).optUserBarUnbar(net, "BAR", Constants.BAR_AS_BOTH);

            Login.init(t1).loginAsSuperAdmin("ESCROW_APP");
            EscrowToEscrowTransfer.init(t1).startNegativeTest().approveEscrowToEscrowTransfer(String.valueOf(txnId1));

            Assertion.verifyActionMessageContain("escrow.approval.party.barred", "Escrow Approval not successful", t1);

            Transactions.init(t1).optUserBarUnbar(net, "UNBAR", Constants.BAR_AS_BOTH);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
