package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.HashMap;

import static framework.util.common.DataFactory.getRandomNumberAsString;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_BankAccountRegistrationWEB_Negative_MON_4443
 * Author Name      : Pushpalatha
 * Created Date     : 1/07/2018
 */

public class Suite_v5_BankAccountRegistrationWEB_Negative_MON_4443 extends TestInit {

    private OperatorUser optNetAdmin, optBankApprover, optModifyuser;
    private User whs, subs;
    private String provider, custId, accNo;
    private Bank defaultBank, nonTrustBank;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            provider = DataFactory.getDefaultProvider().ProviderName;
            defaultBank = DataFactory.getAllTrustBanksLinkedToProvider(provider).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(provider).get(0);
            optNetAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            optBankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            optModifyuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");

            subs = new User(Constants.SUBSCRIBER);
            whs = new User(Constants.WHOLESALER);

            // map wallet preferences for channel User
            CurrencyProviderMapping.init(eSetup)
                    .mapWalletPreferencesUserRegistration(whs, DataFactory.getPayIdApplicableForChannelUser());

            CurrencyProviderMapping.init(eSetup)
                    .mapWalletPreferencesUserRegistration(subs, DataFactory.getPayIdApplicableForSubs());

            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(subs);
            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(whs);

            ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(whs, false);
            SubscriberManagement.init(eSetup).createSubscriberDefaultMapping(subs, true, false);

            //Setting preference AUTO_BANK_ACC_LINK_MATCH_ID to Y
            SystemPreferenceManagement.init(eSetup).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            SystemPreferenceManagement.init(eSetup).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @BeforeMethod(alwaysRun = true)
    public void generateCustIDandAccNO() throws Exception {

        custId = DataFactory.getRandomNumberAsString(6);
        accNo = DataFactory.getRandomNumberAsString(9);

    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void verifyThatBankCustomerIDandAccountNumberShouldBeUniqueInTheSystemWhileAssociatingBankAccountWithTheUserThroughBankAccountRegistration() throws Exception {

        ExtentTest t1 = pNode.createNode("SC12",
                "Verify that customer ID is unique per bank in the system " +
                        "while associating bank account(s) with the user through Bank Account Registration");
        try {
            //Setting preference AUTO_BANK_ACC_LINK_MATCH_ID to N
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(whs, provider, defaultBank.BankName, custId, accNo, false);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(whs, provider, defaultBank.BankName);


            Login.init(t1).login(optNetAdmin);

            startNegativeTest();
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(whs, provider, defaultBank.BankName, custId, getRandomNumberAsString(9), false);
            Assertion.verifyErrorMessageContain("bankAccount.registration.custId.alreadyAssociated",
                    "Customer ID already associated", t1, custId, defaultBank.BankName);

            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(whs, provider, defaultBank.BankName, getRandomNumberAsString(6), accNo, false);
            Assertion.verifyErrorMessageContain("bankAccount.registration.accNo.alreadyAssociated",
                    "Account Number already associated", t1, accNo);

            stopNegativeTest();

           /* Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(whs, provider, defaultBank.BankName, custId, accNo, false);

            Login.init(t1).login(optNetAdmin);
            BankAccountRegistration_Pg1 page1 = new BankAccountRegistration_Pg1(t1);
            BankAccountRegistration_Pg2 page2 = new BankAccountRegistration_Pg2(t1);

            page1.navigateToLink();
            page1.selectProvider(provider);
            page1.selectUserType(Constants.USR_TYPE_SUBS);
            page1.setMSISDNPrefN(subs.MSISDN);
            page1.clickSubmitBtnPrefN();
            page2.clickAddMoreBtn();

            page2.clickAddMoreBtnAgain();
            Assertion.verifyErrorMessageContain("bankAccount.registration.add.account.twice", "Account Number already associated", t1, subs.MSISDN);*/
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void verifyThatNetworkAdminIsNotAbleToAssociateBankWhenKYCPreferenceIsNotSetForBank() throws Exception {

        ExtentTest t1 = pNode.createNode("SC04",
                "Verify that network admin is not able to associate bank account(s) " +
                        "with user when KYC preference is not defined for bank ");

        try {

            // make sure that the KYC preference for Bank is not set
            MobiquityGUIQueries.deleteKYCPreferenceForBank(Constants.SUBSCRIBER,
                    Constants.PREFERENCE_TYPE_BANK_ACCOUNT_LINKING,
                    defaultBank.BankID,
                    Constants.REGTYPE_NO_KYC
            );

            ConfigInput.isAssert = false; // start Negative test
            Transactions.init(t1)
                    .bankRegistration(subs, defaultBank, Constants.CUSTOMER_REIMB, null, null)
                    .verifyMessage("90001");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            // make sure Bank Account link KYC Preference is Mapped
            CurrencyProviderMapping.init(t1).linkPrimaryBankWalletPreference(subs);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void verifyThatCustomerIDAndAccountNumberIsMandatoryWhenBANK_ACC_LINKING_VIA_MSISDNisN() throws Exception {

        ExtentTest t1 = pNode.createNode("SC08",
                "Verify that Customer ID & Account Number is mandatory " +
                        "when BANK_ACC_LINKING_VIA_MSISDN = N " +
                        "while bank account association through Bank Account Registration");

        try {
            //Setting preference AUTO_BANK_ACC_LINK_MATCH_ID to N
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1)
                    .startNegativeTest()
                    .initiateBankAccountRegistration(subs, provider, defaultBank.BankName, "", accNo, false);
            Assertion.verifyErrorMessageContain("mBanking.error.custidrequired", "CustomerID is mandatory", t1);

            BankAccountAssociation.init(t1)
                    .startNegativeTest()
                    .initiateBankAccountRegistration(subs, provider, defaultBank.BankName, custId, "", false);
            Assertion.verifyErrorMessageContain("bank.account.registration.accNo.error", "Account Number is mandatory", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void verifyThatAdminIsAbleToSuspendTheBankAccountAssociatedInBankAccountRegistrationThroughModifyChannelUser() throws Exception {

        ExtentTest t1 = pNode.createNode("SC10",
                "Verify that network admin or channel admin is able to modify " +
                        "the bank account(s) associated in Bank Account Registration through Modify Channel User");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1).initiateBankAccountRegistration(whs, provider, defaultBank.BankName, custId, accNo, false);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1).approveAssociatedBanksForUser(whs, provider, defaultBank.BankName);

            Login.init(t1).login(optModifyuser);
            BankAccountAssociation.init(t1).modifyBankStatusForChannelUser(whs, "Suspended", accNo);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1).approveAssociatedBanksForUser(whs, provider, defaultBank.BankName);

            String status = MobiquityGUIQueries.getBankAccountStatus(custId);

            DBAssertion.verifyDBAssertionEqual(status, "S", "Bank account is suspended", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    // this test is covered throughout this suite, by deleting bank account via modification page
    @Test(priority = 5, groups = {FunctionalTag.v5_0}, enabled = false)
    public void verifyThatAdminIsAbleToDeleteTheBankAccountAssociatedInBankAccountRegistrationThroughModifyChannelUser() throws Exception {

        ExtentTest t1 = pNode.createNode("SC11",
                "Verify that network admin or channel admin is able to delete " +
                        "the bank account(s) associated in Bank Account Registration through Modify Channel User");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            User whs1 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs1, false);

            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(whs1, provider, defaultBank.BankName, custId, accNo, false);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(whs1, provider, defaultBank.BankName);

            Login.init(t1).login(optModifyuser);
            ChannelUserManagement.init(t1)
                    .modifyUserApproval(whs1);

            BankAccountAssociation.init(t1)
                    .modifyBankStatusForChannelUser(whs1, "Deleted", accNo);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(whs1, provider, defaultBank.BankName);

            String status = MobiquityGUIQueries.getBankAccountStatus(custId);

            DBAssertion.verifyDBAssertionEqual(status, "N", "Bank account is deleted", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /********DeRegistration********************/

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void verifyThatNetworkAdminIsAbleToDeleteTheBankaccountofanuserthroughBankAccountDeRegistration() throws Exception {

        ExtentTest t1 = pNode.createNode("SC01_DeRegistration",
                "Verify that network admin is able to delete the bank account(s) " +
                        "of an user through Bank Account Association > Bank Account DeRegistration");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            //Associate Bank with the user
            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(subs, provider, defaultBank.BankName, custId, accNo, false);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(subs, provider, defaultBank.BankName);

            HashMap<String, String> value = BankAccountAssociation.init(t1)
                    .modifySubscriberPageBankCheck(subs, whs, accNo);

            Utils.captureScreen(t1);
            Assertion.verifyEqual(value.get("LinkedBank"), defaultBank.BankName,
                    "Verify Selected Bank during registration is retained as Linked Bank", t1);

            Assertion.verifyEqual(value.get("CustomerId"), custId,
                    "MSISDN is set as customer Id for the user", t1);

            Assertion.verifyEqual(value.get("AccountNumber"), accNo,
                    "MSISDN is set as Account Number for the user", t1);

            // initiate de registration
            Login.init(t1).login(optNetAdmin);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountDeRegistration(subs, provider, accNo);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .approveAssociatedBanksForUser(subs, provider, defaultBank.BankName);

            HashMap<String, String> value1 = BankAccountAssociation.init(t1).modifySubscriberPageBankCheck(subs, whs, accNo);

            if (value1.get("AccountNumber") == null) {
                t1.pass("Bank is deleted");
            } else {
                t1.fail("Bank is not deleted");
                Assert.fail();
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void VerifythatbankaccountdeletionthroughBankAccountDeRegistrationshouldbesuccessfulforthebankaccountaddedthroughwebAPI() throws Exception {

        ExtentTest t1 = pNode.createNode("SC02_DeRegistration",
                "Verify that bank account(s) deletion through Bank Account DeRegistration should " +
                        "be successful for the bank account(s) added through web & API");
        try {

            //Associate Bank with the user
            if (Transactions.init(t1)
                    .bankRegistration(subs, defaultBank, Constants.CUSTOMER_REIMB, accNo, null).isSuccess()) {
                HashMap<String, String> value = BankAccountAssociation.init(t1)
                        .modifySubscriberPageBankCheck(subs, whs, accNo);

                Assertion.verifyEqual(value.get("LinkedBank"), defaultBank.BankName,
                        "Verify Selected Bank during registration is retained as Linked Bank", t1);

                Assertion.verifyEqual(value.get("AccountNumber"), accNo,
                        "MSISDN is set as Account Number for the user", t1);

                Login.init(t1).login(optNetAdmin);
                BankAccountAssociation.init(t1)
                        .initiateBankAccountDeRegistration(subs, provider, accNo);

                Login.init(t1).login(optBankApprover);
                CommonUserManagement.init(t1)
                        .approveAssociatedBanksForUser(subs, provider, defaultBank.BankName);

                HashMap<String, String> value1 = BankAccountAssociation.init(t1).modifySubscriberPageBankCheck(subs, whs, accNo);

                if (value1.get("AccountNumber") == null) {
                    t1.pass("Bank is deleted");
                } else {
                    t1.fail("Bank is not deleted");
                    Assert.fail();
                }
            } else {
                t1.fail("Test failed to associate Bank via API");
            }


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void VerifythatbankaccountdeleteInitiatedthroughBankAccountDeRegistrationshouldBeDisplayedForTheUserIfChannelAdminRejects() throws Exception {

        ExtentTest t1 = pNode.createNode("SC03_DeRegistration",
                "Verify that bank account(s) which is delete initiated through Bank Account DeRegistration on rejection by channel admin should be displayed in modify page of the user");
        try {

            if (Transactions.init(t1)
                    .bankRegistration(subs, defaultBank, Constants.CUSTOMER_REIMB, accNo, null).isSuccess()) {
                HashMap<String, String> value = BankAccountAssociation.init(t1)
                        .modifySubscriberPageBankCheck(subs, whs, accNo);

                Assertion.verifyEqual(value.get("LinkedBank"), defaultBank.BankName,
                        "Verify Selected Bank during registration is retained as Linked Bank", t1);

                Assertion.verifyEqual(value.get("AccountNumber"), accNo,
                        "MSISDN is set as Account Number for the user", t1);

                Login.init(t1).login(optNetAdmin);
                BankAccountAssociation.init(t1)
                        .initiateBankAccountDeRegistration(subs, provider, accNo);

                Login.init(t1).login(optBankApprover);
                CommonUserManagement.init(t1)
                        .rejectAssociatedBanksForUser(subs, provider, defaultBank.BankName, accNo);

                HashMap<String, String> value1 = BankAccountAssociation.init(t1).modifySubscriberPageBankCheck(subs, whs, accNo);

                Assert.assertEquals(value1.get("AccountNumber"), String.valueOf(accNo));
                t1.pass("Account is not deleted");

            } else {
                t1.fail("Test failed to associate Bank via API");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

}
