package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.entity.InstrumentTCP;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.AddEnterprise_Pg3;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

public class Suite_v5_EnterpriseAssociatedWithBank extends TestInit {

    private OperatorUser usrCreator, bankApprover, modifyuser, usrApprover;
    private CurrencyProvider defaultProvider;
    private User entUser;
    private Bank defaultBank, nonTrustBank;


    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {

            entUser = new User(Constants.ENTERPRISE);
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            modifyuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            defaultProvider = DataFactory.getDefaultProvider();
            defaultBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);

            SystemPreferenceManagement.init(eSetup)
                    .updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        ExtentTest t = pNode.createNode("teardown", "Teardown specific for this script");
        SystemPreferenceManagement.init(t)
                .updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE"); // TODO - need a final reference variable
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void MON_4469_1() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4469_1",
                "Verify Operator user should able to register enterprise along with banking details. ");

        ExtentTest t2 = pNode.createNode("MON_4469_2",
                "Verify after registering enterprise with banking details," +
                        "details are getting populated in confirm and approval page. ");

        try {
            //Create ChannelUser With Bank
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(entUser)
                    .assignHierarchy(entUser)
                    .assignWebGroupRole(entUser);

            ChannelUserManagement.init(t1)
                    .setEnterprisePreference()
                    .setEnterpriseCategoryWalletCombination();

            CommonUserManagement.init(t1)
                    .mapDefaultWalletPreferences(entUser);

            // Set enterprise Preference and Bank Mapping
            AddEnterprise_Pg3 page3 = AddEnterprise_Pg3.init(t1);
            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(t1);

            page3.setEnterpriseLimit2(entUser.EnterpriseLimit);
            page3.clickOnBulkRegistrationIsRequired();
            page3.selectBulkPayerType();
            page3.setCompCode(DataFactory.getRandomNumberAsString(5));
            page3.clickNext();
            page5.addBankPreferences(entUser);
            page5.addLiquidationBankPreferences(entUser);
            page5.clickNext(entUser.CategoryCode);

            /*
            Verify that Banks are shown on the Confirmation page
             */
            List<String> bankList = DataFactory.getAllBankNamesForUserCreation(GlobalData.defaultProvider.ProviderName);
            List<String> liquidationBankList = DataFactory.getAllLiquidationBanksLinkedToProvider(GlobalData.defaultProvider.ProviderName);
            for (String bankName : bankList) {
                Assertion.verifyEqual(page5.isSelectedTextIsShownOnConfirmPage(bankName), true,
                        "Verify Bank: " + bankName + " is shown on the confirmation page", t2);
            }
            for (String bankName : liquidationBankList) {
                Assertion.verifyEqual(page5.isSelectedTextIsShownOnConfirmPage(bankName), true,
                        "Verify Liquidation Bank: " + bankName + " is shown on the confirmation page", t2);
            }
            Utils.scrollToBottomOfPage();
            Utils.captureScreen(t2);

            page5.completeUserCreation(entUser, false);

            // Verify Enterprise user is registered successfully {this verification is part of Test1}
            Assertion.verifyActionMessageContain("channeluser.add.approval",
                    "Verify That Enterprise User can be registered into system along with Bank Details", t1);

            // Verify Bank Details are shown on the Approval Page
            Login.init(t2)
                    .login(usrApprover);

            startNegativeTestWithoutConfirm(); // make sure that confirm page is available for Validation
            CommonUserManagement.init(t2)
                    .addInitiatedApproval(entUser, true);

            CommonChannelUserPage appConfirmpage = CommonChannelUserPage.init(t2);
            for (String bankName : bankList) {
                Assertion.verifyEqual(appConfirmpage.isSelectedTextIsShownOnConfirmPage(bankName), true,
                        "Verify Bank: " + bankName + " is shown on the Approval page", t2);
            }

            for (String bankName : liquidationBankList) {
                Assertion.verifyEqual(appConfirmpage.isSelectedTextIsShownOnConfirmPage(bankName), true,
                        "Verify Bank: " + bankName + " is shown on the confirmation page", t2);
            }

            Utils.scrollToBottomOfPage();
            Utils.captureScreen(t2);

            stopNegativeTestWithoutConfirm(); // resume assertions and complete Approval
            CommonUserManagement.init(t1)
                    .addInitiatedApproval(entUser, true);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1)
                    .approveAllAssociatedBanks(entUser);

            CommonUserManagement.init(t1)
                    .changeFirstTimePassword(entUser);

            Transactions.init(t1)
                    .changeChannelUserMPinTPin(entUser);

            Assertion.verifyEqual(entUser.getStatus().equals(Constants.STATUS_ACTIVE), true,
                    "Completed Enterprise Registration along with Bank association", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "MON_4469_1")
    public void verifyBankDetailsAreAvailableInModifyViewSuspendPage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4469_3",
                "Verify after registering enterprise with banking details," +
                        "details are getting populated in Modify, View, Suspend Page. ");

        try {
            Login.init(t1).login(modifyuser);

            ChannelUserManagement.init(t1)
                    .initiateChannelUserModification(entUser);
            CommonUserManagement.init(t1)
                    .navModifyToBankMapping(entUser);

            Select sel5 = new Select(driver.findElement(By.xpath("//select[@id='0']")));
            String providerActual = sel5.getFirstSelectedOption().getText();

            Select sel6 = new Select(driver.findElement(By.xpath("(//select[@id='walletTypeID0'])")));
            String bankActual = sel6.getFirstSelectedOption().getText();

            Select sel8 = new Select(driver.findElement(By.xpath("(//select[@id='grade0'])")));
            String gradeActual = sel8.getFirstSelectedOption().getText();

            Select sel9 = new Select(driver.findElement(By.xpath("(//select[@id='tcp0'])")));
            String tcpActual = sel9.getFirstSelectedOption().getText();

            WebElement custId1 = driver.findElement(By.xpath("//input[@id='customerId0']"));
            String custIdActual = custId1.getAttribute("value");

            WebElement account1 = driver.findElement(By.xpath("//input[@id='accountNumber0']"));
            String accountActual = account1.getAttribute("value");

            Utils.captureScreen(t1);
            t1.info("Provider: " + providerActual);
            t1.info("Bank: " + bankActual);
            t1.info("Grade: " + gradeActual);
            t1.info("Tcp: " + tcpActual);
            t1.info("Customer ID: " + custIdActual);
            t1.info("Account : " + accountActual);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    private User ent_02;

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_4469_4() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4469_4",
                "Verify Operator user should able to add bank to existing enterprise User.");

        ent_02 = new User(Constants.ENTERPRISE);
        try {
            // Get an enterprise User without Bank association
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(ent_02, false);

            //Modifying the User
            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).initiateChannelUserModification(ent_02);
            CommonUserManagement.init(t1)
                    .navModifyToBankMapping(ent_02)
                    .mapBankPreferences(ent_02, true);

            //Approving the User
            ChannelUserManagement.init(t1).approveChannelUserModification(ent_02);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1).approveAllAssociatedBanks(ent_02);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "MON_4469_4")
    public void MON_4469_7() throws Exception {

        ExtentTest t0 = pNode.createNode("MON_4469_9_a",
                "Verify channel admin should Not be able to Delete Primary Bank Associated with the Enterprise.");
        try {

            //Modifying Status of Bank
            Login.init(t0).login(modifyuser);
            ConfigInput.isConfirm = false;
            BankAccountAssociation.init(t0)
                    .modifyBankStatusForChannelUserBasedOnBankName(ent_02, "Deleted", defaultBank.BankName);
            Assertion.verifyErrorMessageContain("channel.error.validate.noprimary.bank",
                    "Verify that Primary Bank Linked to enterprise can not be deleted", t0);

        } catch (Exception e) {
            markTestAsFailure(e, t0);
        } finally {
            ConfigInput.isConfirm = true;
        }

        ExtentTest t1 = pNode.createNode("MON_4469_9",
                "Verify channel admin should be able to reject the deleted bank for existing enterprise.");
        try {

            Login.init(t0).login(modifyuser);
            String bankCustId = BankAccountAssociation.init(t1)
                    .modifyBankStatusForChannelUserBasedOnBankName(ent_02, "Deleted", nonTrustBank.BankName);

            // approve modification
            ChannelUserManagement.init(t1)
                    .approveChannelUserModification(ent_02);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1)
                    .rejectAssociatedBanksForUser(ent_02, DataFactory.getDefaultProvider().ProviderName, nonTrustBank.BankName, bankCustId);

            String status = MobiquityGUIQueries.getBankAccountStatus(bankCustId);
            DBAssertion.verifyDBAssertionEqual(status, "Y", "Verify that Bank Modification is rejected and Bank account is not deleted", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON_4469_7",
                "Verify channel admin should be able to delete bank for existing enterprise.");
        try {
            Login.init(t2).login(modifyuser);

            String bankCustId = BankAccountAssociation.init(t2)
                    .modifyBankStatusForChannelUserBasedOnBankName(ent_02, "Deleted", nonTrustBank.BankName);

            // approve modification
            ChannelUserManagement.init(t2)
                    .approveChannelUserModification(ent_02);

            //Approving Channel Bank Modification
            Login.init(t2).login(bankApprover);
            CommonUserManagement.init(t2)
                    .approveAssociatedBanksForUser(ent_02, DataFactory.getDefaultProvider().ProviderName, nonTrustBank.BankName);

            String status = MobiquityGUIQueries.getBankAccountStatus(bankCustId);
            DBAssertion.verifyDBAssertionEqual(status, "N", "Verify taht Bank Modification is succesful and Bank account is Deleted", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    User ent_03;

    @Test(groups = {FunctionalTag.v5_0}, priority = 3)
    public void MON_4469_5() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4469_5",
                "Verify Operator user should able to add bank through api for enterprise.");

        ent_03 = new User(Constants.ENTERPRISE);
        try {
            // make sure Bank Mapping is done in waller preference
            CurrencyProviderMapping.init(t1).linkPrimaryBankWalletPreference(ent_03);

            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(ent_03, false);

            Transactions.init(t1)
                    .bankRegistration(ent_03, defaultBank, Constants.CHANNEL_REIMB, null, null);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "MON_4469_5")
    public void MON_4469_8() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4469_8",
                "Verify channel admin should be able to reject the suspended bank for existing enterprise.");

        //Create ChannelUser With Bank
        try {
            //Modifying Status of Bank
            Login.init(t1).login(modifyuser);
            String bankCustId = BankAccountAssociation.init(t1)
                    .modifyBankStatusForChannelUserBasedOnBankName(ent_03, "Suspended", defaultBank.BankName);

            //Approving Channel User
            ChannelUserManagement.init(t1).approveChannelUserModification(ent_03);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1)
                    .rejectAssociatedBanksForUser(ent_03, DataFactory.getDefaultProvider().ProviderName, defaultBank.BankName, bankCustId);

            String status = MobiquityGUIQueries.getBankAccountStatus(bankCustId);
            DBAssertion.verifyDBAssertionEqual(status, "Y", "Verify successfully rejected Bank Modification, Bank account is not suspended", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON_4469_6",
                "Verify channeladmin should be able to suspend bank for existing enterprise.");

        try {

            //Modifying Status of Bank
            Login.init(t2).login(modifyuser);
            String bankCustId = BankAccountAssociation.init(t1)
                    .modifyBankStatusForChannelUserBasedOnBankName(ent_03, "Suspended", defaultBank.BankName);

            //Approving Channel User
            ChannelUserManagement.init(t2).approveChannelUserModification(ent_03);

            Login.init(t2).login(bankApprover);
            CommonUserManagement.init(t2)
                    .approveAssociatedBanksForUser(ent_03, DataFactory.getDefaultProvider().ProviderName, defaultBank.BankName);

            String status = MobiquityGUIQueries.getBankAccountStatus(bankCustId);
            DBAssertion.verifyDBAssertionEqual(status, "S", "Bank account is suspended for Enterprise User", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(dependsOnMethods = "MON_4469_4", groups = {FunctionalTag.v5_0})
    public void verifyAccountNumberNotUniqueMessageIsComingIfSameAccountNumberIsGivenForTwoDifferentUsers() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4469_10",
                "Verify same account number cant be used for two different users");
        try {
            String accnum = ent_02.getDefaultAccNum();

            //Create ChannelUser With Same Account Number
            User entUser = new User(Constants.ENTERPRISE);

            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(entUser)
                    .assignHierarchy(entUser);


            CommonUserManagement.init(t1)
                    .assignWebGroupRole(entUser);

            ChannelUserManagement.init(t1)
                    .setEnterprisePreference()
                    .setEnterpriseCategoryWalletCombination();

            CommonUserManagement.init(t1)
                    .mapDefaultWalletPreferences(entUser);

            AddEnterprise_Pg3 page = AddEnterprise_Pg3.init(t1);
            AddChannelUser_pg5 page1 = AddChannelUser_pg5.init(t1);

            page.setEnterpriseLimit2(entUser.EnterpriseLimit);
            page.clickOnBulkRegistrationIsRequired();
            page.selectBulkPayerType();

            page.setCompCode(DataFactory.getRandomNumberAsString(5));
            page.clickNext();

            String provider = DataFactory.getDefaultProvider().ProviderName;

            // get instrument TCP
            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(entUser.DomainName, entUser.CategoryName, entUser.GradeName, provider, defaultBank.BankName);

            // UI elements
            WebElement providerSelect = driver.findElement(By.name("bankCounterList[0].providerSelected"));
            Select sel = new Select(providerSelect);
            sel.selectByVisibleText(provider);


            WebElement bankSelect = driver.findElement(By.name("bankCounterList[0].paymentTypeSelected"));
            Select sel1 = new Select(bankSelect);
            sel1.selectByVisibleText(defaultBank.BankName);


            Thread.sleep(2000);
            WebElement gradeSelect = driver.findElement(By.name("bankCounterList[0].channelGradeSelected"));
            Select sel2 = new Select(gradeSelect);
            sel2.selectByVisibleText(entUser.GradeName);

            WebElement tcpSelect = driver.findElement(By.name("bankCounterList[0].tcpSelected"));
            Select sel3 = new Select(tcpSelect);
            sel3.selectByVisibleText(insTcp.ProfileName);

            /*
                Set The Customer ID, set with the default Bank
                 */
            WebElement customerID = driver.findElement(By.name("bankCounterList[0].customerId"));
            String custId = DataFactory.getRandomNumberAsString(9);
            customerID.sendKeys(custId);

            /*
              Set The account number, set with the default Bank
             */
            WebElement accountNum = driver.findElement(By.name("bankCounterList[0].accountNumber"));

            // Set the account number of existing Enterprise User
            accountNum.sendKeys(accnum);


            // Select Primary account Types
            WebElement selPrimaryAccount = driver.findElement(By.name("bankCounterList[0].primaryAccountSelected"));
            Select sel4 = new Select(selPrimaryAccount);
            sel4.selectByVisibleText("Yes");

            page1.clickNext(entUser.CategoryCode);
            Assertion.verifyErrorMessageContain("user.error.accountNumber.not.unique", "Account number is not same", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


}
