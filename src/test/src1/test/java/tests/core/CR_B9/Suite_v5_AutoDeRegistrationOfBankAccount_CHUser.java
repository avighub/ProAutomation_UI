package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.TxnResponse;
import framework.entity.Bank;
import framework.entity.User;
import framework.features.apiManagement.OldTxnOperations;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.TransactionType;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static framework.util.JsonPathOperation.delete;
import static framework.util.JsonPathOperation.set;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_AutoDeRegistrationOfBankAccount_SUBUser
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 13/04/2018
 */
public class Suite_v5_AutoDeRegistrationOfBankAccount_CHUser extends TestInit {
    private User whsUser, ssaUser;
    private CurrencyProvider defaultProvider;
    private Bank defaultBank, nonTrustBank;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            defaultProvider = DataFactory.getDefaultProvider();
            defaultBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            whsUser = new User(Constants.WHOLESALER);
            ssaUser = new User(Constants.SPECIAL_SUPER_AGENT);

            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(whsUser);
            CurrencyProviderMapping.init(eSetup).linkPrimaryBankWalletPreference(ssaUser);

            ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(ssaUser, false);
            ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(whsUser, false);


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void deRegisterBankForChannelUserThroughAPI() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5799_01",
                "verify channel user/customer able to de register bank account with " +
                        "BKDREG api with new enhancements.");
        String primaryAccountNum = DataFactory.getRandomNumberAsString(8);
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            // add primary Bank
            Transactions.init(t1)
                    .bankRegistration(whsUser, defaultBank, Constants.CHANNEL_REIMB, primaryAccountNum, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            // add 2nd bank which could be deRegistration
            String accNumber = DataFactory.getRandomNumberAsString(9);
            Transactions.init(t1)
                    .bankRegistration(whsUser, nonTrustBank, Constants.CHANNEL_REIMB, accNumber, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            Transactions.init(t1)
                    .bankDeAssociation(whsUser, nonTrustBank, accNumber);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON_5799_02",
                "verify de register the primary Bank associated with the Channel USer");
        try {
            Transactions.init(t2)
                    .bankDeAssociation(whsUser, defaultBank, primaryAccountNum);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "deRegisterBankForChannelUserThroughAPI")
    public void deRegisterBankForChannelUserWhenAUTO_BANK_ACC_LINK_MATCH_IDIsN() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5799_03",
                "verify Id number is optional when AUTO_BANK_ACC_LINK_MATCH_ID is N ");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
            SystemPreferenceManagement.init(t1).updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "N");

            String accNumber = DataFactory.getRandomNumberAsString(9);
            Transactions.init(t1)
                    .bankRegistration(whsUser, nonTrustBank, Constants.CHANNEL_REIMB, accNumber, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_DEREGISTRATION,
                    set("COMMAND.MSISDN", whsUser.MSISDN),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", whsUser.FirstName),
                    set("COMMAND.LNAME", whsUser.LastName),
                    delete("COMMAND.IDNO"),
                    set("COMMAND.BANKACCNUMBER", accNumber)

            );
            t1.info(operation.bodyString);

            TxnResponse res = operation.postOldTxnURLForHSB(t1);
            Assertion.verifyEqual(res.isTxnSuccessful(Constants.TXN_SUCCESS), true,
                    "Verify Successfully Deregister Bank when AUTO_BANK_ACC_LINK_MATCH_ID = 'N'", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "Y");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "deRegisterBankForChannelUserWhenAUTO_BANK_ACC_LINK_MATCH_IDIsN")
    public void errorMessageWhileDeRegisteringBankForChannelUserWhenAUTO_BANK_ACC_LINK_MATCH_IDIsYWithoutIDNO() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5799_04",
                "verify Id number is mandatory when AUTO_BANK_ACC_LINK_MATCH_ID is Y ");
        String accNumber = DataFactory.getRandomNumberAsString(9);
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("AUTO_BANK_ACC_LINK_MATCH_ID", "Y");
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            Transactions.init(t1)
                    .bankRegistration(whsUser, nonTrustBank, Constants.CHANNEL_REIMB, accNumber, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_DEREGISTRATION,
                    set("COMMAND.MSISDN", whsUser.MSISDN),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", whsUser.FirstName),
                    set("COMMAND.LNAME", whsUser.LastName),
                    delete("COMMAND.IDNO"),
                    set("COMMAND.BANKACCNUMBER", accNumber)
            );
            t1.info(operation.bodyString);

            TxnResponse res = operation.postOldTxnURLForHSB(t1);
            Assertion.verifyMessageContain(res.Message, "channeluser.validation.externalCode.empty",
                    "verify Id number is mandatory when AUTO_BANK_ACC_LINK_MATCH_ID is Y", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("MON_5799_05",
                "verify UserRole tag should be optional when trying Bank De registration for channel user");
        try {

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_DEREGISTRATION,
                    set("COMMAND.MSISDN", whsUser.MSISDN),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", whsUser.FirstName),
                    set("COMMAND.LNAME", whsUser.LastName),
                    set("COMMAND.IDNO", whsUser.ExternalCode),
                    set("COMMAND.BANKACCNUMBER", accNumber),
                    delete("COMMAND.USERROLE")
            );

            t2.info(operation.bodyString);
            TxnResponse res = operation.postOldTxnURLForHSB(t2);
            Assertion.verifyEqual(res.isTxnSuccessful(Constants.TXN_SUCCESS), true,
                    "Verify Successfully Deregister Bank when and verified that USERROLE field is optional", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.v5_0})
    public void deRegisterBankForChannelUserWhenBANK_ACC_LINKING_VIA_MSISDNIsY() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("MON_5799_06",
                "verify channel user able to de register bank account of " +
                        "bank account number is same as MSISDN when " +
                        "BANK_ACC_LINKING_VIA_MSISDN is Y,without providing BANK account number.");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
            Transactions.init(t1)
                    .bankRegistration(ssaUser, nonTrustBank, Constants.CHANNEL_REIMB, ssaUser.MSISDN, ssaUser.MSISDN)
                    .verifyStatus(Constants.TXN_SUCCESS);

            OldTxnOperations operation1 = new OldTxnOperations(TransactionType.BANK_DEREGISTRATION,
                    set("COMMAND.MSISDN", ssaUser.MSISDN),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", ssaUser.FirstName),
                    set("COMMAND.LNAME", ssaUser.LastName),
                    set("COMMAND.IDNO", ssaUser.ExternalCode),
                    delete("COMMAND.BANKACCNUMBER")

            );
            t1.info(operation1.bodyString);
            TxnResponse res = operation1.postOldTxnURLForHSB(t1);
            Assertion.verifyEqual(res.isTxnSuccessful(Constants.TXN_SUCCESS), true,
                    "Verify Successfully Deregister Bank when BANK_ACC_LINKING_VIA_MSISDN = 'Y'", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0}, dependsOnMethods = "deRegisterBankForChannelUserWhenBANK_ACC_LINKING_VIA_MSISDNIsY")
    public void deRegisterBankForChannelUserWhenBANK_ACC_LINKING_VIA_MSISDNIsN() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_5799_07",
                "verify system should throw proper error message when channel user able to " +
                        "de register bank account with out providing BANK account number," +
                        "if BANK_ACC_LINKING_VIA_MSISDN preference is N.");
        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");

            String accNumber = DataFactory.getRandomNumberAsString(9);
            Transactions.init(t1)
                    .bankRegistration(ssaUser, nonTrustBank, Constants.CHANNEL_REIMB, accNumber, null)
                    .verifyStatus(Constants.TXN_SUCCESS);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_DEREGISTRATION,
                    set("COMMAND.MSISDN", ssaUser.MSISDN),
                    delete("COMMAND.BANKACCNUMBER"),
                    set("COMMAND.PROVIDER", nonTrustBank.ProviderId),
                    set("COMMAND.BANKID", nonTrustBank.BankID),
                    set("COMMAND.BANKNAME", nonTrustBank.BankName),
                    set("COMMAND.FNAME", ssaUser.FirstName),
                    set("COMMAND.LNAME", ssaUser.LastName),
                    set("COMMAND.IDNO", ssaUser.ExternalCode)
            );
            t1.info(operation.bodyString);
            TxnResponse res = operation.postOldTxnURLForHSB(t1);
            Assertion.verifyMessageContain(res.Message, "message.valid.accountno.error.while.encryption",
                    "Verify that Customer ID/ Bank Account num is mandatory when associating Bank to Customer", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
