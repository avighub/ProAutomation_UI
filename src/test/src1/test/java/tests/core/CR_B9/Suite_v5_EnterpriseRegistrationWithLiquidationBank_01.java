package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Liquidation;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.AddEnterprise_Pg3;
import framework.pageObjects.userManagement.LiquidationDetailsApprovalPage;
import framework.pageObjects.userManagement.LiquidationDetailsConfirmPage;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Enterprise Registration With Liquidation Details
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 21/03/2018
 */

public class Suite_v5_EnterpriseRegistrationWithLiquidationBank_01 extends TestInit {

    private OperatorUser usrCreator, bankApprover, usrApprover;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        try {
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void channelAdminRejectsBankOnApproveBankPage() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_4780_8", " Verify Channel Admin able to reject the liquidation Bank account");


        try {
            CurrencyProvider defautProvider = GlobalData.defaultProvider;
            //Create ChannelUser With Liquidation Bank
            User entUser = new User(Constants.ENTERPRISE);

            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1)
                    .addChannelUser(entUser); // create Enterprise with Bank Details

            Login.init(t1).login(usrApprover);
            CommonUserManagement.init(t1)
                    .addInitiatedApproval(entUser, true);

            ExtentTest t2 = pNode.createNode("MON_4780_1",
                    "Verify Operator user should able to register enterprise " +
                            "along with Non-partner(liquidation) banking details. ");

            Assertion.verifyEqual(entUser.isApproved, "Y",
                    "Verify that Enterprise user is created Successfully with Liquidation Bank Details", t2);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1)
                    .rejectAssociatedBanksForUser(entUser,
                            GlobalData.defaultProvider.ProviderName,
                            DataFactory.getDefaultLiquidationBankName(GlobalData.defaultProvider.ProviderId));

            String status = MobiquityGUIQueries.getStatusOfLiquidationBank(entUser.MSISDN);
            String status1 = MobiquityGUIQueries.getBankAccountStatusWithLoginId(entUser.LoginId);
            DBAssertion.verifyDBAssertionEqual(status, "N", "Verify Bank Status", t1);
            DBAssertion.verifyDBAssertionEqual(status1, "N", "Verify Bank Status", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0}, enabled = false)
    public void liquidationDetailsInConfirmAndApprovePage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4780_9/10", " Verify Liquidation bank details should be populated in Confirm and Approve page. ");

        try {
            //Create ChannelUser With Liquidation Bank
            User channeluser = new User(Constants.ENTERPRISE);

            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1).
                    initiateChannelUser(channeluser).assignHierarchy(channeluser);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser);
            ChannelUserManagement.init(t1).setEnterprisePreference().setEnterpriseCategoryWalletCombination();

            CommonUserManagement.init(t1)
                    .mapDefaultWalletPreferences(channeluser);
            startNegativeTestWithoutConfirm();
            ChannelUserManagement.init(t1).setEnterprisePreference(channeluser, true);
            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);

            String liquidationBankAccountNumber = channeluser.getLiquidationBankAccountNumber();
            String liquidationBankBranchName = channeluser.getLiquidationBankBranchName();
            String accountHolderName = channeluser.getAccountHolderName();
            String frequencyTradingName = channeluser.getFrequencyTradingName();

            //Verify Details In Confirm Page

            LiquidationDetailsConfirmPage liqCon = new LiquidationDetailsConfirmPage(t1);
            liqCon.verifyHeadersDetails(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId),
                    liquidationBankAccountNumber,
                    liquidationBankBranchName,
                    accountHolderName,
                    frequencyTradingName);
            t1.pass("Fields Are Validated In Confirm Page");
            page5.completeUserCreation(channeluser, false);

            //Verify Details In Approval Page

            Login.init(t1).login(usrCreator);
            startNegativeTestWithoutConfirm();
            CommonUserManagement.init(t1).addInitiatedApproval(channeluser, true);
            LiquidationDetailsApprovalPage liqApp = new LiquidationDetailsApprovalPage(t1);
            liqApp.verifyHeadersDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), liquidationBankAccountNumber, liquidationBankBranchName, accountHolderName, frequencyTradingName);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    // Disabled - this scenario will be automatically tested whenever multiple Providers are configured in Config input and an enterprise user is created via Base Setup
    @Test(priority = 4, groups = {FunctionalTag.v5_0}, enabled = false)
    public void channelUserWithLiquidationDetailsWithSameMFSProvider() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4780_6", "Verify each user should be able to set up one liquidation bank details for each MFS Provider");
        try {

            //Create ChannelUser With Liquidation Bank
            AddChannelUser_pg5 page1 = new AddChannelUser_pg5(t1);
            User channeluser = new User(Constants.ENTERPRISE);
            channeluser.setProviderName(DataFactory.getDefaultProvider().ProviderName);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(channeluser).assignHierarchy(channeluser);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser);

            ChannelUserManagement.init(t1).
                    setEnterprisePreference().setEnterpriseCategoryWalletCombination();

            CommonUserManagement.init(t1).mapDefaultWalletPreferences(channeluser);
            AddEnterprise_Pg3 page = AddEnterprise_Pg3.init(t1);

            page.setEnterpriseLimit2(channeluser.EnterpriseLimit);
            page.clickOnBulkRegistrationIsRequired();
            page.selectBulkPayerType();

            page.setCompCode(DataFactory.getRandomNumberAsString(5)); // company code should be
            page.clickNext();
            for (int i = 0; i < 2; i++) {

                page1.liquidationclick();

                WebElement liqaccountNum = driver.findElement(By.name("liquidationBankAccountNo1[" + i + "]"));
                WebElement liqbranchnam = driver.findElement(By.name("liquidationBranchName1[" + i + "]"));
                WebElement liqaccountholdernam = driver.findElement(By.name("liquidationAccHolderName1[" + i + "]"));
                WebElement freqtradingnam = driver.findElement(By.name("liquidationTradingName1[" + i + "]"));

                // UI elements

                new Select(driver.findElement(By.id("liquidationProvider" + i + ""))).selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
                new Select(driver.findElement(By.id("liquidationBankListId" + i + ""))).selectByVisibleText(DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));
                Liquidation liq = new Liquidation();
                liqaccountNum.sendKeys(liq.LiquidationBankAccountNumber);
                liqbranchnam.sendKeys(liq.LiquidationBankBranchName);
                liqaccountholdernam.sendKeys(liq.AccountHolderName);
                freqtradingnam.sendKeys(liq.FrequencyTradingName);

            }
            page1.selectLiquidationFrequency(channeluser.LiquidationFrequency, channeluser.LiquidationDay);
            page1.clickNext(channeluser.CategoryCode);
            AlertHandle.acceptAlert(t1);
            AlertHandle.acceptAlert(t1);

            Assertion.verifyErrorMessageContain("LiquidationBank.one.permfsprovider", "Only one Bank per mfs provider is allowed", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}



