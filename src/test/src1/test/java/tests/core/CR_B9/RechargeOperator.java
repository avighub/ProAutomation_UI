package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.operatorManagement.OperatorManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.operatorManagement.OperatorManagementPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;
import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : API to fetch list of currencies associated with user
 * Author Name      : Nirupama MK
 * Created Date     : 16/07/2018
 */
public class RechargeOperator extends TestInit {
    private String defaultValue;
    private OperatorUser usrAddOperator;

    @Test(priority = 1, groups = {FunctionalTag.v5_0,
            FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void MON_6278() throws Exception {

        /*
         MON_6278
         create Recharge Operator
         Verify successful message
         verify created "operatorID" is copied to "InterfaceID" in the table "mbk_operators"

         MON_6279
         Login as nwAdmin
         verify that InterfaceID can be modified in UI
         verify InterfaceID is updated in the table "mbk_operators"

         MON_6434
         Login as nwAdmin
         change the preference "MULTI_OPERATOR_SUPPORT" to 'N'
         Verify error message while create multiple Recharge operators
         */

        ExtentTest MON_6278 = pNode.createNode("MON-6278", "Verify that when a recharge operator " +
                "is created operator ID is added to the Interface ID field in DB");

        try {
            String operatorName = "OPAIRTEL" + DataFactory.getRandomNumber(4);
            usrAddOperator = DataFactory.getOperatorUserWithAccess("OPPADD");

            if (AppConfig.isMultipleRechargeOperatorRequired) {

                List<String> status = MobiquityGUIQueries.fetchStatus();

                if (!status.contains("Y")) {

                    OperatorManagement.init(MON_6278)
                            .addRechargeOperator(operatorName, DataFactory.getDefaultProvider().ProviderId);

                    Map<String, String> operatorDetails = MobiquityGUIQueries.fetchRechargeOperatorDetails(operatorName);

                    String interfaceId = operatorDetails.get("interfaceId");
                    String operatorId = operatorDetails.get("OperatorId");
                    Assertion.assertEqual(interfaceId, operatorId, "OperatorId added to interfaceID", pNode);


                    ExtentTest MON_6279 = pNode.createNode("MON-6279", "Verify that interface ID " +
                            "field is enabled during edit");

                    OperatorManagement.init(MON_6279).modifyOperator(operatorName);

                    // deleting is done tho its not part of the scenario,
                    //OperatorManagement.init(MON_6279).deleteOperator(operatorName);

                } else {

                    Assertion.logAsPass("Multiple Recharge Operator cannot be created when " +
                            "Preference 'MULTI_OPERATOR_SUPPORT' is set to 'N' ", MON_6278);
                }

                ExtentTest MON_6434 = pNode.createNode("MON-6434", "Verify the error message, while creating" +
                        " multiple Recharge operators, when system preference 'MULTI_OPERATOR_SUPPORT' is set to 'N'").assignCategory(FunctionalTag.ECONET_UAT_5_0);

                operatorName = "OPAIRTEL" + DataFactory.getRandomNumber(1);
                Login.init(MON_6434).login(usrAddOperator);

                /*OperatorManagement.init(MON_6434)
                        .addRechargeOperator(operatorName, DataFactory.getDefaultProvider().ProviderId);*/

                OperatorManagementPage page1 = OperatorManagementPage.init(MON_6434);
                page1.navigateToAddOperatorLink();

                page1.setOperatorName(operatorName);
                page1.clickAdd();

                Assertion.verifyErrorMessageContain("operator.validation.MultiOperatorSupport",
                        "Only Single Operator support is allowed", MON_6434);

            }
        } catch (Exception e) {

            markTestAsFailure(e, MON_6278);
        }
        Assertion.finalizeSoftAsserts();

    }
    /*
         MON_6435
         Login as nwAdmin
         change the preference "MULTI_OPERATOR_SUPPORT" to 'Y'
         should be able to create multiple Recharge operators
         Verify successful message
         verify created "operatorID" is copied to "InterfaceID" in the table "mbk_operators"

         MON_6438
         Verify the error message while creating the recharge operator with the same name which already exist

         MON_6436
         Login as nwAdmin, change the preference "MULTI_OPERATOR_SUPPORT" to 'Y'
         verify that InterfaceID can be modified in UI
         verify InterfaceID is updated in the table "mbk_operators"

         MON_6439
         Login as nwAdmin, delete the recharge operator
         verify success message
         verify Interface_ID remain unchanged even after deletion

    */

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.RECHARGE_SYSTEM_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void MON_6435() throws Exception {

        ExtentTest MON_6435 = pNode.createNode("MON-6435", "Verify that multiple recharge operators " +
                "can be created when preference 'MULTI_OPERATOR_SUPPORT' is set to'Y', and operator_ID is " +
                "added to the Interface_ID in the table 'mbk_operators' ").assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {

            defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MULTI_OPERATOR_SUPPORT");

            SystemPreferenceManagement.init(MON_6435)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", "Y");

            String operatorName = "OPAIRTEL" + DataFactory.getRandomNumber(3);

            OperatorManagement.init(MON_6435).addRechargeOperator(operatorName,
                    DataFactory.getDefaultProvider().ProviderId);

            Map<String, String> operatorDetails = MobiquityGUIQueries.fetchRechargeOperatorDetails(operatorName);
            String interfaceId = operatorDetails.get("interfaceId");

            String operatorId = operatorDetails.get("OperatorId");
            Assertion.assertEqual(interfaceId, operatorId, "OperatorId added to interfaceID", MON_6435);


            ExtentTest MON_6438 = pNode.createNode("MON-6438", "Verify the error message " +
                    "while creating the recharge operator with the same name which already exist (when preference set to 'Y')");

            OperatorManagementPage page1 = OperatorManagementPage.init(MON_6438);
            page1.navigateToAddOperatorLink();

            page1.setOperatorName(operatorName);
            page1.clickAdd();

            Assertion.verifyErrorMessageContain("operator.error.checkAddedOperator",
                    "The Operator you are trying to add is already present", MON_6438);


            ExtentTest MON_6436 = pNode.createNode("MON-6436", " Able to modify interfaceId of " +
                    "a recharge operator when preference 'MULTI_OPERATOR_SUPPORT' is set to'Y', also " +
                    "verify 'interface_Id' is modified in the table 'mbk_operators' ");

            String modifiedInterfacId = OperatorManagement.init(MON_6436).modifyOperator(operatorName);


            ExtentTest MON_6439 = pNode.createNode(" MON-6439", " verify successful deletion " +
                    "of recharge operator and 'interface_Id' should remain same(not null) " +
                    "in the table 'mbk_operators' ");

            OperatorManagement.init(MON_6439).deleteOperator(operatorName);

            operatorDetails = MobiquityGUIQueries.fetchRechargeOperatorDetails(operatorName);
            interfaceId = operatorDetails.get("interfaceId");

            Assertion.assertEqual(modifiedInterfacId, interfaceId,
                    "interface_ID remains un-changed even after deletion", MON_6439);

        } catch (Exception e) {

            markTestAsFailure(e, MON_6435);

        } finally {
            //OperatorManagement.init(MON_6435).deleteAllRechargeOperator();

            SystemPreferenceManagement.init(MON_6435)
                    .updateSystemPreference("MULTI_OPERATOR_SUPPORT", defaultValue);
        }
    }

}
