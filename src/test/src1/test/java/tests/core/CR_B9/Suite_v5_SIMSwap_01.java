package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : SIM Swap / Replacement
 * Author Name      : Nirupama MK
 * Created Date     : 12/06/2018
 */
public class Suite_v5_SIMSwap_01 extends TestInit {

    private String defPrefValue_status, defPrefValue_duration;
    private OperatorUser nwAdmin;
    private User subscriber;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Sim Swap");
        try {
            nwAdmin = DataFactory.getOperatorUserWithAccess(Roles.RESET_PIN);

            defPrefValue_duration = MobiquityGUIQueries
                    .fetchDefaultValueOfPreference("DISABLE_PIN_RESET_SIM_SWAP_DURATION");

            SystemPreferenceManagement.init(eSetup)
                    .updateSystemPreference("DISABLE_PIN_RESET_SIM_SWAP_DURATION",
                            Constants.PREFERENCE_SIM_SWAP_DURATION_1h);
            subscriber = new User(Constants.SUBSCRIBER);

            CurrencyProviderMapping.init(eSetup)
                    .mapPrimaryWalletPreference(subscriber);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0})
    public void MON_4832() throws Exception {
        /*
            MON_5977
            Create a Subscriber
            fetch the value of SIM_SWAPPED and SIM_SWAPPED_ON for the associated Subscriber
            Verify that the initial value is NULL (Before Swapping the SIM)

            MON_5978
            Perform SIM Swap tru API
            Verify SIM Swap is Successfull
            Verify that the value of SIM_SWAPPED is "Y" and SIM_SWAPPED_ON is "TimeStamp" for the associated subscriber

            MON_5979 = TC_ECONET_0616_a
            Login as NwAdin
            Navigate to Enquiries > Reset PIN
            Verify the Error Msg when trying to change the Mpin before the Time duration

            MON_5980 = TC_ECONET_0616_b
            Login as NwAdin
            Navigate to Enquiries > Reset PIN
            Verify the Error Msg when trying to change the Tpim before the Time duration

        */

        ExtentTest MON_5977 = pNode.createNode("MON-5977", "verify that the 'SIM_Swapped' and " +
                "'SIM_Swapped_on' stores null value before the sim swap ");

        MON_5977.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0,
                FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0);

        try {

            SubscriberManagement.init(MON_5977).createDefaultSubscriberUsingAPI(subscriber);

            Map<String, String> simStatus = MobiquityGUIQueries.fetchSIMSwappedStatus(subscriber, subscriber.MSISDN);

            String Sim_Swapped = simStatus.get("SIM_Swapped");
            String Sim_Swapped_On = simStatus.get("SIM_SwappedOn");

            if ((Sim_Swapped == null) && (Sim_Swapped_On == null)) {

                MON_5977.info("'Sim_Swapped' and 'Sim_Swapped_ON' holds Null value initially");
                MON_5977.pass("SUCCESS");

            } else {
                MON_5977.fail("'Sim_Swapped' and 'Sim_Swapped_ON' is not Null initially");
            }


            ExtentTest MON_5978 = pNode.createNode("MON-5978", "verify that the sim swap is success, " +
                    " 'SIM_Swapped' stores value 'Y' and 'SIM_Swapped_on' stores 'timestamp' (after the sim swap) ");

            //to swap the sim
            Transactions.init(MON_5978)
                    .SIMSwap(subscriber.MSISDN)
                    .verifyStatus("200");

            String curDate = new DateAndTime().getCurrentDate();

            //to fetch "sim_swapped" and "sim_swapped_on" values
            simStatus = MobiquityGUIQueries.fetchSIMSwappedStatus(subscriber, subscriber.MSISDN);

            Sim_Swapped = simStatus.get("SIM_Swapped");
            String[] sim_Swapped_On = simStatus.get("SIM_SwappedOn").split(" ");

            if ((Sim_Swapped.equalsIgnoreCase("Y")) && (curDate.equals(sim_Swapped_On[0]))) {

                MON_5978.info("After SIM swap the Values for 'Sim_Swapped' is " + Sim_Swapped + "\n" +
                        "and 'Sim_Swapped_ON' is " + sim_Swapped_On[0]);

                MON_5978.pass("SUCCESS");

            } else {
                MON_5978.fail("'Sim_Swapped' and 'Sim_Swapped_ON' is not updated");
            }

            ExtentTest MON_5979 = pNode.createNode("MON-5979", " verify the Error Msg when trying" +
                    " to change the Mpin before the time duration specified (Enquiries > ResetPin) ");

            // fetch the "simSwaped_on" time and increase time by "1hour"
            String myTime = sim_Swapped_On[1];
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date d = df.parse(myTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.HOUR, 1);
            String newTime = df.format(cal.getTime());

            // to convert the "date" format
            String dateReceivedFromUser = sim_Swapped_On[0];
            DateFormat userDateFormat = new SimpleDateFormat("yyyy-mm-dd");
            DateFormat dateFormatNeeded = new SimpleDateFormat("dd-mm-yyyy");
            Date date = userDateFormat.parse(dateReceivedFromUser);
            String convertedDate = dateFormatNeeded.format(date);

            Login.init(MON_5979).login(nwAdmin);

            Enquiries.init(MON_5979)
                    .startNegativeTest()
                    .negativeTestConfirmation()
                    .resetPin(subscriber, Constants.SUBS_USERS_CONST, Constants.RESET_MPIN_CONST);

            Assertion.verifyErrorMessageContain("ENQ_ResetMPin",
                    "Verify Error the Message when Reset MPIN is not happening",
                    MON_5979, convertedDate, newTime);


            ExtentTest MON_5980 = pNode.createNode("MON-5980", " verify the Error Msg when trying" +
                    " to change the Tpin before the time duration specified (Enquiries > ResetPin) ");


            Enquiries.init(MON_5980)
                    .startNegativeTest()
                    .negativeTestConfirmation()
                    .resetPin(subscriber, Constants.SUBS_USERS_CONST, Constants.RESET_TPIN_CONST);

            Assertion.verifyErrorMessageContain("ENQ_ResetTPin",
                    "Verify Error the Message when Reset TPIN is not happening",
                    MON_5980, convertedDate, newTime);

            /*

            MON_6157
            perform sim swap
            Login as NwAdin
            Set the System preference "DISABLE_PIN_RESET_ON_SIM_SWAP" to "N"
            Navigate to Enquiries > Reset PIN
            Enables to Reset the Mpin for associated user

            MON_6269
            perform sim swap
            Login as NwAdin
            Set the System preference "DISABLE_PIN_RESET_ON_SIM_SWAP" to "N"
            Navigate to Enquiries > Reset PIN
            Enables to Reset the Tpin for associated user
            */

            ExtentTest MON_6157 = pNode.createNode("MON-6157", "  Enables to re-set the Mpin when" +
                    " System preference 'DISABLE_PIN_RESET_ON_SIM_SWAP' flag is set as 'N");

            defPrefValue_status = MobiquityGUIQueries
                    .fetchDefaultValueOfPreference("DISABLE_PIN_RESET_ON_SIM_SWAP");

            SystemPreferenceManagement.init(MON_6157)
                    .updateSystemPreference("DISABLE_PIN_RESET_ON_SIM_SWAP", "N");


            stopNegativeTestWithoutConfirm();

            Enquiries.init(MON_6157)
                    .resetPin(subscriber, Constants.SUBS_USERS_CONST, Constants.RESET_MPIN_CONST);

            ExtentTest MON_6269 = pNode.createNode("MON-6269", "  Enables to re-set the Mpin when" +
                    " System preference 'DISABLE_PIN_RESET_ON_SIM_SWAP' flag is set as N'");

            Enquiries.init(MON_6269)
                    .resetPin(subscriber, Constants.SUBS_USERS_CONST, Constants.RESET_TPIN_CONST);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5977);

        } finally {
            SystemPreferenceManagement.init(MON_5977)
                    .updateSystemPreference("DISABLE_PIN_RESET_ON_SIM_SWAP", defPrefValue_status);

            SystemPreferenceManagement.init(MON_5977)
                    .updateSystemPreference("DISABLE_PIN_RESET_SIM_SWAP_DURATION", defPrefValue_duration);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_6155() throws Exception {
        /*
            MON_6155
            Create a Channel User without changing PIN
            perform sim swap
            Verify that the Self pin reset(Change PIN) is successfull

            MON_6156
            Create a Subscriber without changing PIN
            perform sim swap
            Verify that the Self pin reset(Change PIN) is successfull
        */

        ExtentTest MON_6155 = pNode.createNode("MON-6155", " Self pin reset will be allowed " +
                "even if Disable PIN Reset on Sim swap is 'Y' for Channel user");

        try {

            User chUser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(MON_6155).initiateAndApproveChUser(chUser, false);

            Transactions.init(MON_6155)
                    .SIMSwap(chUser.MSISDN)
                    .verifyStatus("200");

            Transactions.init(MON_6155).changeChannelUserMPinTPin(chUser);

            ExtentTest MON_6156 = pNode.createNode("MON-6156", " Self pin reset will be " +
                    "allowed even if Disable PIN Reset on Sim swap is 'Y' for Subscriber");

            User subscriber = new User(Constants.SUBSCRIBER);
            Transactions.init(MON_6156).selfRegistrationForSubscriberWithKinDetails(subscriber);

            Transactions.init(MON_6156)
                    .SIMSwap(subscriber.MSISDN)
                    .verifyStatus("200");

            Transactions.init(MON_6156).changeCustomerMpinTpin(subscriber);

        } catch (Exception e) {
            markTestAsFailure(e, MON_6155);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void MON_5983() throws Exception {
        /*
            MON-5983
            login as NwAdmin
            change the preference "DISABLE_PIN_RESET_SIM_SWAP_DURATION" to seconds.
            Create a Subscriber
            perform sim swap for subscriber
            Login as NwAdmin
            navigate to Enquiries > Reset Pin
            able to reset Mpin after the time duration specified in the preference "DISABLE_PIN_RESET_SIM_SWAP_DURATION"


            MON-5984
            login as NwAdmin
            change the preference "DISABLE_PIN_RESET_SIM_SWAP_DURATION" to seconds.
            Create a Subscriber
            perform sim swap for subscriber
            Login as NwAdmin
            navigate to Enquiries > Reset Pin
            able to reset Tpin after the time duration specified in the preference "DISABLE_PIN_RESET_SIM_SWAP_DURATION"
        */

        ExtentTest MON_5983 = pNode.createNode("MON-5983", " Successful in changing Mpin " +
                "after the time duration specified");

        try {

            defPrefValue_duration = MobiquityGUIQueries
                    .fetchDefaultValueOfPreference("DISABLE_PIN_RESET_SIM_SWAP_DURATION");

            SystemPreferenceManagement.init(MON_5983)
                    .updateSystemPreference("DISABLE_PIN_RESET_SIM_SWAP_DURATION",
                            Constants.PREFERENCE_SIM_SWAP_DURATION_1S);

            User sub1 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(MON_5983).createDefaultSubscriberUsingAPI(sub1);

            Transactions.init(MON_5983)
                    .SIMSwap(sub1.MSISDN)
                    .verifyStatus("200");

            Login.init(MON_5983).login(nwAdmin);
            Enquiries.init(MON_5983)
                    .resetPin(sub1, Constants.SUBS_USERS_CONST, Constants.RESET_MPIN_CONST);


            ExtentTest MON_5984 = pNode.createNode("MON-5983", " Successful in changing Tpin " +
                    "after the time duration specified");

            Enquiries.init(MON_5984)
                    .resetPin(sub1, Constants.SUBS_USERS_CONST, Constants.RESET_TPIN_CONST);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5983);

        } finally {
            SystemPreferenceManagement.init(MON_5983)
                    .updateSystemPreference("DISABLE_PIN_RESET_SIM_SWAP_DURATION", defPrefValue_duration);
        }
    }

}