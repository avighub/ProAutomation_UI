package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.util.common.Assertion;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_v5_SubscriberSelfRegistrationApiEnhancements extends TestInit {
    /*
    MON_6226
    create subscriber through self registration
    provide future date as expiry date
    validate success message

    MON_6229
    create subscriber through channelUser
    provide future date as expiry date
    validate success message
 */

    private TxnResponse response;
    private String id_type;
    private String expiryDate = new DateAndTime().getDate(2);

    @Test
    public void run1() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-6226", "Successful Subscriber " +
                "self registration when new parameters are added");
        User sub = new User(Constants.SUBSCRIBER);
        response = Transactions.init(t1)
                .SubscriberRegistrationByChannelWithAdditionalParameters(sub, id_type)
                .verifyStatus(Constants.TXN_SUCCESS);
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void MON_6226() throws Exception {

        ExtentTest MON_6226 = pNode.createNode("MON-6226", "Successful Subscriber " +
                "self registration when new parameters are added");
        try {

            id_type = MobiquityGUIQueries.getProofType();

            User sub1 = new User(Constants.SUBSCRIBER);
            sub1.setExpiryDate(expiryDate);

            response = Transactions.init(MON_6226)
                    .selfRegistrationForSubscriberWithAdditionalFields(sub1, id_type)
                    .verifyStatus(Constants.TXN_SUCCESS);


            ExtentTest MON_6229 = pNode.createNode("MON-6229", "Successful Subscriber Registration " +
                    "through channel user when new parameters are added in the Existing XML(ID type and Expiry Date)");

            User sub2 = new User(Constants.SUBSCRIBER);
            sub2.setExpiryDate(expiryDate);

            response = Transactions.init(MON_6229)
                    .SubscriberRegistrationByChannelWithAdditionalParameters(sub2, id_type)
                    .verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            markTestAsFailure(e, MON_6226);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0}, enabled = false)
    /*
            MON_6227
            create subscriber through self registration
            provide past date as expiry date
            validate error message

            MON_6230
            create subscriber through channelUser
            provide past date as expiry date
            validate error message
     */
    public void MON_6227() throws Exception {

        ExtentTest MON_6227 = pNode.createNode("MON-6227", " Unsuccessful Subscriber self " +
                "registration when Expiry date is Past Date");

        try {

            String pastDateAsExpiryDate = new DateAndTime().getDate(-2);

            User subscriber = new User(Constants.SUBSCRIBER);
            subscriber.setExpiryDate(pastDateAsExpiryDate);

            Transactions.init(MON_6227)
                    .selfRegistrationForSubscriberWithAdditionalFields(subscriber, id_type)
                    .assertMessage("SSR001");


            ExtentTest MON_6230 = pNode.createNode("MON-6230", "Unsuccessful Subscriber Registration" +
                    " through channel user when Expiry date is Past Date");

            User subscriber1 = new User(Constants.SUBSCRIBER);
            subscriber1.setExpiryDate(pastDateAsExpiryDate);

            TxnResponse res = Transactions.init(MON_6230)
                    .SubscriberRegistrationByChannelWithAdditionalParameters(subscriber1, id_type);

            String actual = res.Message;
            String expected = MessageReader.getMessage("SSR001", null);

            Assertion.verifyContains(actual, expected, "Error Message not present", MON_6230);


        } catch (Exception e) {
            markTestAsFailure(e, MON_6227);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0}, enabled = false)
    /*
    *       MON_6228
            create subscriber through self registration
            provide current date as expiry date
            validate error message

            MON_6231
            create subscriber through channelUser
            provide current date as expiry date
            validate error message

    * */
    public void MON_6228() throws Exception {

        ExtentTest MON_6228 = pNode.createNode("MON-6228", "Unsuccessful Subscriber self " +
                "registration when Expiry date is present Date ");

        try {

            String presentDateAsExpiryDate = new DateAndTime().getDate(0);

            User subs = new User(Constants.SUBSCRIBER);
            subs.setExpiryDate(presentDateAsExpiryDate);

            Transactions.init(MON_6228)
                    .selfRegistrationForSubscriberWithAdditionalFields(subs, id_type)
                    .assertMessage("SSR001");


            ExtentTest MON_6231 = pNode.createNode("MON-6231", "Unsuccessful Subscriber " +
                    "Registration through channel user when Expiry date is present Date ");

            User subs1 = new User(Constants.SUBSCRIBER);
            subs1.setExpiryDate(presentDateAsExpiryDate);

            TxnResponse res = Transactions.init(MON_6231)
                    .SubscriberRegistrationByChannelWithAdditionalParameters(subs1, id_type);

            String actual = res.Message;
            String expected = MessageReader.getMessage("SSR001", null);

            Assertion.verifyContains(actual, expected, "Error Message not present", MON_6231);


        } catch (Exception e) {
            markTestAsFailure(e, MON_6228);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    /*
            MON_6243
            create subscriber through self registration
            provide invalid ID Type
            validate proper error message

            MON_6244
            create subscriber through channelUser
            provide invalid ID Type
            validate proper error message
    */
    public void MON_6243() throws Exception {

        ExtentTest MON_6243 = pNode.createNode("MON-6243", "Unsuccessful Subscriber self " +
                "registration when invalid ID-Type is provided and validate proper error message");
        try {

            User sub1 = new User(Constants.SUBSCRIBER);
            sub1.setExpiryDate(expiryDate);

            Transactions.init(MON_6243)
                    .startNegativeTest()
                    .selfRegistrationForSubscriberWithAdditionalFields(sub1, "PORT")
                    .assertMessage("SSR002");


            ExtentTest MON_6244 = pNode.createNode("MON-6244", "Successful Subscriber Registration " +
                    "through channel user when invalid ID-Type in provided and validate proper error message");

            User sub2 = new User(Constants.SUBSCRIBER);
            sub2.setExpiryDate(expiryDate);

            TxnResponse res = Transactions.init(MON_6244)
                    .SubscriberRegistrationByChannelWithAdditionalParameters(sub2, "PORT");

            String actual = res.Message;
            String expected = MessageReader.getMessage("SSR002", null);

            Assertion.verifyContains(actual, expected, "Error Message not present", MON_6244);

        } catch (Exception e) {
            markTestAsFailure(e, MON_6243);
        }
        Assertion.finalizeSoftAsserts();
    }
}
