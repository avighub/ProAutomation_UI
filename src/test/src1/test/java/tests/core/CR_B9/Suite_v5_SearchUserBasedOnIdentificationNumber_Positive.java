package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : Search user based on Identification Number in CCE Portal
 * when user hasmultiple mobile numbers with same national id
 * Author Name      : Nirupama MK
 * Created Date     : 22/05/2018
 */

public class Suite_v5_SearchUserBasedOnIdentificationNumber_Positive extends TestInit {

    private User sub1, sub2, subscriber;
    private String preferenceCode = "FREQ_EXT_CODE_N_SUBS", defaulPreferencValue, defaultModifyAllowed;
    private OperatorUser netAdmin;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "fetch the default value of system preference");

        try {
            defaultModifyAllowed = MobiquityGUIQueries.fetchmodifyAllowedFromGUI(preferenceCode);
            MobiquityGUIQueries
                    .updateModifyAllowed(preferenceCode, "Y");

            defaulPreferencValue = MobiquityGUIQueries.fetchDefaultValueOfPreference(preferenceCode);
            SystemPreferenceManagement.init(eSetup)
                    .updateSystemPreference(preferenceCode, Constants.PREFERENCE_EXTERNAL_CODE);

            subscriber = new User(Constants.SUBSCRIBER);
            CurrencyProviderMapping.init(eSetup)
                    .mapPrimaryWalletPreference(subscriber);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.MONEY_SMOKE})
    public void MON_5755() throws Exception {

        /*
            MON_5755
            Login as netAdmin,
            System preference "FREQ_EXT_CODE_N_SUBS" Set to "2".
            Should be able to Create Multiple Subscriber with same External Code(Identification Number).

            MON_5756
            fetch list of subscriber associated with same External_code from DB
            Login as netAdmin.
            Follow the links "Enquiries -> Customer Care Executive"
            the logged in user can search for another user based on various parameters
            like KYC ID, mobile number, etc.

            MON_5757
            Validate fields when Single Subscriber is displayed

            MON_5761
            Validate Fields when List of Subscribers are displayed

        */

        ExtentTest MON_5755 = pNode.createNode("MON-5755", "Multiple subscribers can be registered " +
                " with same national id (EXTERNAL_CODE) when System preference" +
                " FREQ_EXT_CODE_N_SUBS is set to more than '1'.").assignCategory(FunctionalTag.ECONET_SIT_5_0);
        String mainWindow = null;
        try {
            netAdmin = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

            sub1 = new User(Constants.SUBSCRIBER);

            Transactions.init(MON_5755)
                    .selfRegistrationForSubscriberWithKinDetails(sub1)
                    .assertStatus(Constants.TXN_SUCCESS);

            //should be able to create 2nd self subscriber with 1st subscriber's identification number
            sub2 = new User(Constants.SUBSCRIBER);

            sub2.setExternalCode(sub1.ExternalCode);

            Transactions.init(MON_5755)
                    .selfRegistrationForSubscriberWithKinDetails(sub2)
                    .assertStatus(Constants.TXN_SUCCESS);


            ExtentTest MON_5756 = pNode.createNode("MON-5756", "list of all subscribers with the " +
                    " matching national id is displayed when user type selected is Subscriber and the " +
                    " account identifier selected is KYC ID");

            //fetch List of subscriber with same identification num from DB
            List<String> subList = MobiquityGUIQueries.fetchAllSubscriberMsisdn(sub1.ExternalCode);

            Login.init(MON_5756).login(netAdmin);

            mainWindow = Enquiries.init(MON_5756)
                    .initiateGlobalSearch(Constants.SUBSCRIBER_REIMB, Constants.ACCOUNT_IDENTIFIER_KYC_ID, sub1.ExternalCode);

            //fetch list of matching sub's MSISDN  from UI
            List<WebElement> msisdnList = driver.findElements(By.xpath("//*[@id='global_search_user_details_list']//a"));

            //verify the List of sub's MSISDN with same identification number is displayed
            MON_5756.info("List of Subscribers having EXTERNAL_CODE " + sub1.ExternalCode);

            for (WebElement msisdn : msisdnList) {
                String MSISDN = msisdn.getText();

                if (subList.contains(MSISDN)) {
                    MON_5756.pass("subscriber  " + MSISDN);

                } else {
                    MON_5756.fail("subscriber with unique identification number");
                }
            }

            ExtentTest MON_5757 = pNode.createNode("MON-5757", " validate the fields are available " +
                    " Mobile number, First name, Last name, date of birth");

            //verify the fields when Only one subscriber details are displayed

            boolean status = Utils.checkElementPresent("msisdnLabel", Constants.FIND_ELEMENT_BY_ID);

            status = Utils.checkElementPresent("mobileNumberLabel", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "mobile_number Field", MON_5757);

            status = Utils.checkElementPresent("firstNameLabel", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "first_name Field", MON_5757);

            status = Utils.checkElementPresent("lastNameLabel", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "last_name Field", MON_5757);

            status = Utils.checkElementPresent("dateOfBirthLabel", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "date_of_birth Field", MON_5757);

            Utils.captureScreen(MON_5757);


        } catch (Exception e) {
            markTestAsFailure(e, MON_5755);
        } finally {
            if (mainWindow != null)
                Utils.closeWindowsExceptOne(mainWindow);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_5761() throws Exception {

        ExtentTest MON_5761 = pNode.createNode("MON-5761", "display the subscriber's details ," +
                " If only 1 subscriber matches the KYC Id ");

        try {

            netAdmin = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);
            sub1 = new User(Constants.SUBSCRIBER);

            Transactions.init(MON_5761)
                    .selfRegistrationForSubscriberWithKinDetails(sub1)
                    .assertStatus(Constants.TXN_SUCCESS);

            sub2 = new User(Constants.SUBSCRIBER);

            sub2.setExternalCode(sub1.ExternalCode);

            Transactions.init(MON_5761)
                    .selfRegistrationForSubscriberWithKinDetails(sub2)
                    .assertStatus(Constants.TXN_SUCCESS);

            Login.init(MON_5761).login(netAdmin);

            String mainWindow = Enquiries.init(MON_5761).initiateGlobalSearch(Constants.SUBSCRIBER_REIMB,
                    Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, sub1.MSISDN);


            boolean status = Utils.checkElementPresent("msisdnLabel", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "MSISDN Field", MON_5761);

            status = Utils.checkElementPresent("nameLabel", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "name Field", MON_5761);

            status = Utils.checkElementPresent("date_of_birth_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "date_of_birth Field", MON_5761);

            status = Utils.checkElementPresent("date_of_birth_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "address Field", MON_5761);

            status = Utils.checkElementPresent("registered_on_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "Registered on Field", MON_5761);

            status = Utils.checkElementPresent("registered_by_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "registered_by Field", MON_5761);

            status = Utils.checkElementPresent("last_transaction_status_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "last_transaction_status Field", MON_5761);

            status = Utils.checkElementPresent("last_transaction_on_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "last_transaction_on Field", MON_5761);

            status = Utils.checkElementPresent("last_transaction_type_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "last_transaction_type Field", MON_5761);

            status = Utils.checkElementPresent("current_customer_status_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "current_customer_status Field", MON_5761, true);

            Utils.captureScreen(MON_5761);

            driver.switchTo().window(mainWindow);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5761);

        } finally {
            SystemPreferenceManagement.init(MON_5761).updateSystemPreference(preferenceCode, defaulPreferencValue);
            MobiquityGUIQueries.updateModifyAllowed(preferenceCode, defaultModifyAllowed);
        }
    }


}