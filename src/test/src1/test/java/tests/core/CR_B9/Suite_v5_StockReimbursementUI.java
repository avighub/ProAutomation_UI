package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.stockManagement.ReimbursementApproval_page1;
import framework.pageObjects.stockManagement.Reimbursement_page1;
import framework.pageObjects.stockManagement.Reimbursement_page2;
import framework.pageObjects.stockManagement.Reimbursement_page3;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_StockReimbursement_NetworkSuspend
 * Author Name      : PushpaLatha
 * Created Date     : 24/07/2018
 */
public class Suite_v5_StockReimbursementUI extends TestInit {

    private Object[] expectedCommon, expectedUsr, actualData;
    private ArrayList expectedData, actual;
    private String act, prefValue;
    private OperatorUser opt;
    private User whs, subs;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        TransactionManagement.init(pNode).makeSureChannelUserHasBalance(whs, new BigDecimal("50"));

        prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("CURRENCY_FACTOR");

    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void fieldValidation() throws Exception {

        ExtentTest t1 = pNode.createNode("SC01",
                "Verify that all the mandatory fields are displayed in Stock Reimbursement initiate screen");

        try {
            Login.init(t1).login(opt);

            Reimbursement_page1 reimbursement_page1 = new Reimbursement_page1(t1);

            reimbursement_page1.navigateToLink();

            //UserType=Subscriber
            reimbursement_page1.type_SelectValue("SUBSCRIBER");

            expectedCommon = UserFieldProperties.getLabels("Reimb.pg1");
            expectedUsr = UserFieldProperties.getLabels("Reimb.subs");

            expectedData = new ArrayList(Arrays.asList(expectedCommon));
            expectedData.addAll(Arrays.asList(expectedUsr));
            Collections.sort(expectedData);

            actualData = PageInit.fetchLabelTexts("//table[@class='wwFormTableC']/tbody/tr[not(contains(@style,'display: none;'))]/td[1]/label|//tr[11]/td[3]");
            actual = new ArrayList(Arrays.asList(actualData));
            Collections.sort(actual);

            Assert.assertEquals(actual, expectedData);
            t1.pass("Labels verified successfully.");


            //UserType=Operator
            reimbursement_page1.type_SelectValue("OPERATOR");

            expectedCommon = UserFieldProperties.getLabels("Reimb.pg1");
            expectedUsr = UserFieldProperties.getLabels("Reimb.opt");

            expectedData = new ArrayList(Arrays.asList(expectedCommon));
            expectedData.addAll(Arrays.asList(expectedUsr));
            Collections.sort(expectedData);

            actualData = PageInit.fetchLabelTexts("//table[@class='wwFormTableC']/tbody/tr[not(contains(@style,'display: none;'))]/td[1]/label|//tr[11]/td[3]");
            actual = new ArrayList(Arrays.asList(actualData));
            Collections.sort(actual);

            Assert.assertEquals(actual, expectedData);
            t1.pass("Labels verified successfully.");

            //UserType=Biller/Merchant
            reimbursement_page1.type_SelectValue("MERCHANT");

            expectedCommon = UserFieldProperties.getLabels("Reimb.pg1");
            expectedUsr = UserFieldProperties.getLabels("Reimb.biller");
            expectedData = new ArrayList(Arrays.asList(expectedCommon));
            expectedData.addAll(Arrays.asList(expectedUsr));
            Collections.sort(expectedData);

            actualData = PageInit.fetchLabelTexts("//table[@class='wwFormTableC']/tbody/tr[not(contains(@style,'display: none;'))]/td[1]/label|//tr[11]/td[3]");
            actual = new ArrayList(Arrays.asList(actualData));
            Collections.sort(actual);

            Assert.assertEquals(actual, expectedData);
            t1.pass("Labels verified successfully.");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void selectTypeAsSubscriberMsisdnForChannelUser() throws Exception {

        ExtentTest t1 = pNode.createNode("SC02",
                "Verify that error message is displayed on selecting Type as Channel and entering Subscriber msisdn in reimbursement initiate screen");

        try {
            if (MobiquityGUIQueries.checkMsisdnExistInSubscribers(whs.MSISDN)) {
                whs = new User(Constants.WHOLESALER);
                ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);
            }
            if (MobiquityGUIQueries.checkMsisdnExistInUsers(subs.MSISDN)) {
                subs = new User(Constants.SUBSCRIBER);
                SubscriberManagement.init(t1).
                        createDefaultSubscriberUsingAPI(subs);
            }
            Login.init(t1).login(opt);

            Reimbursement_page1 reimbursement_page1 = new Reimbursement_page1(t1);

            reimbursement_page1.navigateToLink();

            //UserType=Subscriber
            reimbursement_page1.type_SelectValue("SUBSCRIBER");
            reimbursement_page1.msisdn_SetText(whs.MSISDN);
            reimbursement_page1.clickOnProvider();
            Thread.sleep(1000);
            act = AlertHandle.getAlertText(t1);
            Assertion.verifyMessageContain(act, "error.msisdnnotexist", "Enter ChannelUser MSISDN", t1);
            AlertHandle.acceptAlert(t1);
            Thread.sleep(1000);

            //UserType=Channel User
            reimbursement_page1.navigateToLink();
            reimbursement_page1.type_SelectValue("CHANNEL");
            reimbursement_page1.msisdn_SetText(subs.MSISDN);
            reimbursement_page1.clickOnProvider();
            Thread.sleep(1000);
            act = AlertHandle.getAlertText(t1);
            Assertion.verifyMessageContain(act, "error.msisdnnotexist", "Enter Subscriber MSISDN", t1);
            Thread.sleep(1000);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void validateMSISDNFieldIsNumericOnly() throws Exception {

        ExtentTest t1 = pNode.createNode("SC03",
                "Verify that MSISDN field is numeric only");
        try {
            String remark = "Test";

            Login.init(t1).login(opt);
            Reimbursement_page1 reimbursement_page1 = new Reimbursement_page1(t1);
            reimbursement_page1.navigateToLink();

            //Less than 10 numbers
            reimbursement_page1.type_SelectValue("SUBSCRIBER");
            reimbursement_page1.msisdn_SetText("774586954");
            reimbursement_page1.clickOnProvider();
            Thread.sleep(1000);
            act = AlertHandle.getAlertText(t1);
            Assertion.verifyMessageContain(act, "stock.reimbursement.msisdn.less", "Enter MSISDN less than 10 digits", t1);
            AlertHandle.acceptAlert(t1);
            Thread.sleep(5000);

            //characters
            reimbursement_page1.navigateToLink();
            reimbursement_page1.type_SelectValue("CHANNEL");
            reimbursement_page1.msisdn_SetText("subscriber");
            reimbursement_page1.clickOnProvider();
            Thread.sleep(1000);
            act = AlertHandle.getAlertText(t1);
            Assertion.verifyMessageContain(act, "ci.error.onlynum", "Enter text in MSISDN", t1);
            AlertHandle.acceptAlert(t1);
            Thread.sleep(5000);

            //Special characters
            reimbursement_page1.navigateToLink();
            reimbursement_page1.type_SelectValue("SUBSCRIBER");
            reimbursement_page1.msisdn_SetText("!@#$%^^&*%");
            reimbursement_page1.clickOnProvider();
            Thread.sleep(1000);
            act = AlertHandle.getAlertText(t1);
            Assertion.verifyMessageContain(act, "ci.error.onlynum", "Enter specialCharacter in MSISDN", t1);
            AlertHandle.acceptAlert(t1);
            Thread.sleep(5000);

            //AlphaNumeric
            reimbursement_page1.navigateToLink();
            reimbursement_page1.type_SelectValue("CHANNEL");
            reimbursement_page1.msisdn_SetText("77458abcdr");
            reimbursement_page1.clickOnProvider();
            Thread.sleep(1000);
            act = AlertHandle.getAlertText(t1);
            Assertion.verifyMessageContain(act, "ci.error.onlynum", "Enter specialCharacter in MSISDN", t1);
            AlertHandle.acceptAlert(t1);
            Thread.sleep(5000);

            //Invalid
            reimbursement_page1.navigateToLink();
            reimbursement_page1.type_SelectValue("SUBSCRIBER");
            reimbursement_page1.msisdn_SetText("7700000000");
            reimbursement_page1.clickOnProvider();
            Thread.sleep(1000);
            act = AlertHandle.getAlertText(t1);
            Assertion.verifyMessageContain(act, "error.msisdnnotexist", "Enter specialCharacter in MSISDN", t1);
            AlertHandle.acceptAlert(t1);
            Thread.sleep(5000);


            //blank
            reimbursement_page1.navigateToLink();
            reimbursement_page1.type_SelectValue("CHANNEL");
            reimbursement_page1.msisdn_SetText("");
            reimbursement_page1.clickOnProvider();
            Thread.sleep(1000);
            act = AlertHandle.getAlertText(t1);
            Assertion.verifyMessageContain(act, "ci.error.msisdnvalue", "Enter specialCharacter in MSISDN", t1);
            AlertHandle.acceptAlert(t1);
            Thread.sleep(5000);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void verifyThatRefNoFieldIsNumericOnly() throws Exception {

        ExtentTest t1 = pNode.createNode("SC04",
                "Verify that ref No field is numeric only");
        try {
            String remark = "Test";

            Login.init(t1).login(opt);
            Reimbursement_page1 reimbursement_page1 = new Reimbursement_page1(t1);
            reimbursement_page1.navigateToLink();

            reimbursement_page1.type_SelectValue("CHANNEL");
            reimbursement_page1.msisdn_SetText(whs.MSISDN);
            Thread.sleep(2000);
            reimbursement_page1.provider_SelectIndex();
            Thread.sleep(2000);

            //Special Characters
            reimbursement_page1.refNumber_SetText("!@#$%^");
            reimbursement_page1.remark_SetText(remark);
            Thread.sleep(3000); // sleep is required
            reimbursement_page1.submit_Click();
            Thread.sleep(2000);

            Assertion.verifyErrorMessageContain("stock.reimbursement.refNo.special.character", "Reference Number field validation", t1);
            Thread.sleep(2000);

            //Blank
            reimbursement_page1.refNumber_SetText("");
            reimbursement_page1.remark_SetText(remark);
            Thread.sleep(3000); // sleep is required
            reimbursement_page1.submit_Click();
            Thread.sleep(2000);
            Assertion.verifyErrorMessageContain("stock.reimbursement.refNo.blank", "Reference Number field validation", t1);
            Thread.sleep(2000);

            //Duplicate
            String refNo = RandomStringUtils.randomAlphanumeric(7);
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            TransactionManagement.init(pNode).makeSureChannelUserHasBalance(whs, new BigDecimal("50"));

            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "2", "test", null, false);
            startNegativeTest();
            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "5", "test1", null, false);
            Thread.sleep(2000);
            Assertion.verifyErrorMessageContain("stock.reimbursement.refNo.duplicate", "Reference Number field validation", t1);
            Thread.sleep(2000);

            //Alphanumeric with spaces
            stopNegativeTest();
            StockManagement.init(t1)
                    .initiateStockReimbursement(whs, DataFactory.getRandomNumberAsString(7) + " " + DataFactory.getRandomString(2), "2", "test", null, false);
            Thread.sleep(2000);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void validateThatFirstPageDetailsAreReflectedInSecondPage() throws Exception {

        ExtentTest t1 = pNode.createNode("SC05",
                "Verify that details entered in first page of reimbursement initiate screen is displayed properly in second page");

        try {
            String refNo = DataFactory.getRandomNumberAsString(5);
            String remark = "Test";

            Login.init(t1).login(opt);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "5", remark, null, false);
            Thread.sleep(2000);
            Reimbursement_page2 reimbursement_page2 = new Reimbursement_page2(pNode);
            List<String> values = reimbursement_page2.returnDetails();

            ArrayList<String> expected = new ArrayList<String>();
            expected.add(opt.FirstName + " " + opt.LastName);
            expected.add(whs.FirstName);
            expected.add(whs.LastName);
            expected.add(whs.MSISDN);
            expected.add(DataFactory.getCurrentDateSlash());
            expected.add(whs.CategoryName);
            expected.add(remark);

            Assert.assertEquals(values, expected);
            t1.info("Details verified successfully");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void verifyThatProperPayerBalanceIsDisplayed() throws Exception {

        ExtentTest t1 = pNode.createNode("SC06",
                "Verify that proper payer balance is displayed in stock reimbursement initiate screen");

        try {
            UsrBalance bal = MobiquityGUIQueries.getUserBalance(whs, null, null);

            int balance = bal.Balance.intValue(); // Integer.parseInt(prefValue);

            Login.init(t1).login(opt);
            String refNo = RandomStringUtils.randomAlphanumeric(5);
            String remark = "Test";
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "5", remark, null, false);
            Thread.sleep(2000);
            Reimbursement_page2 reimbursement_page2 = new Reimbursement_page2(pNode);
            String value = reimbursement_page2.getPayerBalance();

            if (String.valueOf(balance).equalsIgnoreCase(value)) {
                t1.info("Proper Payer balance is displayed: \n" + "Expected: " + bal.Balance.toString() + "\n" + "Actual: " + value + "\n");
            } else {
                t1.info("Payer balance is not proper");
                Assert.fail();
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void validateAmountField() throws Exception {

        ExtentTest t1 = pNode.createNode("SC07",
                "Verify that Amount field is numeric only");

        try {
            Login.init(t1).login(opt);
            String refNo = DataFactory.getRandomNumberAsString(3);
            String remark = "Test";

            //Characters
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "5", remark, null, false);
            Reimbursement_page2 reimbursement_page2 = new Reimbursement_page2(pNode);
            reimbursement_page2.amount_SetText("test");
            reimbursement_page2.confirm_Click();
            Assertion.verifyErrorMessageContain("o2c.initiate.error.amountVal", "Enter characters in Amount field", t1);
            Thread.sleep(2000);

            //Special Character
            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "5", remark, null, false);
            reimbursement_page2.amount_SetText("!@#$%^");
            reimbursement_page2.confirm_Click();
            Assertion.verifyErrorMessageContain("o2c.initiate.error.amountVal", "Enter Special characters in Amount field", t1);
            Thread.sleep(2000);

            //Negative value
            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "5", remark, null, false);
            reimbursement_page2.amount_SetText("-2");
            reimbursement_page2.confirm_Click();
            Assertion.verifyErrorMessageContain("o2c.initiate.error.amountVal", "Enter negative value in Amount field", t1);
            Thread.sleep(2000);

            //value equal to 0
            startNegativeTest();
            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "5", remark, null, false);
            reimbursement_page2.amount_SetText("0");
            reimbursement_page2.confirm_Click();
            Assertion.verifyErrorMessageContain("o2c.initiate.error.amountVal", "Enter 0 in Amount field", t1);
            Thread.sleep(2000);

            //decimal values
            StockManagement.init(t1).initiateStockReimbursement(whs, refNo, "5", remark, null, false);
            reimbursement_page2.amount_SetText("2.14527");
            reimbursement_page2.confirm_Click();
            Assertion.verifyErrorMessageContain("c2c.error.corrDecFormat", "Enter decimal value in Amount field", t1);
            Thread.sleep(2000);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void validateConfirmscreenDetails() throws Exception {

        ExtentTest t1 = pNode.createNode("SC08",
                "Verify that entered details are properly displayed in confirmation screen");

        try {
            UsrBalance bal = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int balance = bal.Balance.intValue();

            Login.init(t1).login(opt);
            String refNo = RandomStringUtils.randomAlphanumeric(4);
            String remark = "Test";

            startNegativeTest();
            StockManagement.init(t1).expectErrorB4Confirm().initiateStockReimbursement(whs, refNo, "5", remark, null, false);
            Reimbursement_page3 reimbursement_page3 = new Reimbursement_page3(pNode);
            List<String> values = reimbursement_page3.getConfirmPageDetails();
            //Collections.sort(values);

            ArrayList<String> expected = new ArrayList<String>();
            expected.add(opt.FirstName + " " + opt.LastName);
            expected.add(whs.FirstName);
            expected.add(whs.LastName);
            expected.add(whs.MSISDN);
            expected.add(DataFactory.getCurrentDateSlash());
            expected.add(whs.CategoryName);
            expected.add(remark);
            expected.add(String.valueOf(balance));
            //Collections.sort(expected);

            Assert.assertEquals(values, expected);
            t1.info("Details verified successfully");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.v5_0})
    public void validateBackbutton() throws Exception {

        ExtentTest t1 = pNode.createNode("SC09",
                "Verify that back button is working as expected");

        try {
            UsrBalance bal = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int balance = bal.Balance.intValue();

            Login.init(t1).login(opt);
            String refNo = DataFactory.getRandomNumberAsString(5);
            String remark = "Test";
            String amount = "5";

            Reimbursement_page3 reimbursement_page3 = new Reimbursement_page3(pNode);
            Reimbursement_page2 reimbursement_page2 = new Reimbursement_page2(pNode);
            Reimbursement_page1 reimbursement_page1 = new Reimbursement_page1(pNode);

            reimbursement_page1.navigateToLink();
            reimbursement_page1.type_SelectValue("CHANNEL");
            reimbursement_page1.msisdn_SetText(whs.MSISDN);
            Thread.sleep(2000);
            reimbursement_page1.provider_SelectIndex();
            reimbursement_page1.refNumber_SetText(refNo);
            reimbursement_page1.remark_SetText(remark);

            Thread.sleep(3000); // sleep is required
            reimbursement_page1.submit_Click();
            reimbursement_page2.amount_SetText(amount);
            Thread.sleep(1000);
            reimbursement_page2.confirm_Click();
            Thread.sleep(1000);

            reimbursement_page3.back_Click();

            Thread.sleep(2000);
            List<String> labels = reimbursement_page2.getLabelDetails();
            labels.add(reimbursement_page2.getAmount());
            labels.add(reimbursement_page2.getPayerBalance());
            Collections.sort(labels);

            ArrayList<String> expected = new ArrayList<String>();
            expected.add(opt.FirstName + " " + opt.LastName);
            expected.add(whs.FirstName);
            expected.add(whs.LastName);
            expected.add(whs.MSISDN);
            expected.add(DataFactory.getCurrentDateSlash());
            expected.add(whs.CategoryName);
            expected.add(remark);
            expected.add(amount);
            expected.add(String.valueOf(balance));
            Collections.sort(expected);

            Assert.assertEquals(labels, expected);
            t1.info("Page2 Details verified successfully on clicking back button");

            reimbursement_page2.back_Click1();


            ArrayList<String> labelsPage1 = new ArrayList<String>();
            labelsPage1.add(reimbursement_page1.getMSISDNValue());
            labelsPage1.add(reimbursement_page1.getRefNoValue());
            labelsPage1.add(reimbursement_page1.getRemarksValue());

            ArrayList<String> expected1 = new ArrayList<String>();
            expected1.add(whs.MSISDN);
            expected1.add(refNo);
            expected1.add(remark);

            Assert.assertEquals(labelsPage1, expected1);
            t1.info("Page1 Details verified successfully on clicking back button");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.v5_0})
    public void verifyInApprovalScreenStockReimbursementTransfersPendingForApprovalButNotInitiatedByTheSameUserIsDisplayed() throws Exception {

        ExtentTest t1 = pNode.createNode("SC010",
                "Verify that on clicking Stock Reimbursement Approval link, all transfers pending for approval, but not initiated by the same user who has logged in " +
                        "should be listed");

        try {
            OperatorUser opt = DataFactory.getOperatorUserWithAccess("STOCK_REINIT", Constants.NETWORK_ADMIN);
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            TransactionManagement.init(pNode).makeSureChannelUserHasBalance(whs, new BigDecimal("50"));

            Login.init(t1).login(opt);
            startNegativeTest();
            StockManagement.init(t1).initiateStockReimbursement(whs, DataFactory.getRandomNumberAsString(6), "5", "test", null, false);
            String msg = Assertion.getActionMessage();
            String txnId = msg.split("ID:")[1].trim();

            ReimbursementApproval_page1 reimbursementApproval_page1 = new ReimbursementApproval_page1(pNode);
            reimbursementApproval_page1.navigate();
            List<WebElement> values = reimbursementApproval_page1.getRequestors();

            int flag = 0;
            for (WebElement req : values) {
                if (!(req.getText().contains(opt.FirstName))) {
                    flag = 0;
                } else {
                    flag = 1;
                    break;
                }
            }

            if (flag == 1) {
                t1.fail("Requestor initiated transactions are displayed in approval page");
            } else {
                t1.pass("Requestor initiated transactions are not displayed in approval page");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
