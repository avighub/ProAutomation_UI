package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_v5_fetchListOfCurrencies_Negative extends TestInit {

    /***
     * Company Name     : Comviva Technologies Ltd.
     * Application Name : Mobiquity 5.0
     * Objective        : API to fetch list of currencies associated with user
     * Author Name      : Nirupama MK
     * Created Date     : 21/05/2018
     */

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void MON_5720() throws Exception {

        /*
         MON_5720
         fetch list of currencies for the Subscriber, by making user type as "Channel User"
         validate Error message "User not Found"
         */

        ExtentTest t1 = pNode.createNode("MON-5720", "when a Subscriber currencies are " +
                " enquired for Channel User OR vice versa. The response should be User  Not found");

        try {

            User subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            User channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            String userType = MobiquityGUIQueries.fetchUserType(channelUser, channelUser.MSISDN);

            TxnResponse response = Transactions.init(t1)
                    .startNegativeTest()
                    .fetchListOfCurrenciesAssociatedWithUser(subscriber.MSISDN, userType);

            response.assertMessage("00066");


        } catch (Exception e) {

            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
