package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Stock Liquidation API Generation
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 21/03/2018
 */

public class Suite_v5_StockLiquidationAPIGeneration extends TestInit {
    OperatorUser baruser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        try {
            baruser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0})
    public void generateAPIforSuspendedInitiatedChannelUser() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4089_5", "verify system should initiate stock liquidation for suspended initiate channel user.");

        try {
            //Create ChannelUser

            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(channeluser);
            Login.init(t1).login(baruser);
            //Suspend Initiate ChannelUser
            ChannelUserManagement.init(t1).suspendInitiateChannelUser(channeluser);
            //Generating Stock Liquidation
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(channeluser, batchId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0})
    public void netadminabletoUpdateSystemPreference() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4089_7", "Verify Network admin can able to update minimum balance preference for liquidation stock initiation. ");

        try {
            //NetAdmin Should be Able to Change the System Preference
            MobiquityGUIQueries m = new MobiquityGUIQueries();

            String pref = MobiquityGUIQueries.fetchDefaultValueOfPreference("LIQ_CONFIGURE_AMOUNT");
            SystemPreferenceManagement.init(t1).updateSystemPreference("LIQ_CONFIGURE_AMOUNT", "40");
            t1.info("NetworkAdmin able to update Preference");

            //Reverting Back the System Preference
            SystemPreferenceManagement.init(t1).updateSystemPreference("LIQ_CONFIGURE_AMOUNT", pref);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
