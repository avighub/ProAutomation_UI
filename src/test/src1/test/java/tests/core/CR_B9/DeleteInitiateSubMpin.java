package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by gurudatta.praharaj on 8/21/2018.
 */
public class DeleteInitiateSubMpin extends TestInit {
    private User whsUsr, subUsr;
    private ServiceCharge agentAccClose;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Initiating Setup");
        whsUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        subUsr = new User(Constants.SUBSCRIBER);

        agentAccClose = new ServiceCharge
                (Services.ACCOUNT_CLOSURE_BY_AGENT, subUsr, whsUsr, null, null, null, null);

        TransferRuleManagement.init(setup)
                .configureTransferRule(agentAccClose);

        ServiceChargeManagement.init(setup)
                .configureServiceCharge(agentAccClose);

    }

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest test1 = pNode.createNode("Story ID: 5800", "Subscriber can be able to login while subscriber is in delete initiate stage");
        ExtentTest test2 = pNode.createNode("Story ID: 5800", "Subscriber can not be able to login if wrong mPin is entered while subscriber is in delete initiate stage");
        try {

            SubscriberManagement.init(test1)
                    .createDefaultSubscriberUsingAPI(subUsr);

            Transactions.init(test1)
                    .initiateCashIn(subUsr, whsUsr, new BigDecimal("5"));

            Login.init(test1).login(whsUsr);

            SubscriberManagement.init(test1)
                    .deleteSubscriberByAgent(subUsr, DataFactory.getDefaultProvider().ProviderId);

            startNegativeTest();
            ConfigInput.mPin = "1356";
            TxnResponse response = Transactions.init(test2)
                    .subscriberLogin(subUsr);
            response.assertStatus("000680");

            Assertion.verifyEqual(response.getMessage(), MessageReader.getMessage("000680", null),
                    "" + subUsr.FirstName + ": mPin authentication failed: Login Failed", test2);

            stopNegativeTest();
            ConfigInput.mPin = "1357";
            Transactions.init(test1)
                    .subscriberLogin(subUsr);

        } catch (Exception e) {
            markTestAsFailure(e, test1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
