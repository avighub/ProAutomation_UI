package tests.core.CR_B9;

/**
 * Created by shiva.gangaraddi on 5/11/2018.
 */

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_v5_ChurnSettlement extends TestInit {

    private OperatorUser OptUser;
    private String defaultProvider;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest t1 = pNode.createNode("Setup",
                "configure service charge for churn reacivation");

        try {
            defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            OptUser = DataFactory.getOperatorUserWithAccess("CHURNMGMT_MAIN");
            User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            //Defining service charge and Transfer rule for Churn Reactivation
            ServiceCharge ChurnReactivationServiceForChannel = new ServiceCharge(Services.CHURN_REAC_SUB, OptUser, chUser,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).
                    configureServiceCharge(ChurnReactivationServiceForChannel);

            ServiceCharge ChurnReactivationServiceForSub = new ServiceCharge(Services.CHURN_REAC_SUB, OptUser, sub,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).
                    configureServiceCharge(ChurnReactivationServiceForSub);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0})
    public void MON_6342() throws Exception {

        ExtentTest t1 = pNode.createNode("MON-6342",
                "Verify the error message when entered churned msisdn is channel user's " +
                        "and user type selected is subscriber while performing churn settlement");

        try {
            //Create channeluser and Churn it

            User channeluser = CommonUserManagement.init(t1)
                    .getChurnedUser(Constants.WHOLESALER, null);

            Login.init(t1).login(OptUser);
            ChurnUser_Page1 page = ChurnUser_Page1.init(t1);
            page.navUserChurn_Settlement_Initiate();
            page.enterChurnedMssidn(channeluser.MSISDN);
            page.selectusertypebytext("Subscriber");
            page.clickOnSubmitBtn();

            Assertion.verifyErrorMessageContain("churn.alert.sub", "Select User Type as Subscriber" +
                    "and provide msisdn of channel user. Verify that user is not a subscriber", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0})
    public void MON_6343() throws Exception {

        ExtentTest t3 = pNode.createNode(" MON-6343", "Verify the error message when entered " +
                "churned msisdn is subscriber's and user type selected is channel user, while performing churn settlement");

        try {
            //Create subscriber
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            Login.init(t3).login(OptUser);
            User subscriber = CommonUserManagement.init(t3).getChurnedUser(Constants.SUBSCRIBER, null);

            //Initiate and approve churn settlement
            CommonUserManagement.init(t3)
                    .startNegativeTest()
                    .initiateChurnSettlement(subscriber, "Channel User");

            Assertion.verifyErrorMessageContain("churn.alert.channel", "User is not a Channel User", t3);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void MON_6345() throws Exception {

        ExtentTest t5 = pNode.createNode("MON-6345", "To Verify channel user is selected from the" +
                " dropdown and able to complete the churn reactivation");

        try {
            User churnedChUser = CommonUserManagement.init(t5)
                    .getChurnedUser(Constants.WHOLESALER, null);

            CommonUserManagement.init(t5)
                    .initiateChurnReactivation(churnedChUser)
                    .approveChurnSettlement(churnedChUser.MSISDN, true);


        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.v5_0})
    public void MON_6346() throws Exception {
        ExtentTest t6 = pNode.createNode("MON-6346", "To Verify Subscriber is selected" +
                " form the dropdown and completed the churn reactivation");

        try {
            User sub2 = CommonUserManagement.init(t6)
                    .getChurnedUser(Constants.SUBSCRIBER, null);

            CommonUserManagement.init(t6)
                    .initiateChurnReactivation(sub2)
                    .approveChurnSettlement(sub2.MSISDN, true);

        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, enabled = true, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_SIT_5_0})
    public void MON_5408() throws Exception {

        ExtentTest t4 = pNode.createNode("MON-5408", "Verify that user is able to select the " +
                "subscriber form the drop down and proceed with the churn settlement \n").assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {

            User subsReceiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            User churnedSUBS = CommonUserManagement.init(t4).getChurnedUser(Constants.SUBSCRIBER, null);

            //Initiate and approve churn settlement
            CommonUserManagement.init(t4).startNegativeTest()
                    .initiateApproveChurnSettlement(churnedSUBS, subsReceiver);

            Assertion.assertActionMessageContain("churn.settle.success.approval",
                    "Verify Successfully Approved Churn Settlement for MSISDN: " + churnedSUBS.MSISDN, t4);

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 6, enabled = true, groups = {FunctionalTag.v5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN})
    public void MON_5410() throws Exception {

        ExtentTest t2 = pNode.createNode("MON-5410", "Verify that user is able to select the " +
                "channel user form the drop down and proceed with the churn settlement");
        t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN);

        try {

            User subsReceiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            User churnedChUser = CommonUserManagement.init(t2).getChurnedUser(Constants.WHOLESALER, null);

            //Initiate and approve churn settlement
            CommonUserManagement.init(t2).startNegativeTest()
                    .initiateApproveChurnSettlement(churnedChUser, subsReceiver);
            Assertion.assertActionMessageContain("churn.settle.success.approval",
                    "Verify Successfully Approved Churn Settlement for MSISDN: " + churnedChUser.MSISDN, t2);


        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

}