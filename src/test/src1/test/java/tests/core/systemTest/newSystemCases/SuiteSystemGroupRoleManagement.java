/*
 *  COPYRIGHT: Comviva Technologies Pvt. Ltd.
 *  This software is the sole property of Comviva
 *  and is protected by copyright law and international
 *  treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of
 *  it may result in severe civil and criminal penalties
 *  and will be prosecuted to the maximum extent possible
 *  under the law. Comviva reserves all rights not
 *  expressly granted. You may not reverse engineer, decompile,
 *  or disassemble the software, except and only to the
 *  extent that such activity is expressly permitted
 *  by applicable law notwithstanding this limitation.
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 *  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 *  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 *  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 *  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *  Author Name: surya.dhal
 *  Date: 25-May-2018
 *  Purpose: Test Cases for Group Role Management
 */

package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.WebGroupRole;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.pageObjects.groupRole.GroupRole_Page1;
import framework.pageObjects.groupRole.GroupRole_Page2;
import framework.pageObjects.groupRole.GroupRole_Page3;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by surya.dhal on 5/25/2018.
 */
public class SuiteSystemGroupRoleManagement extends TestInit {

    private WebGroupRole webGroupRole;
    private List<String> applicableRole;
    private OperatorUser networkAdmin;
    private SuperAdmin superAdmin;

    /**
     * SETUP
     */

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            applicableRole = new ArrayList<>(Arrays.asList("SUBSADDAP", "SUBSADD"));
            webGroupRole = new WebGroupRole(Constants.WHOLESALER, "AUTROLETEST" + DataFactory.getRandomNumber(3), applicableRole, 1);
            superAdmin = DataFactory.getSuperAdminWithAccess("GRP_ROL");
            networkAdmin = DataFactory.getOperatorUserWithAccess("GRP_ROL");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_004_1
     * DESC : To verify that Network Admin  should be able to add group role in the system.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1}, priority = 1)
    public void P1_TC_004_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_004_1\n :Group Role Add", "To verify that Network Admin  should be able to add group role in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.GROUP_ROLE_MANAGEMENT);
        //----Login SuperAdmin which can create Group Role- ---

        try {
            Login.init(t1).login(networkAdmin);

            GroupRoleManagement.init(t1)
                    .addWebGroupRole(webGroupRole);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_007_1
     * DESC : To verify that Network Admin  should be able to view group role in the system.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1})
    public void P1_TC_007_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_007_1: Group Role View", "To verify that Network Admin  should be able to view group role in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            Login.init(t1).login(networkAdmin);

            GroupRoleManagement.init(t1)
                    .viewWebGroupRoleDetails(webGroupRole);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_005_2
     * DESC : To verify that Network Admin  should be able to Modify group role in the system.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1})
    public void P1_TC_005_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_005_2 :Group Role Modify", "To verify that Network Admin  should be able to Modify group role in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            Login.init(t1).login(networkAdmin);

            webGroupRole.removeRole("SUBSADDAP");

            GroupRoleManagement.init(t1)
                    .addWebGroupRole(webGroupRole);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_006_1
     * DESC : To verify that Network Admin  should be able to Delete group role in the system.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1}, alwaysRun = true)
    public void P1_TC_006_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_006_1 :Group Role Delete", "To verify that Network Admin  should be able to Delete group role in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1, FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            Login.init(t1).login(networkAdmin);

            GroupRoleManagement.init(t1)
                    .deleteWebGroupRoleDetails(webGroupRole);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID :
     * DESC :
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM}, enabled = false)
    public void TC_001() throws Exception {
        WebGroupRole userRole = GlobalData.rnrDetails.get(0);

        ExtentTest t1 = pNode.createNode("TC0079\n : Group Role Duplicate", "To verify that the valid user can Duplicate group role in the system.-");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            GroupRoleManagement.init(t1)
                    .duplicateWebRole(userRole);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_305_1
     * DESC : To verify that Network Admin should not be able to "delete group" role in the system if the Group Role is "linked" to any entity and system
     * should displays error message ‘Group Role can’t be deleted as one or more users associated with this Group Role’.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_P1}, alwaysRun = true)
    public void P1_TC_305_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_305_1 :Group Role Delete",
                "To verify that Network Admin should not be able to \"delete group\" role in the system if the Group Role is \"linked\" to any entity and system  " +
                        "should displays error message ‘Group Role can’t be deleted as one or more users associated with this Group Role’. ");

        t1.assignCategory(FunctionalTag.PVG_P1);

        try {
            Login.init(t1).login(networkAdmin);

            webGroupRole.CategoryCode = Constants.WHOLESALER;
            webGroupRole.RoleName = DataFactory.getWebGroupRoleName(Constants.WHOLESALER);

            startNegativeTest();
            GroupRoleManagement.init(t1)
                    .deleteWebGroupRoleDetails(webGroupRole);
            Assertion.verifyErrorMessageContain("grouprole.deleteUsedGroupRole.Error", "Delete Group Role", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_305_3
     * DESC : To verify that Super Admin should not be able to "delete group" role in the system if the Group Role is "linked" to any entity and system  " +
     * "should displays error message ‘Group Role can’t be deleted as one or more users associated with this Group Role’.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_P1}, alwaysRun = true)
    public void P1_TC_305_3() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_305_3 :Group Role Delete",
                "To verify that Super Admin should not be able to \"delete group\" role in the system if the Group Role is \"linked\" to any entity and system  " +
                        "should displays error message ‘Group Role can’t be deleted as one or more users associated with this Group Role’. ");

        t1.assignCategory(FunctionalTag.PVG_P1);

        try {
            Login.init(t1).loginAsSuperAdmin(superAdmin);

            webGroupRole.CategoryCode = Constants.WHOLESALER;
            webGroupRole.RoleName = DataFactory.getWebGroupRoleName(Constants.WHOLESALER);

            startNegativeTest();
            GroupRoleManagement.init(t1)
                    .deleteWebGroupRoleDetails(webGroupRole);
            Assertion.verifyErrorMessageContain("grouprole.deleteUsedGroupRole.Error", "Delete Group Role", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_391_2
     * DESC : To verify that while "Adding Group role" Network Admin should be able to see a section where the user will have to select the" +
     * " "Domain", "Category" and the "role (Web or Mobile)" which they need to define for the selected category.
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_P1})
    public void P1_TC_391_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_391_2 ",
                "To verify that while \"Adding Group role\" Network Admin should be able to see a section where the user will have to select the" +
                        " \"Domain\", \"Category\" and the \"role (Web or Mobile)\" which they need to define for the selected category.");

        t1.assignCategory(FunctionalTag.PVG_P1);

        try {
            Login.init(t1).login(networkAdmin);
            GroupRole_Page1 page = new GroupRole_Page1(t1);
            page.navAddWebGroupRole();
            GroupRole_Page1.verifySectionOnGroupRoleManagementPage();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * TEST : POSITIVE
     * ID : P1_TC_392_2
     * DESC : To verify that while adding "Web Role" Network Admin should be able to see "role/service names" with "check box" for selection and there is an option to 'check all’ roles/ services.
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_P1})
    public void P1_TC_392_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_392_2 ",
                "To verify that while adding \"Web Role\" Network Admin should be able to see \"role/service names\" with \"check box\" for selection and there is an option to 'check all’ roles/ services.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM);

        try {
            Login.init(t1).login(networkAdmin);
            GroupRole_Page1 page1 = new GroupRole_Page1(t1);
            page1.navAddWebGroupRole();
            page1.selectDomainName(DataFactory.getDomainName(Constants.CHANNEL_ADMIN));
            page1.selectCategoryName(DataFactory.getCategoryName(Constants.CHANNEL_ADMIN));
            page1.checkWebGroupRole();
            page1.submit();

            GroupRole_Page2 page2 = new GroupRole_Page2(t1);
            page2.initiateAddGroupRole();
            page2.verifyRoleOrServiceNamesWithCheckBox();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_392_3
     * DESC : To verify that while adding "Mobile Role" Super Admin should be able to select the "MFS Provider", "Payment Instrument" and "Wallet/Bank Type" and
     * Based on MFS Provider, the Payment instrument and Wallet/bank Type will be shown to the User for selection.
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_P1})
    public void P1_TC_392_3() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_392_3 ",
                "To verify that while adding \"Mobile Role\" Super Admin should be able to select the \"MFS Provider\", \"Payment Instrument\" and \"Wallet/Bank Type\" and  \n" +
                        "Based on MFS Provider, the Payment instrument and Wallet/bank Type will be shown to the User for selection.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM);

        try {
            Login.init(t1).loginAsSuperAdmin(superAdmin);

            GroupRole_Page1 page = new GroupRole_Page1(t1);

            page.navAddWebGroupRole();
            page.selectDomainName(DataFactory.getDomainName(Constants.WHOLESALER));
            page.selectCategoryName(DataFactory.getCategoryName(Constants.WHOLESALER));
            page.selectGradeNameByIndex();
            page.checkMobileGroupRole();
            page.submit();

            new GroupRole_Page2(t1).initiateAddGroupRole();

            GroupRole_Page3 page3 = new GroupRole_Page3(t1);
            page3.selectCurrencyProvider(DataFactory.getDefaultProvider().ProviderName);
            page3.selectPaymentInstrumentByValue(Constants.PAYINST_WALLET_CONST);
            page3.selectWalletOrBankByValue(DataFactory.getDefaultWallet().WalletId);

            if (!Assertion.isErrorInPage(t1)) {
                t1.info("Super Admin should be able to select the MFS Provider, Payment Instrument and Wallet/Bank Type");
                GroupRole_Page3.verifyElementsOnAddMobileRolePage();
            } else {
                t1.fail("Failure");
                Assertion.markAsFailure("failed to select provider/pay instrument or payment instrument type");
            }
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_392_4
     * DESC : To verify that while adding "Mobile Role" Network Admin should be able to select the "MFS Provider", "Payment Instrument" and "Wallet/Bank Type" and
     * Based on MFS Provider, the Payment instrument and Wallet/bank Type will be shown to the User for selection.
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_P1})
    public void P1_TC_392_4() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_392_4 ",
                "To verify that while adding \"Mobile Role\" Super Admin should be able to select the \"MFS Provider\", \"Payment Instrument\" and \"Wallet/Bank Type\" and  \n" +
                        "Based on MFS Provider, the Payment instrument and Wallet/bank Type will be shown to the User for selection.");

        t1.assignCategory(FunctionalTag.PVG_P1);

        try {
            Login.init(t1).login(networkAdmin);

            GroupRole_Page1 page1 = new GroupRole_Page1(t1);
            page1.navAddWebGroupRole();
            page1.selectDomainName(DataFactory.getDomainName(Constants.WHOLESALER));
            page1.selectCategoryName(DataFactory.getCategoryName(Constants.WHOLESALER));
            page1.selectGradeNameByIndex();
            page1.checkMobileGroupRole();
            page1.submit();

            GroupRole_Page2 page2 = new GroupRole_Page2(t1);
            page2.initiateAddGroupRole();

            GroupRole_Page3 page3 = new GroupRole_Page3(t1);
            page3.selectCurrencyProvider(DataFactory.getDefaultProvider().ProviderName);
            page3.selectPaymentInstrumentByValue(Constants.PAYINST_WALLET_CONST);
            page3.selectWalletOrBankByValue(DataFactory.getDefaultWallet().WalletId);
            if (!Assertion.isErrorInPage(t1)) {
                t1.info("Super Admin should be able to select the \"MFS Provider\", \"Payment Instrument\" and \"Wallet/Bank Type\"");
                GroupRole_Page3.verifyElementsOnAddMobileRolePage();
            } else {
                t1.fail("Failure reason - " + Assertion.getErrorMessage());
                Assertion.markAsFailure("Failure reason - " + Assertion.getErrorMessage());
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_393_2
     * DESC : To verify that "Network Admin" should be able to add group role in the system and System will display message  ‘Group Role is successfully added’
     * and this newly added group role is shown in the list of Group Roles for that particular Domain and category.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_P1}, priority = 2)
    public void P1_TC_393_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_393_2\n :Group Role Add", "To verify that \"Network Admin\" should be able to add group role in the system and System will display message  " +
                "‘Group Role is successfully added’ and this newly added group role is shown in the list of Group Roles for that particular Domain and category.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            WebGroupRole newWebGroupRole = new WebGroupRole(Constants.WHOLESALER, "DummyTestRole" + DataFactory.getRandomNumber(3), applicableRole, 1);

            Login.init(t1).login(networkAdmin);

            GroupRoleManagement.init(t1)
                    .addWebGroupRole(newWebGroupRole)
                    .viewWebGroupRoleDetails(newWebGroupRole);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_394_1
     * DESC : To verify that while "Modifying" web group role "Super Admin" should be able to see a section where the user will have to select the Domain, " +
     * "Category and the role (Web or Mobile) which they need to define for the selected category.
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_P1})
    public void P1_TC_394_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_394_1 ",
                "To verify that while \"Modifying\" web group role \"Super Admin\" should be able to see a section where the user will have to select the Domain, " +
                        "Category and the role (Web or Mobile) which they need to define for the selected category.");

        t1.assignCategory(FunctionalTag.PVG_P1);

        try {
            Login.init(t1).loginAsSuperAdmin(superAdmin);
            GroupRole_Page1 page = new GroupRole_Page1(t1);
            page.navAddWebGroupRole();

            page.selectDomainName(DataFactory.getDomainName(Constants.CHANNEL_ADMIN));
            page.selectCategoryName(DataFactory.getCategoryName(Constants.CHANNEL_ADMIN));

            Assertion.verifyEqual(page.checkWebGroupRole(), true, "Check - Group Role is selectable", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_394_2
     * DESC : To verify that while "Modifying" web group role "Network Admin" should be able to See a section where the user will have to select the Domain, " +
     * "Category and the role (Web or Mobile) which they need to define for the selected category.
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_P1})
    public void P1_TC_394_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_394_2 ",
                "To verify that while \"Modifying\" web group role \"Network Admin\" should be able to See a section where the user will have to select the Domain, " +
                        "Category and the role (Web or Mobile) which they need to define for the selected category.");

        t1.assignCategory(FunctionalTag.PVG_P1);

        try {
            Login.init(t1).login(networkAdmin);
            GroupRole_Page1 page = new GroupRole_Page1(t1);
            page.navAddWebGroupRole();

            page.selectDomainName(DataFactory.getDomainName(Constants.CHANNEL_ADMIN));
            page.selectCategoryName(DataFactory.getCategoryName(Constants.CHANNEL_ADMIN));

            Assertion.verifyEqual(page.checkWebGroupRole(), true, "Check - Group Role is selectable", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
