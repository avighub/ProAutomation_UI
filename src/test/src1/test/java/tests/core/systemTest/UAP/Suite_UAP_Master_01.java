package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.master.Master;
import framework.util.common.DataFactory;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for Network Status
 *
 * @author navin.pramanik
 */
public class Suite_UAP_Master_01 extends TestInit {

    OperatorUser netStat;

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.MASTER}, priority = 1)
    public void networkStatus() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0098: Modify Network Status", "To verify that the valid user can modify the message in network status..");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.MASTER);

        netStat = DataFactory.getOperatorUserWithAccess("MNT_STS");

        Login.init(t1).login(netStat);

        Master.init(t1).changeNetworkStatus();

    }

}
