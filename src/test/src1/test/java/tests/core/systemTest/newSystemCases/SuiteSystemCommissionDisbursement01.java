package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.commissionDisbursement.CommissionDisbursement;
import framework.features.common.Login;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page1;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page2;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

/**
 * Created by prashant.kumar on 11/16/2017.
 */
public class SuiteSystemCommissionDisbursement01 extends TestInit {
    private OperatorUser optCD;
    private User chUser, chUser2;
    private Map<String, User> chUsers;

    public static void checkPost(User payer, User payee) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, Constants.NORMAL_WALLET, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, Constants.COMMISSION_WALLET, null);
        DBAssertion.postRecon = MobiquityGUIQueries.getReconBalance();
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClassPrerequisite() throws Exception {
        chUsers = DataFactory.getChannelUserSetUniqueGrade();
        chUser = DataFactory.getAnyChannelUser();
        chUser2 = DataFactory.getAnyChannelUser();
        chUsers.remove(chUser.LoginId, chUser);
        optCD = DataFactory.getOperatorUserWithAccess("COMMDIS_INITIATE");
    }

    public void checkPre(User payer, User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, Constants.NORMAL_WALLET, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, Constants.COMMISSION_WALLET, null);
        DBAssertion.preRecon = MobiquityGUIQueries.getReconBalance();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT})
    public void Commission_Disbursement1() throws Exception {
        for (User users : chUsers.values()) {
            if (!(users.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER) || users.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE))) {
                ExtentTest t1 = pNode.createNode("SYS_TC_CommisionDisbursement_P_0001 :" + getTestcaseid(),
                        "To verify that Network admin is able to perform Commission Disbursement for " + users.CategoryCode + " of grade" + users.GradeName);
                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT);

                Login.init(t1).login(optCD);

                checkPre(users, users);

                CommissionDisbursement.init(t1).commissionDisburse(users);

                checkPost(users, users);

                DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);
            }
        }
    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT})
    public void Commission_Disbursement3() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CommisionDisbursement_P_0003 :Negative",
                "To verify that \"back\" button functionality should work correctly on \"Commission Disbursement ->Approval\" page for user \"Network Admin\".\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT);
        Login.init(t1).login(optCD);
        CommissionDisbursement.init(t1).commissionByDropdowns();
        new Commission_Disbursement_Page2(t1).backButtonClick();
        boolean isPresent = new Commission_Disbursement_Page1(t1).isSubmitButtonDisplayed();
        if (isPresent) {
            t1.pass("Submit button is Displayed.");
            Utils.captureScreen(t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT})
    public void Commission_Disbursement4() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CommisionDisbursement_P_0004",
                "To verify that \"Network Admin\" user can search a user at \"Commission Disbursement\" page by entering valid \"MFS Provider\" and \"MSISDN\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT);
        Login.init(t1).login(optCD);
        CommissionDisbursement.init(t1).negativeTestWithoutConfirm().
                commissionDisburse(chUser);

        new Commission_Disbursement_Page2(t1).verifyUsers(chUser);


    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT})
    public void Commission_Disbursement5() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CommisionDisbursement_P_0005 ",
                "To verify that proper error is displayed when no checkbox is selected on \"Commision Disbursement->Approval\" page but \"approve\" button is clicked");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT);
        Login.init(t1).login(optCD);
        CommissionDisbursement.init(t1).negativeTestWithoutConfirm().commissionByDropdowns();
        new Commission_Disbursement_Page2(t1).clickonApprove();

        Assertion.alertAssertion("select.atleast.onerowto.approve", t1);
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT})
    public void Commission_Disbursement6() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CommisionDisbursement_P_0006 ", "To verify that the Network Admin user can not perform service Commission Disbursement when MSISDN contains Special character  e.g. @#$\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT);
        Login.init(t1).login(optCD);
        chUser2.setMSISDN(Constants.SPECIAL_CHARACTER_CONSTANT);
        CommissionDisbursement.init(t1).negativeTestWithoutConfirm().commissionDisburse(chUser2);
        Assertion.verifyErrorMessageContain("subs.error.msisdnnumeric", "Assert Error Message", t1);
    }


    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT})
    public void Commission_Disbursement7() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CommisionDisbursement_P_0007", "To verify that proper error is displayed when \"MFS Provider\" and \"Domain\" are selected but \"Category\" is NOT selected on Commision Disbursement\" page.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT);
        Login.init(t1).login(optCD);
        new Commission_Disbursement_Page1(t1).navigateToLink().selectProvider().selectDomain().clickOnSubmit();
        Assertion.verifyErrorMessageContain("commission.required.categoryCode", "Assert Error Message", t1);
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT})
    public void Commission_Disbursement8() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CommisionDisbursement_P_0008 ", "To verify that the Network Admin user can not perform service Commission Disbursement when MSISDN contains words e.g. abcd,two etc");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT);

        Login.init(t1).login(optCD);

        chUser2.setMSISDN(Constants.ALPHABET_CONSTANT);

        CommissionDisbursement.init(t1).negativeTestWithoutConfirm().commissionDisburse(chUser2);

        Assertion.verifyErrorMessageContain("subs.error.msisdnnumeric", "Assert Error Message", t1);
    }

}
