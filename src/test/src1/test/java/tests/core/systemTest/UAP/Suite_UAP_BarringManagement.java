package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 8/28/2017.
 */
public class Suite_UAP_BarringManagement extends TestInit {

    private User user;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {
        ExtentTest setupNode = pNode.createNode("SETUP", "Create User/ Fetch User to Bar.");
        user = CommonUserManagement.init(setupNode).getDefaultSubscriber(null);
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP})
    public void barTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0008 : Bar User",
                "To verify that the valid user can bar channel user/subscriber.").
                assignCategory(FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT);

        try {
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BAR_USER);

            if (user == null) {
                t1.fail("Failed to get User with Category: " + Constants.SUBSCRIBER + " from App Data");
            } else {
                ChannelUserManagement.init(t1).barChannelUser(user, Constants.USER_TYPE_SUBS, Constants.BAR_AS_SENDER);
                user.setStatus(Constants.BAR_AS_SENDER);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0})
    public void viewBarTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0010 : View Bar User", "To verify that the valid user can View barred users.").
                assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).loginAsOperatorUserWithRole(Roles.VIEW_BARRED_USER);

            user = CommonUserManagement.init(t1).
                    getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);

            ChannelUserManagement.init(t1).viewBarredUser(user, Constants.USER_TYPE_SUBS);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

    }


    /**
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP})
    public void unBarTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0009 : Un-Bar User", "To verify that the valid user can Unbar users.").
                assignCategory(FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT);

        try {
            Login.init(t1).loginAsOperatorUserWithRole(Roles.VIEW_BARRED_USER);

            user = CommonUserManagement.init(t1).
                    getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);

            ChannelUserManagement.init(t1).unBarChannelUser(user, Constants.USER_TYPE_SUBS);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }
}
