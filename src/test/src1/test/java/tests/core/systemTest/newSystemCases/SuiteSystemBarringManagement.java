package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.TxnResponse;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

/**
 * Created by surya.dhal on 6/11/2018.
 */
public class SuiteSystemBarringManagement extends TestInit {

    private OperatorUser networkAdmin, barredNetworkAdmin;
    private User barredAsBothChUser, barredAsSenderChUser;
    private User barredAsReceiverSubUser, barredAsSenderSubUser, subscriber, chUser;
    private Biller biller;


    /**
     * Method to get Users before run Test
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            networkAdmin = DataFactory.getOperatorUserWithAccess("PTY_BLKL", Constants.NETWORK_ADMIN);
            subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * TEST : NEGATIVE TEST
     * ID : P1_TC_127
     * DESC : Unbar another user if the user is of same or upper level hierarchy
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_127_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_127_2",
                "Barring Management >> Unbar User\n" +
                        "To Verify that a network admin will not be able to Unbar another user if the user is of same or upper level hierarchy.");

        t1.assignCategory(FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.PVG_P1);

        barredNetworkAdmin = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);

        Login.init(t1).login(networkAdmin);
        startNegativeTest();
        CommonUserManagement.init(t1).unBarOperatorUser(barredNetworkAdmin);

        String expectedMessage = MessageReader.getDynamicMessage("bar.operatorUser.samelevel", barredNetworkAdmin.FirstName);
        Assertion.verifyDynamicErrorMessageContain(expectedMessage, "Verify Unbar Failed Message", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * ID : P1_TC_480
     * DESC : Test for CashIN when Channel User Barred as both
     */
    @Test(priority = 9, groups = {FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_480() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_480: ",
                "To verify that \"Channel User\" should not be able to perform \"CashIn\" when barred as both (Sender and receiver).");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        barredAsBothChUser = CommonUserManagement.init(t1).
                getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_BOTH, null);

        t1.info("MSISD of user is :" + barredAsBothChUser.DomainName);

        Login.init(t1).login(barredAsBothChUser);

        startNegativeTest();

        TransactionManagement.init(t1).performCashIn(subscriber, Constants.CASHIN_TRANS_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

        Assertion.verifyErrorMessageContain("initiator.bar.both", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_483
     * DESC : To verify that "Channel User" "barred as sender" by the operator is not able to perform "Balance Enquiry".
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT})
    public void P1_TC_483() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_483",
                "To verify that \"Channel User\" \"barred as sender\" by the operator is not able to perform \"Balance Enquiry\" when service charge or commission is define more than the zero");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);

        barredAsSenderChUser = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);

        ServiceCharge sCharge = new ServiceCharge(Services.BALANCE_ENQUIRY, barredAsSenderSubUser, networkAdmin, null, null, null, null);

        ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(sCharge);

        Map<String, String> map = MobiquityGUIQueries.getNFSC_Amount("BALENQ1632sc");

        Boolean isApplicable = map.get("SERVICE_CHARGE_NONFIN").equals("0");
        if (isApplicable) {
            t1.warning("Service Charge applied is Zero So we are not Performing this Transaction");
            return;
        } else {
            isApplicable = map.get("COMM_TELESCOPIC_CHARGING").equals("0");
            if (isApplicable) {
                t1.warning("Commission  Charge applied is Zero So we are not Performing this Transaction");
                return;
            }
        }


        Login.init(t1).login(barredAsSenderChUser);
        startNegativeTest();
        Enquiries.init(t1).doSelfBalanceEnquiry(barredAsSenderChUser);

        String error = Assertion.checkErrorPresent();
        if (error != null) {
            t1.pass("Balance Enquiry Failed for Barred Channel User");
        } else {
            t1.fail("Barred Channel User is able to Perform Balance Enquiry.");
            Assertion.attachScreenShotAndLogs(t1);
        }

    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_482
     * DESC : To verify that the Customer barred as sender by the operator is not able to perform Balance Enquiry
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT})
    public void P1_TC_482() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_482",
                "To verify that the Customer barred as sender by the operator is not able to perform Balance Enquiry when service charge or commission is define more than the zero");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);

        barredAsSenderSubUser = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);

        ServiceCharge sCharge = new ServiceCharge(Services.BALANCE_ENQUIRY, barredAsSenderSubUser, networkAdmin, null, null, null, null);

        ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(sCharge);

        Map<String, String> map = MobiquityGUIQueries.getNFSC_Amount(sCharge.ServiceChargeName);

        Boolean isApplicable = map.get("SERVICE_CHARGE_NONFIN").equals("0");
        if (isApplicable) {
            t1.warning("Service Charge applied is Zero So we are not Performing this Transaction");
            return;
        } else {
            isApplicable = map.get("COMM_TELESCOPIC_CHARGING").equals("0");
            if (isApplicable) {
                t1.warning("Commission  Charge applied is Zero So we are not Performing this Transaction");
                return;
            }
        }


        if (!TransactionManagement.init(t1).checkLeafUserHasBalance(barredAsSenderSubUser)) {

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_VBLK"));

            SubscriberManagement.init(t1).unBarSubscriber(barredAsSenderSubUser);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(barredAsSenderSubUser);

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_BLKL"));

            SubscriberManagement.init(t1).barSubscriber(barredAsSenderSubUser, Constants.BAR_AS_SENDER);

        }


        TxnResponse response = Transactions.init(t1).subscriberBalanceEnquiry(barredAsSenderSubUser);

        t1.info(response.getMessage());

        if (!response.TxnStatus.equals("200")) {
            t1.pass("User Is Not able to Perform Balance Enquiry.");
        } else {
            t1.fail("User Is able to Perform Balance Enquiry.");
            Assertion.attachScreenShotAndLogs(t1);
        }


    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_481
     * DESC : To verify that the Customer "barred as Receiver" by the operator is able to perform P2P transaction to other "Customer".
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_481() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_481",
                "To verify that the Customer \"Barred as Receiver\" by the operator is able to perform P2P transaction to other \"Customer\".");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        barredAsReceiverSubUser = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_RECIEVER, null);

        ServiceCharge serviceCharge = new ServiceCharge(Services.P2P, barredAsReceiverSubUser, subscriber, null, null, null, null);

        TransferRuleManagement.init(t1).configureTransferRule(serviceCharge);

        if (!TransactionManagement.init(t1).checkLeafUserHasBalance(barredAsReceiverSubUser)) {

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_VBLK"));

            SubscriberManagement.init(t1).unBarSubscriber(barredAsReceiverSubUser);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(barredAsReceiverSubUser);

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_BLKL"));

            SubscriberManagement.init(t1).barSubscriber(barredAsReceiverSubUser, Constants.BAR_AS_RECIEVER);

        }

        String amount = "8";

        SfmResponse response = Transactions.init(t1).performP2PTransfer(barredAsReceiverSubUser, subscriber, amount);

        String txnid = response.TransactionId;

        response.verifyMessage("Transaction.success.message",
                "P2P - Send Money", amount, barredAsReceiverSubUser.MSISDN, subscriber.MSISDN, txnid);

    }

    //TODO - Have to Change in API for View Bill

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_479
     * DESC : Barring Management - View Bill
     * To Verify That Subscriber should not be able to view Bill if subscriber is Barred as Sender or both.
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT, "RUN"})
    public void P1_TC_479_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_479_1",
                "Barring Management - View Bill\n" +
                        "To Verify That Subscriber should be able to view Bill if subscriber is Barred as Sender");
        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);
        //Generate file for Bill upload
        biller = BillerManagement.init(t1).getDefaultBiller();

        String accNo1 = DataFactory.getRandomNumberAsString(5);
        String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1);

        barredAsSenderSubUser = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);

        Login.init(t1).login(networkAdmin);

        BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename1);

        SfmResponse response = Transactions.init(t1).initiateOfflineBillPayment(biller, barredAsSenderSubUser, "5", accNo1);

        t1.info("" + response.getMessage());
        Transactions.init(t1).viewBillBySubscriber(biller, barredAsSenderSubUser);
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT})
    public void P1_TC_479_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_479_2",
                "Barring Management - View Bill\n" +
                        "To Verify That Subscriber should be able to view Bill if subscriber is Barred as Both");
        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);
        //Generate file for Bill upload
        biller = BillerManagement.init(t1).getDefaultBiller();

        String accNo1 = DataFactory.getRandomNumberAsString(5);
        String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1);

        barredAsBothChUser = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);

        Login.init(t1).login(networkAdmin);

        BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename1);

        SfmResponse response = Transactions.init(t1).initiateOfflineBillPayment(biller, barredAsBothChUser, "5", accNo1);

        t1.info("" + response.getMessage());

        Transactions.init(t1).viewBillBySubscriber(biller, barredAsBothChUser);

    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_130
     * DESC : Barring Management - Pay Bill
     * To Verify That Subscriber is not able to do Bill Pay if subscriber is barred as Sender or both.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT})
    public void P1_TC_130_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_130_1",
                "Barring Management - Pay Bill\n" +
                        "To Verify That Subscriber is not able to do Bill Pay if subscriber is barred as Sender");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);

        barredAsSenderSubUser = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);

        biller = BillerManagement.init(t1).getDefaultBiller();

        String accNo1 = DataFactory.getRandomNumberAsString(5);

        startNegativeTest();

        SfmResponse response = Transactions.init(t1).initiateOfflineBillPayment(biller, barredAsSenderSubUser, "10", accNo1);

        response.assertStatus("FAILED");

        response.assertMessage("payer.barred.sender", t1);
    }


    @Test(priority = 5, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT})
    public void P1_TC_130_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_130_2",
                "Barring Management - Pay Bill\n" +
                        "To Verify That Subscriber is not able to do Bill Pay if subscriber is barred as  both.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);

        barredAsSenderSubUser = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_BOTH, null);

        biller = BillerManagement.init(t1).getDefaultBiller();

        String accNo1 = DataFactory.getRandomNumberAsString(5);
        startNegativeTest();
        SfmResponse response = Transactions.init(t1).initiateOfflineBillPayment(biller, barredAsSenderSubUser, "10", accNo1);
        response.assertStatus("FAILED");
        response.assertMessage("initiator.bar.both", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_312
     * DESC : To verify that if customer is barred in the system than user should not able to delete initiate it.
     * *
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT})
    public void P1_TC_312() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_312",
                "To verify that if customer is barred in the system than user should not able to delete initiate it.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);

        barredAsSenderSubUser = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);
        startNegativeTestWithoutConfirm();
        SubscriberManagement.init(t1).deleteSubscriber(barredAsSenderSubUser, DataFactory.getDefaultProvider().ProviderId);

        Assertion.verifyErrorMessageContain("subs.blacklist.msg.oneorbothexist", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_131
     * DESC : To verify that Channel User and Subscriber having same MSISDN,(as defined in System preference- Channel user as subscriber- "Y") if  channel user is Barred, then  "Subscriber" should not get impacted.
     * Also, P2P Transaction  should be successful
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.CRITICAL_CASES_TAG})
    public void P1_TC_131() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_131",
                "To verify that Channel User and Subscriber having same MSISDN,(as defined in System preference- Channel user as subscriber- \"Y\") if  channel user is Barred, then  \"Subscriber\" should not get impacted.\n" +
                        "    Also, P2P Transaction  should be successful");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);
        if (AppConfig.isChannelUserAllowedAsSubscriber) {
            t1.info("System Preference is set as Y");
        } else {
            SystemPreferenceManagement.init(t1).updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");
        }

        barredAsBothChUser = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_BOTH, null);

        User subs = new User(Constants.SUBSCRIBER);
        subs.setMSISDN(barredAsBothChUser.MSISDN);

        ChannelUserManagement.init(t1).createChannelUser(subs);

        Transactions.init(t1).performP2PSendMoneySubs(subs, barredAsBothChUser, "10");

        if (!AppConfig.isChannelUserAllowedAsSubscriber)
            SystemPreferenceManagement.init(t1).updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "N");
    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_128
     * DESC : Barring Management - Upload Bill
     * To verify that bill company should not be able to upload the bill when bill company is barred.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT})
    public void P1_TC_128() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_128",
                "Barring Management - Upload Bill\n" +
                        "To verify that bill company should not be able to upload the bill when bill company is barred.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);

        biller = BillerManagement.init(t1).getBarredBiller(Constants.BAR_AS_BOTH);


        String accNo1 = DataFactory.getRandomNumberAsString(5);
        String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1);

        //Upload bill
        Login.init(t1).login(networkAdmin);
        BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename1);

        //Download LogFile
        BillerManagement.init(t1)
                .downloadBulkRegistrationLogFile();

        String logData = BillerManagement.init(t1).getLogEntry(accNo1);
        Assertion.verifyContains(logData, "Biller Is Barred as Both", "Verify Error Message from LogFile", t1, true);
    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_129
     * DESC : To verify that valid Operator user cannot bar user below its hierarchy, if roles are not assigned to the operator user
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT})
    public void P1_TC_129() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_129",
                "To verify that valid Operator user cannot bar user below its hierarchy, if roles are not assigned to the operator user");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.BARRING_MANAGEMENT);

        WebGroupRole naRole = DataFactory.getWebRoleFromRnR(Constants.NETWORK_ADMIN);

        GroupRoleManagement.init(t1).removeSpecificRole(naRole, new String[]{"PTY_BLKL"});

        Login.init(t1).login(networkAdmin);
        FunctionLibrary.init(t1).clickLink("PARTY_ALL");

        if (Utils.checkElementPresent("PARTY_PTY_BLKL", Constants.FIND_ELEMENT_BY_ID, t1)) {
            t1.fail("Network Admin is able to Perform 'Bar User' as Link is Visible.");
        } else {
            t1.pass("Network Admin is not able to Perform 'Bar User' as Link is not Visible.");
        }
        Utils.captureScreen(t1);
        GroupRoleManagement.init(t1).addSpecificRole(naRole, "PTY_BLKL");
    }


}
