package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 8/9/2017.
 */
public class Suite_UAP_EmailNotification_01 extends TestInit {


    public static OperatorUser optUser;


    /**
     * TEST : TC0053 :E-mail Notification Configuration
     * To verify that the valid user can configure Email-Notification.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void emailNotification() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0053",
                "E-mail Notification Configuration: To verify that the netadmin can configure Email-Notification.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.EMAIL_NOTIFICATION, FunctionalTag.ECONET_UAT_5_0);
        String serviceType = "Suspend Channel User Approval";

        try {
            OperatorUser optUser = DataFactory.getOperatorUserWithAccess("SMS_CR");

            Login.init(t1).login(optUser);

            Login.init(t1).login(optUser);
            Preferences.init(t1).updateEmailNotification(serviceType,
                    MessageReader.getMessage("notification.email.subject.suspend.approval.channeluser", null),
                    null
            );
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
