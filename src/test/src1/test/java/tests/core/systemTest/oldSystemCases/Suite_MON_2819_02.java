package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.InstrumentTCP;
import framework.entity.MobileGroupRole;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.BulkChUserRegistrationPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by rahul.rana on 5/27/2017.
 */
public class Suite_MON_2819_02 extends TestInit {

    private User whs1, whs2;
    private OperatorUser bankApprover, netAdmin;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest t1 = pNode.createNode("SETUP", "Create Test Data Specific to this Suite!");
        try {
            whs1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
            whs2 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            netAdmin = DataFactory.getOperatorUserWithAccess("CHECK_ALL", Constants.NETWORK_ADMIN);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Bulk Register User with Parent Bank Id's associated
     *
     * @throws Exception
     */
    @Test(priority = 2, enabled = true)
    public void TC_09() throws Exception {
        driver = DriverFactory.getDriver();
        ExtentTest t1 = pNode.createNode("MON_2819_TC_09",
                "Bulk Register User with Parent Bank Id's associated");

        // Get the CSV file
        String csvFile = ExcelUtil.getBulkUserRegistrationFile();

        // Get The User to be Bulk Registered
        User usr = new User(Constants.RETAILER, whs1);

        // Write The user Data to csv file
        ChannelUserManagement.init(t1)
                .writeBulkChUserWalletDetailsInCSV(usr, csvFile, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultWallet().WalletId);


        /**
         * Assign Bank Details of Same Parent User
         */
        __writeBulkChUserBankDetailsInCSV(usr, csvFile, whs1.DefaultAccNum, whs1.DefaultCustId, t1);

        // Upload the File
        Login.init(t1).login(netAdmin);
        BulkChUserRegistrationPage page = BulkChUserRegistrationPage.init(t1);
        page.navBulkUserRegistration();
        page.uploadFile(csvFile);
        page.submitCsv();

        // Verify that user Registration is Successful
        String message = driver.findElements(By.className("actionMessage")).get(0).getText();
        Assertion.verifyMessageContain(message, "bulkUpload.channel.label.success",
                "Bulk Channel User Registration with Bank Account Same as Parent", t1);

        String id = message.split("ID :")[1].trim();

        ChannelUserManagement.init(t1)
                .approveBulkRegistration(id);

        // Login as Ch admin and approve associated Banks
        Login.init(t1).login(bankApprover);
        CommonUserManagement.init(t1).
                approveAllAssociatedBanks(usr);
    }

    /**
     * Bulk Register User with ID other than Parent User
     *
     * @throws Exception
     */
    @Test(priority = 3)
    public void TC_10() throws Exception {
        driver = DriverFactory.getDriver();
        ExtentTest t1 = pNode.createNode("MON_2819_TC_10", "Bulk Channel User Registration!");

        // Get the CSV file
        String csvFile = ExcelUtil.getBulkUserRegistrationFile();

        // Get The User to be Bulk Registered
        User usr = new User(Constants.RETAILER, whs1);

        // Write The user Data to csv file
        ChannelUserManagement.init(t1)
                .writeBulkChUserWalletDetailsInCSV(usr, csvFile, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultWallet().WalletId);


        /**
         * Assign Bank Details of Other Channel User
         */
        __writeBulkChUserBankDetailsInCSV(usr, csvFile, whs2.DefaultAccNum, whs2.DefaultCustId, t1);

        // Upload the File
        Login.init(t1).login(netAdmin);
        BulkChUserRegistrationPage page = BulkChUserRegistrationPage.init(t1);
        page.navBulkUserRegistration();
        page.uploadFile(csvFile);
        page.submitCsv();

        // Verify that user Registration is Not allowed
        if (Assertion.isErrorInPage(t1)) {
            t1.pass("Successfully Verified that User with Bank details other than Parent is not allowed");
        } else {
            t1.fail("Failed to Verify that User with Bank details other than Parent is not allowed");
            t1.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
        }
        //TODO - more specific Validation
    }


    /********************************************************************************************************
     * HELPER METHODS
     */


    /**
     * This function will fetch Bank related details and write it to CSV
     *
     * @return - null
     * @author - Dalia Debnath
     */
    public void __writeBulkChUserBankDetailsInCSV(User user, String file, String accNum, String custId, ExtentTest pNode) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("writeBulkChUserBankDetailsInCSV: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();
            String ownerMsisdn, parentMsisdn, l_userStatus, l_primary, l_accStatus;

            // Below Code is written assuming only 2 level Hierarchy
            if (user.ParentUser != null) {
                ownerMsisdn = user.ParentUser.MSISDN;
                parentMsisdn = user.ParentUser.MSISDN;
            } else {
                ownerMsisdn = user.MSISDN;
                parentMsisdn = user.MSISDN;
            }

            String geoZone = dbHandler.dbGetGeoDomainName();
            String geoArea = dbHandler.dbGetChannelGeo(geoZone);
            String dateOfBirth = "28/02/1986";
            String expireDate = "28/02/2024";
            String issueDate = DataFactory.getCurrentDateSlash();
            String webRole = user.WebGroupRole;

            String providerName = DataFactory.getDefaultProvider().ProviderName;
            String bank = defaultProvider.Bank.BankName;

            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, providerName, bank);
            MobileGroupRole mRole = DataFactory.getMobileGroupRole(user.DomainName, user.CategoryName, user.GradeName, providerName, bank);

            String l_tcpID = MobiquityGUIQueries.getIDofTCPprofile(insTcp.ProfileName);

            l_userStatus = "N";
            l_primary = "Y";
            l_accStatus = "A";

            String[] allData = {"Mr", user.LoginId, user.LoginId, user.ExternalCode, "Gurgaon",
                    "Haryana", "India", user.Email, "Quality",
                    user.LoginId, user.MSISDN, "Male", dateOfBirth,
                    user.MSISDN, user.LoginId, l_userStatus, user.MSISDN,
                    "English", user.CategoryCode, ownerMsisdn, parentMsisdn,
                    geoArea, webRole, mRole.ProfileName, user.GradeCode,
                    l_tcpID, l_primary, accNum, custId,
                    "Saving", l_accStatus, "ONLINE", issueDate,
                    expireDate, "India", "India", "120021",
                    "India", "Mahindra Comviva", expireDate,
                    "Address1", user.LoginId, "Address2"};

            pNode.info("Writing User Bank Related info to CSV");
            ExcelUtil.writeDataToBulkUserRegisModifCsv(file, allData, true);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

}
