/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 24-Nov-2017
 *  Purpose: Test Of CashIN
 */

package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.cashInCashOut.CashIn_page1;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by surya.dhal on 11/24/2017.
 */
public class SuiteSystemCashIN extends TestInit {
    private User subs, cashinUser, payee;
    private String expected, alertMsg, firstName, lastName, dob;
    private ServiceCharge cashinService;
    private OperatorUser usrSuspResume;
    private User suspendChUser, barAsSenderChUser, barAsReceiverChUser;
    private User suspendSubUser, barAsReceiverSubUser;
    private String defaultProvider;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        defaultProvider = DataFactory.getDefaultProvider().ProviderName;
        firstName = "BLKL" + DataFactory.getRandomNumber(3);
        lastName = "BLKL" + DataFactory.getRandomNumber(3);
        dob = new DateAndTime().getYear(-20);
        usrSuspResume = DataFactory.getOperatorUserWithAccess("SR_USR", Constants.NETWORK_ADMIN);
        preCondition();
        payee = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
    }


    public boolean preCondition() throws Exception {
        boolean found = false;
        ExtentTest t1 = pNode.createNode("setup", "Setup Specific to this Script");
        try {
            barAsReceiverChUser = CommonUserManagement.init(t1)
                    .getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_RECIEVER, null);
            barAsSenderChUser = CommonUserManagement.init(t1)
                    .getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
            suspendChUser = CommonUserManagement.init(t1)
                    .getSuspendedUser(Constants.WHOLESALER, null);
            barAsReceiverSubUser = CommonUserManagement.init(t1)
                    .getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_RECIEVER, null);
            suspendSubUser = CommonUserManagement.init(t1)
                    .getSuspendedUser(Constants.SUBSCRIBER, null);
            found = true;

        } catch (NullPointerException ne) {
            return false;
        } catch (ArrayIndexOutOfBoundsException ne) {
            return false;
        }
        return found;
    }

    public void checkPre(User payer, User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payer, User payee) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : To verify that the valid user can perform CashIn for All grades.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_001() throws Exception {

        Map<String, User> chUsers = DataFactory.getChannelUserSet(true);
        List<User> payers = new ArrayList<User>();
        List<User> subs = new ArrayList<User>();


        for (User user : chUsers.values()) {
            Boolean flag = false;

            if (!(user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER) || user.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE))) {
                //looping through existing list to not add if same grade code already added in list
                Iterator<User> iterator = payers.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().GradeCode.equals(user.GradeCode)) {
                        flag = true;
                    }
                }
                if (flag == false) {
                    payers.add(user);
                }
            }
            if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                Iterator<User> iterator = subs.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().GradeCode.equals(user.GradeCode)) {
                        flag = true;
                    }
                }
                if (flag == false) {
                    subs.add(user);
                }
            }
        }
        if (subs.size() == 0) {
            pNode.warning("Kindly Enter more than two Subscriber of Each Category in ChannelUser Sheet.");
        }

        for (User payer : payers) {
            for (User payee : subs) {

                ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_001" + getTestcaseid(),
                        "To verify that the " + DataFactory.getGradeName(payer.GradeCode) + " user should able to do Cash In to " + DataFactory.getGradeName(payee.GradeCode) + " from web");

                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

                if (!ConfigInput.is4o14Release) {

                    ServiceCharge cashinService = new ServiceCharge(Services.CASHIN, payer, payee, null, null, null, null);

                    TransferRuleManagement.init(t1)
                            .configureTransferRule(cashinService);

                    if (!ConfigInput.isCoreRelease) {
                        ServiceChargeManagement.init(t1).
                                configureServiceCharge(cashinService);
                    }

                }

                checkPre(payer, payee);
                Login.init(t1).login(payer);

                TransactionManagement.init(t1).performCashIn(payee, Constants.CASHIN_TRANS_AMOUNT, defaultProvider);

                checkPost(payer, payee);
                DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

            }
        }
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test to Verify Back Button Functionality
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0010() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0010 : ",
                "To verify that the \"Channel user\" should able to go back to the initiation page when click on the back button on Cashin confirmation page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(cashinUser);

        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).performCashIn(subs, Constants.CASHIN_TRANS_AMOUNT, defaultProvider);

        CashIn_page1 cashIn_page1 = new CashIn_page1(t1);
        cashIn_page1.clickBackButton();

        boolean isEnabled = cashIn_page1.checkElementDisabled();

        Assertion.verifyEqual(isEnabled, true, "Verify Back Button", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIn to Invalid MSISDN (Alphabets)
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0011() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0011 : ",
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web with invalid MSISDN.(Alphabets)");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(cashinUser);
        CashIn_page1 page1 = new CashIn_page1(t1);
        page1.navigateToLink();
        page1.subscriberMSISDNSetText(Constants.ALPHABET_CONSTANT);
        page1.clickSubmit();

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("ci.error.onlynum", null);
        Assertion.verifyContains(alertMsg, expected, "Verify Alert Text", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN to Invalid MSISDN (Special character)
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0012() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0012 : ",
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web with invalid MSISDN.(Special Character)");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(cashinUser);
        CashIn_page1 page1 = new CashIn_page1(t1);
        page1.navigateToLink();
        page1.subscriberMSISDNSetText(Constants.SPECIAL_CHARACTER_CONSTANT);
        page1.clickSubmit();

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("ci.error.onlynum", null);
        Assertion.verifyContains(alertMsg, expected, "Verify Alert Text", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for ChasIN when MSISDN left blank
     *
     * @return
     */

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0013() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0013 : ",
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web When MSISDN left Blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(cashinUser);
        CashIn_page1 page1 = new CashIn_page1(t1);
        page1.navigateToLink();
        page1.subscriberMSISDNSetText(Constants.SPACE_CONSTANT);
        page1.clickSubmit();

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("ci.error.msisdnvalue", null);
        Assertion.verifyContains(alertMsg, expected, "Verify Alert Text", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CahIN when Invalid Amount (Alphabet) is entered
     *
     * @return
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0014() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0014 : ",
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web with invalid Amount.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(cashinUser);

        TransactionManagement.init(t1).cashInNegative(subs, Constants.ALPHABET_CONSTANT, Constants.paymentID);
        Assertion.verifyErrorMessageContain("agentcashout.error.improperAmount", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when Amount left blank
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0015() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0015 : " + getTestcaseid(),
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web when amount left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(cashinUser);

        TransactionManagement.init(t1).cashInNegative(subs, Constants.SPACE_CONSTANT, Constants.paymentID);
        Assertion.verifyErrorMessageContain("agentcashout.error.Amount", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when Payment ID left blank
     * Below test cases is set to enabled = false as
     * Payment ID is not a mandatory field in setup
     * <p>
     * Don't enabled it unless required
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN}, enabled = false)
    public void SYS_TC_CashIN_S_0016() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0016 : ",
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web when Payment ID left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(cashinUser);

        TransactionManagement.init(t1).cashInNegative(subs, Constants.CASHIN_TRANS_AMOUNT, Constants.SPACE_CONSTANT);
        Assertion.verifyErrorMessageContain("cashin.error.improperReferenceNumber", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when Channel User have insufficient balance
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0017() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0017 : " + getTestcaseid(),
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web when \"Channel user\" have Insufficient balance.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(barAsReceiverChUser);

        startNegativeTest();
        TransactionManagement.init(t1).performCashIn(subs, Constants.CASHIN_TRANS_AMOUNT, defaultProvider);
        Assertion.verifyErrorMessageContain("channeluser.insufficient.balance", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when Subscriber Barred as Receiver
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0018() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0018 : " + getTestcaseid(),
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web When Subscriber is barred as  receiver.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        Login.init(t1).login(cashinUser);

        startNegativeTest();
        TransactionManagement.init(t1).performCashIn(barAsReceiverSubUser, Constants.CASHIN_TRANS_AMOUNT, defaultProvider);

        Assertion.verifyErrorMessageContain("payee.barred.reciever", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when Channel User Barred as Sender
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0019() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0019 : " + getTestcaseid(),
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web When \"Channel User\" is barred as  sender.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        Login.init(t1).login(barAsSenderChUser);

        startNegativeTest();
        TransactionManagement.init(t1).performCashIn(payee, Constants.CASHIN_TRANS_AMOUNT, defaultProvider);

        Assertion.verifyErrorMessageContain("payer.barred.sender", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when amount less than defined service charge
     */
    //TODO - Parked Because it will affect Service Charge
    @Test(enabled = false, priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0020() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0020 : " + getTestcaseid(),
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web When amount is less than minimum transfer amount defined in service charge.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);


        cashinService = new ServiceCharge(Services.CASHIN, cashinUser, subs, null, null, null, null);
        if (!ConfigInput.is4o14Release) {
            ServiceChargeManagement.init(t1).
                    configureServiceCharge(cashinService);
        }

        BigDecimal minValue = MobiquityGUIQueries.getMinTransferValue(cashinService.ServiceChargeName);
        minValue = minValue.divide(new BigDecimal("2"));

        Login.init(t1).login(cashinUser);

        startNegativeTest();
        TransactionManagement.init(t1).performCashIn(subs, minValue.toString(), defaultProvider);
        Assertion.verifyErrorMessageContain("00409", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when amount is more than defined service charge
     */
    //TODO - Have to Modify According to 4.9
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0021() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0021 : " + getTestcaseid(),
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web When amount is more than maximum transfer amount defined in service charge.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);


        cashinService = new ServiceCharge(Services.CASHIN, cashinUser, subs, null, null, null, null);

        if (!(ConfigInput.is4o14Release || ConfigInput.isCoreRelease)) {
            ServiceChargeManagement.init(t1).
                    configureServiceCharge(cashinService);

            BigDecimal maxValue = MobiquityGUIQueries.getMaxTransferValue(cashinService.ServiceChargeName);
            maxValue = maxValue.add(BigDecimal.valueOf(10000));

            Login.init(t1).login(cashinUser);

            startNegativeTest();
            TransactionManagement.init(t1).performCashIn(subs, maxValue.toString(), defaultProvider);
            Assertion.verifyErrorMessageContain("00410", "Verify Error Message", t1);
        } else {
            t1.skip("Skipping the case as Cashin Service charge creation is not applicable in this version.");
        }


    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when Subscriber is suspended
     */
    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0022() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0022 : " + getTestcaseid(),
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web When \"Subscriber\" is \"suspended\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        Login.init(t1).login(cashinUser);

        startNegativeTest();
        TransactionManagement.init(t1).performCashIn(suspendSubUser, Constants.CASHIN_TRANS_AMOUNT, defaultProvider);

        Assertion.verifyErrorMessageContain("reciever.suspended", "Receiver is Suspended", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test for CashIN when Channel User is suspended
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CashIN_S_0023() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CashIN_S_0023 : " + getTestcaseid(),
                "To verify that the Channel user should not be able to do \"Cash In\" to Subscriber from web When \"Channel User\" is suspended.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN);

        cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        Login.init(t1).tryLogin(suspendChUser.LoginId, suspendChUser.Password);

        Login.resetLoginStatus();

        Assertion.verifyErrorMessageContain("payer.suspended", "Verify Error Message", t1);
    }

}
