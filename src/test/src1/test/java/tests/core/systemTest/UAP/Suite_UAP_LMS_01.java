package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.loyaltyManagement.LMS;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 8/14/2017.
 */
public class Suite_UAP_LMS_01 extends TestInit {

    public String PromoName;
    public String fromDate;
    public String toDate;
    public String Type;
    private OperatorUser proCreator, proModifier;
    private ExtentTest tearDown;
    private boolean isCreated = false;
    private SuperAdmin supadmin;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        PromoName = "LMS" + DataFactory.getRandomNumber(5);
        String dateFormat = "dd-MM-yyyy";
        String time = " 02:30:00 PM";
        fromDate = new DateAndTime().getDate(dateFormat, 3) + time;
        toDate = new DateAndTime().getDate(dateFormat, 21) + time;
        proCreator = DataFactory.getOperatorUserWithAccess("C_ROLE");
        proModifier = DataFactory.getOperatorUsersWithAccess("MC_ROLE").get(0);
        supadmin = DataFactory.getSuperAdminWithAccess("AC_ROLE");
        tearDown = pNode.createNode("Tear Down");
        isCreated = false;
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0091 : CREATE LMS PROFILE ", "To verify that the valid user can create promotion in system.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        Login.init(t1).login(proCreator);
        isCreated = LMS.init(t1).addLMSpromotion(PromoName, fromDate, toDate, Services.INTERNAT_SEND_MONEY);
        Login.init(tearDown).loginAsSuperAdmin("AC_ROLE");
        LMS.init(tearDown).approveLMS(PromoName);
    }

    //Already Automated in --> tests.core.econet5_0.Suite_RewardsAndPromotion
    @Test(enabled = false, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP})
    public void Test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0368 : Loyalty Management ", "To verify that the valid user can view promotion in system.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        if (isCreated) {
            Login.init(t1).login(proModifier);
            LMS.init(t1).ViewLMS(PromoName, fromDate, toDate);
        } else {
            t1.skip("Test cases skipp because profile is not created");
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PROMOTION, FunctionalTag.PVG_UAP})
    public void Test_03() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1250", "To verify that network admin is able " +
                "to create promotion successfully.");

        ExtentTest t2 = pNode.createNode("TC0094", "To verify that the valid user can modify promotion in system.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(proCreator);
            LMS.init(t1).createPromotion(PromoName, Constants.WHS,
                    Constants.MERCHANT, Services.C2C);

            Login.init(t1).loginAsSuperAdmin("AC_ROLE");
            LMS.init(t1).approveLMS(PromoName);

            Login.init(t2).login(proModifier);
            LMS.init(t2).modifyLMS(PromoName);
            Login.init(t2).loginAsSuperAdmin(supadmin);
            LMS.init(t2).approveLMS(PromoName);
            //}else {
            //    t1.skip("Test cases skipp because profile is not created");
            //}
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP}, dependsOnMethods = "Test_01")
    public void Test_04() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0097\n : Loyalty Management ", "To verify that the valid user can add/modify/deleteLMSProfile award in the system.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS);
        proModifier = DataFactory.getOperatorUsersWithAccess("MC_ROLE").get(0);
        Login.init(t1).login(proModifier);
        LMS.init(t1).deleteLMSProfile(PromoName);
        Login.init(tearDown).loginAsSuperAdmin("AC_ROLE");
        LMS.init(tearDown).approveLMS(PromoName);
    }


}
