package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.TxnResponse;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.blackListManagement.BlackListManagement;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ApprovalSubscriber_Page1;
import framework.util.common.Assertion;
import framework.util.common.CMDExecutor;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by navin.pramanik on 08-Jun-18.
 */
public class SuiteSystemSubscriberManagement01 extends TestInit {

    private SuperAdmin saSysPref;
    private User chAddSubs, chApproveSubs, modifyAccessUsr, modifyAppAccessUsr, chUserDelSubsByAgent;
    private String DEFAULT_VALUE_FAIL_FROZEN_TXN_ON_DEL;
    private String DEFAULT_VALUE_FAIL_OTF_ON_DEL;
    private ExtentTest setup;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
        chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");
        modifyAccessUsr = DataFactory.getChannelUserWithAccess("SUBSMOD");
        modifyAppAccessUsr = DataFactory.getChannelUserWithAccess("SUBSMODAP");
        saSysPref = DataFactory.getSuperAdminWithAccess("PREF001");
        chUserDelSubsByAgent = DataFactory.getChannelUserWithAccess("SUBSDELBYAGT");
        setup = pNode.createNode("Setup", "Setup for Subscriber Management.");
        DEFAULT_VALUE_FAIL_FROZEN_TXN_ON_DEL = MobiquityGUIQueries.fetchDefaultValueOfPreference("FAIL_FROZEN_TXN_ON_DEL");
        DEFAULT_VALUE_FAIL_OTF_ON_DEL = MobiquityGUIQueries.fetchDefaultValueOfPreference("FAIL_OTF_ON_DEL");
    }


    /**
     * To get the Already assigned MSISDN from Users table
     *
     * @param userType
     * @return
     */
    private String getAlreadyAssignedMsisdnFromUsersForSubs(String userType) {
        List<String> allSubsMsisdn = MobiquityGUIQueries.getAllActiveMsisdnFromMtxParty();
        List<String> allUsersMsisdn = MobiquityGUIQueries.getAllActiveMsisdnFromUsers(userType);

        for (int i = 0; i < allUsersMsisdn.size(); i++) {
            if (!allSubsMsisdn.contains(allUsersMsisdn.get(i))) {
                return allUsersMsisdn.get(i);
            }
        }
        return null;
    }

    /**
     * For Logging In and Modify Preference
     *
     * @param prefCode
     * @param defValue
     * @param node
     * @throws Exception
     */
    private void doLoginAndModifyPreference(String prefCode, String defValue, ExtentTest node) throws Exception {
        Login.init(node).loginAsSuperAdmin(saSysPref);
        Preferences.init(node).modifySystemPreferences(prefCode, defValue);
    }


    private void initAndApproveSubs(User subs, ExtentTest t1) throws Exception {
        Login.init(t1).login(chAddSubs);
        SubscriberManagement.init(t1).addSubscriber(subs, false);
        Login.init(t1).login(chApproveSubs);
        SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);
    }

    /**
     * P1_TC_364 : CheckSubsAccClosure
     * To verify that when FAIL_FROZEN_TXN_ON_DEL preference is set as false then account closure should be successful
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void P1_TC_364() throws Exception {

        ExtentTest test = pNode.createNode("P1_TC_364 : CheckSubsAccClosure",
                "To verify that when FAIL_FROZEN_TXN_ON_DEL preference is set as false then account closure should be successful")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        User chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        OperatorUser initTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRECTION");

        TransactionManagement.init(test).makeSureChannelUserHasBalance(chUsr);

        User sub = new User(Constants.SUBSCRIBER);
        SubscriberManagement.init(test).createSubscriberDefaultMapping(sub, true, false);

        ServiceCharge cashinService = new ServiceCharge(Services.CASHIN, chUsr, sub, null, null, null, null);

        TransferRuleManagement.init(test)
                .configureTransferRule(cashinService);

        String transactionId = Transactions.init(test).initiateCashIn(sub, chUsr, new BigDecimal("5")).TransactionId;

        //transaction correction initiation is required to put the amount to frozen
        Transactions.init(test)
                .initiateTransactionReversalByOperator(initTxnCorrection, transactionId,
                        Constants.NORMAL_WALLET, Services.CASHIN,
                        "false", "false");

        SystemPreferenceManagement.init(test)
                .updateSystemPreference("FAIL_FROZEN_TXN_ON_DEL", "False");

        SystemPreferenceManagement.init(test)
                .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "N");

        OperatorUser opt = new OperatorUser(Constants.NETWORK_ADMIN);
        ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE, sub, opt, null, null, null, null);
        ServiceChargeManagement.init(test)
                .configureServiceCharge(sChargeAccClose);

        Login.init(test).loginAsChannelUserWithRole(Roles.DELETE_SUBSCRIBER);
        SubscriberManagement.init(test)
                .deleteSubscriber(sub, (DataFactory.getDefaultProvider()).ProviderId);
    }


    /**
     * TEST : P1_TC_365 : FAIL_FROZEN_TXN_ON_DEL(Account Closure) Preference as True
     * DESC : To verify that when FAIL_FROZEN_TXN_ON_DEL Preference is set as true then Account closure should not be successful
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_365() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_365 : FAIL_FROZEN_TXN_ON_DEL(Account Closure) Preference as True",
                "To verify that when FAIL_FROZEN_TXN_ON_DEL Preference is set as true then Account closure should not be successful.")

                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT,
                        FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        User subs = new User(Constants.SUBSCRIBER);

        boolean isPrefChanged = false;

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

        Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(subs.MSISDN, subs.CategoryCode), "Y", "Verify User Status in DB", t1);
        try {
            if (DEFAULT_VALUE_FAIL_FROZEN_TXN_ON_DEL.equalsIgnoreCase("true")) {
                t1.info("FAIL_FROZEN_TXN_ON_DEL Preference is set as TRUE");
            } else {
                Login.init(t1).loginAsSuperAdmin(saSysPref);
                Preferences.init(t1).modifySystemPreferences("FAIL_FROZEN_TXN_ON_DEL", "true");
                isPrefChanged = true;
            }

            User cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(cashinUser);

            ServiceCharge cashinService = new ServiceCharge(Services.CASHIN, cashinUser, subs, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(cashinService);

            Login.init(t1).login(cashinUser);

            String transID = TransactionManagement.init(t1).
                    performCashIn(subs, Constants.MAX_CASHIN_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

            OperatorUser txnCorrUsr = DataFactory.getOperatorUserWithAccess("TXN_CORRECTION", Constants.NETWORK_ADMIN);

            Login.init(t1).login(txnCorrUsr);

            TransactionCorrection.init(t1).initiateTxnCorrection(transID, false, false);

            Login.init(t1).login(chUserDelSubsByAgent);

            startNegativeTest();

            ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, subs, chUserDelSubsByAgent, null, null, null, null);
            ServiceChargeManagement.init(pNode)
                    .configureServiceCharge(sChargeAccClose);

            SubscriberManagement.init(t1).deleteSubscriberByAgent(subs, DataFactory.getDefaultProvider().ProviderId);

            Markup m = MarkupHelper.createLabel("Frozen Balance of User :" + MobiquityGUIQueries.getFrozenBalanceOfUser(subs), ExtentColor.GREEN);
            t1.info(m);

            stopNegativeTest();

            Assertion.verifyErrorMessageContain("accClosure.frozen.balance.error", "Pending Transaction check", t1);

        } finally {
            if (isPrefChanged) {
                Login.init(t1).loginAsSuperAdmin(saSysPref);
                Preferences.init(t1).modifySystemPreferences("FAIL_FROZEN_TXN_ON_DEL", DEFAULT_VALUE_FAIL_FROZEN_TXN_ON_DEL);
            }
        }
    }

    /**
     * TODO : Complete the Test Case
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.CRITICAL_CASES_TAG})
    public void P1_TC_366() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_366 : CheckSubsAccClosure",
                "To verify that when FAIL_OTF_ON_DEL preference is set as false then account closure should be successful")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(chAddSubs);

        User subs = new User(Constants.SUBSCRIBER);

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

        try {
            if (!DEFAULT_VALUE_FAIL_OTF_ON_DEL.equalsIgnoreCase("false")) {
                Login.init(t1).loginAsSuperAdmin(saSysPref);
                Preferences.init(t1).modifySystemPreferences("FAIL_OTF_ON_DEL", "false");
            }

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal("100"));

            User nonRegSubs = new User(Constants.SUBSCRIBER);

            SfmResponse sfmResponse = Transactions.init(t1).p2pNonRegTransaction(subs, nonRegSubs, new BigDecimal("10"));

            t1.info("Message of P2P non reg :" + sfmResponse.getMessage());
            Assertion.verifyEqual(sfmResponse.Status, "SUCCEEDED", "Verify P2P Status", t1);

            Login.init(t1).login(chUserDelSubsByAgent);

            String tid = SubscriberManagement.init(t1).deleteSubscriberByAgent(subs, DataFactory.getDefaultProvider().ProviderId);

            Transactions.init(t1).accountClosurebyAgentConfirmation(subs.MSISDN, tid);

        } finally {
            Login.init(t1).loginAsSuperAdmin(saSysPref);
            Preferences.init(t1).modifySystemPreferences("FAIL_OTF_ON_DEL", DEFAULT_VALUE_FAIL_OTF_ON_DEL);
        }
    }


    /**
     * TODO : Complete the Test Case
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.CRITICAL_CASES_TAG})
    public void P1_TC_367() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_365 : CheckSubsAccClosure",
                "To verify that when FAIL_OTF_ON_DEL Preference is set as true then account closure should not be successful")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(chAddSubs);

        User subs = new User(Constants.SUBSCRIBER);

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

        try {
            if (!DEFAULT_VALUE_FAIL_OTF_ON_DEL.equalsIgnoreCase("true")) {
                Login.init(t1).loginAsSuperAdmin(saSysPref);
                Preferences.init(t1).modifySystemPreferences("FAIL_OTF_ON_DEL", "true");
            }

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal("10"));

            User nonRegSubs = new User(Constants.SUBSCRIBER);

            SfmResponse sfmResponse = Transactions.init(t1).p2pNonRegTransaction(subs, nonRegSubs, new BigDecimal("10"));

            t1.info("Message of P2P non reg :" + sfmResponse.getMessage());
            Assertion.verifyEqual(sfmResponse.Status, "SUCCEEDED", "Verify P2P Status", t1);

            Login.init(t1).login(chUserDelSubsByAgent);

            startNegativeTest();

            SubscriberManagement.init(t1).deleteSubscriberByAgent(subs, DataFactory.getDefaultProvider().ProviderId);

            stopNegativeTest();

            Assertion.verifyErrorMessageContain("accClosure.otf.pending", "Pending On The Fly check", t1);

        } finally {
            Login.init(t1).loginAsSuperAdmin(saSysPref);
            Preferences.init(t1).modifySystemPreferences("FAIL_OTF_ON_DEL", DEFAULT_VALUE_FAIL_OTF_ON_DEL);
        }
    }


    /**
     * P1_TC_485
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_485() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_485",
                "To verify that channel user (Wholesaler/Retailer) should not be able to initiate Subscriber creation if its full name and date of birth is present in blacklist.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        OperatorUser custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        User blklSubs = new User(Constants.SUBSCRIBER);

        blklSubs.FirstName = "FName" + DateAndTime.getTimeStamp();
        blklSubs.LastName = "LName" + DateAndTime.getTimeStamp();

        if (custblkl == null) {
            t1.error("User not found with Role Code : 'BLK_ABL' . Please check Operator User sheet");
            Assert.fail("User not found with Role Code : 'BLK_ABL' . Please check Operator User sheet");
        } else {
            Login.init(t1).login(custblkl);
            BlackListManagement.init(t1).doCustomerBlacklist(blklSubs.FirstName, blklSubs.LastName, blklSubs.DateOfBirth);
        }

        Login.init(t1).login(chAddSubs);
        startNegativeTest();
        SubscriberManagement.init(t1).addInitiateSubscriber(blklSubs);

        Assertion.verifyDynamicErrorMessageContain(MessageReader.getDynamicMessage("blacklist.subs.blacklisted.added", blklSubs.FirstName, blklSubs.LastName), "Check Subs Addition", t1);
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_487() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_487 : RejectSubsModification",
                "To verify that Channel user (Wholesaler/Retailer) should be able to reject modification request of a subscriber.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        SubscriberManagement subsMgmt = SubscriberManagement.init(t1);

        User subs = CommonUserManagement.init(t1).getDefaultSubscriber(null);

        subsMgmt.modifySubscriber(modifyAccessUsr, subs);

        Login.init(t1).login(modifyAppAccessUsr);

        subsMgmt.rejectSubsModification(subs);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_486() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_486",
                "To verify that valid user should be able to approve the bank association with customer when more than one bank is associated.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(chAddSubs);

        int allBankSize = DataFactory.getAllBank().size();

        if (allBankSize >= 2) {
            User subscriber = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subscriber, true, true);
        } else {
            t1.skip("More than 2 Banks required to test this Service.");
        }
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_488() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_488 : SubsDeleteInitiate",
                "To verify that Channel user (Wholesaler/Retailer) should be able to initiate deletion of an existing subscriber.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subsNew = new User(Constants.SUBSCRIBER);

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subsNew, true, false);

        TransactionManagement.init(t1).makeSureLeafUserHasBalance(subsNew);

        ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, subsNew, chUserDelSubsByAgent, null, null, null, null);
        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sChargeAccClose);

        Login.init(t1).login(chUserDelSubsByAgent);

        SubscriberManagement.init(t1).deleteSubscriberByAgent(subsNew, DataFactory.getDefaultProvider().ProviderId);
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_035() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_035 : CheckMessageEncrypt",
                "Add subscriber user>>To verify that while creating subscriber, the Pin in secured form in message sent logs. ")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subsNew = new User(Constants.SUBSCRIBER);

        Login.init(t1).login(chAddSubs);

        String beforeCreationTime = CMDExecutor.getLinuxServerDateTime();

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subsNew, true, false);

        String afterCreationTime = CMDExecutor.getLinuxServerDateTime();

        if (subsNew.isPwdReset.equalsIgnoreCase("Y")) {
            String msg = CMDExecutor.getMessageFromMessageSentLog(beforeCreationTime, afterCreationTime, subsNew.MSISDN);
            t1.info("Sent Message Logs :" + msg);
            String msgSplit = msg.split("mPin ")[1].split("\\s")[0];

            Assertion.verifyEqual(msgSplit.toUpperCase(), "XXXX", "Check Encryption of Pin in Logs", t1);

        }
    }


    /**
     * To verify that customer is not able to perform the service if its wallet/bank is in delete initiated state.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_132() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_132 : SubsDeleteInitiateTxnCheck",
                "To verify that customer is not able to perform the service if its wallet/bank is in delete initiated state.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        User subs2 = CommonUserManagement.init(t1).getTempDeleteInitiatedSubscriber(null);

        ServiceCharge p2pSendMoney = new ServiceCharge(Services.P2P, subs, subs2, null, null, null, null);
        TransferRuleManagement.init(t1).configureTransferRule(p2pSendMoney);

        if (!(ConfigInput.isCoreRelease || ConfigInput.is4o9Release)) {
            ServiceChargeManagement.init(t1).configureServiceCharge(p2pSendMoney);
        }

        startNegativeTest();

        TxnResponse response = Transactions.init(t1).performP2PSendMoneySubs(subs, subs2, "10");

        response.verifyStatus("88888");

        Assertion.verifyContains(response.Message, MobiquityGUIQueries.getMessageFromSysMessages("88888", "1"), "Delete Initited Subs Txn", t1);
    }


    /**
     * P1_TC_489
     * To verify that valid user should be able to add the Subscriber if MSISDN
     * is already assigned to another Operator User who is registered in the system.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_489() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_489",
                "To verify that valid user should be able to add the Subscriber if MSISDN is already assigned to another Operator User who is registered in the system.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subsNew = new User(Constants.SUBSCRIBER);

        Login.init(t1).login(chAddSubs);

        //Assign subs msisdn with already assigned operator msisdn
        subsNew.MSISDN = getAlreadyAssignedMsisdnFromUsersForSubs("OPERATOR");

        t1.info(MarkupHelper.createLabel("MSISDN of Operator in System is :" + subsNew.MSISDN, ExtentColor.ORANGE));

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subsNew, true, false);
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_138() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_138",
                "To verify that Channel user (Wholesaler/Retailer) should not be able to delete initiate a subscriber " +
                        "who is suspended in the system and having balance in the wallet")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subsNew = CommonUserManagement.init(t1).
                getDefaultSubscriber(null);

        TransactionManagement.init(t1).makeSureLeafUserHasBalance(subsNew, new BigDecimal("100"));

        OperatorUser usrSuspResume = DataFactory.getOperatorUserWithAccess("SR_USR", Constants.NETWORK_ADMIN);

        Login.init(t1).login(usrSuspResume);

        SubscriberManagement.init(t1).suspendSubscriber(subsNew);

        Login.init(t1).login(chUserDelSubsByAgent);
        //For starting Negative Test
        startNegativeTestWithoutConfirm();

        SubscriberManagement.init(t1).deleteSubscriberByAgent(subsNew, DataFactory.getDefaultProvider().ProviderId);

        Assertion.verifyErrorMessageContain("subs.message.notactive", "Delete Suspended Subscriber", t1);
        //For Tear Down
        Login.init(t1).login(usrSuspResume);

        SubscriberManagement.init(t1).resumeSubscriber(subsNew, Constants.USER_TYPE_SUBS);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_492() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_492",
                "To Verify that Channel user check availability of Mobile number while registering a subscriber(For already registered mobile number in Mobiquity System)")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subsNew = new User(Constants.SUBSCRIBER);

        subsNew.MSISDN = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER).MSISDN;

        Login.init(t1).login(chAddSubs);

        AddSubscriber_Page1 addSubscriberPage1 = new AddSubscriber_Page1(t1);

        addSubscriberPage1.navAddSubscriber();
        addSubscriberPage1.setMobileNumber(subsNew.MSISDN);
        addSubscriberPage1.clickOnCheckMsisdnAvailabilityButton();

        Assertion.verifyContains(addSubscriberPage1.getAvailableMsisdnText(),
                MessageReader.getMessage("systemparty.validation.partyAccessidExists", null), "Checking Availability of MSISDN", t1);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_484() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_484",
                "To verify that Channel user (Wholesaler/Retailer) should be able to approve the initiated multiple subscriber at one instance of approval.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        List<String> subsList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Login.init(t1).login(chAddSubs);

            User subsNew1 = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(t1).addSubscriber(subsNew1);

            subsList.add(subsNew1.MSISDN);
        }

        Login.init(t1).login(chApproveSubs);

        ApprovalSubscriber_Page1 approvalSubscriberPage1 = new ApprovalSubscriber_Page1(t1);

        approvalSubscriberPage1.navSubscriberApproval();
        approvalSubscriberPage1.selectUserstoApprove(subsList);
        approvalSubscriberPage1.setReason("Automation");
        approvalSubscriberPage1.clickApprove();
        approvalSubscriberPage1.confirmApproval();

        List<String> actualMessages = Assertion.getAllActionMessages();

        String expected = MessageReader.getMessage("subs.add.message.success", null);

        for (int i = 0; i < subsList.size(); i++) {
            Assertion.verifyContains(actualMessages.get(i), expected, "Approve Subscriber", t1);
        }

    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_136_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_136_2 ",
                "To Verify that network admin should able to registered Subscribers to get particular Biller Company's Notifications.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        OperatorUser billAssocUser = DataFactory.getOperatorUserWithAccess("SUBBILREG");

        if (billAssocUser == null) {
            t1.fail("Operator User not found with Access : SUBBILREG");
        } else {
            User subsNew = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subsNew, true, false);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subsNew, new BigDecimal(Constants.MIN_CASHIN_AMOUNT));

            Biller biller = BillerManagement.init(t1)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.addBillForCustomer(subsNew.MSISDN, DataFactory.getRandomNumberAsString(5));

            CustomerBill bill = new CustomerBill(biller, subsNew.MSISDN, DataFactory.getRandomNumberAsString(5), "Test Bill");

            Login.init(t1).login(billAssocUser);

            BillerManagement.init(t1).initiateSubscriberBillerAssociationWithoutBill(biller, bill);
        }
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.CRITICAL_CASES_TAG})
    public void P1_TC_143() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_143 ",
                "To verify that system cannot register a subscriber through 2 step registration when subscriber rejects the Push message.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subsNew = new User(Constants.SUBSCRIBER);

        startNegativeTest();

        if (AppConfig.isTwoStepRegistrationForSubs) {

            initAndApproveSubs(subsNew, t1);

            Transactions.init(t1)
                    .performSubscriberAcquisition(subsNew, "1").verifyStatus("666033").verifyMessageFromDB("666033", "1");
        } else {

            String displayAllowedFromGUI = MobiquityGUIQueries.fetchDisplayAllowedOnGUI("TWO_STEP_SUBS_REG");
            t1.info("Changing Preference as TWO_STEP_SUBS_REG is set as FALSE");

            if (displayAllowedFromGUI.equalsIgnoreCase("Y")) {
                doLoginAndModifyPreference("TWO_STEP_SUBS_REG", "TRUE", t1);

                initAndApproveSubs(subsNew, t1);

                Transactions.init(t1)
                        .performSubscriberAcquisition(subsNew, "1").verifyStatus("666033").verifyMessageFromDB("666033", "1");

                doLoginAndModifyPreference("TWO_STEP_SUBS_REG", "FALSE", t1);

            } else {
                t1.skip("Skipping the test case as 'TWO_STEP_SUBS_REG' is not allowed and Preference DisplayAllowed is Set as 'N'");
            }
        }
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.CRITICAL_CASES_TAG})
    public void P1_TC_494() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_494 ",
                "System should successfully send the Push and subscriber accepts the message." +
                        "Then subscriber is registered successfully and notification is sent to the request initiator as 'Subscriber with MSISDN has been registered in the system.'")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subsNew = new User(Constants.SUBSCRIBER);

        if (AppConfig.isTwoStepRegistrationForSubs) {

            initAndApproveSubs(subsNew, t1);

            Transactions.init(t1)
                    .performSubscriberAcquisition(subsNew, Constants.STATUS_ACCEPT).verifyMessageFromDB("666055", "1");

        } else {

            String displayAllowedFromGUI = MobiquityGUIQueries.fetchDisplayAllowedOnGUI("TWO_STEP_SUBS_REG");
            t1.info("Changing Preference as TWO_STEP_SUBS_REG is set as FALSE");

            if (displayAllowedFromGUI.equalsIgnoreCase("Y")) {
                doLoginAndModifyPreference("TWO_STEP_SUBS_REG", "TRUE", t1);

                initAndApproveSubs(subsNew, t1);

                Transactions.init(t1)
                        .performSubscriberAcquisition(subsNew, Constants.STATUS_ACCEPT).verifyMessageFromDB("666055", "1");

                doLoginAndModifyPreference("TWO_STEP_SUBS_REG", "FALSE", t1);

            } else {
                t1.skip("Skipping the test case as 'TWO_STEP_SUBS_REG' is not allowed and Preference DisplayAllowed is Set as 'N'");
            }
        }
    }


    /**
     * DESC : To verify that subscriber registered should have random/generic(can be configured) PIN generated.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_495() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("P1_TC_495 ",
                "To verify that subscriber registered should have random/generic(can be configured) PIN generated.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User subsNew = new User(Constants.SUBSCRIBER);
        Login.init(t1).login(chAddSubs);

        SubscriberManagement.init(t1).addSubscriber(subsNew, false);

        Login.init(t1).login(chApproveSubs);
        SubscriberManagement.init(t1).addInitiatedApprovalSubs(subsNew);

        //User subsNew = DataFactory.getUserUsingMsisdn("7711111936");

        String message = MobiquityGUIQueries
                .getMobiquityUserMessage(subsNew.MSISDN, "ASC");

        t1.info("Message in DB is ===> " + message);

        String[] pi = message.split("Pin ");
        String pinFromMessage = pi[1].trim().split("\\s")[0];


        if (AppConfig.isRandomPinAllowed) {
            Assertion.verifyNotEqual(pinFromMessage, AppConfig.defaultPin, "Verify Subscriber Default PIN", t1);
        } else {
            Assertion.verifyEqual(pinFromMessage, AppConfig.defaultPin, "Verify Subscriber Default PIN", t1);
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_383() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_383 : CheckSubsAccClosure",
                "To verify that subscriber can confirm the account closure request generated by the channel user, using his 4 digit MPIN and TPIN")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(chAddSubs);

        User subs = new User(Constants.SUBSCRIBER);

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

        TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal("10"));

        Login.resetLoginStatus();

        Login.init(t1).login(chUserDelSubsByAgent);

        String tid = SubscriberManagement.init(t1).deleteSubscriberByAgent(subs, DataFactory.getDefaultProvider().ProviderId);

        Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(subs.MSISDN, subs.CategoryCode), "DI", "Verify User Delete Initiated Status", t1);

        t1.info("Subscriber's MPIN is :" + ConfigInput.mPin);
        t1.info("Subscriber's TPIN is :" + ConfigInput.tPin);

        Transactions.init(t1).accountClosurebyAgentConfirmation(subs.MSISDN, tid);

        Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(subs.MSISDN, subs.CategoryCode), "N", "Verify User Status", t1);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_491() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_491 :",
                "To verify that network admin can approve a service charge for Account Closure service between Operator & Subscriber.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(chAddSubs);

        User subs = new User(Constants.SUBSCRIBER);

        OperatorUser operator = new OperatorUser(Constants.NETWORK_ADMIN);

        ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, operator, null, null, null, null);

        Map<String, String> dbSCharge = new MobiquityGUIQueries().dbGetServiceCharge(sChargeAccClose);

        try {
            if (!dbSCharge.isEmpty()) {
                ServiceChargeManagement.init(t1)
                        .deleteServiceCharge(sChargeAccClose);
            }

            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sChargeAccClose);
        } finally {
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sChargeAccClose);
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.CRITICAL_CASES_TAG})
    public void P1_TC_496() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_496 : Two Step subscriber registration:",
                "To verify that subscriber registration through 2 step service is active as per MFS provider if a user is registered through different MFS providers, then he will have to confirm each request separately.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(chAddSubs);

        List<String> allProviders = DataFactory.getAllProviderId();

        User subsNew = new User(Constants.SUBSCRIBER);

        if (allProviders.size() > 1) {
            if (AppConfig.isTwoStepRegistrationForSubs) {
                SubscriberManagement.init(t1).createSubscriber(subsNew);

                for (String proveiderID : allProviders) {
                    Transactions.init(t1)
                            .performSubscriberAcquisition(subsNew, Constants.STATUS_ACCEPT, proveiderID);
                }
            } else {

                String displayAllowedFromGUI = MobiquityGUIQueries.fetchDisplayAllowedOnGUI("TWO_STEP_SUBS_REG");
                t1.info("Changing Preference as TWO_STEP_SUBS_REG is set as FALSE");

                if (displayAllowedFromGUI.equalsIgnoreCase("Y")) {
                    doLoginAndModifyPreference("TWO_STEP_SUBS_REG", "TRUE", t1);

                    SubscriberManagement.init(t1).createSubscriber(subsNew);

                    for (String proveiderID : allProviders) {
                        Transactions.init(t1)
                                .performSubscriberAcquisition(subsNew, Constants.STATUS_ACCEPT, proveiderID);
                    }

                    doLoginAndModifyPreference("TWO_STEP_SUBS_REG", "FALSE", t1);

                } else {
                    t1.skip("Skipping the test case as 'TWO_STEP_SUBS_REG' is not allowed and Preference DisplayAllowed is Set as 'N'");
                }
            }
        } else {
            t1.skip("Skipping this case as at least 2 MFS providers required to perform the Test.");
        }
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_498() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_498 :Subscriber Registration:",
                "Channel user (Agent) should be able to initiate Subscriber creation with all MFS provider,wallet and bank account.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        User agent = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.RETAILER);

        if (agent == null) {
            t1.fail("User not found with access 'SUBSADD' and Category Code 'RT'. Pls check Channel User sheet and ConfigInput.xlsx ");
            Assert.fail("User not found with access 'SUBSADD' and Category Code 'RT'. Pls check Channel User sheet and ConfigInput.xlsx ");
        }

        Login.init(t1).login(agent);

        User subsWithAllProviderWalletBank = new User(Constants.SUBSCRIBER);

        SubscriberManagement.init(t1).createSubscriber(subsWithAllProviderWalletBank);

        Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(subsWithAllProviderWalletBank.MSISDN, subsWithAllProviderWalletBank.CategoryCode), "Y", "Verify DB Status", t1);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1}, enabled = false)
    public void P1_TC_144() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_498 :Subscriber Joining Fees",
                "To verify that Subscriber Joining Fees is applicable only when subscriber confirms his registration.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(chAddSubs);

        User subs = new User(Constants.SUBSCRIBER);

        initAndApproveSubs(subs, t1);

        /*if (AppConfig.isTwoStepRegistrationForSubs) {
            Transactions.init(pNode)
                    .subscriberAcquisition(subs);
        }*/

        // Change Mpin Tpin
        Transactions.init(pNode)
                .changeCustomerMpinTpin(subs);


    }

}
