package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.SuperAdmin;
import framework.entity.WebGroupRole;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author navin.pramanik
 */
public class Suite_UAP_GroupRoleManagement_01 extends TestInit {
    SuperAdmin saMaker;
    private WebGroupRole webGroupRole;
    private List<String> applicableRole;

    /**
     * SETUP
     */

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        applicableRole = new ArrayList<>(Arrays.asList("SUBSADDAP", "SUBSADD"));
        webGroupRole = new WebGroupRole(Constants.WHOLESALER, "DummyTestRole" + DataFactory.getRandomNumber(3), applicableRole, 1);
    }

    /**
     * Test Case: TC003 -> Add Group Role
     * Description : To verify that valid user should be able to add WEB Group Role
     * with specific Roles in the system
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 1)
    public void groupRoleAddTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0080:Group Role Add", "To verify that valid user should be able to add WEB Group Role.");

        t1.assignCategory(FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        //----Login SuperAdmin which can create Group Role- ---
        saMaker = DataFactory.getSuperAdminWithAccess("GRP_ROL");

        Login.init(t1).loginAsSuperAdmin(saMaker);

        GroupRoleManagement.init(t1)
                .addWebGroupRole(webGroupRole);

    }

    @Test(priority = 2, dependsOnMethods = "groupRoleAddTest", groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void groupRoleViewTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0078\n : Group Role View", "To verify that valid user should be able to view created WEB Group Role.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        saMaker = DataFactory.getSuperAdminWithAccess("GRP_ROL");
        Login.init(t1).loginAsSuperAdmin(saMaker);


        GroupRoleManagement.init(t1)
                .viewWebGroupRoleDetails(webGroupRole);
    }

    /**
     * Test Case: TC004 -> Modify Group Role
     * Description : To verify that  Super Admin should be able to Modify group role in the system and display a successful update message to the User.
     * <p>
     * This depends on method viewGroup role
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void groupRoleModifyTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0076 :Group Role Modify", "To verify that  Super Admin should be able to Modify group role in the system and display a successful update message to the User.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        //----Login SuperAdmin----
        saMaker = DataFactory.getSuperAdminWithAccess("GRP_ROL");
        Login.init(t1).loginAsSuperAdmin(saMaker);
        //Modify Existing Group Role---
        webGroupRole.removeRole("SUBSADDAP");

        GroupRoleManagement.init(t1)
                .addWebGroupRole(webGroupRole);

    }


    /**
     * Test Case: TC005 -> Delete Group Role
     * Description : To verify that valid user should be able to delete existing WEB Group Role
     * <p>
     * This depends on method -->  groupRoleAddTest
     *
     * @throws Exception
     */
    @Test(priority = 4, dependsOnMethods = "groupRoleModifyTest", groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, alwaysRun = true)
    public void groupRoleDeleteTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0077 :Group Role Delete", "To verify that valid user should be able to delete existing WEB Group Role");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        saMaker = DataFactory.getSuperAdminWithAccess("GRP_ROL");
        //----Login SuperAdmin----
        Login.init(t1).loginAsSuperAdmin(saMaker);

        GroupRoleManagement.init(t1)
                .deleteWebGroupRoleDetails(webGroupRole);
    }

    /**
     * Test         : TC_01
     * Description  : Duplicate and verify an existing Group Role
     * Date         : 3rd July 2017
     * Author       :
     *
     * @throws Exception
     */
    @Test(priority = 6, enabled = false, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void TC_001() throws Exception {
        WebGroupRole userRole = GlobalData.rnrDetails.get(0);

        ExtentTest t1 = pNode.createNode("TC0079\n : Group Role Duplicate", "To verify that the valid user can Duplicate group role in the system.-");

        saMaker = DataFactory.getSuperAdminWithAccess("GRP_ROL");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.GROUP_ROLE_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        GroupRoleManagement.init(t1)
                .duplicateWebRole(userRole);
    }
}
