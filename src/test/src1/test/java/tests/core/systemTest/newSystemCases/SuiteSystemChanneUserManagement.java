package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.InstrumentTCP;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.CMDExecutor;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 6/5/2018.
 */
public class SuiteSystemChanneUserManagement extends TestInit {


    private OperatorUser optAddUser, optSuspendUser, optAppUser, optModify, optModifyApp, delChUser, networkAdmin, initiator;
    private User chUser, wholesaler, subscriber, enterprise;
    private InstrumentTCP instTCP;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        optAddUser = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);
        optSuspendUser = DataFactory.getOperatorUserWithAccess("PTY_SCU", Constants.CHANNEL_ADMIN);
        optAppUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        optModify = DataFactory.getOperatorUserWithAccess("PTY_MCU");
        optModifyApp = DataFactory.getOperatorUserWithAccess("PTY_MCHAPP");
        delChUser = DataFactory.getOperatorUserWithAccess("PTY_DCU", Constants.CHANNEL_ADMIN);
        initiator = DataFactory.getOperatorUserWithAccess("PTY_SCU", Constants.CHANNEL_ADMIN);

        networkAdmin = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        instTCP = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDomainName(Constants.WHOLESALER),
                DataFactory.getCategoryName(Constants.WHOLESALER),
                DataFactory.getGradesForCategory(Constants.WHOLESALER).get(0).GradeName,
                "WALLET", DataFactory.getDefaultWallet().WalletName);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() throws Exception {
        chUser = new User(Constants.WHOLESALER);
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_103
     * DESC : Channel user Management >> Modify channel user>>" +
     * "To verify that when clicked on Next button on Channel user registratiion assign wallet Page without selecting any wallet application should proceed bank association page.
     *
     * @throws Exception
     */
    @Test(priority = 0, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1}, enabled = false)
    public void P1_TC_103() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_103",
                "Channel user Management >> Modify channel user>>" +
                        "To verify that when clicked on Next button on Channel user registratiion assign wallet Page without selecting any wallet application should proceed bank association page.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optAddUser);
        ChannelUserManagement.init(t1).initiateChannelUser(chUser)
                .assignHierarchy(chUser)
                .assignWebGroupRole(chUser)
                .mapWalletPreferences(chUser);
    }


    /**
     * TEST : POSITIVE
     * ID : P1_TC_313
     * DESC : Channel user Management >> Add channel user Approval
     * To verify that channel admin can reject initiated "Agent"
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_313() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_313",
                "Channel user Management >> Add channel user Approval \n" +
                        "To verify that channel admin can reject initiated \"Agent");
        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);

        User agent = new User(Constants.RETAILER);

        Login.init(t1).login(optAddUser);
        ChannelUserManagement.init(t1).addChannelUser(agent);
        CommonUserManagement.init(t1).addInitiatedApproval(agent, false);
    }

    //TODO - Have to Change the Expected Message Code

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_314
     * DESC : Channel user Management >> Suspend channel user: To verify that suspend channel user is not successful if
     * any pending transaction is present at Channel User end
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1}, enabled = false)
    public void P1_TC_314() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_314",
                "Channel user Management >> Suspend channel user: To verify that suspend channel user " +
                        "is not successful if any pending transaction is present at Channel User end");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);

        wholesaler = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);
        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        /*ServiceCharge commonScharge = new ServiceCharge(Services.CASHIN, wholesaler, subscriber, null, null, null, null);
        ServiceChargeManagement.init(t1)
                .configureServiceCharge(commonScharge);
        String txnAmount = getTransactionAmount(commonScharge);*/

        Login.init(t1).login(wholesaler);
        String txnID = TransactionManagement.init(t1).performCashIn(subscriber, Constants.CASHIN_TRANS_AMOUNT, DataFactory.getDefaultProvider().ProviderName);
        TransactionCorrection.init(t1).initiateTxnCorrection(txnID, true, true);

        Login.init(t1).login(initiator);
        startNegativeTest();
        ChannelUserManagement.init(t1).initiateSuspendChannelUser(wholesaler);

        Assertion.verifyErrorMessageContain("", "Verify Error Message.", t1);

        ChannelUserManagement.init(t1).initiateSuspendUserApproval(wholesaler, false);

    }

    //TODO - Have to Modify

    /**
     * TEST : POSITIVE
     * ID : P1_TC_319
     * DESC : Channel user Management >> Add channel user
     * To verify that channel admin cannot initiate Channel user addition if no profile is assigned to him.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1}, enabled = false)
    public void P1_TC_319() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_319",
                "Channel user Management >> Add channel user \n" +
                        "To verify that channel admin cannot initiate Channel user addition if no profile is assigned to him.");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);

        instTCP = DataFactory.getInstrumentTCP(DataFactory.getDomainName(Constants.WHOLESALER),
                DataFactory.getCategoryName(Constants.WHOLESALER),
                DataFactory.getGradesForCategory(Constants.WHOLESALER).get(0).GradeName,
                DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultWallet().WalletName);

        wholesaler = new User(Constants.WHOLESALER);
        /*TCPManagement.init(t1).deleteInitiateInstrumentTCP(instTCP).deleteApproveInstrumentTCP(instTCP);
        TCPManagement.init(t1).addInstrumentTCP(instTCP);
        TCPManagement.init(t1).approveInstrumentTCP(instTCP, t1);*/

        Login.init(t1).login(optAddUser);
        ChannelUserManagement.init(t1).initiateChannelUser(wholesaler).assignHierarchy(wholesaler);
        CommonUserManagement.init(pNode).assignWebGroupRole(wholesaler).mapWalletPreferences(wholesaler);

        Assertion.verifyErrorMessageContain("", "Verify Error Message.", t1);

    }


    /**
     * TEST : POSITIVE
     * ID : P1_TC_372
     * DESC : Channel user Management >> Add channel user>>To verify that channel user can register with multiple wallets for a MFS provider.
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_372() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_372",
                "Channel user Management >> Add channel user>>To verify that channel user can register with multiple wallets for a MFS provider. ");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        wholesaler = new User(Constants.WHOLESALER);

        Login.init(t1).login(optAddUser);
        ChannelUserManagement.init(t1).addChannelUser(wholesaler);
        Login.init(t1).login(optAppUser);
        ChannelUserManagement.init(t1).approveChannelUser(wholesaler);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_320
     * DESC : Channel user Management >> Delete channel user Approval
     * To verify that channel admin deletion request can be rejected at Delete Channel User Approval page.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_320() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_320",
                "Channel user Management >> Delete channel user Approval \n" +
                        "To verify that channel admin deletion request can be rejected at Delete Channel User Approval page.");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        wholesaler = new User(Constants.WHOLESALER);

        Login.init(t1).login(optAddUser);
        ChannelUserManagement.init(t1).addChannelUser(wholesaler).approveChannelUser(wholesaler);
        ChannelUserManagement.init(t1).initiateChannelUserDelete(wholesaler);
        ChannelUserManagement.init(t1).approveRejectDeleteChannelUser(wholesaler, false);

    }


    /**
     * TEST : POSITIVE
     * ID : P1_TC_324
     * DESC : To verify that channel admin can delete a channel user even if that channel user is suspended in application
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_324() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_324",
                "To verify that channel admin can delete a channel user even if that channel user is suspended in application");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        wholesaler = new User(Constants.WHOLESALER);

        Login.init(t1).login(optAddUser);
        ChannelUserManagement.init(t1).addChannelUser(wholesaler).approveChannelUser(wholesaler);
        ChannelUserManagement.init(t1).suspendChannelUser(wholesaler);
        ChannelUserManagement.init(t1).deleteChannelUser(wholesaler);
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_382
     * DESC : Channel user Management >> Add channel user>>" +
     * "To verify that while creating channel user, the Pin, Password should be in secured form in message sent logs and resent sms option on web.
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_382() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_382",
                "Channel user Management >> Add channel user>>" +
                        "To verify that while creating channel user, the Pin, Password should be in secured form in message sent logs and resent sms option on web.");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        wholesaler = new User(Constants.WHOLESALER);

        Login.init(t1).login(optAddUser);
        String beforeCreationTime = CMDExecutor.getLinuxServerDateTime();

        ChannelUserManagement.init(t1).createChannelUser(wholesaler);

        String afterCreationTime = CMDExecutor.getLinuxServerDateTime();

        if (wholesaler.isPwdReset.equalsIgnoreCase("Y")) {
            String msg = CMDExecutor.getMessageFromMessageSentLog(beforeCreationTime, afterCreationTime, wholesaler.MSISDN, true);
            t1.info("Sent Message Logs :" + msg);
            String password = msg.split("Password")[1].split("\\s")[0];
            String PIN = msg.split("PIN")[1].split("\\s")[0];

            Assertion.verifyEqual(password.toUpperCase(), "XXXX", "Check Encryption of \"Password\" in Logs", t1);
            Assertion.verifyEqual(PIN.toUpperCase(), "XXXX", "Check Encryption of \"PIN\" in Logs", t1);

        }
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_504
     * DESC : Channel user Management >> Modify channel user approval
     * To verify that Channel admin can reject modification request of an Channel user
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_504() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_504",
                "Channel user Management >> Modify channel user approval \n" +
                        "To verify that Channel admin can reject modification request of an Channel user");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(optAddUser);

        ChannelUserManagement.init(t1).createChannelUser(wholesaler).
                initiateChannelUserModification(wholesaler).
                completeChannelUserModification().
                approveRejectModifyChannelUser(wholesaler, true);
    }


    /**
     * TEST : POSITIVE
     * ID : P1_TC_505
     * DESC : Channel user Management >> Modify channel user approval
     * To verify that Channel admin can reject modification request of an Channel user
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_505() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_505",
                "Channel user Management >> Suspend channel user Approval \n" +
                        "To Verify that Channel admin can reject suspend initiate of channel user");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(optSuspendUser);

        ChannelUserManagement.init(t1).
                initiateSuspendChannelUser(wholesaler).
                approveSuspendChannelUser(wholesaler, false);
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_510
     * DESC : Channel user Management >> Modify channel user approval
     * To verify that Channel admin can reject modification request of an Channel user
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_510() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_510",
                "Channel user Management >> Modify channel user>>" +
                        "To verify that when creating Enterprise the bank screen will not show up.");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        enterprise = new User(Constants.ENTERPRISE);

        Login.init(t1).login(optAddUser);

        ChannelUserManagement.init(t1).
                createChannelUser(enterprise);
    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_326
     * DESC : Channel user Management >> Modify channel user>>" +
     * "To verify that channel user cannot register with same wallet type for more than one times a MFS provider. Eg. A channel User with MFS Provider 1 cannot have two salary wallet types.
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_326() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_326",
                "Channel user Management >> Add channel user>>" +
                        "To verify that channel user cannot register with same wallet type for more than one times a MFS provider. Eg. A channel User with MFS Provider 1 cannot have two \"Normal\" wallet types.");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        wholesaler = new User(Constants.WHOLESALER);

        Login.init(t1).login(optAddUser);

        ChannelUserManagement.init(t1).
                initiateChannelUser(wholesaler).assignHierarchy(wholesaler);

        CommonUserManagement.init(t1).assignWebGroupRole(wholesaler);

        CommonUserManagement.init(t1)
                .mapWalletPreferences(wholesaler);

        Assertion.verifyErrorMessageContain("cannot.have.multiple.samewallettype.per.mfs.provider", "Verify Error Message when \"Channel User\" register with same wallet type for more than one times", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_327
     * DESC : Channel user Management >> Modify channel user>>To verify that only one wallet can be set as Primary wallet for each MFS provider.
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_327() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_327",
                "Channel user Management >> Modify channel user>>To verify that only one wallet can be set as Primary wallet for each MFS provider.");

        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        wholesaler = new User(Constants.WHOLESALER);

        Login.init(t1).login(optAddUser);

        ChannelUserManagement.init(t1).
                initiateChannelUser(wholesaler).assignHierarchy(wholesaler);

        CommonUserManagement.init(t1).assignWebGroupRole(wholesaler);

        //todo need to fix belo code [rahul:21-aug]
        CommonUserManagement.init(t1)
                .mapWalletPreferences(wholesaler);

        Assertion.verifyErrorMessageContain("cannot.have.multiple.primaryaccount.per.mfs.provider", "Verify Error Message when 2 Wallet Set As Primary Account.", t1);
    }


}
