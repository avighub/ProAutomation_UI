package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.LMSProfile;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.loyaltyManagement.LMS;
import framework.pageObjects.Promotion.AddPromotion_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 7/18/2018.
 */
public class SuiteSystemLMS extends TestInit {
    private static SuperAdmin lmsApprover;
    private String PromoName, fromDate, toDate, Type;
    private OperatorUser proCreator, proModifier;
    private SuperAdmin approve;
    private ExtentTest tearDown;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            tearDown = pNode.createNode("teardown", "Teardown specific to this suite");
            PromoName = "LMS" + DataFactory.getRandomNumber(5);
            String dateFormat = "dd-MM-yyyy";
            String time = " 02:30:00 PM";
            fromDate = new DateAndTime().getDate(dateFormat, 3) + time;
            toDate = new DateAndTime().getDate(dateFormat, 21) + time;
            proCreator = DataFactory.getOperatorUserWithAccess("C_ROLE");
            proModifier = DataFactory.getOperatorUserWithAccess("MC_ROLE");
            approve = DataFactory.getSuperAdminWithAccess("AC_ROLE");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_111() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_111",
                "To verify that Promotion duration field value will be " +
                        "auto populated showing the difference of promotion end date " +
                        "and promotion start date as the duration of promotion is selected by the Network admin");
        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS);

        try {
            Login.init(t1).login(proCreator);
            AddPromotion_page1 addPromotion = new AddPromotion_page1(t1);
            addPromotion.NavigateNA();
            addPromotion.sendname(PromoName);

            String m1 = new DateAndTime().getDate("MMMM", 0);
            String m2 = new DateAndTime().getDate("MMMM", 31);

            String d1 = new DateAndTime().getDate("dd", 1);
            String d2 = new DateAndTime().getDate("dd", 4);

            addPromotion.selectStartDate(m1, d1);
            addPromotion.selectEndDate(m2, d2);
            Thread.sleep(2000);
            String duration = addPromotion.getPromotionDuration();
            if (duration == null) {
                t1.fail("Duration field is not populated");
            } else {
                t1.pass("Duration field is auto populated");
            }
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS})
    public void P1_TC_113() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_113",
                "To verify that while  Modifying  Promotion Network Admin can not change " +
                        "the service type- basis of promotion.");
        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS);

        try {
            Login.init(t1).login(proCreator);
            LMS.init(t1).addLMSpromotion(PromoName, fromDate, toDate, Services.CASHIN);
            Login.init(t1).loginAsSuperAdmin(approve);
            LMS.init(t1).approveLMS(PromoName);

            Login.init(t1).login(proModifier);
            LMS.init(t1).modifyLMS(PromoName);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Login.init(t1).loginAsSuperAdmin(approve);
            LMS obj = LMS.init(t1);
            obj.approveLMS(PromoName);
            Login.init(t1).login(proModifier);
            obj.deleteLMSProfile(PromoName);
            Login.init(t1).loginAsSuperAdmin(approve);
            obj.approveLMS(PromoName);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS})
    public void P1_TC_185() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_185",
                "To verify that Whenever a promotion is modified in the " +
                        "system a new version for that promotion with modified details " +
                        "is created in the system. All promotion versions are displayed " +
                        "in modify promotion home screen.");
        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS);

        LMS obj = LMS.init(t1);
        try {
            Login.init(t1).login(proCreator);

            Login.init(t1).login(proCreator);
            obj.addLMSpromotion(PromoName, fromDate, toDate, Services.CASH_IN_OTHERS);
            Login.init(t1).loginAsSuperAdmin(approve);
            obj.approveLMS(PromoName);
            Login.init(t1).login(proModifier);
            obj.modifyLMS(PromoName);
            Login.init(t1).loginAsSuperAdmin(approve);
            obj.approveLMS(PromoName);
            Login.init(t1).login(proModifier);
            // todo steps missing ??

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            String dateFormat = "dd-MM-yyyy";
            String time = " 02:30:00 PM";
            String from = new DateAndTime().getDate(dateFormat, -10) + time;
            String to = new DateAndTime().getDate(dateFormat, 21) + time;
            obj.ViewLMS(PromoName, from, to);
            Login.init(t1).login(proModifier);
            LMS.init(t1).deleteLMSProfile(PromoName);
            Login.init(t1).loginAsSuperAdmin(approve);
            LMS.init(t1).approveLMS(PromoName);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS,
            FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_186() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_186", "To verify that in case the promotion is suspended, " +
                "there will be an option to resumeLMSProfile the promotion until the promotion expiry date. These options will be " +
                "provided under modify promotion.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS, FunctionalTag.ECONET_UAT_5_0);

        User payer = new User(Constants.WHOLESALER);
        User payee = new User(Constants.SUBSCRIBER);

        LMSProfile lmsProfile = new LMSProfile(payer, payee, Services.CASHIN, 3, 45);
        try {
            Login.init(t1).login(proCreator);
            LMS obj = LMS.init(t1);

            Login.init(t1).login(proCreator);
            obj.addLMSpromotion(lmsProfile);

            Login.init(t1).loginAsSuperAdmin(approve);
            obj.approveLMS(lmsProfile.profileName);

            Login.init(t1).login(proModifier);
            obj.suspendLMSProfile(lmsProfile.profileName);

            Login.init(t1).loginAsSuperAdmin(approve);
            obj.approveLMS(lmsProfile.profileName);

            Login.init(t1).login(proModifier);
            obj.resumeLMSProfile(lmsProfile.profileName);

            Login.init(t1).loginAsSuperAdmin(approve);
            obj.approveLMS(lmsProfile.profileName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Login.init(t1).login(proModifier);
            LMS.init(t1).deleteLMSProfile(lmsProfile.profileName);
            Login.init(t1).loginAsSuperAdmin(approve);
            LMS.init(t1).approveLMS(lmsProfile.profileName);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 5, groups = {FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS})
    public void P1_TC_355() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_355", "To verify that System should not allow duplication of promotion on a particular service for the same amount at same time.");
        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.LMS);

        try {
            Login.init(t1).login(proCreator);
            String profile = "Neg" + DataFactory.getRandomNumber(5);
            Login.init(t1).login(proCreator);

            LMS.init(t1).addLMSpromotion(profile, fromDate, toDate, Services.COMPLETECASHOUT);


            profile = "Neg2" + DataFactory.getRandomNumber(5);
            Login.init(t1).login(proCreator);
            startNegativeTestWithoutConfirm();
            LMS.init(t1).addLMSpromotion(profile, fromDate, toDate, Services.COMPLETECASHOUT);
            Assertion.verifyErrorMessageContain("promotion.association.exists", "Assert error message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


}
