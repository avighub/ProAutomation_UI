package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created on 10-07-2017.
 */

public class Suite_UAP_Subscriber_Notification_Registration_01 extends TestInit {

    private User subscriber;
    private OperatorUser subsInfoViewer;

    @BeforeClass
    public void setup() throws Exception {
        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        subsInfoViewer = DataFactory.getOperatorUserWithAccess("NOTFREG");

    }

    /**
     * Test: TC197
     * Description: To verify that  valid user can add subscriber
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM})
    public void TC109() throws Exception {

        ExtentTest subsTest = pNode.createNode("TC109: Subscriber Notification Registration", "To verify that the valid user can add notifcation with subscriber to a particular biller.");

        subsTest.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT);
        Login.init(subsTest).
                login(subsInfoViewer);
        //SubscriberNotificationRegistration.init(subsTest).SubscriberNotificationRegistration(subscriber);

    }


}
