package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.accessManagement.AccessManagement;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.loginPasswordManagement.LoginPasswordManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for Login Password Management
 *
 * @author navin.pramanik
 */
public class Suite_UAP_LoginPasswordMgmt_01 extends TestInit {


    private static final String defSecretInput = "000000";
    private static final int invalidAttempt = 3;
    private OperatorUser resetPINUser;
    private SuperAdmin superAdmin;
    private User user;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        user = new User(Constants.WHOLESALER);
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP,
            FunctionalTag.DEPENDENCY_TAG})
    public void TC057() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0089 :Login Pass Mgmt: 1",
                "To verify that System user/ Channel user is assigned a login ID and Password for logging into system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.LOGIN);

        try {
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(user, false);

            String info = "Login ID assigned to user: " + user.LoginId + "\nPassword assigned to user: " + user.Password;
            Markup markup = MarkupHelper.createCodeBlock(info);
            t1.info(markup);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM,
            FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.DEPENDENCY_TAG})
    public void TC058() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0085 :Login Pass Mgmt: 2", "To verify that a system user/channel users  can change his password successfully.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.LOGIN, FunctionalTag.ECONET_UAT_5_0,
                FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            Login.init(t1).login(user);

            LoginPasswordManagement.init(t1)
                    .changePassword(user.Password, "Auto@13879");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void resetPwd() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0001 :Access Mgmt:Reset Password", "To verify that Super admin/system user can reset the password of any channel users/System User.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.ACCESS_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            String defaultPwd = AppConfig.defaultUsrPwd;

            superAdmin = DataFactory.getSuperAdminWithAccess("SR_PASS");

            Login.init(t1).loginAsSuperAdmin(superAdmin);

            AccessManagement.init(t1).resetPassword(user, Constants.CHANNEL_USERS_CONST);

            Assertion.verifyDBMessageContains(user, "01073", "SMS verification ", t1, defaultPwd);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 4, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0}, dependsOnMethods = "TC057")
    public void resetPin() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0118 : Reset Pin", "To verify that the nwAdmin can do Reset PIN of system users.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            resetPINUser = DataFactory.getOperatorUsersWithAccess("SR_PIN").get(0);
            Login.init(t1).login(resetPINUser);

            Enquiries.init(t1).resetPin(user, Constants.CHANNEL_USERS_CONST, Constants.RESET_MPIN_CONST);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_UAP})
    public void Test() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0086 : Login Pwd Mgmt: Invalid Login",
                "To verify that mobiquity should not allow user to login into the application with 3 (configurable) invalid password attempt.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.LOGIN);

        try {

            LoginPasswordManagement.init(t1).invalidLogin(user.LoginId, invalidAttempt);
            //After three time wrong login try with valid login
            LoginPasswordManagement.init(t1).invalidLogin(user.LoginId, defSecretInput);
            //To Reset the status
            Login.resetLoginStatus();
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }
}
