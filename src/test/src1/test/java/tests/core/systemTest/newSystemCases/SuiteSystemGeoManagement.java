/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Febin Thomas
 *  Date: 9-Dec-2017
 *  Purpose: Feature of BlackListManagement
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Geography;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.GeographyManagement;
import framework.pageObjects.Geo_managment.Geography_ModifyPage1;
import framework.pageObjects.Geo_managment.Geography_Page1;
import framework.pageObjects.Geo_managment.Geography_Page2;
import framework.pageObjects.Geo_managment.Geography_Page3;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * /**
 * Created by febin.thomas on 11/24/2017.
 */
public class SuiteSystemGeoManagement extends TestInit {

    private Geography geoMain = new Geography();
    private String areaName, areaCode, areaShortName, areaDesc, zoneName, zoneCode, zoneShortName, zoneDesc;
    private Geography geo, geo2;
    private OperatorUser optUser;


    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        ExtentTest t1 = pNode.createNode("Preconditions");

        optUser = DataFactory.getOperatorUserWithAccess("VIEWGRPHDOMAIN");
        geo2 = new Geography();
        geo = new Geography();
        Login.init(t1).login(optUser);


        GeographyManagement.init(t1).addZone(geo2);
        GeographyManagement.init(t1).addArea(geo2);

        areaCode = geo2.getAreaCode();
        areaName = geo2.getAreaName();
        areaShortName = geo2.getAreaShortName();
        areaDesc = geo2.getAreaDescription();
        zoneCode = geo2.getZoneCode();
        zoneName = geo2.getZoneName();
        zoneShortName = geo2.getZoneShortName();
        zoneDesc = geo2.getZoneDescription();

        areaCode = geo.getAreaCode();
        areaName = geo.getAreaName();
        areaShortName = geo.getAreaShortName();
        areaDesc = geo.getAreaDescription();
        zoneCode = geo.getZoneCode();
        zoneName = geo.getZoneName();
        zoneShortName = geo.getZoneShortName();
        zoneDesc = geo.getZoneDescription();
    }

    @BeforeMethod(alwaysRun = true)
    public void setGeoObject() {
        geo2.setAreaCode(areaCode);
        geo2.setAreaName(areaName);
        geo2.setAreaShortName(areaShortName);
        geo2.setAreaDescription(areaDesc);
        geo2.setZoneCode(zoneCode);
        geo2.setZoneName(zoneName);
        geo2.setZoneShortName(zoneShortName);
        geo2.setZoneDescription(zoneDesc);

        geo.setAreaCode(areaCode);
        geo.setAreaName(areaName);
        geo.setAreaShortName(areaShortName);
        geo.setAreaDescription(areaDesc);
        geo.setZoneCode(zoneCode);
        geo.setZoneName(zoneName);
        geo.setZoneShortName(zoneShortName);
        geo.setZoneDescription(zoneDesc);
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void addZoneTest() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0001: Add Zone", "To verify that the valid user can \"Add new Zone\" in the system using \"Network Admin\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).addZone(geoMain);
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void addAreaTest() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0002: Add Area", "To verify that the valid user can \"Add new Area\" in the system using \"Network Admin\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).addArea(geoMain);

    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void updateZoneTest() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0003:" + getTestcaseid() + " Update Zone", "To verify that the valid user can \"Update Zone \" in the system using \"Network Admin\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).modifyZone(geoMain, "Active");

    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void updateAreaTest() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0004:" + getTestcaseid() + " Update Area", "To verify that the valid user can \"Update Area \" in the system using \"Network Admin\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).modifyArea(geoMain, "Active");

    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void deleteAreaTest() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0005:" + getTestcaseid() + " Delete Area", "To verify that the valid user can \"Delete Area\" in the system using \"Network Admin\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).deleteArea(geoMain);

    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void deleteZoneTest() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0006:" + getTestcaseid() + " Delete Zone", "To verify that the valid user can \"Delete Zone\" in the system using \"Network Admin\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).deleteZone(geoMain);

    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_1() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0007:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Code\" and submitted on \"Add Geographical domain\" page of \"Zone\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneCode(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("geo.error.domaincode.alphaNumeric", "Verify Error Message", t1);
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_2() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0008:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Name\" and submitted on \" Add Geographical domain\" page of \"Zone\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneName(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("geo.error.grphDomainName.alphabetic", "Verify Error Message", t1);
    }

    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_3() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0009: Zone Add Negative", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Short Name\"and submitted on \"Add Geographical domain\" page of \"Zone\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneShortName(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("geo.error.grphDomainShortName.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_4() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0010:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when special characters are entered in  \"Description\"and submitted on \" Add Geographical domain\" page of \"Zone\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneDescription(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("geo.error.description.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_5() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0011:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"Space is left between Characters\" in \"Geographical Domain Code\" and submitted on \"Add Geographical domain\" page of \"Zone\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneCode("ads asa");

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("geo.error.domaincode.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_6() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0012:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"Space is left between Characters\" in \"Geographical Domain Short Name\"and submitted on \"Add Geographical domain\" page of \"Zone\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneShortName("ash alk");

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("geo.error.grphDomainShortName.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_7() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0013", "To verify that proper error is displayed when \"Geographical Domain Code\" is left \"blank\" and submitted on \"Add Geographical domain\" page of \"Zone\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneCode(Constants.BLANK_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("grphDomain.domainCode.required", "Verify Error Message", t1);

    }

    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_8() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0014:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"Geographical Domain Name\" is left \"blank\" and submitted on \"Add Geographical domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneName(Constants.BLANK_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("grphDomain.domainName.required", "Verify Error Message", t1);
    }

    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_9() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0015:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"Geographical Domain Short Name\" is left \"blank\" and submitted on \"Add Geographical domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneShortName(Constants.BLANK_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("grphDomain.shrotName.required", "Verify Error Message", t1);

    }

    @Test(priority = 16, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_10() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0016:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"Description\" is left \"blank\" and submitted on \"Add Geographical domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        geo.setZoneDescription(Constants.BLANK_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Assertion.verifyErrorMessageContain("requiredstring.description", "Verify Error Message", t1);

    }

// Update Cases

    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_11() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0017: Zone Gui",
                "To verify that proper error is displayed when \"Geographical Domain Name\" is left \"blank\" and submitted on \"Modify Geographical Domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addZone(geo);*/
        geo.setZoneName(Constants.BLANK_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone(geo, "Active");

        Assertion.verifyErrorMessageContain("grphDomain.domainName.required", "Verify Error Message", t1);

    }

    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_12() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0018:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"Geographical Domain Short Name\" is left \"blank\" and submitted on \"Modify Geographical Domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);


        /*GeographyManagement.init(t1).startNegativeTest().addZone(geo);*/
        geo.setZoneShortName(Constants.BLANK_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone(geo, "Active");

        Assertion.verifyErrorMessageContain("grphDomain.shrotName.required", "Verify Error Message", t1);

    }

    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_13() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0019:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"Description\" is left \"blank\" and submitted on \"Modify Geographical Domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addZone(geo);*/
        geo.setZoneDescription(Constants.BLANK_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone(geo, "Active");

        Assertion.verifyErrorMessageContain("requiredstring.description", "Verify Error Message", t1);

    }

    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_14() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0020:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Name\" submitted on \"Modify Geographical Domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addZone(geo);*/

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone_DomName(geo, Constants.SPECIAL_CHARACTER_CONSTANT, "Active");

        Assertion.verifyErrorMessageContain("geo.error.grphDomainName.alphabetic", "Verify Error Message", t1);

    }

    @Test(priority = 21, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_15() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0021:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Short Name\" submitted on \"Modify Geographical Domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addZone(geo);*/
        geo.setZoneShortName(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone(geo, "Active");

        Assertion.verifyErrorMessageContain("geo.error.grphDomainShortName.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 22, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_16() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0022:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when special characters are entered in \"Description\"submitted on \"Modify Geographical Domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addZone(geo);*/
        geo.setZoneDescription(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone(geo, "Active");

        Assertion.verifyErrorMessageContain("geo.error.description.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 23, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void ZoneTest_17() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0023:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"Space is left between Characters\" in \"Geographical Domain Short Name\" submitted on \"Modify Geographical Domain\" page of \"Zone\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addZone(geo);*/
        geo.setZoneShortName("sc as");

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone(geo, "Active");

        Assertion.verifyErrorMessageContain("geo.error.grphDomainShortName.alphaNumeric", "Verify Error Message", t1);

    }

    // Area Cases

    @Test(priority = 24, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_1() throws Exception {


        ExtentTest t1 = pNode.createNode("SYS_TC_F_0024:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Code\" and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaCode(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addAreaNeg(geo);

        Assertion.verifyErrorMessageContain("geo.error.domaincode.alphaNumeric", "Verify Error Message", t1);


    }

    @Test(priority = 25, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_2() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0025:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Name\" and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaName(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addAreaNeg(geo);

        Assertion.verifyErrorMessageContain("geo.error.grphDomainName.alphabetic", "Verify Error Message", t1);


    }

    @Test(priority = 26, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_3() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0026:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Short Name\" and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaShortName(Constants.SPECIAL_CHARACTER_CONSTANT);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaNeg(geo);
        Assertion.verifyErrorMessageContain("geo.error.grphDomainShortName.alphaNumeric", "Verify Error Message", t1);


    }

    @Test(priority = 27, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_4() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0027:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when special characters are entered in  \"Description\"and submitted on \" Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaDescription(Constants.SPECIAL_CHARACTER_CONSTANT);
        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addAreaNeg(geo);
        Assertion.verifyErrorMessageContain("geo.error.description.alphaNumeric", "Verify Error Message", t1);


    }

    @Test(priority = 28, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_5() throws Exception {


        ExtentTest t1 = pNode.createNode("SYS_TC_F_0028:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Space is left between Characters\" in \"Geographical Domain Code\" and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaCode("sa sa");

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaNeg(geo);

        Assertion.verifyErrorMessageContain("geo.error.domaincode.alphaNumeric", "Verify Error Message", t1);


    }

    @Test(priority = 29, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_6() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0029:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Space is left between Characters\" in \"Geographical Domain Short Name\"and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaShortName("ss sa");
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaNeg(geo);
        Assertion.verifyErrorMessageContain("geo.error.grphDomainShortName.alphaNumeric", "Verify Error Message", t1);


    }

    @Test(priority = 30, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_7() throws Exception {


        ExtentTest t1 = pNode.createNode("SYS_TC_F_0030:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Geographical Domain Code\" is left \"blank\" and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaCode(Constants.BLANK_CONSTANT);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaNeg(geo);

        Assertion.verifyErrorMessageContain("grphDomain.domainCode.required", "Verify Error Message", t1);


    }

    @Test(priority = 31, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_8() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0031:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Geographical Domain Name\" is left \"blank\" and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaName(Constants.BLANK_CONSTANT);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaNeg(geo);

        Assertion.verifyErrorMessageContain("grphDomain.domainName.required", "Verify Error Message", t1);


    }

    @Test(priority = 32, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_9() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0032:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Geographical Domain Short Name\" is left \"blank\" and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaShortName(Constants.BLANK_CONSTANT);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaNeg(geo);
        Assertion.verifyErrorMessageContain("grphDomain.shrotName.required", "Verify Error Message", t1);


    }

    @Test(priority = 33, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_10() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0033:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Description\" is left \"blank\" and submitted on \"Add Geographical domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).selectZone(geo2);
        geo.setAreaDescription(Constants.BLANK_CONSTANT);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaNeg(geo);
        Assertion.verifyErrorMessageContain("requiredstring.description", "Verify Error Message", t1);


    }

    @Test(priority = 34, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_11() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0034:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Geographical Domain Name\" is left \"blank\" and submitted on \"Modify Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTest();
        GeographyManagement.init(t1).addArea(geo2);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyArea_DomName(geo2, "Active", Constants.BLANK_CONSTANT);
        Assertion.verifyErrorMessageContain("grphDomain.domainName.required", "Verify Error Message", t1);

    }

    @Test(priority = 35, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_12() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0035:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Geographical Domain Short Name\" is left \"blank\" and submitted on \"Modify Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        geo2.setAreaShortName(Constants.BLANK_CONSTANT);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyArea(geo2, "Active");

        Assertion.verifyErrorMessageContain("grphDomain.shrotName.required", "Verify Error Message", t1);

    }

    @Test(priority = 36, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_13() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0036:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Description\" is left \"blank\" and submitted on \"Modify Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        geo2.setAreaDescription(Constants.BLANK_CONSTANT);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyArea(geo2, "Active");

        Assertion.verifyErrorMessageContain("requiredstring.description", "Verify Error Message", t1);

    }

    @Test(priority = 37, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_14() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0037:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Name\" submitted on \"Modify Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyArea_DomName(geo2, "Active", Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("geo.error.grphDomainName.alphabetic", "Verify Error Message", t1);

    }

    @Test(priority = 38, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_15() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0038:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when special characters are entered in \"Geographical Domain Short Name\" submitted on \"Modify Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        geo2.setAreaShortName(Constants.SPECIAL_CHARACTER_CONSTANT);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyArea(geo2, "Active");

        Assertion.verifyErrorMessageContain("geo.error.grphDomainShortName.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 39, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_16() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0039:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when special characters are entered in \"Description\"submitted on \"Modify Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        geo2.setAreaDescription(Constants.SPECIAL_CHARACTER_CONSTANT);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyArea(geo2, "Active");

        Assertion.verifyErrorMessageContain("geo.error.description.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 40, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void AreaTest_17() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0040:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Space is left between Characters\" in \"Geographical Domain Short Name\" submitted on \"Modify Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        geo2.setAreaShortName("sd sa");
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyArea(geo2, "Active");

        Assertion.verifyErrorMessageContain("geo.error.grphDomainShortName.alphaNumeric", "Verify Error Message", t1);

    }

    @Test(priority = 41, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void Zone_18() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0041:" + getTestcaseid() + " Zone Gui", "To verify that proper error is displayed when \"no area is selected\" and \"Update\" button is clicked on \"View Geographical Domain\" page of \"Zone\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyZoneNoSelections();

        Assertion.verifyErrorMessageContain("geo.select.graphDomain", "Verify Error Message", t1);


    }

    @Test(priority = 42, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void Zone_19() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0042:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"no area is selected\" and \"Update\" button is clicked on \"View Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).modifyAreaNoSelection(geo);

        Assertion.verifyErrorMessageContain("geo.select.graphDomain", "Verify Error Message", t1);

    }

    @Test(priority = 43, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void Zone_20() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0043:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"Zone\" is left \"Blank\" and submitted in the \"Select Parent Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        geo.setZoneName("");
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaSelectZone(geo);

        Assertion.verifyErrorMessageContain("grphdomain.operation.msg.selectparent", "Verify Error Message", t1);

    }

    @Test(priority = 44, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void Zone_21() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0044:" + getTestcaseid() + " Area Gui", "To verify that proper error is displayed when \"invalid zone\" is given in \"Zone\" and submitted in the \"Select Parent Geographical Domain\" page of \"Area\".\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        /*GeographyManagement.init(t1).startNegativeTest().addArea(geo2);*/
        geo.setZoneName("slsl");
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1).addAreaSelectZone(geo);

        Assertion.verifyErrorMessageContain("grphdomain.operation.msg.selectparent", "Verify Error Message", t1);

    }

    @Test(priority = 45, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_1() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0045:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"View Geographical Domain\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);
        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit()
                .clickMainBack();
        boolean check = page.checkGeoMain();

        Assertion.verifyEqual(check, true, "Verify back button", t1);

    }

    @Test(priority = 46, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_2() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0046:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"Add Area\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);
        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit()
                .add()
                .clickBackAdd();


        boolean check = page.checkGeoPage2();

        Assertion.verifyEqual(check, true, "Verify back button", t1);

    }

    @Test(priority = 47, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_3() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0047:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"Add Zone Confirm\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);
        Geography_Page3 page1 = new Geography_Page3(t1);
        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit()
                .add()
                .adddvalues(geo.ZoneCode, geo.ZoneName, geo.ZoneShortName, geo.ZoneDescription)
                .addgeo()
                .clickBackAddConfirm();
        boolean check = page1.checkGeoPageAdd();

        Assertion.verifyEqual(check, true, "Verify back button", t1);

    }

    @Test(priority = 48, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_4() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0048:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"Update Zone Confirm\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);

        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit();


        Geography_ModifyPage1 page1 = new Geography_ModifyPage1(t1);
        page1.selectgeo(geo.ZoneName)
                .clickUpdateButton()
                .setDomName(geo.ZoneName)
                .setDomShortName(geo.ZoneShortName)
                .setGeoDesc(geo.ZoneDescription)
                .selectStatus("Active")
                .clickModify()
                .clickBackUpdateConfirm();
        boolean check = page1.checkGeoUpdate();

        Assertion.verifyEqual(check, true, "Verify back button", t1);

    }

    @Test(priority = 49, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_5() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0049:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"Update Zone\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);

        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit();


        Geography_ModifyPage1 page1 = new Geography_ModifyPage1(t1);
        Geography_Page3 page2 = new Geography_Page3(t1);
        page1.selectgeo(geo.ZoneName)
                .clickUpdateButton();
        page2.clickBackUpdate();
        boolean check = page.checkGeoPage2();

        Assertion.verifyEqual(check, true, "Verify back button", t1);

    }

    @Test(priority = 50, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_6() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0050:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"Add Area Confirm\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);

        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Area")
                .clicksubmit()
                .selectzone(geo.ZoneName)
                .add()
                .adddvalues(geo.AreaCode, geo.AreaName, geo.AreaShortName, geo.AreaDescription)
                .addgeo();
        Geography_Page3 page1 = new Geography_Page3(t1);
        page1.clickBackAddConfirm();
        boolean check = page1.checkGeoPageAdd();

        Assertion.verifyEqual(check, true, "Verify back button", t1);

    }

    @Test(priority = 51, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_7() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0051:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"Add Area\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);

        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Area")
                .clicksubmit()
                .selectzone(geo.ZoneName)
                .add()
                .clickBackAdd();
        boolean check = page.checkGeoPage2();

        Assertion.verifyEqual(check, true, "Verify back button", t1);

    }

    @Test(priority = 52, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_8() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0052:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"Update Area Confirm\" page.\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);
        Login.init(t1).login(optUser);
        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);
        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Area")
                .clicksubmit()
                .selectzone(geo.ZoneName);
        Geography_ModifyPage1 page1 = new Geography_ModifyPage1(t1);
        page1.selectgeo(geo.AreaName)
                .clickUpdateButton()
                .setDomName(geo.AreaName)
                .setDomShortName(geo.AreaShortName)
                .setGeoDesc(geo.AreaDescription)
                .selectStatus("Active")
                .clickModify()
                .clickBackUpdateConfirm();
        boolean check = page1.checkGeoUpdate();

        Assertion.verifyEqual(check, true, "Verify back button", t1);
    }

    @Test(priority = 53, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void GEO_9() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0053:" + getTestcaseid() + " Geo", "To verify that \"Back\" button works correctly on \"Update Area\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();
        GeographyManagement.init(t1);

        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Area")
                .clicksubmit()
                .selectzone(geo.ZoneName);

        Geography_ModifyPage1 page1 = new Geography_ModifyPage1(t1);
        Geography_Page3 page2 = new Geography_Page3(t1);
        page1.selectgeo(geo.AreaName)
                .clickUpdateButton();
        page2.clickBackUpdate();
        boolean check = page.checkGeoPage2();

        Assertion.verifyEqual(check, true, "Verify back button", t1);

    }

    @Test(priority = 54, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void Suspend_Area() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0072:" + getTestcaseid() + " Geo", "To verify that valid user is able to suspend a Area.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).modifyArea(geo, "Suspended");

        GeographyManagement.init(t1).modifyArea(geo, "Active");

    }

    @Test(priority = 55, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void Suspend_Zone() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0071:" + getTestcaseid() + " Geo", "To verify that valid user is able to suspend a zone.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).modifyZone(geo, "Suspended");

        GeographyManagement.init(t1).modifyZone(geo, "Active");

    }

    /*@Test(priority = 56, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT})
    public void Area_Test() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0072:" + getTestcaseid() + " Geo", "To verify that valid user is able to suspend a Area.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT);

        Login.init(t1).login(optUser);

        GeographyManagement.init(t1).modifyArea(geo, "Suspended");

        GeographyManagement.init(t1).modifyArea(geo, "Active");

    }*/



    /*
     *  ##############################################################################
     *  ############ GEO MANAGEMENT *P1 * CASES * STARTS * FROM * HERE ###############
     *  ##############################################################################
     */


    @Test(priority = 57, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_011: Geo",
                "To verify that Network Admin should be able to check visibility of \"Back\", \"Cancel\" and \"Confirm\" button on \"Modify Area Confirm Page.\"");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyArea(geo, "Active");

        Geography_ModifyPage1 page1 = new Geography_ModifyPage1(t1);

        Assertion.verifyEqual(page1.backVisible(), true, "Check Back Button Visible", t1);
        Assertion.verifyEqual(page1.cancelVisible(), true, "Check Cancel Button Visible", t1);
        Assertion.verifyEqual(page1.confirmVisible(), true, "Check Confirm Visible", t1);

    }

    @Test(priority = 58, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_012: Geo", "To verify that Network Admin should be able to check visibility of \"add\", \"update\", \"delete\" and \"back\"  button on 'Delete Area Page'.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        new Geography_Page1(t1).
                navigateToLink().
                selectgeo("Zone").
                clicksubmit();

        Geography_Page2 page2 = new Geography_Page2(t1);

        Assertion.verifyEqual(page2.addVisible(), true, "Check Add Button Visible", t1);
        Assertion.verifyEqual(page2.updateVisible(), true, "Check Update Button Visible", t1);
        Assertion.verifyEqual(page2.deleteVisible(), true, "Check Delete Button Visible", t1);
        Assertion.verifyEqual(page2.backVisible(), true, "Check Back Button Visible", t1);

    }

    @Test(priority = 59, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_029() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_029", "To verify that Application displays the following information :\n" +
                "• Network Name \n" +
                "• Parent Geographical domain name, \n" +
                "• Network short name \n" +
                "• Parent Geographical  domain type \n" +
                "• Zone \n" +
                "• Geographical Domain Name \n" +
                "• Geographical domain short code \n" +
                "• Status\n" +
                "Buttons to click :\n" +
                "• Button to Add\n" +
                "• Button to Update \n" +
                "• Button to Delete\n" +
                " on Add Area Confirm page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addArea(geo2);

        Geography_Page3 page = new Geography_Page3(t1);

        //String netCode = MobiquityGUIQueries.executeQueryAndReturnResult("select NETWORK_CODE from NETWORK_PREFIXES where status='Y'", "NETWORK_CODE");

        Assertion.verifyEqual(page.networkNameVisible(), true, "Visibility of Network Name", t1);

        Assertion.verifyEqual(page.confirmNetworkShortNameVisible(), true, "Visibility of Network Short Name", t1);

        Assertion.verifyEqual(page.getConfirmPageDomainCode(), geo2.AreaCode, "Verify Area Code", t1);
        Assertion.verifyEqual(page.getConfirmPageDomainName(), geo2.AreaName, "Verify Area Name", t1);
        Assertion.verifyEqual(page.getConfirmPageDomainShortName(), geo2.AreaShortName, "Verify Area Short Name", t1);
        Assertion.verifyEqual(page.getConfirmPageDescription(), geo2.AreaDescription, "Verify Area Description", t1);
        Assertion.verifyEqual(page.statusVisible(), true, "Visibility of Status", t1);

        Assertion.verifyEqual(page.backVisible(), true, "Visibility of Back Button", t1);
        Assertion.verifyEqual(page.cancelVisible(), true, "Visibility of Cancel Button", t1);
        Assertion.verifyEqual(page.confirmVisible(), true, "Visibility of Confirm Button", t1);
    }


    @Test(priority = 61, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_5() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_094: Geo", "To verify that application should be display the following information \n" +
                "1.Name\n" +
                "2.Short name\n" +
                "3.Status\n" +
                "on Modify Area Page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Area")
                .clicksubmit()
                .selectzone(geo2.ZoneName);

        Geography_ModifyPage1 modifyPage1 = new Geography_ModifyPage1(t1);
        modifyPage1
                .selectgeo(geo2.AreaName)
                .clickUpdateButton()
                .setDomName(geo2.AreaName)
                .setDomShortName(geo2.AreaShortName)
                .setGeoDesc(geo2.AreaDescription).
                clickModify();

        Assertion.verifyEqual(modifyPage1.statusVisible(), true, "Status Visible", t1);
        Assertion.verifyEqual(modifyPage1.nameVisible(), true, "Domain Name Visible", t1);
        Assertion.verifyEqual(modifyPage1.shortNameVisible(), true, "Domain Short Name Visible", t1);
    }


    @Test(priority = 62, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_008() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_008: Geo", "To verify that Application displays \"Add \",\"Update\",\"Delete\" and \"Back\" buttons on 'Add Zone initiation page");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit();
        Geography_Page2 page2 = new Geography_Page2(t1);

        Assertion.verifyEqual(page2.addVisible(), true, "Add Button Visible", t1);
        Assertion.verifyEqual(page2.updateVisible(), true, "Update Button Visible", t1);
        Assertion.verifyEqual(page2.deleteVisible(), true, "Delete Button Visible", t1);
        Assertion.verifyEqual(page2.backVisible(), true, "Back Button Visible", t1);
    }

    @Test(priority = 63, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_009() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_009: Geo", "To verify that Network Admin should be able to check visibility of  \"confirm\", \"back\" and \"cancel\" buttons on \"Modify Zone Confirm Page\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone(geo, Constants.ACTIVE);

        Geography_ModifyPage1 page1 = new Geography_ModifyPage1(t1);

        Assertion.verifyEqual(page1.backVisible(), true, "Back Button Visible", t1);
        Assertion.verifyEqual(page1.cancelVisible(), true, "Cancel Button Visible", t1);
        Assertion.verifyEqual(page1.confirmVisible(), true, "Confirm Button Visible", t1);

    }

    @Test(priority = 64, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_088() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_088", "To verify that Network Admin should be able to enter  \"Domain name\", \"Short name\" and \"Description\" on Modify Zone Page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        new Geography_Page1(t1)
                .navigateToLink()
                .selectgeo("Zone")
                .clicksubmit();

        Geography_ModifyPage1 page1 = new Geography_ModifyPage1(t1);
        page1.selectgeo(geo.ZoneName)
                .clickUpdateButton()
                .setDomName(geo.ZoneName)
                .setDomShortName(geo.ZoneShortName)
                .setGeoDesc(geo.ZoneDescription).clickModify().clickBackUpdateConfirm();

        Assertion.verifyEqual(page1.getGeoDomainNameFromTbox(), geo.ZoneName, "Check Domain Name Entered", t1);
        Assertion.verifyEqual(page1.getGeoShortNameFromTbox(), geo.ZoneShortName, "Check Domain Short Name Entered", t1);
        Assertion.verifyEqual(page1.getGeoDescFromTbox(), geo.ZoneDescription, "Check Description Entered", t1);


    }

    @Test(priority = 65, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_9() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_089", "To verify that Network Admin should be able to check visibility of  \"Network Name\", \"Short name\" and \"Status\" on \"Add Zone Confirm Page\"");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).modifyZone(geo, Constants.ACTIVE);

        Geography_ModifyPage1 page = new Geography_ModifyPage1(t1);

        Assertion.verifyEqual(page.nameVisible(), true, "Name Visibility", t1);
        Assertion.verifyEqual(page.shortNameVisible(), true, "Short Name Visibility", t1);
        Assertion.verifyEqual(page.statusVisible(), true, "Status Visibility", t1);

    }

    /**
     * @throws Exception
     */
    @Test(priority = 66, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_400() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_400",
                "To verify that Super Admin or Network Admin should be able to add Zone in system and check if user is able to enter \"name\", \"short name\" and \"description\"\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addZone(geo);

        Geography_Page3 page3 = new Geography_Page3(t1);

        page3.clickBackAddConfirm();


       /* page.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit()
                .add()
                .adddvalues(geo.ZoneCode, geo.ZoneName, geo.ZoneShortName, geo.ZoneDescription);
*/


        Assertion.verifyEqual(page3.getGeoNameGui(), geo.ZoneName, "Check Domain Name Entered", t1);
        Assertion.verifyEqual(page3.getShortNameGui(), geo.ZoneShortName, "Check Domain Short Name Entered", t1);
        Assertion.verifyEqual(page3.getDescriptionGui(), geo.ZoneDescription, "Check Description Entered", t1);
    }

    @Test(priority = 67, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_11() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_402", "To verify that Network Admin should be able to check the visibility of  \"Network Name\", \"Short name\" and \"Description\" on \"Add Area Confirm Page\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        startNegativeTestWithoutConfirm();

        GeographyManagement.init(t1).addArea(geo);

        Geography_Page3 page = new Geography_Page3(t1);

        Assertion.verifyEqual(page.nameVisible(), true, "Check visibility of Name", t1);
        Assertion.verifyEqual(page.shortNameVisible(), true, "Check visibility of Short Name", t1);
        Assertion.verifyEqual(page.statusVisible(), true, "Check visibility of Status", t1);

    }

    @Test(priority = 68, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_12() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_090:Geo", "To verify that Application should allow Network Admin to select \"Area\" from Geographical domain type drop down.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        Geography_Page1 page = new Geography_Page1(t1);
        page.navigateToLink()
                .selectgeo("Area");
        String value = page.getSelectedGeo();

        Assertion.verifyEqual(value, "Area", "Selected Geography type", t1, true);

    }

    /**
     * @throws Exception
     */
    @Test(priority = 69, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_13() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_091", "To verify that Application displays the following information :\n" +
                "• Network Name \n" +
                "• Parent Geographical domain name, \n" +
                "• Network short name \n" +
                "• Parent Geographical  domain type \n" +
                "• Zone \n" +
                "• Geographical Domain Name \n" +
                "• Geographical domain short code \n" +
                "• Status\n" +
                "on Add Zone Page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        Geography_Page1 page1 = new Geography_Page1(t1);
        page1.navigateToLink()
                .selectgeo("Zone")
                .clicksubmit();

        Geography_Page3 page3 = new Geography_Page3(t1);


        Assertion.verifyEqual(page1.networkNameInit.isDisplayed(), true, "Network Name Visible", t1);
        Assertion.verifyEqual(page1.networkShortNameInit.isDisplayed(), true, "Network Short Name Visible", t1);
        Assertion.verifyEqual(page1.parentNameInit.isDisplayed(), true, "Parent Geo Name Visible", t1);
        Assertion.verifyEqual(page1.parentDomainTypeInit.isDisplayed(), true, "Parent Geo Type Visible", t1);
        Assertion.verifyEqual(page1.domainNameInit.isDisplayed(), true, "Domain Name Visible", t1);
        Assertion.verifyEqual(page1.domainShortNameInit.isDisplayed(), true, "Domain Short Name Visible", t1);
        Assertion.verifyEqual(page1.statusInit.isDisplayed(), true, "Status Visible", t1);


        Assertion.verifyEqual(page3.networkNameInitVisible(), true, "Network Name Visible", t1);
        Assertion.verifyEqual(page3.parentDomNameInitVisible(), true, "Parent Domain Name Visible", t1);
        Assertion.verifyEqual(page3.parentDomTypeInitVisible(), true, "Parent Domain Type Visible", t1);
    }

    /**
     * P1_TC_404
     *
     * @throws Exception
     */
    @Test(priority = 100, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_14() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_404", "To verify that Network Admin should be able to select the Zone through \"Iconic button\" while adding Area in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        Geography_Page1 page = new Geography_Page1(t1);

        page.navigateToLink()
                .selectgeo("Area")
                .clicksubmit();

        page.selectZoneFromChildWindow(geo2.ZoneCode);

        String zoneNameTextBox = page.getZoneTextFromTextBox();

        Assertion.verifyEqual(zoneNameTextBox, geo2.ZoneName, "Zone selected from Icon Button", t1);
    }

    @Test(priority = 71, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_15() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_406:Geo", "To verify that when Network Admin clicks on Confirm button on Delete Area Confirmation Page then application should asks about the confirmation to delete Area");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        new Geography_Page1(t1).navigateToLink().selectgeo("Area").clicksubmit().selectzone(geo2.ZoneName);

        new Geography_Page2(t1).selectRadio(geo2.AreaName).clickDeleteButton();

        String alert = AlertHandle.getAlertText(t1);

        Assertion.verifyEqual(alert, MessageReader.getMessage("msg.delete.confirm", null), "Alert Check", t1);

        AlertHandle.rejectAlert(t1);
    }

    /**
     * @throws Exception
     */
    @Test(priority = 72, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_16() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_087:Geo", "To verify that Application allows the user to select the \n" +
                "Zone from Geographical domain Type dropdown.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        String value = new Geography_Page1(t1).navigateToLink().selectgeo("Zone").getSelectedGeo();

        Assertion.verifyEqual(value, "Zone", "Selected Geography type", t1);
    }

    /**
     * TEST : P1_TC_403
     * DESC :To verify that when Network Admin clicks on Confirm button on Delete Zone
     * then application should asksabout the confirmation to delete Zone.
     *
     * @throws Exception
     */
    @Test(priority = 73, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1})
    public void Area_Test_P1_17() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_403:Geo",
                "To verify that when Network Admin clicks on Confirm button on Delete Zone then application should asks about the confirmation to delete Zone.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(optUser);

        new Geography_Page1(t1).navigateToLink().selectgeo("Zone").clicksubmit();

        new Geography_Page2(t1).selectRadio(geo2.ZoneName).clickDeleteButton();

        String alert = AlertHandle.getAlertText(t1);

        Assertion.verifyEqual(alert, MessageReader.getMessage("msg.delete.confirm", null), "Alert Check", t1);

        AlertHandle.rejectAlert(t1);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        ExtentTest t1 = pNode.createNode("Teardown", "Deletes the Created Zones and Areas");
        //GeographyManagement.init(t1).deleteArea(geo);
        /*GeographyManagement.init(t1).deleteArea(geo2);*/
        //GeographyManagement.init(t1).deleteZone(geo);
        /*GeographyManagement.init(t1).deleteZone(geo2);*/
    }

}
