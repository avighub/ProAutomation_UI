package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by shruti.gupta on 10-07-2017.
 */
public class Suite_P1_SubscriberManagement_01 extends TestInit {
    private User usrWholeSaler;
    private User sub_197_213;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        usrWholeSaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
    }

    /**
     * Test:                Description:                                            date:             author:
     * TC197                To verify that while creating subscriber,               10-07-2017
     * on confirm page all the non editable
     * information should be correct.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_034() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_034",
                "To verify that while creating subscriber, " +
                        "on confirm page all the non editable information should be correct.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.SUBSCRIBER_MANAGEMENT);
        try {
            //----Login Wholesaler/retailer----
            sub_197_213 = new User(Constants.SUBSCRIBER);

            Login.init(extentTest).
                    login(usrWholeSaler);

            SubscriberManagement.init(extentTest)
                    .addInitiateSubscriber(sub_197_213);

            if (ConfigInput.isCoreRelease) {
                CommonUserManagement.init(extentTest).assignWebGroupRole(sub_197_213);
            }

            CommonUserManagement.init(extentTest)
                    .mapWalletPreferences(sub_197_213)
                    .mapBankPreferences(sub_197_213);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test:                Description:                                            date:             author:
     * TC256                To verify that after successful registration of         11-07-2017
     * subscriber,subscriber MSISDN should be present
     * in database.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_037() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_037",
                "To verify that after successful registration of subscriber," +
                        "subscriber MSISDN should be present in database.")
                .assignCategory(FunctionalTag.P1, FunctionalTag.SUBSCRIBER_MANAGEMENT);
        try {
            User subs_256 = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(extentTest)
                    .createDefaultSubscriberUsingAPI(subs_256);

            String loginId = MobiquityGUIQueries.getSubscriberLoginId(subs_256.MSISDN);
            Assertion.verifyEqual(subs_256.LoginId, loginId, "Subscriber Exist in DB", extentTest);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TestCase ID:     Description:                                                Date:               Author:
     * NT-0001          Subscriber Creation should fail When                        07-06-2017          Dalia
     * Uploaded File FileSize IsMore Than Specified
     *
     * @throws Exception
     */
    @Test(priority = 4)
    public void NT_0001() throws Exception {
        ExtentTest t1 = pNode.createNode("NT-0001",
                "subscriber Creation Should Fail When Uploaded File FileSize IsMore Than Specified");
        t1.assignCategory(FunctionalTag.P1, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

        User subs = new User(Constants.SUBSCRIBER);
        subs.setPhotoProof(FilePath.maxSizeFilePath); // set the upload file, max size

        Login.init(t1).
                login(usrWholeSaler);

        SubscriberManagement.init(t1)
                .startNegativeTest()
                .addInitiateSubscriber(subs);

        Assertion.verifyErrorMessageContain("subs.upload.content.filesize.invalid", "Upload Max File Size", t1);
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test:                Description:                                            date:             author:
     * TC213                Verify details on Add Subscriber Approval screen        11-07-2017
     * (View All Data link).
     *
     * @throws Exception
     */
    @Test(priority = 5, dependsOnMethods = "P1_TC_034", groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_036() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_036",
                "To verify that while creating subscriber, on confirm page all " +
                        "the non editable information should be correct.");
        try {
            Login.init(extentTest).login(usrWholeSaler);
            SubscriberManagement.init(extentTest)
                    .verifySubscriberApprovalView(sub_197_213);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        }

        Assertion.finalizeSoftAsserts();
    }
}
