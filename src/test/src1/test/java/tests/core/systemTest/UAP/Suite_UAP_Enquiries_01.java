package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases FOr Channel Enquiry
 *
 * @author navin.pramanik
 */
public class Suite_UAP_Enquiries_01 extends TestInit {
    private OperatorUser netStat;
    private User user;

    /**
     * TEST TYPE : POSITIVE
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0})
    public void channelSubsEnq() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0054   : Channel/Subs Enquiry",
                "To verify that the netadmin can do enquiry of system users.").assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.ENQUIRIES, FunctionalTag.ECONET_UAT_5_0);

        netStat = DataFactory.getOperatorUserWithAccess("ENQ_US");

        if (netStat == null) {
            t1.fail("Operator User not present with access 'ENQ_US'. Please check OperatorUser Excel and RnR sheet.");
            Assert.fail("Operator User not present with access 'ENQ_US'. Please check OperatorUser Excel and RnR sheet.");
        }

        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        if (user == null) {
            t1.fail("Channel User not present with Category. Please check ChannelUser Excel file.");
            Assert.fail("Channel User not present with Category. Please check ChannelUser Excel file.");
        }


        Login.init(t1).login(netStat);

        Enquiries.init(t1).channelSubsEnquiry(user, Constants.CHANNEL_USERS_CONST);
        Assertion.finalizeSoftAsserts();

    }

}
