package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.adminTrailManagement.AuditAdminTrail;
import framework.features.common.Login;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 8/9/2017.
 */
public class SuiteSystemAdminTrail01 extends TestInit {


    private static String domainCode;
    private static String categoryCode;
    private static String fromDate;
    private static String toDate;
    private static SuperAdmin saAuditTrail, saAdminTrail;
    private static User cashinUser, subs;
    private static OperatorUser netAdmin;

    /**
     * SETUP
     */
    @BeforeMethod(alwaysRun = true)
    public void setup() throws Exception {
        try {
            categoryCode = Constants.WHOLESALER;
            domainCode = DataFactory.getDomainCode(Constants.WHOLESALER);
            fromDate = new DateAndTime().getDate(-5);
            toDate = new DateAndTime().getDate(0);
            saAdminTrail = DataFactory.getSuperAdminWithAccess("ADMIN_TRAIL");
            netAdmin = new OperatorUser(Constants.NETWORK_ADMIN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : POSITIVE
     * ID : SYS_TC_AdminTrail_S_0009
     * DESC : To verify that "Network Admin" should be able to the see details in Admin Trail and able to download file.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_AdminTrail_S_0009() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AdminTrail_S_0009",
                "To verify that \"Network Admin\" should be able to the see details in Admin Trail and able to download file.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.AUDIT_TRAIL);

        try {
            netAdmin = DataFactory.getOperatorUserWithAccess("ADMIN_TRAIL", Constants.NETWORK_ADMIN);
            fromDate = new DateAndTime().getDate(-20);
            Login.init(t1).login(netAdmin);
            AuditAdminTrail.init(t1).adminTrail(domainCode, categoryCode, fromDate, toDate);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * TEST : POSITIVE
     * ID : SYS_TC_AdminTrail_S_0001
     * DESC : To verify that admin trail should display the detail of admin/system users created in the system with in the date range mentioned in "From date" and "To date".
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.PVG_P1, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_562() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_562", "To verify that admin trail should display the detail of admin/system users created in the system with in the date range mentioned in \"From date\" and \"To date\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ADMIN_TRAIL, FunctionalTag.PVG_P1);


        try {
            OperatorUser optUser = new OperatorUser(Constants.NETWORK_ADMIN);
            OperatorUserManagement.init(t1).initiateAndApproveOptUser(optUser);
            Login.init(t1).loginAsSuperAdmin(saAdminTrail);
            AuditAdminTrail.init(t1).adminTrail(optUser, fromDate, toDate);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID :      * DESC :
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1})
    public void P1_TC_563() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_563", "To verify that admin trail should display the following fields:\n" +
                "1. User ID\n" + "2. User MSISDN\n" + "3. Logged In\n" + "4. Log Out\n" + "5. Category\n" + "6. Action Type\n" + "7. Action Performed On\n" + "8. Barred User\n" + "9. Remarks\n" +
                "10. Created By\n" + "11. Created On\n" + "12. Att 1 Name\n" + "13. Att 1 Value\n" + "14. Att 2 Name\n" + "15. Att 2 Value\n" + "16. Att 3 Name\n" + "17. Att 3 Value");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_P1);
        netAdmin.DomainCode = DataFactory.getDomainCode(Constants.NETWORK_ADMIN);

        Login.init(t1).loginAsSuperAdmin(saAdminTrail);

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().adminTrail(netAdmin, fromDate, toDate);

        AuditAdminTrail.init(t1).verifyAdminTrailHeaders();
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AdminTrail_S_0003
     * DESC : To verify that "Network Admin" should not be able to view the details when "Domain name" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AdminTrail_S_0003() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AdminTrail_S_0003", "To verify that \"Network Admin\" should not be able to view the details when \"Domain name\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin(saAdminTrail);

        netAdmin.DomainCode = null;
        netAdmin.CategoryCode = null;

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().adminTrail(netAdmin, fromDate, toDate);
        Assertion.verifyErrorMessageContain("audit.domain.required", "Verify Error Message When Domain Name is not selected.", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AdminTrail_S_0004
     * DESC : To verify that "Network Admin" should not be able to view the details when "Domain name" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AdminTrail_S_0004() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AdminTrail_S_0004", "To verify that \"Network Admin\" should not be able to view the details when \"Category name\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);
        netAdmin.DomainCode = DataFactory.getDomainCode(Constants.NETWORK_ADMIN);

        Login.init(t1).loginAsSuperAdmin(saAdminTrail);

        netAdmin.CategoryCode = null;

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().adminTrail(netAdmin, fromDate, toDate);
        Assertion.verifyErrorMessageContain("audit.category.required", "Verify Error Message When Category Name is not selected.", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AdminTrail_S_0005
     * DESC : To verify that "Network Admin" should not be able to view the details when "From Date" is not Entered.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AdminTrail_S_0005() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AdminTrail_S_0005",
                "To verify that \"Network Admin\" should not be able to view the details when \"From Date\" is not Entered.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin(saAdminTrail);

        fromDate = Constants.BLANK_CONSTANT;

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().adminTrail(netAdmin, fromDate, toDate);
        Assertion.verifyErrorMessageContain("audittrail.validation.fromdate", "Verify Error Message When From Date is not Entered.", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AdminTrail_S_0006
     * DESC : To verify that "Network Admin" should not be able to view the details when "From Date" is not Entered.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AdminTrail_S_0006() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AdminTrail_S_0006",
                "To verify that \"Network Admin\" should not be able to view the details when \"To Date\" is not Entered.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin(saAdminTrail);

        toDate = Constants.BLANK_CONSTANT;

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().adminTrail(netAdmin, fromDate, toDate);
        Assertion.verifyErrorMessageContain("audittrail.validation.todate", "Verify Error Message When \"To Date\" is not Entered.", t1);
    }


    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AdminTrail_S_0007
     * DESC : To verify that "Network Admin" should not be able to view the details when "From Date" is the future date.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AdminTrail_S_0007() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AdminTrail_S_0007",
                "To verify that \"Network Admin\" should not be able to view the details when \"From Date\" is the future date.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin(saAdminTrail);

        fromDate = new DateAndTime().getDate(+1);

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().adminTrail(netAdmin, fromDate, toDate);
        Assertion.verifyErrorMessageContain("btsl.error.msg.fromdatebeforecurrentdate", "Verify Error Message When \"From Date\" is Future Date.", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AdminTrail_S_0008
     * DESC : To verify that "Network Admin" should not be able to view the details when "To Date" is the future date.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AdminTrail_S_0008() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AdminTrail_S_0008",
                "To verify that \"Network Admin\" should not be able to view the details when \"To Date\" is the future date.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin(saAdminTrail);

        toDate = new DateAndTime().getDate(+1);

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().adminTrail(netAdmin, fromDate, toDate);
        Assertion.verifyErrorMessageContain("btsl.error.msg.todatebeforecurrentdate", "Verify Error Message When \"To Date\" is Future Date.", t1);
    }
}
