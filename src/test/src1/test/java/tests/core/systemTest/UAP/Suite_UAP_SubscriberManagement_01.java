package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;


public class Suite_UAP_SubscriberManagement_01 extends TestInit {

    private User subscriber;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        subscriber = new User(Constants.SUBSCRIBER);
    }

    /**
     * Test: TC197
     * Description: To verify that  valid user can add subscriber
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.SUBSCRIBER_MANAGEMENT,
            FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
            FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.DEPENDENCY_TAG})
    public void addSubs() throws Exception {

        ExtentTest subsTest = pNode.createNode("TC0143",
                "Add Subscriber, To verify that the wholesaler can add subscriber.")
                .assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            SubscriberManagement.isBankRequired = false;

            /*SubscriberManagement.init(subsTest)
                    .createSubscriber(subscriber);*/

            SubscriberManagement.init(subsTest)
                    .createSubscriberDefaultMapping(subscriber,true,false);
        } catch (Exception e) {
            markTestAsFailure(e, subsTest);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(dependsOnMethods = "addSubs", priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP,
            FunctionalTag.ECONET_UAT_5_0, FunctionalTag.DEPENDENCY_TAG})
    public void modifySubsTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0141",
                "Modify Subscriber, To verify that the wholesaler can modify subscriber.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            SubscriberManagement.init(t1).
                    subsModifyInitAndApprove(subscriber);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : Suspend/Resume Test
     * DESC : To verify that the valid user can suspend/resume subscriber.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM,
            FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.DEPENDENCY_TAG}, dependsOnMethods = "addSubs")
    public void suspendResumeTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0142",
                "Suspend/Resume Subscriber: To verify that the netadmin can suspend/resume subscriber.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            SubscriberManagement.init(t1).
                    suspendAndResumeSubscriber(subscriber, Constants.USER_TYPE_SUBS);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : Delete Subscriber
     * DSEC : To verify that the valid user can delete subscriber.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP})
    public void deleteSubsTest() throws Exception {
        ExtentTest delSubsTest;
        ExtentTest setup = pNode.createNode("Setup For Deletion", "To verify that the wholesaler can delete subscriber.");
        OperatorUser optUser = new OperatorUser(Constants.NETWORK_ADMIN);

        ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE, subscriber, optUser, null, null, null, null);
        ServiceChargeManagement.init(setup)
                .configureServiceCharge(sChargeAccClose);

        //To give subscriber balance for account closure service charge
        TransactionManagement.init(setup).makeSureLeafUserHasBalance(subscriber, new BigDecimal("10"));

        if (!ConfigInput.isCoreRelease) {
            delSubsTest = pNode.createNode("TC0144: Delete Subscriber By Agent", "To verify that the wholesaler can delete subscriber by Agent.");

            delSubsTest.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT);

            SubscriberManagement.init(delSubsTest).deleteSubscriberByAgent(subscriber, DataFactory.getDefaultProvider().ProviderId, true);
        } else {

            delSubsTest = pNode.createNode("TC198: Delete Subscriber", "To verify that the wholesaler can delete subscriber.");

            delSubsTest.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT);

            String tid = SubscriberManagement.init(delSubsTest)
                    .deleteSubscriber(subscriber, DataFactory.getDefaultProvider().ProviderId);

            //Account Closure by subs
            if (tid != null) {
                Transactions.init(pNode).subsAccountClosure(subscriber, tid);
            }

        }

    }
}
