package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.pageObjects.serviceCharge.ViewServiceCharge_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by rahul.rana on 6/22/2017.
 */
public class Suite_Smoke_TSCG1220 extends TestInit {

    private MobiquityGUIQueries dbHandler;
    private ServiceCharge sCharge;
    private OperatorUser usrSChargeCreator, usrSChargeApprover, usrSChargeModify, usrSChargeView;
    private List<String> notActiveStatus = Arrays.asList("UIr", "DI", "AI");
    private String initialApplicableDate = null;

    /**
     * Test         :   Setup
     * Description  :   Pre-Requisite for Service Charge Test Creation, Modification and Deletion
     * Date         :   6/22/2017
     * Author       :   rahul rana
     * NOTE - Observe that the same service charge Object is used across suite,
     * hence do not alter the test case sequence
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        // Create the Suite Node

        ExtentTest s1 = pNode.createNode("Setup", "Setup specific for this suite");
        try {
            dbHandler = new MobiquityGUIQueries();

            usrSChargeCreator = DataFactory.getOperatorUserWithAccess("SVC_ADD");
            usrSChargeApprover = DataFactory.getOperatorUserWithAccess("SVC_APP");
            usrSChargeModify = DataFactory.getOperatorUserWithAccess("SVC_MOD");
            usrSChargeView = DataFactory.getOperatorUserWithAccess("SVC_VIEW");

            User whs = new User(Constants.WHOLESALER);
            User usrRetailer = new User(Constants.RETAILER);

            sCharge = new ServiceCharge(Services.TXN_CORRECTION,
                    usrRetailer, whs,
                    null, null, null, null);

            // Configure Transfer Rule
            TransferRuleManagement.init(s1).
                    configureTransferRule(sCharge);

            // Delete the service Charge if already exist
            ServiceChargeManagement.init(s1)
                    .deleteServiceCharge(sCharge);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test             :       TUNG51346
     * <p>
     * Description      :       Add Service Charge To verify that, valid user can initiate addition of
     * Service Charge for Payment Instrument as  Wallet.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.MONEY_SMOKE})
    public void TUNG51346() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51346", "Add Service Charge" +
                "To verify that valid user can initiate  addition of  Service Charge for Payment Instrument as  Wallet.");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            Login.init(t1).login(usrSChargeCreator);
            ServiceChargeManagement.init(t1)
                    .addInitiateServiceCharge(sCharge)
                    .setCommissionDetail(sCharge)
                    .completeServiceChargeCreation(sCharge);

            // Verify that the Service Charge Status is Not Active Yet
            Map<String, String> dbSCharge = dbHandler.dbGetServiceCharge(sCharge);
            Assertion.verifyNotEqual(dbSCharge.get("Status"), "Y", "Status is NOT Active", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test             :       TUNG51348
     * <p>
     * Description      :       Service Charge Approval To verify that valid user can Reject Service Charge
     * for Payment Method Wallet.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.MONEY_SMOKE})
    public void TUNG51348() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51348", "Service Charge >> Service Charge Approval " +
                "To verify that valid user can Reject Service Charge for Payment Method Wallet.");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            Login.init(t1).login(usrSChargeApprover);
            ServiceChargeManagement.init(t1)
                    .rejectServiceCharge(sCharge);

            // Verify that the Service Can Be Crated with The Same Name
            t1.info("Create Service Charge with the Same name as of the rejected serviceCharge");
            Login.init(t1).login(usrSChargeCreator);
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Test             :       TUNG51350
     * <p>
     * Description      :       Service Charge Approval To verify that valid user can modify Existing Service Charge
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void TUNG51350() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51350", "Service Charge >> Service Charge Approval " +
                "To verify that valid user can modify Existing Service Charge");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            // Make sure that Service Charge is configured

            ViewServiceCharge_Pg1 page = ViewServiceCharge_Pg1.init(t1);

        /*
        Modify the Applicable Date and update the existing service Charge
         */
            String newDate = DataFactory.getNextDate(8);
            sCharge.setApplicableDate(newDate);

            Login.init(t1).login(usrSChargeModify);
            ServiceChargeManagement.init(t1)
                    .modifyServiceChargeGeneralInfo(sCharge);

            // View Service Charge verify that modification is not shown as Modifications are not approved Yet
            Login.init(t1).login(usrSChargeView);
            ServiceChargeManagement.init(t1)
                    .startNegativeTest()
                    .viewServiceChargesLatestVersion(sCharge, "1");
            Assertion.verifyNotEqual(page.fetchApplicableDate(), newDate, "Modified Date should Not reflect", t1);

            // TearDown
            ServiceChargeManagement.init(t1)
                    .approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_MODIFY);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test             :       TUNG51351
     * <p>
     * Description      :       Service Charge Approval To verify that valid User can Approve any modification
     * made to an Service Charge.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void TUNG51351() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51351", "Service Charge >> Service Charge Approval " +
                "To verify that valid user can Approve any modification made to an Service Charge");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
 /*
        Modify the Applicable Date and update the existing service Charge
         */
            String newDate = DataFactory.getNextDate(10);
            sCharge.setApplicableDate(newDate);

            Login.init(t1).login(usrSChargeModify);
            ServiceChargeManagement.init(t1)
                    .modifyServiceChargeGeneralInfo(sCharge)
                    .approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_MODIFY);

            // View Service Charge verify that modification are now shown as Modifications are Approved
            Login.init(t1).login(usrSChargeView);
            ServiceChargeManagement.init(t1)
                    .viewServiceChargesLatestVersion(sCharge, "1");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test             :       TUNG51352
     * <p>
     * Description      :       Service Charge Approval To verify that valid User can reject any modification
     * made to an Service Charge.
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void TUNG51352() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51352", "Service Charge >> Service Charge Approval " +
                "To verify that valid user can reject any modification made to an Service Charge");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            ViewServiceCharge_Pg1 page = ViewServiceCharge_Pg1.init(t1);

        /*
        Modify the Applicable Date and update the existing service Charge
         */
            String newDate = DataFactory.getNextDate(2);
            sCharge.setApplicableDate(newDate);

            Login.init(t1).login(usrSChargeModify);
            ServiceChargeManagement.init(t1)
                    .modifyServiceChargeGeneralInfo(sCharge)
                    .rejectServiceCharge(sCharge);

            // View Service Charge verify that modification is not shown as Modifications are Rejected
            Login.init(t1).login(usrSChargeView);
            ServiceChargeManagement.init(t1)
                    .startNegativeTest()
                    .viewServiceChargesLatestVersion(sCharge, "1");
            Assertion.verifyNotEqual(page.fetchApplicableDate(), newDate, "Modified Date should Not reflect", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test             :       TUNG51353
     * <p>
     * Description      :       Service Charge Approval To verify that valid User can delete initiate a service Charge.
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void TUNG51353() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51353", "Service Charge >> Service Charge Approval " +
                "To verify that valid User can delete initiate a service Charge");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            Login.init(t1).login(usrSChargeModify);
            ServiceChargeManagement.init(t1)
                    .initiateDeleteServiceCharge(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Test             :       TUNG51353
     * <p>
     * Description      :       Service Charge Approval To verify that valid User can reject delete initiate
     * made to an Service Charge.
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void TUNG51355() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51355", "Service Charge >> Service Charge Approval " +
                "To verify that valid user can reject delete initiate made to an Service Charge");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            Login.init(t1).login(usrSChargeModify);
            ServiceChargeManagement.init(t1)
                    .rejectServiceCharge(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test             :       TUNG51354
     * <p>
     * Description      :       Service Charge Approval To verify that valid User can Approve Delete Initiate
     * made to an Service Charge.
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void TUNG51354() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51354", "Service Charge >> Service Charge Approval " +
                "To verify that valid user can Approve delete initiate made to an Service Charge");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            Login.init(t1).login(usrSChargeModify);
            ServiceChargeManagement.init(t1)
                    .initiateDeleteServiceCharge(sCharge)
                    .approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_DELETE);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Test             :       TUNG51347
     * <p>
     * Description      :       Service Charge Approval To verify that valid user can Approve Add Initiate Service Charge
     * for Payment Method Wallet.
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void TUNG51347() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51347", "Service Charge >> Service Charge Approval " +
                "To verify that valid user can Approve Service Charge for Payment Method Wallet.");
        t1.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
// Delete the service Charge if already exist
            ServiceChargeManagement.init(t1)
                    .deleteServiceCharge(sCharge);

            // Set Sort Name, as sort name cannot be of same as deleted Service Charge - TODO: need to verify this with QA
            sCharge.setShortName(sCharge.ServiceChargeId + DataFactory.getRandomNumber(3));
            sCharge.setApplicableDate(initialApplicableDate); // re setting the initial Applicable date, as it was modified in previous tests

            Login.init(t1).login(usrSChargeCreator);
            ServiceChargeManagement.init(t1)
                    .addInitiateServiceCharge(sCharge)
                    .setCommissionDetail(sCharge)
                    .completeServiceChargeCreation(sCharge);

            Login.init(t1).login(usrSChargeApprover);
            ServiceChargeManagement.init(t1)
                    .approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_CREATE);

            // View Service Charge
            Login.init(t1).login(usrSChargeView);
            ServiceChargeManagement.init(t1)
                    .viewServiceChargesLatestVersion(sCharge, "1");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
