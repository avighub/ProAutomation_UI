package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.entity.WebGroupRole;
import framework.features.blackListManagement.BlackListManagement;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.blacklist.CustomerBlacklist_Page;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/**
 * Created by Febin.Thomas on 5/14/2018.
 */

public class SuiteSystemCustomerBlacklisting extends TestInit {

    private static String firstName2, lastName2, dob2, firstName, lastName, dob, blklMsisdn = null;
    private static User blackListedUser;
    SuperAdmin saMaker;
    OperatorUser custblkl;
    private WebGroupRole webGroupRole;
    private List<String> applicableRole;

    /**
     * SETUP
     */

    @BeforeTest(alwaysRun = true)
    public void prerequisite() throws Exception {
        firstName = "BLKL" + DataFactory.getRandomNumber(3);
        lastName = "BLKL" + DataFactory.getRandomNumber(3);
        dob = new DateAndTime().getYear(-20);
        firstName2 = "BLKL2" + DataFactory.getRandomNumber(3);
        lastName2 = "BLKL2" + DataFactory.getRandomNumber(3);
        dob2 = new DateAndTime().getYear(-20);
        blklMsisdn = DataFactory.getAvailableMSISDN();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 1)
    public void custBlacklist_1() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0001", "To verify that blacklist customers is not able to perform financial services.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        User subs = new User(Constants.SUBSCRIBER);

        SubscriberManagement.init(t1).createSubscriber(subs);

        BlackListManagement.init(t1).doCustomerBlacklist(subs.FirstName, subs.LastName, subs.DateOfBirth);

        User cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        Login.init(t1).login(cashinUser);

        startNegativeTest();

        TransactionManagement.init(t1).performCashIn(subs, Constants.MIN_CASHIN_AMOUNT);

        Assertion.verifyErrorMessageContain("00036", "User Blacklisted", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.CRITICAL_CASES_TAG}, priority = 2)
    public void custBlacklist_2() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0002", "To verify that CUST_BLKLIST_CHECK preference under system preference can be made configurable.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        Preferences.init(t1).modifySystemPreferences("CUST_BLKLIST_CHECK", "FALSE");

        BlackListManagement.init(t1).doCustomerBlacklist(firstName2, lastName2, dob2);

        Preferences.init(t1).modifySystemPreferences("CUST_BLKLIST_CHECK", "TRUE");

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 3)
    public void custBlacklist_3() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0003", "To verify that proper error is displayed when \"First Name\" is left blank and submitted on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(Constants.BLANK_CONSTANT, lastName, dob);

        Assertion.verifyErrorMessageContain("blacklist.empty.firstName", "First Name", t1);


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 4)
    public void custBlacklist_4() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0004", "To verify that proper error is displayed when \"Last Name\" is left blank and submitted on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, Constants.BLANK_CONSTANT, dob);

        Assertion.verifyErrorMessageContain("blacklist.empty.lastName", "Last Name", t1);


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 5)
    public void custBlacklist_5() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0005", "To verify that proper error is displayed when \"Date Of Birth\" is left blank and submitted on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, lastName, Constants.BLANK_CONSTANT);

        Assertion.verifyErrorMessageContain("blacklist.empty.dateOfBirth", "Date Of Birth", t1);


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 6)
    public void custBlacklist_6() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0006", "To verify that proper error is displayed when \"Special Characters\" are passed in\"First Name\" and submitted on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(Constants.SPECIAL_CHARACTER_CONSTANT, lastName, dob);

        Assertion.verifyErrorMessageContain("systemparty.error.firstnameformat", "First Name", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 7)
    public void custBlacklist_7() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0007", "To verify that proper error is displayed when \"Special Characters\" are passed in\"Last Name\" and submitted on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, Constants.SPECIAL_CHARACTER_CONSTANT, dob);

        Assertion.verifyErrorMessageContain("systemparty.error.lastnameformat", "Last Name", t1);


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 8)
    public void custBlacklist_8() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0008", "To verify that proper error is displayed when \"NON- CSV file\" are passed in\" file upload\" and submitted on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateJunkFile();

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        CustomerBlacklist_Page page = new CustomerBlacklist_Page(pNode);
        page.clickReset();

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        Assertion.verifyErrorMessageContain("billUpload.nofile.error", "CSV file", t1);


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 9)
    public void custBlacklist_9() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0009", "To verify that \"Reset\" button works correctly on \"Subscriber Management ->Add Customer Blacklist Page\"\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, lastName, dob);

        CustomerBlacklist_Page page = new CustomerBlacklist_Page(t1);
        page.clickBack();
        page.clickReset();
        boolean check = page.resetCheck();

        Assertion.verifyEqual(check, true, "Verify Reset Button", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 10)
    public void custBlacklist_10() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0010", "To verify that \"Back\" button works correctly on \"Subscriber Management ->Add Customer Blacklist-> Confirm Page.\"\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, lastName, dob);

        CustomerBlacklist_Page page = new CustomerBlacklist_Page(t1);
        page.clickBack();
        boolean check = page.checkSubscriberManagement();

        Assertion.verifyEqual(check, true, "Verify back button", t1);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 11)
    public void custBlacklist_11() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0011", "To verify that \"First Name\" entered on \"Subscriber Management ->Add Customer Blacklist\" is same on  \"Subscriber Management ->Add Customer Blacklist-> Confirm Page\".\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, lastName, dob);

        CustomerBlacklist_Page page = new CustomerBlacklist_Page(t1);

        String fname = page.getFirstName();

        Assertion.verifyEqual(fname, firstName, "Verify First Name", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 12)
    public void custBlacklist_12() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0012", "To verify that \"Last Name\" entered on \"Subscriber Management ->Add Customer Blacklist\" is same on  \"Subscriber Management ->Add Customer Blacklist-> Confirm Page\".\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, lastName, dob);

        CustomerBlacklist_Page page = new CustomerBlacklist_Page(t1);

        String lname = page.getLastName();

        Assertion.verifyEqual(lname, lastName, "Verify Last Name", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 13)
    public void custBlacklist_13() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0013", "To verify that \"Date Of Birth\" entered on \"Subscriber Management ->Add Customer Blacklist\" is same on  \"Subscriber Management ->Add Customer Blacklist-> Confirm Page\".\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, lastName, dob);

        CustomerBlacklist_Page page = new CustomerBlacklist_Page(t1);

        String dateofB = page.getDOB();

        Assertion.verifyEqual(dob2, dob, "Verify Date Of Birth", t1);

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 14)
    public void custBlacklist_14() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0014", "To verify that proper error is displayed when \"First Name\" is left blank and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, Constants.BLANK_CONSTANT, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        Assertion.verifyErrorMessageContain("subs.error.blacklist.errror", "CSV file", t1);


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 15)
    public void custBlacklist_15() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0015", "To verify that proper error is displayed when \"Last Name\" is left blank and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, Constants.BLANK_CONSTANT, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        Assertion.verifyErrorMessageContain("subs.error.blacklist.errror", "CSV file", t1);


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 16)
    public void custBlacklist_16() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0016", "To verify that proper error is displayed when \"Date Of Birth\" is left blank and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, lastName, Constants.BLANK_CONSTANT);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        Assertion.verifyErrorMessageContain("subs.error.blacklist.errror", "CSV file", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 17)
    public void custBlacklist_17() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0017", "To verify that proper error is displayed when \"MSISDN\" is left blank and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(Constants.BLANK_CONSTANT, firstName, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        Assertion.verifyErrorMessageContain("subs.error.blacklist.errror", "CSV file", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 18)
    public void custBlacklist_18() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0018", "To verify that proper error is displayed when \"Special Characters\" are passed in\"First Name\" and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, Constants.SPECIAL_CHARACTER_CONSTANT, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);
        String status = pg.getStatus();

        Assertion.verifyContains(status, MessageReader.getMessage("systemparty.error.firstnameformat", null), "Status of File", t1, true);
        pg.clickBulkConfirm();

        Assertion.verifyErrorMessageContain("bulk.payment.failure", "CSV file", t1);


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 19)
    public void custBlacklist_19() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0019", "To verify that proper error is displayed when \"Special Characters\" are passed in\"Last Name\" and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, Constants.SPECIAL_CHARACTER_CONSTANT, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);
        String status = pg.getStatus();

        Assertion.verifyContains(status, MessageReader.getMessage("systemparty.error.lastnameformat", null), "Status of File", t1, true);
        pg.clickBulkConfirm();

        Assertion.verifyErrorMessageContain("bulk.payment.failure", "CSV file", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 20)
    public void custBlacklist_20() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0020", "To verify that proper error is displayed when \"Special Characters\" are passed in\"Date Of Birth\" and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, lastName, Constants.SPECIAL_CHARACTER_CONSTANT);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);
        String status = pg.getStatus();

        Assertion.verifyContains(status, MessageReader.getMessage("blacklist.dateOfBirthNotValid", null), "Status of File", t1, true);
        pg.clickBulkConfirm();

        Assertion.verifyErrorMessageContain("bulk.payment.failure", "CSV file", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 21)
    public void custBlacklist_21() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0021", "To verify that proper error is displayed when \"Special Characters\" are passed in\"MSISDN \" and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(Constants.SPECIAL_CHARACTER_CONSTANT, firstName, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);
        String status = pg.getStatus();

        Assertion.verifyContains(status, MessageReader.getMessage("opt.error.msisdnprefixnotmatch", null), "Status of File", t1, true);
        pg.clickBulkConfirm();

        Assertion.verifyErrorMessageContain("bulk.payment.failure", "CSV file", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 22)
    public void custBlacklist_22() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0022", "To verify that proper error is displayed when \"AlphaNumeric Characters\" are passed in\"Date Of Birth\" and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, lastName, Constants.ALPHANUMERIC_CONSTANT);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);
        String status = pg.getStatus();

        Assertion.verifyContains(status, MessageReader.getMessage("blacklist.dateOfBirthNotValid", null), "Status of File", t1, true);
        pg.clickBulkConfirm();

        Assertion.verifyErrorMessageContain("bulk.payment.failure", "CSV file", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 23)
    public void custBlacklist_23() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0023", "To verify that proper error is displayed when \"AlphaNumeric Characters\" are passed in\"MSISDN \" and Uploaded on  \"Add Customer Blacklist\" page.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(Constants.ALPHANUMERIC_CONSTANT, firstName, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);
        String status = pg.getStatus();

        Assertion.verifyContains(status, MessageReader.getMessage("opt.error.msisdnprefixnotmatch", null), "Status of File", t1, true);
        pg.clickBulkConfirm();

        Assertion.verifyErrorMessageContain("bulk.payment.failure", "CSV file", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 24)
    public void custBlacklist_24() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0024", "To verify that \"First Name\" Uploaded on \"Subscriber Management ->Add Customer Blacklist\" is same on  \"Subscriber Management ->Add Customer Blacklist-> Confirm Page\".\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);

        String var = pg.getBulkFname();
        Assertion.verifyEqual(var, firstName, "Verify First Name", t1);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 25)
    public void custBlacklist_25() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0025", "To verify that \"Last Name\" Uploaded on \"Subscriber Management ->Add Customer Blacklist\" is same on  \"Subscriber Management ->Add Customer Blacklist-> Confirm Page\".\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);

        String var = pg.getBulkLname();
        Assertion.verifyEqual(var, lastName, "Verify Last Name", t1);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 26)
    public void custBlacklist_26() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0026", "To verify that \"Date Of Birth\" Uploaded on \"Subscriber Management ->Add Customer Blacklist\" is same on  \"Subscriber Management ->Add Customer Blacklist-> Confirm Page\".\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);

        String var = pg.getBulkDob();

        Assertion.verifyEqual(var, dob, "Verify Date Of Birth", t1);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 27)
    public void custBlacklist_27() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0027", "To verify that \"Msisdn\" Uploaded on \"Subscriber Management ->Add Customer Blacklist\" is same on  \"Subscriber Management ->Add Customer Blacklist-> Confirm Page\".\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn, firstName, lastName, dob);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);

        String var = pg.getBulkMsisdn();

        Assertion.verifyEqual(var, blklMsisdn, "Verify MSISDN", t1);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 28)
    public void custBlacklist_28() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0028", "To verify that proper error is thrown when the \"Headers\" of the file are not according to the template.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkInvalidHeaderCsvFile(blklMsisdn);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTest();

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        Assertion.verifyErrorMessageContain("blacklist.error.invalidDataCount", "CSV file", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 29)
    public void custBlacklist_29() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_0029", "To verify that proper error is displayed when Both \"First Name, last name and DOB\" and \"CSV upload\" is submitted.\n");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(blklMsisdn);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, lastName, dob);

        CustomerBlacklist_Page pg = new CustomerBlacklist_Page(t1);
        pg.clickBack();
        pg.uploadFile(fileName);
        pg.clickSubmitButton();

        Assertion.verifyErrorMessageContain("blacklist.wrong.entered", "CSV file", t1);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1}, priority = 30)
    public void custBlacklist_30() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_183", "To verify that system should not allow blacklisting of an already blacklisted customer.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.PVG_P1);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        blackListedUser = CommonUserManagement.init(t1).getBlacklistedUser(Constants.SUBSCRIBER, null);

        startNegativeTestWithoutConfirm();

        BlackListManagement.init(t1).doCustomerBlacklist(blackListedUser.FirstName, blackListedUser.LastName, blackListedUser.DateOfBirth);

        Assertion.verifyErrorMessageContain("blacklist.alreadyBlacklisted", "Blacklist Duplicacy", t1);

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1}, priority = 31)
    public void custBlacklist_31() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_182_1", "Bulk Upload: To verify that after Blacklisting a user which is already available in system through Bulk upload process ,then user is not allowed to be part of any financial transaction.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.PVG_P1);

        User subs = new User(Constants.SUBSCRIBER);

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(subs.MSISDN);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        User cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        startNegativeTest();

        TransactionManagement.init(t1).makeSureChannelUserHasBalance(cashinUser);

        Login.init(t1).login(cashinUser);

        TransactionManagement.init(t1).performCashIn(subs, Constants.MIN_CASHIN_AMOUNT);

        Assertion.verifyErrorMessageContain("00036", "User Blacklisted", t1);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1}, priority = 31)
    public void custBlacklist_32() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_182_2", "Bulk Upload: To verify that when a customer is blacklisted through bulk upload then the system should not allow to create subscriber with the same details.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.PVG_P1);

        User subs = new User(Constants.SUBSCRIBER);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(subs.MSISDN, subs.FirstName, subs.LastName, subs.DateOfBirth);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);

        User chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");

        Login.init(t1).login(chAddSubs);

        SubscriberManagement.init(t1).addInitiateSubscriber(subs);

        //String expectedMsg = MessageReader.getDynamicMessage("blacklist.subs.blacklisted.added", subs.FirstName, subs.LastName);

        Assertion.verifyErrorMessageContain("blacklist.subs.blacklisted.added", "Check Subs Addition", t1, subs.FirstName, subs.LastName);

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1}, priority = 32)
    public void custBlacklist_33() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_557_1", "To verify that after BlackListing a registered subscriber using First Name,Last Name and Date Of Birth,  the user should not be allowed to perform any financial transaction.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.PVG_P1);

        User subs = new User(Constants.SUBSCRIBER);

        SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        BlackListManagement.init(t1).doCustomerBlacklist(subs.FirstName, subs.LastName, subs.DateOfBirth);

        User cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

        startNegativeTest();

        TransactionManagement.init(t1).makeSureChannelUserHasBalance(cashinUser);

        Login.init(t1).login(cashinUser);

        TransactionManagement.init(t1).performCashIn(subs, Constants.MIN_CASHIN_AMOUNT);

        Assertion.verifyErrorMessageContain("00036", "User Blacklisted", t1);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1}, priority = 32)
    public void custBlacklist_34() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_557_2",
                "To verify that when a customer is blacklisted using First Name,Last Name & Date Of Birth , then Channel User should not be able to create subscriber with the same details.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.PVG_P1);

        User subs = new User(Constants.SUBSCRIBER);

        subs.FirstName = "TEMP" + DateAndTime.getTimeStampDayAndTime();
        subs.LastName = "TEMP" + DateAndTime.getTimeStampDayAndTime();

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        Login.init(t1).login(custblkl);

        BlackListManagement.init(t1).doCustomerBlacklist(subs.FirstName, subs.LastName, subs.DateOfBirth);

        User chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");

        Login.init(t1).login(chAddSubs);

        SubscriberManagement.init(t1).addInitiateSubscriber(subs);
        //String expectedMsg = MessageReader.getDynamicMessage("blacklist.subs.blacklisted.added", subs.FirstName, subs.LastName);

        Assertion.verifyErrorMessageContain("blacklist.subs.blacklisted.added", "Check Subs Addition", t1, subs.FirstName, subs.LastName);


    }


}

