/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: surya.dhal
 *  Date: 27-Nov-2017
 *  Purpose: Test of C2C
 */

package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by surya.dhal on 11/27/2017.
 */
public class SuiteSystemC2C extends TestInit {

    private static OperatorUser usrTRuleCreator;
    private User payer, payee;
    private User barAsSender, barAsReceiver, suspendChUser;
    private ServiceCharge sCharge;
    private String expected, alertMsg, c2cTxnAmount;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        payer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER, 0);
        payee = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER, 1);
        usrTRuleCreator = DataFactory.getOperatorUserWithAccess("T_RULES");
        preCondition();
    }

    public boolean preCondition() throws Exception {
        boolean found;
        ExtentTest t1 = pNode.createNode("Setup", "Setup specific to this test");
        try {
            barAsSender = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
            barAsReceiver = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_RECIEVER, null);
            suspendChUser = CommonUserManagement.init(t1).getSuspendedUser(Constants.WHOLESALER, null);

            if (ConfigInput.isCoreRelease) {
                sCharge = new ServiceCharge(Services.C2C, payer, payee, null, null, null, null);
            } else {
                sCharge = new ServiceCharge(Services.P2P, payer, payee, null, null, null, null);
            }

            c2cTxnAmount = getTransactionAmount(sCharge);

            found = true;

        } catch (NullPointerException ne) {
            found = false;
        } catch (Exception ne) {
            found = false;
        }
        return found;
    }

    public void checkPre(User payer, User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payer, User payee) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test to Perform C2C to All Users with All Grades
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_001() throws Exception {

        //MAKING PAYER LIST
        List<User> user_list = new ArrayList<>();
        User usr1 = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER, 0);
        User usr2 = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER, 1);
        user_list.add(usr1);
        user_list.add(usr2);
        List<User> payer_list = new ArrayList<User>();
        List<User> payee_list = new ArrayList<User>();

        for (User user : user_list) {
            Boolean flag = false;

            if (!(user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER) || user.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE))) {
                //looping through existing list to not add if same grade code already added in list
                Iterator<User> iterator = payer_list.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().GradeCode.equals(user.GradeCode)) {
                        flag = true;
                    }
                }
                if (flag == false) {
                    payer_list.add(user);
                }
            }
        }

        //MAKING PAYEE LIST
        Map<String, User> user2_list = DataFactory.getChannelUserSet(true);

        // List<User> payee_list = new ArrayList<User>();
        for (User userL : user2_list.values()) {
            Boolean flag2 = false;
            //Check Grade code of  userL so that if there are multiple users with that grade code then add the other user which is not in payer list of same grade
            for (User payer_user : payer_list) {
                if (!(userL.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER) || userL.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE))) {

                    if (userL.GradeCode.equalsIgnoreCase(payer_user.GradeCode) && !(userL.MSISDN.equalsIgnoreCase(payer_user.MSISDN))) {
                        payee_list.add(userL);
                    }
                }
            }
        }

        if (payee_list.size() == 0) {
            pNode.warning("Kindly Enter more than two Channel User of Each Category in ChannelUser Sheet.");
        }

        //Run C2C for payer and payee
        for (User payer : payer_list) {
            for (User payee : payee_list) {
                if (!(payer.MSISDN.equals(payee.MSISDN))) {
                    try {
                        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_001" + getTestcaseid(), "To verify that the " + payer.GradeName + " should able to do C2C to " + payee.GradeName + " from web");

                        t1.info("Checking Service Charge for the Transaction..");

                        ServiceCharge sCharge;

                        if (ConfigInput.isCoreRelease) {
                            sCharge = new ServiceCharge(Services.C2C, payer, payee, null, null, null, null);
                        } else {
                            sCharge = new ServiceCharge(Services.P2P, payer, payee, null, null, null, null);
                        }

                        ServiceChargeManagement.init(t1)
                                .configureServiceCharge(sCharge);

                        String c2cTxnAmount = getTransactionAmount(sCharge);

                        checkPre(payer, payee);

                        //Login with valid user
                        Login.init(t1).login(payer);
                        TransactionManagement.init(t1).performC2C(payee, c2cTxnAmount, DataFactory.getTimeStamp());

                        checkPost(payer, payee);
                        DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * TEST : TEST POSITIVE
     * DESC : Test for verify functionality of back button
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0029() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0029 : ",
                "To verify that Channel user should able to navigate back to \"C2C Transfer\" page when user click on \"Back\" Button.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        Login.init(t1).login(payer);

        TransactionManagement trObj = TransactionManagement.init(t1);

        String c2cTxnAmount = getTransactionAmount(sCharge);
        startNegativeTestWithoutConfirm();
        trObj.performC2C(payee, c2cTxnAmount, DataFactory.getTimeStamp());

        trObj.clickBack();

        boolean isElePresent = Utils.checkElementPresent("c2cTrf_confirm_button_submit", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(isElePresent, true, "Verify back Button", t1);

        if (isElePresent) {
            t1.pass("Successfully Navigated to previous page.");
            t1.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
        } else {
            t1.fail("Failed to Navigate to previous page.");
            t1.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
        }
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test to perform C2C when amount left blank
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0030() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0030 : " + getTestcaseid(),
                "To verify that \"Channel User\"  Should not be able to do \"C2C\" to another \"Channnel User\" from web when amount field left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        //Login with valid user
        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).performC2C(payee, Constants.SPACE_CONSTANT, DataFactory.getTimeStamp());

        Assertion.verifyErrorMessageContain("c2c.error.improperAmount", "Verify Error message when amount left blank", t1);

    }


    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C with Invalid MSISDN (Special Character)
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_C2C_S_0031() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0031 : " + getTestcaseid(),
                "To verify that \"Channel User\"  Should not be able to do C2C to another \"Channel User\" from web with invalid MSISDN (Special Character).");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        //Login with valid user
        Login.init(t1).login(payer);
        TransactionManagement trObj = TransactionManagement.init(t1);
        trObj.negativeMSISDN(Constants.SPECIAL_CHARACTER_CONSTANT);

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("ci.error.onlynum", null);
        Assertion.verifyContains(alertMsg, expected, "Verify Alert Text", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C with Invalid MSISDN(Alphabets)
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0032() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0032 : " + getTestcaseid(),
                "To verify that \"Channel User\"  Should not be able to do C2C to another \"Channel User\" from web with invalid MSISDN (Alphabets).");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        //Login with valid user
        Login.init(t1).login(payer);
        TransactionManagement trObj = TransactionManagement.init(t1);
        trObj.negativeMSISDN(Constants.ALPHABET_CONSTANT);

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("ci.error.onlynum", null);
        Assertion.verifyContains(alertMsg, expected, "Verify Alert Text", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when MSISDN left blank
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0033() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0033 : " + getTestcaseid(),
                "To verify that \"Channel User\"  Should not be able to do C2C to another \"Channel User\" from web When MSISDN left blank.\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        //Login with valid user
        Login.init(t1).login(payer);
        TransactionManagement trObj = TransactionManagement.init(t1);
        trObj.negativeMSISDN(Constants.SPACE_CONSTANT);

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("c2c.error.msisdnvalue", null);
        Assertion.verifyContains(alertMsg, expected, "Verify Alert Text", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when Payment ID left blank
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0034() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0034 : " + getTestcaseid(),
                "To verify that \"Channel User\"  Should not be able to do C2C to another \"Channel User\" from web when Payment ID left blank.\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        //Login with valid user
        Login.init(t1).login(payer);

        TransactionManagement obj = TransactionManagement.init(t1);
        startNegativeTestWithoutConfirm();
        obj.performC2C(payee, c2cTxnAmount, Constants.BLANK_CONSTANT);

        Assertion.verifyErrorMessageContain("invc2c.error.payemntId", "Verify Error Message for Blank Payment ID", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C with Invalid Payment ID (Special Character)
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0035() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0035 : " + getTestcaseid(),
                "To verify that \"Channel User\"  Should not be able to do C2C to another \"Channel User\" from web with invalid Payment ID.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        //Login with valid user
        Login.init(t1).login(payer);

        TransactionManagement obj = TransactionManagement.init(t1);
        startNegativeTestWithoutConfirm();
        obj.performC2C(payee, c2cTxnAmount, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("invc2c.error.payemntId", "Verify Error Message for invalid Payment ID", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when payee is barred as Receiver
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_C2C_S_0036() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0036 : " + getTestcaseid(),
                "To verify that \"Channel User (Payer)\"  Should not be able to do C2C to another \"Channel User (Payee)\" from web when Payee is Barred as Receiver.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        Login.init(t1).login(payer);

        TransactionManagement obj = TransactionManagement.init(t1);
        startNegativeTest();
        obj.performC2C(barAsReceiver, Constants.MIN_CASHIN_AMOUNT, DataFactory.getTimeStamp());

        Assertion.verifyErrorMessageContain("c2c.receiver.barred", "Verify Error Message for Barred User", t1);


    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when Payer is suspended
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0037() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0037 : " + getTestcaseid(),
                "To verify that \"Channel User (Payer)\"  Should not be able to do C2C to another \"Channel User (Payee)\" from web when Payer is Suspended.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        Login.init(t1).tryLogin(suspendChUser.LoginId, suspendChUser.Password);

        Login.resetLoginStatus();

        Assertion.verifyErrorMessageContain("payer.suspended", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when payee is suspended
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0038() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0038 : " + getTestcaseid(),
                "To verify that \"Channel User (Payer)\"  Should not be able to do C2C to another \"Channel User (Payee)\" from web when Payee is Suspended.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        Login.init(t1).login(payer);

        TransactionManagement obj = TransactionManagement.init(t1);
        startNegativeTest();
        obj.performC2C(suspendChUser, c2cTxnAmount, DataFactory.getTimeStamp());

        Assertion.verifyErrorMessageContain("c2c.reciever.suspended", "Verify Error Message for Suspended User", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when Payer is Barred as Sender
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_C2C_S_0039() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0039 : ",
                "To verify that \"Channel User (Payer)\"  Should not be able to do C2C to another \"Channel User (Payee)\" from web when Payer is Barred as Sender.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        Login.init(t1).login(barAsSender);
        startNegativeTest();
        TransactionManagement.init(t1).performC2C(payee, Constants.MIN_CASHIN_AMOUNT, DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("payer.barred.sender", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when Payer have insufficient balance
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_C2C_S_0040() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0040 :",
                "To verify that \"Channel User (Payer)\"  Should not be able to do C2C to another \"Channel User (Payee)\" from web when Payer have \"insufficient  wallet balance\".\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        Login.init(t1).login(barAsReceiver);

        TransactionManagement obj = TransactionManagement.init(t1);
        startNegativeTest();
        obj.performC2C(payee, Constants.MIN_CASHIN_AMOUNT, DataFactory.getTimeStamp());

        Assertion.verifyErrorMessageContain("payer.insufficient.balance", "Verify Error Message for insufficient balance", t1);

    }


    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when Amount is less than defined service charge
     *
     * @throws Exception
     */
    //Parked Because it will affect Service Charge
    @Test(enabled = false, priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0041() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0040 : " + getTestcaseid(),
                "To verify that \"Channel User (Payer)\"  Should not be able to do C2C to another \"Channel User (Payee)\" from web when amount is less than minimum transfer value defined in service charge.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        sCharge = new ServiceCharge(Services.C2C, payer, payee, null, null, null, null);

        ServiceChargeManagement.init(t1).
                configureServiceCharge(sCharge);

        BigDecimal minValue = MobiquityGUIQueries.getMinTransferValue(sCharge.ServiceChargeName);
        minValue = minValue.divide(new BigDecimal("2"));

        Login.init(t1).login(payer);
        startNegativeTest();
        TransactionManagement.init(t1).performC2C(payee, minValue.toString(), DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("00409", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform C2C when amount is more than defined service charge
     *
     * @throws Exception
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C})
    public void SYS_TC_C2C_S_0042() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0042 : " + getTestcaseid(),
                "To verify that \"Channel User (Payer)\"  Should not be able to do C2C to another \"Channel User (Payee)\" from web when amount is more than maximum transfer value defined in service charge.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C);

        if (ConfigInput.isCoreRelease) {
            sCharge = new ServiceCharge(Services.C2C, payer, payee, null, null, null, null);
        } else {
            sCharge = new ServiceCharge(Services.P2P, payer, payee, null, null, null, null);
        }

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        BigDecimal maxValue = MobiquityGUIQueries.getMaxTransferValue(sCharge.ServiceChargeName);
        maxValue = maxValue.add(BigDecimal.valueOf(1));

        Login.init(t1).login(payer);
        startNegativeTest();
        TransactionManagement.init(t1).performC2C(payee, maxValue.toString(), DataFactory.getTimeStamp());

        Assertion.verifyErrorMessageContain("00410", "Verify Error Message", t1);
    }

    /**
     * TEST : NEGATIVE
     * DESC : C2C not successful when Transfer rule is not defined or suspended.
     *
     * @throws Exception
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0}, enabled = true)
    public void SYS_TC_C2C_S_0043() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_C2C_S_0043 :",
                "To verify that Money Transfer request will not be successful if Transfer rule is not defined or suspended.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0);

        if (ConfigInput.isCoreRelease) {
            sCharge = new ServiceCharge(Services.C2C, payer, payee, null, null, null, null);
        } else {
            sCharge = new ServiceCharge(Services.P2P, payer, payee, null, null, null, null);
        }
        try {
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            Login.init(pNode).login(usrTRuleCreator);
            String ruleID = TransferRuleManagement.init(t1).deleteTransferRule(sCharge);
            //TransferRuleManagement.init(t1).approveTransferRule(ruleID, Constants.STATUS_DELETE_INITIATE);

            Login.init(t1).login(payer);
            startNegativeTest();
            TransactionManagement.init(t1).
                    performC2C(payee, Constants.MIN_CASHOUT_AMOUNT, DataFactory.getTimeStamp());
            Assertion.verifyErrorMessageContain("5001", "Verify Error Message", t1);

            if (ConfigInput.isCoreRelease) {
                sCharge = new ServiceCharge(Services.C2C, payer, payee, null, null, null, null);
            } else {
                sCharge = new ServiceCharge(Services.P2P, payer, payee, null, null, null, null);
            }


        } finally {
            TransferRuleManagement.init(pNode)
                    .configureTransferRule(sCharge);
        }


    }
}






