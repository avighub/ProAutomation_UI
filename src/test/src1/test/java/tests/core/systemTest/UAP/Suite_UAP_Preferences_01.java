package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.WalletPreference;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for Wallet Preferences
 *
 * @author navin.pramanik
 */
public class Suite_UAP_Preferences_01 extends TestInit {

    private static String sysPrefCode, sysPrefValue;
    private static String serviceCode, language, messageCode;
    SuperAdmin saMapWallet, saSysPref;
    OperatorUser optUsr;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        sysPrefCode = "CLIENT_SITE";
        sysPrefValue = AppConfig.clientSite;
        serviceCode = "ALL";
        language = "English";
        messageCode = "00001";
    }


    /**
     * TEST : TC0168  :Preferences-> Wallet Preferences
     * DESC : To verify that the valid user can configured the wallet preferences.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void walletPrefTest() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("TC0168  :Preferences-> Wallet Preferences", "To verify that the Superadmin can configured the wallet preferences.");

            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.WALLET_PREFERENCES, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

            saMapWallet = DataFactory.getSuperAdminWithAccess("CAT_PREF");

            Login.init(t1).loginAsSuperAdmin(saMapWallet);

            WalletPreference wPref = new WalletPreference(Constants.SUBSCRIBER,
                    Constants.REGTYPE_NO_KYC,
                    DataFactory.getDefaultWallet().WalletName, true,
                    null, null);
            Preferences.init(t1).
                    configureWalletPreferences(wPref);
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }

    }

    /**
     * TEST : TC0149 :Preferences-> Modify System Preferences
     * DESC : To verify that the valid user can update/modify the list of system preferences defined in the system.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void systemPrefTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0149 :Preferences-> Modify System Preferences", "To verify that Superadmin can update/modify the list of system preferences defined in the system.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SYSTEM_PREFERENCES, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        saSysPref = DataFactory.getSuperAdminWithAccess("PREF001");

        Login.init(t1).loginAsSuperAdmin(saSysPref);

        Preferences.init(t1).modifySystemPreferences(sysPrefCode, sysPrefValue);

    }

    /**
     * TEST : TC0150  :Preferences-> View System Preferences
     * DESCRIPTION : To verify that the valid user can view the list of system preferences defined in the system.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP})
    public void systemPrefViewTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0150  :Preferences-> View System Preferences", "To verify that the valid user can view the list of system preferences defined in the system.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.SYSTEM_PREFERENCES);

        saSysPref = DataFactory.getSuperAdminWithAccess("PREF001");

        Login.init(t1).loginAsSuperAdmin(saSysPref);

        Preferences.init(t1).viewSystemPreferences(sysPrefCode, sysPrefValue);

    }

    /**
     * TEST : TC0128  :Preferences-> SMS Configuration
     * DESCRIPTION : To verify that the valid user can configure the SMS in System
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP})
    public void smsConfigTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0128  :Preferences-> SMS Configuration", "To verify that the valid user can configure the SMS.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.SMS_CONFIGURATION);

        optUsr = DataFactory.getOperatorUsersWithAccess("SMS_CR").get(0);

        Login.init(t1).login(optUsr);

        Preferences.init(t1).updateSMSConfigurationMessageText(serviceCode, messageCode, null);
    }
}
