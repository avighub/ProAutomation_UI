package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.userManagement.AddOperatorUser_pg2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by sapan.dang on 14-07-2017.
 * Reviewed and fixed issues by Ravindra.dumpa on 30-01-2018
 * This class will contain all the cases related to the Operator user management
 */
public class Suite_System_OperatorUserManagement extends TestInit {
    //Global vars - These vars will be used by the Tests
    private OperatorUser modifyOPT; //This operator user will be used by the modifications Tests
    private OperatorUser usrCreator, usrModify, usrDelete, usrApprover, bankApprover;
    private SuperAdmin maker, checker, saModifier;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ASU");
            usrApprover = DataFactory.getOperatorUserWithAccess("PTY_ASUA");
            maker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            checker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
            saModifier = DataFactory.getSuperAdminWithAccess("PTY_MSU");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * Test Operator User Creation
     *
     * @throws Exception
     */
    @Test(priority = 0, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void initiateNetworkAdmin() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_01", "To verify that super admin can initiate Network admin creation");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.NETWORK_ADMIN);

            Login.init(t1).loginAsSuperAdmin(maker);

            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser);

            Login.init(t1).loginAsSuperAdmin(checker);

            OperatorUserManagement.init(t1)
                    .approveOperatorUser(optUser)
                    .changeFirstTimePasswordOpt(optUser);

            //Store this operator user in Global vars
            modifyOPT = optUser;
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Negative Test
     *
     * @throws Exception Disabling this test - As the first name length is handled as per customer requirement
     */
    @Test(priority = 1, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void systemTestOperatorUserManagement() throws Exception {

        ExtentTest t2 = pNode.createNode("SYS_TEST_02",
                "To verify that super admin can't  initiate  Network admin creation, if :" +
                        " First Name Contain more than 80 characters")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

        try {
            Login.init(t2)
                    .loginAsSuperAdmin(maker);

            OperatorUser optUser_02 = new OperatorUser(Constants.NETWORK_ADMIN);

            optUser_02.FirstName = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabaasdasdasd";

            OperatorUserManagement obj = OperatorUserManagement.init(t2);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_02);

            String readOnlyFirstName = AddOperatorUser_pg2.init(t2)
                    .getFirstNameReadOnlyText();

            t2.info("Test Data, FirstName String length > 80 Char: " + optUser_02.FirstName);
            t2.info("Accepted String: " + readOnlyFirstName);

            Assertion.verifyEqual(readOnlyFirstName.length(), 80, "Verify that FirstName accepts only 80 character", t2);

            //Test 3
            ExtentTest t3 = pNode.createNode("SYS_TEST_03", "To verify that super admin can't  initiate  Network admin creation, if : " +
                    "First Name is in invalid format i.e contains some special characters etc ")
                    .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_03 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_03.FirstName = "@##@!!##";
            obj = OperatorUserManagement.init(t3);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_03);

            Assertion.verifyErrorMessageContain("systemparty.error.firstnameformat", "Verify First Name", t3);


            //Test 4
            ExtentTest t4 = pNode.createNode("SYS_TEST_04", "To verify that super admin can't  initiate  Network admin user creation if : MSISDN is invalid ");
            t4.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_04 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_04.MSISDN = "123444";

            obj = OperatorUserManagement.init(t4);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_04);

            Assertion.verifyErrorMessageContain("00051", "Verify Wrong Msisdn", t4);

            //Test 5
            ExtentTest t5 = pNode.createNode("SYS_TEST_05", "To verify that super admin can't  initiate  Network admin user creation if : MSISDN is invalid ");
            t5.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_05 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_05.MSISDN = "123444";

            obj = OperatorUserManagement.init(t5);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_05);

            Assertion.verifyErrorMessageContain("00051", "Verify Wrong Msisdn", t5);

            // Test 6

            ExtentTest t6 = pNode.createNode("SYS_TEST_06", "To verify that super admin can't  initiate  Network admin user creation if : invalid Identification Number is entered.   ");
            t6.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_06 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_06.ExternalCode = "@#$%^&***"; //Set invalid data

            obj = OperatorUserManagement.init(t6);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_06);

            Assertion.verifyErrorMessageContain("subs.error.alphaexternalCode", "Identification Number is not valid", t6);

            // Test 7

            ExtentTest t7 = pNode.createNode("SYS_TEST_07", "To verify that super admin can't  initiate  Network admin user creation if :E-mail ID Contain more than 60 characters ");
            t7.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_07 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_07.Email = "sdfsdfsdfsfsddsfghjklsadfghjkqwertyuiopQWERTYUIOASDFGHJKJHGFDGVFCXMNBVCXZUYTRED@GMAIL.COM";

            obj = OperatorUserManagement.init(t7);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_07);

            Assertion.verifyErrorMessageContain("subs.error.emailId.format", "E-mail is not in valid format", t7);

            //Test 8

            ExtentTest t8 = pNode.createNode("SYS_TEST_08", "To verify that super admin can't  initiate  Network admin user creation if :E-mail ID is in invalid format i.e  contains some special characters other than @  ");
            t8.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_08 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_08.Email = "sdfsdf#.com";

            obj = OperatorUserManagement.init(t8);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_08);

            Assertion.verifyErrorMessageContain("subs.error.emailId.format", "E-mail is not in valid format", t8);

            //Test 9

            ExtentTest t9 = pNode.createNode("SYS_TEST_09", "To verify that super admin can't  initiate  Network admin user creation if :E-mail ID is blank");
            t9.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_09 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_09.Email = "";

            obj = OperatorUserManagement.init(t9);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_09);

            Assertion.verifyErrorMessageContain("subs.error.emailId.format", "E-mail is not in valid format", t9);
            /**
             * Test 10
             */
            ExtentTest t10 = pNode.createNode("SYS_TEST_10", "To verify that super admin can't  initiate  Network admin user creation if : Division Contain more than 30 characters ");
            t10.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            // Set the Division text More Than 30 Char
            OperatorUser optUser_10 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_10.Division = "abcdefghijklmnopqrstuvwxyz00031";

            Login.init(t10)
                    .loginAsSuperAdmin(maker);

            obj = OperatorUserManagement.init(t10);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_10);

            String readOnlyDivision = AddOperatorUser_pg2.init(t10)
                    .getDivisionReadOnlyText();

            t10.info("Test Data, Division String length > 30 Char: " + optUser_10.Division);
            t10.info("Accepted String: " + readOnlyDivision);

            Assertion.verifyEqual(readOnlyDivision.length(), 30, "Verify that Division accepts only 30 character", t10);

            /**
             * Test 11
             */
            ExtentTest t11 = pNode.createNode("SYS_TEST_11", "To verify that super admin can't  initiate  Network admin user creation if :Division is in invalid format i.e contains some special characters etc. ");
            t11.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_11 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_11.Division = "@#$%qwertyuiASDFGHJQWERTYASXDFGSDF23";

            obj = OperatorUserManagement.init(t11);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_11);

            Assertion.verifyErrorMessageContain("subs.error.division", "Division Accepts Only Alphanumeric Values", t11);

            /**
             * Test 12
             */
            ExtentTest t12 = pNode.createNode("SYS_TEST_12", "To verify that super admin can't  initiate  Network admin user creation if :Division is blank.");
            t12.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_12 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_12.Division = "";

            obj = OperatorUserManagement.init(t12);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_12);

            Assertion.verifyErrorMessageContain("systemparty.validation.division", "Division field can't be Empty", t12);

            /**
             * Test 13
             */
            ExtentTest t13 = pNode.createNode("SYS_TEST_13", "To verify that super admin can't  initiate  Network admin user creation if :Department Contain more than 30 characters k.");
            t13.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_13 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_13.Dept = "qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjkl";

            obj = OperatorUserManagement.init(t13);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_13);

            Assertion.assertErrorInPage(t13);

            /**
             * Test 14
             */
            ExtentTest t14 = pNode.createNode("SYS_TEST_14", "To verify that super admin can't  initiate  Network admin user creation if :Department is in invalid format i.e contains some special characters etc.");
            t14.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_14 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_14.Dept = "";

            obj = OperatorUserManagement.init(t14);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_14);

            Assertion.verifyErrorMessageContain("systemparty.validation.department", "You must enter department", t14);

            /**
             * Test 15
             */
            ExtentTest t15 = pNode.createNode("SYS_TEST_15", "To verify that super admin can't  initiate  Network admin user creation if :Department is in invalid format i.e contains some special characters etc.");
            t15.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_15 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_15.Dept = "@!#$%^&*#$%^";

            obj = OperatorUserManagement.init(t15);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_15);

            Assertion.verifyErrorMessageContain("subs.error.department", "Department Accepts Only Alphanumeric Values", t15);

            /**
             * Test 16
             */
            ExtentTest t16 = pNode.createNode("SYS_TEST_16", "To verify that super admin can't  initiate  Network admin user creation if :Department is blank.");
            t16.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_16 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_16.Dept = "";

            obj = OperatorUserManagement.init(t16);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_16);

            Assertion.verifyErrorMessageContain("systemparty.validation.department", "You must enter department", t16);

            /**
             * Test 17
             */
            ExtentTest t17 = pNode.createNode("SYS_TEST_17",
                    "To verify that super admin can't  initiate  Network admin user creation if " +
                            "Contact No Contain more than MAX_MSISDN_LENGTH, set in the preference");

            t17.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_17 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_17.ContactNum = "12345678912345678912345678";

            obj = OperatorUserManagement.init(t17);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_17);

            String readOnlyContactNum = AddOperatorUser_pg2.init(t10)
                    .getContactNumReadOnlyText();

            t10.info("Test Data, ContactNum String length > 15 Char: " + optUser_10.ContactNum);
            t10.info("Accepted String: " + readOnlyContactNum);

            Assertion.verifyEqual(readOnlyContactNum.length(), AppConfig.msisdnLength, "Verify that Contact Number accepts only " + AppConfig.msisdnLength + " character", t17);

            /**
             * Test 18
             */
            ExtentTest t18 = pNode.createNode("SYS_TEST_18", "To verify that super admin can't  initiate  Network admin user creation if :Contact No is in invalid format i.e contains some special characters, letters etc.");

            t18.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_18 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_18.ContactNum = "#$%^&*()(*&^%";

            obj = OperatorUserManagement.init(t18);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_18);

            Assertion.verifyErrorMessageContain("subs.error.contactnnumeric", "Contact No. should be numeric", t18);

            /**
             * Test 19
             */
            ExtentTest t19 = pNode.createNode("SYS_TEST_19", "To verify that super admin can't  initiate  Network admin user creation if :Contact No is blank.");
            t19.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_19 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_19.ContactNum = "";

            obj = OperatorUserManagement.init(t19);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_19);

            Assertion.verifyErrorMessageContain("systemparty.validation.contactNo", "Contact No. is Required.", t19);

            /**
             * Test 20
             */
            ExtentTest t20 = pNode.createNode("SYS_TEST_20",
                    "To verify that super admin can't  initiate  Network admin user creation if " +
                            "Web Login Id Contain more than 20 characters .");
            t20.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_20 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_20.LoginId = "qwertyuiopasdfghjkzxcvbnmqwertyuiopsdfghj";

            obj = OperatorUserManagement.init(t20);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_20);

            String readOnlyLoginId = AddOperatorUser_pg2.init(t10)
                    .getLoginIdReadOnlyText();

            t10.info("Test Data, Login Id String length > 20 Char: " + optUser_10.LoginId);
            t10.info("Accepted String: " + readOnlyLoginId);

            Assertion.verifyEqual(readOnlyLoginId.length(), 20, "Verify that Login Id accepts only 20 character", t20);

            /**
             * Test 21
             */
            ExtentTest t21 = pNode.createNode("SYS_TEST_21", "To verify that super admin can't  initiate  Network admin user creation if :Web Login Id is in invalid format i.e   contains some special characters etc.");
            t21.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_21 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_21.LoginId = "@#$%^&*($%^&";

            obj = OperatorUserManagement.init(t21);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_21);

            Assertion.verifyErrorMessageContain("systemparty.validation.loginAccessWebLoginidWhitespace", "Web Login ID cannot contain whitespace or Special characters", t21);

            /**
             * Test 22
             */
            ExtentTest t22 = pNode.createNode("SYS_TEST_22", "To verify that super admin can't  initiate  Network admin user creation if : Web Login Id is blank.");
            t22.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_22 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_22.LoginId = "";

            obj = OperatorUserManagement.init(t22);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_22);

            Assertion.verifyErrorMessageContain("systemparty.validation.loginlength", "The login Id length must be between {0} & {1}", t22, "3", "20");

            /**
             * Test 23
             */
            ExtentTest t23 = pNode.createNode("SYS_TEST_23", "To verify that super admin can't  initiate  Network admin user creation if :Web Login Id is in invalid format i.e   contains some special characters etc.");
            t23.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);


            OperatorUser optUser_23 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_23.LoginId = "@#$%^&*($%^&";

            obj = OperatorUserManagement.init(t23);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_23);

            Assertion.verifyErrorMessageContain("systemparty.validation.loginAccessWebLoginidWhitespace", "Web Login ID cannot contain whitespace or Special characters", t23);

            /**
             * Test 24
             */
            ExtentTest t24 = pNode.createNode("SYS_TEST_24", "To verify that super admin can't  initiate  Network admin user creation if :all mandatory field is empty.");
            t24.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);

            OperatorUser optUser_24 = new OperatorUser(Constants.NETWORK_ADMIN);
            optUser_24.LoginId = "";
            optUser_24.MSISDN = "";
            optUser_24.ContactNum = "";
            optUser_24.Division = "";
            optUser_24.Dept = "";

            obj = OperatorUserManagement.init(t24);
            startNegativeTest();
            obj.initiateOperatorUser(optUser_24);

            Assertion.verifyErrorMessageContain("systemparty.validation.loginlength", "The login Id length must be between {0} & {1}", t24, "3", "20");
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();

    }

    //===========================================================================================================
    //========================== BANK ADMIN CASES ===============================================================
    //===========================================================================================================

    /**
     * Create OPT (BANK ADMIN)
     *
     * @throws Exception
     * @CASE_TYPE NEGATIVE
     */
    @Test(priority = 2, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_25() throws Exception {
        ExtentTest t24_1 = pNode.createNode("SYS_TEST_25", "To verify that super can initiate bank admin");

        t24_1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.BANK_ADMIN);

            Login.init(t24_1).loginAsSuperAdmin(maker);

            OperatorUserManagement.init(t24_1)
                    .initiateOperatorUser(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t24_1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Create OPT (BANK ADMIN) : and Approve it
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_26() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_26", "To verify that super admin can approve the initiated Bank Admin creation.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.BANK_ADMIN);

            Login.init(t1).loginAsSuperAdmin(maker);
            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser);
            // Approve Operator User
            Login.init(pNode).loginAsSuperAdmin(checker);

            OperatorUserManagement.init(t1)
                    .approveOperatorUser(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }


    /**
     * Create OPT Channel admin
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_27() throws Exception {
        ExtentTest t27 = pNode.createNode("SYS_TEST_27", "To verify that Network admin can initiate channel Admin.");

        t27.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.CHANNEL_ADMIN);

            Login.init(t27).login(usrCreator);
            OperatorUserManagement.init(t27)
                    .initiateOperatorUser(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t27);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Create OPT Channel admin and approve it
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_28() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TEST_28", "To verify that Network admin can approve channel Admin.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.CHANNEL_ADMIN);

            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser).approveOperatorUser(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Create OPT CCE
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_29() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_29", "To verify that Network admin can create CCE.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.CUSTOMER_CARE_EXE);

            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }


    /**
     * Create OPT CCE and approve it
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_30() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_30", "To verify that Network admin can create CCE and approve it.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.CUSTOMER_CARE_EXE);

            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser).approveOperatorUser(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Cannot initiate Channel Admin : FIRST NAME Is Blank
     *
     * @throws Exception
     */
    @Test(priority = 8)
    public void SYS_TEST_31() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_31",
                "To verify that Network admin can Not initiate channel Admin if First name is blank");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);


        try {
            OperatorUser optUser = new OperatorUser(Constants.CHANNEL_ADMIN);
            optUser.FirstName = "";

            Login.init(t1).login(usrCreator);

            startNegativeTest();
            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser);

            Assertion.verifyErrorMessageContain("error.user.firstname.is.blank", "Verify First Name is Mandatory for Channel admin Creation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Cannot initaite CCE : FIRST NAME is Blank
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_32() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_32",
                "To verify that Network admin cannot initiate Customer Care if First name is blank");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
            optUser.FirstName = "";

            Login.init(t1).login(usrCreator);
            startNegativeTest();
            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser);

            Assertion.verifyErrorMessageContain("error.user.firstname.is.blank", "Network admin cannot initiate Customer Care if First name is blank", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Cannot initaite Channel Admin : FIRST NAME contain invalid chars
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_33() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_33",
                "To verify that Network admin can initiate channel Admin if First contain invalid characters");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            OperatorUser optUser = new OperatorUser(Constants.CHANNEL_ADMIN);
            optUser.FirstName = "!@#$%^&*())(*&";

            Login.init(t1).login(usrCreator);
            startNegativeTest();
            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser);

            Assertion.verifyErrorMessageContain("systemparty.error.firstnameformat",
                    "Network admin cannot initiate Channel admin if First contain invalid chars", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Cannot initaite CCE : FIRST NAME contain invalid chars
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_34() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_34",
                "To verify that Network admin can initiate CCE if First name contain contain invalid chars");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {

            OperatorUser optUser = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
            optUser.FirstName = "!@#$%^&*())(*&";

            Login.init(t1).login(usrCreator);
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTest();
            obj.initiateOperatorUser(optUser);

            Assertion.verifyErrorMessageContain("systemparty.error.firstnameformat",
                    "Network admin cannot initiate CCE if First name is blank", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Cannot initaite CCE : last name is blank
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_35() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_35",
                "To verify that Network admin cannot initiate CCE if last name is blank");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {

            OperatorUser optUser = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
            optUser.LastName = "";

            Login.init(t1).login(usrCreator);
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTest();
            obj.initiateOperatorUser(optUser);

            Assertion.verifyErrorMessageContain("subs.error.lastnamerequired",
                    "Network admin cannot initiate CCE if Last Name is blank", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Cannot initiate CCE : Web login ID is blank
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_36() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_36",
                "To verify that Network admin cannot initiate CCE if login ID is blank");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {

            OperatorUser optUser = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
            optUser.LoginId = "";

            Login.init(t1).login(usrCreator);
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTest();
            obj.initiateOperatorUser(optUser);

            Assertion.verifyErrorMessageContain("systemparty.validation.loginspace",
                    "The login Id cannot be empty for CCE", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }


    /**
     * Cannot initaite CCE : if MSISDN alrady exist
     *
     * @throws Exception
     */
    @Test(priority = 14, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_37() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_37",
                "To verify that Network admin cannot initiate CCE if MSISDN already Exist");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {

            OperatorUser optUser = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
            optUser.MSISDN = usrCreator.MSISDN;

            Login.init(t1).login(usrCreator);
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTest();
            obj.initiateOperatorUser(optUser);

            Assertion.verifyErrorMessageContain("systemparty.validation.existedMsisdn",
                    "Network admin cannot initiate CCE if MSISDN already Exist", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }


    //================================================================================================================
    //==============  OPERATOR USER MODIFICATION TESTS ===============================================================
    //================================================================================================================

    /**
     * TODO - NOT YET COMPLETED
     *
     * @throws Exception
     */
    @Test(priority = 15, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void SYS_TEST_38() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TEST_38",
                "To verify that superadmin is able to modify the network admin");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {

            OperatorUser optUser = new OperatorUser(Constants.NETWORK_ADMIN);
            OperatorUserManagement.init(t1).createAdmin(optUser);

            //Now modify the user
            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1)
                    .modifyOperatorUser(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

}
