/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: surya.dhal
 *  Date: 21-Nov-2017
 *  Purpose: Test Of O2C
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.channelEnquiry.ChannelEnquiry;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.channelEnquiry.ChannelEnquiry_Page1;
import framework.pageObjects.ownerToChannel.O2CInitiation_Page1;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by surya.dhal on 11/21/2017.
 */
public class SuiteSystemO2C extends TestInit {
    private static String transID, o2cTxnAmount, providerName, defaultWalletName;
    private static ServiceCharge commonScharge;
    private OperatorUser payer, usrTRuleCreator, optUser, networkAdmin, approver1, approver2;
    private User payee, barAsSender, barAsReceiver, suspendChUser, barAsBoth;
    private String expected, alertMsg;
    private Wallet wNormal, wCommission;
    private ExtentTest setupNode;

    /**
     * Method to get Users before run Test
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            usrTRuleCreator = DataFactory.getOperatorUserWithAccess("O2C_TRULES");
            payer = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            approver1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");
            approver2 = DataFactory.getOperatorUserWithAccess("O2C_APP2");
            networkAdmin = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);
            payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUser = DataFactory.getOperatorUserWithAccess("SVC_SUS");
            commonScharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

            providerName = DataFactory.getDefaultProvider().ProviderName;
            defaultWalletName = DataFactory.getDefaultWallet().WalletName;
            setupNode = pNode.createNode("SETUP", "Setup For O2C ");


            ServiceChargeManagement.init(setupNode)
                    .configureServiceCharge(commonScharge);
            o2cTxnAmount = getTransactionAmount(commonScharge);

            wNormal = DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL);
            wCommission = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    public void checkPre(User payee) {
        DBAssertion.prePayerBal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityGUIQueries.getReconBalance();
    }

    public void checkPost(User payee) {
        DBAssertion.postPayerBal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityGUIQueries.getReconBalance();
    }


    /**
     * TEST : POSITIVE TEST
     * DESC : Test to Perform O2C with All Users With All Grades
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void SYS_TC_O2C_S_0016() throws Exception {

        List<OperatorUser> o2cUsers = DataFactory.getOperatorUsersWithAccess("O2C_INIT");
        Map<String, User> payees = DataFactory.getChannelUserSet(true);
        List<User> payees_list = new ArrayList<User>(payees.values());
        List<User> chUsers = new ArrayList<User>();

        for (User user : payees_list) {
            Boolean flag = false;

            if (!(user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER) ||
                    user.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE))) {
                //looping through existing list to not add if same grade code already added in list
                Iterator<User> iterator = chUsers.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().GradeCode.equals(user.GradeCode)) {
                        flag = true;
                    }
                }
                if (flag == false) {
                    chUsers.add(user);
                }
            }

        }

        for (OperatorUser payer : o2cUsers) {

            for (User payee : chUsers) {
                try {

                    ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0016" + getTestcaseid(),
                            "To verify that " + DataFactory.getCategoryName(payer.CategoryCode)
                                    + " should be able to initiate O2C to " + DataFactory.getGradeName(payee.GradeCode));
                    ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

                    ServiceChargeManagement.init(t1)
                            .configureServiceCharge(sCharge);

                    t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);
                    BigDecimal o2cAmount = MobiquityGUIQueries.getMaxTransferValue(sCharge.ServiceChargeName);
                    o2cAmount = o2cAmount.subtract(BigDecimal.valueOf(1));

                    checkPre(payee);

                    Login.init(t1).login(payer);

                    TransactionManagement.init(t1).
                            initiateAndApproveO2C(payee, o2cAmount.toString(), Constants.REMARKS, DataFactory.getTimeStamp());
                    checkPost(payee);
                    DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

    }

    //Parked because it will affect system service  charge.

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test to perform O2C when entered amount is more than the Available Stock
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_174() throws Exception {

        List<OperatorUser> chUsers = DataFactory.getOperatorUsersWithAccess("O2C_INIT");

        ExtentTest t1 = pNode.createNode("P1_TC_174",
                "To verify that Network admin should not initiate the O2C service if entered amount is more than the Available Stock in the mobiquity system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        t1.info("Checking Service Charge for the Transaction.");

        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        BigDecimal stockAmount = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
        BigDecimal maxValue = stockAmount.add(BigDecimal.valueOf(1000));
        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateAndApproveO2C(payee, maxValue.toString(), Constants.REMARKS, DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("o2c.initiation.maxvalue.amt", "Verify O2C Max Value", t1);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Delete Channel when O2C in approval 1
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_175() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_175 ",
                "To verify that deletion of channel user should not be successful by Channel admin when their O2C amount are in approval 1.").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        Login.init(t1).login(payer);
        transID = TransactionManagement.init(t1).initiateO2C(payee, o2cTxnAmount, Constants.REMARKS, DataFactory.getTimeStamp());

        if (transID != null) {
            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_DCU"));
            ChannelUserManagement.init(t1).startNegativeTest().initiateChannelUserDelete(payee);
        }

        String actualMessage = Assertion.getErrorMessage();
        String expectedMessage = MessageReader.getMessage("not.delete.channel.existing.transaction", null);
        if (actualMessage.contains(expectedMessage)) {
            Assertion.verifyErrorMessageContain(expectedMessage, "Delete Channel User.", t1);
        } else {
            Assertion.verifyErrorMessageContain("some.wallets.have.balance", "Delete Channel User.", t1);
        }

    }

    /**
     * TEST : POSITIVE
     * DESC : Test Method to Reject O2C at level 2
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1})
    public void P1_TC_539() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_539",
                "To verify that Network admin can Reject the Approved 1 O2C amount at level 2.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        BigDecimal minTransferValue = MobiquityGUIQueries.getMinTransferValue(sCharge.ServiceChargeName);
        t1.info("Minimum Transfer value is : " + minTransferValue);
        BigDecimal o2cLimit = minTransferValue.add(BigDecimal.valueOf(10));
        BigDecimal txnAmount = o2cLimit.add(BigDecimal.valueOf(1));

        Login.init(t1).login(usrTRuleCreator);

        String limitAmount = TransferRuleManagement.init(t1).setO2CTransferLimitWithAmount(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDomainName(Constants.WHOLESALER),
                DataFactory.getCategoryName(Constants.WHOLESALER),
                Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName, o2cLimit.toString());

        Login.init(t1).login(payer);
        String txnID = TransactionManagement.init(t1).initiateO2C(payee, txnAmount.toString(), Constants.REMARKS, DataFactory.getTimeStamp());
        startNegativeTest();

        /**
         * Same user can not approve the transaction into the system
         */

        TransactionManagement.init(t1).o2cApproval1(txnID);
        Login.init(t1).login(approver2);
        stopNegativeTest();
        TransactionManagement.init(t1).rejectO2CLevel2(txnID);

        TransferRuleManagement.init(t1).setO2CTransferLimitWithAmount(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDomainName(Constants.WHOLESALER),
                DataFactory.getCategoryName(Constants.WHOLESALER),
                Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName, limitAmount);


    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test Method to verify Network Admin successfully approve the O2C service at level-1
     * TC_ECONET_0394     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_384() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_384",
                "To verify that Network admin can successfully approve the O2C service at level-1 and request will close at this level when requested amount is less than Approval limit 1.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        try {
            payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            Login.init(t1).login(usrTRuleCreator);
            TransferRuleManagement.init(t1).setO2CTransferLimitWithAmount(providerName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    Constants.PAYINST_WALLET_CONST, defaultWalletName, "100");

            ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            checkPre(payee);

            TransactionManagement tObj = TransactionManagement.init(t1);

            Login.init(t1).login(payer);

            String txnId = tObj.initiateO2C(payee, "99", Constants.REMARKS, DataFactory.getTimeStamp());

            startNegativeTest();

            Login.init(t1).loginAsOperatorUserWithRole(Roles.OWNER_TO_CHANNEL_TRANSFER_APPROVAL1);

            tObj.o2cApproval1(txnId);

            Assertion.verifyActionMessageContain("o2c.approval.success", "O2C Approval 1", t1);

            checkPost(payee);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Login.init(t1).login(usrTRuleCreator);
            TransferRuleManagement.init(t1).setO2CTransferLimit(providerName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    Constants.PAYINST_WALLET_CONST, defaultWalletName);
        }

        Assertion.finalizeSoftAsserts();
    }

    /*Parked because it will affect Service Charge*/

    /**
     * TEST : NEGATIVE TEST
     * DESC :Test to verify min transfer value in service charge.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1})
    public void P1_TC_347() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_347",
                "To verify that o2c transaction should not be successful if transaction amount is less than defined min transfer value in service charge.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);
        Login.init(t1).login(payer);

        BigDecimal minValue = MobiquityGUIQueries.getMinTransferValue(sCharge.ServiceChargeName);
        t1.info("Min value of Service Charge : " + minValue);
        minValue = minValue.divide(new BigDecimal("2"));

        startNegativeTest();
        TransactionManagement.init(t1).initiateAndApproveO2C(payee, minValue.toString(), Constants.REMARKS, DataFactory.getTimeStamp());
        //TransactionManagement.init(t1).initiateO2C(payee, minValue.toString(), Constants.REMARKS, DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("o2c.error.servicecharge.lessamount", "O2C Transaction", t1);
        Assertion.finalizeSoftAsserts();

    }

    //Parked because it will affect Service Charge

    /**
     * TEST : NEGATIVE TEST
     * ID : P1_TC_348
     * DESC : Test to verify transaction amount is more than defined max transfer value
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1})
    public void P1_TC_348() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_348",
                "To verify that o2c transaction should not be successful if transaction amount is more than defined max transfer value in service charge.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);
        Login.init(t1).login(payer);

        BigDecimal maxValue = MobiquityGUIQueries.getMaxTransferValue(sCharge.ServiceChargeName);
        t1.info("Max transfer Value in Service Charge is : " + maxValue);
        maxValue = maxValue.add(new BigDecimal("100"));
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateO2C(payee, maxValue.toString(), Constants.REMARKS, DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("amount.morethan.serviceCharge", "O2C Transaction", t1);

    }


    /**
     * TEST : NEGATIVE TEST
     * DESC : Test to verify O2C if payee is barred as receiver
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_540_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_540_1",
                "To verify that Network admin cannot initiate the O2C service if payee (Channel User - Wholesaler) is barred as receiver");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        barAsReceiver = CommonUserManagement.init(setupNode).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_RECIEVER, null);

        Login.init(t1).login(networkAdmin);

        startNegativeTest();
        TransactionManagement.init(t1).initiateO2C(barAsReceiver, o2cTxnAmount, Constants.REMARKS, DataFactory.getTimeStamp());

        Assertion.verifyErrorMessageContain("payee.barred.reciever", "O2C Transaction", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test to verify O2C if payee is barred as Both
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_540_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_540_2",
                "To verify that Network admin cannot initiate the O2C service if payee (Channel User - Wholesaler) is barred as Both");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        barAsBoth = CommonUserManagement.init(setupNode).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_BOTH, null);

        Login.init(t1).login(networkAdmin);
        startNegativeTest();
        TransactionManagement.init(t1).initiateO2C(barAsBoth, o2cTxnAmount, Constants.REMARKS, DataFactory.getTimeStamp());

        Assertion.verifyErrorMessageContain("payee.barred.both", "O2C Transaction", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when payee is barred as sender
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_540_3() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_540_3",
                "verify that Network admin can initiate the O2C service if payee (Channel User - Wholesaler) is barred as sender.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        barAsSender = CommonUserManagement.init(setupNode).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);

        t1.info("Barred User is : " + barAsSender);

        Login.init(t1).login(networkAdmin);
        TransactionManagement.init(t1).initiateAndApproveO2C(barAsSender, o2cTxnAmount, Constants.REMARKS, DataFactory.getTimeStamp());

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : deletion of Channel user when amount in approval level
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_541() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_541 ",
                "To verify that deletion of channel user should not be successful by Channel admin / when their O2C amount are in approval 2..");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        payee = DataFactory.getChannelUserWithCategory(Constants.RETAILER);

        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        BigDecimal minTransferValue = MobiquityGUIQueries.getMinTransferValue(sCharge.ServiceChargeName);
        t1.info("Minimum Transfer value is : " + minTransferValue);
        BigDecimal o2cLimit = minTransferValue.add(BigDecimal.valueOf(10));
        BigDecimal txnAmount = o2cLimit.add(BigDecimal.valueOf(1));

        Login.init(t1).login(usrTRuleCreator);

        String limitAmount = TransferRuleManagement.init(t1).setO2CTransferLimitWithAmount(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDomainName(Constants.WHOLESALER),
                DataFactory.getCategoryName(Constants.WHOLESALER),
                Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName, o2cLimit.toString());

        Login.init(t1).login(payer);
        String txnID = TransactionManagement.init(t1).initiateO2C(payee, txnAmount.toString(), Constants.REMARKS, DataFactory.getTimeStamp());
        startNegativeTest();

        /**
         * Same user can not approve the transaction into the system
         */

        TransactionManagement.init(t1).o2cApproval1(txnID);
        //Login.init(t1).login(approver2);
        Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_DCU"));
        ChannelUserManagement.init(t1).initiateChannelUserDelete(payee);
        String actualMessage = Assertion.getErrorMessage();
        String expectedMessage = MessageReader.getMessage("not.delete.channel.existing.transaction", null);
        if (actualMessage.contains(expectedMessage)) {
            Assertion.verifyErrorMessageContain(expectedMessage, "Delete Channel User.", t1);
        } else {
            Assertion.verifyErrorMessageContain("some.wallets.have.balance", "Delete Channel User.", t1);
        }

        TransferRuleManagement.init(t1).setO2CTransferLimitWithAmount(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDomainName(Constants.WHOLESALER),
                DataFactory.getCategoryName(Constants.WHOLESALER),
                Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName, limitAmount);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when service charge is suspended
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_542() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_542", "To Verify that if O2C service charge is in suspended state, then the O2C transaction which is already at O2C Approval stage should not get successfully approved.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        Login.init(t1).login(payer);
        transID = TransactionManagement.init(t1).initiateO2C(payee, Constants.O2C_TRANS_AMOUNT, Constants.REMARKS, DataFactory.getTimeStamp());

        Login.init(t1).login(optUser);
        ServiceChargeManagement.init(t1).suspendServiceCharge(sCharge);

        Login.init(t1).login(payer);
        startNegativeTest();
        TransactionManagement.init(t1).o2cApproval1(transID);

        Assertion.verifyActionMessageContain("00046", "Service Charge suspended", t1);
        stopNegativeTest();
        Login.init(t1).login(optUser);
        ServiceChargeManagement.init(t1).resumeServiceCharge(sCharge);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Approve O2C at level 1
     *
     * @throws Exception
     */
    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_385() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_385",
                "To verify that Network admin can successfully approve the O2C service at level-2 and " +
                        "request will close at this level when requested amount is more than Approval limit 1.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);
        try {


            payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            BigDecimal minTransferValue = MobiquityGUIQueries.getMinTransferValue(commonScharge.ServiceChargeName);
            t1.info("Minimum Transfer value is : " + minTransferValue);
            BigDecimal o2cLimit = minTransferValue.add(BigDecimal.valueOf(10));
            BigDecimal txnAmount = o2cLimit.add(BigDecimal.valueOf(1));

            Login.init(t1).login(usrTRuleCreator);

            TransferRuleManagement.init(t1).setO2CTransferLimitWithAmount(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName, o2cLimit.toString());

            Login.init(t1).login(payer);

            TransactionManagement tObj = TransactionManagement.init(t1);

            String txnID = tObj.initiateO2C(payee, txnAmount.toString(), Constants.REMARKS, DataFactory.getTimeStamp());

            startNegativeTest();

            Login.init(t1).login(approver1);

            tObj.approveO2CLevelOne(txnID);

            Assertion.verifyActionMessageContain("o2c.approval.label.secondLevelNeeded", "Check O2C 2nd Level Approval required", t1);

            stopNegativeTest();

            Login.init(t1).login(approver2);

            tObj.o2cApproval2(txnID);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Login.init(t1).login(usrTRuleCreator);
            TransferRuleManagement.init(t1).setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Reject O2C at level 1
     *
     * @throws Exception
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void SYS_TC_O2C_S_0015() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0015",
                "To verify that Network admin can Reject O2C amount at level 1.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);

        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        CurrencyProvider provider1 = DataFactory.getDefaultProvider();
        TransactionManagement.init(t1).initiateAndRejectO2C(payee, provider1.ProviderName, o2cTxnAmount, Constants.REMARKS, DataFactory.getTimeStamp());
    }


    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when MSISDN left blank
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_O2C_S_0001() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0001",
                "To verify that Network Admin should not be able to initiate O2C to Channel User when MSISDN left blank.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        O2CInitiation_Page1 page1 = new O2CInitiation_Page1(t1);

        page1.navigateToOwner2ChannelInitiationPage();
        page1.setMobileNumber(Constants.SPACE_CONSTANT);
        page1.clickSubmit();

        Assertion.alertAssertion("ci.error.msisdnvalue", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when  Invalid MSISDN (Special character) is entered
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_O2C_S_0028() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0028",
                "To verify that Network Admin should not be able to initiate O2C to Channel User when invalid MSISDN (Special Character)  is entered.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        O2CInitiation_Page1 page1 = new O2CInitiation_Page1(t1);

        page1.navigateToOwner2ChannelInitiationPage();
        page1.setMobileNumber(Constants.SPECIAL_CHARACTER_CONSTANT);
        page1.clickSubmit();

        Assertion.alertAssertion("ci.error.onlynum", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when invalid MSISDN (Alphabets) entered
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_O2C_S_0029() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0029",
                "To verify that Network Admin should not be able to initiate O2C to Channel User when invalid MSISDN (Alphabets)  is entered.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        O2CInitiation_Page1 page1 = new O2CInitiation_Page1(t1);

        page1.navigateToOwner2ChannelInitiationPage();
        page1.setMobileNumber(Constants.ALPHABET_CONSTANT);
        page1.clickSubmit();

        Assertion.alertAssertion("ci.error.onlynum", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when amount is blank
     *
     * @throws Exception
     */
    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void SYS_TC_O2C_S_0030() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0030",
                "To verify that Network Admin should not be able to initiate O2C to Channel User  when Amount left blank.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).
                initiateO2C(payee, Constants.SPACE_CONSTANT, Constants.REMARKS, DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("o2c.initiate.error.amount", "Verify Error Message with blank amount", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC :Perform O2C when invalid amount is entered
     *
     * @throws Exception
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_O2C_S_0031() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0031",
                "To verify that Network Admin should not be able to initiate O2C to Channel User  when invalid Amount  is entered.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateO2C(payee, Constants.ALPHANUMERIC_CONSTANT, Constants.REMARKS, DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("inv.o2c.error.improperAmount", "Verify Error Message with invalid amount", t1);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_O2C_S_0031_a() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0031_a",
                "To verify that Network Admin should not be able to initiate O2C to Channel User  when negative Amount  is entered.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateO2C(payee, Constants.NEGATIVE_AMOUNT_CONSTANT, Constants.REMARKS, DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("opt.withdraw.error.amountVal", "Verify Error Message with invalid amount", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when when Remarks left blank
     *
     * @throws Exception
     */
    @Test(priority = 16, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_O2C_S_0032() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0032",
                "To verify that Network Admin should not be able to initiate O2C to Channel User  when Remarks left blank.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateO2C(payee, Constants.O2C_TRANS_AMOUNT, Constants.SPACE_CONSTANT, DataFactory.getTimeStamp());
        Assertion.verifyErrorMessageContain("o2c.initiate.error.remark", "Verify Error Message with blank remarks", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when Reference number left blank
     *
     * @throws Exception
     */
    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void SYS_TC_O2C_S_0033() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0033",
                "To verify that Network Admin should not be able to initiate O2C to Channel User when Reference number left blank.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateO2C(payee, Constants.O2C_TRANS_AMOUNT, Constants.REMARKS, Constants.SPACE_CONSTANT);
        Assertion.verifyErrorMessageContain("o2c.initiate.error.referenceNumber", "Verify Error Message with blank reference", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when invalid Reference number is entered.
     *
     * @throws Exception
     */
    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void SYS_TC_O2C_S_0034() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0034",
                "To verify that Network Admin should not be able to initiate O2C to Channel User when invalid Reference number is entered.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateO2C(payee, Constants.O2C_TRANS_AMOUNT, Constants.REMARKS, Constants.SPECIAL_CHARACTER_CONSTANT);
        Assertion.verifyErrorMessageContain("o2c.initiate.error.referenceNumber.AlphaNumeric", "Verify Error Message with invalid reference", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when invalid Payment number is entered.
     *
     * @throws Exception
     */
    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void SYS_TC_O2C_S_0035() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0035",
                "To verify that Network Admin should not be able to initiate O2C to Channel User when invalid Payment number is entered.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateO2C(payee, Constants.O2C_TRANS_AMOUNT, Constants.REMARKS, DataFactory.getTimeStamp());

        O2CInitiation_Page1 page1 = new O2CInitiation_Page1(t1);
        page1.clickBackButton();
        page1.setPaymentNumber(Constants.SPECIAL_CHARACTER_CONSTANT);
        page1.clickSubmit();

        Assertion.verifyErrorMessageContain("o2c.initiation.numeric.paymentNumber", "Verify Error Message with invalid Payment Number", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when when "Channel User" is suspended
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void SYS_TC_O2C_S_0036() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0036",
                "To verify that \"Network Admin\" should not be able to initiate O2C to " +
                        "\"Channel User\" when \"Channel User\" is suspended.\n").assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);
        suspendChUser = CommonUserManagement.init(setupNode).getSuspendedUser(Constants.WHOLESALER, null);

        Login.init(t1).login(payer);
        startNegativeTest();
        TransactionManagement.init(t1).initiateO2C(suspendChUser, Constants.O2C_TRANS_AMOUNT, Constants.REMARKS, DataFactory.getTimeStamp());

        Assertion.verifyErrorMessageContain("reciever.suspended", "Verify Error Message for Suspended User", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when when "Channel User" is suspended
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_O2C_S_0037() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0037",
                "To verify that system should display the same values on confirmation screen which are entered in Owner to Channel Transfer Initiate screen");
        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);


        Login.init(t1).login(payer);

        String referenceNumber = DataFactory.getTimeStamp();
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).
                initiateO2C(payee, o2cTxnAmount, Constants.REMARKS, referenceNumber);

        O2CInitiation_Page1 page = new O2CInitiation_Page1(t1);
        page.verifyValuesInConfirmationScreen(payee, referenceNumber);
    }


    /**
     * TEST : NEGATIVE TEST
     * ID : P1_TC_176
     * DESC : modify the MSISDN of Channel user if their transaction are in O2C approval stage
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_176() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_176",
                "To verify that Network Admin can't modify the MSISDN of a Channel user if an O2C transaction with that Channel User is pending at O2C approval stage.");
        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        Login.init(t1).login(payer);

        TransactionManagement.init(t1).
                initiateO2C(payee, o2cTxnAmount, Constants.REMARKS, DataFactory.getTimeStamp());
        ChannelUserManagement.init(t1).initiateChannelUserModification(payee).
                modifyChannelUserMSISDN(payee);
    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Perform O2C when when "Channel User" is suspended
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void SYS_TC_O2C_S_0041() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_O2C_S_0041",
                "To verify that O2C transaction should not be successful if transaction amount is more than defined slab values(To value).\n");
        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);

        BigDecimal o2cAmount = MobiquityGUIQueries.getSlabRangeFromServiceCharge(sCharge, Services.O2C);
        o2cAmount = o2cAmount.add(new BigDecimal(10));

        Login.init(t1).login(payer);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).
                initiateO2C(payee, o2cAmount.toString(), Constants.REMARKS, DataFactory.getTimeStamp());

        if (Assertion.checkErrorMessageContain("amount.morethan.serviceCharge", "Verify Error Message", t1)) {
        } else {
            Assertion.verifyErrorMessageContain("00043", "Verify Error Message.", t1);
        }
    }

    /**
     * TEST : NEGATIVE TEST
     * ID : P1_TC_173
     * DESC : application should not display any  error message i.e.There is a problem in Application for an O2C transaction," +
     * "when the Transaction amount is less than or equal to all the service charges, commissions and tax deductions defined for that service.
     *
     * @throws Exception
     */
    @Test(priority = 21, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void P1_TC_173() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_173",
                "To verify that application should not display any  error message i.e.There is a problem in Application for an O2C transaction," +
                        "when the Transaction amount is less than or equal to all the service charges, commissions and tax deductions defined for that service.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1);

        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        BigDecimal o2cAmount = MobiquityGUIQueries.getMinTransferValue(sCharge.ServiceChargeName);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(payer);
        String transID = TransactionManagement.init(t1).
                initiateO2C(payee, o2cAmount.toString(), Constants.REMARKS, DataFactory.getTimeStamp());

        if (transID != null) {
            Login.init(t1).login(payer);
            TransactionManagement.init(t1).o2cApproval1(transID);
        }

    }

    /**
     * TEST : NEGATIVE TEST
     * ID : P1_TC_538
     * DESC : To Verify that valid user can do O2C Enquiry, Also verify that If Non mandatory " +
     * "fields are not selected and only Manadatory field is selected and  perform operation, application should not display error.
     *
     * @throws Exception
     */
    @Test(priority = 22, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1})
    public void P1_TC_538() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_538\n",
                "To Verify that valid user can do O2C Enquiry, Also verify that If Non mandatory " +
                        "fields are not selected and only Manadatory field is selected and  perform operation, application should not display error.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1);
        payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        transID = MobiquityGUIQueries.getLastTransactionIDUsingMSISDN(Services.O2C, payee.MSISDN);

        Login.init(t1).login(payer);
        ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingMobileNumber(payee.MSISDN, transID);
        if (!Assertion.checkErrorMessageContain("notif.error.allFields", "Verify When Non Mandatory Fields not entered", t1)) {
            t1.pass("No Error Present.");
        } else {
            t1.pass("Error Present.");
        }
    }

    /**
     * TEST : NEGATIVE TEST
     * ID : P1_TC_017
     * DESC : doing enquiry of "operator to channel transfer" Wallet Type should be displayed for both Normal and commission wallet " +
     * "once clickOnSubmitButton button is clicked and again on click of "Back" button in the dropdown of the wallet type.
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C})
    public void P1_TC_017() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_017",
                "To verify that while doing enquiry of \"operator to channel transfer\" Wallet Type should be displayed for both Normal and commission wallet " +
                        "once clickOnSubmitButton button is clicked and again on click of \"Back\" button in the dropdown of the wallet type");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C);

        Login.init(t1).login(payer);
        startNegativeTest();
        ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingMobileNumber(payee.MSISDN, transID);
        ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(t1);
        page.clickOnBack();
        stopNegativeTest();

        List<String> walletOptions = page.getWalletOptions();
        Assertion.verifyListContains(walletOptions, wNormal.WalletName, "Verify that Normal Wallet is available in the Wallet Dropdown", t1);
        Assertion.verifyListContains(walletOptions, wCommission.WalletName, "Verify that Commission Wallet is available in the Wallet Dropdown", t1);
    }


}
