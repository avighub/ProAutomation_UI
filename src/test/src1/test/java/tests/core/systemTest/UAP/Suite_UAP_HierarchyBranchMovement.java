package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.channelUserManagement.HierarchyBranchMovement;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 11/3/2017.
 */
public class Suite_UAP_HierarchyBranchMovement extends TestInit {

    private OperatorUser hierarchyMover;
    private User usrWholeSaler, childUserRet;


    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        hierarchyMover = DataFactory.getOperatorUserWithAccess("UTL_MCAT");
        usrWholeSaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        childUserRet = new User(Constants.RETAILER, usrWholeSaler);
    }


    /**
     * TEST : TC0081: Hierarchy Branch Movement
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0})
    public void HierarchyBranchMovement() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("TC0081: Hierarchy Branch Movement", "To verify that the valid user can perform channel user movement to another parent or another owner in the same domain.");

            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.HIERARCHY_BRANCH_MOVEMENT, FunctionalTag.ECONET_UAT_5_0);

            ChannelUserManagement.init(t1).createChannelUser(childUserRet, false);

            Login.init(t1).login(hierarchyMover);

            HierarchyBranchMovement.init(t1).initiateChUserHierarchyMovement(childUserRet, usrWholeSaler);

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }
}
