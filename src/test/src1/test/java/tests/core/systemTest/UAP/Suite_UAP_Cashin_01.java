package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.SentSMS;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for Cash In and out
 *
 * @author navin.pramanik
 */
public class Suite_UAP_Cashin_01 extends TestInit {

    private User subs;
    private User cashinUser, cashoutUser;

    public void checkPre(User payerMSISDN, User payeeMSISDN) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payerMSISDN, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payeeMSISDN, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payerMSISDN, User payeeMSISDN) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payerMSISDN, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payeeMSISDN, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }


    /**
     * Test Case : CASHIN
     * DESC : To verify that the valid user can perform Cash in.
     * <p>
     * This Case Is Already Covered Base Setup
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC161() throws Exception {

        ExtentTest t1 = pNode.createNode("TC161: Cashin WEB", "To verify that the wholesaler can perform Cashin.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.CASHIN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge cashinService = new ServiceCharge(Services.CASHIN, cashinUser, subs, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(cashinService);

            if (!ConfigInput.isCoreRelease || ConfigInput.is4o14Release) {

                ServiceChargeManagement.init(t1).
                        configureServiceCharge(cashinService);
            }
            checkPre(cashinUser, subs);

            Login.init(t1).login(cashinUser);

            String transID = TransactionManagement.init(t1).
                    performCashIn(subs, Constants.CASHIN_TRANS_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

            checkPost(cashinUser, subs);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


        // SentSMS.verifyPayerPayeeDBMessage(cashinUser, subs, Services.CASHIN, transID, t1);
        //String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(subs.MSISDN,"DESC");
        //DBAssertion.verifyDBMessageContains(dbMessage,"01616","Verify DB Message For O2C Transaction",t1,subs.LoginId,Constants.CASHIN_TRANS_AMOUNT);

    }


    /**
     * Test Case : CASHOUT
     * DESC : To verify that the valid user can perform Cashin.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHOUT, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 2)
    public void TC162() throws Exception {

        ExtentTest t1 = pNode.createNode("TC162: CashOut WEB",
                "To verify that the wholesaler can perform Cash-Out.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.CASHOUT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            cashoutUser = DataFactory.getChannelUserWithAccess("COUT_WEB");

            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            if (!ConfigInput.isCoreRelease) {
                ServiceCharge cashoutService = new ServiceCharge(Services.CASHOUT, subs, cashoutUser, null, null, null, null);

                TransferRuleManagement.init(t1)
                        .configureTransferRule(cashoutService);

                ServiceChargeManagement.init(t1).
                        configureServiceCharge(cashoutService);
            }

            checkPre(cashoutUser, subs);

            Login.init(t1).login(cashoutUser);

            String tid = TransactionManagement.init(t1)
                    .performCashOut(subs, Constants.MIN_CASHOUT_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

            if (tid != null) {
                //Transactions.init(t1).webCashOutApprovalBySubs(subs.MSISDN, tid, Constants.MIN_CASHOUT_AMOUNT);
                String requestID = MobiquityGUIQueries.dbGetServiceRequestId(tid);
                Transactions.init(t1).cashoutApprovalBysubs(subs, requestID);
                checkPost(cashoutUser, subs);

                DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(tid), Constants.TXN_STATUS_SUCCESS,
                        "Transaction Status in DB verified successfully.", t1);
                SentSMS.verifyPayerPayeeDBMessage(subs, cashoutUser, Services.CASHOUT, tid, t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

}
