/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 2-Jan-2018
 *  Purpose: Biller Management System Cases
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.BillerCategory;
import framework.entity.OperatorUser;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/**
 * Contains the System cases for Biller Management
 *
 * @author navin.pramanik
 */
public class SuiteSystemBillerRegnMgmt01 extends TestInit {

    private static String categoryCode, providerName;
    private static boolean isCatExist = false;
    private OperatorUser categoryCreator, billerCreator;
    private Biller billerObj, billerObj2;
    private BillerCategory billerCategory;
    private String selectTCPBlank = "Select TCP";


    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {
            categoryCreator = DataFactory.getOperatorUserWithAccess("UTL_MCAT");
            billerCreator = DataFactory.getOperatorUserWithAccess("UTL_CREG");
            categoryCode = Constants.BILLER_CATEGORY;
            providerName = DataFactory.getDefaultProvider().ProviderName;
            billerCategory = new BillerCategory(categoryCode);
            billerObj = new Biller(providerName, Constants.BILLER_CATEGORY, Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_BOTH);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() throws Exception {
        billerObj = new Biller(providerName, Constants.BILLER_CATEGORY, Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_BOTH);
    }

    /**
     * To check the Biller Category exist
     *
     * @param t1 Extent Test
     * @throws Exception
     */
    public void checkForBillerCategory(ExtentTest t1) throws Exception {

        Login.init(t1).login(categoryCreator);

        //it will check if Biller category already present or not
        if (!isCatExist)
            isCatExist = BillerManagement.init(t1).
                    checkBillerCategoryExist(billerCategory.BillerCategoryName);
        //If Biller Category not already present then add Biller Category
        if (!isCatExist)
            BillerManagement.init(t1).addBillerCategory(billerCategory);
    }

    /**
     * TEST TYPE   : POSITIVE
     * TEST CASE   : SYS_TC_BILL_CATEGORY_N_0001 : Biller Registration Positive
     * DESCRIPTION : To verify that "Network Admin" should be able to register new "Biller" in the system successfully.
     *
     * @throws Exception
     */
    //@Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM ,FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0001() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0001 : Biiler Regn Positive",
                "To verify that \"Network Admin\" should be able to register new \"Biller\" in the system successfully.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION, FunctionalTag.BILLER_MANAGEMENT);

        checkForBillerCategory(t1);

        //Initiate and approve Biller registration (Note: Login method is written inside)
        BillerManagement.init(t1).
                initAndApproveBillerRegistration(billerObj);


        // billerObj.writeDataToExcel();
    }


    /**
     * TEST TYPE   : POSITIVE
     * TEST CASE   : SYS_TC_BILL_CATEGORY_N_0002 : Biiler Registration Positive
     * DESCRIPTION : To verify that "Channel Admin" should be able to register new "Biller" in the system successfully.
     *
     * @throws Exception
     */
    //@Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM ,FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0002() throws Exception {

        ExtentTest t2 = pNode.createNode("SYS_TC_BILLER_REGN_N_0001 : Biiler Regn Positive",
                "To verify that \"Channel Admin\" should be able to register new \"Biller\" in the system successfully.");

        t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION);

        List<OperatorUser> operatorUsers = DataFactory.getOperatorUsersWithAccess("UTL_CREG");

        checkForBillerCategory(t2);

        for (OperatorUser billerCreatorUsr : operatorUsers) {

            billerObj2 = new Biller(providerName, Constants.BILLER_CATEGORY, Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_BOTH);

            Login.init(pNode).login(billerCreatorUsr);

            BillerManagement.init(t2).
                    initiateBillerRegistration(billerObj2);

            Login.init(pNode).login(billerCreatorUsr);

            BillerManagement.init(t2).
                    initiateApproveBiller(billerObj2, true);
        }


        //Initiate and approve Biller registration (Note: Login method is written inside)
        //TODO Create New Biller Object


    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0003 : Biller Category Negative
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system when
     * "Login ID " field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0003() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0003 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when " +
                        "\"Login ID \" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);
        //Setting Login ID as Blank
        billerObj.setLoginId(Constants.BLANK_CONSTANT);
        //Initiate and approve Biller registration (Note: Login method is written inside)
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("error.username.required", "Negative LoginID Test", t1);
    }

    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system
     * when "Password" field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0004() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0004 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Password\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);
        //Setting Password as Blank
        billerObj.setPassword(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("error.password.required", "Negative Password Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0005
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system
     * <p>when "Confirm Password" field is left blank.</p>
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0005() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0005 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Confirm Password\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);
        //Setting Login ID as Blank
        billerObj.setConfirmPassword(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("user.validation.confirm.password", "Negative LoginID Test", t1);
    }

    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0005
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system
     * when "Password" and "Confirm Password" are not same.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0006() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0006 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Password\" and \"Confirm Password\" are not same.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        //Setting Confirm Password of billerObj
        billerObj.setConfirmPassword("jdwjd");

        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("systemparty.error.passnotsame", "Negative Confirm Password Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0007
     * DESCRIPTION : To verify that "Network Admin" should not be able to
     * add Biller in the system when "Biller Name" field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0007() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0007 : Biller Regn Negative",
                "To verify that 'Network Admin' should not be able to add Biller in the system when 'Biller Name' field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);

        //Setting Biller Name of billerObj
        billerObj.setBillerName(Constants.BLANK_CONSTANT);

        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.label.error2", "Negative Biller Name Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0008
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system
     * when "Biller Code" field is left blank.");
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0008() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0008 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Biller Code\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);
        //Setting Confirm Password of billerObj
        billerObj.setBillerCode(Constants.BLANK_CONSTANT);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.label.error2", "Negative Biller Code Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0009
     * DESCRIPTION : To verify that "Network Admin" should not be able to
     * add Biller in the system when special characters are entered in "Biller Code" field .
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0009() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0009 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when special characters are entered in \"Biller Code\" field .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);
        //Setting Confirm Password of billerObj
        billerObj.setBillerCode(Constants.SPECIAL_CHARACTER_CONSTANT);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.label.error3", "Negative Biller Code Test", t1);
    }

    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0010
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller
     * in the system when "Biller E-mail" field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0010() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0010 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Biller E-mail\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        //Setting Confirm Password of billerObj
        billerObj.setEmail(Constants.BLANK_CONSTANT);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("subs.error.emailId", "Negative LoginID Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0011
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller
     * in the system when "Biller E-mail" format is not correct.
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0011() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0011 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Biller E-mail\" format is not correct.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        billerObj.setEmail("abcd");

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.email", "Negative E-Mail ID Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0012
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system when "Service Level" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0012() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0012 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Service Level\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        //Setting Confirm Password of billerObj
        billerObj.setServiceLevel(Constants.BLANK_CONSTANT);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.label.error2", "Negative LoginID Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0013
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system
     * when "Paid Bill Notification Frequency" field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0013() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0013 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Paid Bill Notification Frequency\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        //Setting Confirm Password of billerObj
        billerObj.setServiceLevel(Constants.BLANK_CONSTANT);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.label.error2", "Negative LoginID Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0014
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system
     * when special characters are entered in "Paid Bill Notification Frequency" field .
     *
     * @throws Exception
     */
    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0014() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0014 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system " +
                        "when special characters are entered in \"Paid Bill Notification Frequency\" field .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        //Setting Confirm Password of billerObj
        billerObj.setPaidNotificationFrequency(Constants.SPECIAL_CHARACTER_CONSTANT);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.paidBillNotf", "Invalid Paid Bill Notification Frequency Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0015
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system
     * when "Auto Bill Deletion Frequency" field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0015() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0015 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Auto Bill Deletion Frequency\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        //Setting Confirm Password of billerObj
        billerObj.setDeleteFrequency(Constants.BLANK_CONSTANT);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.label.error2", "Negative LoginID Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0016
     * DESCRIPTION : To verify that "Network Admin" should not be able to
     * add Biller in the system when special characters are entered in "Auto Bill Deletion Frequency" field .
     *
     * @throws Exception
     */
    @Test(priority = 16, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0016() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0016 : Biller Regn Negative", "To verify that" +
                " \"Network Admin\" should not be able to add Biller in the system when special characters are entered in \"Auto Bill Deletion Frequency\" field .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        //Setting Confirm Password of billerObj
        billerObj.setDeleteFrequency(Constants.SPECIAL_CHARACTER_CONSTANT);

        //Login with Biller Creator
        Login.init(t1).login(billerCreator);
        //Initiate and approve Biller registration
        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.autoBillDelete", "Negative Auto Bill Delete Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0017
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system when
     * "Biller Category Name" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0017() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0017 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Biller Category Name\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        Login.init(t1).login(billerCreator);

        //Setting Biller name as blank of the  billerObj
        billerObj.setBillerCategoryName(Constants.VALUE_SELECT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.billercategory", "Negative Biller Category Name Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0018
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system
     * when "Process Type" is not selected.     *
     *
     * @throws Exception
     */
    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0018() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0018 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Process Type\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        //Set Process Type dropdown as Blank
        billerObj.setProcessType(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.process", "Negative Biller Category Name Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0019
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller
     * in the system when "Biller Type" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0019() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0019 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Biller Type\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        //Set Biller Type dropdown as Blank
        billerObj.setBillerType(Constants.BLANK_CONSTANT);
        billerObj.setPaymentSubType(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.type", "Negative Biller Type Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0020
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller
     * in the system when "Bill Amount" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0020() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0020 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Bill Amount\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        //Set Biller Type dropdown as Blank
        billerObj.setBillAmount(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.billamount", "Negative Bill Amount Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0021
     * DESCRIPTION : To verify that "Network Admin" should not be able to
     * add Biller in the system when "Sub Type" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 21, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0021() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0021 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Sub Type\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        //Set Biller Type dropdown as Blank
        billerObj.setPaymentSubType(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.partpaysubtype", "Negative Payment SubType Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0022
     * DESCRIPTION : To verify that "Network Admin" should not be able to
     * add Biller in the system when "Payment Effected To" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 22, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0022() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0022 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"Payment Effected To\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        //Set Biller Payment Effected To as Blank
        billerObj.setPaymentEffectedTo(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.error.paymentfeeto", "Negative Biller Category Name Test", t1);
    }

    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0023
     * DESCRIPTION : To verify that "Network Admin" should not be able to
     * add Biller in the system when "MFS Provider" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 23, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0023() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0023 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"MFS Provider\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        //Set Biller Provider Name as Blank
        billerObj.setProviderName(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.tcpProviderChk", "Negative Provider Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0024
     * DESCRIPTION : To verify that "Network Admin" should not be able to add Biller in the system when "TCP" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 24, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0024() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0024 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when \"TCP\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        //Set Biller TCP
        billerObj.setTCP(selectTCPBlank);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                initiateBillerRegistration(billerObj);

        Assertion.verifyErrorMessageContain("utility.merchant.providerTcpChk", "Negative Biller Category Name Test", t1);
    }


    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILLER_REGN_N_0025
     * DESCRIPTION : To verify that "Network Admin" should not be able to
     * add Biller in the system when same Biller already exist in the System.     *
     *
     * @throws Exception //TODO Remove Comment from @Test
     */
    //@Test(priority = 25, groups = { FunctionalTag.PVG_SYSTEM,FunctionalTag.BILLER_REGISTRATION})
    public void SYS_TC_BILLER_REGN_N_0025() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILLER_REGN_N_0025 : Biller Regn Negative",
                "To verify that \"Network Admin\" should not be able to add Biller in the system when same Biller already exist in the System.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.BILLER_REGISTRATION);

        checkForBillerCategory(t1);

        Login.init(t1).login(billerCreator);

        boolean billerExist = BillerManagement.init(t1).checkBillerExists(billerObj.BillerName, billerObj.BillerCode);
        if (!billerExist)
            BillerManagement.init(t1).initAndApproveBillerRegistration(billerObj);

        if (billerExist) {
            BillerManagement.init(t1).startNegativeTest().
                    initiateBillerRegistration(billerObj);

            //TODO verify exact Error Message
            Assertion.verifyErrorMessageContain("utility.merchant.label.error2", "Negative Biller Category Name Test", t1);
        }


    }


}
