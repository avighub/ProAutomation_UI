package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;

/**
 * Contain the cases for Bank, Bank Type Master & Wallet Type Master
 *
 * @author navin.pramanik
 */
public class Suite_UAP_BankMaster_01 extends TestInit {

    private String walletName;
    private String bankName, providerName, bankAcNo;
    private Bank bank;


    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {
            bankName = "AUTO" + DataFactory.getRandomNumber(3);
            providerName = DataFactory.getDefaultProvider().ProviderName;
            walletName = DataFactory.getAllWallet().get(0).WalletName;
            bankAcNo = DataFactory.getTimeStamp();
            bank = new Bank(defaultProvider.ProviderId,
                    Constants.TEMP_BANK_TRUST,
                    null,
                    null,
                    true,
                    false);

            /**
             * Test 1, rest all tests are dependent on this
             */
            ExtentTest t1 = pNode.createNode("TC0004 :Add Bank",
                    "To verify that the Superadmin can add a bank (With Pool account number) with all the details.")
                    .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                            FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

            Login.init(t1).loginAsSuperAdmin("BANK_ADD");
            CurrencyProviderMapping.init(t1).addBank(bank);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * @throws IOException
     */
    @Test(priority = 0, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP,
            FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void bankAdd() throws Exception {

        ExtentTest t2 = pNode.createNode("TC0005 : Add Service Provider Bank Account",
                "To verify that the Super Admin can Add service provider bank account.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1);

        try {
            Login.init(t2).loginAsSuperAdmin("OPT_BANK_ACC_ADD");
            CurrencyProviderMapping.init(t2).addServiceProviderBankAccount(bankName, bankAcNo);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();

        ExtentTest t3 = pNode.createNode("TC0007 :Modify Service Provider Bank A/c",
                "To verify that the 'Super Admin' can modify service provider bank account.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER,
                        FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        try {
            Login.init(t3).loginAsSuperAdmin("OPT_BANK_ACC_MOD");
            CurrencyProviderMapping.init(t3).modifyServiceProviderBankAccount(bankName, bankAcNo);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        ExtentTest t4 = pNode.createNode("TC0101 : Map Bank with Provider",
                "To verify that the Super Admin can Add MFS provider Bank type master.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.MFS_PROVIDER_BANK, FunctionalTag.ECONET_SIT_5_0);

        try {
            Login.init(t4).loginAsSuperAdmin("MFSBTM01");
            CurrencyProviderMapping.init(t4).initiateMFSProviderBankMapping(providerName, bankName);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();

        ExtentTest t5 = pNode.createNode("TC0099 : Modify Bank Services",
                "To verify that the \"Super Admin\" can modify MFS provider Bank type master..")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_BANK);


        try {
            Login.init(t5).loginAsSuperAdmin("MFSBMD");
            CurrencyProviderMapping.init(t5).modifyServiceProviderBankMapping(providerName, bankName);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
            FunctionalTag.PVG_UAP, FunctionalTag.MFS_PROVIDER_WALLET})
    public void mapWallet() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0104 :Add Wallet Mapping",
                "To verify that the Super Admin can Add MFS provider wallet type master.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.MFS_PROVIDER_WALLET);

        try {
            Login.init(t1).loginAsSuperAdmin("MFSWTM01");
            CurrencyProviderMapping.init(t1).initiateMFSProviderWalletMapping(providerName, walletName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    // Covered in base setup
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP,
            FunctionalTag.MFS_PROVIDER_WALLET})
    public void modifyWalletMap() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0102 : Modify Wallet Mapping",
                "To verify that the Super Admin can Modify MFS provider wallet type master.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_WALLET);

        try {
            Login.init(t1).loginAsSuperAdmin("MFSMD");
            CurrencyProviderMapping.init(t1).modifyMFSProviderWalletMapping(providerName, walletName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * TEST : TC0103 :Delete Bank Mapping
     * DESC : To verify that the "Super Admin" can delete MFS provider Bank type master.
     *
     * @throws IOException
     */

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP,
            FunctionalTag.MFS_PROVIDER_BANK, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.DEPENDENCY_TAG})
    public void deleteBankMapping() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0103 :Delete Bank Mapping",
                "To verify that the Super Admin can delete MFS provider Bank type master.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_BANK, FunctionalTag.ECONET_SIT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin("MFSBMD");
            CurrencyProviderMapping.init(t1).deleteServiceProviderBankMapping(DataFactory.getDefaultProvider().ProviderId, bankName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * TEST : deleteBank
     * DESC : To verify that the valid user can delete bank.
     *
     * @throws IOException
     */
    @Test(enabled = false, priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SMOKE,
            FunctionalTag.PVG_UAP, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0})
    public void deleteBank() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0006 : Delete Bank",
                "To verify that the \"Super Admin\" user can delete bank.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin("BANK_ADD");
            CurrencyProviderMapping.init(t1).deleteBank(bank);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
