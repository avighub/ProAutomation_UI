/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 9-Dec-2017
 *  Purpose: Feature of BlackListManagement
 */

package tests.core.systemTest.newSystemCases;


import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.loginPasswordManagement.LoginPasswordManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * /**
 * Created by febin.thomas on 11/24/2017.
 */
public class SuiteSystemLoginPasswordManagement extends TestInit {
    private static final String defSecretInput = "000000";
    private static final int invalidAttempt = 3;
    protected OperatorUser resetPINUser;
    protected SuperAdmin saMaker;
    protected SuperAdmin saChecker;
    protected OperatorUser user;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        user = new OperatorUser(Constants.NETWORK_ADMIN);
        saMaker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
        saChecker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");

    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.LOGIN, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC058() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0085_1 :Login Pass Mgmt: 2", "To verify that a system users can change his password successfully.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.LOGIN, FunctionalTag.ECONET_SIT_5_0);

        OperatorUserManagement.init(t1)
                .createAdmin(user);

        LoginPasswordManagement.init(t1).changePassword(user.Password, "Com@97531");

    }


}