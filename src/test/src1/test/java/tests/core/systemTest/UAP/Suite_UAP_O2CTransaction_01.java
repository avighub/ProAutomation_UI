package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.channelEnquiry.ChannelEnquiry;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 7/20/2017.
 */


public class Suite_UAP_O2CTransaction_01 extends TestInit {

    private static String transID, refNo;
    private static MobiquityGUIQueries dbHandler;
    private OperatorUser payer, o2cInitiator, o2cApprover1;
    private User payee;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        payee = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
        payer = new OperatorUser(Constants.NETWORK_ADMIN);
        o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
        o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");
        dbHandler = new MobiquityGUIQueries();

    }


    public void checkPre(User payee) {
        DBAssertion.prePayerBal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payee) {
        DBAssertion.postPayerBal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }


    /**
     * Test Case : ownertoChannelTest : Owner to Channel Transfer
     * To verify that the valid user can perform
     * Owner to Channel Transfer
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 1)
    public void ownerToChannelTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0116 : O2C (Owner To Channel)",
                "To verify that the valid user can perform Owner to Channel Transfer.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        t1.info("Checking Service Charge for the Transaction..");

        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        Login.init(t1).login(o2cInitiator);

        checkPre(payee);

        transID = TransactionManagement.init(t1).
                initiateO2C(payee, Constants.O2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

        if (transID != null) {
            Login.init(t1).login(o2cApprover1);
            TransactionManagement.init(t1).o2cApproval1(transID);

            checkPost(payee);
            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

            //SentSMS.verifyPayeeDBMessage(o2cInitiator, payee, Services.O2C, transID, t1);
            //TODO Correct the Message Code and Remove the commented line
            /*String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(payee.MSISDN,"DESC");
            DBAssertion.verifyDBMessageContains(dbMessage,"01616","Verify DB Message For O2C Transaction",t1,payee.LoginId,Constants.O2C_TRANS_AMOUNT);
*/
        }

    }


    /**
     * TEST : TC0043 ( Channel Enquiry-->O2C Transfer Enquiry)
     * In this Test case first it searches for Operator User in Operator User sheet with Role code 'O2C_ENQ'
     * then it fetch the O2C transaction ID from the above Test Case of O2C
     * if O2C transaction got failed then it fetch Last TXN ID from DB and then perform the enquiry of O2C transaction.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 2)
    public void channelEnquiry() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0043 : Channel Enquiry-->O2C Transfer Enquiry", "To verify that the valid user can perform Owner to Channel Enquiry.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.CHANNEL_ENQUIRIES, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        OperatorUser enqUsr = DataFactory.getOperatorUserWithAccess("O2C_ENQ");


        if (transID == null) {
            transID = dbHandler.dbGetLastTransID(Services.O2C);
        }


        if (transID != null) {
            Login.init(t1).login(enqUsr);
            ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingTxnID(transID);
        } else {
            t1.skip("Skipping test case as Transaction ID is NULL");
        }

    }


}


