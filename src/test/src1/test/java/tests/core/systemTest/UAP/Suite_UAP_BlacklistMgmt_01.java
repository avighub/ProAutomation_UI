package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.blackListManagement.BlackListManagement;
import framework.features.common.Login;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for Suite_Blacklist
 *
 * @author navin.pramanik
 */
public class Suite_UAP_BlacklistMgmt_01 extends TestInit {

    private static String senderMSISDN, recievMSISDN;
    private static String firstName, lastName, dob, blklMsisdn = null;
    private OperatorUser p2pBlkl, custblkl;
    private User customer;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        firstName = "BLKL" + DataFactory.getRandomNumber(3);
        lastName = "BLKL" + DataFactory.getRandomNumber(3);
        dob = new DateAndTime().getYear(-20);
        blklMsisdn = DataFactory.getAvailableMSISDN();
        customer = new User(Constants.SUBSCRIBER);
        /**
         *  customer is never used in system so why we are creating it
         *  */
        //SubscriberManagement.init(pNode).createDefaultSubscriberUsingAPI(customer);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP}, priority = 1)
    public void blacklistP2PTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0027 :  Suit_Blacklist P2P Sender", "To verify that the valid user can Suit_Blacklist P2P Sender.");
        if (!ConfigInput.isCoreRelease) {
            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.BLACKLIST_P2P);

            p2pBlkl = DataFactory.getOperatorUsersWithAccess("P2POPTCOM").get(0);

            senderMSISDN = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1).MSISDN;
            recievMSISDN = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0).MSISDN;

            Login.init(t1).login(p2pBlkl);

            BlackListManagement.init(t1).doBlacklistP2PSender(recievMSISDN, senderMSISDN, DataFactory.getDefaultProvider().ProviderName);
        } else {
            t1.info("TC0027 is descoped in 5.0");
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP}, dependsOnMethods = "blacklistP2PTest", priority = 2)
    public void unBlacklistP2PTest() throws Exception {

        String[] blackListed = new String[]{senderMSISDN};
        if (!ConfigInput.isCoreRelease) {
            ExtentTest t1 = pNode.createNode("TC0167 : UnBlacklist P2P Sender",
                    "To verify that the valid user can UnBlacklist P2P Sender.")
                    .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.BLACKLIST_P2P);
            p2pBlkl = DataFactory.getOperatorUsersWithAccess("P2POPTCOM").get(0);

            Login.init(t1).login(p2pBlkl);

            BlackListManagement.init(t1).unBlacklistP2P(recievMSISDN, DataFactory.getDefaultProvider().ProviderName, blackListed);
        }

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 3)
    public void custBlacklistTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0051: Customer Blacklist", "To verify that the netadmin can add customer into blacklist of mobiquity.(Single Entry)");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");

        if (custblkl == null) {
            t1.fail("User Not found with role : 'BLK_ABL' . Please check Operator User sheet and ConfigInput.xlsx");
            Assert.fail("User Not found with role : 'BLK_ABL' . Please check Operator User sheet and ConfigInput.xlsx");
        }

        Login.init(t1).login(custblkl);

        BlackListManagement.init(t1).doCustomerBlacklist(firstName, lastName, dob);


    }

    /**
     * Commented this test as it includes DB update query
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0}, priority = 4)
    public void custBlacklistBulkTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0052 : Customer Suit_Blacklist Bulk", "To verify that the valid user can add customer into blacklist of mobiquity.(Bulk Upload)");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.CUSTOMER_BLACK_LISTING, FunctionalTag.ECONET_UAT_5_0);

        User blklSubs = new User(Constants.SUBSCRIBER);

        String fileName = BlackListManagement.init(t1).generateBulkBlacklistCsvFile(customer.MSISDN);

        custblkl = DataFactory.getOperatorUsersWithAccess("BLK_ABL").get(0);

        Login.init(t1).login(custblkl);

        BlackListManagement.init(t1).uploadBulkCustomerBlacklist(fileName);
    }
}
