package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 11/16/2017.
 */
public class SYS_CCE extends TestInit {
    private OperatorUser networkAdmin;
    private User subs;

    @BeforeClass
    public void prequest() throws Exception {
        subs = new User(Constants.SUBSCRIBER);
    }


    @Test(priority = 1, groups = {"PVG_SYSTEM", FunctionalTag.CCE})
    public void SYS_TC_0001() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0001 : CCE", "To verify that valid user can change the consumer/channel user status from Active to Bar");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        subs = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);

    }


    @Test(priority = 2, groups = {"PVG_SYSTEM", FunctionalTag.CCE})
    public void SYS_TC_0002() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0002  : CCE", "To verify that valid user can change the consumer/channel user status from Bar to unBar");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
        Login.init(t1).login(networkAdmin);
        ChannelUserManagement.init(t1).unBarChannelUser(subs, Constants.USER_TYPE_SUBS, Constants.BAR_AS_SENDER);

    }


    @Test(priority = 3, groups = {"PVG_SYSTEM", FunctionalTag.CCE})
    public void SYS_TC_0675() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0675  : CCE", "To verify that CCE should be able to search the consumer or channel user details as follows with minimal effort through Global Search.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
        Login.init(t1).login(networkAdmin);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        // GlobalSearch.init(t1).globalSearchUsingMSISDN(subs);
    }


    @Test(priority = 4, groups = {"PVG_SYSTEM", FunctionalTag.BILL_PAYMENT})
    public void SYS_TC_0694() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_0694 : CCE", "To verfiy that valid user can perform Transaction Correction Initiation");

        networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);

        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).login(subs);

        String transID = TransactionManagement.init(t1).performCashIn(subs, "100", null);

        Login.init(t1).login(networkAdmin);

        TransactionCorrection.init(t1).initiateTxnCorrection(transID, false, false);

    }


}
