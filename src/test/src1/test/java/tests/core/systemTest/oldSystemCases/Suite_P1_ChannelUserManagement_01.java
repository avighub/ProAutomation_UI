package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by rahul.rana on 7/10/2017.
 */
public class Suite_P1_ChannelUserManagement_01 extends TestInit {
    private OperatorUser chAdmDelInitiate, chAdmDelInitiateApp, chAdmModifyChUsr, suspendChUsr, suspendChUsrApprove;
    private User whsUser_01, whsParent_46327;
    private OperatorUser networkAdmin;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            chAdmDelInitiate = DataFactory.getOperatorUserWithAccess("PTY_DCU", Constants.CHANNEL_ADMIN);
            chAdmDelInitiateApp = DataFactory.getOperatorUserWithAccess("PTY_DCHAPP", Constants.CHANNEL_ADMIN);
            chAdmModifyChUsr = DataFactory.getOperatorUserWithAccess("PTY_MCU", Constants.CHANNEL_ADMIN);
            suspendChUsr = DataFactory.getOperatorUserWithAccess("PTY_SCU", Constants.CHANNEL_ADMIN);
            suspendChUsrApprove = DataFactory.getOperatorUserWithAccess("PTY_SCHAPP", Constants.CHANNEL_ADMIN);
            networkAdmin = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");
            whsUser_01 = new User(Constants.WHOLESALER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /*
     * Test           :               TC307
     * Description    :               Channel user Management >> Delete channel user
     *                                To verify that channel admin can delete initiate special super agent
     * date           :               24-07-2017
     * author         :               shruti.gupta
     * */
    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_156() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_156", "Channel user Management >> Delete channel user " +
                "To verify that channel admin can delete initiate special super agent");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {

            //add new User without SystemWallet Association
            ChannelUserManagement.init(extentTest)
                    .createChannelUserDefaultMapping(whsUser_01, false);

            //delete Channel user
            Login.init(extentTest)
                    .login(chAdmDelInitiate);

            // initiate Ch User Deletion
            ChannelUserManagement.init(extentTest).
                    initiateChannelUserDelete(whsUser_01);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /*
     * Test           :               TC277
     * Description    :               To verify that channel admin can reject initiate delete of Parent channel user
     * date           :               21-07-2017
     * author         :               Shruti.gupta
     * */
    @Test(priority = 2)
    public void TC277() throws Exception {
        ExtentTest extentTest = pNode.createNode("TC277", "To verify that channel admin can reject initiate delete of Parent channel user");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        //Login Channel Admin
        Login.init(extentTest)
                .login(chAdmDelInitiateApp);

        //reject delete request
        ChannelUserManagement.init(extentTest)
                .approveRejectDeleteChannelUser(whsUser_01, false);

    }

    /*
     * Test           :               TC308
     * Description    :               Channel user Management >> Delete channel user Approval
     *                                To verify that channel admin can approve delete initiated special super agent.
     * date           :               24-07-2017
     * author         :               shruti.gupta
     * */
    @Test(priority = 3, dependsOnMethods = "P1_TC_156", groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_157() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_157", "Channel user Management >> Delete channel user Approval " +
                "To verify that channel admin can approve delete initiated special super agent.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {
            //re - initiate delete Channel user
            Login.init(extentTest)
                    .login(chAdmDelInitiate);

//        ChannelUserManagement.init(extentTest).
//                initiateChannelUserDelete(whsUser_01);

            //Login approve
            Login.init(extentTest)
                    .login(chAdmDelInitiateApp);

            //approve delete request
            ChannelUserManagement.init(extentTest)
                    .approveRejectDeleteChannelUser(whsUser_01);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /*
     * Test           :               TUNG46327
     * Description    :               To verify that channel admin can initiate delete of Parent channel user
     * date           :               28-07-2017
     * author         :               Shruti.gupta
     * */
    @Test(priority = 4, groups = {FunctionalTag.MONEY_SMOKE})
    public void TUNG46327() throws Exception {
        ExtentTest extentTest = pNode.createNode("TUNG46327",
                "To verify that channel admin can Not initiate delete of Parent channel user");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {
            String parentCategoryCode = DataFactory.getParentCategoryCode(Constants.RETAILER);
            whsParent_46327 = new User(parentCategoryCode);

            // create a user and Bank is not associated
            ChannelUserManagement.init(extentTest)
                    .createChannelUserDefaultMapping(whsParent_46327, true);

            //Add new child user
            User usrChild = new User(Constants.RETAILER, whsParent_46327);

            ChannelUserManagement.init(extentTest)
                    .createChannelUserDefaultMapping(usrChild, false);

            //Login Initiate CH delete
            Login.init(extentTest)
                    .login(chAdmDelInitiate);

            //Delete Parent Channel User
            ChannelUserManagement.init(extentTest)
                    .startNegativeTest()
                    .initiateChannelUserDelete(whsParent_46327);

            Assertion.verifyErrorMessageContain("channel.error.childExist",
                    "To verify that channel admin can Not initiate delete of Parent channel user", extentTest);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        }
        Assertion.finalizeSoftAsserts();

    }

    /*
     * Test           :               TC290
     * Description    :               To verify that channel admin can delete a channel user even if that channel user is suspended in application
     * date           :               27-07-2017
     * author         :               Shruti.gupta
     * */
    @Test(priority = 5)
    public void TC290() throws Exception {
        ExtentTest extentTest = pNode.createNode("TC290", "To verify that channel admin can delete " +
                "a channel user even if that channel user is suspended in application");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        User usr_290 = new User(Constants.RETAILER);

        ChannelUserManagement.init(extentTest)
                .createChannelUserDefaultMapping(usr_290, false);

        Login.init(extentTest)
                .login(suspendChUsr);

        //Suspend a channel User
        ChannelUserManagement.init(extentTest)
                .initiateSuspendChannelUser(usr_290)
                .approveSuspendChannelUser(usr_290, true);

        //Delete Channel User
        Login.init(extentTest)
                .login(chAdmDelInitiate);

        ChannelUserManagement.init(extentTest).
                initiateChannelUserDelete(usr_290);

        //Login Channel Admin
        Login.init(extentTest)
                .login(chAdmDelInitiate);

        //delete suspended Channel User
        ChannelUserManagement.init(extentTest)
                .approveRejectDeleteChannelUser(usr_290);
    }

    /*
     * Test           :               TC289
     * Description    :               To verify that channel admin can delete a channel user even if that channel user is barred
     * date           :               27-07-2017
     * author         :               Shruti.gupta
     * */
    @Test(priority = 7, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC289() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_323", "To verify that channel admin can delete a channel user even if that channel user is barred");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            User usr_289 = new User(Constants.RETAILER);
            ChannelUserManagement.init(extentTest)
                    .createChannelUserDefaultMapping(usr_289, false);

            Login.init(extentTest).login(chAdmDelInitiate);
            //Bar a channel User
            ChannelUserManagement.init(extentTest)
                    .barChannelUser(usr_289, "CH_USERS", "BLK_PR_B");

            Assertion.assertActionMessageContain("2106", "User Barred" + usr_289.LoginId, extentTest);

            //Delete Channel User
            ChannelUserManagement.init(extentTest).
                    deleteChannelUser(usr_289);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /*
     * Test           :               TC288
     * Description    :               To verify that channel admin can't delete a channel user having pending balance in his account
     * date           :               27-07-2017
     * author         :               Shruti.gupta
     * */
    @Test(priority = 9, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC288() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_322", "To verify that channel admin can't delete a channel user having pending balance in his account");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST, FunctionalTag.ECONET_SIT_5_0);
        try {
            User usr_288 = DataFactory.getChannelUserWithCategory(Constants.RETAILER);

            TransactionManagement.init(extentTest)
                    .makeSureChannelUserHasBalance(usr_288);

            //Delete Channel User
            ChannelUserManagement.init(extentTest)
                    .startNegativeTest()
                    .initiateChannelUserDelete(usr_288);

            Assertion.verifyErrorMessageContain("some.wallets.have.balance",
                    "Unable to delete as User having pending balance ", extentTest);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /*
     * Test              :               TC259
     * Description       :               To verify that Network admin can approve
     *                                   the Modify initiated category.
     * date              :               18-07-2017
     * author            :               shruti.gupta
     * */
    @Test(priority = 10, enabled = false, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_149() throws Exception {
        ExtentTest P1_TC_149 = pNode.createNode("P1_TC_149", "To verify that Network admin can approve the Modify initiated category.");
        P1_TC_149.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        String parentCategory = DataFactory.getParentCategoryCode(Constants.RETAILER);
        User usr_259 = new User(Constants.RETAILER);

        ChannelUserManagement.init(P1_TC_149)
                .createChannelUserDefaultMapping(usr_259, false);

        // login as Channel Admin who can initiate Channel User Modification
        Login.init(P1_TC_149)
                .login(chAdmModifyChUsr);

        //Reset Users Category and Other Related information
        ChannelUserManagement.init(P1_TC_149)
                .initiateChannelUserModification(usr_259)
                .resetUserCategory(usr_259, parentCategory);

        /* TODO -  need to change the parent and owner if category changed*/
        modifyCategoryCode(usr_259, P1_TC_149);

        //Approve User Category Modification
        Login.init(P1_TC_149)
                .login(networkAdmin);
        ChannelUserManagement.init(P1_TC_149)
                .modifyUserApproval(usr_259);
    }

    /*
     * HELP METHODS
     * */

    /*
     *
     * Modify Category Code and related attributes of the user
     *
     * */

    private void modifyCategoryCode(User user, ExtentTest extentTest) throws Exception {
        try {
            AddChannelUser_pg2 page = AddChannelUser_pg2.init(extentTest);
            page.clickNextUserDetail();
            ChannelUserManagement.init(extentTest)
                    .assignHierarchy(user);

            CommonUserManagement.init(extentTest)
                    .assignWebGroupRole(user)
                    .mapWalletPreferences(user)
                    .mapBankPreferences(user, true);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, extentTest);
        }
    }
}
