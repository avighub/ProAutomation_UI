package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg4;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by shubham.kumar1 on 7/13/2017.
 */
public class Suite_P1_ChannelUserManagement_02 extends TestInit {

    private OperatorUser chAdmModifyChUsr, chAdmSuspendChUsr, chAdmModifyChUsrApp, chAdmAddChUsr, chAdmAddChUsrApp;
    private User user;
    private WebDriver driver;
    private User merchant_267;
    private User subs;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            chAdmModifyChUsr = DataFactory.getOperatorUserWithAccess("PTY_MCU", Constants.CHANNEL_ADMIN);
            chAdmModifyChUsrApp = DataFactory.getOperatorUserWithAccess("PTY_MCHAPP", Constants.CHANNEL_ADMIN);
            chAdmSuspendChUsr = DataFactory.getOperatorUserWithAccess("PTY_SCU", Constants.CHANNEL_ADMIN);
            chAdmAddChUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);
            chAdmAddChUsrApp = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2", Constants.CHANNEL_ADMIN);
            user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * Test             :             TC284
     * Description       :             To Verify that after successful creation of channel
     * user Each Channel User(Not of merchant domain) when
     * created will be assigned an Agent code. This code can
     * be used in transactions for single step Cash Out
     * date             :             13-07-2017
     * author           :             shruti.gupta
     *
     * @throws Exception
     */
    @Test(priority = 1, enabled = false)
    public void TC284() throws Exception {
        ExtentTest extentTest = pNode.createNode("TC284", "To Verify that after successful creation of channel user Each Channel User(Not of merchant domain) when created will be assigned an Agent code. This code can be used in transactions for single step Cash Out");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {
            ChannelUserManagement.init(extentTest).
                    createChannelUserDefaultMapping(user, false);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * Test         :      TC283
     * Description  :   Channel user Management >> Modify channel user>>
     * To verify that channel user registration assign Wallet page should open with all valid details.
     * date         :
     * author       :
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_101() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_101", "Channel user Management >> Modify channel user>>To verify that channel user registration assign Wallet page should open with all valid details.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            Login.init(extentTest).
                    login(chAdmModifyChUsr);

            ChannelUserManagement.init(extentTest)
                    .initiateChannelUserModification(user);

            CommonUserManagement.init(extentTest)
                    .navigateAndSelectWebGroupRole(user)
                    .verifyWalletPageDetails(user);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * Test         :            TC294
     * Description  :            Channel user Management >> Modify channel user>>
     * To verify that MFS provider drop down should available
     * on channel user registration assign Wallet page as a Mandatory field.
     * date         :
     * author       :
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_102() throws Exception {
        ExtentTest P1_TC_102 = pNode.createNode("P1_TC_102", "To verify that MFS provider drop down should available on channel user registration assign Wallet page as a Mandatory field.");
        P1_TC_102.assignCategory(FunctionalTag.P1, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            Login.init(P1_TC_102).
                    login(chAdmModifyChUsr);

            ChannelUserManagement.init(P1_TC_102)
                    .initiateChannelUserModification(user);
            CommonUserManagement.init(P1_TC_102)
                    .navigateAndSelectWebGroupRole(user)
                    .verifyProvidersDetails();
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_102);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * Test         :       TC292
     * Description  :       To verify that system proceeds to the Channel User
     * Registration-Assign Wallet page when at least one Group
     * Role is selected and clicked on Next Button.
     * Date         :       13-07-2017
     * author       :
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_100() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_100", "To verify that system proceeds to the Channel User Registration-Assign Wallet page when at least one Group Role is selected and clicked on Next Button.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            Login.init(extentTest)
                    .login(chAdmModifyChUsr);

            User chUsr = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(extentTest)
                    .initiateChannelUser(chUsr)
                    .assignHierarchy(chUsr)
                    .assignWebGroupRole(chUsr);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            boolean status = Utils.checkElementPresent("counterList[0].groupRoleSelected", Constants.FIND_ELEMENT_BY_NAME);
            //Boolean status = DriverFactory.getDriver().findElement(By.name("counterList[0].groupRoleSelected")).isDisplayed();

            Assertion.verifyEqual(status, true, "Wallet Assign Page Is Displayed", extentTest, true);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * Test         :           TC270
     * Description  :           Channel user Management >> Add channel user>>
     * To verify that channel user can register with
     * multiple wallets for a MFS provider.
     * Date         :
     * Author       :
     *
     * @throws Exception
     */
    @Test(priority = 5, enabled = false)
    public void TC270() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_372", "To verify that channel user can register with multiple wallets for a MFS provider.");
        extentTest.assignCategory(FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        user = new User(Constants.WHOLESALER);
        if (DataFactory.getWalletForChannelUser().size() < 1) {
            extentTest.warning("Atleast Need two Wallets for this test, please check the Application Configuration");
        } else {
            ChannelUserManagement.init(extentTest)
                    .createChannelUser(user);
        }
    }


    /**
     * Test         :           TC297_73
     * Description  :           To verify that without selecting default wallet
     * application fails to proceeds when modifying user if
     * Default wallet type not selected
     * date        :
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_318() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_318", "To verify that without selecting default wallet application fails to proceeds when modifying user if Default wallet type not selected");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            if (DataFactory.getWalletForChannelUser().size() < 1) {
                extentTest.warning("Need atleast one Wallets for this test, please check the Application Configuration");

            } else {
                Login.init(extentTest).
                        login(chAdmModifyChUsr);
                ChannelUserManagement.init(extentTest)
                        .initiateChannelUserModification(user);

                CommonUserManagement.init(extentTest)
                        .navigateAndSelectWebGroupRole(user);

                defaultWalletNegativeTest(user, extentTest);
                Assertion.verifyErrorMessageContain("channel.error.validate.noprimary.wallet",
                        "verified that application fails to proceeds when modifying user if Default wallet type not selected", extentTest);
            }
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * Test             :    TC267
     * Description      :    To verify that suspend channel user is not successful if
     * any pending transaction is present at Channel User end
     * Date             :    17-07-2017
     * Author           :Functionality Changed in 5.0, we can suspend user who is having pending transactions.
     * remark:          : Gurudatta Praharaj
     *
     * @throws Exception
     */
    @Test(enabled = false, priority = 9, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC267() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_105", " To verify that suspend channel user is not successful if any pending transaction is present at Channel User end");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        try {
            User c2cSender = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
            User c2cReceiver = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);

            Login.init(extentTest).login(c2cSender);

            TransactionManagement.init(extentTest)
                    .performC2C(c2cReceiver, "1", "12131");

            Login.init(extentTest).login(chAdmModifyChUsr);
            startNegativeTest();
            ChannelUserManagement.init(extentTest)
                    .initiateSuspendChannelUser(c2cReceiver);

            // TODO assertion, MON-3674 - able to suspend, test must fail
            Assertion.verifyErrorMessageContain("channel.error.pendingTransactionExist", "Verify That user cannot be suspedned", extentTest);
            extentTest.info("Already a Jira Bug is raised with id: MON-3674, also once this is fixed the actual error message may vary.");
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }


    /**
     * Test             :     TC299
     * Description      :     To verify that Account type drop down should available
     * on channel user registration assign Linked bank page
     * as a Mandatory field.
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_104() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_104", "To verify that Account type drop down should available on channel user registration assign Linked bank page as a Mandatory field.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            Login.init(extentTest)
                    .login(chAdmModifyChUsr);

            User chUsr = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(extentTest)
                    .initiateChannelUser(chUsr).assignHierarchy(chUsr)
                    .assignWebGroupRole(chUsr).mapWalletPreferences(chUsr);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            boolean status = Utils.checkElementPresent("bankCounterList[0].accountTypeSelected", Constants.FIND_ELEMENT_BY_NAME);
            //Boolean status = DriverFactory.getDriver().findElement(By.name("bankCounterList[0].accountTypeSelected")).isDisplayed();

            Assertion.verifyEqual(status, true, "Account Type Dropdown Is Displayed", extentTest, true);

        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * Test             :         TC300
     * Description      :          To verify that status drop down should available on
     * channel user registration assign Linked bank page as
     * a Mandatory field.
     * Date             :
     * Author           :
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_105() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_105", "To verify that status drop down should available on channel user registration assign Linked bank page as a Mandatory field.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {
            Login.init(extentTest)
                    .login(chAdmModifyChUsr);

            User chUsr = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(extentTest)
                    .initiateChannelUser(chUsr).assignHierarchy(chUsr)
                    .assignWebGroupRole(chUsr).mapWalletPreferences(chUsr);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            boolean status = Utils.checkElementPresent("bankCounterList[0].statusSelected", Constants.FIND_ELEMENT_BY_NAME);
            //Boolean status = DriverFactory.getDriver().findElement(By.name("bankCounterList[0].accountTypeSelected")).isDisplayed();

            Assertion.verifyEqual(status, true, "Status Dropdown Is Displayed", extentTest, true);

        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * Test             :           TC271_72
     * Description      :           Channel user Management >> Add channel user>>
     * To verify that channel user cannot register with
     * same wallet type for more than one times a MFS provider.
     * date             :
     * author           :
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_316() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_316", "To verify that channel user cannot register with same wallet type for more than one times a MFS provider.");
        t1.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(chAdmModifyChUsr);

            user = new User(Constants.WHOLESALER);

            mapWalletNegativeTest(user, t1);

            Assertion.verifyErrorMessageContain("cannot.have.multiple.samewallettype.per.mfs.provider",
                    "verified that System does not allow user to register with same wallet type for more than one times a MFS provider", t1);

            ExtentTest t2 = pNode.createNode("P1_TC_317", "To verify that only one wallet can be set as Primary wallet for each MFS provider.");
            t2.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

            Assertion.verifyErrorMessageContain("cannot.have.multiple.primaryaccount.per.mfs.provider",
                    "verified that System does not allow user to register with more than one wallet to be set as Primary wallet for each MFS provider", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /*
     * Test           :               TC258
     * Description    :               To verify that channel admin can reject initiated special super agent
     * date           :               18-07-2017
     * author         :               Shruti.gupta
     * */
    @Test(priority = 13, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_313() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_313", "To verify that channel admin can reject initiated special super agent");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        user = new User(Constants.SPECIAL_SUPER_AGENT);

        try {
            // Add Channel User
            Login.init(extentTest)
                    .login(chAdmAddChUsr);
            ChannelUserManagement.init(extentTest)
                    .addChannelUser(user);

            // Reject add Channel User
            Login.init(extentTest)
                    .login(chAdmAddChUsrApp);
            CommonUserManagement.init(extentTest)
                    .addInitiatedApproval(user, false);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * HELP METHODS
     */

    /**
     * For Negative Test of Wallets
     *
     * @param user
     * @throws Exception
     */
    public void mapWalletNegativeTest(User user, ExtentTest t1) throws Exception {

        Markup m = MarkupHelper.createLabel("mapDefaultWalletPreferences: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        driver = DriverFactory.getDriver();

        try {
            Login.init(pNode).login(chAdmAddChUsr);
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(user)
                    .assignHierarchy(user);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(user);


            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(pNode);

            Select selProviderUI = new Select(driver.findElement(By.name("counterList[0].providerSelected")));
            Select selPaymentTypeUI = new Select(driver.findElement(By.name("counterList[0].paymentTypeSelected")));
            Select selGrade = new Select(driver.findElement(By.name("counterList[0].channelGradeSelected")));

            selProviderUI.selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
            Thread.sleep(1200);
            selPaymentTypeUI.selectByVisibleText(DataFactory.getDefaultWallet().WalletName);
            Thread.sleep(1200);
            selGrade.selectByVisibleText(user.GradeName);
            Thread.sleep(2500);

            try {
                page4.clickAddMore(user.CategoryCode);
            } catch (Exception e) {
                if (e.getMessage().contains("element is not attached")) {
                    Thread.sleep(8000);
                    page4.clickAddMore(user.CategoryCode);
                }
            }

            Select selProviderUI1 = new Select(driver.findElement(By.name("counterList[1].providerSelected")));
            Select selPaymentTypeUI1 = new Select(driver.findElement(By.name("counterList[1].paymentTypeSelected")));
            Select selGrade1 = new Select(driver.findElement(By.name("counterList[1].channelGradeSelected")));

            selProviderUI1.selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
            Thread.sleep(1200);
            selPaymentTypeUI1.selectByVisibleText(DataFactory.getDefaultWallet().WalletName);
            Thread.sleep(1200);
            selGrade1.selectByVisibleText(user.GradeName);
            Thread.sleep(2500);


            page4.clickNext(user.CategoryCode);
        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
    }

    /**
     * @param user
     * @param extentTest
     * @throws Exception
     */
    public void defaultWalletNegativeTest(User user, ExtentTest extentTest) throws Exception {
        driver = DriverFactory.getDriver();
        Markup m = MarkupHelper.createLabel("defaultWalletNegativeTest: " + user.LoginId, ExtentColor.BLUE);
        extentTest.info(m); // Method Start Marker
        try {
            Select selPrimaryAccount = new Select(driver.findElement(By.name("counterList[0].primaryAccountSelected")));
            selPrimaryAccount.selectByIndex(1);
            //don't select any wallet as default wallet and click on nect button
            CommonChannelUserPage.init(extentTest).clickNextWalletMap();
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        }
    }

}

