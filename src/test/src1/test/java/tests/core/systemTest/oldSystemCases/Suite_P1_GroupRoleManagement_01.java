package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.SuperAdmin;
import framework.entity.WebGroupRole;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by shruti.gupta on 03-07-2017.
 * <p>
 * Modified by Navin to separate addGroup role and view Group Role
 */
public class Suite_P1_GroupRoleManagement_01 extends TestInit {
    private SuperAdmin saMaker;
    private WebGroupRole webGroupRole;
    private List<String> applicableRole;

    /**
     * SETUP
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        applicableRole = new ArrayList<>(Arrays.asList("SUBSADDAP", "SUBSADD"));
        webGroupRole = new WebGroupRole(Constants.WHOLESALER, "DummyTestRole" + DataFactory.getRandomNumber(3), applicableRole, 1);
        saMaker = DataFactory.getSuperAdminWithAccess("GRP_ROL");
    }

    /**
     * Test Case: TC003 -> Add Group Role
     * Description : To verify that valid user should be able to add WEB Group Role
     * with specific Roles in the system
     *
     * @throws Exception
     */
    @Test(groups = {"uap", "SMOKE"}, priority = 1)
    public void groupRoleAddTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC005", "Group Role Add To verify that valid user should be able to Add WEB Group Role.");

        t1.assignCategory(FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            //----Login SuperAdmin which can create Group Role- ---
            Login.init(t1).loginAsSuperAdmin(saMaker);

            GroupRoleManagement.init(t1)
                    .addWebGroupRole(webGroupRole);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * Test Case: TC004 -> Modify Group Role
     * Description : To verify that valid user should be able to modify existing WEB Group Role
     * <p>
     * This depends on method viewGroup role
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {"UAP"})
    public void groupRoleModifyTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC006", "Group Role Modify: To verify that valid user should be able to modify WEB Group Role.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GROUP_ROLE_MANAGEMENT);
        //----Login SuperAdmin----
        Login.init(t1).loginAsSuperAdmin(saMaker);

        //Modify Existing Group Role---
        webGroupRole.removeRole("SUBSADDAP");
        GroupRoleManagement.init(t1)
                .addWebGroupRole(webGroupRole);

    }


    /**
     * Test Case: TC005 -> Delete Group Role
     * Description : To verify that valid user should be able to delete existing WEB Group Role
     * <p>
     * This depends on method -->  groupRoleAddTest
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {"UAP"}, dependsOnMethods = "groupRoleAddTest")
    public void groupRoleDeleteTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC007", "Group Role Delete To verify that valid user should be able to delete existing WEB Group Role");

        t1.assignCategory(FunctionalTag.UAP, FunctionalTag.PVG_UAP, FunctionalTag.GROUP_ROLE_MANAGEMENT);

        //----Login SuperAdmin----
        Login.init(t1).loginAsSuperAdmin(saMaker);

        GroupRoleManagement.init(t1)
                .deleteWebGroupRoleDetails(webGroupRole);
    }

    /**
     * Test         : TC_01
     * Description  : Duplicate and verify an existing Group Role
     * Date         : 3rd July 2017
     * Author       :
     *
     * @throws Exception
     */
    @Test(priority = 6, enabled = true)
    public void duplicateGroupRole() throws Exception {
        WebGroupRole userRole = GlobalData.rnrDetails.get(0);
        // Create the Suite Node
        ExtentTest t1 = pNode.createNode("TC008", "Verify Duplicate Group Role")
                .assignCategory(FunctionalTag.REGRESSION, FunctionalTag.GROUP_ROLE_MANAGEMENT);
        WebGroupRole newRole = null;
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            newRole = GroupRoleManagement.init(t1)
                    .duplicateWebRole(userRole);
        } finally {
            GroupRoleManagement.init(t1)
                    .deleteWebGroupRoleDetails(newRole);
        }

    }
}
