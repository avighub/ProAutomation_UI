package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.channelEnquiry.ChannelEnquiry;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.channelEnquiry.ChannelEnquiry_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by surya.dhal on 12/5/2017.
 */
public class SuiteSystemChannelEnquiry extends TestInit {
    private String transID;
    private String fromDate;
    private String toDate;
    private OperatorUser payer, o2cApprover, channelEnqUser;
    private User payee;
    private Wallet wNormal, wCommission;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        try {
            payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            payer = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            channelEnqUser = DataFactory.getOperatorUserWithAccess("O2C_ENQ");
            wNormal = DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL);
            wCommission = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);

            preRequisite();

            fromDate = new DateAndTime().getDate(-5);
            toDate = new DateAndTime().getDate(0);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    private void preRequisite() throws Exception {
        o2cApprover = DataFactory.getOperatorUserWithAccess("O2C_APP1");

        ExtentTest t1 = pNode.createNode("Initiate O2C for Enquiry");
        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, payee, null, null, null, null);
        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        BigDecimal o2cAmount = MobiquityGUIQueries.getMaxTransferValue(sCharge.ServiceChargeName);
        o2cAmount = o2cAmount.subtract(BigDecimal.valueOf(1));

        Login.init(t1).login(payer);

        transID = TransactionManagement.init(t1).
                initiateO2C(payee, o2cAmount.toString(), DataFactory.getTimeStamp());

        if (transID != null) {
            Login.init(t1).login(o2cApprover);
            TransactionManagement.init(t1).o2cApproval1(transID);
        }

        t1.info("Transaction ID : " + transID);
    }

    /**
     * TEST : POSITIVE
     * DESC : Test Method to perform Chsannel Enquiry from all users of all grades
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES, FunctionalTag.MONEY_SMOKE}, priority = 1)
    public void SYS_TC_ChannelEnquiry_S_0001() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0001",
                "To verify that the" + DataFactory.getCategoryName(channelEnqUser.CategoryCode) + " can perform \"Owner to Channel\" Enquiry using transaction ID.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        try {
            Login.init(t1).login(channelEnqUser);
            ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingTxnID(transID);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : POSITIVE
     * DESC : Test method to do Channel Enquiry using Mobile number
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES, FunctionalTag.MONEY_SMOKE}, priority = 2)
    public void SYS_TC_ChannelEnquiry_S_0002() throws Exception {

        try {

            List<OperatorUser> o2cUsers = DataFactory.getOperatorUsersWithAccess("O2C_ENQ");

            for (OperatorUser enqUser : o2cUsers) {

                System.out.println("PAYER IS : " + DataFactory.getCategoryName(enqUser.CategoryCode));

                ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0002"
                        + getTestcaseid(), "To verify that the " + DataFactory.getCategoryName(enqUser.CategoryCode) + " can perform \"Owner to Channel\" Enquiry using Mobile number.\n");

                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

                Login.init(t1).login(enqUser);

                ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingMobileNumber(payee.MSISDN, transID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * TEST : POSITIVE
     * DESC : Test method to do Channel Enquiry using Date Range
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 3)
    public void SYS_TC_ChannelEnquiry_S_0004() throws Exception {

        try {

            List<OperatorUser> o2cUsers = DataFactory.getOperatorUsersWithAccess("O2C_ENQ");

            for (OperatorUser payer : o2cUsers) {

                System.out.println("PAYER IS : " + DataFactory.getCategoryName(payer.CategoryCode));

                ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0004" + getTestcaseid(), "To verify that the " + DataFactory.getCategoryName(payer.CategoryCode) + " can perform \"Owner to Channel\" Enquiry using Date Range.\n");

                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

                Login.init(t1).login(payer);

                ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingDateRange(transID, fromDate, toDate, Constants.TXN_STATUS_SUCCESS);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * TEST : POSITIVE
     * DESC : Test to check functionality of Back button
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0006() throws Exception {


        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0006" + getTestcaseid(), "To verify that \"Network Admin\" should be navigate to previous page when click on back button.");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry.init(t1).startNegativeTest().doO2CTransferEnquiryUsingTxnID(transID);
        ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(t1);
        page.clickOnBackButton();

        Boolean isTrue = Utils.checkElementPresent("check", Constants.FIND_ELEMENT_BY_NAME);
        if (isTrue) {
            t1.pass("Successfully Navigate to previous page.");
        } else {
            t1.fail("Failed to Navigate to previous page.");
        }

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test to do ChannelEnquiry when all mandatory fields are left blank
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0007() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0007" + getTestcaseid(), "To verify that \"Network Admin\" should not perform \"Owner to channel\" when mandatory fields are left blank.");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(t1);
        page.navChannelEnquiry();
        page.setTxnId(Constants.BLANK_CONSTANT);
        page.clickOnSubmit();
        Assertion.verifyErrorMessageContain("notif.error.allFields", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do Enquiry when all fields are selected
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0008() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0008" + getTestcaseid(), "To verify that \"Network Admin\" should not perform \"Owner to channel\" when all fields are selected.");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);
        ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingAllValue(payee.MSISDN, transID);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do Enquiry when Invalid transaction ID is selected
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0009() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0009" + getTestcaseid(),
                "To verify that \"Network Admin\" should not perform \"Owner to channel\" when \"Transaction ID\" is invalid.");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(t1);
        page.navChannelEnquiry();
        page.setTxnId(Constants.SPECIAL_CHARACTER_CONSTANT);
        page.clickOnSubmit();
        Assertion.verifyErrorMessageContain("btsl.error.msg.notxnId", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do Enquiry when wallet type is not selected
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0010() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0010" + getTestcaseid(),
                "To verify that \"Network Admin\" should not perform \"Owner to channel\" when \"Wallet Type\" is not selected.");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);
        ChannelEnquiry.init(t1).startNegativeTest().doO2CTransferEnquiryUsingMobileNumber(payee.MSISDN, transID, false);
        Assertion.verifyErrorMessageContain("userenq.walletOrBanks.error", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test to do Enquiry when Payment Instrument is not selected
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0011() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0011" + getTestcaseid(),
                "To verify that \"Network Admin\" should not perform \"Owner to channel\" when \"Payment Instrument\" is not selected.");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry.init(t1).doO2CTransferEnquiryWithoutPaymentID(payee.MSISDN, transID);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do Enquiry when Invalid MSISDN (Alphabets) is entered
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0012() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0012" + getTestcaseid(),
                "To verify that \"Network Admin\" should not perform \"Owner to channel\" when invalid \"MSISDN\" (Alphabets) is entered.\n");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry.init(t1).startNegativeTest().doO2CTransferEnquiryUsingMobileNumber(Constants.ALPHABET_CONSTANT, transID);

        Assertion.verifyErrorMessageContain("subs.error.msisdnnumeric", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do Enquiry when invalid MSISDN (Special character) is entered
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0013() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0013" + getTestcaseid(),
                "To verify that \"Network Admin\" should not perform \"Owner to channel\" when invalid \"MSISDN\" (Special Character) is entered.\n");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry.init(t1).startNegativeTest().doO2CTransferEnquiryUsingMobileNumber(Constants.SPECIAL_CHARACTER_CONSTANT, transID);

        Assertion.verifyErrorMessageContain("subs.error.msisdnnumeric", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do Enquirz of invalid length of "MSISDN".
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0014() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0014" + getTestcaseid(),
                "To verify that \"Network Admin\" should not perform \"Owner to channel\" when invalid length of \"MSISDN\" is entered.\n");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);
        ChannelEnquiry.init(t1).startNegativeTest().doO2CTransferEnquiryUsingMobileNumber("12345", transID);
        Assertion.verifyErrorMessageContain("report.emp.inValid.MsisdnLength", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do enquiry when "Status" is not selected.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0015() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0015",
                "To verify that \"Network Admin\" should not perform \"Owner to channel\" when \"Status\" is not selected.");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry.init(t1).startNegativeTest().doO2CTransferEnquiryUsingDateRange(transID, fromDate, toDate);

        Assertion.verifyErrorMessageContain("stock.enquiry.error.noStatus", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do Channel Enquiry When "From Date" is Future date.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 16)
    public void SYS_TC_ChannelEnquiry_S_0016() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0016",
                "To verify that \"Network Admin\" should not perform \"Owner to channel\" when \"From Date\" is Future date.");
        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        fromDate = new DateAndTime().getDate(5);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry.init(t1).startNegativeTest().doO2CTransferEnquiryUsingDateRange(transID, fromDate, toDate, Constants.TXN_STATUS_SUCCESS);

        Assertion.verifyErrorMessageContain("btsl.error.msg.fromdatebeforecurrentdate", "Verify Error Message", t1);


    }

    /**
     * TEST : NEGATIVE
     * DESC : Test to do Channel Enquiry when TO-Date is future date
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES}, priority = 4)
    public void SYS_TC_ChannelEnquiry_S_0017() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0017", "To verify that \"Network Admin\" should not perform \"Owner to channel\" when \"To Date\" is Future date.\n");

        payer = DataFactory.getOperatorUserWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_ENQUIRIES);

        toDate = new DateAndTime().getDate(5);

        Login.init(t1).login(channelEnqUser);

        ChannelEnquiry.init(t1).startNegativeTest().doO2CTransferEnquiryUsingDateRange(transID, fromDate, toDate, Constants.TXN_STATUS_SUCCESS);

        Assertion.verifyErrorMessageContain("btsl.error.msg.todatebeforecurrentdate", "Verify Error Message", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_ChannelEnquiry_S_0018
     * DESC : doing enquiry of "operator to channel transfer" Wallet Type should be displayed for both Normal and commission wallet " +
     * "once clickOnSubmitButton button is clicked and again on click of "Back" button in the dropdown of the wallet type.
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1})
    public void SYS_TC_ChannelEnquiry_S_0018() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_ChannelEnquiry_S_0018\n" + getTestcaseid(),
                "To verify that while doing enquiry of \"operator to channel transfer\" Wallet Type should be displayed for both Normal and commission wallet " +
                        "once clickOnSubmitButton button is clicked and again on click of \"Back\" button in the dropdown of the wallet type");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1);

        Login.init(t1).login(payer);
        startNegativeTest();
        ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingMobileNumber(payee.MSISDN, transID);
        ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(t1);
        page.clickOnBack();
        endNegativeTest();

        List<String> walletOptions = page.getWalletOptions();
        Assertion.verifyListContains(walletOptions, wNormal.WalletName, "Verify that Normal Wallet is available in the Wallet Dropdown", t1);
        Assertion.verifyListContains(walletOptions, wCommission.WalletName, "Verify that Commission Wallet is available in the Wallet Dropdown", t1);
    }

    private void endNegativeTest() {
        ConfigInput.isAssert = true;
    }

    /**
     * TEST : NEGATIVE TEST
     * ID : TC442
     * DESC : To Verify that valid user can do O2C Enquiry, Also verify that If Non mandatory " +
     * "fields are not selected and only Manadatory field is selected and  perform operation, application should not display error.
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1})
    public void TC442() throws Exception {
        ExtentTest t1 = pNode.createNode("TC442\n" + getTestcaseid(),
                "To Verify that valid user can do O2C Enquiry, Also verify that If Non mandatory " +
                        "fields are not selected and only Manadatory field is selected and  perform operation, application should not display error.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.PVG_P1);

        Login.init(t1).login(payer);
        ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingMobileNumber(payee.MSISDN, transID);
        if (!Assertion.checkErrorMessageContain("notif.error.allFields", "Verify When Non Mandatory Fields not entered", t1)) {
            t1.pass("No Error Present.");
        } else {
            t1.pass("Error Present.");
        }
    }

}
