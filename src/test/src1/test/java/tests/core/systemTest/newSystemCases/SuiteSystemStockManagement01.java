/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *  Author Name: Autoamtion team
 *  Date: 9/12/2017
 *  Purpose: System Testcases of Stock Management
 */


package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.stockManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by prashant.kumar on 11/16/2017.
 */
public class SuiteSystemStockManagement01 extends TestInit {

    private static final String AMOUNT_ALPHABET_CONSTANT = "abcd";
    private static String refNumber = "1235687";
    private static String providerName;
    private OperatorUser stockInitiator, stockApprover1, stockApprover2, stockLimiter, stockEAInitiator, stockEAApprover1, stockEAApprover2,
            stockEALimiter, stockIMTInitiator, stockIMTApprover1, stockIMTApprover2, stockIMTLimiter, stockReimbInitiator, stockReimbApprover, stockRA, stockRALimit, stockRA1, stockRA2;

    @BeforeMethod(alwaysRun = true)
    public void beforeMethodRun() {
        refNumber = DataFactory.getRandomNumberAsString(7);
    }

    @BeforeClass(alwaysRun = true)
    public void prerequist() throws Exception {
        try {
            providerName = DataFactory.getDefaultProvider().ProviderName;
            stockInitiator = DataFactory.getOperatorUserWithAccess("STOCK_INIT");
            stockApprover1 = DataFactory.getOperatorUserWithAccess("STOCK_APP1");
            stockApprover2 = DataFactory.getOperatorUserWithAccess("STOCK_APP2");
            stockLimiter = DataFactory.getOperatorUserWithAccess("STOCK_LIMIT");
            stockEAInitiator = DataFactory.getOperatorUserWithAccess("STR_INIT");
            stockEAApprover1 = DataFactory.getOperatorUserWithAccess("STOCKTR_APP1");
            stockEAApprover2 = DataFactory.getOperatorUserWithAccess("STOCKTR_APP2");
            stockEALimiter = DataFactory.getOperatorUserWithAccess("STOCKTR_LIMIT");
            stockIMTLimiter = DataFactory.getOperatorUserWithAccess("STK_IMT_INI");
            stockIMTInitiator = DataFactory.getOperatorUserWithAccess("STK_IMT_INI");
            stockIMTApprover1 = DataFactory.getOperatorUserWithAccess("STK_IMT_APPROVAL1");
            stockIMTApprover2 = DataFactory.getOperatorUserWithAccess("STK_IMT_APPROVAL2");
            stockReimbInitiator = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");
            stockReimbApprover = DataFactory.getOperatorUserWithAccess("STOCK_REMB");

            stockRALimit = DataFactory.getOperatorUserWithAccess("STK_LOYALTY_LMT");
            stockRA = DataFactory.getOperatorUserWithAccess("STK_LOYALTY_INI");
            stockRA1 = DataFactory.getOperatorUserWithAccess("STK_LOYALTY_APPR1");
            stockRA2 = DataFactory.getOperatorUserWithAccess("STK_LOYALTY_APPR2");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT,
            FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_051() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_051",
                "To verify that while initiating Stock Transfer to Reward Account" +
                        "the stock amount initiated is same as requested.")
                .assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0,
                        FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            StockManagement stockManagement = StockManagement.init(t1);
            Login.init(t1).login(stockRALimit);

            stockManagement.stockLimitRA(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);
            Login.init(t1).login(stockRA);

            String txnid = stockManagement.initiateRAStock(DataFactory.getDefaultProvider().ProviderName, refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT);

            Login.init(t1).login(stockRA1);
            stockManagement.approveRAStockL1(txnid, refNumber, Constants.REMARKS, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, true);

            Login.init(t1).login(stockRA2);
            stockManagement.approveRAStockL2(txnid, refNumber, Constants.REMARKS, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT,
            FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.MONEY_SMOKE})
    public void P1_TC_187() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_187",
                "To verify that When we initiate to withdraw the loyalty stock by the \"Stock withdrawal of Loyalty account\"  then it is done for the same amount(IND09) .");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            Login.init(t1).login(stockRA);

            StockManagement stockManagement = StockManagement.init(t1);
            Login.init(t1).login(stockRALimit);

            stockManagement.stockLimitRA(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);
            Login.init(t1).login(stockRA);

            String txnid = stockManagement.initiateRAStock(DataFactory.getDefaultProvider().ProviderName, refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT);

            Login.init(t1).login(stockRA1);
            stockManagement.approveRAStockL1(txnid, refNumber, Constants.REMARKS, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, true);

            Login.init(t1).login(stockRA2);
            stockManagement.approveRAStockL2(txnid, refNumber, Constants.REMARKS, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, true);

            stockManagement.loyaltyWithdrawal("10", Constants.REMARKS);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /*
     *  ##############################################################################
     *  ############ STOCK *P1 *SYSTEM * CASES * STARTS * FROM * HERE ################
     *  ##############################################################################
     */


    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_528() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_528",
                "To verify that Network Admin Approver user can see the Details of Requested Stock Transfer to EA account at level 1");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

        String txnId = stockManagement.initiateEAStock(refNumber, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, DataFactory.getDefaultProvider().ProviderName);

        Login.init(t1).login(stockEAApprover1);

        stockManagement.approveEAStockL1(txnId);

        DBAssertion.init(t1)
                .verifyPrePostBalanceForTransaction(txnId, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, preRecon);

    }


    /**
     * DESC :To verify that Network Admin can successfully approve the stock at level-1 and close the request at this level
     * when Transfer Amount is less than Approval limit 1.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_529() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_529",
                "To verify that Network Admin can successfully approve the stock at level-1 and close the request at this level " +
                        "when Transfer Amount is less than Approval limit 1.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockLimiter);

        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockEAInitiator);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

        //checkPreTransaction(GlobalData.defaultProvider.ProviderId + Constants.WALLET_101_101, GlobalData.defaultProvider.ProviderId + Constants.WALLET_103);

        String txnId = stockManagement.initiateEAStock(refNumber, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);

        stockManagement.approveEAStockL1(txnId, refNumber);

        if (Assertion.checkActionMessageContain("stock.approve.success", "Stock Approved", t1)) {
            t1.pass("Successfully Approved Network Stock Transfer");
            DBAssertion.init(t1)
                    .verifyPrePostBalanceForTransaction(txnId, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, preRecon);
        } else {
            t1.fail("Failed to Approve Network Stock Transfer");
        }
    }

    /**
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_530() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_530",
                "To verify that Net Admin can successfully approve the stock EA at level-1 and request will go for " +
                        "Second Approval when transfer Amount is more than Approval limit 1.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEALimiter);

        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockEAInitiator);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();
        //checkPreTransaction(GlobalData.defaultProvider.ProviderId + Constants.WALLET_101_101, GlobalData.defaultProvider.ProviderId + Constants.WALLET_103);

        String txnId = stockManagement.initiateEAStock(refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);

        stockManagement.approveEAStockL1(txnId, refNumber);

        ConfigInput.isAssert = true;

        Login.init(t1).login(stockEAApprover2);

        stockManagement.approveEAStockL2(txnId);

        DBAssertion.init(t1)
                .verifyPrePostBalanceForTransaction(txnId, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, preRecon);
    }

    /**
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_531() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_531", "To verify that Net Admin can Reject the Initiated EA Stock at level 1");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String tnxId = stockManagement.initiateEAStock(ref, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);

        stockManagement.rejectEAStockL1(tnxId);
    }


    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_0645() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0645",
                "To verify the details displayed when netAdmin clicks on the link Stock Management>>Stock Transfer to EA Approval 2");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEALimiter);

        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockEAInitiator);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

        String ref = DataFactory.getRandomNumberAsString(9);

        String tnxId = stockManagement.initiateEAStock(ref, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);

        stockManagement.approveEAStockL1(tnxId, ref);

        Login.init(t1).login(stockEAApprover2);

        stockManagement.approveEAStockL2(tnxId, ref);

        BigDecimal postRecon = MobiquityDBAssertionQueries.getReconBalance();

        DBAssertion.init(t1).verifyDBRecon(preRecon, postRecon);
    }


    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_533() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_533",
                "To verify that Network Admin can successfully approve the EA Stock at level-2 when Transfer amount is more than Approval Limit1.");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEALimiter);

        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();
        //checkPreTransaction(GlobalData.defaultProvider.ProviderId + Constants.WALLET_101_101, GlobalData.defaultProvider.ProviderId + Constants.WALLET_103);

        Login.init(t1).login(stockEAInitiator);

        String txnId = stockManagement.initiateEAStock(refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);

        stockManagement.approveEAStockL1(txnId, refNumber);

        ConfigInput.isAssert = true;

        Login.init(t1).login(stockEAApprover2);

        stockManagement.approveEAStockL2(txnId, refNumber);
        DBAssertion.init(t1)
                .verifyPrePostBalanceForTransaction(txnId, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, preRecon);

    }


    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_534() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_534",
                "To verify that Network Admin can Reject the Approved 1 EA Stock at Stock EA Approval Level 2");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEALimiter);

        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String tnxId = stockManagement.initiateEAStock(ref, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);

        stockManagement.approveEAStockL1(tnxId, ref);

        ConfigInput.isAssert = true;

        Login.init(t1).login(stockEAApprover2);

        stockManagement.rejectEAStockL2(tnxId);

    }


    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_535() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_535 : Stock Transfer Limit for EA",
                "To verify that Network Admin can set the Stock EA Limit for Level-1 Approval.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEALimiter);

        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);
    }


    //***********************************EA Ends***********************************//


    //**********************************************Stock initiate ***************************************//

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_519() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_519 ",
                "To verify that Network Admin cannot initiate the stock if requested quantity is negative value");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        try {
            StockManagement stockManagement = StockManagement.init(t1);

            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), "-" + Constants.STOCK_TRANSFER_LEVEL1_AMOUNT);
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Negative Initiate Network Stock", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void P1_TC_520() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_520",
                "To verify that Network  Admin can successfully approve the stock at level-1 and request will close at this level when requested quantity is less than Approval limit 1.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockLimiter);

        stockManagement
                .addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockInitiator);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

        String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDefaultBankNameForDefaultProvider(),
                Constants.STOCK_TRANSFER_LEVEL1_AMOUNT);

        startNegativeTest();
        stockManagement.approveNetworkStockL1(txnID);
        Assertion.verifyActionMessageContain("stock.approve.success", "Approve Network Stock", t1, txnID);

        DBAssertion.init(t1)
                .verifyPrePostBalanceForTransaction(txnID, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, preRecon);
    }


    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_521() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_521",
                " To verify that Network Admin can Reject the Initiated Stock at level 1");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockLimiter);

        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockInitiator);

        String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL1_AMOUNT);

        Login.init(t1).login(stockApprover1);

        stockManagement.rejectNetworkStockL1(txnID);

        Assertion.verifyActionMessageContain("stock.reject.success", "Negative Initiate Network Stock", t1);
    }

    /**
     * SYS_TC_0636
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_523() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_523",
                "To verify that Network Admin can Reject the Approved Stock from Level 1 at level 2");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockInitiator);

        String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL2_AMOUNT);
        startNegativeTest();
        stockManagement.approveNetworkStockL1(txnID);

        stockManagement.rejectNetworkStockL2(txnID);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void P1_TC_522() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_522:Stock Transfer Approve Level-2",
                "To verify that Network Admin can successfully approve the stock at level-2 ");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        StockManagement stockManagement = StockManagement.init(t1);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

        Login.init(t1).login(stockLimiter);
        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockInitiator);

        String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL2_AMOUNT);
        startNegativeTest();
        stockManagement.approveNetworkStockL1(txnID);
        ConfigInput.isAssert = true;
        stockManagement.approveNetworkStockL2(txnID);

        DBAssertion.init(t1)
                .verifyPrePostBalanceForTransaction(txnID, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, preRecon);

    }


    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_171() throws Exception {

        ExtentTest t1 = pNode.createNode(" P1_TC_171",
                "To verify that Network Admin can initiate the stock with requested quantity having decimal value.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

        String payerWallet = MobiquityDBAssertionQueries.getWalletNumberByUserID(MobiquityDBAssertionQueries.getBankID(DataFactory.getDefaultBankNameForDefaultProvider()), Constants.NORMAL_WALLET);

        Login.init(t1).login(stockInitiator);

        String tid = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFERINDECIMAL2);

        Login.init(t1).login(stockApprover1);

        stockManagement.approveNetworkStockL1(tid);

        DBAssertion.init(t1)
                .verifyPrePostBalanceForTransaction(tid, Constants.STOCK_TRANSFERINDECIMAL2, preRecon);
    }

    //----------------------------Stock initiate ends------------------------------------------//

    //----------------------------------- IMT starts---------------------------------------------//

    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_197() throws Exception {

        ExtentTest t1 = pNode.createNode(" P1_TC_197",
                "To verify that Network Admin can initiate the IMT with requested quantity having decimal value.");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

        String ref = DataFactory.getRandomNumberAsString(9);

        Login.init(t1).login(stockIMTInitiator);

        String transID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFERINDECIMAL2, Constants.REMARKS);

        DBAssertion.init(t1)
                .verifyPrePostBalanceForTransaction(transID, Constants.STOCK_TRANSFERINDECIMAL2, preRecon);
        stockManagement.rejectIMTStockLevel1(transID);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_196() throws Exception {

        ExtentTest t1 = pNode.createNode(" P1_TC_196",
                "To verify that Network Admin can initiate the IMT.");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        String imtTxnID = null;
        StockManagement stockManagement = StockManagement.init(t1);
        try {

            String ref = DataFactory.getRandomNumberAsString(9);

            BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

            Login.init(t1).login(stockIMTInitiator);

            imtTxnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

            BigDecimal postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(preRecon, postRecon, "Verify that Pre and Post Recon are Same!", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            if (imtTxnID != null) {
                Login.init(t1).login(stockIMTApprover1);
                //Added for the rejection of initiated Transaction
                startNegativeTest();
                stockManagement.rejectIMTStockLevel1(imtTxnID);
            }
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 16, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_198() throws Exception {

        ExtentTest t1 = pNode.createNode(" P1_TC_198",
                " To verify that Network  Admin can successfully approve the IMT at level-1 and request will " +
                        "close at this level when requested quantity is less than Approval limit 1.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        try {
            Login.init(t1).login(stockInitiator);
            stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

            String ref = DataFactory.getRandomNumberAsString(9);

            Login.init(t1).login(stockIMTInitiator);

            BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

            String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

            Login.init(t1).login(stockIMTApprover1);
            startNegativeTest();
            stockManagement.approveIMTStockL1(txnID);

            Assertion.checkActionMessageContain("stock.approve.success", "Approval 1", t1);

            BigDecimal postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(preRecon, postRecon, "Verify that Pre and Post Recon are Same!", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_199() throws Exception {

        ExtentTest t1 = pNode.createNode(" P1_TC_199:",
                " To verify that Network  Admin can successfully approve the IMT at level-1 and request will go for Second Approval when requested quantity is more than Approval limit 1.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        try {
            Login.init(t1).login(stockEALimiter);

            stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

            Login.init(t1).login(stockIMTInitiator);

            BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

            String txnID = stockManagement.initiateIMT(refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS);

            Login.init(t1).login(stockIMTApprover1);
            startNegativeTest();
            stockManagement.approveIMTStockL1(txnID);

            Login.init(t1).login(stockIMTApprover2);

            ConfigInput.isAssert = true;

            stockManagement.approveIMTStockL2(txnID);

            DBAssertion.init(t1)
                    .verifyPrePostBalanceForTransaction(txnID, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, preRecon);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test Method to Reject the Initiated IMT
     *
     * @throws Exception
     */
    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_200() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_200", "To verify that Network Admin can Reject the Initiated IMT at level 1");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        try {
            Login.init(t1).login(stockIMTInitiator);

            String txnID = stockManagement.initiateIMT(refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS);

            Login.init(t1).login(stockIMTApprover1);

            stockManagement.rejectIMTStockLevel1(txnID);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test Method to Reject the Approved 1 at level 2
     *
     * @throws Exception
     */
    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_202() throws Exception {

        ExtentTest t1 = pNode.createNode(" P1_TC_202", "To verify that Network Admin can Reject the Approved 1 IMT Stock at IMT Stock Level 2.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String txnID = stockManagement.initiateIMT(refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS);

        Login.init(t1).login(stockIMTApprover1);
        startNegativeTest();
        stockManagement.approveIMTStockL1(txnID);

        Login.init(t1).login(stockIMTApprover2);

        ConfigInput.isAssert = true;

        stockManagement.rejectIMTStockLevel2(txnID);
    }


    /*
     *  ##############################################################################
     *  ############ STOCK *SYSTEM * CASES * STARTS * FROM * HERE ################
     *  ##############################################################################
     */

    /**
     * SYS_TC_STOCK_P_0001 : Stock Initiate Negative Amount
     * DESC : To verify that the "Network Admin " user cannot
     * Initiate Stock Initiation when he enter requested amount value as "negative value" e.g. -100
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0001() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0001: Stock Initiate Negative Amount",
                "To verify that the \"Network Admin \" user cannot Initiate Stock Initiation when he enter requested amount value as \"negative value\" e.g. -100");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.NEGATIVE_AMOUNT_CONSTANT);

        if (!ConfigInput.is4o9Release) {
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Assert Negative stock initiation", t1);
        } else {
            Assertion.alertAssertion("negative.stock.transfer.invalidamount", t1);
        }
    }


    @Test(priority = 21, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0002() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0002 : Stock Initiation Blank Amount ",
                "To verify that the \"Network Admin \" user cannot Initiate Stock Initiation with blank requested amount.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.BLANK_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.blank.requestedQuantity", "Verify stock initiation with blank amount value", t1);
    }

    @Test(priority = 22, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0003() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0003",
                "To verify that the \"Network Admin \" user cannot Initiate Stock Initiation when he enter requested amount contains Special character.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.SPECIAL_CHARACTER_CONSTANT, refNumber);

        if (ConfigInput.isCoreRelease || ConfigInput.is4o14Release) {
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "verify amount as SPECIAL_CHARACTER", t1);

        } else {
            Assertion.alertAssertion("negative.stock.transfer.invalidamount", t1);
        }

    }


    /**
     * TEST : SYS_TC_STOCK_P_0004 : Stock Initiation Invalid Amount
     * DESC : To verify that the "Network Admin " user cannot Initiate Stock Initiation when user enter requested amount in words
     *
     * @throws Exception
     */
    @Test(priority = 23, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0004() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0004",
                "To verify that the \"Network Admin \" user cannot Initiate Stock Initiation when user enter requested amount in words");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.ALPHABET_CONSTANT, refNumber);

        if (ConfigInput.isCoreRelease || ConfigInput.is4o14Release) {
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Assert ALPHABET stock initiation", t1);
        } else {
            Assertion.alertAssertion("negative.stock.transfer.invalidamount", t1);
        }
    }


    /**
     * @throws Exception
     */
    @Test(priority = 24, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0005() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0005:Initiate Stock",
                "To verify that the \"Network Admin \" user cannot Initiate Stock Initiation with zero as requested amount");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), "0");

        if (ConfigInput.isCoreRelease || ConfigInput.is4o14Release) {
            Assertion.verifyErrorMessageContain("stock.error.requestedQuantity.greaterThanZero", "Assert stock initiation with zero", t1);
        } else {
            Assertion.alertAssertion("negative.stock.transfer.invalidamount", t1);
        }
    }


    @Test(priority = 25, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0006() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_STOCK_P_0006",
                "To verify that the \"Network Admin \" user can initiate stock initiation with value 1 as requested amount\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

        String tid = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), "1");

        Login.init(t1).login(stockApprover1);
        startNegativeTest();
        stockManagement.rejectNetworkStockL1(tid);

        //verifyPostTransactionDetails();
    }


    @Test(priority = 26, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_STOCK_P_0007() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0007",
                "To verify that the \"Network Admin \" user cannot Initiate Stock Initiation when Reference Number contains Special character  e.g. @#$");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.refNo", "Assert Error message ", t1);
    }

    @Test(priority = 28, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0008() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0008: Stock Management ",
                "To verify that the \"Network Admin \" user cannot Initiate Stock Initiation when Remarks contains Special character  e.g. @#$");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, "Automation", Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Assert Error message", t1);
    }


    /**
     * To verify that the "Network Admin " user cannot Approve Stock Level 1
     * when Remarks contains Special character e.g. @#$
     * This test case is failing (Need to check the functionality)
     * Or issue can be raised
     *
     * @throws Exception
     */
    @Test(priority = 29, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0009() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0009 ",
                "To verify that the \"Network Admin \" user cannot Approve Stock Level 1 when Remarks contains Special character e.g. @#$");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        String txnId = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL1_AMOUNT);

        Login.init(t1).login(stockApprover1);
        startNegativeTest();
        stockManagement.approveNetworkStockL1(txnId, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Assert Error message", t1);
    }


    @Test(priority = 30, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0010() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0010",
                "To verify that the \"Network Admin \" user cannot change Approved stock Value at level 1  in  Stock Approval level 1 Page\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        String txnId = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL1_AMOUNT);
        startNegativeTest();
        stockManagement.approveNetworkStockL1(txnId);
    }

    /**
     * Need to check the Remarks functionality
     * This case is failing bcoz remarks iis taking Special chars
     *
     * @throws Exception
     */
    @Test(priority = 31, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0011() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0011",
                "To verify that the \"Network Admin \" user cannot Approver Stock Level 2 when Remarks contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        String txnId = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL2_AMOUNT);

        Login.init(t1).login(stockApprover1);
        startNegativeTest();
        stockManagement.approveNetworkStockL1(txnId);

        Login.init(t1).login(stockApprover2);
        startNegativeTest();
        stockManagement.approveNetworkStockL2(txnId, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Assert Error message", t1);
    }


    @Test(priority = 32, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0012() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0012:Stock Management",
                "To verify that the \"Network Admin \" user cannot change Approved stock Value at level 2 in  Stock Approval level 2 page \n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        String txnId = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL2_AMOUNT);
        startNegativeTest();
        stockManagement.approveNetworkStockL1(txnId);

        stockManagement.approveNetworkStockL2(txnId);
    }


    @Test(priority = 33, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0013() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0013",
                "To verify that the  \"Back\" button functionality is working on  stock Approval level 1 page");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        String txnId = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_AMOUNT);

        Login.init(t1).login(stockApprover1);
        startNegativeTestWithoutConfirm();
        stockManagement.approveNetworkStockL1(txnId);

        StockApproval_Page1 page1 = new StockApproval_Page1(t1);

        page1.clickOnBackButton();

        boolean isTrue = Utils.checkElementPresent("stockApprove1_approve_button_submit", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(isTrue, true, "Verify Back Button", t1);

        if (isTrue) {
            t1.pass("Successfully Navigated to previous page");
        }
    }


    @Test(priority = 34, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0014() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0014: Stock Management ",
                "To verify that the  \"Back\" button functionality is working on  stock Approval level 2 page");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        String txnId = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL2_AMOUNT);
        startNegativeTest();
        stockManagement.approveNetworkStockL1(txnId);
        startNegativeTestWithoutConfirm();
        stockManagement.approveNetworkStockL2(txnId);

        StockApproval_Page2 page1 = new StockApproval_Page2(t1);

        page1.clickOnBackButton();

        boolean isTrue = Utils.checkElementPresent("stockApprove2_approve_button_submit", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(isTrue, true, "Verify Back Button", t1);

        if (isTrue) {
            t1.pass("Successfully Navigated to previous page");
        }
    }


    @Test(priority = 35, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0015() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0015",
                "  To verify that  \"Network Admin \" user can see the Details of Requested Stock Transfer at level 1 page");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        String txnId = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_AMOUNT, refNumber);

        Login.init(t1).login(stockApprover1);

        stockManagement.verifyNetworkStockApproveL1Page(txnId, refNumber);
    }

    @Test(priority = 36, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0016() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0016 : Stock Management ",
                "  To verify that  \"Network Admin \" user can see the Details of Requested Stock Transfer at  level 2 page");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        String ref = DataFactory.getRandomNumberAsString(5);

        String txnId = stockManagement.initiateNetworkStock(providerName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, ref);

        Login.init(t1).login(stockApprover1);

        startNegativeTest();

        stockManagement.approveNetworkStockL1(txnId);

        Login.init(t1).login(stockApprover2);

        stockManagement.verifyNetworkStockApproveL2Page(txnId, ref);
    }



    /*
     *  ######################################################################################
     *  ############ STOCK * TRANSFER *EA * SYSTEM * CASES * STARTS * FROM * HERE ############
     *  ######################################################################################
     */

    @Test(priority = 37, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0017() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0017",
                "To verify that the \"Network Admin \" user cannot Initiate EA Stock Initiation when he enter requested amount value as \"negative value\" e.g. -100");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateEAStock(ref, Constants.NEGATIVE_AMOUNT_CONSTANT, Constants.REMARKS, providerName);

        if (ConfigInput.isCoreRelease) {
            Assertion.alertAssertion("negative.stock.transfer.invalidamount", t1);
        } else {
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Assert EA stock initiation Error Messages", t1);
        }
    }

    @Test(priority = 38, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0018() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0018 :Stock EA ", "To verify that the \"Network Admin \" user cannot Initiate  EA Stock Initiation with blank requested amount ");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateEAStock(ref, Constants.BLANK_CONSTANT, Constants.REMARKS, providerName);

        Assertion.verifyErrorMessageContain("stockInitiation.blank.requestedQuantity", "Assert Blank EA stock initiation", t1);

    }


    @Test(priority = 39, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0019() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0019:", "To verify that the \"Network Admin \" user cannot Initiate EA Stock Initiation when he enter requested amount  contains Special character  e.g. @#$");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateEAStock(ref, "@#)*", Constants.REMARKS, providerName);

        if (ConfigInput.isCoreRelease) {
            Assertion.alertAssertion("negative.stock.transfer.invalidamount", t1);
        } else {
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Negative EA Initiate Network Stock", t1);
        }

    }


    @Test(priority = 40, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0020() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0020 ", "To verify that the \"Network Admin \" user cannot Initiate   EA Stock Initiation when user enter requested amount in words\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateEAStock(ref, AMOUNT_ALPHABET_CONSTANT, Constants.REMARKS, providerName);

        if (ConfigInput.isCoreRelease) {
            Assertion.alertAssertion("negative.stock.transfer.invalidamount", t1);
        } else
            Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Negative EA Initiate Network Stock", t1);
    }


    @Test(priority = 41, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0021() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0021 ", "To verify that the \"Network Admin \" user cannot Initiate   EA Stock Initiation with zero  e.g \"0\" requested amount\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateEAStock(ref, "0", Constants.REMARKS, providerName);

        Assertion.verifyErrorMessageContain("stock.error.requestedQuantity.greaterThanZero", "Negative EA Initiate Network Stock", t1);
    }


    @Test(priority = 42, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0022() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0022", "To verify that the \"Network Admin \" user can initiate   EA Stock initiation with value 1 as requested amount\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String tid = stockManagement.initiateEAStock(ref, "1", Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);
        //For the rejection of initiated EA Stock
        startNegativeTest();
        stockManagement.rejectEAStockL1(tid);

    }

    @Test(priority = 43, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0023() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0023 ",
                "To verify that the \"Network Admin \" user cannot Initiate EA Stock Initiation when Reference Number contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateEAStock(Constants.SPECIAL_CHARACTER_CONSTANT, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, providerName);

        Assertion.verifyErrorMessageContain("stockInitiation.special.refNo", "Negative EA Initiate Network Stock", t1);

    }


    @Test(priority = 44, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0024() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0024 : Stock EA ",
                "To verify that the \"Network Admin \" user cannot Initiate EA Stock Initiation when Remarks contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTest();
        stockManagement.initiateEAStock(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.SPECIAL_CHARACTER_CONSTANT, providerName);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Negative EA Initiate Network Stock", t1);

    }


    @Test(priority = 45, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT}, enabled = false)
    public void SYS_TC_STOCK_P_0025() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0025: Stock EA ", "To verify that the \"Network Admin \" user cannot Approve EA Stock Level 1 when Remarks contains Special character.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnId = stockManagement.initiateEAStock(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, providerName);
        startNegativeTest();
        stockManagement.approveEAStockL1(txnId, ref, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Negative EA Initiate Network Stock", t1);

    }

    @Test(priority = 46, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0026() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0026 : Stock Management ",
                "To verify that the Network Admin  user cannot change Approved EA Stock Value at level 1 in EA Stock Approval level 1 Page");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnId = stockManagement.initiateEAStock(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);
        startNegativeTest();
        stockManagement.approveEAStockL1(txnId);


    }

    @Test(priority = 47, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT}, enabled = false)
    public void SYS_TC_STOCK_P_0027() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0027  " + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot Approver   EA Stock Level 2 when Remarks  contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String txnId = stockManagement.initiateEAStock(refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);
        startNegativeTest();
        stockManagement.approveEAStockL1(txnId, refNumber);

        Login.init(t1).login(stockEAApprover2);

        stockManagement.approveEAStockL2(txnId, refNumber, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Assert error message", t1);

    }

    @Test(priority = 48, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0028() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0028 : Stock Management ",
                "To verify that the \"Network Admin \" user cannot change Approved EA Stock Value at level 2 in  EA Stock Approval level 2 page \n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(5);

        String txnId = stockManagement.initiateEAStock(ref, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);
        startNegativeTest();
        stockManagement.approveEAStockL1(txnId, ref);

        Login.init(t1).login(stockEAApprover2);

        stockManagement.approveEAStockL2(txnId);

    }

    @Test(priority = 49, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0029() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0029  " + getTestcaseid() + " : Stock Management ", "To verify that the  \"Back\" button functionality is working on  EA Stock Approval level 1 page\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String ref = DataFactory.getRandomNumberAsString(5);

        String txnId = stockManagement.initiateEAStock(ref, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);
        startNegativeTestWithoutConfirm();
        stockManagement.approveEAStockL1(txnId);

        new StockTransferToEAapproval1_page2(t1).backClick();

        boolean backPresent = Utils.checkElementPresent("stockTransferApprove1_approve_button_submit", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(backPresent, true, "Verify Back Button", t1);

        if (backPresent) {
            t1.pass("Successfully Navigated to previous page");
        }

    }

    @Test(priority = 50, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0030() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0030  " + getTestcaseid() + " : Stock Management ", "To verify that the  \"Back\" button functionality is working on  EA Stock Approval level 2 page\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);

        String txnId = stockManagement.initiateEAStock(refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);
        startNegativeTest();
        stockManagement.approveEAStockL1(txnId, refNumber);

        Login.init(t1).login(stockEAApprover2);
        startNegativeTestWithoutConfirm();
        stockManagement.approveEAStockL2(txnId);

        new StockTransferApprove(t1).backClick();

        boolean backPresent = Utils.checkElementPresent("stockTransferApprove2_approve_button_submit", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(backPresent, true, "Verify Back Button", t1);

        if (backPresent) {
            t1.pass("Successfully Navigated to previous page");
        }
    }


    /*
     *      ##############################################################################
     *      ############# STOCK * IMT * SYSTEM * CASES * STARTS * FROM * HERE ############
     *      ##############################################################################
     */


    /**
     * @throws Exception
     */
    @Test(priority = 51, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0031() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0031",
                "To verify that the \"Network Admin \" user cannot Initiate IMT Stock Initiation when he enter requested amount value as \"negative value\" e.g. -100\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTest();
        startNegativeTestWithoutConfirm();
        stockManagement.initiateIMT(ref, Constants.NEGATIVE_AMOUNT_CONSTANT, Constants.REMARKS);

        Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Assert error message", t1);
    }


    @Test(priority = 52, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0032() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0032 " + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot Initiate IMT Stock Initiation when he enter requested amount value as \"negative value\" e.g. -100\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTest();
        startNegativeTestWithoutConfirm();
        stockManagement.initiateIMT(ref, Constants.NEGATIVE_AMOUNT_CONSTANT, Constants.REMARKS);

        Assertion.verifyErrorMessageContain("invstock.error.improperAmount", "Assert Negative Amount IMT", t1);
    }


    @Test(priority = 53, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0033() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0033 " + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot Initiate   IMT Stock Initiation when he enter requested amount  contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateIMT(ref, Constants.SPECIAL_CHARACTER_CONSTANT, Constants.REMARKS);

        Assertion.verifyErrorMessageContain("stock.error.requestedQuantity.numericPositiveNumber", "Assert error message", t1);
    }

    @Test(priority = 54, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0034() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0034: Stock IMT ", "To verify that the \"Network Admin \" user cannot Initiate   IMT Stock Initiation when user enter requested amount in words\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateIMT(ref, Constants.ALPHABET_CONSTANT, Constants.REMARKS);

        String msgcode = "stock.error.requestedQuantity.numericPositiveNumber";

        Assertion.verifyErrorMessageContain(msgcode, "Assert error message", t1);
    }

    @Test(priority = 55, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0035() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0035 " + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot Initiate   IMT Stock Initiation with zero  e.g \"0\" requested amount\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateIMT(ref, "0", Constants.REMARKS);

        String msgcode = "stock.error.requestedQuantity.greaterThanZero";

        Assertion.verifyErrorMessageContain(msgcode, "Assert error message", t1);
    }


    @Test(priority = 56, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0036() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0036",
                "To verify that the \"Network Admin \" user can initiate IMT Stock initiation with value 1 as requested amount\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTLimiter);

        String ref = DataFactory.getRandomNumberAsString(9);

        stockManagement.initiateIMT(ref, "1", Constants.REMARKS);
    }


    @Test(priority = 57, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0037() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0037: Stock Management ", "To verify that the \"Network Admin \" user cannot Initiate   IMT Stock Initiation when Reference Number contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateIMT(Constants.SPECIAL_CHARACTER_CONSTANT, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

        Assertion.verifyErrorMessageContain("stockInitiation.special.refNo", "Assert Error message ", t1);
    }


    @Test(priority = 58, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT}, enabled = false)
    public void SYS_TC_STOCK_P_0038() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0038 : Stock Management ", "To verify that the \"Network Admin \" user cannot Initiate   IMT Stock Initiation when Remarks contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Assert Error message", t1);
    }


    @Test(priority = 59, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT}, enabled = false)
    public void SYS_TC_STOCK_P_0039() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0039 : Stock Management ",
                "To verify that the \"Network Admin \" cannot Approve IMT Stock Level 1 when Remarks field contains Special character\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

        Login.init(t1).login(stockIMTApprover1);
        startNegativeTest();
        stockManagement.approveIMTStockL1(txnID, ref, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Assert Error message", t1);
    }


    @Test(priority = 60, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0040() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0040 " + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot change Approved IMT Stock Value at level 1  in   IMT Stock Approval level 1 Page\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

        Login.init(t1).login(stockIMTApprover1);
        startNegativeTestWithoutConfirm();
        stockManagement.approveIMTStockL1(txnID, ref);

        Utils.checkElementIsEditable("//*[@id='stockIMTApprove1_approve__approvedStock']", t1);

    }

    @Test(priority = 61, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT}, enabled = false)
    public void SYS_TC_STOCK_P_0041() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0041 " + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot Approver   IMT Stock Level 2 when Remarks  contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS);
        startNegativeTest();
        stockManagement.approveIMTStockL1(txnID, ref);
        stockManagement.approveIMTStockL2(txnID, ref, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Assert Error message", t1);


    }


    @Test(priority = 62, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0042() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0042", "To verify that the \"Network Admin \" user cannot change Approved IMT Stock Value at level 2 in  IMT Stock Approval level 2 page \n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

        Login.init(t1).login(stockIMTApprover1);
        startNegativeTest();
        stockManagement.approveIMTStockL1(txnID, ref);

        Login.init(t1).login(stockIMTApprover2);
        startNegativeTestWithoutConfirm();
        stockManagement.approveIMTStockL2(txnID, ref);

        Utils.checkElementIsEditable("//*[@id='stockIMTApprove2_approve__approvedStock']", t1);

    }

    @Test(priority = 63, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0043() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0043" + getTestcaseid() + " : Stock Management ", "To verify that the  \"Back\" button functionality is working on  IMT Stock Approval level 1 page\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

        Login.init(t1).login(stockIMTApprover1);
        startNegativeTestWithoutConfirm();
        stockManagement.approveIMTStockL1(txnID, ref);

        new IMTapproval1_page1(t1).backButtonClick();

        boolean isElementFound = Utils.checkElementPresent("stockIMTApprove1_approve_button_submit", Constants.FIND_ELEMENT_BY_ID);

        if (Assertion.verifyEqual(isElementFound, true, "Checking Element Present", t1)) {
            t1.pass("Successfully navigated to previous page...");
        }

    }

    @Test(priority = 64, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0044() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0044 : Stock IMT ", "To verify that the  \"Back\" button functionality is working on  IMT Stock Approval level 2 page\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

        Login.init(t1).login(stockIMTApprover1);
        startNegativeTest();
        stockManagement.approveIMTStockL1(txnID);

        Login.init(t1).login(stockIMTApprover2);
        startNegativeTestWithoutConfirm();
        stockManagement.approveIMTStockL2(txnID, ref);

        new IMTapproval2_page1(t1).backButtonClick();

        Utils.checkElementPresent("stockIMTApprove2_approve_button_submit", Constants.FIND_ELEMENT_BY_ID);

    }


    @Test(priority = 65, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0045() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0045 :Stock IMT ", "  To verify that  \"Network Admin \" user can see the Details of Requested IMT Stock Transfer at  level 1 page\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS);

        Login.init(t1).login(stockIMTApprover1);
        startNegativeTest();
        stockManagement.approveIMTStockL1(txnID, ref);

    }


    @Test(priority = 66, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0046() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0046" + getTestcaseid() + " : Stock Management ", "  To verify that  \"Network Admin \" user can see the Details of Requested IMT Stock Transfer at  level 2 page\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockIMTInitiator);

        String ref = DataFactory.getRandomNumberAsString(9);

        String txnID = stockManagement.initiateIMT(ref, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS);

        Login.init(t1).login(stockIMTApprover1);
        startNegativeTest();
        stockManagement.approveIMTStockL1(txnID);

        Login.init(t1).login(stockIMTApprover2);
        startNegativeTest();
        stockManagement.approveIMTStockL2(txnID, ref);

    }

    /*
     *      ##############################################################################
     *      ######### STOCK * EA* LIMIT SET *SYSTEM * CASES * STARTS * FROM * HERE #######
     *      ##############################################################################
     */

    @Test(priority = 67, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0052() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0052" + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot set EA Stock Limit if  amount value as \"negative value\" e.g. -100\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEALimiter);
        startNegativeTest();
        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.NEGATIVE_AMOUNT_CONSTANT);

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert EA limit Error", t1);

    }

    @Test(priority = 68, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0053() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0053", "To verify that the \"Network Admin \" user cannot set EA Stock Limit with blank Limit amount \n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);
        Login.init(t1).login(stockEALimiter);
        startNegativeTest();
        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.BLANK_CONSTANT);

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert EA limit Error", t1);

    }


    @Test(priority = 69, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0054() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0054", "To verify that the \"Network Admin \" user cannot set EA Stock Limit  to zero  0\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);
        Login.init(t1).login(stockEALimiter);
        startNegativeTest();
        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, "0");

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert EA limit Error", t1);

    }


    @Test(priority = 70, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0055() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0054", "To verify that the \"Network Admin \" user cannot set EA Stock Limit if   amount  contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);
        Login.init(t1).login(stockEALimiter);
        startNegativeTest();
        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert EA limit Error", t1);

    }


    @Test(priority = 71, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0056() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0054", "To verify that the \"Network Admin \" user cannot set EA Stock Limit if amount is in Words e.g. abcd ,def etc\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);
        Login.init(t1).login(stockEALimiter);
        startNegativeTest();
        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.ALPHABET_CONSTANT);

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert EA limit Error", t1);

    }


    /*
     *  ##############################################################################
     *  ######### STOCK *LIMIT * SET *SYSTEM * CASES * STARTS * FROM * HERE ##########
     *  ##############################################################################
     */


    @Test(priority = 72, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0057() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0057", "To verify that the \"Network Admin \" user cannot set EA Stock Limit if  amount value as \"negative value\" e.g. -100\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);
        Login.init(t1).login(stockEALimiter);
        startNegativeTest();
        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.NEGATIVE_AMOUNT_CONSTANT);

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert  limit Error", t1);

    }

    @Test(priority = 73, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0058() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0058", "To verify that the \"Network Admin \" user cannot set  Stock Limit with blank Limit amount \n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);
        Login.init(t1).login(stockEALimiter);
        startNegativeTest();
        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.BLANK_CONSTANT);

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert  limit Error", t1);

    }


    @Test(priority = 74, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0059() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0059", "To verify that the \"Network Admin \" user cannot set  Stock Limit  to zero  0\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockLimiter);
        startNegativeTest();
        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, "0");

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert  limit Error", t1);

    }


    @Test(priority = 75, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0060() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0060 ",
                "To verify that the \"Network Admin \" user cannot set  Stock Limit if   amount  contains Special character  e.g. @#$\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);
        Login.init(t1).login(stockLimiter);
        startNegativeTest();
        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert  limit Error", t1);

    }


    @Test(priority = 76, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0061() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0061 ", "To verify that the \"Network Admin \" user cannot set  Stock Limit if amount is in Words e.g. abcd ,def etc\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockLimiter);
        startNegativeTest();
        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.ALPHABET_CONSTANT);

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Assert  limit Error", t1);
    }


    @Test(priority = 77, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0062() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0062 : Stock EA", "To verify that the 'Network Admin ' user cannot Initiate Stock Initiation with out Entering Reference number");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);
        startNegativeTest();
        stockManagement.initiateEAStock(Constants.BLANK_CONSTANT, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

        Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "Assert  Blank reference Stock Initiate", t1);
    }


    @Test(priority = 78, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0063() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0063",
                "To verify that 'Network Admin' user cannot initiate IMT Stock Initiation without entering Reference number");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);
        Login.init(t1).login(stockEALimiter);
        startNegativeTestWithoutConfirm();
        stockManagement.
                initiateIMT(Constants.BLANK_CONSTANT, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

        if (ConfigInput.isCoreRelease) {
            try {
                if (DriverFactory.getDriver().findElement(By.id("stockInitIMT_confirmInitiate__refNo")).getAttribute("required") != null) {
                    t1.pass("Not able to initiate IMT");
                }
            } catch (Exception e) {
                t1.info("Required field is not present");
            }
        } else {
            Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "Verify Blank reference Stock Initiate", t1);
        }


    }


    @Test(priority = 79, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0064() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0064", "To verify that the 'Network Admin' user cannot Initiate EA Stock Initiation with out Entering Reference number");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEAInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateEAStock(Constants.BLANK_CONSTANT, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, providerName);

        Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "Assert  Blank reference Stock Initiate", t1);
    }






    /*
     *  ##############################################################################
     *  ######### STOCK * REIMBURSEMENT *SYSTEM * CASES * STARTS * FROM * HERE #######
     *  ##############################################################################
     */

    @Test(priority = 66, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void reimbPositive() throws Exception {

        OperatorUser opt = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");

        Map<String, User> users = DataFactory.getChannelUserSetUniqueGrade();

        for (User u : users.values()) {
            ExtentTest t2 = pNode.createNode("SYS_TC_STOCK_P_0066 : REIMBURSEMENT POSITIVE" + getTestcaseid(),
                    "To verify that the \"Network Admin \" can perform  Reimbursement for " + u.CategoryName + " with Grade " + u.GradeName);

            try {
                t2.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

                ServiceCharge serviceCharge = new ServiceCharge(Services.OPERATOR_WITHDRAW, u, opt, null, null, null, null);
                TransferRuleManagement.init(t2).configureTransferRule(serviceCharge);

                Login.resetLoginStatus();

                Login.init(t2).login(stockReimbInitiator);

                StockManagement stockManagement = StockManagement.init(t2);

                DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(u, null, null);
                DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getWalletBalanceByWalletNumber(Constants.WALLET_101_101);
                DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

                String refNo = DataFactory.getRandomNumberAsString(7);

                TransactionManagement.init(t2).makeSureChannelUserHasBalance(u, new BigDecimal(Constants.REIMBURSEMENT_AMOUNT));

                String txnID = stockManagement.
                        initiateStockReimbursement(u, refNo, Constants.REIMBURSEMENT_AMOUNT, Constants.REMARKS, null);

                if (txnID != null) {
                    Login.init(t2).login(stockReimbApprover);
                    stockManagement.approveReimbursement(txnID);

                    DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(u, null, null);
                    DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getWalletBalanceByWalletNumber(Constants.WALLET_101_101);
                    DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

                    DBAssertion.verifyUserAndReconBalance(DBAssertion.postPayerBal, DBAssertion.prePayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t2);
                }

            } catch (Exception e) {
                Assertion.raiseExceptionAndContinue(e, t2);
            }
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 77, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0077() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0077", "To verify that the \"Network Admin \" user cannot perform Reimbursement with blank MSISDN");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        new Reimbursement_page1(t1).navigateToLink().type_SelectValue("CHANNEL").clickOnProvider();

        Assertion.alertAssertion("ci.error.msisdnvalue", t1);
    }

    @Test(priority = 78, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0078() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0078",
                "To verify that the \"Network Admin \" user cannot perform Reimbursement with invalid MSISDN");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        new Reimbursement_page1(t1).navigateToLink().type_SelectValue("CHANNEL").msisdn_SetText(DataFactory.getAvailableMSISDN()).
                clickOnProvider();

        if (ConfigInput.isCoreRelease) {
            Assertion.alertAssertion("error.msisdnnotexist", t1);
        }

    }

    @Test(priority = 79, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0079() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0079", "To verify that the \"Network Admin \" user cannot perform Reimbursement  when MSISDN contains Special character  e.g. @#$");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        new Reimbursement_page1(t1).navigateToLink().type_SelectValue("CHANNEL").msisdn_SetText(Constants.SPECIAL_CHARACTER_CONSTANT).clickOnProvider();

        Assertion.alertAssertionDynamic("ci.error.onlynum", "" + DataFactory.getAvailableMSISDN().length(), t1);
    }

    @Test(priority = 83, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0081() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0081  " + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot perform Reimbursement  when MSISDN contains words e.g. abc,def etc");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        new Reimbursement_page1(t1).navigateToLink().type_SelectValue("CHANNEL").msisdn_SetText(Constants.ALPHABET_CONSTANT).clickOnProvider();

        Assertion.alertAssertion("ci.error.onlynum", t1);
    }

    @Test(priority = 84, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0082() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0082", "To verify that the \"Network Admin \" user cannot perform Reimbursement without selecting user type in reimbursement page 1\n");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);
        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getAnyChannelUser();

        new Reimbursement_page1(t1).navigateToLink().msisdn_SetText(u.MSISDN).clickOnProvider();

        Assertion.alertAssertion("error.msisdnnotexist", t1);
    }


    @Test(priority = 85, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0083() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0083 " + getTestcaseid() + " : Stock Management ", "To verify that the Network Admin user cannot perform Reimbursement when user type is selected as Subscriber and entered MSISDN of wholesaler/retailor");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getAnyChannelUser();

        new Reimbursement_page1(t1).navigateToLink().type_SelectValue("SUBSCRIBER").msisdn_SetText(u.MSISDN).clickOnProvider();

        Assertion.alertAssertion("error.msisdnnotexist", t1);
    }

    @Test(priority = 86, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0084() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0084", "To verify that the Network Admin user cannot perform Reimbursement when user type is selected as Channel and entered MSISDN of Subscriber ");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        new Reimbursement_page1(t1).navigateToLink().type_SelectValue("CHANNEL").msisdn_SetText(u.MSISDN).clickOnProvider();

        Assertion.alertAssertion("error.msisdnnotexist", t1);
    }

    @Test(priority = 87, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0085() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0085",
                "To verify that the \"Network Admin \" user cannot perform Reimbursement when Reference Number is blank\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        StockManagement stockManagement = StockManagement.init(t1);
        startNegativeTestWithoutConfirm();
        stockManagement.
                initiateStockReimbursement(u, Constants.BLANK_CONSTANT, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, null);

        Assertion.verifyErrorMessageContain("o2c.initiate.error.referenceNumber", "Assert Error Message", t1);
    }


    @Test(priority = 88, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0086() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0086 " + getTestcaseid() + " : Stock Management ", "To verify that the \"Network Admin \" user cannot perform Reimbursement when Reference Number contains Special character  e.g. @#$");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        StockManagement stockManagement = StockManagement.init(t1);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateStockReimbursement(u, Constants.SPECIAL_CHARACTER_CONSTANT, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, null);

        Assertion.verifyErrorMessageContain("o2c.initiate.error.referenceNumber.Numeric", "Assert Error Message", t1);
    }

    @Test(priority = 89, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0087() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0087", "To verify that the \"Network Admin \" user cannot perform Reimbursement when Remarks contains Special character  e.g. @#$");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        StockManagement stockManagement = StockManagement.init(t1);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateStockReimbursement(u, refNumber, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.SPECIAL_CHARACTER_CONSTANT, null);

        Assertion.verifyErrorMessageContain("stock.reimbursment.remarks", "Assert Error Message", t1);
    }

    @Test(priority = 90, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0088() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0088 " + getTestcaseid() + " : Stock Management ",
                "To verify that the \"Network Admin \" user cannot perform Reimbursement when Reference Number contains alphabets.");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);


        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        StockManagement stockManagement = StockManagement.init(t1);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateStockReimbursement(u, Constants.ALPHABET_CONSTANT, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, null);

        Assertion.verifyErrorMessageContain("o2c.initiate.error.referenceNumber.Numeric", "Assert Error Message", t1);
    }

    /**
     * TEST ID : SYS_TC_STOCK_P_0089
     * DESC : To verify that the "Back" button functionality is working on Reimbursement confirmation page 1
     *
     * @throws Exception
     */
    @Test(priority = 91, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0089() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0089 ",
                "To verify that the \"Back\" button functionality is working on Reimbursement confirmation page 1");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getAnyChannelUser();

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockReimbInitiator);

        String refNo = DataFactory.getRandomNumberAsString(9);
        startNegativeTestWithoutConfirm();
        stockManagement.
                initiateStockReimbursement(u, refNo, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, null);

        new Reimbursement_page2(t1).back_Click();

        boolean elePresent = Utils.checkElementPresent("withdraw_confirmDisplay_button_submit", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(elePresent, true, "Verify Back Button", t1);
    }


    /**
     * SYS_TC_STOCK_P_0091
     * To verify that the "Back" button functionality is working on  reimbursement confirmation page 2
     *
     * @throws Exception
     */
    @Test(priority = 92, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0091() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0091",
                "To verify that the \"Back\" button functionality is working on  reimbursement confirmation page 2");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        User u = DataFactory.getAnyChannelUser();

        StockManagement stockManagement = StockManagement.init(t1);

        String refNo = DataFactory.getRandomNumberAsString(9);

        Login.init(t1).login(stockReimbInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateStockReimbursement(u, refNo, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, null);

        new Reimbursement_page2(t1).amount_SetText(Constants.REIMBURSEMENT_AMOUNT).confirm_Click();

        new Reimbursement_page3(t1).back_Click();

        boolean isPresent = Utils.checkElementPresent("withdraw_back2_butn", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(isPresent, true, "Verify Back Button", t1);
    }

    @Test(priority = 93, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0095() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0095" + getTestcaseid() + " : Stock Management ",
                "To verify that the \"Network Admin \" user cannot  perform Reimbursement when user enter amount in Negative\n");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);
        User u = DataFactory.getAnyChannelUser();

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockReimbInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateStockReimbursement(u, refNumber, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, null);

        new Reimbursement_page2(t1).amount_SetText(Constants.NEGATIVE_AMOUNT_CONSTANT).confirm_Click();
        new Reimbursement_page3(t1).confirm_Click();
        Assertion.verifyErrorMessageContain("opt.withdraw.error.amountVal", "Verify invalid amount", t1);
    }

    @Test(priority = 94, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0096() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0096",
                "To verify that the \"Network Admin \" user cannot  perform Reimbursement when user enter " +
                        "amount contains Special characters.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(stockReimbInitiator);

        User u = DataFactory.getAnyChannelUser();

        StockManagement stockManagement = StockManagement.init(t1);

        String refNo = DataFactory.getRandomNumberAsString(9);

        Login.init(t1).login(stockReimbInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateStockReimbursement(u, refNo, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, null);

        new Reimbursement_page2(t1).amount_SetText(Constants.SPECIAL_CHARACTER_CONSTANT).confirm_Click();

        new Reimbursement_page3(t1).confirm_Click();

        Assertion.verifyErrorMessageContain("opt.withdraw.error.amountVal", "Assert Error Message", t1);

    }

    @Test(priority = 95, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0097() throws Exception {

        ExtentTest t1 = pNode.createNode(" SYS_TC_STOCK_P_0097" + getTestcaseid() + " : Stock Management ",
                "To verify that the \"Network Admin \" user cannot  perform Reimbursement when user enter amount is blank");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);
        User u = DataFactory.getAnyChannelUser();

        StockManagement stockManagement = StockManagement.init(t1);

        String refNo = DataFactory.getRandomNumberAsString(9);

        Login.init(t1).login(stockReimbInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateStockReimbursement(u, refNo, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS, null);

        new Reimbursement_page2(t1).amount_SetText(Constants.BLANK_CONSTANT).confirm_Click();

        new Reimbursement_page3(t1).confirm_Click();

        Assertion.verifyErrorMessageContain("opt.withdraw.error.amountVal", "Verify Invalid amount Message", t1);
    }

    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_527() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_527",
                "To verify that network admin can reject any stock Reimbursement initiation request");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        String refNo = DataFactory.getRandomNumberAsString(9);

        Login.init(t1).login(stockReimbInitiator);

        String txnId = stockManagement.initiateStockReimbursement(Constants.OPERATOR_REIMB, refNo, Constants.REIMBURSEMENT_AMOUNT, Constants.REMARKS, null);

        Login.init(t1).login(stockReimbApprover);

        stockManagement.rejectReimbursement(txnId);
    }

    /**
     * TEST : Negative
     * ID : SYS_TC_STOCK_P_0104
     * DESCRIPTION : Stock Management >> Stock Initiation.To verify that Network Admin cannot initiate the stock if all fields are left blank.
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void SYS_TC_STOCK_P_0104() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_STOCK_P_0104",
                "Stock Management >> To verify that Network Admin cannot initiate the stock if all fields are left blank.");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        stockManagement.initiateNetworkStock(Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT);
        Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "Verify Error Message when All fields left blank.", t1);

    }

    /**
     * TEST : Negative
     * ID : SYS_TC_STOCK_P_0105
     * DESCRIPTION : Verify Links after click on Stock Management>>Stock Transfer to EA approval 2
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0105() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_STOCK_P_0105",
                "\"Stock Management >>Stock Transfer to EA Approval 2                \n" +
                        "To verify the details displayed when netadmin clicks on the link Stock Management>>Stock Transfer to EA   approval 2\n" +
                        "4)Transfer Amount\n" +
                        "5)Transaction Id\n" +
                        "6)Current Status\"\n");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockInitiator);

        stockManagement.verifyEAStockTransferApproval2TableHeaders();
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT})
    public void SYS_TC_STOCK_P_0106() throws Exception {
        ExtentTest t1 = pNode.createNode("TC655", "To verify that Stock Initation is done in IND08 while Stock withdrawal is done from account IND09.");
        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockLimiter);
        stockManagement.addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);
        Login.init(t1).login(stockInitiator);
        String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultBankNameForDefaultProvider(), Constants.STOCK_TRANSFER_LEVEL1_AMOUNT);
        Login.init(t1).login(stockApprover1);
        stockManagement.approveNetworkStockL1(txnID);
        Login.init(t1).login(stockApprover2);
        stockManagement.approveNetworkStockL2(txnID);
    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_044() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_044",
                "To verify that Network Admin approver user can see the Details of Requested Stock Transfer to EA account at level 2");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        StockManagement stockManagement = StockManagement.init(t1);

        Login.init(t1).login(stockEALimiter);

        stockManagement.addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

        Login.init(t1).login(stockEAInitiator);

        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();


        String txnId = stockManagement.initiateEAStock(refNumber, Constants.STOCK_TRANSFER_LEVEL2_AMOUNT, Constants.REMARKS, providerName);

        Login.init(t1).login(stockEAApprover1);

        stockManagement.approveEAStockL1(txnId, refNumber, Constants.REMARKS);

        ConfigInput.isAssert = true;

        Login.init(t1).login(stockEAApprover2);

        try {
            Markup m = MarkupHelper.createLabel("approveEAStockL2", ExtentColor.BLUE);
            t1.info(m); // Method Start Marker
            StockTransferToEaApproval2_page1 page1 = new StockTransferToEaApproval2_page1(t1);
            StockTransferToEaApproval2_Page2 page2 = new StockTransferToEaApproval2_Page2(t1);


            page1.navigateToEAStockApprovalLevel2();
            page1.selectTransactionID(txnId);
            page1.clickSubmit();


            String actual = page2.getRequester();
            Assertion.verifyEqual(actual, stockEAInitiator.FirstName, "Assert Requester", t1, true);


            if (ConfigInput.isConfirm)
                page2.clickOnSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.approve.success", "Approve EA Stock Level 2", t1, txnId);
                DBAssertion.verifyDBAssertionEqual(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                        Constants.TXN_STATUS_SUCCESS, "Verify DB Status Success", t1);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

}
