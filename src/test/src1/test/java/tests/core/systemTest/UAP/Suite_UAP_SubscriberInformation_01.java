package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created on 10-07-2017.
 */
public class Suite_UAP_SubscriberInformation_01 extends TestInit {
    private User subsInfoViewer;
    private User subscriber;
    private String provider;
    private String paymentInst;
    private String walletType;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        provider = DataFactory.getDefaultProvider().ProviderId;
        paymentInst = "WALLET";
        walletType = DataFactory.getDefaultWallet().WalletName;

    }

    /**
     * Test: TC197
     * Description: To verify that  valid user can view subscriber information
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.DEPENDENCY_TAG}, priority = 1)
    public void TC197() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0169 : View Subscriber Information", "To verify that the valid user can view Subscriber Information.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_INFORMATION);

        subsInfoViewer = DataFactory.getChannelUserWithAccess("SUBS_INF");

        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        Login.init(t1).
                login(subsInfoViewer);

        SubscriberManagement.init(t1).doSubsInformation(provider, paymentInst, walletType, subscriber);
    }
}
