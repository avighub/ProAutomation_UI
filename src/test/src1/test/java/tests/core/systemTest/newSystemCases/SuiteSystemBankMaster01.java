/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 22-Dec-2017
 *  Purpose: Bank Related Cases (For System Test)
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.bankMaster.AddServiceProviderBankAccounts1;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Button;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;
import java.util.List;


public class SuiteSystemBankMaster01 extends TestInit {

    private SuperAdmin saAddBank, saAddBankAccount, saSysPref;
    private Bank bank, tempBank;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("SETUP", "Setup for Bank Master.");
        try {
            saAddBank = DataFactory.getSuperAdminWithAccess("BANK_ADD");
            saAddBankAccount = DataFactory.getSuperAdminWithAccess("OPT_BANK_ACC_ADD");

            bank = new Bank(defaultProvider.ProviderId,
                    "AUTO" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    true,
                    false
            );

            tempBank = new Bank(defaultProvider.ProviderId,
                    "TEMP" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    true,
                    false
            );

        } catch (Exception e) {
            markSetupAsFailure(e);
        }

        Assertion.finalizeSoftAsserts();
    }


    /**
     * NAME : SYS_TC_BANK_MASTER_N_0001
     * DESC: To verify that "Super Admin" should not be able to add Bank in the system when Bank Name field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0})
    public void bankMaster001() throws Exception {

        ExtentTest t1 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0001", "To verify that \"Super Admin\" should not be " +
                "able to add Bank in the system when Bank Name field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            bank.BankName = Constants.BLANK_CONSTANT;
            startNegativeTest();
            CurrencyProviderMapping.init(t1)
                    .addBank(bank);

            Assertion.verifyErrorMessageContain("Bank.validation.bankName.blank", "Bank Name Blank Test", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0002
     * DESC : To verify that "Super Admin" should not be able to add Bank
     * in the system when special characters are entered in "Bank Name" field .
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void bankMaster002() throws Exception {

        ExtentTest t1 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0002", "To verify that \"Super Admin\" should not be able to add Bank " +
                "in the system when special characters are entered in \"Bank Name\" field .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        try {
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            bank.BankName = Constants.SPECIAL_CHARACTER_CONSTANT;
            startNegativeTest();
            CurrencyProviderMapping.init(t1)
                    .addBank(bank);

            Assertion.verifyErrorMessageContain("Bank.validation.bankName.alphaNumeric", "Bank Name Validation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * DESC: To verify that "Super Admin" should not be able to add Bank in the system when Pool Account Number field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void SYS_TC_BANK_MASTER_N_0003() throws Exception {
        boolean isModified = false;
        try {
            ExtentTest t1 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0003",
                    "To verify that Super Admin should not be able to" +
                            " add Bank in the system when Pool Account Number field is left blank.");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);

            Login.init(t1).loginAsSuperAdmin(saAddBank);

            bank.PoolAccNum = Constants.BLANK_CONSTANT;
            bank.CBSType = Constants.BLANK_CONSTANT;

            startNegativeTest();
            CurrencyProviderMapping.init(t1)
                    .addBank(bank);

            if (ConfigInput.isCoreRelease) {
                Assertion.verifyErrorMessageContain("poolAccount.mandatory", "Bank pool A/c Validation", t1);
            } else {
                Assertion.verifyErrorMessageContain("Bank.validation.poolAccount.enter", "Bank pool A/c Validation", t1);
            }

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * NAME : SYS_TC_BANK_MASTER_N_0004
     * DESC : To verify that "Super Admin" should not be able to add Bank in the system when special characters are
     * entered in Pool Account Number field.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1})
    public void P1_TC_448() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("P1_TC_448", "To verify that \"Super Admin\" should not be able" +
                    " to add Bank in the system when special characters are entered in Pool Account Number field.");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1);

            Login.init(t1)
                    .loginAsSuperAdmin(saAddBank);


            bank.PoolAccNum = Constants.SPECIAL_CHARACTER_CONSTANT;
            startNegativeTest();
            CurrencyProviderMapping.init(t1)
                    .addBank(bank);

            String expectedMsg = MessageReader.getDynamicMessage("Bank.validation.poolAccount", AppConfig.poolAccountLength, AppConfig.minPoolAccountLength);

            String actual = Assertion.getErrorMessage();

            Assertion.verifyEqual(actual, expectedMsg, "Bank pool A/c Validation", t1);
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }

    }

    /**
     * NAME :   SYS_TC_BANK_MASTER_N_0005
     * DESC :   To verify that "Super Admin" should not be able to add Bank in the system when less than
     * allowed minimum value entered in Pool Account Number field.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_BANK_MASTER_N_0005() throws Exception {

        ExtentTest t1 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0005", "To verify that \"Super Admin\" should not be able" +
                " to add Bank in the system when less than allowed minimum value entered in Pool Account Number field.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t1)
                .loginAsSuperAdmin(saAddBank);

        int minAccNoLength = AppConfig.minPoolAccountLength;
        //Set Random number less than min Acc Number length
        bank.PoolAccNum = DataFactory.getRandomNumberAsString(minAccNoLength - 1);

        startNegativeTest();
        CurrencyProviderMapping.init(t1)
                .addBank(bank);

        String expectedMsg = MessageReader.getDynamicMessage("Bank.validation.poolAccount", AppConfig.poolAccountLength, AppConfig.minPoolAccountLength);

        String actual = Assertion.getErrorMessage();

        Assertion.verifyEqual(actual, expectedMsg, "Bank pool A/c Validation", t1);
    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0006
     * DESC : To verify that "Super Admin" should not be able to add Bank in the system
     * when alphanumeric value is entered in Pool Account Number field.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0006() throws Exception {

        ExtentTest t1 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0006", "To verify that \"Super Admin\" should not be able " +
                "to add Bank in the system when alphanumeric value is entered in Pool Account Number field.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t1)
                .loginAsSuperAdmin(saAddBank);

        bank.PoolAccNum = Constants.ALPHANUMERIC_CONSTANT;

        startNegativeTest();
        CurrencyProviderMapping.init(t1)
                .addBank(bank);

        String expectedMsg = MessageReader.getDynamicMessage("Bank.validation.poolAccount", AppConfig.poolAccountLength, AppConfig.minPoolAccountLength);

        String actual = Assertion.getErrorMessage();

        Assertion.verifyEqual(actual, expectedMsg, "Bank pool A/c Validation", t1);
    }


    /**
     * NAME : SYS_TC_BANK_MASTER_N_0007
     * DESC :To verify that "Super Admin" should not be able to add Bank in the system when
     * Pool Account Type is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0007() throws Exception {

        ExtentTest t1 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0007", "To verify that \"Super Admin\" should not " +
                "be able to add Bank in the system when  Pool Account Type is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t1)
                .loginAsSuperAdmin(saAddBank);

        bank.PoolAccType = Constants.VALUE_SELECT;
        startNegativeTest();
        CurrencyProviderMapping.init(t1)
                .addBank(bank);

        Assertion.verifyErrorMessageContain("Bank.validation.PoolAccType.select", "Bank Pool A/c Type Validation", t1);

    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0008
     * DESC : To verify that "Super Admin" should not be able to add Bank in the system when CBS Type is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0})
    public void SYS_TC_BANK_MASTER_N_0008() throws Exception {

        ExtentTest t1 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0008", "To verify that \"Super Admin\" should not be able" +
                " to add Bank in the system when CBS Type is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1)
                .loginAsSuperAdmin(saAddBank);

        bank.CBSType = Constants.BLANK_CONSTANT;
        startNegativeTest();
        CurrencyProviderMapping.init(t1)
                .addBank(bank);

        Assertion.verifyErrorMessageContain("Bank.validation.cbsType.select", "Bank CBS Type Validation", t1);

    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0009
     * DESC : To verify that "Super Admin" should not be able to add Bank in the system when Bank already exist in the system.
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0009() throws Exception {

        ExtentTest t1 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0009", "To verify that \"Super Admin\" should not be able" +
                " to add Bank in the system when Bank already exist in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t1)
                .loginAsSuperAdmin(saAddBank);

        bank.BankName = DataFactory.getDefaultBankNameForDefaultProvider();

        startNegativeTest();
        CurrencyProviderMapping.init(t1)
                .addBank(bank);

        Assertion.verifyErrorMessageContain("bankName.alreay.Exist", "Bank Already Exist Validation", t1);

    }

    /**
     * NAME :
     * DESC : To verify that the Superadmin can add a bank (without Pool account number) with all the details.
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1})
    public void P1_TC_446() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_446", "To verify that the Superadmin can add a " +
                "bank (without Pool account number) with all the details.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1);

        try {
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_POOL_ACC_REQ", "N");

            Login.init(t1)
                    .loginAsSuperAdmin(saAddBank);

            bank.PoolAccNum = Constants.BLANK_CONSTANT;
            bank.PoolAccType = Constants.VALUE_SELECT;
            bank.CBSType = Constants.BLANK_CONSTANT;

            CurrencyProviderMapping.init(t1)
                    .addBank(bank);

            ExtentTest t2 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0011",
                    "To verify that Superadmin can delete Bank(Without Pool A/c number) from system.");

            t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0);

            CurrencyProviderMapping.init(t2)
                    .deleteBank(bank.BankName);

            ExtentTest t3 = pNode.createNode("P1_TC_445",
                    "To verify that SuperAdmin can again add same bank which" +
                            " was deleted from system (Without Pool A/c number).")
                    .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);

            CurrencyProviderMapping.init(t1)
                    .addBank(bank);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_POOL_ACC_REQ", "Y");
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * NAME : SYS_TC_BANK_MASTER_N_0013
     * DESC : To verify that "Super Admin" should not be able to add Bank Account in the system when account number field is left blank.
     *
     * @throws IOException
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0013() throws Exception {

        ExtentTest t13 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0013", "To verify that \"Super Admin\" should not " +
                "be able to add Bank Account in the system when account number field is left blank.");

        t13.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        try {
            Login.init(t13)
                    .loginAsSuperAdmin(saAddBankAccount);

            bank.PoolAccNum = Constants.BLANK_CONSTANT;

            bank.setBankName(DataFactory.getDefaultBankNameForDefaultProvider());

            startNegativeTestWithoutConfirm();

            CurrencyProviderMapping.init(t13)
                    .addServiceProviderBankAccount(bank.BankName, bank.PoolAccNum);

            String expected = MessageReader.getDynamicMessage("bank.account.mandatory", AppConfig.minPoolAccountLength);

            Assertion.verifyDynamicErrorMessageContain(expected, "Bank Account Blank Validation", t13);
        } catch (Exception e) {
            markTestAsFailure(e, t13);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0014
     * DESC : To veify that "Super Admin" should not be able
     * to add Bank Account in the system when account number is less than allowed limit.
     *
     * @throws IOException
     */
    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0014() throws Exception {

        ExtentTest t14 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0014", "To veify that \"Super Admin\" should not be able " +
                "to add Bank Account in the system when account number is less than allowed limit.");

        t14.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        try {
            Login.init(t14)
                    .loginAsSuperAdmin(saAddBankAccount);

            bank.setBankName(DataFactory.getDefaultBankNameForDefaultProvider());

            int minAccNoLength = AppConfig.minPoolAccountLength;
            //Set Random number less than min Acc Number length
            bank.PoolAccNum = DataFactory.getRandomNumberAsString(minAccNoLength - 1);

            startNegativeTestWithoutConfirm();

            CurrencyProviderMapping.init(t14)
                    .addServiceProviderBankAccount(bank.BankName, bank.PoolAccNum);

            String expected = MessageReader.getDynamicMessage("bank.account.mandatory", AppConfig.minPoolAccountLength);

            Assertion.verifyDynamicErrorMessageContain(expected, "Bank Account Less than Min Length Validation", t14);
        } catch (Exception e) {
            markTestAsFailure(e, t14);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0015
     * DESC : To veify that "Super Admin" should not be able to
     * add Bank Account in the system when special characters are entered in account number field.
     *
     * @throws IOException
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0015() throws Exception {

        ExtentTest t15 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0015", "To veify that \"Super Admin\" should not be able to " +
                "add Bank Account in the system when special characters are entered in account number field.");

        t15.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        try {
            Login.init(t15)
                    .loginAsSuperAdmin(saAddBankAccount);

            bank.setBankName(DataFactory.getDefaultBankNameForDefaultProvider());

            bank.PoolAccNum = Constants.SPECIAL_CHARACTER_CONSTANT;

            startNegativeTestWithoutConfirm();

            CurrencyProviderMapping.init(t15)
                    .addServiceProviderBankAccount(bank.BankName, bank.PoolAccNum);

            Assertion.verifyErrorMessageContain("bank.account.numeric", "Verify Bank Account Special Character", t15);
        } catch (Exception e) {
            markTestAsFailure(e, t15);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0016
     * DESC :To verify that "Super Admin" can Add More account options when clicked on
     * "Add More" under selected bank in "Add ServiceProvider Bank Accounts" Page then one Extra Row should come.
     *
     * @throws IOException
     */
    @Test(priority = 16, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0016() throws Exception {

        ExtentTest t15 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0016", "To verify that \"Super Admin\" can Add More account options when clicked on \"Add More\" " +
                "under selected bank in \"Add ServiceProvider Bank Accounts\" Page then one Extra Row should come.");

        t15.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t15)
                .loginAsSuperAdmin(saAddBankAccount);

        bank.setBankName(DataFactory.getDefaultBankNameForDefaultProvider());

        startNegativeTestWithoutConfirm();

        CurrencyProviderMapping.init(t15)
                .checkAddServiceProviderBankAcPageButtons(bank.BankName, bank.PoolAccNum, Button.ADD_MORE_BANK_AC);

    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0016
     * DESC :To verify that "Super Admin" should not be
     * able add bank account  when clicked on "Remove" button if there is only one row available.
     *
     * @throws IOException
     */
    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0017() throws Exception {

        ExtentTest t15 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0017", "To verify that \"Super Admin\" should not be able " +
                "add bank account  when clicked on \"Remove\" button if there is only one row available.");

        t15.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t15)
                .loginAsSuperAdmin(saAddBankAccount);

        bank.setBankName(DataFactory.getDefaultBankNameForDefaultProvider());

        startNegativeTestWithoutConfirm();

        CurrencyProviderMapping.init(t15)
                .checkAddServiceProviderBankAcPageButtons(bank.BankName, bank.PoolAccNum, Button.REMOVE);

        Assertion.verifyErrorMessageContain("no.bank.account.add", "Remove Bank Account Row", t15);

    }

    /**
     * NAME : SYS_TC_BANK_MASTER_N_0018
     * DESC : To verify that
     * when "Super Admin" clicks on "Back" button then it should navigate to previous page successfully.
     *
     * @throws IOException
     */
    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0018() throws Exception {

        ExtentTest t15 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0018", "To verify that " +
                "when \"Super Admin\" clicks on \"Back\" button then it should navigate to previous page successfully.");

        t15.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t15)
                .loginAsSuperAdmin(saAddBankAccount);

        bank.setBankName(DataFactory.getDefaultBankNameForDefaultProvider());

        startNegativeTestWithoutConfirm();

        CurrencyProviderMapping.init(t15)
                .checkAddServiceProviderBankAcPageButtons(bank.BankName, bank.PoolAccNum, Button.BACK);

    }


    /**
     * NAME : SYS_TC_BANK_MASTER_N_0019
     * DESC : "To verify that when superadmin clicks on Remove more Bank
     * Account button then extra row should be removed."
     *
     * @throws IOException
     */
    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void SYS_TC_BANK_MASTER_N_0019() throws Exception {

        ExtentTest t15 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0019", "To verify that when superadmin clicks on Remove more Bank " +
                "Account button then extra row should be removed.");

        t15.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t15)
                .loginAsSuperAdmin(saAddBankAccount);

        bank.setBankName(DataFactory.getDefaultBankNameForDefaultProvider());

        startNegativeTestWithoutConfirm();

        CurrencyProviderMapping.init(t15)
                .checkAddServiceProviderBankAcPageButtons(bank.BankName, bank.PoolAccNum, Button.REMV_MORE_BANK_AC);

        Assertion.verifyErrorMessageContain("one.bank.account.delete", "Remove Bank Account Extra Row", t15);

    }


    /**
     * TEST : TC125 (From P1 Sheet)
     * DESC : To verify that Banks those are registered with Pool A/C number should
     * get displayed on Channel user/Subscriber Bank Addition Page.
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1, "Test"})
    public void P1_TC_308() throws Exception {

        ExtentTest t308 = pNode.createNode("P1_TC_308",
                "To verify that Banks those are registered with Pool A/C number and mapped with Serivices should get displayed on Channel user/Subscriber Bank Addition Page.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1);

        OperatorUser usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");

        User user = new User(Constants.WHOLESALER);

        Login.init(t308).login(usrCreator);

        ChannelUserManagement chMgmtobj = ChannelUserManagement.init(t308);
        CommonUserManagement commonUsrMgmtObj = CommonUserManagement.init(t308);

        chMgmtobj.initiateChannelUser(user).assignHierarchy(user);

        commonUsrMgmtObj
                .assignWebGroupRole(user);

        commonUsrMgmtObj
                .mapDefaultWalletPreferences(user);

        AddChannelUser_pg5 channelUser_pg5 = new AddChannelUser_pg5(t308);

        List<String> bankNamesFromDb = MobiquityGUIQueries.dbGetBankNamesMappedWithServices(DataFactory.getDefaultProvider().ProviderId);

        boolean isPresent = channelUser_pg5.verifyBankNamesPresentInBankDdown(bankNamesFromDb);

        //driver.findElement(By.name("bankCounterList[0].paymentTypeSelected")).click();

        /*bankNameDdown.click();*/
        Assertion.verifyEqual(isPresent, true, "Verify Bank Name Present", t308);

    }


    /**
     * @throws Exception
     */
    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1})
    public void P1_TC_307() throws Exception {

        ExtentTest t15 = pNode.createNode("P1_TC_307",
                "To verify that superadmin can add more account options when clicked on 'Add More' under selected bank.");

        t15.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.PVG_P1);

        Login.init(t15)
                .loginAsSuperAdmin(saAddBankAccount);

        startNegativeTestWithoutConfirm();

        CurrencyProviderMapping.init(t15).addServiceProviderBankAccount(DataFactory.getDefaultBankNameForDefaultProvider(), "" + DataFactory.getRandomNumber(AppConfig.minPoolAccountLength + 1));

        AddServiceProviderBankAccounts1 page = new AddServiceProviderBankAccounts1(t15);

        page.clickOnBack();
        page.clickOnAddMoreBankAcButton();
        page.setPoolAccountNumber1("" + DataFactory.getRandomNumber(AppConfig.minPoolAccountLength + 1));
        page.clickOnBankSubmit2();
        page.clickOnConfirm();
        Assertion.verifyActionMessageContain("bank.account.addition.success", "Add Bank", t15);
    }


    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER})
    public void bankAddWithoutPool() throws Exception {

        ExtentTest t15 = pNode.createNode("NEGATIVE:SYS_TC_BANK_MASTER_N_0019", "To verify that when superadmin clicks on Remove more Bank " +
                "Account button then extra row should be removed.");

        t15.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER);

        Login.init(t15)
                .loginAsSuperAdmin(saAddBankAccount);

        bank.setBankName(DataFactory.getDefaultBankNameForDefaultProvider());

        startNegativeTestWithoutConfirm();

        CurrencyProviderMapping.init(t15)
                .checkAddServiceProviderBankAcPageButtons(bank.BankName, bank.PoolAccNum, Button.REMV_MORE_BANK_AC);

        Assertion.verifyErrorMessageContain("one.bank.account.delete", "Remove Bank Account Extra Row", t15);

    }

    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_BANK, FunctionalTag.ECONET_SIT_5_0})
    public void deleteBankMapping() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0103_B :Delete Bank Mapping", "To verify that the \"Super Admin\" cannot delete Bank Mapping if it is associated with Group Role or any User.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_BANK, FunctionalTag.ECONET_SIT_5_0);

        Login.init(t1).loginAsSuperAdmin("MFSBMD");

        CurrencyProviderMapping.init(t1).deleteServiceProviderBankMappingNew(DataFactory.getDefaultProvider().ProviderId, DataFactory.getDefaultBankNameForDefaultProvider());

    }


}
