package tests.core.orange;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.BillerAttribute;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


public class Suite_BillDueDate extends TestInit {

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest t1 = pNode.createNode("Bill Pay");
        try {
            Biller biller = BillerManagement.init(t1)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_STANDARD, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_UNDER_PAYMENT);

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //Biller biller = DataFactory.getBillerFromAppdata();
            ServiceCharge bill = new ServiceCharge(Services.AgentAssistedPresentmentBillPayment,
                    whs, biller, null, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(bill);

            String accNo = DataFactory.getRandomNumberAsString(5);
            String filename = BillerManagement.init(t1).generateBillForUpload(biller, accNo);

            Login.init(t1).login(opt);
            BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename);

            //Perform Bill Payment
            ChannelUserManagement.init(t1).billPayment(biller, Constants.BILL_PROCESS_TYPE_OFFLINE,
                    Services.AgentAssistedPresentmentBillPayment, accNo, "10", subs);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}



