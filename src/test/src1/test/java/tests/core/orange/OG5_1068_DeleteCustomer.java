package tests.core.orange;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UserWallets;
import framework.dataEntity.UsrBalance;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.*;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page4;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.GlobalData;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.List;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setTxnProperty;

public class OG5_1068_DeleteCustomer extends TestInit {
    private OperatorUser optBankApprover, optBankInitiator, admin, o2cInitiator, o2cApprover1, o2cApprover2;
    private User deleteSubs, wholesaler, silverSubscriber, subscriber;
    private RechargeOperator RCoperator;
    private String custIdLength, custId, prefValue1;
    private Bank defaultBank;

    //TODO - OG5-1997 - NOT AUTOMATED : Selenium framework doesn't support multi currency end to end
    //TODO - Creating users with multiple currency and multiple SVA's and performing services where balance will be in FROZEN_AMOUNT & FIC column

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Setup Specific to this Script");

        try {
            defaultBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            RCoperator = OperatorUserManagement.init(setup).getRechargeOperator(DataFactory.getDefaultProvider().ProviderId);
            optBankInitiator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            optBankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");
            o2cApprover2 = DataFactory.getOperatorUserWithAccess("O2C_APP2");
            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            silverSubscriber = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            subscriber = DataFactory.getChannelUserWithCategoryAndGradeCode(Constants.SUBSCRIBER, Constants.SILVER_SUBSCRIBER);
            admin = new OperatorUser(Constants.NETWORK_ADMIN);

            custIdLength = MobiquityGUIQueries.fetchDefaultValueOfPreference("CUSTOMER_ID_LENGTH");
            prefValue1 = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANK_ACC_LINKING_VIA_MSISDN");

            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANKING_SERVICES_FOR_TRUST_BANK");
            if (!prefValue.equalsIgnoreCase("TRUE")) {
                SystemPreferenceManagement.init(setup).updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
            }

            /**
             * JIRA test case ID - OG5-1996
             * Verify that Delete Customer API is always single step even if ACCOUNT_CLOSURE_TWO_STEP = Y in the system
             */
            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("ACCOUNT_CLOSURE_TWO_STEP");
            if (!prefValue.equalsIgnoreCase("Y")) {
                SystemPreferenceManagement.init(setup).updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "Y");
            }

            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MODIFY_SUBS_MAKER_CHECKER_ALLOWED");
            if (!prefValue.equalsIgnoreCase("TRUE")) {
                SystemPreferenceManagement.init(setup).updateSystemPreference("MODIFY_SUBS_MAKER_CHECKER_ALLOWED", "TRUE");
            }

            //Performing O2C to Commission wallet of Wholesaler [Tax to be deducted during Delete customer By Agent service]
            SfmResponse o2cResponse = Transactions.init(setup).initiateO2C(o2cInitiator, wholesaler, Constants.minimun_AMOUNT, Constants.COMMISSION_WALLET,
                    defaultProvider.ProviderId);
            SfmResponse o2cResponse1 = Transactions.init(setup).approveO2C(o2cApprover1, o2cResponse.ServiceRequestId);
            if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                    Constants.minimun_AMOUNT, o2cInitiator.LoginId, wholesaler.MSISDN)) {
                Transactions.init(setup).approveO2C(o2cApprover2, o2cResponse1.ServiceRequestId)
                        .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            }

            /*
             * - JIRA test case ID - OG5-1999
             * - Deleting non-financial service charge if exist for Get Banks for Subscriber
             */
            ServiceCharge nfscSCharge = new ServiceCharge(Services.GET_BANKS_PER_PROVIDER, silverSubscriber, admin, null, null, null,
                    null);
            ServiceChargeManagement.init(setup).deleteNFSChargeAllVersions(nfscSCharge);

            /*
             * - JIRA test case ID - OG5-1995 & OG5-2002
             * - JIRA Defect ID - OG5-2205
             * - ACQUISITION FESS service charge can be deleted if ACQUISITION_PAID != N for any of the active silverSubscriber in MTX_PARTY_ACCESS table
             * - Creating Acquisition Fess service charge with charge and commission value as 1 [value more than 0] so that acquisition_paid & comm_paid is N
             */
            /*ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, silverSubscriber, admin, null, null,
                    null, null);
            ServiceChargeManagement.init(setup).deleteServiceChargeAllVersions(sCharge);*/ // TODO - JIRA Defect ID - OG5-2205

            ServiceCharge sCharge1 = new ServiceCharge(Services.ACQFEE, silverSubscriber, admin, null, null,
                    null, null);

            sCharge1.setCommisisonRangeFixed("1"); //To set fixed service charge value as 1 for Acquisition Fees service charge
            sCharge1.setServiceChargeFixed("1"); //To set fixed commission value as 1 for Acquisition Fees service charge
            ServiceChargeManagement.init(setup).configureServiceCharge(sCharge1);

            //Creating Transfer Rule for Recharge - Self for Subscriber
            User rechargeReceiver = new User(Constants.ZEBRA_MER);
            ServiceCharge recharge = new ServiceCharge(Services.SELF_RECHARGE, silverSubscriber, rechargeReceiver, null, null, null,
                    null);
            TransferRuleManagement.init(setup).configureTransferRule(recharge);

            //creating transfer rules for Transaction Correction
            ServiceCharge scTxnCorrect = new ServiceCharge(Services.TXN_CORRECTION, silverSubscriber, wholesaler, null, null,
                    null, null);
            TransferRuleManagement.init(setup).configureTransferRule(scTxnCorrect);

            //creating transfer rules for P2P - send money to unknown
            ServiceCharge charge = new ServiceCharge(Services.P2PNONREG, silverSubscriber, admin, null, null, null,
                    null);
            TransferRuleManagement.init(setup).configureTransferRule(charge);

            //Creating transfer rule for Stock Reimbursement
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, silverSubscriber, admin, null, null, null,
                    null);
            TransferRuleManagement.init(setup).configureTransferRule(obj);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {
            /*
             * - JIRA Defect ID - OG5-2205
             * - ACQUISITION FESS service charge can be deleted if ACQUISITION_PAID != N for any of the active silverSubscriber in MTX_PARTY_ACCESS table
             * - Creating Acquisition Fess service charge with charge and commission value as 0 so that acquisition_paid & comm_paid is Y
             */
            /*ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, silverSubscriber, admin, null, null,
                    null, null);
            ServiceChargeManagement.init(tearDown).deleteServiceChargeAllVersions(sCharge);*/ // TODO - JIRA Defect ID - OG5-2205

            ServiceCharge sCharge1 = new ServiceCharge(Services.ACQFEE, silverSubscriber, admin, null, null,
                    null, null);
            ServiceChargeManagement.init(tearDown).configureServiceCharge(sCharge1);

            ServiceCharge nfscSCharge = new ServiceCharge(Services.GET_BANKS_PER_PROVIDER, silverSubscriber, admin, null, null, null,
                    null);
            ServiceChargeManagement.init(tearDown).configureNonFinancialServiceCharge(nfscSCharge);

        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void TC_OG5_1982() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1982", "Verify that barred subscriber should be deleted successfully through Delete Customer API");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5");

            UsrBalance preSubBal = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            Transactions.init(t1).userBarUnbar(deleteSubs, "BAR", Constants.BAR_AS_BOTH);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            String dbStatus = MobiquityGUIQueries.getBarredUserDetails(userId);
            //once user is deleted, records should be removed from mtx_party_black_list table
            if (dbStatus == null) {
                t1.pass("Deleted user details is not available in mtx_party_black_list table");
            } else {

                t1.fail("Deleted user details is still available in mtx_party_black_list table");
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubBal.Balance.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void TC_OG5_1983() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1983", "Verify that suspended subscriber should be deleted successfully through Delete Customer API");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5");

            UsrBalance preSubBal = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            Transactions.init(t1).suspendResumeUser(deleteSubs, "SUSPEND");

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubBal.Balance.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void TC_OG5_1984() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1984", "Verify that system should not allow to delete the delete initiated subscriber through Delete Customer API");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER);

            //Creating Transfer Rule for Account Closure By Agent
            ServiceCharge accClosure = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, deleteSubs, wholesaler, null,
                    null, null, null);
            ServiceChargeManagement.init(t1).configureServiceCharge(accClosure);

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "7");

            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            String txnID = SubscriberManagement.init(t1).deleteSubscriberByAgent(deleteSubs, DataFactory.getDefaultProvider().ProviderId, true);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), Constants.STATUS_DELETE_INITIATE,
                    "Verify Subscriber Status", t1);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus("77777").verifyMessage("77777");

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), Constants.STATUS_DELETE_INITIATE,
                    "Verify Subscriber Status", t1);

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue(), "Verify that IND01 pre and post balance is same");

            Transactions.init(t1).accountClosurebyAgentConfirmation(deleteSubs.MSISDN, txnID);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4)
    public void TC_OG5_1985() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1985", "Verify that modify initiated subscriber should be deleted successfully through Delete Customer API");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.isBankRequired = false;
            SubscriberManagement.init(t1).createSubscriber(deleteSubs);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "3");

            UsrBalance preSubBal = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            SubscriberManagement.init(t1).modifySubscriber(DataFactory.getChannelUserWithAccess("SUBSMOD"), deleteSubs);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubBal.Balance.intValue());

            String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode);
            //once user is deleted, records should be removed from temporary tables
            if (dbStatus == null) {
                Assertion.logAsPass("Initiated record is removed from mtx_party_m table as expected", t1);
            }else{
                t1.fail("Initiated record still present in mtx_party_m table");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void TC_OG5_1986() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1986", "Verify that system should not allow to delete the churn initiated subscriber through Delete Customer API");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            //Churn initiate the user
            String batchId = CommonUserManagement.init(t1).churnInitiateUser(deleteSubs);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus("1930");

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);

            //POST test case verification churned the user
            CommonUserManagement.init(t1).approveRejectChurnInitiate(deleteSubs, batchId, true);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "N", "Verify Subscriber Status", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6)
    public void TC_OG5_1987_1988_1989_1990() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1987", "Verify that system should not allow to delete the subscriber through Delete Customer API whose bank account " +
                "addition is pending for approval");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            if (!prefValue1.equalsIgnoreCase("N")) {
//                custId = DataFactory.getRandomNumberAsString(Integer.valueOf(custIdLength)); //TODO - changes if custIdLength value is more than 9
                custId = DataFactory.getRandomNumberAsString(9);
            } else {
                custId = deleteSubs.MSISDN;
            }
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            // make sure Bank Account link KYC Preference is Mapped
            CurrencyProviderMapping.init(t1).linkPrimaryBankWalletPreference(deleteSubs);

            Login.init(t1).login(optBankInitiator);
            BankAccountAssociation.init(t1).initiateBankAccountRegistration(deleteSubs, defaultProvider.ProviderName, defaultBank.BankName,custId,custId, false);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus("991235").verifyMessage("991235");

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("OG5-1990", "Verify that system should not allow to delete the subscriber through Delete Customer API if user is having bank " +
                "account linked");
        try {
            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1).approveAssociatedBanksForUser(deleteSubs, defaultProvider.ProviderName, defaultBank.BankName);

            Transactions.init(t2).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE")
                    .verifyStatus("3500").verifyMessage("3500");

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        ExtentTest t3 = pNode.createNode("OG5-1989", "Verify that system should not allow to delete the subscriber through Delete Customer API whose bank account " +
                "modification is pending for approval");
        try {
            //Modifying the bank account(s) of silverSubscriber
            SubscriberManagement.init(t3).navigateToModifySubscriberWalletAssociationPage(deleteSubs);
            ModifySubscriber_page4.init(t3).clickSubmitPg4(); //Click on Next button in wallet association page
            SubscriberManagement.init(t3).modifySubscriberExistingBankStatus("Suspended", custId);

            Transactions.init(t3).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus("3500").verifyMessage("3500");

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);

            SubscriberManagement.init(t3).modifyApprovalSubs(deleteSubs);

            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .rejectAssociatedBanksForUser(deleteSubs, defaultProvider.ProviderName, defaultBank.BankName, custId);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        ExtentTest t4 = pNode.createNode("OG5-1988", "Verify that system should not allow to delete the subscriber through Delete Customer API whose bank account " +
                "deletion is pending for approval");
        try {
            //Deleting the bank account(s) of silverSubscriber
            SubscriberManagement.init(t4).navigateToModifySubscriberWalletAssociationPage(deleteSubs);
            ModifySubscriber_page4.init(t4).clickSubmitPg4(); //Click on Next button in wallet association page
            SubscriberManagement.init(t4).modifySubscriberExistingBankStatus("Deleted", custId);

            Transactions.init(t4).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE")
                    .verifyStatus("3500").verifyMessage("3500");

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);

            Login.init(t4).login(optBankApprover);
            CommonUserManagement.init(t4).approveAssociatedBanksForUser(deleteSubs, defaultProvider.ProviderName, defaultBank.BankName);

            Transactions.init(t4).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);
            Utils.putThreadSleep(2000);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "N", "Verify Subscriber Status", t1);

            String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode);
            //once user is deleted, records should be removed from temporary tables
            if (dbStatus == null) {
                Assertion.logAsPass("Initiated record is removed from mtx_party_m table as expected", t4);
            }else{
                t4.fail("Initiated record still present in mtx_party_m table");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7)
    public void TC_OG5_1991() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1991", "Verify that system should not allow to delete the subscriber through Delete Customer API if there are " +
                "transactions in ambiguous state for the user where user is the payer/payee");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "6");

            //Performing recharge
            SfmResponse response = Transactions.init(t1).rechargeSelf(deleteSubs, RCoperator.OperatorId, "3", Constants.NORMAL_WALLET);
            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(response.TransactionId), Constants.TXN_STATUS_AMBIGUOUS, "Verify Transaction Status", t1);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus("991234").verifyMessage("991234");

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8)
    public void TC_OG5_1992() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1992", "Verify that system should allow to delete the subscriber through Delete Customer API when user is having pending " +
                "transaction [frozen_amount] waiting for approval [incase of subscriber even if FAIL_FROZEN_TXN_ON_DEL = TRUE in the system]");
        try {
            //Creating Subscriber
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            Transactions.init(t1).subRegistrationByAS400(deleteSubs).verifyStatus(Constants.TXN_SUCCESS);
            if (AppConfig.isTwoStepRegistrationForSubs) {
                Transactions.init(t1).performSubscriberAcquisition(deleteSubs, Constants.STATUS_ACCEPT);
            }

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            //Performing 1st Cash In service
            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "6");

            setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
            setTxnProperty(of("txn.reversal.allowed.services", "CASHIN"));

            //Performing 2nd Cash In service for transaction correction
            String txnId = Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5").TransactionId;

            //Subscriber Pre Balance
            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(deleteSubs, null, null);

            //Performing Stock Reimbursement
            String reimTxnId = StockManagement.init(t1)
                    .initiateStockReimbursement(deleteSubs, DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(3), "3",
                            "reimbursement", DataFactory.getDefaultWallet().WalletId);

            //Initiating Transaction Correction for the 1st Cash In
            String txnInitiateId = TransactionCorrection.init(t1).initiateTxnCorrection(txnId, false, false);
            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnInitiateId), Constants.TXN_STATUS_INITIATED, "Verify Transaction Status", t1);

            //Subscriber Post Balance
            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(deleteSubs, null, null);
            Assert.assertEquals(preSubsBalance.frozenBalance.intValue() + 8, postSubsBalance.frozenBalance.intValue());

            String value = MobiquityGUIQueries.fetchDefaultValueOfPreference("FAIL_FROZEN_TXN_ON_DEL");
            if (!value.equalsIgnoreCase("TRUE")) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("FAIL_FROZEN_TXN_ON_DEL", "TRUE");
            }

            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnInitiateId), Constants.TXN_STATUS_FAIL, "Verify Transaction Correction Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(reimTxnId), Constants.TXN_STATUS_FAIL, "Verify Reimbursement Transaction Status", t1);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
                Assertion.verifyEqual(wallet.frozenAmount.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubsBalance.Balance.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9)
    public void TC_OG5_1993() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1993", "Verify that system should allow to delete subscriber through Delete Customer API when there is any P2P " +
                "unregistered transaction(s) for which the money is underlying in IND02 wallet even if FAIL_OTF_ON_DEL = TRUE in the system");
        try {
            //Creating Subscriber
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            //Performing 1st Cash In service
            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5");

            //Performing P2P - Send Money to unknown service
            User nonRegSubs = new User(Constants.SUBSCRIBER);
            String txnId = Transactions.init(t1).p2pNonRegTransaction(deleteSubs, nonRegSubs, new BigDecimal(3)).TransactionId;

            String value = MobiquityGUIQueries.fetchDefaultValueOfPreference("FAIL_OTF_ON_DEL");
            if (!value.equalsIgnoreCase("TRUE")) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("FAIL_OTF_ON_DEL", "TRUE");
            }

            UsrBalance preSubBal = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            BigDecimal preIND02Bal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND02");

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            BigDecimal postIND02Bal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND02");

            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubBal.Balance.intValue() + 3);
            Assert.assertEquals(postIND02Bal.intValue(), preIND02Bal.intValue() - 3);

            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnId), Constants.TXN_STATUS_FAIL, "Verify Transaction Status", t1);

            //Post Delete Customer check
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(nonRegSubs, true, false);
            UsrBalance nonSubBal = MobiquityGUIQueries.getUserBalance(nonRegSubs, GlobalData.defaultWallet.WalletId, null);
            Assert.assertEquals(nonSubBal.Balance.intValue(), 0);
            Transactions.init(t1).deleteCustomer(nonRegSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10)
    public void TC_OG5_1998() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1998", "Verify that if subscriber is having multiple wallets and funds are available in non-primary wallet(s), " +
                "then funds from non-primary wallet(s) should move to the primary wallet of that MFS Provider and then to IND01 of that respective currency");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.isBankRequired = false;
            SubscriberManagement.init(t1).createSubscriber(deleteSubs);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            String[] payIdArr = DataFactory.getPayIdApplicableForSubs();
            String[] walletIds = (payIdArr.length > 0) ? payIdArr : new String[]{DataFactory.getDefaultWallet().WalletId};
            for (String walletId : walletIds) {
                //creating transfer rules for P2P - send money to unknown
//                ServiceCharge charge = new ServiceCharge(Services.CASHIN, wholesaler, deleteSubs, null, walletId, null, null);
//                TransferRuleManagement.init(t1).configureTransferRule(charge);
                Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5", walletId);
            }

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11)
    public void TC_OG5_1995_2002() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-2002", "Verify that system should allow to delete the subscriber through Delete Customer API if the silverSubscriber has not " +
                "paid his acquisition fees");

        ExtentTest t2 = pNode.createNode("OG5-1995", "Verify that system should allow to delete the subscriber through Delete Customer API even if the silverSubscriber has " +
                "not paid his joining fees");
        try {

            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5");

            UsrBalance preSubBal = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preBalance = preSubBal.Balance;
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            String acqPaid = MobiquityGUIQueries.fetchAcquisitionPaid(deleteSubs.MSISDN);
            Assertion.verifyEqual(acqPaid, "N", "Acquisition paid is N", t1);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preBalance.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 12)
    public void TC_OG5_2003() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-2003", "Verify that channel user deletion is not possible through Delete Customer [DELCUSTREQ] API");
        try {
            User SSA = DataFactory.getChannelUserWithCategory(Constants.SPECIAL_SUPER_AGENT);
            Transactions.init(t1).deleteCustomer(SSA, defaultProvider.ProviderId, "FORCE").verifyStatus("99033").verifyMessage("99033");
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(SSA.MSISDN, SSA.CategoryCode), "Y", "Verify User Status", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 13)
    public void TC_OG5_2152() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-2152", "Verify that if there are any transaction(s) that have been initiated by the subscriber, they should be marked " +
                "as failed during silverSubscriber deletion through Delete Customer [DELCUSTREQ] API");
        try {
            //Creating Subscriber
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            //Performing 1st Cash In service
            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "9");

            UsrBalance preDelSubsBalance = MobiquityGUIQueries.getUserBalance(deleteSubs, null, null);

            //creating transfer rules for P2P - send money
            ServiceCharge p2psc = new ServiceCharge(Services.P2P, subscriber, deleteSubs, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(p2psc);

            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            t1.info("Subscriber Pre Balance:" + preSubsBalance);

            //Performing P2P - send money initiated by receiver silverSubscriber
            SfmResponse response = Transactions.init(t1)
                    .initiateP2PSendMoney(subscriber, deleteSubs, new BigDecimal(4), defaultWallet.WalletId, defaultWallet.WalletId, defaultProvider.ProviderId);
            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(response.TransactionId), Constants.TXN_STATUS_INITIATED, "Verify Transaction Status", t1);

            String value = MobiquityGUIQueries.fetchDefaultValueOfPreference("FAIL_FROZEN_TXN_ON_DEL");
            if (!value.equalsIgnoreCase("TRUE")) {
                Login.init(t1).loginAsSuperAdmin(DataFactory.getSuperAdminWithAccess("PREF001"));
                Preferences.init(t1).modifySystemPreferences("FAIL_FROZEN_TXN_ON_DEL", "TRUE");
            }

            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
                Assertion.verifyEqual(wallet.frozenAmount.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preDelSubsBalance.Balance.intValue());

            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(response.TransactionId), Constants.TXN_STATUS_FAIL, "Verify Transaction Status", t1);

            SfmResponse p2pResponse = Transactions.init(t1).startNegativeTest()
                    .genericResumeRequest(subscriber, response.ServiceRequestId, defaultProvider.ProviderId, Services.P2P);
            p2pResponse.verifyErrorMessage("withdrawer.notRegistered");

            //Subscriber Post Balance
            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            Assert.assertEquals(preSubsBalance.Balance.intValue(), postSubsBalance.Balance.intValue());
            t1.info("Subscriber Post Balance:" + postSubsBalance);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 14)
    public void TC_OG5_2074_Negative() throws Exception {

        ExtentTest t1 = pNode.createNode("OG5-2074", "Verify that system displays proper error message when MSISDN is not provided in Delete Customer [DELCUSTREQ] API");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            deleteSubs.MSISDN = "";
            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus("00064").verifyMessage("00064");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("OG5-2074_2", "Verify that system displays proper error message when PROVIDER is not provided in Delete Customer [DELCUSTREQ] API");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t2).createSubscriberDefaultMapping(deleteSubs, true, false);

            Transactions.init(t2).deleteCustomer(deleteSubs, "", "FORCE").verifyStatus("5507").verifyMessage("5507");

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        ExtentTest t3 = pNode.createNode("OG5-2074_3", "Verify that system displays proper error message when MODE is not provided in Delete Customer [DELCUSTREQ] API");
        try {
            Transactions.init(t3).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "").verifyStatus("3503").verifyMessage("3503");

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        ExtentTest t4 = pNode.createNode("OG5-2074_4", "Verify that system displays proper error message when wrong MODE is not provided in Delete Customer API");
        try {
            Transactions.init(t4).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCENORMAL").verifyStatus("3504").verifyMessage("3504");

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }

        ExtentTest t5 = pNode.createNode("OG5-2074_5", "Verify that system displays proper error message when alphanumeric PROVIDER is provided in Delete Customer API");
        try {
            Transactions.init(t5).deleteCustomer(deleteSubs, "1XY", "FORCE").verifyStatus("999106").verifyMessage("999106");

        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }

        ExtentTest t6 = pNode.createNode("OG5-2074_6", "Verify that system displays proper error message when inactive PROVIDER is provided in Delete Customer API");
        try {
            Transactions.init(t6).deleteCustomer(deleteSubs, "786", "FORCE").verifyStatus("9999008").verifyMessage("9999008");

        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 15)
    public void TC_OG5_1994() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1994", "Verify that system should allow to delete the subscriber through Delete Customer API even if the user" +
                " has not changed his MPIN/PIN after registration");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            Transactions.init(t1).subsSelfRegistrationWithPayId(deleteSubs, GlobalData.defaultWallet.WalletId);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5");
            String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            UsrBalance preSubBal = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE")
                    .verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
            for (UserWallets subWallet : subWalletList) {
                Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubBal.Balance.intValue());


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 16)
    public void TC_OG5_2454() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-2454", "Verify that subscriber should not be deleted through Delete Customer API when subscriber's SVA is delete initiated");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.isBankRequired = false;
            SubscriberManagement.init(t1).createSubscriber(deleteSubs);

            String userId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "3");

            UsrBalance preSubBal = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            SubscriberManagement.init(t1).modifySubscriberToDeleteWallet(DataFactory.getChannelUserWithAccess("SUBSMOD"), deleteSubs);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus("00405");

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue());

            Login.init(t1).login(DataFactory.getChannelUserWithAccess("SUBSMODAP"));
            SubscriberManagement.init(t1).rejectSubsModification(deleteSubs);

            Transactions.init(t1).deleteCustomer(deleteSubs, defaultProvider.ProviderId, "FORCE").verifyStatus(Constants.TXN_SUCCESS);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, userId);
            for (UserWallets wallet : subWalletList) {
                Assertion.verifyEqual(wallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(wallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(wallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(wallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal1 = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal1.intValue(), preOptBal.intValue() + preSubBal.Balance.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}