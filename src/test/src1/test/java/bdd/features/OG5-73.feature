Feature: Scenarios on modified search combination

  @First
  Scenario: Verify existing From Date and To Date search combination(UI)
    Given Login as Network Admin having Permission to View Barred Users
    When User Navigate to View Barred User Page
    Then Non Mandatory field Bar Type with date and Bar Reason with date should be available

  Scenario: Verify Bar Type and Bar Reason dropdown options
    Given Login as Network Admin having Permission to View Barred Users
    When User Navigate to View Barred User Page
    Then Verify that Bar Reason with Date Dropdown has the same set of options listed in existing search
    Then Verify that Bar Type with Date Dropdown has the same set of options listed in existing search

  Scenario Outline: Test functionality, <test description>
    Given Login as Network Admin having Permission to View Barred Users
    When User Navigate to View Barred User Page
    When User provide the from date for new bar user search with date "<from date>"
    And User Provide the to date for new bar user search with date "<to date>"
    And User Provide bar type for new bar user search with bar type "<bar type>"
    And User Provide bar reason for new bar user search with bar reason "<bar reason>"
    And User initiate the barred user search
    Then User should get result as "<result>"

    Examples:
      | from date |to date| bar type | bar reason | result | test description |
      | -14 | 0 | empty | empty | search result shows list of barred user | Search Barred user by providing the mandatory fields |
      | -14 | 0 | Sender | empty | search result shows barred users with Bar Type as Sender | Search Barred user by providing Bar Type as Sender |
      | -14 | 0 | Receiver | empty | search result shows barred users with Bar Type as Receiver | Search Barred user by providing Bar Type as Receiver |
      | -14 | 0 | Both | empty | search result shows barred users with Bar Type as Both | Search Barred user by providing Bar Type as Both |
      | -14 | 0 | empty | By Operator | search result shows barred users with specific bar reason | Search Barred user by providing Bar Reason |
      | empty | 0 | Sender | By Operator | error.fromDate.required | Verify error when from date* is not provided |
      | -10 | empty | Both | empty | todateblank.error | Verify error when to date* is not provided |

  Scenario: Verify the search result Table changes
    Given Login as Network Admin having Permission to View Barred Users
    When User Navigate to View Barred User Page
    When User provide the mandatory search fields with date and initiate bar user search
    Then Login Id and MSISDN of the barred user must be shown in the updated search result page

  Scenario: Verify the preference RESULT_RANGE
    Given The system preference RESULT_RANGE is set to 1
    Given Login as Network Admin having Permission to View Barred Users
    When User Navigate to View Barred User Page
    When User provide the mandatory search fields with date and initiate bar user search
    Then Search results must show an error as result has exceeded the set range 1

  Scenario: Verify Unbarring a user searched using the search using date combination
    Given Login as Network Admin having Permission to View Barred Users
    When User Navigate to View Barred User Page
    When User provide the mandatory search fields with date and initiate bar user search
    And User selects a barred user
    And clicks on Unbar
    And clicks on Unbar Confirm
    Then Barred User must be successfully Unbarred