Feature: Number of Categories in Domain

  @First
  Scenario: Pre- Requisite for this Suite
    Given Setup required for this suite is initialized

  Scenario: OG5-41, Verify Number of Categories textbox is added
  under 'Domain Management ->Add Domain' screen and is non-mandatory.
    Given Login as Network Admin having Permission to Add domain and category
    When User Navigate to Domain Management > Add Domain
    Then User can see a text field for providing Domain Name
    And User can see a text field for providing Domain Code
    And User can see a text field for providing Number of Categories for the Domain

   Scenario Outline: <test description>
    Given Login as Network Admin having Permission to Add domain and category
    When User Navigate to Domain Management > Add Domain
    And provide Domain Name "<domain name>"
    And provide Domain Code "<domain code>"
    And Set the number of category "<category count>"
    And Initiate new Domain Creation
    Then User should get error result as "<result>"

    Examples:
      | domain name | domain code | category count | result | test description |
      | empty | TestDomain | empty | domain.name.is.mandatory | Verify Domain name is Mandatory |
      | TestDomain | empty | empty | domain.code.is.mandatory | Verify Domain Code is manadatory |
      | TestDomain | TestDomain | a1 | domain.num.category.not.valid | OG5-413, Verify Number of category can not be alfanumeric |
      | TestDomain | TestDomain | 0 | domain.num.category.not.valid | OG5-415, Verify Number of category can not be Zero |
      | TestDomain | TestDomain | -1 | domain.num.category.not.valid | Verify Number of category can not be Negative |
      | TestDomain | TestDomain | @$#^&* | domain.num.category.not.valid | OG5-414,Verify Number of category can not contain Special Character |

  Scenario: Create Domain with number of category
    When Try to create a new Domain with number of category as 2
    Then Domain is successfully created

  Scenario: Verify that the New categories can be added and not exceed the number of category set when creating Domain
    When trying to create categories more than the set number of category
    Then add category action must fail with error "no.more.catrgory.can.be.added"