package bdd.stepDefinations;

import com.aventstack.extentreports.ExtentTest;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.entity.Category;
import framework.entity.MFSDomain;
import framework.entity.OperatorUser;
import framework.features.categoryManagement.CategoryManagement;
import framework.features.common.Login;
import framework.features.domainCategoryManagement.DomainManagement;
import framework.pageObjects.DomainManagement.AddNew_Domain;
import framework.util.common.*;
import framework.util.globalVars.ConfigInput;
import framework.util.reportManager.ExtentManager;

public class Eco_Cash_Services_Steps {
    private static OperatorUser naAddDomain;
    private static AddNew_Domain pg1;
    private static FunctionLibrary fl;
    private static MFSDomain domain1;
    private ExtentTest tNode;

    @Before
    public void before(Scenario scenario) {
        tNode = ExtentManager.getInstance().createTest(scenario.getName());
    }

    @After
    public void after() {
        ExtentManager.extentFlush();
    }

    @Before("@First")
    @Given("^Setup required for this suite is initialized$")
    public void setup_required_for_this_suite_is_initialized() throws Throwable {
        fl = new FunctionLibrary(DriverFactory.getDriver());
        pg1 = AddNew_Domain.init(tNode);
        naAddDomain = DataFactory.getOperatorUserWithAccess("DOM_ADD");
    }

    @Given("^Login as Network Admin having Permission to Add domain and category$")
    public void login_as_Network_Admin_having_Permission_to_Add_domain_and_category() throws Throwable {
        Login.init(tNode).login(naAddDomain);
    }

    @When("^User Navigate to Domain Management > Add Domain$")
    public void user_Navigate_to_Domain_Management_Add_Domain() throws Throwable {
        pg1.navDomainManagementLink();
    }

    @Then("^User can see a text field for providing Domain Name$")
    public void user_can_see_a_text_field_for_providing_Domain_Name() throws Throwable {
        Utils.captureScreen(tNode);
        Assertion.verifyEqual(fl.elementIsDisplayed(pg1.getElementDomainName()), true,
                "Verify text box to set Domain Name is Available", tNode);
    }

    @Then("^User can see a text field for providing Domain Code$")
    public void user_can_see_a_text_field_for_providing_Domain_Code() throws Throwable {
        Utils.captureScreen(tNode);
        Assertion.verifyEqual(fl.elementIsDisplayed(pg1.getElementDomainCode()), true,
                "Verify text box to set Domain Code is Available", tNode);
    }

    @Then("^User can see a text field for providing Number of Categories for the Domain$")
    public void user_can_see_a_text_field_for_providing_Number_of_Categories_for_the_Domain() throws Throwable {
        Utils.captureScreen(tNode);
        Assertion.verifyEqual(fl.elementIsDisplayed(pg1.getElementNumOfCategory()), true,
                "Verify text box to set Number of category is Available", tNode);
    }

    @When("^provide Domain Name \"([^\"]*)\"$")
    public void provide_Domain_Name(String arg1) throws Throwable {
        if (!arg1.equals("empty")) {
            pg1.setDomainName(arg1);
        }
    }

    @When("^provide Domain Code \"([^\"]*)\"$")
    public void provide_Domain_Code(String arg1) throws Throwable {
        if (!arg1.equals("empty")) {
            pg1.setDomainCode(arg1);
        }
    }

    @When("^Set the number of category \"([^\"]*)\"$")
    public void user_provide_the_number_of_category(String arg1) throws Throwable {
        if (!arg1.equals("empty")) {
            pg1.setNumberOfCategory(arg1);
        }
    }

    @When("^Initiate new Domain Creation$")
    public void user_initiate_the_Domain_Creation() throws Throwable {
        pg1.submit();
    }

    @Then("^User should get error result as \"([^\"]*)\"$")
    public void user_should_get_result_as(String arg1) throws Throwable {
        Assertion.verifyErrorMessageContain(arg1, "Verify Error", tNode);
        Assertion.finalizeSoftAsserts();
    }

    @When("^Try to create a new Domain with number of category as (\\d+)$")
    public void try_to_create_a_new_Domain_with_number_of_category_as(int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        domain1 = new MFSDomain(arg1);
        DomainManagement.init(tNode)
                .addNewDomain(domain1);
        Assertion.finalizeSoftAsserts();
    }

    @When("^trying to create categories more than the set number of category$")
    public void trying_to_create_categories_more_than_the_set_number_of_category() throws Throwable {
        // create categories equal to num of category set in domain
        for (int i = 0; i < domain1.numCategory; i++) {
            try {
                Category newCategory = new Category();
                newCategory.setDomainDetails(domain1.domainName, domain1.domainCode);
                CategoryManagement.init(tNode).initiateNewCategory(newCategory);
                CategoryManagement.init(tNode).approveCategoryDefaultServiceForTesting();
            } catch (Exception e) {
                Assertion.markAsFailure("Failed to Add new Category");
            }
        }

        // try add another category and expect an error
        Category newCategory = new Category();
        newCategory.setDomainDetails(domain1.domainName, domain1.domainCode);
        ConfigInput.isConfirm = false;
        CategoryManagement.init(tNode)
                .initiateNewCategory(newCategory);
        ConfigInput.isConfirm = true;
    }

    @Then("^add category action must fail with error \"([^\"]*)\"$")
    public void add_category_action_must_fail_with_error(String arg1) throws Throwable {
        Assertion.verifyErrorMessageContain(arg1, "Verify no new Categories can be added to the Domain", tNode);
    }
}
