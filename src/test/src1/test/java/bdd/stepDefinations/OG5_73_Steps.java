package bdd.stepDefinations;

import com.aventstack.extentreports.ExtentTest;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.ViewBarUser_pg1;
import framework.pageObjects.userManagement.ViewBarUser_pg2;
import framework.util.common.*;
import framework.util.globalConstant.Constants;
import framework.util.reportManager.ExtentManager;
import tests.core.base.TestInit;

import java.util.HashMap;

public class OG5_73_Steps extends TestInit {
    private static OperatorUser naViewBarred;
    private static User barAsSender, barAsReceiver, barAsBoth;
    private static ViewBarUser_pg1 viewPg1;
    private static ViewBarUser_pg2 viewPg2;
    private static FunctionLibrary fl;
    private ExtentTest tNode;

    @Before
    public void before(Scenario scenario) {
        tNode = ExtentManager.getInstance().createTest(scenario.getName());
    }

    @After
    public void after() {
        ExtentManager.extentFlush();
    }

    @Before("@First")
    public void beforeFirst() throws Exception {
        try {
            naViewBarred = DataFactory.getOperatorUserWithAccess("PTY_VBLK");
            barAsSender = CommonUserManagement.init(tNode).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
            barAsReceiver = CommonUserManagement.init(tNode).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_RECIEVER, null);
            barAsBoth = CommonUserManagement.init(tNode).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_BOTH, null);
            viewPg1 = ViewBarUser_pg1.init(tNode);
            viewPg2 = ViewBarUser_pg2.init(tNode);
            fl = new FunctionLibrary(DriverFactory.getDriver());
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Given("^Login as Network Admin having Permission to View Barred Users$")
    public void login_as_Network_Admin_having_Permission_to_View_Barred_Users() throws Throwable {
        try {
            Login.init(tNode).login(naViewBarred);
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();

    }

    @When("^User Navigate to View Barred User Page$")
    public void user_Navigate_to_View_Barred_User_Page() throws Throwable {
        try {
            viewPg1.navigateToLink();
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Then("^Non Mandatory field Bar Type with date and Bar Reason with date should be available$")
    public void non_Mandatory_field_Bar_Type_with_date_and_Bar_Reason_with_date_should_be_available() throws Throwable {
        try {
            Utils.captureScreen(tNode);
            Assertion.verifyEqual(fl.elementIsDisplayed(viewPg1.getSelBarType()), true,
                    "Verify that Option to select Bar Type with Date is available", tNode);
            Assertion.verifyEqual(fl.elementIsDisplayed(viewPg1.getSelBarReason()), true,
                    "Verify that Option to select Bar Reason with Date is available", tNode);
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Then("^Verify that Bar Reason with Date Dropdown has the same set of options listed in existing search$")
    public void verifyBarReasonWithDateOptions() throws Throwable {
        try {
            Assertion.verifyListsAreEqual(viewPg1.getSelectOptionBarReasonWithDate(),viewPg1.getSelectOptionBarReason(),
                    "Verify that Bar Reason with Date Dropdown has the same set of options listed in existing search", tNode);
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Then("^Verify that Bar Type with Date Dropdown has the same set of options listed in existing search$")
    public void verifySearchDropDownOptions() throws Throwable {
        Assertion.verifyListsAreEqual(viewPg1.getSelectOptionBarTypeWithDate(),viewPg1.getSelectOptionBarType(),
                "Verify that Bar Type with Date Dropdown has the same set of options listed in existing search", tNode);
    }

    @When("^User provide the from date for new bar user search with date \"([^\"]*)\"$")
    public void user_provide_the_from_date(String arg1) throws Throwable {
        if (!arg1.equals("empty")) {
            viewPg1.setFromDate(new DateAndTime().getDate(Integer.parseInt(arg1)));
        }
    }

    @When("^User Provide the to date for new bar user search with date \"([^\"]*)\"$")
    public void user_Provide_the_to_date(String arg1) throws Throwable {
        if (!arg1.equals("empty")) {
            viewPg1.setToDate(new DateAndTime().getDate(Integer.parseInt(arg1)));
        }
    }

    @When("^User Provide bar type for new bar user search with bar type \"([^\"]*)\"$")
    public void user_Provide_bar_type(String arg1) throws Throwable {
        if (!arg1.equals("empty")) {
            viewPg1.selectBarTypeWithDate(arg1);
        }
    }

    @When("^User Provide bar reason for new bar user search with bar reason \"([^\"]*)\"$")
    public void user_Provide_bar_reason(String arg1) throws Throwable {
        if (!arg1.equals("empty")) {
            viewPg1.selectBarReasonWithDate(arg1);
        }
    }

    @When("^User initiate the barred user search$")
    public void user_initiate_the_barred_user_search() throws Throwable {
        viewPg1.clickSubmit();
    }

    @Then("^User should get result as \"([^\"]*)\"$")
    public void user_should_get_result_as(String arg1) throws Throwable {
        Utils.captureScreen(tNode);
        HashMap<String, HashMap<String, String>> data = new HashMap<>();
        // verification
        switch (arg1) {
            case "search result shows list of barred user":
                data = viewPg2.getBarredUserTable();
                Assertion.verifyEqual(data.get(barAsSender.MSISDN) != null, true, "Verify Barred User as Sender " + barAsSender.LoginId + "  is shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsReceiver.MSISDN) != null, true, "Verify Barred User as Sender " + barAsReceiver.LoginId + "  is shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsBoth.MSISDN) != null, true, "Verify Barred User as Sender " + barAsBoth.LoginId + "  is shown in the search Result", tNode);
                break;

            case "search result shows barred users with Bar Type as Sender":
                data = viewPg2.getBarredUserTable();
                Assertion.verifyEqual(data.get(barAsSender.MSISDN) != null, true, "Verify Barred User as Sender " + barAsSender.LoginId + "  is shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsReceiver.MSISDN) == null, true, "Verify Barred User as Sender " + barAsReceiver.LoginId + "  is NOT shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsBoth.MSISDN) == null, true, "Verify Barred User as Sender " + barAsBoth.LoginId + "  is NOT shown in the search Result", tNode);

                break;
            case "search result shows barred users with Bar Type as Receiver":
                // all the three barred user with different Barred type must be shown
                data = viewPg2.getBarredUserTable();
                Assertion.verifyEqual(data.get(barAsSender.MSISDN) == null, true, "Verify Barred User as Sender " + barAsSender.LoginId + "  is NOT shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsReceiver.MSISDN) != null, true, "Verify Barred User as Sender " + barAsReceiver.LoginId + "  is shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsBoth.MSISDN) == null, true, "Verify Barred User as Sender " + barAsBoth.LoginId + "  is NOT shown in the search Result", tNode);

                break;
            case "search result shows barred users with Bar Type as Both":
                // all the three barred user with different Barred type must be shown
                data = viewPg2.getBarredUserTable();
                Assertion.verifyEqual(data.get(barAsSender.MSISDN) == null, true, "Verify Barred User as Sender " + barAsSender.LoginId + "  is NOT shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsReceiver.MSISDN) == null, true, "Verify Barred User as Sender " + barAsReceiver.LoginId + "  is NOT shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsBoth.MSISDN) != null, true, "Verify Barred User as Sender " + barAsBoth.LoginId + "  is shown in the search Result", tNode);

                break;
            case "search result shows barred users with specific bar reason":
                // all the three barred user with different Barred type must be shown as all has the same bar reason
                data = viewPg2.getBarredUserTable();
                Assertion.verifyEqual(data.get(barAsSender.MSISDN) != null, true, "Verify Barred User as Sender " + barAsSender.LoginId + "  is shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsReceiver.MSISDN) != null, true, "Verify Barred User as Sender " + barAsReceiver.LoginId + "  is shown in the search Result", tNode);
                Assertion.verifyEqual(data.get(barAsBoth.MSISDN) != null, true, "Verify Barred User as Sender " + barAsBoth.LoginId + "  is shown in the search Result", tNode);

                break;
            default:
                // check for error message
                Assertion.verifyErrorMessageContain(arg1, "Verify error message", tNode);

        }
        Assertion.finalizeSoftAsserts();
    }

    @When("^User provide the mandatory search fields with date and initiate bar user search$")
    public void user_provide_the_mandatory_search_fields() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {
            viewPg1.setFromDate(new DateAndTime().getDate(-15));
            viewPg1.setToDate(new DateAndTime().getDate(0));
            viewPg1.clickSubmit();
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Then("^Login Id and MSISDN of the barred user must be shown in the updated search result page$")
    public void login_Id_and_MSISDN_of_the_barred_user_must_be_shown_in_the_updated_search_result_page() throws Throwable {
        try {
            Utils.captureScreen(tNode);
            HashMap<String, HashMap<String, String>> data = new HashMap<>();
            data = viewPg2.getBarredUserTable();
            String uiMsisdn = data.get(barAsBoth.MSISDN).get("Login Id/MSISDN");
            String barTypeUi = data.get(barAsBoth.MSISDN).get("Bar Type");
            Assertion.verifyEqual(uiMsisdn, barAsBoth.MSISDN, "Verify that Barred User LoginId/MSISDN is shown in the search result page", tNode);
            Assertion.verifyEqual(barTypeUi, "Both", "Verify that Barred User Bar Type is shown in the search result page", tNode);
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Given("^The system preference RESULT_RANGE is set to (\\d+)$")
    public void the_system_preference_RESULT_RANGE_is_set_to(int arg1) throws Throwable {
        try {
            // set the result range to low value
            SystemPreferenceManagement.init(tNode)
                    .updateSystemPreference("RESULT_RANGE", arg1 + "");
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Then("^Search results must show an error as result has exceeded the set range (\\d+)$")
    public void expectErrorMessageForRefiningSearch(int setRange) throws Throwable {
        try {

            Assertion.verifyErrorMessageContain("error.search.bareduser.refine.search",
                    "Verify that the User is asked to refine the search filters", tNode, setRange + "");
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        } finally {
            // Reset the range to default max
            SystemPreferenceManagement.init(tNode)
                    .updateSystemPreference("RESULT_RANGE", "1000");
        }
        Assertion.finalizeSoftAsserts();
    }

    @When("^User selects a barred user$")
    public void user_selects_a_barred_user() throws Throwable {
        try {
            viewPg2.selectBarredUser(barAsBoth.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @When("^clicks on Unbar$")
    public void clicks_on_Unbar() throws Throwable {
        try {
            viewPg2.clickOnUnbbar();
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @When("^clicks on Unbar Confirm$")
    public void clicks_on_Unbar_Confirm() throws Throwable {
        try {
            viewPg2.clickOnUnbbarConfirm();
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Then("^Barred User must be successfully Unbarred$")
    public void barred_User_must_be_successfully_Unbarred() throws Throwable {
        try {
            Assertion.verifyActionMessageContain("user.unbar.success", "Verify Un-Bar Channel user: " + barAsBoth.LoginId, tNode);
        } catch (Exception e) {
            markTestAsFailure(e, tNode);
        }
        Assertion.finalizeSoftAsserts();

    }


}
