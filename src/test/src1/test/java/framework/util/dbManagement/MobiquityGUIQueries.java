package framework.util.dbManagement;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import framework.dataEntity.ServiceList;
import framework.dataEntity.UserWallets;
import framework.dataEntity.UsrBalance;
import framework.entity.*;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import org.testng.Assert;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by rahul.rana on 5/6/2017.
 */
public class MobiquityGUIQueries {
    private static OracleDB dbConn;
    MobiquityGUIQueries ME;

    /**
     * Initialize Constructor
     */
    public MobiquityGUIQueries() {
        dbConn = new OracleDB();
    }

    /**
     * Get List of applicable roles for a WebRoleName
     *
     * @param webRoleName
     * @return
     */
    public static ArrayList<String> getApplicableRoleCodes(String webRoleName) {

        ArrayList<String> applicableRoles = new ArrayList<String>();
        final String query = "select ROLE_CODE from group_roles where group_role_code = '" + webRoleName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                applicableRoles.add(result.getString("ROLE_CODE").trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return applicableRoles;
    }

    /**
     * Get SuperAdmin Roles
     *
     * @return
     */
    public static ArrayList<String> getSuperAdminRoles() {
        ArrayList<String> roleNames = new ArrayList<String>();
        final String query = "select GROUP_ROLE_CODE from user_roles where User_id in(select User_id from user_roles where User_id like 'SU00%')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                roleNames.add(result.getString("GROUP_ROLE_CODE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return roleNames;
    }

    /**
     * @param webRole
     * @return
     */
    public static String getSuperAdminName(String webRole) {
        String userName = null;
        final String query = "select distinct LOGIN_ID from users where status = 'Y' and user_id in (select user_id from user_roles where group_role_code='" + webRole + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                userName = result.getString("LOGIN_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userName;
    }

    public static Multimap<String, String> getThirdPartyMandatoryFields() {
        Multimap<String, String> multimap = ArrayListMultimap.create();
        final String query = "SELECT tpm.PARTNER_NAME, tpm.FIELD_ID, src.FIELD_NAME FROM THIRD_PARTY_MANDATORY tpm, SYS_REG_CONFIG src, SYS_ENUMERATION se WHERE se.ENUM_TYPE_ID = 'PARTNER_NAME' AND se.ENUM_CODE = tpm.PARTNER_NAME AND tpm.FIELD_ID = src.FIELD_ID AND tpm.STATUS = 'Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                multimap.put(result.getString(1), result.getString(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return multimap;
    }

    public static List<String> getPreSelectedPartners(String msisdn) {
        List<String> selectedPartners = new ArrayList<>();
        String userId = getCustomerUserId(msisdn);
        String query = "SELECT tpsm.PARTNER_NAME FROM THIRD_PARTY_SUBSCRIBER_MAPPING tpsm WHERE tpsm.SUBSCRIBER_ID = '" + userId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                selectedPartners.add(result.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return selectedPartners;
    }

    public static Boolean checkIfBatchIDPresentInBulkTable(String batchId) {
        String query = "SELECT * FROM MTX_BULK_REGISTRATION WHERE BATCH_ID = '" + batchId + "'";
        try {
            ResultSet resultSet = dbConn.RunQuery(query);
            if (resultSet.next())
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean checkIfNewBulkFieldsEnteredInDB(String batchId, String[] enteredValues) {
        String query = "SELECT IS_AUTOSWEEP_ALLOWED, CONTACT_ID_TYPE, ALLOWED_DAYS, FROM_TIME, TO_TIME, COMMERCIAL_FIELD_1, " +
                "COMMERCIAL_FIELD_2, COMMERCIAL_FIELD_3, COMMERCIAL_FIELD_4, COMMERCIAL_FIELD_5, COMMERCIAL_FIELD_6," +
                "COMMERCIAL_FIELD_7, COMMERCIAL_FIELD_8, COMMERCIAL_FIELD_9, COMMERCIAL_FIELD_10, COMMERCIAL_FIELD_11, " +
                "SHOP_NAME FROM MTX_BULK_REGISTRATION WHERE BATCH_ID = '" + batchId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            int columnCount = result.getMetaData().getColumnCount(), i;
            result.next();
            for (i = 1; i <= columnCount; i++) {
                if (!result.getString(i).equalsIgnoreCase(enteredValues[i - 1]))
                    return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String getStatusOfLiquidationBank(String msisdn) {
        String status = null;
        final String query = "select status from liquidation_frequency where user_id in(select user_id from users where msisdn='" + msisdn + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                status = result.getString("STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public static String getStatusOfLiquidationBankForBiller(String billerCode) {
        String status = null;
        final String query = "select status from liquidation_frequency where user_id in(select user_id from users where user_code='" + billerCode + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                status = result.getString("STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    /**
     * Get Registration Types
     *
     * @return
     */
    public static ResultSet getRegistrationType() {
        ResultSet result = null;
        final String query = "select ENUM_ID, DESCRIPTION from SYS_ENUMERATION where ENUM_TYPE_ID = 'KYC_MODE'";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Get Subscriber Commission Rule ID
     *
     * @return
     */
    public static String getSubscriberCommissionRuleId() {
        String id = null;
        final String query = "select RULE_ID from MTX_JOINING_COMM_RULE where rownum <= 1 and applicable_from<= sysdate";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("RULE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public static String getSubscriberCommissionRuleName(String ruleId) {
        String name = null;
        String query = "select comm_rule_name from mtx_joining_comm_rule where rule_id = '" + ruleId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                name = result.getString("COMM_RULE_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    /**
     * @param provider
     * @return
     */
    public static String fetchDefaultBankOfProvider(String provider) {
        ResultSet result = null;
        final String query = "select distinct bank_id from sys_service_pymt_subtype_map where payment_method_type_id='BANK' and is_default='Y' " +
                "and provider_id = '" + provider + "'";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("bank_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
    function to get the agent code stored in db
     */
    public static List<String> fetchAgentCodeForChannelUsers(String msisdnArr[]) {
        ResultSet result = null;
        List<String> agentCodeArr = new ArrayList<String>();
        String param = "(";
        for (String msisdn : msisdnArr) {
            param = param.concat(msisdn + ",");
        }
        param = param.substring(0, param.length() - 1);
        param = param + ")";

        String query = "select AGENT_CODE from users " +
                "where status = 'Y' " +
                "and MSISDN in " + param + ";";

        try {
            result = dbConn.RunQuery(query);

            while (result.next()) {
                agentCodeArr.add(result.getString("AGENT_CODE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return agentCodeArr;
    }

    /**
     * Get Account Details
     *
     * @param user
     * @param providerId
     * @param bankId
     * @return
     */
    public static Map<String, String> dbGetAccountDetails(User user, String providerId, String bankId) {
        ResultSet result = null;
        Map<String, String> map = new HashMap<String, String>();
        String userTable = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? "mtx_party" : "users";
        final String query = "Select ACCOUNT_NO, CUST_ID,ACCOUNT_TYPE from MBK_CUST_ACCOUNTS where USER_ID in " +
                "(select USER_ID from " + userTable + " where MSISDN = '" + user.MSISDN + "') " +
                "and STATUS_ID = 'Y' and PROVIDER_ID = '" + providerId + "' and USER_GRADE='" + user.GradeCode + "' " +
                "and BANK_ID = '" + bankId + "' order by ACCOUNT_NO DESC";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                map.put("ACCOUNT_NO", result.getString("ACCOUNT_NO"));
                map.put("CUST_ID", result.getString("CUST_ID"));
                map.put("ACCOUNT_TYPE", result.getString("ACCOUNT_TYPE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }

    /**
     * Get System wallet balance
     *
     * @param providerCode
     * @param systemId
     * @return
     */
    public static BigDecimal getBalanceSystemWallet(String providerCode, String systemId) {
        BigDecimal res = null;
        final String query = "select sum(Balance) from MTX_WALLET_BALANCES where wallet_number in (select wallet_number from mtx_wallet where user_id ='" + systemId + "' and provider_id='" + providerCode + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = new BigDecimal(result.getString("sum(balance)")).divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Get Bank ID
     *
     * @param bankName
     * @return
     */
    public static String getBankId(String bankName) {
        String Sresult = null;
        final String query = "Select BANK_ID from MBK_BANK_DETAILS where BANK_NAME = '" + bankName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                Sresult = result.getString("BANK_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Sresult;
    }


    /**
     * Fetch the default value of the preference code
     *
     * @param prefCode
     * @return
     */
    public static String fetchDefaultValueOfPreference(String prefCode) {
        String query = "Select default_value from mtx_system_preferences where preference_code='" + prefCode + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("default_value");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * fetch User from DB
     *
     * @param categoryCode - Category Code of the User
     * @param gradeName    -  Gradename of the User
     * @return
     */
    public static ResultSet fetchNonOptUsers(String categoryCode, String gradeName, int index) {
        ResultSet result = null;
        String table1 = (categoryCode.equals("SUBS")) ? "mtx_party" : "users";
        String table2 = (categoryCode.equals("SUBS")) ? "mtx_party_access" : "user_phones";
        String table3 = (categoryCode.equals("SUBS")) ? "mtx_party_m" : "users_m";

        List<String> specialWallets = DataFactory.getSpecialWallet();
        String wWallet = "";
        for (String walletName : specialWallets) {
            wWallet = wWallet + "'" + DataFactory.getWalletId(walletName) + "',";
        }

        wWallet = wWallet.substring(0, wWallet.length() - 1);

        String query = "SELECT DISTINCT u.USER_NAME,u.LAST_NAME,u.LOGIN_ID,u.MSISDN,u.EXTERNAL_CODE "
                + "FROM " + table1 + " u, " + table2 + " mb, mtx_categories mc,channel_grades chg,mtx_wallet mw, " + table3 + " um "
                + "WHERE u.CATEGORY_CODE='" + categoryCode + "' AND chg.GRADE_NAME = '" + gradeName + "' "
                + "AND u.category_code = mc.category_code "
                + "AND u.category_code = chg.category_code "
                + "AND mw.USER_GRADE = chg.GRADE_CODE and mb.mobile_banking = 'Y' "
                + "AND u.status = 'Y' AND mw.msisdn = u.msisdn AND mw.payment_type_id not in (" + wWallet + ") "
                + "AND um.STATUS not in ('DI')";

        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ResultSet fetchOptUsers(String categoryCode, String groupRoleName) {
        ResultSet result = null;

        String query = "select distinct CONTACT_NO, USER_NAME, LAST_NAME, LOGIN_ID, MSISDN, EXTERNAL_CODE from users " +
                "where status = 'Y' " +
                "and user_id in (select user_id from user_roles where group_role_code='" + groupRoleName + "')";

        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean checkMsisdnExistMobiquity(String msisdn) {
        boolean b_result = false;
        String res = null;
        String query = "select msisdn from users where MSISDN ='" + msisdn + "' union select msisdn from mtx_party where MSISDN ='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MSISDN");

                //check the res
                if (res != null) {
                    if (res.equals(msisdn)) {
                        b_result = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return b_result;
    }

    public static boolean checkMsisdnExistInSubscribers(String msisdn) {
        boolean b_result = false;
        String res = null;
        String query = "select msisdn from mtx_party where MSISDN ='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MSISDN");

                //check the res
                if (res != null) {
                    if (res.equals(msisdn)) {
                        b_result = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return b_result;
    }

    public static boolean checkMsisdnExistInUsers(String msisdn) {
        boolean b_result = false;
        String res = null;
        String query = "select msisdn from users where MSISDN ='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MSISDN");

                //check the res
                if (res != null) {
                    if (res.equals(msisdn)) {
                        b_result = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b_result;
    }

    public static String dbGetTransactionStatus(String txnID) {
        final String query = "select TRANSFER_STATUS from MTX_TRANSACTION_HEADER  where TRANSFER_ID = '" + txnID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("TRANSFER_STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * getAllExistMobiquity
     *
     * @return
     */
    public static List<String> getAllExistMobiquity() {
        List<String> allMsisdnPresentDB = new ArrayList<String>();
        final String query = "select msisdn from users union select msisdn from mtx_party";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                allMsisdnPresentDB.add(result.getString("MSISDN"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allMsisdnPresentDB;
    }

    /**
     * @param startMSISDN
     * @param endMSISDN
     * @return
     */
    public static List<String> getAllMSISDNBetRange(String startMSISDN, String endMSISDN) {
        List<String> allMsisdn = new ArrayList<String>();
        final String query = "select msisdn from users where msisdn between '" + startMSISDN + "' and '" + endMSISDN + "' union " +
                "select msisdn from mtx_party where msisdn between '" + startMSISDN + "' and '" + endMSISDN + "' union " +
                "select msisdn from users_m where msisdn between '" + startMSISDN + "' and '" + endMSISDN + "' union " +
                "select msisdn from mtx_party_m where msisdn between '" + startMSISDN + "' and '" + endMSISDN + "' union " +
                "select msisdn from mtx_party_customer_blacklist where msisdn is not null";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                allMsisdn.add(result.getString("MSISDN"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allMsisdn;
    }

    /**
     * @param value
     * @param t
     * @throws Exception
     */
    public static void updateRetryIntervalForThirdParty(String value, ExtentTest t) throws Exception {
        ResultSet result = null, resultCheck = null;
        final String queryCheck = "select * from THIRD_PARTY_PROPERTIES";
        final String query = "UPDATE THIRD_PARTY_PROPERTIES SET RETRY_INTERVAL = '" + value + "'";
        try {
            resultCheck = dbConn.RunQuery(queryCheck);
            if (resultCheck.next()) {
                String retry_interval = resultCheck.getString("RETRY_INTERVAL");
                if (retry_interval.compareToIgnoreCase("1") == 0) {
                    t.pass("RETRY_INTERVAL already equal to 1");
                    return;
                } else {
                    result = dbConn.RunQuery(query);
                    if (result.next()) {
                        t.pass("Updated THIRD_PARTY_PROPERTIES successfully");
                        return;
                    }
                }
            }
        } catch (Exception e) {
            t.fail("couldn't update THIRD_PARTY_PROPERTIES");
            Assertion.raiseExceptionAndStop(e, t);
        }
    }

    public static String getSavingClubId(String clubName) throws Exception {
        final String query = "select CLUB_ID from SVC_SAVING_CLUB where CLUB_NAME = '" + clubName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("CLUB_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static String getRelationshipOfficer(String msisdn) {
        String res = null;
        String query = "select RELATIONSHIP from USERS WHERE MSISDN='" + msisdn + "'";
        System.out.print("Got query " + query);
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("LOGIN_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * This will return the fic,frozenand available balance from the MTX_WALLET_BALANCES
     *
     * @return
     */
    public static ResultSet dbGetAllNetworkStock(String walletName) {
        final String query = "select FIC,FROZEN_AMOUNT,BALANCE from MTX_WALLET_BALANCES where wallet_number='" + walletName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get Role Code Page Code Map
     *
     * @return
     */
    public static ResultSet fetchRoleCodePageCodeMap() {
        ResultSet result = null;
        final String query = "select ROLE_CODE, PAGE_CODE from page_roles";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Only for debugging purpose, get role code from DB by passing the paege code
     *
     * @param pageCode
     * @return
     */
    public static String getRoleCode(String pageCode) {
        ResultSet result = null;
        final String query = "select ROLE_CODE from group_roles where role_code in (select role_code from page_roles where page_code = '" + pageCode + "')";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("ROLE_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static UsrBalance getUserBalance(User user, String walletId, String provider) {
        BigDecimal balance, ficBalance, frozenBalance;

        String userTable = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? "mtx_party" : "users";
        walletId = (walletId == null) ? DataFactory.getDefaultWallet().WalletId : walletId;
        String providerId = (provider == null) ? DataFactory.getDefaultProvider().ProviderId : DataFactory.getProviderId(provider);
        final String query = "SELECT mwb.BALANCE, mwb.FIC,mwb.FROZEN_AMOUNT FROM mtx_wallet_balances MWB, mtx_wallet mw  " +
                "WHERE mw.msisdn ='" + user.MSISDN + "' and MW.user_id in(select user_id from " + userTable + " where status != 'N') " +
                "and mw.wallet_number=mwb.wallet_number and mw.payment_type_id='" + walletId + "' " +
                "and mw.provider_id ='" + providerId + "' and mw.user_grade ='" + user.GradeCode + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                balance = new BigDecimal(result.getString("BALANCE"));
                ficBalance = new BigDecimal(result.getString("FIC"));
                frozenBalance = new BigDecimal(result.getString("FROZEN_AMOUNT"));
                if (balance == null) {
                    balance = BigDecimal.valueOf(0);
                }
                return new UsrBalance(balance, ficBalance, frozenBalance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static UsrBalance fetchPrePostBalance(String txnID, String userType) {
        BigDecimal preBalance, postBalance;

        final String query = "select previous_balance, post_balance from mtx_transaction_items" +
                " where transfer_id = '" + txnID + "' and user_type = '" + userType + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                preBalance = new BigDecimal(result.getString("previous_balance"));
                postBalance = new BigDecimal(result.getString("post_balance"));
                return new UsrBalance(preBalance, postBalance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static UsrBalance getUserBalanceAfterAccountIsClosed(User user, String walletId, String provider) {
        BigDecimal balance = BigDecimal.valueOf(0);
        BigDecimal ficBalance = BigDecimal.valueOf(0);
        BigDecimal frozenBalance = BigDecimal.valueOf(0);

        String userTable = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? "mtx_party" : "users";
        walletId = (walletId == null) ? DataFactory.getDefaultWallet().WalletId : walletId;
        String providerId = (provider == null) ? DataFactory.getDefaultProvider().ProviderId : DataFactory.getProviderId(provider);
        final String query = "SELECT mwb.BALANCE, mwb.FIC,mwb.FROZEN_AMOUNT FROM mtx_wallet_balances MWB, mtx_wallet mw  " +
                "WHERE mw.msisdn ='" + user.MSISDN + "' and MW.user_id in(select user_id from " + userTable + " where status = 'N') " +
                "and mw.wallet_number=mwb.wallet_number and mw.payment_type_id='" + walletId + "' " +
                "and mw.provider_id='" + providerId + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                balance = new BigDecimal(result.getString("BALANCE"));
                ficBalance = new BigDecimal(result.getString("FIC"));
                frozenBalance = new BigDecimal(result.getString("FROZEN_AMOUNT"));
                if (balance == null) {
                    balance = BigDecimal.valueOf(0);
                }
                return new UsrBalance(balance, ficBalance, frozenBalance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultSet fetchDetailsForKycModeAndGradeCode(String kycMode, String gradeCode) throws SQLException {
        final String query = "select * from KYC_PREFERENCES where KYC_MODE = '" + kycMode + "' AND GRADE_CODE = '" + gradeCode + "' ";
        try {
            return dbConn.RunQuery(query);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultSet fetchDetailsOfWalletsForUser(String msisdn) throws SQLException {
        final String query = "select * from mtx_wallet where msisdn = '" + msisdn + "'";
        try {
            return dbConn.RunQuery(query);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultSet fetch_MTX_AMBIGUOUS_TXN(String transferId) {
        ResultSet result = null;
        final String query = "select TRANSFER_STATUS from MTX_AMBIGUOUS_TXN where TRANSFER_ID = '" + transferId + "'";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ResultSet fetch_MTX_AMBIGUOUS_TXN_DETAILS(String transferId) {
        ResultSet result = null;
        final String query = "select TRANSFER_STATUS, IS_PROCESSED from MTX_AMBIGUOUS_TXN_DETAILS where TRANSFER_ID = '" + transferId + "'";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getDefaultBankIdforProvider(String providerId) {
        ResultSet result = null;
        final String query = "select distinct bank_id from sys_service_pymt_subtype_map where payment_method_type_id='BANK' and is_default='Y' " +
                "and provider_id = '" + providerId + "'";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("bank_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fetchLastTransactionID(String MSISDN) {
        ResultSet result = null;
        final String query = "select last_transfer_id from mtx_wallet w, mtx_wallet_balances wb where msisdn='" + MSISDN + "' and w.wallet_number=wb.wallet_number and rownum ='1'";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("last_transfer_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String fetchUserID(String txnID) {
        ResultSet result = null;
        final String query = "select payee_user_id from mtx_transaction_header where transfer_id='" + txnID + "'";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("payee_user_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fetchProfile_id(String user_id) {
        ResultSet result = null;
        final String query = "select mpay_profile_id from mtx_wallet where user_id='" + user_id + "' and provider_id=101 and payment_type_id=12";
        ;
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("mpay_profile_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fetchPayeeCount(String userID, String profileID, String serviceType) {
        ResultSet result = null;
        final String query = "select payee_count from threshold_count where PROFILE_ID='" + profileID + "' " +
                "and SERVICE_TYPE='" + serviceType + "'and USER_ID='" + userID + "'and BEARER_ID='WEB' and group_id='3'";
        ;
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("payee_count");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fetchTransactionStatus(String txnID) {
        ResultSet result = null;
        final String query = "select transfer_status from mtx_transaction_header where transfer_id='" + txnID + "'";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("transfer_status");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // select transfer_status from mtx_transaction_header where transfer_id='011812041018060308'; 
    public static ResultSet updateDisplayAndModifiedAllowedFromGUI(String prefCode, String value) {
        ResultSet result = null;
        final String query = "update MTX_SYSTEM_PREFERENCES set MODIFIED_ALLOWED = '" + value + "', DISPLAY_ALLOWED = '" + value + "' where PREFERENCE_CODE = '" + prefCode + "'";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ResultSet fetchTCPIdAndMobileGroupRoleCode(String gradeCode, String payId, String paymentType) {
        ResultSet result = null;
        String idDesc = (paymentType.equalsIgnoreCase("Wallet") ? "payment_type_id" : "bank_id");

        String query = "select mtcp.profile_id, sgr.group_role_code from mtx_trf_cntrl_profile mtcp, sys_group_roles sgr where mtcp.grade_code=sgr.grade_code " +
                "and mtcp.grade_code='" + gradeCode + "' and mtcp.payment_method_type_id_desc='" + payId + "' and mtcp.payment_method_type_id_desc = sgr." + idDesc;

        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Update SVCGradePreferencesTable
     *
     * @param gradeCode
     * @param bankId
     * @throws IOException
     * @throws SQLException
     */
    public static String dbUpdateSVCGradePreferencesTable(String gradeCode, String bankId) throws IOException, SQLException {
        //Fetching TCP ID & Mobile Group Role Code to insert in SVC_GRADE_PREFERENCES table
        String tcpId1 = null, roleCode1 = null, tcpId2 = null, roleCode2 = null;

        ResultSet result1 = fetchTCPIdAndMobileGroupRoleCode(gradeCode, "18", "WALLET");
        while (result1.next()) {
            tcpId1 = result1.getString("PROFILE_ID");
            roleCode1 = result1.getString("GROUP_ROLE_CODE"); // wallet
        }

        ResultSet result2 = fetchTCPIdAndMobileGroupRoleCode(gradeCode, bankId, "BANK");
        while (result2 != null && result2.next()) {
            tcpId2 = result2.getString("PROFILE_ID");
            roleCode2 = result2.getString("GROUP_ROLE_CODE"); // bank
        }

       /*
        Check if Db entry is already present
        If for a specific grade the combination exist then update else insert into db Table
         */
        final String query1 = "select * from svc_grade_preferences where grade_code= '" + gradeCode + "'";
        ResultSet result3 = dbConn.RunQuery(query1);

        String query2 = null;
        if (result3.next()) {
            // update existing
            query2 = "update SVC_GRADE_PREFERENCES set TCP_ID='" + tcpId1 +
                    "', GROUP_ROLE_CODE='" + roleCode1 +
                    "', BANK_TCP_ID='" + tcpId2 + "', BANK_GRP_ROLE_CODE='" + roleCode2 +
                    "' where GRADE_CODE='" + gradeCode + "'";

        } else {
            // insert new
            query2 = "Insert into SVC_GRADE_PREFERENCES (GRADE_CODE,TCP_ID,GROUP_ROLE_CODE,BANK_TCP_ID,BANK_GRP_ROLE_CODE) values " +
                    "('" + gradeCode + "','" + tcpId1 + "','" + roleCode1 + "','" + tcpId2 + "','" + roleCode2 + "')";
        }

        // Execute the query
        try {
            dbConn.RunQuery(query2);
            dbConn.RunQuery("commit");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Failed to execute Query");
        }

        return query2;

    }

    /**
     * Update SVCGradePreferencesTable
     *
     * @param categoryCode
     * @param bankId
     * @throws IOException
     * @throws SQLException
     */
    public static String dbUpdateSVCGradePreferencesTable(String categoryCode, String gradeCode, String bankId) throws IOException, SQLException {
        //Fetching TCP ID & Mobile Group Role Code to insert in SVC_GRADE_PREFERENCES table
        String tcpId1 = null, roleCode1 = null, tcpId2 = null, roleCode2 = null;

        ResultSet result1 = fetchTCPIdAndMobileGroupRoleCode(categoryCode, "18", "WALLET");
        while (result1 != null && result1.next()) {
            tcpId1 = result1.getString("PROFILE_ID");
            roleCode1 = result1.getString("GROUP_ROLE_CODE"); // wallet
        }

        ResultSet result2 = fetchTCPIdAndMobileGroupRoleCode(categoryCode, bankId, "BANK");
        while (result2 != null && result2.next()) {
            tcpId2 = result2.getString("PROFILE_ID");
            roleCode2 = result2.getString("GROUP_ROLE_CODE"); // bank
        }

        /*
        Check if Db entry is already present
        If for a specific grade the combination exist then update else insert into db Table
         */
        final String query1 = "select * from svc_grade_preferences where grade_code= '" + gradeCode + "'";
        ResultSet result3 = dbConn.RunQuery(query1);

        String query2 = null;
        if (result3.next()) {
            // update existing
            query2 = "update SVC_GRADE_PREFERENCES set TCP_ID='" + tcpId1 +
                    "', GROUP_ROLE_CODE='" + roleCode1 +
                    "', BANK_TCP_ID='" + tcpId2 + "', BANK_GRP_ROLE_CODE='" + roleCode2 +
                    "' where GRADE_CODE='" + gradeCode + "'";

        } else {
            // insert new
            query2 = "Insert into SVC_GRADE_PREFERENCES (GRADE_CODE,TCP_ID,GROUP_ROLE_CODE,BANK_TCP_ID,BANK_GRP_ROLE_CODE) values " +
                    "('" + gradeCode + "','" + tcpId1 + "','" + roleCode1 + "','" + tcpId2 + "','" + roleCode2 + "')";
        }

        // Execute the query
        try {
            dbConn.RunQuery(query2);
            dbConn.RunQuery("commit");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Failed to execute Query");
        }
        return query2;
    }

    /**
     * The below method will fetch Mobile Role
     *
     * @param catCode
     * @param gradeCode
     * @return
     */
    public static String fetchMobileRole(String catCode, String gradeCode) {
        String groupRoleId = null;
        final String query = "select GROUP_ROLE_CODE from SYS_GROUP_ROLES where CATEGORY_CODE = '" + catCode + "'" +
                " AND GROUP_ROLE_TYPE  = 'WALLET' AND GRADE_CODE='" + gradeCode + "'" +
                " AND PROVIDER_ID='101' AND PAYMENT_TYPE_ID='12' AND STATUS ='Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                groupRoleId = result.getString("GROUP_ROLE_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return groupRoleId;
    }

    public static String fetchWebGroupRole(String catCode) {
        String groupRoleId = null;
        final String query = "select GROUP_ROLE_CODE from SYS_GROUP_ROLES where CATEGORY_CODE = '" + catCode + "'" +
                " AND GROUP_ROLE_TYPE = 'WEB' AND PROVIDER_ID='101' AND PAYMENT_TYPE_ID='12' AND STATUS ='Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                groupRoleId = result.getString("GROUP_ROLE_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return groupRoleId;
    }

    /**
     * Get List of All mobile roles starting from Automation prefix - 'Mob'
     *
     * @return
     */
    public static List<MobileGroupRole> fetchAvailableMobileGroupRoles() {
        List<MobileGroupRole> mobileRolesDB = new ArrayList<>();

        final String query = "select category_code, \n" +
                "grade_code, " +
                "group_role_name, " +
                "group_role_code, " +
                "group_role_type, " +
                "provider_id, " +
                "bank_id, " +
                "payment_type_id " +
                "from sys_Group_roles " +
                "where group_role_type != 'WEB' " +
                "and group_role_name like '" + Constants.MOBILE_ROLE_PREFIX + "%' " +
                "and status = 'Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                String catCode = result.getString("category_code");
                String gradeCode = result.getString("grade_code");
                String roleName = result.getString("group_role_name");
                String roleCode = result.getString("group_role_code");
                String payInstrument = result.getString("group_role_type");
                String providerId = result.getString("provider_id");
                String bankId = result.getString("bank_id");
                String walletId = result.getString("payment_type_id");

                // Skip, if Bank is not mentioned in ConfigInput
                if(bankId!= null && DataFactory.getBankName(bankId) == null)
                    continue;

                // Skip, if Grade is not mentioned in ConfigInput
                if(DataFactory.getGradeName(gradeCode) == null)
                    continue;

                // Skip, if Wallet is not mentioned in ConfigInput
                if(walletId!= null && DataFactory.getWalletName(walletId) == null)
                    continue;

                MobileGroupRole tRole = new MobileGroupRole(catCode, gradeCode, roleName, roleCode, payInstrument, providerId, bankId, walletId);
                mobileRolesDB.add(tRole);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mobileRolesDB;
    }

    public static String fetchGeographyDomain() {
        String geoDomain = null;
        final String query = "select GRPH_DOMAIN_CODE from GEOGRAPHICAL_DOMAINS where status ='Y' and GRPH_DOMAIN_TYPE='AR'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                geoDomain = result.getString("GRPH_DOMAIN_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return geoDomain;
    }

    /**
     * @param catCode
     * @param gradeCode
     * @return
     */
    public static String fetchTCPId(String catCode, String gradeCode) {
        String profileID = null;
        final String query = "select PROFILE_ID from MTX_TRF_CNTRL_PROFILE where CATEGORY_CODE = '" + catCode + "' AND GRADE_CODE='" + gradeCode + "'" +
                "AND PAYMENT_METHOD_TYPE_ID='WALLET' AND PROVIDER_ID='101' AND PAYMENT_METHOD_TYPE_ID_DESC='12' AND STATUS_ID='Y' AND ROWNUM=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                profileID = result.getString("PROFILE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return profileID;
    }

    /*
     * verify MSISDN from Db if it matches with new Subscriber added.
     * */
    public static String getSubscriberLoginId(String msisdn) {
        String res = null;
        String query = "select LOGIN_ID from MTX_PARTY WHERE MSISDN='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("LOGIN_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String dbFetchResetPassWordFromEmailQueue(String msisdn) {
        String res = null;
        final String query = "select * from mtx_email_queue eq,users u where u.msisdn='" + msisdn + "' and u.email=eq.to_addr and eq.subject like '%Reset%'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("SENSITIVE_INFO");
            }

            System.out.println(res);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    /**
     * Get Customer ID
     *
     * @param msisdn
     * @return
     */
    public static String getCustomerUserId(String msisdn) {
        String res = null;
        String query = "select USER_ID from MTX_PARTY where MSISDN = '" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("USER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getBillerUserId(String loginId) {
        String res = null;
        String query = "select USER_ID from USERS WHERE LOGIN_ID='" + loginId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("USER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static BigDecimal getChurnBalance(String providerId) {
        BigDecimal res = null;
        String query = "select sum(balance) from mtx_wallet_balances where wallet_number like '%" + providerId + "IND10%'";
        System.out.print("Got query " + query);
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("SUM(BALANCE)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static BigDecimal getReconBalance() {
        BigDecimal res = null;
        String query = "select sum(balance) from mtx_wallet_balances";
        System.out.print("Got query " + query);
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("SUM(BALANCE)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String fetchPayerPayeeSMSreq(String requestType) {
        String s1 = null;
        final String query = "select IS_PAYER_SUCCESS_SMS_REQ,\n" +
                "IS_PAYEE_SUCCESS_SMS_REQ,\n" +
                "IS_PAYER_FAIL_SMS_REQ,\n" +
                "IS_PAYEE_FAIL_SMS_REQ from SYS_Service_PYMT_MAP where REQUEST_TYPE = '" + requestType + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                s1 = result.getString("IS_PAYER_SUCCESS_SMS_REQ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s1;
    }

    public static ResultSet updatePayerPayeeSMS_REQ(String requestType, String value) {
        ResultSet result = null;
        final String query = "update SYS_Service_PYMT_MAP set IS_PAYER_SUCCESS_SMS_REQ = '" + value +
                "', IS_PAYEE_SUCCESS_SMS_REQ = '" + value + "', IS_PAYER_FAIL_SMS_REQ = '" + value +
                "', IS_PAYEE_FAIL_SMS_REQ = '" + value + "' where  REQUEST_TYPE = '" + requestType + "'";

        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String fetchDisplayAllowedOnGUI(String prefCode) {
        String s1 = null;
        //ResultSet result;
        final String query = "select DISPLAY_ALLOWED from MTX_SYSTEM_PREFERENCES where PREFERENCE_CODE = '" + prefCode + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                s1 = result.getString("DISPLAY_ALLOWED");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return s1;
    }

    public static String fetchmodifyAllowedFromGUI(String prefCode) {
        String s1 = null;
        // result;
        final String query = "select MODIFIED_ALLOWED from MTX_SYSTEM_PREFERENCES where PREFERENCE_CODE = '" + prefCode + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                s1 = result.getString("MODIFIED_ALLOWED");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return s1;
    }


    /**
     * To fetch DISPLAY_ALLOWED from GUI is allowed or Not
     *
     * @param prefCode
     * @return
     */
    public static String fetchDisplayAllowedFromGUI(String prefCode) {
        String s1 = null;
        final String query = "select DISPLAY_ALLOWED from MTX_SYSTEM_PREFERENCES where PREFERENCE_CODE = '" + prefCode + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                s1 = result.getString("DISPLAY_ALLOWED");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return s1;
    }

    public static ResultSet updateModifyAllowed(String prefCode, String value) {
        ResultSet result = null;
        final String query = "update MTX_SYSTEM_PREFERENCES set MODIFIED_ALLOWED = '" + value + "' where PREFERENCE_CODE = '" + prefCode + "'";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getOperatorId() {
        String res = null;
        String query = "select * from MBK_OPERATORS where STATUS='Y' and rownum=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("OPERATOR_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getOperatorName(String operatorId) {
        String res = null;
        String query = "select operator_name from MBK_OPERATORS where operator_id='" + operatorId + "'";
        System.out.print("Got query " + query);
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("OPERATOR_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static BigDecimal fetchOperatorBalance(String provideId, String walletId) throws SQLException {
        String res = null;
        final String query = "select sum(balance/(SELECT DEFAULT_VALUE FROM MTX_SYSTEM_PREFERENCES WHERE PREFERENCE_CODE = 'CURRENCY_FACTOR')) " +
                "as OPTBALANCE FROM mtx_wallet_balances where wallet_number = '" + provideId + "" + walletId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("OPTBALANCE");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (res != null) {
            return new BigDecimal(res);
        } else {
            return null;
        }
    }

    public static BigDecimal fetchOperatorBalance(String walletNum) throws SQLException {
        String res = null;
        final String query = "select sum(balance/(SELECT DEFAULT_VALUE FROM MTX_SYSTEM_PREFERENCES WHERE PREFERENCE_CODE = 'CURRENCY_FACTOR')) " +
                "as OPTBALANCE FROM mtx_wallet_balances where wallet_number = '" + walletNum + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("OPTBALANCE");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (res != null) {
            return new BigDecimal(res);
        } else {
            return null;
        }
    }

    public static BigDecimal getLoyaltyBalance(String provideId, String walletId) throws SQLException {
        String res = null;
        final String query = "select balance from mtx_loyalty_wallet_balances where wallet_number='" + provideId + "" + walletId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("BALANCE");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (res != null) {
            return new BigDecimal(res);
        } else {
            return null;
        }

    }

    public static UsrBalance getBillerBalance(String billerCode) throws SQLException {
        String res = null;
        BigDecimal balance = BigDecimal.valueOf(0), ficBalance = BigDecimal.valueOf(0), frozenBalance = BigDecimal.valueOf(0);
        final String query = "SELECT mwb.BALANCE, mwb.FIC,mwb.FROZEN_AMOUNT FROM mtx_wallet_balances MWB, mtx_wallet mw " +
                "WHERE MW.user_id in (select user_id from users where user_code ='" + billerCode + "') and " +
                "mw.wallet_number=mwb.wallet_number";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                balance = new BigDecimal(result.getString("BALANCE"));
                ficBalance = new BigDecimal(result.getString("FIC"));
                frozenBalance = new BigDecimal(result.getString("FROZEN_AMOUNT"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UsrBalance(balance, ficBalance, frozenBalance);
    }

    public static String getBillerDbStatus(String billerCode) {
        String SR = null;
        final String query = "select status from users where user_code ='" + billerCode + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                SR = result.getString("status");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return SR;
    }

    public static String dbGetServiceRequestId(String TRANSFER_ID) {
        String SR = null;
        final String query = "select service_request_id from mtx_transaction_header where transfer_id='" + TRANSFER_ID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                SR = result.getString("SERVICE_REQUEST_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return SR;
    }

    public static String getExternalReferenceId(String TRANSFER_ID) {
        String externalRefId = null;
        final String query = "select ftxn_id from mtx_transaction_header where transfer_id='" + TRANSFER_ID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                externalRefId = result.getString("FTXN_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return externalRefId;
    }

    public static String getSCincludedStatusupdated(String servicetype) throws Exception {
        final String query = "select is_sc_included_in_req_amount from sys_service_pymt_map where service_type='" + servicetype + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("is_sc_included_in_req_amount");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static BigDecimal fetchOperatorBalanceinTotal(String provideId, String walletId) throws SQLException {
        String res = null;
        final String query = "select sum(balance/(SELECT DEFAULT_VALUE FROM MTX_SYSTEM_PREFERENCES WHERE PREFERENCE_CODE = 'CURRENCY_FACTOR')) as balance " +
                "FROM mtx_wallet_balances where wallet_number = '" + provideId + "" + walletId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("balance");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (res != null) {
            return new BigDecimal(res);
        } else {
            return null;
        }

    }

    public static BigDecimal fetchOperatorBalanceinTotalwithFrozenAmt(String provideId, String walletId) throws SQLException {
        String res = null, res1 = null;
        float total = 0;
        final String query = "select sum(balance/(SELECT DEFAULT_VALUE FROM MTX_SYSTEM_PREFERENCES WHERE PREFERENCE_CODE = 'CURRENCY_FACTOR')),sum(frozen_amount/(SELECT DEFAULT_VALUE FROM MTX_SYSTEM_PREFERENCES WHERE PREFERENCE_CODE = 'CURRENCY_FACTOR')) " +
                "FROM mtx_wallet_balances where wallet_number = '" + provideId + "" + walletId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("SUM(BALANCE/(SELECTDEFAULT_VALUEFROMMTX_SYSTEM_PREFERENCESWHEREPREFERENCE_CODE=:\"SYS_B_0\"))");
                res1 = result.getString("SUM(FROZEN_AMOUNT/(SELECTDEFAULT_VALUEFROMMTX_SYSTEM_PREFERENCESWHEREPREFERENCE_CODE=:\"SYS_B_1\"))");
                total = Float.valueOf(res) - Float.valueOf(res1);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (res != null) {
            return new BigDecimal(total);
        } else {
            return null;
        }

    }

    public static ResultSet updateDefaultValueOfPreference(String preferenceCode, String value) {
        ResultSet result = null;
        final String query = "mtx_system_preferences set default_value = '" + value +
                "' where  PREFERENCE_CODE = '" + preferenceCode + "'";

        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Get the Min Transfer Value of Service Charge for the enterprise
     *
     * @param serviceChargeName
     * @return
     */
    public static BigDecimal getMinTransferValue(String serviceChargeName) {
        BigDecimal minValue = null;
        String query = "select MIN_TRANSFER_VALUE from mtx_service_charge where SERVICE_CHARGE_NAME ='" + serviceChargeName + "'";
        System.out.print("Got query " + query);
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                minValue = result.getBigDecimal("MIN_TRANSFER_VALUE");
                minValue = minValue.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return minValue;
    }

    /**
     * getMaxTransferValue
     *
     * @param serviceChargeName
     * @return
     */
    public static Map<String, String> getNFSC_Amount(String serviceChargeName, boolean... commission) {
        Map<String, String> map = new HashMap<>();

        String sc = null, cm = null;
        String query = "select SERVICE_CHARGE_NONFIN,COMM_TELESCOPIC_CHARGING from mtx_service_charge where SERVICE_CHARGE_NAME ='" + serviceChargeName + "'  and STATUS_ID='Y' ORDER BY LAST_VERSION ASC";
        System.out.print("Got query " + query);
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                BigDecimal minValue = result.getBigDecimal("SERVICE_CHARGE_NONFIN");
                if (minValue == null) {
                    minValue = minValue.divide(AppConfig.currencyFactor);
                    sc = "" + minValue;
                } else {
                    sc = "0";
                }


                minValue = result.getBigDecimal("COMM_TELESCOPIC_CHARGING");
                minValue = minValue.divide(AppConfig.currencyFactor);
                cm = "" + minValue;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("SERVICE_CHARGE_NONFIN", sc);
        map.put("COMM_TELESCOPIC_CHARGING", cm);
        return map;
    }

    public static BigDecimal getMaxTransferValue(String serviceChargeName) {
        BigDecimal maxValue = null;
        String query = "select MAX_TRANSFER_VALUE from mtx_service_charge where SERVICE_CHARGE_NAME ='" + serviceChargeName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                maxValue = result.getBigDecimal("MAX_TRANSFER_VALUE");
                maxValue = maxValue.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxValue;
    }

    /**
     * Method to fetch All TCP amount
     *
     * @param categoryCode
     * @param gradeCode
     * @return
     * @throws Exception
     */
    public static BigDecimal fetchMinTCPAmount(String categoryCode, String gradeCode) throws Exception {
        String dProvider = DataFactory.getDefaultProvider().ProviderId;

        BigDecimal minValue = null, maxValue;
        String query = "select MIN_TXN_AMT from MTX_TRF_CNTRL_PROFILE WHERE CATEGORY_CODE = '" + categoryCode + "' " +
                "and GRADE_CODE = '" + gradeCode + "' and PAYMENT_METHOD_TYPE_ID = 'WALLET' and PROVIDER_ID ='" + dProvider + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                minValue = result.getBigDecimal("MIN_TXN_AMT");
                minValue = minValue.divide(AppConfig.currencyFactor);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return minValue;
    }

    /**
     * Method to fetch Profile ID from MTX_TRF_CNTRL_PROFILE table
     *
     * @param user
     * @param providerID
     * @param walletID
     * @return
     */
    public static String getTCPProfileID(User user, String providerID, String walletID) {
        String profileID = null;
        final String query = "select PROFILE_ID from MTX_TRF_CNTRL_PROFILE where CATEGORY_CODE = '" + user.CategoryCode + "' " +
                "and GRADE_CODE = '" + user.GradeCode + "' and PAYMENT_METHOD_TYPE_ID_DESC = '" + walletID + "' and PROVIDER_ID ='" + providerID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                profileID = result.getString("PROFILE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return profileID;
    }

    public static String getAgentCode(String msisdn) {
        String agentCode = null;
        final String query = "select AGENT_CODE from users where MSISDN = '" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                agentCode = result.getString("AGENT_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return agentCode;
    }

    public static String getRechargeMerchantCode() {
        String agentCode = null;
        final String query = "select OPERATOR_NAME from MBK_OPERATORS";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                agentCode = result.getString("OPERATOR_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return agentCode;
    }

    /**
     * This method will return the last batch payout Transaction ID
     *
     * @return
     */
    public static String getCurrencyCode(String providerID) {
        String res = null;
        String query = "select currency_code from sys_service_provider where provider_id='" + providerID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("CURRENCY_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getUserStatus(String msisdn, String categoryCode) {

        String res = null;
        String query;
        if (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            query = "select status from mtx_party where msisdn='" + msisdn + "'";
        } else
            query = "select status from users where msisdn='" + msisdn + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static boolean isUserActiveAndNotBlackListed(String msisdn, String categoryCode) throws IOException, SQLException {

        String res = null;
        String query;
        String tableName = (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) ? "mtx_party" : "users";
        query = "select t1.status from " + tableName + " t1 " +
                "where t1.status = 'Y' " +
                "and t1.msisdn = '" + msisdn + "' " +
                "and t1.user_id not in (select distinct party_id from mtx_party_black_list)";

        ResultSet result = dbConn.RunQuery(query);
        while (result.next()) {
            res = result.getString("STATUS");
        }

        if (res != null)
            return true;
        else
            return false;
    }

    /**
     * Get User currentDB Status
     *
     * @param user
     * @param field
     * @return
     */
    public static String getUserCurrentDBStatus(User user, String field) {

        String res = null;
        String query;
        if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            query = "select " + field + " from mtx_party where msisdn='" + user.MSISDN + "'";
        } else
            query = "select " + field + " from users where msisdn='" + user.MSISDN + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString(field);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Get User currentDB Status
     *
     * @param biller
     * @param field
     * @return
     */
    public static String getBillerCurrentDBStatus(Biller biller, String field) {

        String res = null;
        String query;
        query = "select " + field + " from users where login_id='" + biller.LoginId + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString(field);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getUserDeleteStatus(String msisdn, String categoryCode) {

        String res = null;
        String query;
        if (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            query = "select status from mtx_party_m where msisdn='" + msisdn + "'";
        } else
            query = "select status from users_m where msisdn='" + msisdn + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * get User bar Status
     *
     * @param loginId
     * @return
     */
    public static String getUserBarStatus(String loginId) {
        String res = null;
        String query = "select black_list_type_id from mtx_party_black_list where party_name='" + loginId + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("black_list_type_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * get User bar Status
     *
     * @param user
     * @return
     */
    public static String getUserChurnStatus(User user) {
        String tableName = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? "mtx_party" : "users";
        String res = null;
        String query = "select status from " + tableName + " where LOGIN_ID ='" + user.LoginId + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("status");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getChurnUserSettledStatus(String msisdn) {
        String res = null;
        String query = "select is_settled from MTX_CHURN_USERS where msisdn = '" + msisdn + "' and status = 'TS'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("is_settled");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getKYCId(String msisdn, String categoryCode) throws NullPointerException {

        String kycId = null;
        String query;
        if (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            query = "select id_no from mtx_party where msisdn='" + msisdn + "'";
        } else
            query = "select id_no from users where msisdn='" + msisdn + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                kycId = result.getString("ID_NO");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return kycId;
    }

    /**
     * Check if atleast a users password is updated once
     *
     * @param loginId
     * @return
     */
    public static boolean isUserPasswordUpdatedAtleastOnce(String loginId) {
        String res = null;
        String query = "select pswd_modified_on from users where login_id = '" + loginId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("pswd_modified_on");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (res != null)
            return true;
        else
            return false;
    }

    /**
     * Check if Liquidation Banks
     *
     * @return
     */

    public static List<String> getListOfBankForLiquidation() {
        List<String> bank = new ArrayList<String>();
        final String query = "select bank_name from mbk_bank_details where bank_type='LIQUIDATION'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                bank.add(result.getString("BANK_NAME"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bank;
    }

    public static String getWalletNumber(String input) {
        final String query = "select wallet_number from mtx_wallet where user_id='" + input + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("WALLET_NUMBER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * The function will fetch User ID
     * from Users table or Mtx_Party table based on the Category Code
     *
     * @param msisdn
     * @param categoryCode
     * @return
     * @throws NullPointerException
     */
    public static String getUserID(String msisdn, String categoryCode) throws NullPointerException {

        String uid = null;
        String query;
        if (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            query = "select USER_ID from mtx_party where msisdn='" + msisdn + "'";
        } else
            query = "select USER_ID from users where msisdn='" + msisdn + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                uid = result.getString("USER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uid;
    }

    /**
     * Get Automation Zone and Area () Geography Object
     *
     * @return Object Geography
     */
    public static Geography getAutomationZoneAndArea() {
        String query1 = "select PARENT_GRPH_DOMAIN_CODE, GRPH_DOMAIN_NAME, GRPH_DOMAIN_SHORT_NAME, GRPH_DOMAIN_CODE, DESCRIPTION from GEOGRAPHICAL_DOMAINS " +
                "where PARENT_GRPH_DOMAIN_CODE in (select PARENT_GRPH_DOMAIN_CODE from GEOGRAPHICAL_DOMAINS " +
                "where GRPH_DOMAIN_NAME like 'AUTAREA%' and STATUS = 'Y' and GRPH_DOMAIN_TYPE='AR') and STATUS = 'Y' and ROWNUM = 1";
        String parentCode = null, areaName = null, areaCode = null, areaShortName = null, areaDescription = null,
                zoneName = null, zoneShortName = null, zoneCode = null, zoneDescription = null;
        try {
            ResultSet result = dbConn.RunQuery(query1);
            while (result.next()) {
                areaName = result.getString("GRPH_DOMAIN_NAME");
                areaCode = result.getString("GRPH_DOMAIN_CODE");
                areaShortName = result.getString("GRPH_DOMAIN_SHORT_NAME");
                areaDescription = result.getString("DESCRIPTION");
                parentCode = result.getString("PARENT_GRPH_DOMAIN_CODE");
            }

            if (parentCode == null) {
                return null;
            } else {
                String query2 = "select GRPH_DOMAIN_NAME, GRPH_DOMAIN_NAME, GRPH_DOMAIN_SHORT_NAME, GRPH_DOMAIN_CODE, DESCRIPTION from GEOGRAPHICAL_DOMAINS \n" +
                        "where GRPH_DOMAIN_CODE = '" + parentCode + "'";
                ResultSet result1 = dbConn.RunQuery(query2);
                while (result1.next()) {
                    zoneName = result1.getString("GRPH_DOMAIN_NAME");
                    zoneCode = result1.getString("GRPH_DOMAIN_CODE");
                    zoneShortName = result1.getString("GRPH_DOMAIN_SHORT_NAME");
                    zoneDescription = result1.getString("DESCRIPTION");
                }

                if (zoneName == null) {
                    return null;
                } else {
                    return new Geography(areaName, areaCode, areaShortName, areaDescription, zoneName, zoneCode, zoneShortName, zoneDescription);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUserUserId(String msisdn) {
        String res = null;
        String query = "select USER_ID from USERS WHERE MSISDN='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("USER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * getChannelUserCode
     *
     * @param msisdn
     * @return
     * @throws NullPointerException
     */
    public static String getChannelUserCode(String msisdn) throws NullPointerException {

        String userCode = null;
        String query = "select USER_CODE from users where msisdn='" + msisdn + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                userCode = result.getString("USER_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userCode;
    }

    /**
     * Method to Fetch Created On, Modified On and Last Login Details from users Table
     *
     * @param MSISDN
     * @return
     */
    public static ResultSet fetchDetailsForGlobalSearch(String MSISDN) {
        ResultSet result = null;
        final String query = "select CREATED_ON,MODIFIED_ON,LAST_LOGIN_ON from users where MSISDN = '" + MSISDN + "'";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to fetch USER ID from users table
     *
     * @param msisdn
     * @return
     */
    public static String fetchUserIDFromUsers(String msisdn) {
        String userID = null;
        final String query = "select user_id from users where msisdn = '" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                userID = result.getString("user_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userID;
    }

    /**
     * Method to
     *
     * @param msisdn
     * @return
     */
    public static String getLastTransactionOnFromTxnHeader(String msisdn) {
        String userID = fetchUserIDFromUsers(msisdn);
        String lastTxnOn = null;
        final String query = "select TRANSFER_ON from mtx_transaction_header where (PAYER_USER_ID = 'PT180514.1232.016626' or PAYEE_USER_ID = '" + userID + "') and SERVICE_TYPE in ('C2C','O2C') and TRANSFER_STATUS = 'TS'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                lastTxnOn = result.getString("TRANSFER_ON");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastTxnOn;
    }

    public static BigDecimal getSlabRangeFromServiceCharge(ServiceCharge serviceCharge, String serviceType) throws NullPointerException {

        BigDecimal slabEndRange = null;
        String query = "select end_range from  MTX_SERVICE_CHARGE_RANGE " +
                "where  SERVICE_CHARGE_RANGE_ID  in (select SERVICE_CHARGE_RANGE_ID from  MTX_SERV_CHRG_RANGE_DETAILS " +
                "where SERVICE_CHARGE_ID in  (SELECT msc.SERVICE_CHARGE_ID " +
                "FROM mtx_service_charge msc,mtx_service_charge_version mscv " +
                "WHERE status_id = 'Y' " +
                "AND service_type ='" + serviceType + "' " +
                "AND payer_grade_code ='" + serviceCharge.Payer.GradeCode + "' " +
                "AND payee_grade_code ='" + serviceCharge.Payee.GradeCode + "' " +
                "AND payer_provider_id='" + DataFactory.getDefaultProvider().ProviderId + "' " +
                "AND payee_provider_id='" + DataFactory.getDefaultProvider().ProviderId + "' " +
                "AND payer_payment_type_id='" + serviceCharge.Payer.PaymentTypeID + "' " +
                "AND payee_payment_type_id='" + serviceCharge.Payee.PaymentTypeID + "' " +
                "and msc.SERVICE_CHARGE_ID = mscv.SERVICE_CHARGE_ID " +
                "AND mscv.SERVICE_CHARGE_VERSION IN (" +
                "SELECT MAX (SERVICE_CHARGE_VERSION) FROM mtx_service_charge msc,mtx_service_charge_version mscv " +
                "WHERE status_id = 'Y'AND service_type ='" + serviceType + "' " +
                "AND payer_grade_code ='" + serviceCharge.Payer.GradeCode + "' " +
                "AND payee_grade_code ='" + serviceCharge.Payee.GradeCode + "' " +
                "AND payer_provider_id='" + DataFactory.getDefaultProvider().ProviderId + "' " +
                "AND payee_provider_id='" + DataFactory.getDefaultProvider().ProviderId + "' " +
                "AND payer_payment_type_id='" + serviceCharge.Payer.PaymentTypeID + "' " +
                "AND payee_payment_type_id='" + serviceCharge.Payer.PaymentTypeID + "' " +
                "and msc.SERVICE_CHARGE_ID=mscv.SERVICE_CHARGE_ID))) and slab_code=0";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                slabEndRange = result.getBigDecimal("end_range");
                slabEndRange = slabEndRange.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return slabEndRange;
    }

    public static Map<String, String> fetchInitiatedCategoryDetails() throws IOException, SQLException {
        Map<String, String> map = new HashMap<>();
        ResultSet result;

        String query1 = "select DOMAIN_CODE,CATEGORY_CODE,PARENT_CATEGORY_CODE from MTX_CATEGORIES where STATUS='AI'";

        result = dbConn.RunQuery(query1);
        while (result.next()) {
            map.put("DOMAIN_CODE", result.getString("DOMAIN_CODE"));
            map.put("CATEGORY_CODE", result.getString("CATEGORY_CODE"));
            map.put("PARENT_CATEGORY_CODE", result.getString("PARENT_CATEGORY_CODE"));
        }
        return map;
    }

    /**
     * Method to fetch Parent category code.
     *
     * @param domainCode
     * @return
     */
    public static String fetchParentCategoryCode(String domainCode) {

        String res = null;
        String query = "select PARENT_CATEGORY_CODE from mtx_categories where DOMAIN_CODE ='" + domainCode + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("PARENT_CATEGORY_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String GuiModifyIsNotAllowed() {
        String s1 = null;
        // result;
        final String query = "select PREFERENCE_CODE from MTX_SYSTEM_PREFERENCES where  MODIFIED_ALLOWED = 'N' and DISPLAY_ALLOWED='Y' ";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                s1 = result.getString("PREFERENCE_CODE");
                return s1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return s1;
    }


    /**
     * Generic method to execute query
     * Pass query and column name , this will return the required query output data
     *
     * @param query
     * @param columnLabel
     * @return
     * @throws NullPointerException
     */
    public static String executeQueryAndReturnResult(String query, String columnLabel) throws NullPointerException {

        String queryOutput = null;

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                queryOutput = result.getString(columnLabel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queryOutput;
    }


    /**
     * Query updated by Navin for fetching unique msisdn for In-active. Earlier it was fetching msisdn with status 'Y' also
     * @return
     * @throws NullPointerException
     */
    public static String getinActiveMsisdn() throws NullPointerException {

        final String query = "select MSISDN from users where msisdn in " +
                "(select MSISDN from users group by MSISDN having count (status)=1) and status='N' " +
                "union " +
                "select MSISDN from mtx_party where msisdn in " +
                "(select MSISDN from mtx_party group by MSISDN having count (status)=1) and status='N'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("MSISDN");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static List<String> getAllActiveMsisdnFromMtxParty() throws NullPointerException {

        List<String> msisdns = new ArrayList<>();
        final String query = "select msisdn from MTX_PARTY where status = 'Y' union select msisdn from MTX_PARTY_M";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                msisdns.add(result.getString("MSISDN"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msisdns;
    }

    public static List<String> getAllActiveMsisdnFromUsers(String userType) throws NullPointerException {

        List<String> msisdns = new ArrayList<>();
        final String query = "select msisdn from USERS where USER_TYPE ='" + userType + "' and status = 'Y'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                msisdns.add(result.getString("MSISDN"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msisdns;
    }

    //Added for Bulk Registration to fetch TCP and Mobile Role

    public static String getMessageFromSysMessages(String messageCode, String langCode) throws NullPointerException {

        String message = null;
        final String query = "select MESSAGE from SYS_MESSAGES where MESSAGE_CODE ='" + messageCode + "' and LANGUAGE_CODE = '" + langCode + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                message = result.getString("MESSAGE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;

    }

    /**
     * Method to fetch All Services Related to A specific Wallet.
     *
     * @param walletTypeID
     * @return
     */
    public static List<String> getAllServicesAgainstWallet(String walletTypeID, String paymentInstrument) {
        List<String> services = new ArrayList<>();
        String query = "select DESCRIPTION from SYS_SERVICE_PYMT_MAP where SERVICE_TYPE in (select SERVICE_TYPE from SYS_SERVICE_PYMT_SUBTYPE_MAP where PAYMENT_TYPE_ID ='" + walletTypeID + "') and INITIATOR_PAYMENT_INSTRUMENT = '" + paymentInstrument + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                services.add(result.getString("DESCRIPTION"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return services;
    }

    public static String getLastTransactionID(String serviceType) {
        String res = null;
        String query = "select TRANSFER_ID from mtx_transaction_header where transfer_status = 'TS' and SERVICE_TYPE ='" + serviceType + "' order by transfer_on desc";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("TRANSFER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getLastTransactionIDUsingMSISDN(String serviceType, String msisdn, String... txnStatus) {
        String status = null;
        if (txnStatus.length == 0) {
            status = "TS";
        } else {
            status = txnStatus[0];
        }
        String res = null;
        String query = "select TRANSFER_ID from (select TRANSFER_ID from mtx_transaction_items where transfer_status = '" + status + "' and SERVICE_TYPE ='" + serviceType + "' and ACCOUNT_ID = '" + msisdn + "' order by transfer_on desc) where rownum = 1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("TRANSFER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static Map<String, String> getTransferIDandTransferStatus(String serviceType, String msisdn) {
        ResultSet result = null;
        Map<String, String> map = new HashMap<String, String>();
        final String query1 = "select TRANSFER_ID, TRANSFER_STATUS from (select TRANSFER_ID, transfer_status from mtx_transaction_items where SERVICE_TYPE ='" + serviceType + "' and ACCOUNT_ID = '" + msisdn + "' order by transfer_on desc) where rownum = 1";

        try {
            result = dbConn.RunQuery(query1);

            if (result.next()) {
                map.put("TRANSFER_ID", result.getString("TRANSFER_ID"));
                map.put("TRANSFER_STATUS", result.getString("TRANSFER_STATUS"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public static String getBlackListedUser(User usr) {
        String res = null;
        String query = "select FIRST_NAME from MTX_PARTY_CUSTOMER_BLACKLIST where FIRST_NAME = '" + usr.FirstName + "' and LAST_NAME ='" + usr.LastName + "' and DATE_OF_BIRTH = TO_DATE('" + usr.DateOfBirth + "', 'MM/DD/YYYY')";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("FIRST_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;

    }

    public static Date getDateOfBirth(User user) {
        Date res = null;
        String query = "select DOB from mtx_party where msisdn ='" + user.MSISDN + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getDate("DOB");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;

    }

    public static String getDOB(User user) {
        String res = null;
        String query = "select DOB from mtx_party where msisdn = '" + user.MSISDN + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("DOB");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;

    }

    public static ResultSet getDOB(String MSISDN) {
        ResultSet result = null;
        System.out.println("MSISDN is >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + MSISDN);
        String query = "select DOB from mtx_party where msisdn = '" + MSISDN + "'";
        //String query = "select PARENT_CATEGORY_CODE from mtx_categories where DOMAIN_CODE ='DISTWS'";
        try {
            System.out.println("QUERY IS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + query);
            result = dbConn.RunQueryPreparedStatement(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String dbGetUserStatusFromMtx_Party_M(String msisdn) {
        String status = null;
        String query = "select STATUS from mtx_party_m where msisdn='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                status = result.getString("STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    public static String getUserTCPName(User user) {
        String query = null, res = null;
        if (user.CategoryCode.equals(Constants.SUBSCRIBER)) {
            query = "select PROFILE_NAME from MTX_TRF_CNTRL_PROFILE where PROFILE_ID = (select MPAY_PROFILE_ID from mtx_wallet where USER_ID = (select USER_ID from mtx_party where msisdn ='" + user.MSISDN + "') and IS_PRIMARY = 'Y')";
        } else {
            query = "select PROFILE_NAME from MTX_TRF_CNTRL_PROFILE where PROFILE_ID = (select MPAY_PROFILE_ID from mtx_wallet where USER_ID = (select USER_ID from users where msisdn ='" + user.MSISDN + "') and IS_PRIMARY = 'Y')";
        }

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("PROFILE_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getUserTCPID(User user, String providerID, String walletID) {
        String query = null, res = null;
        if (user.CategoryCode.equals(Constants.SUBSCRIBER)) {
            query = "select MPAY_PROFILE_ID from mtx_wallet where USER_ID = (select USER_ID from mtx_party where msisdn ='" + user.MSISDN + "') and IS_PRIMARY = 'Y'";
        } else {
            query = "select MPAY_PROFILE_ID from mtx_wallet where USER_ID = (select USER_ID from users where msisdn ='" + user.MSISDN + "') " +
                    "and IS_PRIMARY = 'Y' and USER_GRADE ='" + user.GradeCode + "' and PROVIDER_ID = '" + providerID + "' and PAYMENT_TYPE_ID ='" + walletID + "'";
        }

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MPAY_PROFILE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * This method will fetch the Message from Two Tables i.e
     * SENTSMS and SMS_DELIVERED
     * it first check in SENTSMS if msg is present then it will return otherwise
     * it wl go and fetch form SMS_DELIVERED
     *
     * @param msisdn
     * @param reference
     * @return
     */
    public static String getMobiquityUserMessage(String msisdn, String orderBy, String... reference) throws Exception {
        if(!ConfigInput.dbMsgVerificationRequired)
            return null;

        Thread.sleep(Constants.MAX_WAIT_TIME);

        String userMsg = null;

        if (reference.length > 0) {
            userMsg = getMessageFromSentSMS(msisdn, orderBy, reference);
        } else {
            userMsg = getMessageFromSentSMS(msisdn, orderBy);
        }

        if (userMsg == null) {
            if (reference.length > 0)
                userMsg = getMessageFromSMSDelivered(msisdn, orderBy, reference);
            else
                userMsg = getMessageFromSMSDelivered(msisdn, orderBy);
        }
        try {
            userMsg = new DesEncryptor().decrypt(userMsg);
        } catch (MoneyException e) {
            e.printStackTrace();
        }
        return userMsg;
    }

    public static String getMessageFromSentSMS(String msisdn, String orderBy, String... reference) {
        String res = null;
        String query;

        if (reference.length > 0) {
            query = "select MESSAGE from (select MESSAGE from SENTSMS  where MSISDN ='" + msisdn + "' and reference = '" + reference[0] + "' order by deliveredon " + orderBy + ") where rownum=1";
        } else {
            query = "select MESSAGE from (select MESSAGE from SENTSMS  where MSISDN ='" + msisdn + "' order by deliveredon " + orderBy + ") where rownum=1";
        }

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MESSAGE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * @param msisdn
     * @param orderBy   --> Pass ASC or DESC
     * @param reference
     * @return
     */
    public static String getMessageFromSMSDelivered(String msisdn, String orderBy, String... reference) {
        String res = null;
        String query;

        if (reference.length > 0) {
            query = "select MESSAGE from (select MESSAGE from SMS_DELIVERED  where MSISDN ='" + msisdn + "' and reference = '" + reference[0] + "' order by DELIVEREDON " + orderBy + ") where rownum=1";
        } else {
            query = "select MESSAGE from (select MESSAGE from SMS_DELIVERED  where MSISDN ='" + msisdn + "' order by DELIVEREDON " + orderBy + ") where rownum=1";
        }

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MESSAGE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static List<String> getAllMessageFromSMSdelivered(String msisdn, String orderBy, String... reference) {
        ResultSet result = null;
        List<String> msg = new ArrayList<>();
        String query;

        if (reference.length > 0) {
            query = "select MESSAGE from (select MESSAGE from SMS_DELIVERED  where MSISDN ='" + msisdn + "' and reference = '" + reference[0] + "' order by DELIVEREDON " + orderBy + ")";
        } else {
            query = "select MESSAGE from (select MESSAGE from SMS_DELIVERED  where MSISDN ='" + msisdn + "' order by DELIVEREDON " + orderBy + ")";
        }

        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                msg.add(result.getString("MESSAGE"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }

    public static List<String> getAllMessageFromSentSMS(String msisdn, String orderBy, String... reference) {
        ResultSet result = null;
        List<String> msg = new ArrayList<>();
        String query;

        if (reference.length > 0) {
            query = "select MESSAGE from (select MESSAGE from SENTSMS  where MSISDN ='" + msisdn + "' and reference = '" + reference[0] + "' order by deliveredon " + orderBy + ")";
        } else {
            query = "select MESSAGE from (select MESSAGE from SENTSMS  where MSISDN ='" + msisdn + "' order by deliveredon " + orderBy + ")";
        }

        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                msg.add(result.getString("MESSAGE"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;
    }

    public static List<String> getMobiquityUserMessageList(String msisdn, String orderBy, String... reference) {
        List<String> userMsg = null;

        if (reference.length > 0) {
            userMsg = getAllMessageFromSentSMS(msisdn, orderBy, reference);
        } else {
            userMsg = getAllMessageFromSentSMS(msisdn, orderBy);
        }

        //if list is '0', no msgs in "SentSMS" table, hence search in "SMSdelevered" table
        if (userMsg.size() == 0) {
            if (reference.length > 0)
                userMsg = getAllMessageFromSMSdelivered(msisdn, orderBy, reference);
            else
                userMsg = getAllMessageFromSMSdelivered(msisdn, orderBy);
        }

        return userMsg;
    }

    public static List<String> dbGetBankNamesMappedWithServices(String providerID) {
        List<String> bankNames = new ArrayList<>();
        String query = "select BANK_NAME from MBK_BANK_DETAILS where BANK_ID in " +
                "(select distinct(BANK_ID) from SYS_SERVICE_PYMT_SUBTYPE_MAP where PAYMENT_METHOD_TYPE_ID ='BANK' and PROVIDER_ID='" + providerID + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                bankNames.add(result.getString("BANK_NAME"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bankNames;
    }

    /**
     * Fetch User Status when user is Initiated.
     *
     * @param msisdn
     * @param category
     * @return
     */
    public static String dbGetUserInitiatedStatus(String msisdn, String category) {
        String status = null, query;
        if (!category.equalsIgnoreCase(Constants.SUBSCRIBER))
            query = "select STATUS from users_m where msisdn='" + msisdn + "'";
        else
            query = "select STATUS from mtx_party_m where msisdn='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                status = result.getString("STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    public static String getEmployeeStatus(String manager_id) {
        String emp_status = null, query;

        query = "select emp_status from mtx_employee where manager_id='" + manager_id + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                emp_status = result.getString("EMP_STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return emp_status;
    }

    /**
     * Method to Get Bank Names
     *
     * @param providerId
     * @return
     */
    public static List<String> getLinkedBankNames(String providerId) {
        List<String> bankNames = new ArrayList<>();
        String query = "select BANK_NAME from MBK_BANK_DETAILS where provider_id='" + providerId + "' and POOL_ACCOUNT_TYPE='02'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                bankNames.add(result.getString("BANK_NAME"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bankNames;
    }

    /**
     * Get Liquidation Id
     *
     * @param msisdn
     * @return
     */
    public static String getLiquidationId(String msisdn, String batchId) {
        String LiquidationId = null;
        final String query = "select liq_transaction_id from liquidation_details where user_id in (select user_id from users where msisdn='" + msisdn + "') and batch_id='" + batchId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                LiquidationId = result.getString("liq_transaction_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return LiquidationId;
    }

    public static List<String> getAllWallet() {

        List<String> wallets = new ArrayList<>();

        String query = "select SUBTYPE_NAME from SYS_PAYMENT_METHOD_SUBTYPES where STATUS = 'Y' and PAYMENT_METHOD_TYPE_ID ='WALLET'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                wallets.add(result.getString("SUBTYPE_NAME"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wallets;
    }


    public static String getFrozenBalanceOfUser(User user) {
        final String query = "select FROZEN_AMOUNT from MTX_WALLET_BALANCES where wallet_number in (select WALLET_NUMBER from MTX_WALLET where msisdn='" + user.MSISDN + "' and status='Y' and PAYMENT_TYPE_ID='12')";

        BigDecimal bal = new BigDecimal("0");
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                bal = result.getBigDecimal("FROZEN_AMOUNT");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bal.divide(AppConfig.currencyFactor).toString();
    }

    /**
     * Get Liquidation Id For Biller
     *
     * @param billerCode
     * @return
     */
    public static String getLiquidationIdForBiller(String billerCode, String batchId) {
        String LiquidationId = null;
        final String query = "select liq_transaction_id from liquidation_details where user_id in (select user_id from users where USER_CODE='" + billerCode + "') and batch_id='" + batchId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                LiquidationId = result.getString("liq_transaction_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return LiquidationId;
    }

    /**
     * This will return the fic,frozenand available balance from the MTX_WALLET_BALANCES
     *
     * @return UsrBalance
     */
    public static UsrBalance dbGetAllNetworkStock1(String walletName) {
        BigDecimal balance = BigDecimal.valueOf(0), ficBalance = BigDecimal.valueOf(0), frozenBalance = BigDecimal.valueOf(0);
        final String query = "select sum(balance), sum(fic),sum(frozen_amount) from MTX_WALLET_BALANCES where wallet_number='" + walletName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                balance = new BigDecimal(result.getString("SUM(BALANCE)"));
                ficBalance = new BigDecimal(result.getString("SUM(FIC)"));
                frozenBalance = new BigDecimal(result.getString("SUM(FROZEN_AMOUNT)"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UsrBalance(balance, ficBalance, frozenBalance);
    }

    public static List<String> getListOfBankForSpecificProvider(String providerId, String reqCoulmn, String bankType) {
        List<String> bank = new ArrayList<String>();
        final String query = "select " + reqCoulmn + " from mbk_bank_details where provider_id='" + providerId + "' and bank_pool_account_no IS NOT NULL and bank_type in (" + bankType + ")";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                if (reqCoulmn.equalsIgnoreCase("bank_name")) {
                    bank.add(result.getString("BANK_NAME"));
                } else {
                    bank.add(result.getString("BANK_ID"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bank;
    }

    public static List<String> getListOfBanks(String providerId, String reqCoulmn, String bankType) {
        List<String> bank = new ArrayList<String>();
        final String query = "select " + reqCoulmn + " from mbk_bank_details where provider_id='" + providerId + "' and bank_type in (" + bankType + ")";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                if (reqCoulmn.equalsIgnoreCase("bank_name")) {
                    bank.add(result.getString("BANK_NAME"));
                } else {
                    bank.add(result.getString("BANK_ID"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bank;
    }

    public static String getUserId(String loginId) {
        String res = null;
        String query = "select USER_ID from USERS WHERE LOGIN_ID='" + loginId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("USER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * @return
     */
    public static ArrayList<String> getServiceTypeFromThresholdCount(String msisdn) {
        ArrayList<String> serviceType = new ArrayList<String>();
        final String query = "select distinct(service_type) from threshold_count where user_id in(select user_id from users where msisdn='" + msisdn + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);

            while (result.next()) {
                serviceType.add(result.getString("service_type"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serviceType;
    }

    public static ArrayList<String> getBearerTypeFromThresholdCount(String msisdn) {
        ArrayList<String> bearerId = new ArrayList<String>();
        final String query = "select distinct(bearer_id) from threshold_count where user_id in(select user_id from users where msisdn='" + msisdn + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);

            while (result.next()) {
                bearerId.add(result.getString("bearer_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bearerId;
    }

    public static Map<String, String> dbGetExtensionValue(String msisdn, String serviceType, String frequency, String bearer) throws IOException, SQLException {

        ResultSet result;
        Map<String, String> map = new HashMap<>();

        String query1 = "select ext_status,ext_amount_multiplier,ext_count_multiplier " +
                "from threshold_count where user_id in(select user_id from users where msisdn='" + msisdn + "') and service_type='" + serviceType + "' and group_id='" + frequency + "' and bearer_id='" + bearer + "'";

        result = dbConn.RunQuery(query1);

        while (result.next()) {
            if (result.getString("ext_status").equals("TS")) {
                map.put("AMOUNT", result.getString("ext_amount_multiplier"));
                map.put("COUNT", result.getString("ext_count_multiplier"));
                map.put("Status", result.getString("ext_status"));
            }
        }
        return map;
    }

    /**
     * @param user
     * @param msisdn
     * @return
     */
    public static String fetchUserType(User user, String msisdn) {

        // user's table is selected based on the user passed as an argument
        String userTable = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? "mtx_party" : "users";

        String query = "Select user_type from " + userTable + " where msisdn='" + user.MSISDN + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("user_type");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * fetch all the subscriber associated with same External_code
     *
     * @return
     */
    public static List<String> fetchAllSubscriberMsisdn(String external_code) {
        List<String> subList = new ArrayList<>();
        String query = "Select MSISDN from MTX_PARTY where external_code = '" + external_code + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                subList.add(result.getString("MSISDN"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subList;
    }

    /***
     *
     * @return
     */
    public static List<String> fetchListOfCurrencyCode() {
        List<String> subList = new ArrayList<>();
        String query = "Select CURRENCY_CODE from sys_service_provider ";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                subList.add(result.getString("CURRENCY_CODE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subList;
    }

    /**
     * @return
     */
    public static Map<String, String> fetchUserWhoIsBothSubsAndChanlUser() {

        Map<String, String> map = new HashMap<String, String>();

        final String query = "select a.msisdn, a.user_type from users a,mtx_party b where a.msisdn=b.msisdn and rownum=1";

        try {
            ResultSet result = dbConn.RunQuery(query);

            if (result.next()) {
                map.put("MSISDN", result.getString("MSISDN"));
                map.put("UserType", result.getString("USER_TYPE"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * @return
     */
    public static Map<String, String> fetchSIMSwappedStatus(User user, String MSISDN) {

        String userTable = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? "mtx_party" : "users";

        ResultSet result = null;
        Map<String, String> map = new HashMap<String, String>();

        final String query = "select SIM_SWAPPED, SIM_SWAPPED_ON from " + userTable + " where msisdn='" + MSISDN + "'";

        try {
            result = dbConn.RunQuery(query);
            if (result.next()) {
                map.put("SIM_Swapped", result.getString("SIM_SWAPPED"));
                map.put("SIM_SwappedOn", result.getString("SIM_SWAPPED_ON"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public static String getDefaultCurrencyCode(User user) {

        // user's table is selected based on the user passed as an argument
        String userTable = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? "mtx_party" : "users";

        String query = "Select default_currency_code from " + userTable + " where msisdn ='" + user.MSISDN + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("default_currency_code");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getProviderIdAssociatedWithCurrency(String currencyCode) {

        String query = "select provider_ID from sys_service_provider where currency_code = '" + currencyCode + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("provider_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getProofType() {

        String query = "Select ENUM_CODE from SYS_ENUMERATION where ENUM_TYPE_ID='PROOF_TYPE' and rownum=1";


        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("ENUM_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBankAccountStatus(String custID) {
        String res = null;
        String query = "select status_id from mbk_cust_accounts where cust_id='" + custID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("STATUS_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getBankAccountStatusWithLoginId(String loginId) {
        String res = null;
        String query = "select status_id from mbk_cust_accounts where user_id in(select user_id from users where login_id='" + loginId + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("STATUS_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static List<String> fetchStatus() {
        List<String> subList = new ArrayList<>();
        String query = "select status from MBK_OPERATORS";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                subList.add(result.getString("status"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subList;
    }


    public static String fetchStatus(User user) {
        String res = null;
        String userTable = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? "mtx_party" : "users";
        String query = "select status from " + userTable + " where MSISDN = '" + user.MSISDN + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("status");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static List<String> fetchGradeStatus(String grade_code) {
        List<String> subList = new ArrayList<>();
        String query = "select status from channel_grades where grade_code ='" + grade_code + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                subList.add(result.getString("status"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subList;
    }

    public static Map<String, String> fetchRechargeOperatorDetails(String operatorName) {

        ResultSet result = null;
        Map<String, String> map = new HashMap<String, String>();

        final String query = "select interface_id, operator_id from mbk_operators where operator_name = '" + operatorName + "'";

        try {
            result = dbConn.RunQuery(query);
            if (result.next()) {
                map.put("interfaceId", result.getString("interface_id"));
                map.put("OperatorId", result.getString("operator_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public static UsrBalance getRechargeOperatorBalance(String operatorId) {

        BigDecimal balance = BigDecimal.valueOf(0), ficBalance = BigDecimal.valueOf(0), frozenBalance = BigDecimal.valueOf(0);

        final String query = "select sum(balance), sum(fic),sum(frozen_amount) from mtx_wallet_balances where wallet_number in(select wallet_number from mtx_wallet where user_id in(select user_id " +
                "from users where user_name in(select operator_name from mbk_operators where operator_id='" + operatorId + "')))";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                balance = new BigDecimal(result.getString("SUM(BALANCE)"));
                ficBalance = new BigDecimal(result.getString("SUM(FIC)"));
                frozenBalance = new BigDecimal(result.getString("SUM(FROZEN_AMOUNT)"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UsrBalance(balance, ficBalance, frozenBalance);
    }

    public static RechargeOperator getRechargeOperatorDetails(String providerId) {
        ResultSet result = null;

        final String query = "select distinct mo.operator_id, u.user_code, u.user_name, sum(mwb.balance), sum(mwb.fic),sum(mwb.frozen_amount), mw.user_grade, u.category_code" +
                " from mbk_operators mo, mtx_wallet_balances mwb, users u, mtx_wallet mw where mo.operator_name = u.user_name and u.user_id = mw.user_id " +
                "and mw.wallet_number = mwb.wallet_number and mo.status='Y' and mw.provider_id='" + providerId + "' group by mo.operator_id, u.user_code, u.user_name, " +
                "mwb.wallet_number, mw.user_grade, u.category_code order by mo.operator_id desc";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                return new RechargeOperator(result.getString("OPERATOR_ID"),
                        result.getString("USER_NAME"),
                        result.getString("USER_CODE"),
                        result.getString("USER_GRADE"),
                        result.getString("CATEGORY_CODE"),
                        providerId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getInterfaceId(Biller biller) {

        String query = "select interface_id from users where login_id='" + biller.LoginId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("INTERFACE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getInterfaceId(User channeluser) {

        String query = "select interface_id from users where msisdn='" + channeluser.MSISDN + "'";


        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("INTERFACE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBankId(Bank bank) {

        String query = "select bank_id from mbk_bank_details where bank_name='" + bank.BankName + "'";


        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("BANK_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get Trust Bank id as default routing bank
     *
     * @param nonPartnerBankName
     * @return
     */
    public static String getDefaultRoutingBankId(String nonPartnerBankName) {
        String query = "select trust_bank_id from MBK_BANK_DETAILS where BANK_NAME = '" + nonPartnerBankName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("trust_bank_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getInterfaceId(String bankName) {

        String query = "select interface_id from mbk_bank_details where bank_name='" + bankName + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("INTERFACE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Delete Wallet preference mapping | this delete entries and hence must be used carefully
     * Currently GUI is not available and hence DB query is used to delete mappings
     *
     * @param categoryCode
     * @param prefrenceType
     * @param paymentId
     */
    public static void deleteKYCPreferenceForWallet(String categoryCode, String prefrenceType, String paymentId) {

        final String query = "delete from kyc_preferences where category_code='" + categoryCode + "' and preference_type='" + prefrenceType + "' and payment_type_id='" + paymentId + "'";
        try {
            dbConn.RunUpdateQuery(query);
            dbConn.RunUpdateQuery("commit");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Delete Bank preference mapping | this delete entries and hence must be used carefully
     * Currently GUI is not available and hence DB query is used to delete mappings
     *
     * @param categoryCode
     * @param prefrenceType
     * @param bankId
     */
    public static void deleteKYCPreferenceForBank(String categoryCode, String prefrenceType, String bankId, String regType) {

        final String query = "delete from kyc_preferences " +
                "where category_code='" + categoryCode + "' " +
                "and preference_type='" + prefrenceType + "' " +
                "and KYC_MODE = '" + regType + "'" +
                "and bank_id='" + bankId + "'";
        try {
            dbConn.RunUpdateQuery(query);
            dbConn.RunUpdateQuery("commit");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Get Servic List Data from DB
     *
     * @param categoryCode
     * @param prefrenceType
     * @param bankId
     */
    public static void getServicListData(String categoryCode, String prefrenceType, String bankId) {

        final String query = "Select SERVICE_TYPE, DESCRIPTION, PAYER_PAYMENT_INSTRUMENT, PAYEE_PAYMENT_INSTRUMENT from SYS_SERVICE_PYMT_MAP;";
        String commitQuery = "commit";
        try {
            ResultSet result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String dbGetPasscode(String batchid) {
        String passcode = null;
        final String query = "select serial_number from mtx_transaction_header where attr_3_value='" + batchid + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                passcode = result.getString("SERIAL_NUMBER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return passcode;
    }

    /**
     * Get Grades associated to a category
     *
     * @param categoryCode
     * @return
     */
    public static List<String> dbGetGradesPerCategory(String categoryCode) {
        List<String> grade = new ArrayList<String>();
        final String query = "SELECT GRADE_NAME from CHANNEL_GRADES " + "where CATEGORY_CODE = '" + categoryCode
                + "' and STATUS = 'Y' order by grade_id asc";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result != null && result.next()) {
                grade.add(result.getString("GRADE_NAME"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return grade;
    }

    public static boolean checkUserLoginIdExist(String loginId) {
        boolean b_result = false;
        String res = null;
        String query = "select login_id from users where login_id ='" + loginId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("LOGIN_ID");

                //check the res
                if (res != null) {
                    if (res.equals(loginId)) {
                        b_result = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return b_result;
    }

    public static void updateWalletThresholdCount(int i, ExtentTest t) {

        String query = "update MTX_SYSTEM_PREFERENCES SET MAX_SIZE = " + i + " where PREFERENCE_CODE = 'MAX_WLT_ADD_PERDAY'";
        try {
            dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        t.info("MAX_WLT_ADD_PERDAY Value set to : " + i);
    }

    /**
     * Get Service List MAp
     *
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public static List<ServiceList> getServiceListDataFromDB() throws IOException, SQLException {
        List<ServiceList> serviceMap = new ArrayList<>();
        ResultSet result = dbConn.RunQuery("Select DISTINCT SERVICE_TYPE from SYS_SERVICE_PYMT_MAP");
        String serviceName = null, payerPaymentType = null, payeePaymentType = null;
        while (result.next()) {
            String serviceType = result.getString("SERVICE_TYPE");

            ResultSet result1 = dbConn.RunQuery("select a.DESCRIPTION, a.PAYER_PAYMENT_INSTRUMENT, a.PAYEE_PAYMENT_INSTRUMENT " +
                    "from  SYS_SERVICE_PYMT_MAP a,  SYS_SERVICE_types b " +
                    "where  a.SERVICE_TYPE = '" + serviceType + "' and a.DESCRIPTION = b.SERVICE_NAME");

            while (result1.next()) {
                serviceName = result1.getString("DESCRIPTION");
                payerPaymentType = result1.getString("PAYER_PAYMENT_INSTRUMENT");
                payeePaymentType = result1.getString("PAYEE_PAYMENT_INSTRUMENT");
            }

            ServiceList obj = new ServiceList(serviceName, serviceType, payerPaymentType, payeePaymentType);
            serviceMap.add(obj);
        }
        return serviceMap;
    }

    public static String dbGetUserGrade(User user) {
        String status = null;
        String query = "select GRADE_NAME from CHANNEL_GRADES where GRADE_CODE=( SELECT USER_GRADE from mtx_wallet WHERE msisdn = '" + user.MSISDN + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                status = result.getString("GRADE_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    public static String dbGetSubsMobileGroupRole(User user) {
        String status = null;
        //select GROUP_ROLE_CODE from user_roles where user_id=(select user_id from mtx_party where  msisdn = '"+user.MSISDN+"'") and GATEWAY_TYPES='WALLET'
        String query = "select GROUP_ROLE_CODE from user_roles where user_id=(select user_id from mtx_party where  msisdn = '" + user.MSISDN + "') and GATEWAY_TYPES='WALLET'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                status = result.getString("GROUP_ROLE_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    public static String dbGetEncryptedPwd(String loginId) {
        final String query = "select password from users where login_id = '" + loginId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("PASSWORD");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSecurityQnAssociatedWithUser() {
        ResultSet result = null;
        String question = null;
        final String query = "select distinct(question) from master_questions a,user_questions b where a.question_code=b.question_code and rownum=1";

        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                question = result.getString("QUESTION");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return question;
    }

    public static ArrayList<String> dbGetCategoriesForSelfRestPIN() {
        ArrayList<String> categories = new ArrayList<String>();
        final String query = "Select category_code from rules_set_definition where status_id='Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                categories.add(result.getString("CATEGORY_CODE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return categories;
    }

    public static ArrayList<String> dbGetQuestionsForCategory(String lanuageCode, String categoryCode) {

        ArrayList<String> questions = new ArrayList<String>();
        final String query = "select question from master_questions where question_code in " +
                "(select question_code from rules_set_questions where language_code='" + lanuageCode + "' and rules_set_id in " +
                "(select rules_set_id from rules_set_definition where category_code='" + categoryCode + "'))";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                questions.add(result.getString("QUESTION"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questions;
    }

    public static ArrayList<String> dbGetQuestionsFromMaster(String lanuageCode, String isMaster) {

        ArrayList<String> questions = new ArrayList<String>();

        final String query = "select question from master_questions where language_code='" + lanuageCode + "' and is_master='" + isMaster + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                questions.add(result.getString("QUESTION"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questions;
    }

    public static String dbGetMaxAccountBalanceForCategory(String categoryCode) {

        String maxBalance = null;
        final String query = "select max_acc_balance from rules_set_definition where category_code='" + categoryCode + "' and status_id='Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                maxBalance = result.getString("MAX_ACC_BALANCE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return maxBalance;
    }

    public static ArrayList<String> dbGetQuestionCodeForUser(String lanuageCode, String categoryCode) {

        ArrayList<String> questions = new ArrayList<String>();
        final String query = "select question_code from master_questions where question_code in " +
                "(select question_code from rules_set_questions where language_code='" + lanuageCode + "' and rules_set_id in " +
                "(select rules_set_id from rules_set_definition where category_code='" + categoryCode + "'))";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                questions.add(result.getString("QUESTION_CODE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questions;
    }

    public static String dbGetQuestionCodeForQuestion(String question) {

        String questions = null;
        final String query = "select question_code from master_questions where question='" + question + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                questions = result.getString("QUESTION_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questions;
    }

    public static HashMap<String, String> getKinDetailsForUser(String msisdn) {
        HashMap<String, String> kinDetails = new HashMap<>();
        final String query = "select * from mtx_party where msisdn='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                kinDetails.put("Kin_FN", result.getString("KIN_FIRSTNAME"));
                kinDetails.put("Kin_LN", result.getString("KIN_LASTNAME"));
                kinDetails.put("Kin_MN", result.getString("KIN_MIDDLENAME"));
                kinDetails.put("Kin_RelationShip", result.getString("KIN_RELATIONSHIP"));
                kinDetails.put("Kin_ContactNo", result.getString("KIN_CONTACTNUMBER"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return kinDetails;
    }

    public static String fetchRelationShipDetails(String MSISDN) {
        ResultSet result = null;
        String details = null;
        final String query = "select RELATIONSHIP from users where MSISDN = '" + MSISDN + "'";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                details = result.getString("RELATIONSHIP");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return details;
    }

    public static String getWalletID(String walletName) {
        String walletID = null;
        String query = "select PAYMENT_TYPE_ID from SYS_PAYMENT_METHOD_SUBTYPES where SUBTYPE_NAME ='" + walletName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                walletID = result.getString("PAYMENT_TYPE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return walletID;
    }

    /**
     * this method get count or sum of all success full  transaction a single day
     *
     * @param gateway
     * @param service
     * @param providerid
     * @param paymentId
     * @param type
     * @return
     */
    public static String getTCPCountOrValueDaily(String gateway, String service, String providerid, String paymentId, String type) {
        String value = "count(*)";
        if (type.equalsIgnoreCase(Constants.SUM)) {
            value = "sum(transfer_value)";
        } else {
            value = "count(*)";
        }
        String query = "SELECT " + value + " FROM mtx_transaction_header WHERE transfer_status = 'TS' AND request_gateway_type = '" + gateway
                + "'   AND service_type = '" + service + "'   and PAYER_PROVIDER_ID =" + providerid + " and payer_payment_type_id ='" + paymentId
                + "'AND transfer_date between (SELECT round(SYSDATE)-1 FROM DUAL) and (SELECT round(SYSDATE) FROM DUAL)";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Map<String, String> getOperatorDetails(String providerId) {
        ResultSet result = null;
        Map<String, String> map = new HashMap<String, String>();

        final String query = "select distinct mo.operator_id, u.user_code, u.user_name, sum(mwb.balance), sum(mwb.fic),sum(mwb.frozen_amount) from mbk_operators mo, mtx_wallet_balances mwb, users u, mtx_wallet mw " +
                "where mo.operator_name = u.user_name and u.user_id = mw.user_id and mw.wallet_number = mwb.wallet_number and mo.status='Y' and mw.provider_id='101' group by mo.operator_id, " +
                "u.user_code, u.user_name, mwb.wallet_number order by mo.operator_id desc";
        try {
            result = dbConn.RunQuery(query);
            while (result.next()) {
                map.put("opt_id", result.getString("OPERATOR_ID"));
                map.put("opt_code", result.getString("USER_CODE"));
                map.put("opt_name", result.getString("USER_NAME"));
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }

    public static void deleteTodayCreatedWallet() {

        final String query = "delete from SYS_PAYMENT_METHOD_SUBTYPES where CREATED_ON > sysdate - 1 ";
        try {
            ResultSet result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDeletedWallet() {
        String walletName = null;
        final String query = "select SUBTYPE_NAME from SYS_PAYMENT_METHOD_SUBTYPES where STATUS = 'N'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                walletName = result.getString("SUBTYPE_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return walletName;
    }

    public static String dbGetTransactionStatusWithFTXNID(String txnID) {
        final String query = "select TRANSFER_STATUS from MTX_TRANSACTION_HEADER  where FTXN_ID = '" + txnID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("TRANSFER_STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMasterQuestionsCount(String languageCode) {
        String count = null;
        String query = "select count(*) from MASTER_QUESTIONS where language_code = '" + languageCode + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                count = result.getString("count(*)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public static ResultSet fetchPayerPayeeCount(String txnID) {
        ResultSet result = null;
        final String query = "select payer_count, payee_count from threshold_count " +
                "where id = '" + txnID + "'";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getOwnerIDOfUser(String msisdn) {
        String ownerID = null;

        try {
            ResultSet resultSet = dbConn.RunQuery("select OWNER_ID from users where msisdn='" + msisdn + "'");

            while (resultSet.next()) {
                ownerID = resultSet.getString("OWNER_ID");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return ownerID;
    }

    public static String getParentIDOfUser(String msisdn) {
        String ownerID = null;

        try {
            ResultSet resultSet = dbConn.RunQuery("select PARENT_ID from users where msisdn='" + msisdn + "'");

            while (resultSet.next()) {
                ownerID = resultSet.getString("PARENT_ID");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ownerID;
    }

    public static String getMsisdnUsingUserId(String userId) {
        String msisdn = null;

        try {
            ResultSet resultSet = dbConn.RunQuery("select MSISDN from users where USER_ID='" + userId + "'");

            while (resultSet.next()) {
                msisdn = resultSet.getString("MSISDN");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msisdn;
    }

    public static List<String> getCategoryNameListUsingDomainCode(String domainCode) {
        List<String> list = new ArrayList<>();

        try {
            ResultSet resultSet = dbConn.RunQuery("select CATEGORY_NAME from MTX_CATEGORIES where DOMAIN_CODE='" + domainCode + "'");

            while (resultSet.next()) {
                list.add(resultSet.getString("CATEGORY_NAME"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static BigDecimal getBalanceBankWallet(String bankName) {
        BigDecimal res = null;
        final String query = "select mwb.BALANCE from mtx_wallet_balances mwb,mtx_wallet mw,mbk_bank_details bnk where mw.WALLET_NUMBER=mwb.WALLET_NUMBER and \n" +
                "mw.USER_ID=bnk.BANK_ID and bnk.BANK_NAME='" + bankName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = new BigDecimal(result.getString("balance")).divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getUserIdFromUserName(String userName) {
        String userId = null;
        String query = "select user_id from users where user_name = '" + userName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                userId = result.getString("USER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userId;
    }

    /***
     * it will return user id based on wallet number from MTX_WALLET table
     * @param walletNumber
     * @return
     * @throws Exception
     */
    public static String getUserIdFromMtxWallet(String walletNumber) throws Exception {
        String userId = null;
        String providerID = DataFactory.getDefaultProvider().ProviderId;
        String query = "select user_id from mtx_wallet where wallet_number = '" + providerID + "" + walletNumber + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                userId = result.getString("USER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userId;
    }

    /**
     * it will return user id based on customer id from MBK_CUST_ACCOUNTS table;
     *
     * @param customerId
     * @return
     */
    public static String getUserIdFromCustAccounts(String customerId) {
        String userId = null;
        String query = "select user_id from mbk_cust_accounts where cust_id = '" + customerId + "' and status_id = 'Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                userId = result.getString("USER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userId;
    }

    /**
     * it will return operator's active bank account number
     *
     * @param walletType
     * @param bankId
     * @return
     * @throws Throwable
     */
    public static String getOperatorBankAccountNum(String walletType, String bankId, String... userName) throws Throwable {
        String userId;
        if (walletType != null) {
            userId = getUserIdFromMtxWallet(walletType);
        } else {
            userId = getUserIdFromUserName(userName[0]);
        }
        String custId = null;
        String bankAccNum = null;
        String getCustIdQuery = "select cust_id from mbk_cust_accounts where status_id = 'Y' and bank_id = '" + bankId + "' and USER_ID = '" + userId + "'";
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        try {
            ResultSet result = dbConn.RunQuery(getCustIdQuery);
            while (result.next()) {
                custId = result.getString("CUST_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        String getBankAccNumQuery = "select account_no from mbk_cust_accounts where cust_id = '" + custId + "' and status_id = 'Y'";
        try {
            ResultSet result = dbConn.RunQuery(getBankAccNumQuery);
            while (result.next()) {
                bankAccNum = result.getString("ACCOUNT_NO");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        String accNum = new DesEncryptor().decrypt(bankAccNum);

        return accNum;
    }

    /**
     * Fetch Domain and category when providing the category Code
     *
     * @param categoryCode
     * @return Result set
     * @deprecated
     */
    public ResultSet dbFetchDomainCategoryName(String categoryCode) {
        ResultSet result = null;
        final String query = "select u1.CATEGORY_NAME, u2.DOMAIN_NAME, u2.DOMAIN_CODE "
                + "from mtx_categories u1, mtx_domains u2 where u1.DOMAIN_CODE = u2.DOMAIN_CODE "
                + "and u1.CATEGORY_CODE = '" + categoryCode + "'";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Get Domain name From Grade Code
     *
     * @param gradeName
     * @return
     */
    public String dbGetDomainNameFromGrade(String gradeName) {
        String domainName = null;
        final String query = "SELECT DOMAIN_NAME from MTX_DOMAINS where "
                + "DOMAIN_CODE IN (SELECT DOMAIN_CODE FROM MTX_CATEGORIES WHERE CATEGORY_CODE IN "
                + "(select CATEGORY_CODE from CHANNEL_GRADES where grade_name='" + gradeName + "'))";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                domainName = result.getString("DOMAIN_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return domainName;
    }

    /**
     * Get list of all the bank available in the system
     *
     * @return
     */
    public List<String> dbGetbankName() {
        List<String> grade = new ArrayList<String>();
        final String query = "select bank_name from mbk_bank_details";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                grade.add(result.getString("BANK_NAME"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return grade;
    }

    /**
     * @param TRANSFER_ID
     * @return
     */
    public String dbGetRequestedAmount(String TRANSFER_ID) {
        String RV = null;
        final String query = "select REQUESTED_VALUE from MTX_TRANSACTION_HEADER  where TRANSFER_ID='" + TRANSFER_ID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                RV = result.getString("REQUESTED_VALUE");
                RV = RV.substring(0, RV.length() - 4);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return RV;
    }

    /**
     * It will return the last Transaction ID from the Db
     *
     * @return
     */
    public String dbGetLastTransID(String ServiceType) {
        String tID = null;
        final String query = "select TRANSFER_ID from (select TRANSFER_ID from MTX_TRANSACTION_HEADER  " +
                "where SERVICE_TYPE = '" + ServiceType + "'   order by TRANSFER_DATE  DESC) " +
                "where rownum = 1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                tID = result.getString("TRANSFER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tID;
    }

    /**
     * This method will return the last batch payout Transaction ID
     *
     * @return
     */
    public String dbGetLastBatchPayoutID() {
        String res = null;
        String query = "select BATCH_ID from (Select BATCH_ID from MTX_BATCHES order By Created_on desc)  where ROWNUM=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("BATCH_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * This will return the msisdn of the enterprise user
     *
     * @param msisdn
     * @return
     */
    public String getEnterpriseEmpCode(String msisdn) {
        String res = null;
        String query = "select user_code from users where login_id ='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("user_code");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * @return
     */
    public ResultSet dbExternalCode() {
        ResultSet result = null;
        final String query = "select DEFAULT_VALUE from MTX_SYSTEM_PREFERENCES where PREFERENCE_NAME = 'MIN_EXTERNAL_CODE_LENGTH' ";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    /**
     * Get Service Charge Details From DB
     *
     * @param sCharge
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public Map<String, String> dbGetServiceCharge(ServiceCharge sCharge) throws IOException, SQLException {
        ServiceList serviceInfo = sCharge.ServiceInfo;
        User payer = sCharge.Payer;
        User payee = sCharge.Payee;
        Map<String, String> map = new HashMap<>();
        ResultSet result;

        String query1 = "select SERVICE_CHARGE_ID,SERVICE_CHARGE_NAME,STATUS_ID,SHORT_CODE from MTX_SERVICE_CHARGE where " +
                "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                "AND PAYER_GRADE_CODE='" + payer.GradeCode + "' " +
                "AND PAYEE_GRADE_CODE='" + payee.GradeCode + "' " +
                "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                "AND PAYEE_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayeePaymentType + "' " +
                "AND " + payer.dbPaymentTypeId + " ='" + payer.PaymentTypeID + "' " +
                "AND " + payee.dbPaymentTypeId + " ='" + payee.PaymentTypeID + "' " +
                "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "' ORDER BY SERVICE_CHARGE_ID DESC";

        result = dbConn.RunQuery(query1);
        while (result.next()) {
            if (result.getString("STATUS_ID").equals("Y") || result.getString("STATUS_ID").equals("S")) {
                map.put("ID", result.getString("SERVICE_CHARGE_ID"));
                map.put("Name", result.getString("SERVICE_CHARGE_NAME"));
                map.put("Status", result.getString("STATUS_ID"));
                map.put("Code", result.getString("SHORT_CODE"));
            }
        }

        /*
         * If the result is empty, then check the MTX_SERVICE_CHARGE_M table
         * Service charge might be present, though the status could be UI, DI or AI
         */
        if (!result.next()) {
            String query2 = "select SERVICE_CHARGE_ID,SERVICE_CHARGE_NAME,STATUS_ID from MTX_SERVICE_CHARGE_M where " +
                    "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                    "AND PAYER_GRADE_CODE='" + payer.GradeCode + "' " +
                    "AND PAYEE_GRADE_CODE='" + payee.GradeCode + "' " +
                    "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                    "AND PAYEE_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayeePaymentType + "' " +
                    "AND " + payer.dbPaymentTypeId + " ='" + payer.PaymentTypeID + "' " +
                    "AND " + payee.dbPaymentTypeId + " ='" + payee.PaymentTypeID + "' " +
                    "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                    "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "' ORDER BY SERVICE_CHARGE_ID DESC";

            result = dbConn.RunQuery(query2);
            while (result.next()) {
                map.put("ID", result.getString("SERVICE_CHARGE_ID"));
                map.put("Name", result.getString("SERVICE_CHARGE_NAME"));
                map.put("Status", result.getString("STATUS_ID"));
            }
        }

        return map;
    }

    /**
     * Get Transfer Rule Details From DB
     *
     * @param sCharge
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public Map<String, String> dbGetTransferRule(ServiceCharge sCharge) throws IOException, SQLException {
        ServiceList serviceInfo = sCharge.ServiceInfo;
        User payer = sCharge.Payer;
        User payee = sCharge.Payee;
        ResultSet result;
        Map<String, String> map = new HashMap<>();

        String query1 = "select TRANSFER_RULE_ID,STATUS_ID from MTX_TRANSFER_RULES where " +
                "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                "AND PAYER_CATEGORY_CODE='" + payer.CategoryCode + "' " +
                "AND PAYEE_CATEGORY_CODE='" + payee.CategoryCode + "' " +
                "AND PAYER_GRADE_CODE='" + sCharge.payerGrade + "' " +
                "AND PAYEE_GRADE_CODE='" + sCharge.payeeGrade + "' " +
                "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                "AND PAYEE_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayeePaymentType + "' " +
                "AND " + payer.dbPaymentTypeId + " ='" + payer.PaymentTypeID + "' " +
                "AND " + payee.dbPaymentTypeId + " ='" + payee.PaymentTypeID + "' " +
                "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "'";

        result = dbConn.RunQuery(query1);

        while (result.next()) {
            if (result.getString("STATUS_ID").equals("Y")) {
                map.put("ID", result.getString("TRANSFER_RULE_ID"));
                map.put("Status", result.getString("STATUS_ID"));
            }
        }
        /*
         * If the result is empty, then check the MTX_SERVICE_CHARGE_M table
         * Service charge might be present, though the status could be UI, DI or AI
         */
        if (map.isEmpty()) {
            String query2 = "select TRANSFER_RULE_ID,STATUS_ID from MTX_TRANSFER_RULES_M where " +
                    "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                    "AND PAYER_CATEGORY_CODE='" + payer.CategoryCode + "' " +
                    "AND PAYEE_CATEGORY_CODE='" + payee.CategoryCode + "' " +
                    "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                    "AND PAYEE_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayeePaymentType + "' " +
                    "AND " + payer.dbPaymentTypeId + " ='" + payer.PaymentTypeID + "' " +
                    "AND " + payee.dbPaymentTypeId + " ='" + payee.PaymentTypeID + "' " +
                    "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                    "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "'";

            result = dbConn.RunQuery(query2);
            while (result.next()) {
                map.put("ID", result.getString("TRANSFER_RULE_ID"));
                map.put("Status", result.getString("STATUS_ID"));
            }
        }
        return map;
    }

    /**
     * Get Service Charge Details From DB
     *
     * @param sCharge
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public Map<String, String> dbGetNFSC(ServiceCharge sCharge) throws IOException, SQLException {
        ServiceList serviceInfo = sCharge.ServiceInfo;
        User payer = sCharge.Payer;
        User payee = sCharge.Payee;
        Map<String, String> map = new HashMap<>();
        ResultSet result;

        String query1 = "select SERVICE_CHARGE_ID,SERVICE_CHARGE_NAME,STATUS_ID from MTX_SERVICE_CHARGE where " +
                "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                "AND PAYER_GRADE_CODE='" + payer.GradeCode + "' " +
                "AND PAYEE_GRADE_CODE='" + payee.GradeCode + "' " +
                "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                "AND PAYEE_PAYMENT_METHOD_TYPE is null " +
                //"AND PAYER_PAYMENT_TYPE_ID='" + payer.PaymentTypeID + "' " +
                "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "' ORDER BY SERVICE_CHARGE_ID DESC";

        result = dbConn.RunQuery(query1);
        while (result.next()) {
            if (result.getString("STATUS_ID").equals("Y")) {
                map.put("ID", result.getString("SERVICE_CHARGE_ID"));
                map.put("Name", result.getString("SERVICE_CHARGE_NAME"));
                map.put("Status", result.getString("STATUS_ID"));
            }
        }

        /*
         * If the result is empty, then check the MTX_SERVICE_CHARGE_M table
         * Service charge might be present, though the status could be UI, DI or AI
         */
        if (!result.next()) {
            String query2 = "select SERVICE_CHARGE_ID,SERVICE_CHARGE_NAME,STATUS_ID from MTX_SERVICE_CHARGE_M where " +
                    "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                    "AND PAYER_GRADE_CODE='" + payer.GradeCode + "' " +
                    "AND PAYEE_GRADE_CODE='" + payee.GradeCode + "' " +
                    "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                    "AND PAYEE_PAYMENT_METHOD_TYPE is null " +
                    //"AND PAYER_PAYMENT_TYPE_ID='" + payer.PaymentTypeID + "' " +
                    "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                    "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "' ORDER BY SERVICE_CHARGE_ID DESC";

            result = dbConn.RunQuery(query2);
            while (result.next()) {
                map.put("ID", result.getString("SERVICE_CHARGE_ID"));
                map.put("Name", result.getString("SERVICE_CHARGE_NAME"));
                map.put("Status", result.getString("STATUS_ID"));
            }
        }

        return map;
    }

    public Map<String, String> dbGetNFSCForSpecificWallet(ServiceCharge sCharge) throws IOException, SQLException {
        ServiceList serviceInfo = sCharge.ServiceInfo;
        User payer = sCharge.Payer;
        User payee = sCharge.Payee;
        Map<String, String> map = new HashMap<>();
        ResultSet result;

        String query1 = "select SERVICE_CHARGE_ID, SERVICE_CHARGE_NAME, STATUS_ID from MTX_SERVICE_CHARGE where " +
                "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                "AND PAYER_GRADE_CODE='" + payer.GradeCode + "' " +
                "AND PAYEE_GRADE_CODE='" + payee.GradeCode + "' " +
                "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                "AND PAYEE_PAYMENT_METHOD_TYPE is null " +
                "AND PAYER_PAYMENT_TYPE_ID='" + payer.PaymentTypeID + "' " +
                "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "' ORDER BY SERVICE_CHARGE_ID DESC";

        result = dbConn.RunQuery(query1);
        while (result.next()) {
            if (result.getString("STATUS_ID").equals("Y")) {
                map.put(result.getString("SERVICE_CHARGE_ID"), result.getString("SERVICE_CHARGE_NAME"));

            }
        }
        return map;
    }

    /**
     * Get the registered payee for the enterprise
     *
     * @param msisdn
     * @return
     */
    public String getEnterpriseSubscriber(String msisdn) {
        String res = null;
        String query = "select MSISDN from mtx_party where user_id IN (select user_id from MTX_PAYROLL_EMPLOYEES where PAYROLL_COMPANY_ID IN (select user_id from users " +
                "where MSISDN ='" + msisdn + "')) and status ='Y' and ROWNUM =1";
        System.out.print("Got query " + query);
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MSISDN");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String dbGetBankId(String bankName) {
        final String query = "select BANK_ID from MBK_BANK_DETAILS where BANK_NAME = '" + bankName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("BANK_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get Geo Domain name
     *
     * @return
     */
    public String dbGetGeoDomainName() {
        final String query = "select GRPH_DOMAIN_NAME from GEOGRAPHICAL_DOMAINS where GRPH_DOMAIN_TYPE ='ZO' and status='Y' and rownum <=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("GRPH_DOMAIN_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This function fetches geography area of channel user
     *
     * @param geoZone
     * @return
     */
    public String dbGetChannelGeo(String geoZone) {
        final String query = "select GRPH_DOMAIN_NAME from GEOGRAPHICAL_DOMAINS where PARENT_GRPH_DOMAIN_CODE = '" + geoZone + "' and GRPH_DOMAIN_TYPE = 'AR' " +
                "and status = 'Y' and rownum <=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("GRPH_DOMAIN_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get TCP Profile Id based on the Profile name
     *
     * @param profileName
     * @return
     */
    public static String getIDofTCPprofile(String profileName) {
        String query = "Select PROFILE_ID from MTX_TRF_CNTRL_PROFILE where PROFILE_NAME = '" + profileName + "' and status_id = 'Y' ";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("PROFILE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get System Preferences
     *
     * @return
     */
    public Map<String, String> dbGetMtxPreferences() {
        Map<String, String> map = new HashMap<String, String>();
        final String query = "Select DEFAULT_VALUE, PREFERENCE_CODE from MTX_SYSTEM_PREFERENCES";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                map.put(result.getString("PREFERENCE_CODE"), result.getString("DEFAULT_VALUE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }

    /**
     * Fetch parent category details, CategoryName and Code
     *
     * @param categoryName
     * @return
     */
    public ResultSet dbFetchParentCategoryDetail(String categoryName) {
        ResultSet result = null;
        final String query = "Select u1.CATEGORY_NAME, u1.CATEGORY_CODE from MTX_CATEGORIES u1, "
                + "MTX_CATEGORIES u2 where u2.CATEGORY_NAME ='" + categoryName + "' "
                + "AND u2.PARENT_CATEGORY_CODE = u1.CATEGORY_CODE";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Get Grade Code
     *
     * @param gradeName
     * @return
     */
    public String dbGetGradesCode(String gradeName) {
        String gradeCode = null;
        final String query = "SELECT GRADE_CODE from CHANNEL_GRADES where GRADE_NAME = '" + gradeName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                gradeCode = result.getString("GRADE_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gradeCode;
    }

    /**
     * Get list of all the subscriber MSISDN in the System
     *
     * @return
     */
    public List<String> getAllSubscriberMsisdn() {
        List<String> subList = new ArrayList<>();
        String query = "Select MSISDN from MTX_PARTY where PWDFLAG = 'Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                subList.add(result.getString("MSISDN"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subList;
    }

    /**
     * Fetch Domain Category Map
     *
     * @return
     */
    public ResultSet dbFetchDomainCategoryMap() {
        ResultSet result = null;
        final String query = "select u1.CATEGORY_NAME, u1.CATEGORY_CODE, u2.DOMAIN_NAME, u2.DOMAIN_CODE, u1.PARENT_CATEGORY_CODE " +
                "from mtx_categories u1, mtx_domains u2 where u1.DOMAIN_CODE = u2.DOMAIN_CODE" +
                " and u1.CATEGORY_CODE in (select CATEGORY_CODE from MTX_CATEGORIES where STATUS = 'Y')";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Fetch Currency Provider Map
     *
     * @return
     */
    public ResultSet dbFetchCurrencyProviderMap() {
        ResultSet result = null;
        final String query = " select u1.BANK_ID, u1.BANK_NAME, u2.PROVIDER_ID, u2.PROVIDER_NAME, " +
                "u2.CURRENCY_CODE from mbk_bank_details u1,  sys_service_provider u2 " +
                "where u2.PROVIDER_ID in (select u2.PROVIDER_ID from sys_service_provider where u2.STATUS = 'Y') " +
                "and u1.provider_id=u2.provider_id; ";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Fetch Grade Category Map
     *
     * @return
     */
    public ResultSet dbFetchGradeCategoryMap() {
        ResultSet result = null;
        final String query = "SELECT GRADE_NAME, GRADE_CODE, CATEGORY_CODE from CHANNEL_GRADES " +
                "where CATEGORY_CODE in(select CATEGORY_CODE from MTX_CATEGORIES) and STATUS = 'Y' order by grade_id asc";
        try {
            result = dbConn.RunQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Close current session
     *
     * @return
     * @throws Exception
     */
    public void closeConnection() throws Exception {
        dbConn.CloseConnection();
    }

    public static String dbGetReferencenumber(String txnID) {
        final String query = "select REFERENCE_NUMBER from MTX_TRANSACTION_HEADER  where TRANSFER_ID = '" + txnID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("REFERENCE_NUMBER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getTrustBank() throws Exception {
        final String query = "select BANK_NAME from mbk_bank_details where bank_type='TRUST'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("BANK_NAME");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public String dbGetGeoDomainNameCode() {
        final String query = "select GRPH_DOMAIN_CODE from GEOGRAPHICAL_DOMAINS where GRPH_DOMAIN_TYPE ='ZO' and status='Y' and rownum <=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("GRPH_DOMAIN_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String dbGetChannelGeoCode(String geoZone) {
        final String query = "select GRPH_DOMAIN_CODE from GEOGRAPHICAL_DOMAINS where PARENT_GRPH_DOMAIN_CODE = '" + geoZone + "' and GRPH_DOMAIN_TYPE = 'AR' " +
                "and status = 'Y' and rownum <=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("GRPH_DOMAIN_CODE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String dbFetchPassWordFromEmailQueue(String msisdn) {
        String res = null;
        final String query = "select * from mtx_email_queue eq,users u where u.msisdn='" + msisdn + "' and u.email=eq.to_addr and eq.subject like '%Created%'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("SENSITIVE_INFO");
            }

            System.out.println(res);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    /**
     * Get Transfer Rule for specific grade Details From DB
     *
     * @param sCharge
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public Map<String, String> dbGetTransferRuleForSpecificGrade(ServiceCharge sCharge, String payerGrade, String payeeGrade) throws IOException, SQLException {
        ServiceList serviceInfo = sCharge.ServiceInfo;
        User payer = sCharge.Payer;
        User payee = sCharge.Payee;
        ResultSet result;
        Map<String, String> map = new HashMap<>();

        String query1 = "select TRANSFER_RULE_ID,STATUS_ID from MTX_TRANSFER_RULES where " +
                "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                "AND PAYER_CATEGORY_CODE='" + payer.CategoryCode + "' " +
                "AND PAYEE_CATEGORY_CODE='" + payee.CategoryCode + "' " +
                "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                "AND PAYEE_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayeePaymentType + "' " +
                "AND " + payer.dbPaymentTypeId + " ='" + payer.PaymentTypeID + "' " +
                "AND " + payee.dbPaymentTypeId + " ='" + payee.PaymentTypeID + "' " +
                "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "'" +
                "AND PAYER_GRADE_CODE='" + payerGrade + "'" +
                "AND PAYEE_GRADE_CODE='" + payeeGrade + "'";

        result = dbConn.RunQuery(query1);

        while (result.next()) {
            if (result.getString("STATUS_ID").equals("Y")) {
                map.put("ID", result.getString("TRANSFER_RULE_ID"));
                map.put("Status", result.getString("STATUS_ID"));
            }
        }
        /*
         * If the result is empty, then check the MTX_SERVICE_CHARGE_M table
         * Service charge might be present, though the status could be UI, DI or AI
         */
        if (map.isEmpty()) {
            String query2 = "select TRANSFER_RULE_ID,STATUS_ID from MTX_TRANSFER_RULES_M where " +
                    "SERVICE_TYPE='" + serviceInfo.ServcieType + "' " +
                    "AND PAYER_CATEGORY_CODE='" + payer.CategoryCode + "' " +
                    "AND PAYEE_CATEGORY_CODE='" + payee.CategoryCode + "' " +
                    "AND PAYER_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayerPaymentType + "' " +
                    "AND PAYEE_PAYMENT_METHOD_TYPE ='" + serviceInfo.PayeePaymentType + "' " +
                    "AND " + payer.dbPaymentTypeId + " ='" + payer.PaymentTypeID + "' " +
                    "AND " + payee.dbPaymentTypeId + " ='" + payee.PaymentTypeID + "' " +
                    "AND PAYER_PROVIDER_ID='" + payer.ProviderId + "' " +
                    "AND PAYEE_PROVIDER_ID='" + payee.ProviderId + "'" +
                    "AND PAYER_GRADE_CODE='" + payerGrade + "'" +
                    "AND PAYEE_GRADE_CODE='" + payeeGrade + "'";

            result = dbConn.RunQuery(query2);
            while (result.next()) {
                map.put("ID", result.getString("TRANSFER_RULE_ID"));
                map.put("Status", result.getString("STATUS_ID"));
            }
        }
        return map;
    }

    public static String fetchWalletNumber(String msisdn) {
        final String query = "select wallet_number from mtx_wallet where is_primary='Y' and msisdn='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("WALLET_NUMBER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static String fetchPinNumber(String msisdn) {
        final String query = "select pin_number from mtx_wallet where is_primary='Y' and msisdn='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                return result.getString("PIN_NUMBER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public String dbGetLastFTXNID(String ServiceType) {
        String tID = null;
        final String query = "select FTXN_ID from (select FTXN_ID from MTX_TRANSACTION_HEADER  " +
                "where SERVICE_TYPE = '" + ServiceType + "'   order by TRANSFER_DATE  DESC) " +
                "where rownum = 1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                tID = result.getString("TRANSFER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tID;
    }

    public static List<UserWallets> getActiveWalletsAssociatedWithUser(User user) {
        List<UserWallets> walletList = new ArrayList<>();
        String table = (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) ? "mtx_party" : "users";
        final String query = "select is_primary, payment_type_id, provider_id " +
                "from mtx_wallet " +
                "where user_id in (select user_id from " + table + " where LOGIN_ID ='" + user.LoginId + "')and status = 'Y'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                String isPrimary = result.getString("is_primary");
                String payId = result.getString("payment_type_id");
                String providerId = result.getString("provider_id");
                walletList.add(new UserWallets(payId, providerId, isPrimary, "Y"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return walletList;
    }

    /**
     * Check if Wallet/ bank mapping preference is already present in the system
     *
     * @param providerId
     * @param categoryCode
     * @param regType
     * @param payId
     * @return
     */
    public static boolean isWalletMappingForSVAWithSpecificRole(String providerId,
                                                                String categoryCode,
                                                                String regType,
                                                                String payId,
                                                                String roleId) {
        final String query = "select * from kyc_preferences  " +
                "where provider_id = '" + providerId + "' " +
                "and preference_type = 'SAV_CLUB_CREATION' " +
                "and category_code = '" + categoryCode + "' " +
                "and BANK_ID = '" + payId + "' " +
                "and GROUP_ROLE_CODE = '" + roleId + "' " +
                "and KYC_MODE = '" + regType + "'";
        try {
            if (dbConn.RunQuery(query).next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * Check if user registartion wallet preference is already mapped
     *
     * @param providerId
     * @param categoryCode
     * @param regType
     * @param payId
     * @return
     */
    public static boolean isWalletMappingForUserRegistrationWithSpecificRole(String providerId, String categoryCode,
                                                                             String regType, String payId, String roleId) {
        final String query = "select * from kyc_preferences  " +
                "where provider_id = '" + providerId + "' " +
                "and preference_type = 'USER_REGISTRATION' " +
                "and category_code = '" + categoryCode + "' " +
                "and PAYMENT_TYPE_ID = '" + payId + "' " +
                "and GROUP_ROLE_CODE = '" + roleId + "' " +
                "and KYC_MODE = '" + regType + "'";
        try {
            if (dbConn.RunQuery(query).next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Check if user registartion wallet preference is already mapped
     *
     * @param providerId
     * @param categoryCode
     * @param regType
     * @param payId
     * @return
     */
    public static boolean isWalletMappingForBankLinkingWithSpecificRole(String providerId,
                                                                        String categoryCode,
                                                                        String regType,
                                                                        String payId,
                                                                        String roleId) {
        final String query = "select * from kyc_preferences  " +
                "where provider_id = '" + providerId + "' " +
                "and preference_type = 'BANK_ACCOUNT_LINKING' " +
                "and category_code = '" + categoryCode + "' " +
                "and BANK_ID = '" + payId + "' " +
                "and GROUP_ROLE_CODE = '" + roleId + "' " +
                "and KYC_MODE = '" + regType + "'";
        try {
            if (dbConn.RunQuery(query).next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * DB Response when trying top fetching Service Charge From Db
     */
    public class DBServiceDetail {
        String ServiceChargeName = null, ServiceChargeId = null, Status = null;

        public DBServiceDetail(String name, String id, String status) {
            this.ServiceChargeName = name;
            this.ServiceChargeId = id;
            this.Status = status;
        }
    }


    public ArrayList<String> dbGetServiceChargeForSpecifiedGrade(String payerGrade, String payeeGrade) throws IOException, SQLException {
        ResultSet result;
        ArrayList<String> servicesList = new ArrayList<>();

        String query1 = "select distinct(SERVICE_TYPE) from MTX_SERVICE_CHARGE where " +
                "PAYER_GRADE_CODE in (" + payerGrade + ") " +
                "OR PAYEE_GRADE_CODE in (" + payeeGrade + ")";

        result = dbConn.RunQuery(query1);

        while (result.next()) {
            servicesList.add(result.getString("SERVICE_TYPE"));
        }
        return servicesList;
    }

    public static ResultSet dbGetBankDetails() throws IOException, SQLException {
        try {
            return dbConn.RunQuery("select * from MBK_BANK_DETAILS");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getPasswordFromSentSMS(String msisdn) {
        String msg = null;
        String pass = null;
        String query = "select MESSAGE from sentsms where msisdn = '" + msisdn + "' order by deliveredon asc";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                msg = result.getString("MESSAGE");
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ConfigInput.isCoreRelease) {
            msg = Utils.decryptMessage(msg);
        }
        // msg = Utils.decryptMessage(msg);
        pass = msg.split("Password")[1].trim();
        return pass;
    }

    public static String getLastTransferIDFromOperator(String operatorWalletId) {

        String transferId = null;
        String query = "select last_transfer_id from mtx_wallet_balances where wallet_number = '" + operatorWalletId + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                transferId = result.getString("LAST_TRANSFER_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Last Txn Id: " + transferId);
        return transferId;
    }

    public static String fetchAcquisitionPaid(String msisdn) {

        String acqPaid = null;
        String query = "SELECT ACQUISITION_PAID from MTX_PARTY_ACCESS where MSISDN='" + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                acqPaid = result.getString("ACQUISITION_PAID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return acqPaid;
    }

    public static Map<String, CustomerTCP> getExistingCustomerTCPs() {
        Map<String, CustomerTCP> tcpMap = new HashMap<>();
        String query = "SELECT * FROM MTX_TRF_CNTRL_PROFILE WHERE PROFILE_TYPE = 'CAT'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                String profileName = result.getString("PROFILE_NAME");
                String cateCode = result.getString("CATEGORY_CODE");
                String regType = result.getString("REGISTRATION_TYPE");
                tcpMap.put(profileName, new CustomerTCP(DataFactory.getDomainName(cateCode), cateCode, regType));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tcpMap;
    }

    public static Map<String, InstrumentTCP> getExistingInstrumentTCPs() {
        Map<String, InstrumentTCP> tcpMap = new HashMap<>();
        String query = "SELECT * FROM MTX_TRF_CNTRL_PROFILE WHERE PROFILE_TYPE = 'ROLE'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                String profileName = result.getString("PROFILE_NAME");
                String mfsProvider = DataFactory.getProviderName(result.getString("PROVIDER_ID"));
                String cateCode = result.getString("CATEGORY_CODE");
                String gradeName = DataFactory.getGradeName(result.getString("GRADE_CODE"));
                String payInst = result.getString("PAYMENT_METHOD_TYPE_ID");
                String payInstType = null;
                if (payInst.equalsIgnoreCase("WALLET")) {
                    payInstType = DataFactory.getWalletName(result.getString("PAYMENT_METHOD_TYPE_ID_DESC"));
                } else {
                    payInstType = DataFactory.getBankName(result.getString("PAYMENT_METHOD_TYPE_ID_DESC"));
                }
                tcpMap.put(profileName, new InstrumentTCP(mfsProvider, DataFactory.getDomainName(cateCode), cateCode, gradeName, payInst, payInstType));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tcpMap;
    }

    public static List<UserWallets> getUserDetails(User user, String userId) {
        String table1 = (user.CategoryCode.equals("SUBS")) ? "mtx_party" : "users";
        String table2 = (user.CategoryCode.equals("SUBS")) ? "mtx_party_access" : "user_phones";
        String table3 = (user.CategoryCode.equals("SUBS")) ? "mtx_party_m" : "users_m";

        List<UserWallets> walletList = new ArrayList<>();
        final String query = "select mp.status as PARTY_STATUS, mpa.status as ACCESS_STATUS, mw.status as WALLET_STATUS, mwb.balance, mwb.frozen_amount, mwb.fic, " +
                "mw.provider_id, mw.payment_type_id from " + table1 + " mp, " + table2 + " mpa, mtx_wallet mw, mtx_wallet_balances mwb where mp.user_id='" + userId + "' " +
                "and mp.user_id = mpa.user_id and mpa.user_id = mw.user_id and mw.wallet_number = mwb.wallet_number";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                String partyStatus = result.getString("PARTY_STATUS");
                String partyAccessStatus = result.getString("ACCESS_STATUS");
                String walletStatus = result.getString("WALLET_STATUS");
                BigDecimal balance = new BigDecimal(result.getString("BALANCE"));
                BigDecimal frozenAmount = new BigDecimal(result.getString("FROZEN_AMOUNT"));
                BigDecimal fic = new BigDecimal(result.getString("FIC"));
                String providerId = result.getString("PROVIDER_ID");
                String payId = result.getString("PAYMENT_TYPE_ID");
                walletList.add(new UserWallets(partyStatus, partyAccessStatus, walletStatus, balance, frozenAmount, fic, providerId, payId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return walletList;
    }

    public static String getBarredUserDetails(String userId) {
        ResultSet result = null;
        String data = null;
        String query = "select black_list_type_id from mtx_party_black_list where party_id = '" + userId + "'"; //user_id is unique in the system

        try {
            result = dbConn.RunQuery(query);
            while (result.next()){
                data = result.getString("BLACK_LIST_TYPE_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public static String[] getStatusOfTemplateBlacklist(String sourceName, String[] template) {
        String statusArray[] = new String[template.length];
        String status = null;
        String source = sourceName.toLowerCase();
        for (int i = 0; i < template.length; i++) {
            String query = "SELECT STATUS FROM NOTIFICATION_BLACKLIST WHERE SOURCE ='" + source + "' " +
                    "AND TEMPLATE_ID = (SELECT ID FROM NOTIFICATION_TEMPLATE WHERE NAME='" + template[i] + "')";
            try {
                ResultSet result = dbConn.RunQuery_infra(query);
                while (result.next()) {
                    status = result.getString("STATUS");
                    statusArray[i] = status;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return statusArray;
    }



    /**
     *
     * @param serviceType
     * @param catCode
     * @param providerID
     * @param payID
     * @return
     */
    public static String fetchApprovalLimitForTransaction(String serviceType, String catCode,String providerID,String payID) {

        BigDecimal approvalLimit = null;
        String query = "select APPROVAL_LIMIT from MTX_TRANSFER_APPROVER_LIMITS where TRANSFER_RULE_ID =" +
                "(select TRANSFER_RULE_ID from MTX_TRANSFER_RULES where SERVICE_TYPE = '"+serviceType+"'" +
                "AND PAYEE_CATEGORY_CODE='"+catCode+"' AND STATUS_ID='Y' AND PAYEE_PROVIDER_ID='"+providerID+"' AND PAYEE_PAYMENT_TYPE_ID='"+payID+"')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                approvalLimit = result.getBigDecimal("APPROVAL_LIMIT");
            }

            approvalLimit = approvalLimit.divide(AppConfig.currencyFactor);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return approvalLimit.toString();
    }

    public static List<UserWallets> getWalletDetailsAssociatedWithUser(User user) {
        List<UserWallets> walletList = new ArrayList<>();
        final String query = "select mw.status, mw.payment_type_id, mw.provider_id, mwb.balance, mwb.frozen_amount, mwb.fic from mtx_wallet mw, mtx_wallet_balances mwb " +
                "where mwb.wallet_number = mw.wallet_number and mw.wallet_number in (select wallet_number from mtx_wallet where msisdn = '"+user.MSISDN+"')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                String status = result.getString("status");
                String balance = result.getString("balance");
                String frozenAmount = result.getString("frozen_amount");
                String fic = result.getString("fic");
                walletList.add(new UserWallets(status, balance, frozenAmount, fic));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return walletList;
    }


    public static String checkForUniqueLoginID(String LoginID) {
        final String methodName = "checkForUniqueLoginID";
        String LoginIDStatus = null;
        final String query = "select case when exists (select 1 from users where Login_ID = '"+LoginID+"') then 'Y' else 'N' end as rec_exists from dual";
        try {
            ResultSet result = dbConn.RunQuery(query);
            result.next();
            LoginIDStatus = result.getString(1);
        }
        catch
                (Exception e) {
            e.printStackTrace();
        }
        return LoginIDStatus;
    }

}
