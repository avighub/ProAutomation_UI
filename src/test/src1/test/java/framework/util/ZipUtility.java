package framework.util;

import framework.util.globalConstant.Constants;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by navin.pramanik on 8/28/2017.
 */
public class ZipUtility {

    private static final int BYTE_CONSTANT = 2048;

    //for testing added main method
    public static void main(String[] args) {

        String zipFile = "Attachment_Zip_New1" + ".zip";
        String srcDir = System.getProperty("user.dir") + "\\reports\\";
        String outDir = System.getProperty("user.dir");
        try {
            File srcFile = new File(srcDir);
            File out = new File(outDir);
            zipDirectory(srcFile, out, zipFile);
        } catch (Exception ioe) {
            System.err.println("Error creating zip file: " + ioe);
        }
    }


    /**
     * @param directoryToCompress
     * @param outputDirectory
     * @param zipName
     */
    public static void zipDirectory(File directoryToCompress, File outputDirectory, String zipName) {
        try {
            FileOutputStream dest = new FileOutputStream(new File(outputDirectory, zipName));
            ZipOutputStream zipOutputStream = new ZipOutputStream(dest);

            zipSubdirectory(directoryToCompress, directoryToCompress, zipOutputStream);
            zipOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param rootDirectory
     * @param currentDirectory
     * @param out
     * @throws Exception
     */
    private static void zipSubdirectory(File rootDirectory, File currentDirectory, ZipOutputStream out) {

        try {

            byte[] data = new byte[BYTE_CONSTANT];

            File[] files = currentDirectory.listFiles();
            if (files != null) {
                // no files were found or this is not a directory
                for (File file : files) {
                    if (file.isDirectory()) {
                        zipSubdirectory(rootDirectory, file, out);
                    } else {
                        FileInputStream fi = new FileInputStream(file);
                        // creating structure and avoiding duplicate file names
                        String name = file.getAbsolutePath().replace(rootDirectory.getAbsolutePath(), Constants.BLANK_CONSTANT);

                        ZipEntry entry = new ZipEntry(name);
                        out.putNextEntry(entry);
                        int count;
                        BufferedInputStream origin = new BufferedInputStream(fi, BYTE_CONSTANT);
                        while ((count = origin.read(data, 0, BYTE_CONSTANT)) != -1) {
                            out.write(data, 0, count);
                        }
                        origin.close();
                    }
                }

            }


        } catch (FileNotFoundException fe) {
            System.err.println("File Not Found Exception Occured");
        } catch (IOException fe) {
            System.err.println("IO Exception Occured");
        }


    }
}
