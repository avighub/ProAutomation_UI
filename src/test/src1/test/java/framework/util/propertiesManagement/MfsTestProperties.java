package framework.util.propertiesManagement;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

public final class MfsTestProperties {

    public static final String PROPERTIES_FILE = "mfstest.properties";
    public static final String FILE_NAME_PROPERTY = "mfstest.propertiesFile";
    private static final Logger LOGGER = LoggerFactory.getLogger(MfsTestProperties.class);
    private static final MfsTestProperties ME = new MfsTestProperties();
    private static final int INVALID_VALUE = Integer.MIN_VALUE;
    private Properties properties = new Properties();

    public static final MfsTestProperties getInstance() {
        MfsTestProperties me = ME;
        me.init();
        return me;
    }

    private void loadProperties(InputStream propertiesStream) {
        try {
            properties.load(propertiesStream);
            setSystemProperties();
        } catch (Exception e) {
            LOGGER.error("Failed to load properties file", e);
        }
    }

    private void setSystemProperties() {
        Enumeration<?> propertyNames = properties.propertyNames();
        while (propertyNames.hasMoreElements()) {
            String name = (String) propertyNames.nextElement();
            String systemValue = System.getProperty(name);
            if (StringUtils.isBlank(systemValue)) {
                setProperty(name, StringUtils.trim(properties.getProperty(name)));
            } else {
                properties.put(name, systemValue);
            }
        }
    }

    public String getProperty(String name) {
        return properties.getProperty(name);
    }

    public boolean isPropertySetToYes(String name){
        if(properties.getProperty(name).equalsIgnoreCase("yes"))
            return true;
        else
            return false;
    }

    public String getProperty(String name, String defaultValue) {
        return properties.getProperty(name, defaultValue);
    }

    public int getIntProperty(String name) {
        return getIntProperty(name, INVALID_VALUE);
    }

    public long getLongProperty(String name) {
        return getLongProperty(name, INVALID_VALUE);
    }

    public boolean getBooleanProperty(String name) {
        return getBooleanProperty(name, false);
    }

    public int getIntProperty(String name, int defaultValue) {
        try {
            return Integer.parseInt(getProperty(name, "" + defaultValue));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public long getLongProperty(String name, long defaultValue) {
        try {
            return Long.parseLong(getProperty(name, "" + defaultValue));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public boolean getBooleanProperty(String name, boolean defaultValue) {
        try {
            return Boolean.parseBoolean(getProperty(name, "" + defaultValue));
        } catch (Exception e) {
            return false;
        }
    }

    private InputStream getPropertiesFile() {
        String propertyFile = System.getProperty(FILE_NAME_PROPERTY, PROPERTIES_FILE);
        return this.getClass().getResourceAsStream("/" + propertyFile);
    }

    public void setProperty(String name, String value) {
        properties.setProperty(name, value);
        System.setProperty(name, value);
    }

    public void setPropertyIfAbsent(String name, String value) {
        if (properties.getProperty(name) != null || System.getProperty(name) != null) {
            return;
        }
        setProperty(name, value);
    }


    public void clear() {
        Enumeration<?> propertyNames = properties.propertyNames();
        while (propertyNames.hasMoreElements()) {
            System.clearProperty((String) propertyNames.nextElement());
        }
        properties.clear();
    }

    public void init() {
        loadProperties(getPropertiesFile());
    }
}
