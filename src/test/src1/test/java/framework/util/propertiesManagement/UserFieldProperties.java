package framework.util.propertiesManagement;


import java.util.*;

/**
 * Created by ravindra.dumpa on 12/8/2017.
 */
public class UserFieldProperties {

    public static final String MONEY_LOCALE_LANGUAGE_PROPERTY = "money.locale.language";
    public static final String MONEY_LOCALE_COUNTRY_PROPERTY = "money.locale.country";
    private static final MfsTestProperties mfsTestProperties = MfsTestProperties.getInstance();
    private static String language = mfsTestProperties.getProperty(MONEY_LOCALE_LANGUAGE_PROPERTY, "en");
    private static String country = mfsTestProperties.getProperty(MONEY_LOCALE_COUNTRY_PROPERTY, "US");
    private static Locale locale = null;
    private static ResourceBundle resource = null;

    public static String getField(String labelCode) {

        if (locale == null) {
            locale = new Locale(language, country);
        }

        if (resource == null) {
            resource = ResourceBundle.getBundle("UserFields", locale);
        }

        String label = resource.getString(labelCode).trim();

        return label;

    }


    public static Object[] getLabels(String partialKey) {
        if (locale == null) {
            locale = new Locale(language, country);
        }

        if (resource == null) {
            resource = ResourceBundle.getBundle("UserFields", locale);
        }

        List<Object> expectedData = new ArrayList<>();
        List<Object> list = new ArrayList<>(resource.keySet());
        for (int i = 0; i < list.size(); i++) {
            String value = list.get(i).toString();
            if (value.contains(partialKey)) {
                Object data = resource.getString(value).trim();
                expectedData.add(data);
            }
        }
        Object[] array = expectedData.toArray();
        Arrays.sort(array);

        return array;
    }
}
