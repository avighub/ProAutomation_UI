package framework.util.propertiesManagement;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Created by rahul.rana on 6/15/2017.
 */
public class MsisdnProperties {
    public static final String PROPERTIES_FILE = "Config/msisdn.properties";
    public static final String FILE_NAME_PROPERTY = "msisdn.propertiesFile";
    private static final Logger LOGGER = LoggerFactory.getLogger(MsisdnProperties.class);
    private static final MsisdnProperties ME = new MsisdnProperties();
    private Properties properties = new Properties();

    public static final MsisdnProperties getInstance() {
        MsisdnProperties me = ME;
        me.init();
        return me;
    }

    public String getProperty(String name) {
        return properties.getProperty(name);
    }

    private void setSystemProperties() {
        Enumeration<?> propertyNames = properties.propertyNames();
        while (propertyNames.hasMoreElements()) {
            String name = (String) propertyNames.nextElement();
            String systemValue = System.getProperty(name);
            if (StringUtils.isBlank(systemValue)) {
                setProperty(name, StringUtils.trim(properties.getProperty(name)));
            } else {
                properties.put(name, systemValue);
            }
        }
    }

    public void setProperty(String name, String value) {
        properties.setProperty(name, value);
        System.setProperty(name, value);
    }

    public void setPropertyIfAbsent(String name, String value) {
        if (properties.getProperty(name) != null || System.getProperty(name) != null) {
            return;
        }
        setProperty(name, value);
    }

    private void loadProperties(InputStream propertiesStream) {
        try {
            properties.load(propertiesStream);
            setSystemProperties();
        } catch (Exception e) {
            LOGGER.error("Failed to load properties file", e);
        }
    }

    /**
     * @param key
     * @param value
     */
    public void writeProperty(String key, String value) {
        try {
            File file = new File("src/test/resources/Config/msisdn.properties");
            FileOutputStream fileOut = new FileOutputStream(file);
            properties.setProperty(key, value);
            properties.store(fileOut, null);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private InputStream getPropertiesFile() {
        String propertyFile = System.getProperty(FILE_NAME_PROPERTY, PROPERTIES_FILE);
        return this.getClass().getResourceAsStream("/" + propertyFile);
    }

    public void init() {
        loadProperties(getPropertiesFile());
    }
}
