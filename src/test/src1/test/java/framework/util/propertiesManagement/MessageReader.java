package framework.util.propertiesManagement;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MessageReader {

    public static final String MONEY_LOCALE_LANGUAGE_PROPERTY = "money.locale.language";
    public static final String MONEY_LOCALE_COUNTRY_PROPERTY = "money.locale.country";
    private static final MfsTestProperties mfsTestProperties = MfsTestProperties.getInstance();
    private static String language = mfsTestProperties.getProperty(MONEY_LOCALE_LANGUAGE_PROPERTY, "en");
    private static String country = mfsTestProperties.getProperty(MONEY_LOCALE_COUNTRY_PROPERTY, "US");
    private static ResourceBundle resourceBundle = null;
    private static ResourceBundle resourceBundleKpi = null;

    public static String validateMessage(String ActualMessage, String Code, Object... params) {
        String message = getMessage(Code, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params);
        }

        assertEquals(ActualMessage, message);
        return message;
    }

    /**
     * Get Dynamic message
     *
     * @param Code
     * @param params
     * @return
     */
    public static String getDynamicMessage(String Code, Object... params) {
        String message = getMessage(Code, null);
        return MessageFormat.format(message, params);
    }

    public static boolean loadResourceBundle(Locale locale) {
        if (locale == null) {
            locale = new Locale(language, country);
        }
        try {
            resourceBundle = ResourceBundle.getBundle("messages", locale);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    public static String getMessage(String code, Locale locale) {
        if (resourceBundle == null || locale != null) {
            if (!loadResourceBundle(locale))
                return "";
        }

        String str = "";
        try {
            str = resourceBundle.getString(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str.trim();
    }

    public static void validateMessageContains(String ActualMessage, String Code) {
        String message = getMessage(Code, null);
        assertTrue(ActualMessage.contains(message));
    }

    public static String getDBMessage(String code, Object... params) throws IOException {
        String message = null;
        Properties prop = new Properties();
        InputStream input = null;
        input = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/db_messages.properties");
        prop.load(input);
        message = prop.getProperty(code);
        message.trim();
        if (message.contains("{0}")) {
            message = MessageFormat.format(message, params);
        } else {

        }
        return message;
    }

    public static void main(String[] args) throws IOException {
        /*String message = getDBMessage("200","Surya","Kanta","Dhal");
        System.out.println(message);*/

        String message = "Your account add initiated successfully.Login Id:{0} Password:{1}";

        MessageFormat temp = new MessageFormat(message);
        System.out.println(temp.getFormats().length);

    }

    public static boolean loadKpiResourceBundle(Locale locale) {
        if (locale == null) {
            locale = new Locale(language, country);
        }
        try {
            resourceBundleKpi = ResourceBundle.getBundle("Kpi", locale);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static String getKpiMessage(String code, Locale locale) {
        if (resourceBundleKpi == null || locale != null) {
            if (!loadKpiResourceBundle(locale))
                return "";
        }

        String str = "";
        try {
            str = resourceBundleKpi.getString(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str.trim();
    }
}
