/*
 * ******************************************************************************
 *  * COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  * <p>
 *  * This software is the sole property of Comviva and is protected by copyright
 *  * law and international treaty provisions. Unauthorized reproduction or
 *  * redistribution of this program, or any portion of it may result in severe
 *  * civil and criminal penalties and will be prosecuted to the maximum extent
 *  * possible under the law. Comviva reserves all rights not expressly granted.
 *  * You may not reverse engineer, decompile, or disassemble the software, except
 *  * and only to the extent that such activity is expressly permitted by
 *  * applicable law notwithstanding this limitation.
 *  * <p>
 *  * THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  * EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  * WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  * YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  * Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  * USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  * OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 */

package framework.util.common;

import framework.util.Balance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public final class ProcessingHelper {

    public static final String CORE_POOL_SIZE_PROPERTY = "mfsqa.executor.threadPool.corePoolSize";
    public static final String MAX_POOL_SIZE_PROPERTY = "mfsqa.executor.threadPool.maxPoolSize";
    public static final String QUEUE_SIZE_PROPERTY = "mfsqa.executor.threadPool.queueSize";
    public static final String KEEP_ALIVE_PROPERTY = "mfsqa.executor.threadPool.keepAliveInMillis";
    static final String DEFAULT_CORE_POOL_SIZE = "" + Runtime.getRuntime().availableProcessors();
    static final String DEFAULT_MAX_POOL_SIZE = "" + Runtime.getRuntime().availableProcessors() * 6;
    static final String DEFAULT_QUEUE_SIZE = "2000";
    static final String DEFAULT_KEEP_ALIVE = "30000";
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessingHelper.class);
    private static ExecutorService cachedThreadPool;

    private ProcessingHelper() {
    }


    private static int getCorePoolSize() {
        return Integer.parseInt(System.getProperty(CORE_POOL_SIZE_PROPERTY, DEFAULT_CORE_POOL_SIZE));
    }

    private static int getMaxPoolSize() {
        return Integer.parseInt(System.getProperty(MAX_POOL_SIZE_PROPERTY, DEFAULT_MAX_POOL_SIZE));
    }

    private static int getQueueSize() {
        return Integer.parseInt(System.getProperty(QUEUE_SIZE_PROPERTY, DEFAULT_QUEUE_SIZE));
    }

    private static long getKeepAlive() {
        return Long.parseLong(System.getProperty(KEEP_ALIVE_PROPERTY, DEFAULT_KEEP_ALIVE));
    }

    public static <T> Future<T> fireCallable(Callable<T> callable) {
        initThreadPool();
        return cachedThreadPool.submit(callable);
    }

    public static Future<?> fireRunnable(Runnable runnable) {
        initThreadPool();
        return cachedThreadPool.submit(runnable);
    }

    public static void awaitFuture(Future... futures)
            throws ExecutionException, InterruptedException {
        for (Future future : futures) {
            future.get();
        }
    }

    public static <T> Future<T> futureOf(Supplier<T> supplier) {
        return fireCallable(() -> supplier.get());
    }

    public static void awaitBalance(Balance... balances)
            throws ExecutionException, InterruptedException {
        for (Balance balance : balances) {
            balance.amount();
        }
    }

    private static void initThreadPool() {
        if (cachedThreadPool != null) {
            return;
        }
        synchronized (ProcessingHelper.class) {
            if (cachedThreadPool == null) {
                cachedThreadPool = new ThreadPoolExecutor(getCorePoolSize() + 1, getMaxPoolSize() + 1, getKeepAlive(),
                        TimeUnit.MILLISECONDS,
                        new ArrayBlockingQueue<>(getQueueSize()),
                        makeThreadFactory("mfsqa.chain.executor"), getRejectedExecutionHandler());
                submitDummyTask();
            }
        }
    }

    private static void submitDummyTask() {
        cachedThreadPool.submit(new Runnable() {
            @Override
            public synchronized void run() {
                try {
                    wait();
                } catch (InterruptedException e) {
                    LOGGER.error("Dummy task ended.", e);
                }
            }
        });
    }

    private static RejectedExecutionHandler getRejectedExecutionHandler() {
        return new ThreadPoolExecutor.CallerRunsPolicy();
    }


    private static ThreadFactory makeThreadFactory(String name) {
        return new Factory(name);
    }


    private static final class Factory
            implements ThreadFactory, Thread.UncaughtExceptionHandler {
        private final AtomicInteger counter = new AtomicInteger();
        private final ThreadGroup group;

        Factory(String groupName) {
            group = new ThreadGroup(groupName);
            group.setDaemon(true);
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r, group.getName() + '-' + counter.getAndIncrement());
            t.setDaemon(true);
            t.setUncaughtExceptionHandler(this);
            return t;
        }

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            LOGGER.error("Uncaught exception in " + t.getName() + ". Thread exiting.", e);
        }
    }

}
