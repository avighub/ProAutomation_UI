/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: Surya Dhal & Navin Kumar Pramanik
 *  Date:
 *  Purpose:
 */
package framework.util.common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.SysWalletBalance;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;

import java.io.IOException;
import java.math.BigDecimal;


public class DBAssertion {

    public static BigDecimal prePayerBal, prePayeeBal, preRecon, postPayerBal, postPayeeBal, postRecon, requestedAmount;
    private static ExtentTest pNode;

    public static DBAssertion init(ExtentTest t1) {
        pNode = t1;
        return new DBAssertion();
    }

    /**
     * Common Method to verify GUI message with DB message.
     *
     * @param actual
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static void verifyDBAssertionContain(String actual, String expected, String message, ExtentTest node) throws IOException {
        String code[][] = {{"DB Assertion", message}, {"Expected", expected}, {"Actual", actual}};
        Markup m = MarkupHelper.createTable(code);
        try {
            if (actual.contains(expected)) {
                node.pass(m);
            } else {
                node.fail(m);
            }
        } catch (Exception e) {
            node.fail("" + e);
        }
    }

    /**
     * Verify DB Assertion values are equal
     *
     * @param actual
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static void verifyDBAssertionEqual(String actual, String expected, String message, ExtentTest node) throws IOException {
        String code[][] = {{"DB Assertion", message}, {"Expected", expected}, {"Actual", actual}};
        Markup m = MarkupHelper.createTable(code);

        if (actual.equals(expected)) {
            node.pass(m);
        } else {
            node.fail(m);
        }
    }

    /**
     * Verify DB assertion, value snot equal
     *
     * @param actual
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static void verifyPrePostBalanceNotEqual(BigDecimal actual, BigDecimal expected, String message, ExtentTest node) throws IOException {
        String code[][] = {{"DB Assertion", "Verify NOT EQUAL:" + message}, {"Expected", expected.toString()}, {"Actual", actual.toString()}};
        Markup m = MarkupHelper.createTable(code);

        if (!actual.equals(expected)) {
            node.pass(m);
        } else {
            node.fail(m);
        }
    }

    /**
     * Verify Pre Post Balance
     *
     * @param actual
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static void verifyPrePostBalanceEqual(BigDecimal actual, BigDecimal expected, String message, ExtentTest node) throws IOException {
        String code[][] = {{"DB Assertion", message}, {"Pre-Balance", expected.toString()}, {"Post-Balance", actual.toString()}};
        Markup m = MarkupHelper.createTable(code);

        if (actual.equals(expected)) {
            node.pass(m);
        } else {
            node.fail(m);
        }
    }


    /**
     * Verify Pre Post balance for a User also verify the Recon balance
     *
     * @param postPayerBal
     * @param prePayerBal
     * @param postPayeeBal
     * @param prePayeeBal
     * @param preRecon
     * @param postRecon
     * @param node
     * @throws IOException
     */
    public static void verifyUserAndReconBalance(BigDecimal postPayerBal, BigDecimal prePayerBal, BigDecimal postPayeeBal, BigDecimal prePayeeBal, BigDecimal preRecon, BigDecimal postRecon, ExtentTest node) throws IOException {
        String txnStatus = MobiquityDBAssertionQueries.dbTxnStatus;
        if (txnStatus != null) {
            if (txnStatus.equals(Constants.TXN_STATUS_SUCCESS)) {
                DBAssertion.verifyPrePostBalanceNotEqual(postPayerBal, prePayerBal,
                        "DB Payer Transaction Amount Verification", node);

                DBAssertion.verifyPrePostBalanceNotEqual(postPayeeBal, prePayeeBal,
                        "DB Payee Transaction Amount Verification", node);
            } else if (txnStatus.equals(Constants.TXN_STATUS_FAIL)) {
                DBAssertion.verifyPrePostBalanceEqual(postPayerBal, prePayerBal,
                        "DB Payer Transaction Amount Verification", node);

                DBAssertion.verifyPrePostBalanceEqual(postPayeeBal, prePayeeBal,
                        "DB Payee Transaction Amount Verification", node);
            }
        }
        DBAssertion.verifyDBAssertionContain(preRecon.toString(), postRecon.toString(), "DB Recon Balance verified Successfully", node);
    }

    /**
     * verifyDBRecon
     *
     * @param reconPre  : Pre recon
     * @param reconPost : Post Recon
     */
    public static void verifyDBRecon(BigDecimal reconPre, BigDecimal reconPost) {

        String code[][] = {{"DB Assertion", "Verify Pre Post RECON"}, {"Pre-Recon", reconPre.toString()}, {"Post-Recon", reconPost.toString()}};
        Markup m = MarkupHelper.createTable(code);
        if (reconPre.compareTo(reconPost) == 0) {
            pNode.pass(m);
        } else {
            pNode.fail(m);
            Assertion.markAsFailure("Failed to Verify that Pre Post Recon are Same!");
        }
    }

    /**
     * verifyStockTransactionPreAndPost
     *
     * @param amount
     * @param node
     * @throws IOException
     */
    public static void verifyStockTransactionPreAndPost(String amount, ExtentTest node) throws IOException {
        Markup markup = MarkupHelper.createLabel("DB Assertion : verifyStockTransactionPreAndPost", ExtentColor.GREEN);
        node.info(markup);
        String code = null;
        Markup m = null;

        BigDecimal prePayee = new BigDecimal(prePayeeBal.toString());
        BigDecimal postPayee = new BigDecimal(postPayeeBal.toString());
        BigDecimal prePayer = new BigDecimal(prePayerBal.toString());
        BigDecimal postPayer = new BigDecimal(postPayerBal.toString());
        BigDecimal amt = new BigDecimal(amount);

        BigDecimal payerBalDiff = prePayer.subtract(postPayer).divide(AppConfig.currencyFactor);
        BigDecimal payeeBalDiff = postPayee.subtract(prePayee).divide(AppConfig.currencyFactor);


        if (payeeBalDiff.equals(amt)) {
            code = "Payee Pre Balance: " + prePayee + "\nPayee Post Balance: " + postPayee + "\nBalance Difference Expected : " + amt + "\nBalance Difference Actual: " + payeeBalDiff;
            m = MarkupHelper.createCodeBlock(code);
            node.pass(m);
        } else {
            code = "Payee Pre Balance : " + prePayee + "\nPayee Post Balance: " + postPayee + "\nBalance Difference Expected : " + amt + "\nBalance Difference Actual: " + payeeBalDiff;
            m = MarkupHelper.createCodeBlock(code);
            node.fail(m);
        }

        if (payerBalDiff.equals(amt)) {
            code = "Payer Pre Balance: " + prePayer + "\nPayer Post Balance: " + postPayer + "\nBalance Difference Expected :" + amt + "\nBalance Difference Actual:" + payerBalDiff;
            m = MarkupHelper.createCodeBlock(code);
            node.pass(m);
        } else {
            code = "Payer Pre Balance: " + prePayer + "\nPayer Post Balance: " + postPayer + "\nBalance Difference Expected :" + amt + "\nBalance Difference Actual :" + payerBalDiff;
            m = MarkupHelper.createCodeBlock(code);
            node.fail(m);
        }
    }

    /**
     * Verify DB Message Contains
     *
     * @param expected
     * @param actual
     * @param node
     * @throws IOException
     */
    public static void verifyDBMessageContains(String actual, String expected, String message, ExtentTest node, Object... params) throws Exception {
        if (!ConfigInput.dbMsgVerificationRequired)
            return;

        String expectedMsg = MessageReader.getDBMessage(expected, params);
        expectedMsg = expectedMsg.split("\\{")[0];
        String code[][] = {{"Verify DB", message}, {"Actual", actual.toString()}, {"Expected", expectedMsg.toString()}};
        Markup m = MarkupHelper.createTable(code);

        if (actual.contains(expectedMsg)) {
            node.pass(m);
        } else {
            node.fail(m);
        }
    }

    public static void verifyIMTUserPayerAndPayeeBalance(String txnID, ExtentTest node) throws IOException {
        String txnStatus = MobiquityDBAssertionQueries.dbTxnStatus;
        BigDecimal actualPayerPreBalance = null, actualPayerPostBalance = null, actualPayeePreBalance = null, actualPayeePostBalance = null;
        if (txnStatus != null) {
            if (txnStatus.equals(Constants.TXN_STATUS_SUCCESS)) {
                BigDecimal requestedAmount = MobiquityDBAssertionQueries.getRequestedAmount(txnID);

                String payerWalletNo = MobiquityDBAssertionQueries.getWalletNumberFromItemsTable(txnID, "PAYER");
                String payeeWalletNo = MobiquityDBAssertionQueries.getWalletNumberFromItemsTable(txnID, "PAYEE");

                actualPayerPreBalance = MobiquityDBAssertionQueries.getPreviousBalance(payerWalletNo);
                actualPayerPostBalance = MobiquityDBAssertionQueries.getWalletBalance(payerWalletNo);
                BigDecimal expectedPayerPostBalance = new BigDecimal(String.valueOf(actualPayerPreBalance.subtract(requestedAmount)));

                actualPayeePreBalance = MobiquityDBAssertionQueries.getPreviousBalance(payeeWalletNo);
                actualPayeePostBalance = MobiquityDBAssertionQueries.getWalletBalance(payeeWalletNo);
                BigDecimal expectedPayeePostBalance = new BigDecimal(String.valueOf(actualPayeePreBalance.add(requestedAmount)));


                DBAssertion.verifyDBAssertionEqual(actualPayerPostBalance.toString(), expectedPayerPostBalance.toString(),
                        "Verify Payer Post Balance", node);

                DBAssertion.verifyDBAssertionEqual(actualPayeePostBalance.toString(), expectedPayeePostBalance.toString(),
                        "Verify Payee Post Balance", node);
            } else if (txnStatus.equals(Constants.TXN_STATUS_FAIL) || txnStatus.equals(Constants.TXN_STATUS_INITIATED)) {
                DBAssertion.verifyPrePostBalanceEqual(actualPayerPostBalance, actualPayerPreBalance,
                        "Verify Payer Balance", node);
                DBAssertion.verifyPrePostBalanceNotEqual(actualPayeePostBalance, actualPayeePreBalance,
                        "Verify Payee Balance", node);
            }
        }

    }

    /**
     * @param preBalance
     * @param postBalance
     * @param txnAmount
     * @throws IOException
     * @deprecated
     */
    public void verifyPrePostBalanceForPayerAndPayee(SysWalletBalance preBalance, SysWalletBalance postBalance, String txnAmount) throws IOException {
        Markup m = MarkupHelper.createLabel("verifyPrePostBalanceForPayerAndPayee", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            Assertion.verifyEqual(preBalance.getReconBalance(),
                    postBalance.getReconBalance(), "Verify Pre and Post Recon Balance", pNode);
            Assertion.verifyAccountIsDebited(preBalance.getPayerBalance(),
                    postBalance.getPayerBalance(), new BigDecimal(txnAmount),
                    "Verify Payer Account is Debited", pNode);
            Assertion.verifyAccountIsCredited(preBalance.getPayeeBalance(),
                    postBalance.getPayeeBalance(), new BigDecimal(txnAmount),
                    "Verify Payee Account is Credited", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Verify Balance post Trnsaction
     *
     * @param txnId
     * @param txnAmount
     * @param preReconBal
     * @throws IOException
     */
    public void verifyPrePostBalanceForTransaction(String txnId, String txnAmount, BigDecimal preReconBal) throws IOException {
        Markup m = MarkupHelper.createLabel("verifyPrePostBalanceForTransaction", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            BigDecimal payeePreBalance = MobiquityDBAssertionQueries
                    .getPrePostBalanceUsingTxnId(txnId, Constants.USER_TYPE_PAYEE).get("PREVIOUS_BALANCE");

            BigDecimal payeePostBalance = MobiquityDBAssertionQueries
                    .getPrePostBalanceUsingTxnId(txnId, Constants.USER_TYPE_PAYEE).get("BALANCE");

            BigDecimal payerPreBalance = MobiquityDBAssertionQueries
                    .getPrePostBalanceUsingTxnId(txnId, Constants.USER_TYPE_PAYER).get("PREVIOUS_BALANCE");

            BigDecimal payerPostBalance = MobiquityDBAssertionQueries
                    .getPrePostBalanceUsingTxnId(txnId, Constants.USER_TYPE_PAYER).get("BALANCE");

            Assertion.verifyEqual(preReconBal,
                    MobiquityDBAssertionQueries.getReconBalance(), "Verify Pre and Post Recon Balance", pNode);

            Assertion.verifyAccountIsDebited(payerPreBalance,
                    payerPostBalance, new BigDecimal(txnAmount),
                    "Verify Payer Account is Debited", pNode);

            Assertion.verifyAccountIsCredited(payeePreBalance,
                    payeePostBalance, new BigDecimal(txnAmount),
                    "Verify Payee Account is Credited", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

}
