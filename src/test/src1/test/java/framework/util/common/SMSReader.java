package framework.util.common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.common.DesEncryptor;
import framework.util.dbManagement.OracleDB;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.MessageReader;
import framework.util.propertiesManagement.MfsTestProperties;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.apache.http.protocol.HTTP.USER_AGENT;

public class SMSReader {
    private static ExtentTest pNode;
    private static WebDriver driver;

    public static SMSReader init(ExtentTest t1) throws Exception {
        pNode = t1;
        return new SMSReader();
    }

    /**
     * Verify The most recent Notification
     *
     * @param msisdn  - User / Operator Msisdn
     * @param msgCode - Msg code as mentioned under the Property Resource bundle
     * @param params  - Optional Parameters for Message to be validated (to get the Dynamic Message)
     * @throws IOException
     */
    public void verifyRecentNotification(String msisdn, String msgCode, String... params) throws IOException, InterruptedException {
        if (!getNotifications(msisdn, pNode).isEmpty()) {
            String message = MessageReader.getMessage(msgCode, null);

            if (params != null && params.length != 0) {
                message = MessageFormat.format(message, params); // dynamic Message
            }

            List<String> notificationList = getNotifications(msisdn, pNode);
            Thread.sleep(2000);
            String lastNotification = notificationList.get(0);
            Thread.sleep(2000);
            if (lastNotification.contains(message)) {
                pNode.pass("Successfully verified notification msg contains - " + message);
            } else {
                Assertion.markAsFailure("Verification fail");
                String code[][] = {{"Verification Failed - Expected: ", message}, {"Last Notification: ", lastNotification}, {"Complete Msg: ", notificationList.toString()}};
                Markup m = MarkupHelper.createTable(code);
                pNode.fail(m);
            }
        } else {
            verifyNotificationReceivedFormDB(msgCode, msgCode, params);
        }
    }

    /**
     * Verify that the message is present in all available notifications
     *
     * @param msisdn  - User / Operator Msisdn
     * @param msgCode - Msg code as mentioned under the Property Resource bundle
     * @param params  - Optional Parameters for Message to be validated (to get the Dynamic Message)
     * @throws IOException
     */
    public void verifyNotificationContain(String msisdn, String msgCode, String... params) throws IOException {
        if (!getNotifications(msisdn, pNode).isEmpty()) {
            String message = MessageReader.getMessage(msgCode, null);

            if (params != null && params.length != 0) {
                message = MessageFormat.format(message, params); // dynamic Message
            }

            List<String> notificationList = getNotifications(msisdn, pNode);
            if (notificationList.toString().contains(message)) {
                pNode.pass("Successfully verified notification msg contains - " + message);
            } else {
                Assertion.markAsFailure("Verification fail");
                String code[][] = {{"Verification Failed: ", "Verify Notification Contains"}, {"Expected: ", message}, {"Complete Msg: ", notificationList.toString()}};
                Markup m = MarkupHelper.createTable(code);
                pNode.fail(m);
            }
        } else {
            verifyNotificationReceivedFormDB(msisdn, msgCode, params);
        }
    }

    public String getLastNotification(String msisdn) throws IOException {
        List<String> notificationList = getNotifications(msisdn, pNode);
        return notificationList.get(0);
    }

    /**
     * @param msisdn
     * @param msgCode
     * @param params
     * @return
     * @throws IOException
     */
    public String fetchAmountFromNotification(String msisdn, String msgCode, String... params) throws IOException {
        String message = MessageReader.getMessage(msgCode, null);
        String amount = "";
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        List<String> notificationList = getNotifications(msisdn, pNode);

        //to fetch recent notification
        String msg = notificationList.get(0);
        String msg2 = msg.toString();
        pNode.info(msg2);

        //split when u find "USD"
        String[] msg3 = msg2.split("USD");
        System.out.println(msg3[0]);

        String[] msgs = msg3[0].split(" ");

        for (String mssg : msgs) {
            // below statement will fetch only the "amount" in the given string i.e "msgs"
            // if it starts frm 0-9 (48-57), then fetch that string
            if (mssg.charAt(0) >= 48 && mssg.charAt(0) <= 57) {
                System.out.println(mssg);
                amount = mssg;
            }
        }
        return amount;
    }


    /**
     * Get all notofications
     *
     * @param msisdn
     * @param t1
     * @return
     * @throws IOException
     */
    private List<String> getNotifications(String msisdn, ExtentTest t1) throws IOException {
        List<String> msgList = new ArrayList<>();
        try {
            // create a new driver instance
            driver = DriverFactory.createDriver();
            driver.get(MfsTestProperties.getInstance().getProperty("notification.inbox"));
            t1.info("Navigate to Notification Inbox");
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            try {
                if (driver.findElement(By.xpath("//h1[text() = 'Connection refused']")).isDisplayed()) {
                    t1.info("SMS Notification Simulator Is Offline. Verifying SMS From DB",
                            MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot(driver)).build());
                    return new LinkedList<>();
                }
            } catch (Exception e) {
                driver.findElement(By.id("search-input")).sendKeys("91" + msisdn);
                driver.findElement(By.id("search-input")).sendKeys(Keys.RETURN);
                t1.info("Set Msisdn - " + msisdn);

                // get the WebElements
                List<WebElement> notificationElementList = driver.findElements(By.xpath("//*[@id='message-table']/tr/td[2]/div"));

                // get the Strings
                for (WebElement cell : notificationElementList) {
                    msgList.add(Utils.removeExtraSpace(cell.getText()));
                }
                //capture the Notification screen
                t1.info("Validating SMS Notification From Simulator", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot(driver)).build());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            driver.quit();
        }
        return msgList;
    }


    /**
     * @param msisdn : Pass MSISDN of the USER of which you want Message
     * @param t1     : Extent Test node to capture logs in Extent Report
     * @return
     * @throws Exception
     * @Method :getLatestMessageFromNotificationUrl
     * @Purpose: This will fetch Latest Message From the Notification URL.
     * It is one API provided to fetch Latest SMS delivered to the given MSISDN.
     * In this method one HTTP request is sent and Response is captured and returned from the method.
     */
    public static String getLatestMessageFromNotificationUrl(String msisdn, ExtentTest t1) throws Exception {

        try {
            String url = MfsTestProperties.getInstance().getProperty("notification.url");
            String loginPassword = MfsTestProperties.getInstance().getProperty("internal.api.security.user.name") + ":"
                    + MfsTestProperties.getInstance().getProperty("internal.api.security.user.password");

            url = url + msisdn;
            System.out.println("############### URL: " + url);
            System.out.println("############### Login Password: " + loginPassword);
            String encoded = new sun.misc.BASE64Encoder().encode(loginPassword.getBytes());

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Authorization", "Basic " + encoded);
            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            //get the response code
            int responseCode = con.getResponseCode();
            System.out.println("Sending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        return null;
    }

    /** @see SMSReader
     * @apiNote for validation notification SMS from sent sms table.
     * @apiNote use this in case of notification simulator is offline.
     */
    public void verifyNotificationReceivedFormDB(String mobNum, String expectedMessage, Object... params) {
        try {
            String expMsg = MessageReader.getDynamicMessage(expectedMessage, params);
            OracleDB dbConn = new OracleDB();
            DesEncryptor decrypt = new DesEncryptor();

            String getMessage = "select * from(select message from sentsms where MSISDN = '" + mobNum + "' order by deliveredon desc) where rownum = 1";

            ResultSet result = dbConn.RunQuery(getMessage);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            String actMsg = null;

            while (result.next()) {
                String encryptedMessage = result.getString("MESSAGE");
                actMsg = decrypt.decrypt(encryptedMessage);
            }

            if (actMsg.contains(expMsg)) {
                Assertion.verifyContains(actMsg, expMsg, "Notification Received Successfully", pNode);
            } else {
                pNode.fail("Notification Not Found In Sent SMS Table");
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
