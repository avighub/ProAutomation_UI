package framework.util.common;

import framework.util.propertiesManagement.MfsTestProperties;

/**
 * Created by rahul.rana on 5/6/2017.
 */
public class Application {
    private static final MfsTestProperties APPLICATION_PROPERTIES = MfsTestProperties.getInstance();

    /**
     * Web Login
     */
    public static void openeWebApp() {
        String webUrl = APPLICATION_PROPERTIES.getInstance().getProperty("web.url");
        DriverFactory.getDriver().get(webUrl);
    }

    public static void openTxn() {
        String txnUrl = APPLICATION_PROPERTIES.getInstance().getProperty("txn.url");
        DriverFactory.getDriver().get(txnUrl);
    }
}
