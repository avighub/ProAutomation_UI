package framework.util.common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.MessageCodes;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class SentSMS {

    public static String payeeExpectedMessage, payerExpectedMessage;
    private static String payeeLastName = null, payeeFirstName = null, payeeMsisdn = null, payerLastName = null, payerFirstName = null, payerMsisdn = null, payeeMFSProvider, payerMFSProvider;
    private static BigDecimal payerNewBalance = null, payeeNewBalance = null;
    //TODO - Assign values From DataBase.
    private static String totalTaxPaid = null, commissionTax = null, serviceChargeTax = null, txnAmountTax = null, allCharges = null, commission = null;
    private static String payerCategory = null, payeeCategory = null;
    private static BigDecimal txnAmount = null;
    private static String transactionID = null;
    private static String dateTime = null, agentCode = null;
    private static String serviceCharges, totalamount;
    private static String currency = AppConfig.defaultCurrency;


    /**
     * verifyPayerPayeeDBMessage
     * The method will verify the Payer and Payee Message from DB
     *
     * @param payer
     * @param payee
     * @param serviceType
     * @param txnID
     * @param test
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void verifyPayerPayeeDBMessage(Object payer, Object payee, String serviceType, String txnID, ExtentTest test) throws IOException, SQLException, ClassNotFoundException {

        if (ConfigInput.dbMsgVerificationRequired) {
            //performSMSVerification(payer, payee, serviceType, txnID, test);
            performSMSVerificationPayer(payer, payee, serviceType, txnID, test);
            performSMSVerificationPayee(payer, payee, serviceType, txnID, test);
        } else {
            test.info("SMS verification is Disabled. You can enable it in 'msftest.properties' file");
        }
    }


    public static void verifyPayeeDBMessage(Object payer, Object payee, String serviceType, String txnID, ExtentTest test) throws IOException, SQLException, ClassNotFoundException {
        if (ConfigInput.dbMsgVerificationRequired) {
            performSMSVerificationPayee(payer, payee, serviceType, txnID, test);
        } else {
            test.info("SMS verification is Disabled. You can enable it in 'msftest.properties' file");
        }
    }

    public static void verifyPayerDBMessage(Object payer, Object payee, String serviceType, String txnID, ExtentTest test) throws IOException, SQLException, ClassNotFoundException {
        if (ConfigInput.dbMsgVerificationRequired) {
            performSMSVerificationPayer(payer, payee, serviceType, txnID, test);
        } else {
            test.info("SMS verification is Disabled. You can enable it in 'msftest.properties' file");
        }
    }

    /**
     * The below method is used to perform SMS Verification of Payer
     * performSMSVerificationPayer
     *
     * @param payerObj
     * @param payeeObj
     * @param serviceType
     * @param txnId
     * @param extentTest
     */
    private static void performSMSVerificationPayer(Object payerObj, Object payeeObj, String serviceType, String txnId, ExtentTest extentTest) {
        try {
            Markup markup = MarkupHelper.createLabel("DB SMS Assertion : Verify Payer Message Applied.", ExtentColor.BLUE);
            extentTest.info(markup);
            transactionID = txnId;

            String txnStatus = MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId);
            if ((txnStatus.equals(Constants.TXN_STATUS_SUCCESS)) || (txnStatus.equals(Constants.TXN_STATUS_INITIATED))) {

                payerMFSProvider = DataFactory.getDefaultProvider().ProviderName;
                payeeMFSProvider = DataFactory.getDefaultProvider().ProviderName;

                txnAmount = MobiquityDBAssertionQueries.getRequestedAmount(txnId);
                txnAmount = txnAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

                setPayerDetails(payerObj);
                setPayeeDetails(payeeObj);

                String payerMessageCode = getMessageCodeUsingServiceCode(serviceType).get("PAYER_SUCCESS_MSG_CODE");

                payerExpectedMessage = MobiquityDBAssertionQueries.getUserExpectedMessages(payerMessageCode, payerMsisdn, payerCategory);

                String payerActualMessage = MobiquityDBAssertionQueries.getSMSAppliedFromSentSMS(txnId, payerMsisdn);

                if (payerActualMessage == null) {
                    payerActualMessage = MobiquityDBAssertionQueries.getSMSAppliedFromSMSDelivered(txnId, payerMsisdn);
                }

                //For decrypting of Message
                DesEncryptor d = new DesEncryptor();
                if (payerActualMessage != null) {
                    payerActualMessage = d.decrypt(payerActualMessage);
                    checkPayerMessage(extentTest);
                    splitPayerMessage();
                    verifyPayerMessage(payerActualMessage, extentTest);
                } else {
                    extentTest.info("Payer Message is not available in DB");
                }
            } else {
                extentTest.info("SMS verification is not done as Transaction Failed");
            }
        } catch (MoneyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * The below method is used to perform SMS Verification of Payer
     * Method : performSMSVerificationPayee
     *
     * @param payerObj
     * @param payeeObj
     * @param serviceType
     * @param txnId
     * @param extentTest
     */
    private static void performSMSVerificationPayee(Object payerObj, Object payeeObj, String serviceType, String txnId, ExtentTest extentTest) {
        try {
            Markup markup = MarkupHelper.createLabel("DB SMS Assertion : Verify Payee Message Applied.", ExtentColor.BLUE);
            extentTest.info(markup);
            transactionID = txnId;

            String txnStatus = MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId);
            if ((txnStatus.equals(Constants.TXN_STATUS_SUCCESS)) || (txnStatus.equals(Constants.TXN_STATUS_INITIATED))) {

                payeeMFSProvider = DataFactory.getDefaultProvider().ProviderName;

                txnAmount = MobiquityDBAssertionQueries.getRequestedAmount(txnId);
                txnAmount = txnAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

                setPayeeDetails(payeeObj);
                setPayerDetails(payerObj);

                String payeeMessageCode = getMessageCodeUsingServiceCode(serviceType).get("PAYEE_SUCCESS_MSG_CODE");

                payeeExpectedMessage = MobiquityDBAssertionQueries.getUserExpectedMessages(payeeMessageCode, payeeMsisdn, payeeCategory);

                String payeeActualMessage = MobiquityDBAssertionQueries.getSMSAppliedFromSentSMS(txnId, payerMsisdn);

                if (payeeActualMessage == null) {
                    payeeActualMessage = MobiquityDBAssertionQueries.getSMSAppliedFromSMSDelivered(txnId, payeeMsisdn);
                }

                //For decrypting of Message
                DesEncryptor d = new DesEncryptor();
                if (payeeActualMessage != null) {
                    payeeActualMessage = d.decrypt(payeeActualMessage);
                    checkPayeeMessage(extentTest);
                    splitPayeeMessage();
                    verifyPayeeMessage(payeeActualMessage, extentTest);
                } else {
                    extentTest.info("Payee Message is not available in DB");
                }

            } else {
                extentTest.info("SMS verification is not done as Transaction Failed");
            }
        } catch (MoneyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void performSMSVerification(Object payerObj, Object payeeObj, String serviceType, String txnId, ExtentTest extentTest) {
        try {
            Markup markup = MarkupHelper.createLabel("DB SMS Assertion : Verify Payer And Payee Message Applied.", ExtentColor.BLUE);
            extentTest.info(markup);
            transactionID = txnId;

            String txnStatus = MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId);
            if ((txnStatus.equals(Constants.TXN_STATUS_SUCCESS)) || (txnStatus.equals(Constants.TXN_STATUS_INITIATED))) {

                payeeMFSProvider = DataFactory.getDefaultProvider().ProviderName;
                payerMFSProvider = DataFactory.getDefaultProvider().ProviderName;

                txnAmount = MobiquityDBAssertionQueries.getRequestedAmount(txnId);
                txnAmount = txnAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

                setPayerPayeeDetails(payerObj, payeeObj);

                /*String payeeMessageCode = MobiquityDBAssertionQueries.getMessageCode(serviceType).get("PAYEE_SUCCESS_MSG_CODE");
                String payerMessageCode = MobiquityDBAssertionQueries.getMessageCode(serviceType).get("PAYER_SUCCESS_MSG_CODE");*/
                String payerMessageCode = getMessageCodeUsingServiceCode(serviceType).get("PAYER_SUCCESS_MSG_CODE");
                String payeeMessageCode = getMessageCodeUsingServiceCode(serviceType).get("PAYEE_SUCCESS_MSG_CODE");

                payeeExpectedMessage = MobiquityDBAssertionQueries.getUserExpectedMessages(payeeMessageCode, payeeMsisdn, payeeCategory);
                payerExpectedMessage = MobiquityDBAssertionQueries.getUserExpectedMessages(payerMessageCode, payerMsisdn, payerCategory);

                String payeeActualMessage = MobiquityDBAssertionQueries.getSMSAppliedFromSentSMS(txnId, payeeMsisdn);
                String payerActualMessage = MobiquityDBAssertionQueries.getSMSAppliedFromSentSMS(txnId, payerMsisdn);

                if (payeeActualMessage == null || payerActualMessage == null) {
                    payeeActualMessage = MobiquityDBAssertionQueries.getSMSAppliedFromSMSDelivered(txnId, payeeMsisdn);
                    payerActualMessage = MobiquityDBAssertionQueries.getSMSAppliedFromSMSDelivered(txnId, payerMsisdn);
                }

                //For decrypting of Message
                DesEncryptor d = new DesEncryptor();
                if (payerActualMessage != null) {
                    payerActualMessage = d.decrypt(payerActualMessage);
                    checkPayerMessage(extentTest);
                    splitPayerMessage();
                }
                if (payeeActualMessage != null) {
                    payeeActualMessage = d.decrypt(payeeActualMessage);
                    checkPayeeMessage(extentTest);
                    splitPayeeMessage();
                }

                verifyPayerMessage(payerActualMessage, extentTest);
                verifyPayeeMessage(payeeActualMessage, extentTest);

            } else {
                extentTest.info("SMS verification is not done as Transaction Failed");
            }
        } catch (MoneyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * verifyPayerMessage : To verify Payer Message
     *
     * @param payerActualMessage
     * @param reportNode
     */
    private static void verifyPayerMessage(String payerActualMessage, ExtentTest reportNode) {
        if (payerExpectedMessage != null) {
            if (payerActualMessage != null) {
                Markup markup = MarkupHelper.createLabel("Payer Message Verification", ExtentColor.ORANGE);
                reportNode.info(markup);
                String code1 = "Expected SMS::-> " + payerExpectedMessage;
                String code2 = "Actual SMS ::-> " + payerActualMessage;
                reportNode.info(code1);
                reportNode.info(code2);

                if ((payerActualMessage.contains("{")) || payerActualMessage.contains("}")) {
                    reportNode.fail("SMS Assertion failed as Message contains Curly braces.</b>");
                } else if (payerActualMessage.contains(payerExpectedMessage)) {
                    reportNode.pass("SMS Assertion passed as Actual Message contains Expected value.");
                } else {
                    reportNode.fail("SMS Assertion failed as as Actual Message does not contain Expected value.");
                }
            } else {
                reportNode.fail("Payer Message is not present in DB");
            }
        } else {
            reportNode.info("Payer Message is not applicable(N/A).");
        }
    }

    /**
     * verifyPayeeMessage : Verify Payee Message
     *
     * @param payeeActualMessage
     * @param reportNode
     */
    private static void verifyPayeeMessage(String payeeActualMessage, ExtentTest reportNode) {
        if (payeeExpectedMessage != null) {
            if (payeeActualMessage != null) {
                Markup markup = MarkupHelper.createLabel("Payee Message Verification", ExtentColor.ORANGE);
                reportNode.info(markup);
                String code1 = "Expected SMS ::-> " + payeeExpectedMessage;
                String code2 = "Actual SMS ::-> " + payeeActualMessage;
                reportNode.info(code1);
                reportNode.info(code2);

                if ((payeeActualMessage.contains("{")) || payeeActualMessage.contains("}")) {
                    reportNode.fail("SMS Assertion failed as Message contains Curly braces.</b>");
                } else if (payeeActualMessage.contains(payeeExpectedMessage)) {
                    reportNode.pass("SMS Assertion passed as Actual Message contains Expected value.");
                } else {
                    reportNode.fail("SMS Assertion failed as as Actual Message does not contain Expected value.");
                }
            } else {
                reportNode.fail("Payee Message is not present in DB");
            }
        } else {
            reportNode.info("Payee Message is not applicable(N/A).");
        }

    }


    private static void setPayerDetails(Object payer) {
        if (payer instanceof User) {
            payerFirstName = ((User) payer).FirstName;
            payerLastName = ((User) payer).LastName;
            payerMsisdn = ((User) payer).MSISDN;
            payerCategory = ((User) payer).CategoryCode;
            User user = ((User) payer);
            payerNewBalance = MobiquityDBAssertionQueries.
                    getUserBalance(user, Constants.NORMAL_WALLET, DataFactory.getDefaultProvider().ProviderId);
            payerNewBalance = payerNewBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        } else if (payer instanceof OperatorUser) {
            payerFirstName = ((OperatorUser) payer).FirstName;
            payerLastName = ((OperatorUser) payer).LastName;
            payerMsisdn = ((OperatorUser) payer).MSISDN;
            payerCategory = ((OperatorUser) payer).CategoryCode;
        }
    }

    private static void setPayeeDetails(Object payee) {
        if (payee instanceof User) {
            payeeFirstName = ((User) payee).FirstName;
            payeeLastName = ((User) payee).LastName;
            payeeMsisdn = ((User) payee).MSISDN;
            payeeCategory = ((User) payee).CategoryCode;
            User user =((User) payee);
            payeeNewBalance = MobiquityDBAssertionQueries.
                    getUserBalance(user, Constants.NORMAL_WALLET, DataFactory.getDefaultProvider().ProviderId);
            payeeNewBalance = payeeNewBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        } else if (payee instanceof OperatorUser) {
            payeeFirstName = ((OperatorUser) payee).FirstName;
            payeeLastName = ((OperatorUser) payee).LastName;
            payeeMsisdn = ((OperatorUser) payee).MSISDN;
            payeeCategory = ((OperatorUser) payee).CategoryCode;
        }
    }

    /**
     * setPayerPayeeDetails -->
     * Assign Payer and Payee from Current Instance
     *
     * @param payer
     * @param payee
     */
    private static void setPayerPayeeDetails(Object payer, Object payee) {

        if (payer instanceof User) {
            payerFirstName = ((User) payer).FirstName;
            payerLastName = ((User) payer).LastName;
            payerMsisdn = ((User) payer).MSISDN;
            payerCategory = ((User) payer).CategoryCode;
            User user = ((User) payer);
            payerNewBalance = MobiquityDBAssertionQueries.
                    getUserBalance(user, Constants.NORMAL_WALLET, DataFactory.getDefaultProvider().ProviderId);
            payerNewBalance = payerNewBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        } else if (payer instanceof OperatorUser) {
            payerFirstName = ((OperatorUser) payer).FirstName;
            payerLastName = ((OperatorUser) payer).LastName;
            payerMsisdn = ((OperatorUser) payer).MSISDN;
            payerCategory = ((OperatorUser) payer).CategoryCode;
        }

        if (payee instanceof User) {
            payeeFirstName = ((User) payee).FirstName;
            payeeLastName = ((User) payee).LastName;
            payeeMsisdn = ((User) payee).MSISDN;
            payeeCategory = ((User) payee).CategoryCode;
            User user = ((User) payer);
            payeeNewBalance = MobiquityDBAssertionQueries.
                    getUserBalance(user, Constants.NORMAL_WALLET, DataFactory.getDefaultProvider().ProviderId);
            payeeNewBalance = payeeNewBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        } else if (payee instanceof OperatorUser) {
            payeeFirstName = ((OperatorUser) payee).FirstName;
            payeeLastName = ((OperatorUser) payee).LastName;
            payeeMsisdn = ((OperatorUser) payee).MSISDN;
            payeeCategory = ((OperatorUser) payee).CategoryCode;
        }
    }

    private static String splitPayerMessage() {
        if (payerExpectedMessage != null) {
            payerExpectedMessage = payerExpectedMessage.split("\\{")[0];
        }
        return payerExpectedMessage;
    }

    private static String splitPayeeMessage() {
        if (payeeExpectedMessage != null) {
            payeeExpectedMessage = payeeExpectedMessage.split("\\{")[0];
        }
        return payeeExpectedMessage;
    }


    private static String checkPayerMessage(ExtentTest reportNode) {
        if (payerExpectedMessage != null) {

            if (payerExpectedMessage.contains("{payerLastName}") || payerExpectedMessage.contains("{lastNamepayer}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{payerLastName\\}", payerLastName);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{lastNamepayer\\}", payerLastName);
            }
            if (payerExpectedMessage.contains("{payerFirstName}") || payerExpectedMessage.contains("{firstNamepayer}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{payerFirstName\\}", payerFirstName);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{firstNamepayer\\}", payerFirstName);
            }
            if (payerExpectedMessage.contains("{payeeLastName}") || payerExpectedMessage.contains("{lastNamepayee}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{payeeLastName\\}", payeeLastName);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{lastNamepayee\\}", payeeLastName);
            }
            if (payerExpectedMessage.contains("{payeeFirstName}") || payerExpectedMessage.contains("{firstNamepayee}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{payeeFirstName\\}", payeeFirstName);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{firstNamepayee\\}", payeeFirstName);
            }
            if (payerExpectedMessage.contains("{payerMfsProvider}") || payerExpectedMessage.contains("{payeeMfsProvider}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{payerMfsProvider\\}", payerMFSProvider);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{payeeMfsProvider\\}", payeeMFSProvider);
            }
            if (payerExpectedMessage.contains("{newBalance}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{newBalance\\}", payerNewBalance.toString());
            }
            if (payerExpectedMessage.contains("{txnId}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{txnId\\}", transactionID);
            }
            if (payerExpectedMessage.contains("{txnAmount}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{txnAmount\\}", txnAmount.toString());
            }
            if (payerExpectedMessage.contains("{payeeMsisdn}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{payeeMsisdn\\}", payeeMsisdn);
            }
            if (payerExpectedMessage.contains("{currency}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{currency\\}", currency);
            }
            if (payerExpectedMessage.contains("{payerMsisdn}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{payerMsisdn\\}", payerMsisdn);
            }

            if (payerExpectedMessage.contains("{agentCode}")) {
                agentCode = MobiquityGUIQueries.getAgentCode(payeeMsisdn);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{agentCode\\}", agentCode);
            }

            if (payerExpectedMessage.contains("{totalbalance}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{totalbalance\\}", payerNewBalance.toString());
            }

             /*if (payerExpectedMessage.contains("{totalTaxPaid}")) {
                //payerExpectedMessage = payerExpectedMessage.replaceAll("\\{totalTaxPaid\\}", totalTaxPaid);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{totalTaxPaid\\}", "");
            }
            if (payerExpectedMessage.contains("{commissionTax}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{commissionTax\\}", "");
            }
            if (payerExpectedMessage.contains("{serviceChargeTax}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{serviceChargeTax\\}", "");
            }
            if (payerExpectedMessage.contains("{txnAmountTax}")) {
                // payerExpectedMessage = payerExpectedMessage.replaceAll("\\{txnAmountTax\\}", txnAmountTax);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{txnAmountTax\\}", "");

                 /*if (payerExpectedMessage.contains("{totalamount}")) {
                //payerExpectedMessage = payerExpectedMessage.replaceAll("\\{totalamount\\}", totalamount);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{totalamount\\}", "");
            }

            if (payerExpectedMessage.contains("{dateTime}")) {
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{dateTime\\}", "");
            }
            if (payerExpectedMessage.contains("{commission}")) {
                //payerExpectedMessage = payerExpectedMessage.replaceAll("\\{commission\\}", commission);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{commission\\}", "");
            }
            if (payerExpectedMessage.contains("{allCharges}")) {
                //payerExpectedMessage = payerExpectedMessage.replaceAll("\\{allCharges\\}", allCharges);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{allCharges\\}", "");
            }
            if (payerExpectedMessage.contains("{serviceCharges}")) {
//                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{serviceCharges\\}", serviceCharges);
                payerExpectedMessage = payerExpectedMessage.replaceAll("\\{serviceCharges\\}", "");
            }
            */

        } else {
            reportNode.info("Payer Message is not applicable.");
        }
        return payerExpectedMessage;
    }

    private static String checkPayeeMessage(ExtentTest reportNode) {
        if (payeeExpectedMessage != null) {

            if (payeeExpectedMessage.contains("{lastNamepayer}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{lastNamepayer\\}", payerLastName);
            }
            if (payeeExpectedMessage.contains("{firstNamepayer}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{firstNamepayer\\}", payerFirstName);
            }
            if (payeeExpectedMessage.contains("{payeeMfsProvider}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{payeeMfsProvider\\}", payeeMFSProvider);
            }
            if (payeeExpectedMessage.contains("{newBalance}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{newBalance\\}", payeeNewBalance.toString());
            }
            /*if (payeeExpectedMessage.contains("{commission}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{commission\\}", "");
            }
            if (payeeExpectedMessage.contains("{allCharges}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{allCharges\\}", "");
            }*/
            if (payeeExpectedMessage.contains("{txnId}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{txnId\\}", transactionID);
            }
            if (payeeExpectedMessage.contains("{txnAmount}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{txnAmount\\}", txnAmount.toString());
            }
            if (payeeExpectedMessage.contains("{payeeMsisdn}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{payeeMsisdn\\}", payeeMsisdn);
            }
            if (payeeExpectedMessage.contains("{currency}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{currency\\}", currency);
            }
            if (payeeExpectedMessage.contains("{payerMsisdn}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{payerMsisdn\\}", payerMsisdn);
            }
           /* */
            if (payeeExpectedMessage.contains("{totalbalance}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{totalbalance\\}", payeeNewBalance.toString());
            }
            if (payeeExpectedMessage.contains("{agentCode}")) {
                agentCode = MobiquityGUIQueries.getAgentCode(payerMsisdn);
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{agentCode\\}", agentCode);
            }

             /*if (payeeExpectedMessage.contains("{totalTaxPaid}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{totalTaxPaid\\}", "");
            }
            if (payeeExpectedMessage.contains("{commissionTax}")) {
                // payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{commissionTax\\}", commissionTax);
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{commissionTax\\}", "");
            }
            if (payeeExpectedMessage.contains("{serviceChargeTax}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{serviceChargeTax\\}", "");
            }
            if (payeeExpectedMessage.contains("{txnAmountTax}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{txnAmountTax\\}", "");
            }
            if (payeeExpectedMessage.contains("{dateTime}")) {
                payeeExpectedMessage = payeeExpectedMessage.replaceAll("\\{dateTime\\}", "");
            }



            */
        } else {
            reportNode.info("Payee Message is not applicable(NA).");
        }

        return payeeExpectedMessage;
    }


    public static Map<String, String> getMessageCodeUsingServiceCode(String serviceType) {
        Map<String, String> map = new HashMap<>();
        if (serviceType.equalsIgnoreCase(Services.C2C)) {
            map.put("PAYEE_SUCCESS_MSG_CODE", MessageCodes.C2C_PAYEE_SUCCESS_MSG_CODE);
            map.put("PAYER_SUCCESS_MSG_CODE", MessageCodes.C2C_PAYER_SUCCESS_MSG_CODE);
        } else if (serviceType.equalsIgnoreCase(Services.CASHIN)) {
            map.put("PAYEE_SUCCESS_MSG_CODE", MessageCodes.CASHIN_PAYEE_SUCCESS_MSG_CODE);
            map.put("PAYER_SUCCESS_MSG_CODE", MessageCodes.CASHIN_PAYER_SUCCESS_MSG_CODE);
        } else if (serviceType.equalsIgnoreCase(Services.O2C)) {
            map.put("PAYEE_SUCCESS_MSG_CODE", MessageCodes.O2C_PAYEE_SUCCESS_MSG_CODE);
            map.put("PAYER_SUCCESS_MSG_CODE", MessageCodes.O2C_PAYER_SUCCESS_MSG_CODE);
        } else if (serviceType.equalsIgnoreCase(Services.CASHOUT)) {
            map.put("PAYEE_SUCCESS_MSG_CODE", MessageCodes.CASHOUT_PAYEE_SUCCESS_MSG_CODE);
            map.put("PAYER_SUCCESS_MSG_CODE", MessageCodes.CASHOUT_PAYER_SUCCESS_MSG_CODE);
        } else if (serviceType.equalsIgnoreCase(Services.SVA_TO_BANK)) {
            map.put("PAYEE_SUCCESS_MSG_CODE", MessageCodes.SVA_TO_OWN_BANK_SUCCESS_MSG_CODE);
            map.put("PAYER_SUCCESS_MSG_CODE", null);
        } else if (serviceType.equalsIgnoreCase(Services.COMMISSION_DISBURSEMENT)) {
            map.put("PAYEE_SUCCESS_MSG_CODE", MessageCodes.COMMISSION_DIS_PAYEE_SUCCESS_MSG_CODE);
            map.put("PAYER_SUCCESS_MSG_CODE", MessageCodes.COMMISSION_DIS_PAYER_SUCCESS_MSG_CODE);
        } else {
            //TODO - Fill this condition with appropriate condition
        }

        return map;
    }

    public static void verifyPayerDBMessage(Object payer, Services serviceType, String txnID, ExtentTest test) throws IOException, SQLException, ClassNotFoundException {


    }

    private void assignValues() {

    }

}
