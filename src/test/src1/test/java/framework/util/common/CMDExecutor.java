package framework.util.common;


import com.aventstack.extentreports.ExtentTest;
import com.jcraft.jsch.*;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.NumberConstants;
import framework.util.propertiesManagement.MfsTestProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;


/**
 * @author navin.pramanik
 * @author prashant.kumar
 * This class is created to make connection with the Application Server
 */
public class CMDExecutor {

    private final static String HOSTNAME = MfsTestProperties.getInstance().getProperty("app.server.ip");
    private final static String USERNAME = MfsTestProperties.getInstance().getProperty("app.server.username");
    private final static String PASSWORD = MfsTestProperties.getInstance().getProperty("app.server.password");
    private final static String CATALINAPATH = MfsTestProperties.getInstance().getProperty("app.server.catalinapath");
    private final static String WEBBUSINESSFLOWPATH = MfsTestProperties.getInstance().getProperty("app.server.businessflow.path");
    private final static String TXNCHANNELGATEWAYAPATH = MfsTestProperties.getInstance().getProperty("txn.server.log.path");
    private final static String CATALINALOGLIMIT = MfsTestProperties.getInstance().getProperty("app.server.log.limit");
    private final static String MESSAGE_SENT_LOG_PATH = MfsTestProperties.getInstance().getProperty("app.server.messageSent.path");

    private final static String CATALINALOGSOUTPATH = FilePath.dirCatalinaLogs;
    private final static String CatalinaGrepCommandString = "tail -n  " + CATALINALOGLIMIT + " ";
    private static final int BYTE_CONSTANT = 1024;
    private static final int PORT = 22;
    private static final Logger logger = LoggerFactory.getLogger(CMDExecutor.class);
    private static String CatalinaFileName = "";
    private static String ServerRestartFileName = "";

    public static void main(String args[]) {
        //  System.out.println(getWebBusinessFlowLog());

        String afterTime = "2018-06-21 00:05:05";
        String prevTime = "2018-06-21 00:04:51";
        String msisdn = "7794458844";


        System.out.println(getLinuxServerDateTime());
        //String msg = getMessageFromMessageSentLog(prevTime,afterTime,msisdn);
        String msg = "Message:Your account creation is successful with mPin XXXX for Provider(s) MFS1 . Please modify your MPIN using #144# option. Thank you.";
        System.out.println(msg);

        String msgSplit = msg.split("mPin ")[1].split("\\s")[0];
        System.out.println(msgSplit);

        if (msgSplit.toUpperCase().contains("XXXX")) {
            System.out.println("Test Passed : PIN is Encrypted in Logs");
        }

    }

    /**
     * getWebBusinessFlowLog --> To return the WebBusinessFlowLog
     *
     * @return WebBusinessFlowLog File Name
     */
    public static String getWebBusinessFlowLog() {
        return getServerLog(WEBBUSINESSFLOWPATH);
    }

    /**
     * To extract ChannelGatewayLogs
     *
     * @return LogFileName
     */
    public static String getTxnChannelGatewayLog() {
        return getServerLog(TXNCHANNELGATEWAYAPATH);
    }

    /**
     * To extract CatalinaLogs
     *
     * @return LogFileName
     */
    public static String getCatalinaLog() {
        return getServerLog(CATALINAPATH);
    }

    /**
     * Method - createLogsDirectory
     * This will create Logs Directory if not already exist
     */
    private static void createLogsDirectory() {
        File directory = new File(FilePath.dirCatalinaLogs);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    /**
     * To print server logs in a log file
     *
     * @param logFileName
     * @return LogFileName
     */
    public static String getServerLog(String logFileName) {
        String catalinaFileName = null;
        BufferedWriter out = null;
        try {
            createLogsDirectory();

            String fileName = new File(logFileName).getName().split("\\.")[0];

            catalinaFileName = CATALINALOGSOUTPATH + fileName + DataFactory.getTimeStamp() + ".log";

            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session = jsch.getSession(USERNAME, HOSTNAME, PORT);
            session.setPassword(PASSWORD);
            session.setConfig(config);
            session.connect();

            Channel channel = session.openChannel("exec");
            InputStream in = channel.getInputStream();
            OutputStream outputStream = channel.getOutputStream();

            ((ChannelExec) channel).setCommand(CatalinaGrepCommandString + logFileName);

            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(outputStream);


            channel.connect();
            byte[] tmp = new byte[BYTE_CONSTANT];

            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, BYTE_CONSTANT);
                    if (i < 0) break;
                    String content;
                    try {
                        out = new BufferedWriter(new FileWriter(catalinaFileName, true));
                        content = (new String(tmp, 0, i));
                        out.write(content);
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (channel.isClosed()) {
                    break;
                }
                Utils.putThreadSleep(NumberConstants.SLEEP_1000);
            }
            channel.disconnect();
            session.disconnect();
            out.close();

            logger.debug("Session Disconnected successfully");
        } catch (JSchException jsEx) {
            logger.error("Java Secure Channel Error Occured ", jsEx);
        } catch (IOException ioEx) {
            logger.error("IO Exception Occurred", ioEx);

        }
        return catalinaFileName;
    }


    /**
     * Method - getServerDate
     *
     * @return LinuxServerDate
     */
    public static String getServerDate() {

        String dateFromLinux = null;
        try {
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session = jsch.getSession(USERNAME, HOSTNAME, PORT);
            session.setPassword(PASSWORD);
            session.setConfig(config);
            session.connect();

            Channel channel = session.openChannel("exec");
            OutputStream outputStream = channel.getOutputStream();
            ((ChannelExec) channel).setCommand("date  +%d/%m/%y");
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(outputStream);

            InputStream in = channel.getInputStream();

            channel.connect();
            byte[] tmp = new byte[BYTE_CONSTANT];

            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, BYTE_CONSTANT);
                    dateFromLinux = new String(tmp, 0, i);
                }
                if (dateFromLinux != null) {
                    break;
                }

            }


            channel.disconnect();
            session.disconnect();

            //Log.debug("Session Disconnected successfully");
        } catch (JSchException jsEx) {
            logger.error("Java Secure Channel Error Occured ", jsEx);
        } catch (IOException ioEx) {
            logger.error("IO Exception Occured", ioEx);
        }
        return dateFromLinux.trim();
    }

    /*
    * Function to restart the server
    */
    public static String performServerRestart(ExtentTest pNode) {
        try {
            File directory = new File(FilePath.dirCatalinaLogs);
            if (!directory.exists()) {
                directory.mkdir();
            }

            ServerRestartFileName = "SERVER_RESTART_" + DataFactory.getTimeStamp().toString() + ".log";
            pNode.info("Server Restart FileName: " + ServerRestartFileName);
            File file = new File(directory, ServerRestartFileName);
            executeShellCommandAndSaveLogs("sudo service txnserver stop&&sudo service txnserver status&&exit", file);
            Thread.sleep(10000);
            executeShellCommandAndSaveLogs("sudo service txnserver start&&sudo service txnserver status&&exit", file);
            Thread.sleep(30000);
            pNode.info("Executed Commands for server restart, check log file for more details");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServerRestartFileName;
    }

    public static void executeCmd(String cmdString) {
        String cmdOutput = null;
        try {
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session = jsch.getSession(USERNAME, HOSTNAME, 22);
            session.setPassword(PASSWORD);
            session.setConfig(config);
            session.connect(1000);

            Channel channel = session.openChannel("exec");
            System.out.println("Command is " + cmdString);
            ((ChannelExec) channel).setCommand(cmdString);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();
            channel.connect();
            channel.disconnect();
            session.disconnect();

            //Log.debug("Session Disconnected successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void executeShellCommandAndSaveLogs(String cmdString, File file) {
        Channel channel = null;
        Session session = null;
        try {
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            session = jsch.getSession(USERNAME, HOSTNAME, 22);
            session.setPassword(PASSWORD);
            session.setConfig(config);
            session.connect();

            channel = session.openChannel("shell");
            OutputStream ops = channel.getOutputStream();
            PrintStream ps = new PrintStream(ops, true);

            channel.connect();
            ps.println(cmdString);
            InputStream in = channel.getInputStream();
            byte[] tmp = new byte[1024];
            BufferedWriter out = null;
            Thread.sleep(5000);
            /*while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    String content;
                    try {
                        out = new BufferedWriter(new FileWriter(file, true));
                        content = (new String(tmp, 0, i));
                        out.write(content);
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (channel != null)
                channel.disconnect();
            if (session != null)
                session.disconnect();
        }
    }


    public static String getLinuxServerDateTime() {

        String dateFromLinux = null;
        try {
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session = jsch.getSession(USERNAME, HOSTNAME, PORT);
            session.setPassword(PASSWORD);
            session.setConfig(config);
            session.connect();

            Channel channel = session.openChannel("exec");
            OutputStream outputStream = channel.getOutputStream();
            ((ChannelExec) channel).setCommand("date  +%Y-%m-%d' '%H:%M:%S");
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(outputStream);

            InputStream in = channel.getInputStream();

            channel.connect();
            byte[] tmp = new byte[BYTE_CONSTANT];

            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, BYTE_CONSTANT);
                    dateFromLinux = new String(tmp, 0, i);
                }
                if (dateFromLinux != null) {
                    break;
                }

            }


            channel.disconnect();
            session.disconnect();

            //Log.debug("Session Disconnected successfully");
        } catch (JSchException jsEx) {
            logger.error("Java Secure Channel Error Occured ", jsEx);
        } catch (IOException ioEx) {
            logger.error("IO Exception Occured", ioEx);
        }
        return dateFromLinux.trim();
    }


    public static String getMessageFromMessageSentLog(String beforeDateTime, String afterDateTime, String msisdn, boolean... isPasswordRequired) {

        boolean isPassword = isPasswordRequired.length > 0 ? isPasswordRequired[0] : false;

        String message = null, commandToExecute = null;
        try {
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session = jsch.getSession(USERNAME, HOSTNAME, PORT);
            session.setPassword(PASSWORD);
            session.setConfig(config);
            session.connect();

            Channel channel = session.openChannel("exec");
            OutputStream outputStream = channel.getOutputStream();
            if (isPassword) {
                commandToExecute = "awk '$0 ~ 'Message' && $0 > " + "\"" + beforeDateTime + "\" && $0 < " + "\"" + afterDateTime + "\"' " + MESSAGE_SENT_LOG_PATH + "| grep " + msisdn + " | tr -s '[' '|' |tr -s ']' '|' | cut -d '|' -f 10 | tr -s '\\n' '|'|cut -d '|' -f 2";
            } else {
                commandToExecute = "awk '$0 ~ 'Message' && $0 > " + "\"" + beforeDateTime + "\" && $0 < " + "\"" + afterDateTime + "\"' " + MESSAGE_SENT_LOG_PATH + "| grep " + msisdn + " | tr -s '[' '|' |tr -s ']' '|' | cut -d '|' -f 10 | tr -s '\\n' '|'|cut -d '|' -f 1";
            }
            logger.info("\n *********  Executing Linux Command : ************ \n" + commandToExecute);
            ((ChannelExec) channel).setCommand(commandToExecute);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(outputStream);

            InputStream in = channel.getInputStream();

            channel.connect();
            byte[] tmp = new byte[BYTE_CONSTANT];

            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, BYTE_CONSTANT);
                    message = new String(tmp, 0, i);
                }
                if (message != null) {
                    break;
                }

            }

            channel.disconnect();
            session.disconnect();

            //Log.debug("Session Disconnected successfully");
        } catch (JSchException jsEx) {
            logger.error("Java Secure Channel Error Occured ", jsEx);
        } catch (IOException ioEx) {
            logger.error("IO Exception Occured", ioEx);
        }
        return message.trim();
    }

}