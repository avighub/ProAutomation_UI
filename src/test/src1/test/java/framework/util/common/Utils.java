package framework.util.common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import com.google.gson.Gson;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import framework.util.JsonPathOperation;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.*;

import static com.jayway.jsonpath.internal.JsonFormatter.prettyPrint;

public class Utils {
    private static WebDriver driver;
    private static FunctionLibrary fl;

    public static Map jsonToMap(String json) {
        return new Gson().fromJson(json, Map.class);
    }

    public static <T> T clone(T t) {
        Gson gson = new Gson();
        String json = gson.toJson(t);
        return (T) gson.fromJson(json, t.getClass());
    }

    public static <T> List<T> list(T... values) {
        return Arrays.asList(values);
    }

    public static <T> List<T> list(int numberOfElements, T value) {
        ArrayList<T> list = new ArrayList<>();
        for (int i = 0; i < numberOfElements; i++) {
            list.add(clone(value));
        }
        return list;
    }

    public static String json(String json, JsonPathOperation... operations) {
        DocumentContext context = JsonPath.parse(json);
        for (JsonPathOperation operation : operations) {
            context = operation.perform(context);
        }
        return prettyPrint(context.jsonString());
    }

    public static void setValueUsingJs(WebElement elem, String value) {
        driver = DriverFactory.getDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].value='" + value + "';", elem);
    }

    public static void closeUntitledWindows() throws Exception {
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        String MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();
        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                driver.switchTo().window(ChildWindow);
                driver.close();
            }
        }
        // Switching to Parent window i.e Main Window.
        driver.switchTo().window(MainWindow);
        fl.contentFrame();
    }

    /**
     * Close all windows except the One for which the handle is passed
     *
     * @param winHandle
     * @throws Exception
     */
    public static void closeWindowsExceptOne(String winHandle) throws Exception {
        try {
            if (winHandle != null) {
                driver = DriverFactory.getDriver();
                fl = new FunctionLibrary(driver);
                Set<String> s1 = driver.getWindowHandles();
                Iterator<String> i1 = s1.iterator();

                while (i1.hasNext()) {
                    String ChildWindow = i1.next();

                    if (!winHandle.equalsIgnoreCase(ChildWindow)) {
                        driver.switchTo().window(ChildWindow);
                        driver.close();
                    }
                }
                // Switching to Parent window i.e Main Window.
                driver.switchTo().window(winHandle);
                fl.contentFrame();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void closeWindows() throws Exception {
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String ChildWindow = i1.next();
            driver.switchTo().window(ChildWindow);
            driver.close();
        }
    }


    public static void highLightElement(WebElement webElement) {
        JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getDriver();
        js.executeScript("arguments[0].setAttribute('style','background-color:yellow; border: 2px solid red;');", webElement);
    }

    /**
     * scroll To Bottom Of Page
     */
    public static void scrollToBottomOfPage() {
        driver = DriverFactory.getDriver();
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        try {
            Thread.sleep(Constants.THREAD_SLEEP_1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void scrollToMiddleOfPage() {
        driver = DriverFactory.getDriver();
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 500)");
    }

    public static void scrollToTopOfPage() {
        driver = DriverFactory.getDriver();
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0)");
    }

    public static void scrollUntilElementIsVisible(WebElement element) {
        driver = DriverFactory.getDriver();
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Scroll to a particular element
     *
     * @param scrollArea
     */
    public static void scrollToAnElement(WebElement scrollArea) {
        driver = DriverFactory.getDriver();
        Actions actions = new Actions(driver);
        actions.moveToElement(scrollArea);
        actions.perform();
    }


    public static void checkElementIsEditable(String elemValue, ExtentTest t1) {
        try {
            WebElement check = DriverFactory.getDriver().findElement(By.xpath(elemValue));
            t1.info("Element found");
            if (!check.getTagName().contains("label")) {
                if (check.getAttribute("readonly").contains("true")) {
                    t1.pass("Element have readonly Attribute ");
                } else {
                    t1.fail("Element dose not have readonly Attribute ");
                }
                captureScreen(t1);
            } else {
                t1.info("Element is Label ");
            }

        } catch (Exception e) {
            t1.fail("Element is Not Verified ");
        }
    }


    /**
     * put Thread Sleep
     *
     * @param miliSecond
     */
    public static void putThreadSleep(int miliSecond) {
        try {
            Thread.sleep(miliSecond);
        } catch (InterruptedException e) {
            System.err.println("Interrupted Exception occurred :" + e);
        } catch (Exception e) {
            System.err.println("Exception occurred :" + e);
        }
    }

    /**
     * Capture screenshot and attach to report log
     *
     * @param t1
     * @throws IOException
     */
    public static void captureScreen(ExtentTest t1) throws IOException {
        t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
    }

    /**
     * @param elemValue
     * @param elemType
     * @param t1
     * @return
     */
    public static boolean checkElementPresent(String elemValue, String elemType, ExtentTest... t1) {
        putThreadSleep(Constants.THREAD_SLEEP_1000);
        ExtentTest pnode = (t1.length) > 0 ? t1[0] : null;
        driver = DriverFactory.getDriver();
        boolean isFound = false;
        switch (elemType) {
            case Constants.FIND_ELEMENT_BY_CLASS:
                isFound = (driver.findElements(By.className(elemValue)).size() > 0) ? true : false;
                break;
            case Constants.FIND_ELEMENT_BY_ID:
                isFound = (driver.findElements(By.id(elemValue)).size() > 0) ? true : false;
                break;
            case Constants.FIND_ELEMENT_BY_XPATH:
                isFound = (driver.findElements(By.xpath(elemValue)).size() > 0) ? true : false;
                break;
            case Constants.FIND_ELEMENT_BY_LINK:
                isFound = (driver.findElements(By.linkText(elemValue)).size() > 0) ? true : false;
                break;
            case Constants.FIND_ELEMENT_BY_NAME:
                isFound = (driver.findElements(By.name(elemValue)).size() > 0) ? true : false;
                break;
            case Constants.FIND_ELEMENT_BY_CSS:
                isFound = (driver.findElements(By.cssSelector(elemValue)).size() > 0) ? true : false;
                break;
            default:
                System.out.println("Provide correct element accessor type");

        }
        return isFound;
    }

    public static String removeExtraSpace(String text) {
        return new String(text).trim().replaceAll("\\s{2,}", " ");
    }


    public static void runBatFileThroughCMD(String filePath) {
        try {
            String[] command = {"cmd.exe", "/C", "Start", filePath};

            Runtime.getRuntime().exec(command);

        } catch (FileNotFoundException ex) {
            System.out.println("File Not found" + filePath);
        } catch (IOException ioe) {
            System.out.println("IO Exception occurred : " + ioe.getMessage());
        }
    }


    /**
     * Complete the save download in Ie using Sikuli class
     */
    public static void ieSaveDownloadUsingSikuli() {
        if (ConfigInput.isInternetExplorer) {
            try {
                Pattern saveButton = new Pattern(FilePath.sikuliIeSaveBtn);
                Screen screen = new Screen();
                screen.wait(saveButton, Constants.TWO_SECONDS);
                screen.doubleClick(saveButton);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getLatestFilefromDir(String dirPath) throws Exception {
        Thread.sleep(5000);
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile.getName().toString();
    }

    public static Boolean isFileDownloaded(String oldFile, String newFile) {
        try {
            if (oldFile == null) {
                if (newFile == null) {
                    return false;
                } else {
                    return true;
                }
            } else if (oldFile.equalsIgnoreCase(newFile)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Used for normal File upload
     *
     * @param filePath
     */
    public static void uploadFile_01(String filePath) {
        try {
            String editBox = FilePath.sikuliEditBox_01;
            String openButton = FilePath.sikuliOpen_01;

            Pattern pat1 = new Pattern(editBox);
            Pattern pat2 = new Pattern(openButton);

            Screen screen = new Screen();
            screen.wait(pat1, 2000);
            screen.type(pat1, filePath);
            screen.wait(pat2, 2000);
            screen.click(pat2);
        } catch (FindFailed e) {
            e.printStackTrace();
        }
    }

    /**
     * Used for new Bulk Payout Tool Page
     *
     * @param filePath
     */
    public static void uploadFile_02(String filePath) {
        try {
            String editBox = FilePath.sikuliEditBox_02;
            String openButtonIe = FilePath.sikuliOpen_01;
            String openButtonChrome = FilePath.sikuliOpen_02;

            Pattern pat1 = new Pattern(editBox);
            Pattern pat2 = new Pattern(openButtonIe);
            Pattern pat3 = new Pattern(openButtonChrome);

            Screen screen = new Screen();
            screen.wait(pat1, 2000);
            screen.type(pat1, filePath);
            if (ConfigInput.isInternetExplorer) {
                screen.wait(pat2, Constants.TWO_SECONDS);
                screen.click(pat2);
            } else {
                screen.wait(pat3, Constants.TWO_SECONDS);
                screen.click(pat3);
            }
        } catch (FindFailed e) {
            e.printStackTrace();
        }
    }

    /**
     * Check Log file for specific messages
     *
     * @param filePath
     * @param message
     * @param node
     * @return
     * @throws Exception
     */
    public static void checkLogFileSpecificMessage(String filePath, String message, ExtentTest node) throws Exception {

        try {
            File file = new File(filePath);
            Scanner s = new Scanner(file);

            boolean success = false;
            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (line.contains(message)) {
                    node.pass("found message text: " + message);
                    success = true;
                    break;
                }
            }

            if (!success) {
                Assertion.markAsFailure("Couldn't find message text: " + message);
                node.fail("Couldn't find message text: " + message);
            }
            s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ieSaveDownloadUsingRobot() {
        if (ConfigInput.isInternetExplorer) {
            try {
                Utils.closeUntitledWindows();

                Thread.sleep(1000);
                Robot r = new Robot();
                r.keyPress(KeyEvent.VK_ALT);
                Thread.sleep(1000);
                r.keyPress(KeyEvent.VK_S);
                r.keyRelease(KeyEvent.VK_ALT);
                r.keyRelease(KeyEvent.VK_S);
                Thread.sleep(3000);
            } catch (AWTException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void verifyMessageFromBulkProcessErrorFile(String errorFileName, String expectedMessage, ExtentTest node, String... variation) throws Exception {
        String code = null;
        Markup m = null;
        boolean flag = true;
        String expectedMsg;
        Thread.sleep(5000);
        File errorFile = new File(FilePath.dirFileDownloads + "/" + errorFileName);
        Scanner s = new Scanner(errorFile);
        if (variation.length > 0) {
            expectedMsg = MessageReader.getDynamicMessage(expectedMessage, variation);
        } else {
            expectedMsg = MessageReader.getMessage(expectedMessage, null);
        }

        while (s.hasNextLine()) {
            String actual = s.nextLine();
            if (actual.contains(expectedMsg)) {
                code = "Verified Successfully - " + expectedMessage + ":\nExpected: " + expectedMsg + "\nActual  : " + actual;
                m = MarkupHelper.createCodeBlock(code);
                node.pass(m);
                flag = false;
                break;
            }
        }
        if (flag) {
            code = "verification failure";
            m = MarkupHelper.createCodeBlock(code);
            node.fail(m);
            node.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            Assert.fail();
        }

        s.close();
    }


    public static String decryptMessage(String msgToDecrypt) {
        String msg = null;
        DesEncryptor d = new DesEncryptor();
        try {
            msg = d.decrypt(msgToDecrypt);
        } catch (MoneyException e) {
            e.printStackTrace();
        }
        return msg;
    }

    public static void createDirectoryIfNotPresent(String dirPath) {
        File dirToCheck = new File(dirPath);
        if (!dirToCheck.exists())
            dirToCheck.mkdir();
    }


    public static void createLabelForMethod(String methodName, ExtentTest pNode) {
        Markup m = MarkupHelper.createLabel(methodName, ExtentColor.BLUE);
        pNode.info(m);
    }

    public static void attachFileAsExtentLog(String filename, ExtentTest node) {
        node.log(Status.INFO, "<b> <h6><font color='red'>Click below Link to view File :</b><a href='" + filename + "'><b><h6><font color='blue'> " + filename + "</font></h6></b></a>");
    }


    public static String extractTxnIdFromMessage(String msg, String splitter) {
        String[] compltMsg = msg.split(splitter);
        return compltMsg[1].trim();
    }


}

