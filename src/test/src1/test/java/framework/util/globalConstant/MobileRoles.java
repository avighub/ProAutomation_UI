package framework.util.globalConstant;

public class MobileRoles {
    public static final String WALLET_P2P_SEND_MONEY = "40";
    public static final String WALLET_CASHIN_RECEIVER = "50000";
    public static final String WALLET_CASHIN_OTHER = "50003";
    public static final String WALLET_RECHARGE_OTHER = "143";
    public static final String WALLET_PAY_BILL = "58";
    public static final String WALLET_BALANCE_ENQUIRY = "97";
    public static final String WALLET_BULK_PAYMENT= "589";
    public static final String WALLET_INVERSE_C2C= "999";



    public static final String BANK_RECHARGE_OTHER = "1435";
    public static final String BANK_UTILITY_BILL_PAY = "59";



}
