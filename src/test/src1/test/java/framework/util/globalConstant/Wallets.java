package framework.util.globalConstant;

public class Wallets {
    // this should be in sync with the ConfigInput.xlxs > Wallets Sheet
    public static final String NORMAL = "Normal";
    public static final String COMMISSION = "COMMISSION";
    public static final String SAVINGCLUB = "SAVINGCLUB";
    public static final String REMMITANCE = "REMMITANCE";
    public static final String SALARY = "SALARY";
}
