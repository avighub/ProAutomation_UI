package framework.util.jigsaw;


import framework.util.JsonPathOperation;
import org.json.JSONObject;

import java.util.Map;

import static framework.util.MultiLineString.multiLineString;
import static framework.util.common.Utils.json;
import static framework.util.common.Utils.jsonToMap;
import static framework.util.propertiesManagement.MfsTestUtils.getTenantId;

public class ServiceRequestContracts {

    public static String serviceRequestJson(boolean withTenantId, JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
                {
                    "serviceFlowId":"",
                    "transactionAmount": "2",
                    "initiator": "transactor",
                    "currency": "101",
                    "bearerCode": "USSD",
                    "language":"en",
                    "externalReferenceId": "",
                    "remarks": "",
                    "transactionMode": "",
                    "requestedServiceCode":"",
                    "productOwnerCode":"123456",
                    "productBrand":"almadar",
                    "productCategory":"voice",
                    "transactor": {
                        "idType": "mobileNumber",
                        "productId": "12",
                        "tpin": "1357",
                        "idValue": "7701536637",
                        "mpin": "1357",
                        "password": "1357",
                        "pin": "1357",
                        "txnPassword": null,
                        "employeeId": null,
                        "bankId": null,
                        "bankAccountNumber": null

                        },
                  "depositor": {
                    "idType": "mobileNumber",
                    "idValue": "7700098989",
                    "identificationNo":"77000989891234",
                    "mpin": "1357",
                    "password": "1357",
                    "pin":"1357",
                    "tpin": "1357"
                    },
                    "receiver": {
                        "idType": "mobileNumber",
                        "productId": "12",
                        "idValue": "7700099999",
                        "identificationNo": "77000999991234",
                        "mpin": "1357",
                        "tpin": "1357",
                        "password": null,
                        "pin": "1357",
                        "txnPassword": null,
                        "bankId": null,
                        "bankAccountNumber": null,
                        "partyId": ""

                    }
                   }
            */), operations);
        return withTenantId ? getJsonWithTenantId(json) : json;
    }

    public static String serviceRequestJson(JsonPathOperation... operations) {
        return serviceRequestJson(true, operations);
    }

    public static String getJsonWithTenantId(String json) {
        JSONObject jsonObject = new JSONObject(json);
        jsonObject.put("mfsTenantId", getTenantId());
        return jsonObject.toString();
    }

    public static Map serviceRequestDetails(JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
                    {
                        "idType": "mobileNumber",
                        "productId": "12",
                        "idValue": "7700099999",
                        "identificationNo": "",
                        "mpin": "1357",
                        "tpin": "1358",
                        "password": null,
                        "pin": "1357",
                        "txnPassword": null,
                        "encryptedPassword": null,
                        "passcode": null,
                        "EmployeeId": null,
                        "billAccountNumber": null,
                        "billNumber":"",
                        "bankId":""
                    }
                    */), operations);
        return jsonToMap((json));
    }

    public static String serviceRequestResumeJson(boolean withTenantId, JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
                {
                    "resumeServiceRequestId": "9c685585-491b-42a7-8e9d-d76ade6bb631",
                    "approvalRequestId": null,
                    "bearerCode": "USSD",
                    "currency" : "101",
                    "language": "en",
                    "firstTransactionId": null,
                    "party": {
                        "idType": "mobileNumber",
                        "idValue": "7701536637",
                        "mpin": "1357",
                        "tpin": "1357",
                        "pin" : "1357",
                        "password":"1357"
                    }
                }
             */), operations);
        return withTenantId ? getJsonWithTenantId(json) : json;
    }

    public static String serviceRequestResumeJson(JsonPathOperation... operations) {
        return serviceRequestResumeJson(true, operations);
    }

    public static String serviceRequestCancelJson(boolean withTenantId, JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
                {
                    "cancelServiceRequestId": "",
                    "bearerCode": "USSD",
                    "currency" : "101",
                    "approvalRequestId": null,
                    "firstTransactionId": null,
                    "language": "en",
                    "party": {
                        "idType": "mobileNumber",
                        "idValue": "7701536637",
                        "productId": "12",
                        "mpin": "1357",
                        "tpin": "1357",
                        "password": "1357",
                        "pin": "1357",
                    }
                }
             */), operations);
        return withTenantId ? getJsonWithTenantId(json) : json;
    }

    public static String serviceRequestCancelJson(JsonPathOperation... operations) {
        return serviceRequestCancelJson(true, operations);
    }


    public static String serviceRequestOTPResumeJson(JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
        {
            "resumeServiceRequestId": "7c0fd7d6-3018-4a62-967f-6dc50c8c097b",
            "otp": "828045"
        }
        */), operations);
        return getJsonWithTenantId(json);
    }

    public static String serviceChargePolicyJson(JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
                    {
                      "serviceCode": "CASHIN",
                      "currency": "RUB",
                      "isFinancial": true,
                      "isInclusive": false,
                      "isTaxInclusive": false,
                      "chargePricingRange": {
                        "minimumAmount": null,
                        "maximumAmount": null
                      },
                      "taxPricingRange": {
                        "minimumAmount": 0,
                        "maximumAmount": 9999999
                      },
                      "chargeRules": [
                        {
                          "name": "Default Charge",
                          "startDate": "2015-10-01",
                          "endDate": null,
                          "condition": {
                            "lhsExpression": "bearerCode",
                            "operator": "equals",
                            "rhsValue": "USSD"
                           },
                          "status": "ACTIVE",
                          "chargePricingRange": {
                            "minimumAmount": null,
                            "maximumAmount": null
                          },
                          "chargeStatements": [
                            {
                              "chargePayer": "sender",
                              "chargePayerProductId": "12",
                              "chargeReceiver": "serviceProvider",
                              "pricingFactor": "both",
                              "chargePayerParty":null,
                              "pricingMethod": {
                                "type": "flatPricing",
                                "percentage": "1",
                                "fixedAmount": "10"
                              }
                            }
                          ]
                        }
                      ],
                      "taxRules": [
                        {
                          "name": "Default Tax",
                          "startDate": "2015-02-12",
                          "endDate": "2019-06-29",
                          "condition":null,
                          "taxStatements": [
                            {
                              "pricingMethod": {
                                "percentage": 10,
                                "fixedAmount": 10,
                                "type": "flatPricing"
                              },
                              "pricingFactor": "both"
                            }
                          ]
                        }
                      ]
                    }
                    */), operations);
        return getJsonWithTenantId(json);
    }


}
