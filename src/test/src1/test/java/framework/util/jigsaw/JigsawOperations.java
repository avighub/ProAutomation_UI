package framework.util.jigsaw;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.ValidatableResponse;
import framework.util.JsonPathOperation;
import framework.util.propertiesManagement.MfsTestProperties;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Map;

import static framework.util.propertiesManagement.MessageReader.getMessage;
import static framework.util.propertiesManagement.MfsTestUtils.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.IsEqual.equalTo;

public class JigsawOperations {

    public static final String MONEY_RESPONSE_POLL_INTERVAL_PROPERTY = "serviceRequest.response.pollIntervalInMillis";
    public static final String MONEY_RESPONSE_POLL_COUNT_PROPERTY = "serviceRequest.response.pollCount";
    public static final String MONEY_CURRENCY_CODE_PROPERTY = "mfs1.currency.code";
    public static final String TENANNT_ID_SEPARATOR = "__";
    private static final Logger LOGGER = LoggerFactory.getLogger(JigsawOperations.class);
    private static final MfsTestProperties mfsTestProperties = MfsTestProperties.getInstance();
    private static int pollInterval = mfsTestProperties.getIntProperty(MONEY_RESPONSE_POLL_INTERVAL_PROPERTY, 500);
    private static int pollCount = mfsTestProperties.getIntProperty(MONEY_RESPONSE_POLL_COUNT_PROPERTY, 5);
    private static String currencyCode = mfsTestProperties.getProperty(MONEY_CURRENCY_CODE_PROPERTY);

    private static boolean useServiceCodeInBody = true;
    private static boolean headerBasedTenant = false;

    public static void setUseServiceInBody(boolean status) {
        useServiceCodeInBody = status;
    }


    public static ValidatableResponse performServiceRequestAndWaitForPauseForSyncAPI(String serviceCode, JsonPathOperation... operations) {
        return performServiceRequestAndWaitForCompletionStatusForSyncAPI(serviceCode, operations, Matchers.equalTo("PAUSED"));
    }

    public static ValidatableResponse performServiceRequestAndWaitForSuccessForSyncAPI(String serviceCode, JsonPathOperation... operations) {
        return performServiceRequestAndWaitForCompletionStatusForSyncAPI(serviceCode, operations, Matchers.equalTo("SUCCEEDED"));
        //.body("errors", nullValue())
        //.body("serviceRequestId", notNullValue())
        //.body("message", notNullValue())
        //.body("transactionId", notNullValue())
        //.body("transactionAmount", equalTo(null))
        //.body("transferAmount", equalTo(null))
        //.body("serviceFlow", Matchers.equalTo(serviceCode));
    }

    public static ValidatableResponse performServiceRequestAndWaitForFailedForSyncAPI(String serviceCode, JsonPathOperation... operations) {
        return performServiceRequestAndWaitForCompletionStatusForSyncAPI(serviceCode, operations, Matchers.equalTo("FAILED"));
        //.body("errors", notNullValue())
        //.body("serviceRequestId", notNullValue())
        //.body("transactionAmount", equalTo(null))
        //.body("transferAmount", equalTo(null))
        //.body("serviceFlow", Matchers.equalTo(serviceCode));
    }

    public static ValidatableResponse performServiceRequestAndWaitForCompletionStatusForSyncAPI(String serviceCode, JsonPathOperation[] operations, Matcher<String> matcher) {
        ValidatableResponse response = postServiceRequest(serviceCode, operations).log().all().statusCode(200);
        CoreMatchers.anyOf(is("SUCCEEDED"), is("FAILED"), is("PAUSED"));
        //return response.assertThat().body("status", matcher);
        return response;
    }


    public static ValidatableResponse performServiceRequestResumeAndWaitForSuccessForSyncAPI(String serviceCode, JsonPathOperation... operations) {
        return performServiceRequestResumeAndWaitForCompletionStatusForSyncAPI(serviceCode, operations, Matchers.equalTo("SUCCEEDED"))
                //.body("serviceRequestId", notNullValue())
                //.body("transactionAmount", equalTo(null))
                //.body("transferAmount", equalTo(null))
                .body("serviceFlow", Matchers.equalTo(serviceCode));
    }

    public static ValidatableResponse performServiceRequestResumeAndWaitForFailedForSyncAPI(String serviceCode, JsonPathOperation... operations) {
        return performServiceRequestResumeAndWaitForCompletionStatusForSyncAPI(serviceCode, operations, Matchers.equalTo("FAILED"))
                //.body("serviceRequestId", notNullValue())
                //.body("transactionAmount", equalTo(null))
                //.body("transferAmount", equalTo(null))
                .body("serviceFlow", Matchers.equalTo(serviceCode));
    }

    public static ValidatableResponse performServiceRequestResumeAndWaitForCompletionStatusForSyncAPI(String serviceCode, JsonPathOperation[] operations, Matcher<String> matcher) {
        ValidatableResponse response = postServiceRequestResume(serviceCode, operations).log().all().statusCode(200);
        CoreMatchers.anyOf(is("SUCCEEDED"), is("FAILED"), is("PAUSED"));
        //return response.assertThat().body("status", matcher);
        return response;
    }

    public static ValidatableResponse postServiceRequest(String serviceCode, JsonPathOperation... operations) {
        //Alternate this.
        headerBasedTenant = !headerBasedTenant;
        return postServiceRequestToJigsaw("/jigsaw/serviceRequest", serviceCode, headerBasedTenant,
                (op) -> ServiceRequestContracts.serviceRequestJson(!headerBasedTenant, op), operations);
    }

    public static ValidatableResponse postServiceRequestResume(String serviceCode, JsonPathOperation... operations) {
        headerBasedTenant = !headerBasedTenant;
        return postServiceRequestToJigsaw("/jigsaw/serviceRequest/resume", serviceCode, headerBasedTenant,
                (op) -> ServiceRequestContracts.serviceRequestResumeJson(!headerBasedTenant, op), operations);
    }

    public static ValidatableResponse postServiceRequestResume(String serviceCode, boolean tenantIdInHeader, RequestTemplate requestTemplate,
                                                               JsonPathOperation... operations) {
        return postServiceRequestToJigsaw("/jigsaw/serviceRequest/resume",
                serviceCode, tenantIdInHeader, requestTemplate, operations);
    }

    public static ValidatableResponse postServiceRequestToJigsaw(String postUrl,
                                                                 String serviceCode,
                                                                 boolean tenantIdInHeader,
                                                                 RequestTemplate requestTemplate,
                                                                 JsonPathOperation... operations) {

        JsonPathOperation[] operationForUseInRequest = operations;
        LOGGER.info("Executing jigsaw with service code in body: {}", useServiceCodeInBody);
        if (useServiceCodeInBody) {
            operationForUseInRequest = new JsonPathOperation[operations.length + 1];
            System.arraycopy(operations, 0, operationForUseInRequest, 0, operations.length);
            operationForUseInRequest[operations.length] = JsonPathOperation.set("serviceFlowId", serviceCode);
            //Alternate this.
            useServiceCodeInBody = !useServiceCodeInBody;
        } else {
            postUrl += "/" + serviceCode;
        }
        if (tenantIdInHeader) {
            return moneyUri().given().log().all()
                    .contentType(ContentType.JSON).accept(ContentType.JSON)
                    .header(new Header("mfsTenantId", getTenantId()))
                    .body(requestTemplate.requestTemplate(operationForUseInRequest))
                    .post(postUrl).then().log().all();
        }
        return moneyUri().given().log().all()
                .contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(requestTemplate.requestTemplate(operationForUseInRequest))
                .post(postUrl).then().log().all();

    }

    public static ValidatableResponse performServiceRequestAndWaitForSuccessForSyncAPIWithoutTransactionid(String serviceCode, JsonPathOperation... operations) {
        return performServiceRequestAndWaitForCompletionStatusForSyncAPI(serviceCode, operations, Matchers.equalTo("SUCCEEDED"))
                //.body("errors", nullValue())
                //.body("serviceRequestId", notNullValue())
                //.body("message", notNullValue())
                //.body("transactionId", nullValue())
                //.body("transactionAmount", equalTo(null))
                //.body("transferAmount", equalTo(null))
                .body("serviceFlow", Matchers.equalTo(serviceCode));
    }

    public static ValidatableResponse postServiceRequestCancel(String serviceCode, JsonPathOperation... operations) {
        headerBasedTenant = !headerBasedTenant;
        return postServiceRequestToJigsaw("/jigsaw/serviceRequest/cancel", serviceCode, headerBasedTenant,
                (op) -> ServiceRequestContracts.serviceRequestCancelJson(!headerBasedTenant, op), operations);
    }

    //OTP
    public static ValidatableResponse performServiceRequestOTPResumeAndWaitForPauseForSyncAPI(String serviceCode, JsonPathOperation... operations) {
        return performServiceRequestOTPResumeAndWaitForCompletionStatusForSyncAPI(serviceCode, operations, Matchers.equalTo("PAUSED"))
                //.body("serviceRequestId", notNullValue())
                //.body("transactionAmount", equalTo(null))
                //.body("transferAmount", equalTo(null))
                .body("serviceFlow", Matchers.equalTo(serviceCode));
    }

    public static ValidatableResponse performServiceRequestOTPResumeAndWaitForCompletionStatusForSyncAPI(String serviceCode, JsonPathOperation[] operations, Matcher<String> matcher) {
        ValidatableResponse response = postServiceRequestOTPResume(serviceCode, operations).log().all().statusCode(200);
        CoreMatchers.anyOf(is("SUCCEEDED"), is("FAILED"), is("PAUSED"));
        return response.assertThat().body("status", matcher);
    }

    public static ValidatableResponse postServiceRequestOTPResume(String serviceCode, JsonPathOperation... operations) {
        return postServiceRequestResume(serviceCode, false, ServiceRequestContracts::serviceRequestOTPResumeJson, operations);
    }

    public static ValidatableResponse getOTP(String serviceRequestId) {
        return moneyUriInternalApi().accept(ContentType.JSON)
                .header("mfsTenantId", getTenantId())
                .get("/sv/internal/otp/" + serviceRequestId)
                .then().statusCode(200)
                .body("[0].serviceRequestId", equalTo(serviceRequestId))
                .body("[0].status", equalTo("PENDING"))
                .body("serviceRequestId", notNullValue());
    }

    public static ValidatableResponse getNotificationMessage(String msisdn) {
        return moneyUriInternalApi().accept(ContentType.JSON)
                .header("mfsTenantId", getTenantId())
                .get("/notification/internal/getMessage?toWhom=" + msisdn)
                .then().statusCode(200);

    }

    public static void validateHasErrorCode(ValidatableResponse response, String errorCode, Object... params) {
        String message = getMessage(errorCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params);
        }
        response.assertThat()
                .body("errors", notNullValue())
                //DEF MON-3128 .body("txnStatus", is("TF"))
                .body("errors.code", hasItem(errorCode))
                .body("errors.message", hasItem(message));
    }

    public static ValidatableResponse setJigsawProperties(Map properties) {
        return moneyUriInternalApi().contentType(ContentType.JSON).accept(ContentType.JSON)
                .body((properties))
                .header("mfsTenantId", getTenantId())
                .post("/jigsaw/internal/configuration")
                .then().statusCode(200);
    }

    public static ValidatableResponse getSFMFlow(String FlowName){
        return moneyUriInternalApi().contentType(ContentType.JSON).accept(ContentType.JSON)
                .header("mfsTenantId",getTenantId())
                .get("/sfm/admin/serviceFlow/"+FlowName)
                .then().statusCode(200).log().all();

    }



    @FunctionalInterface
    private interface RequestTemplate {
        String requestTemplate(JsonPathOperation... operations);
    }
}
