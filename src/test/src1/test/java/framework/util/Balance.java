package framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

public class Balance {
    private static final Logger LOGGER = LoggerFactory.getLogger(Balance.class);

    private final Future<BigDecimal> future;

    public Balance(Future<BigDecimal> future) {
        this.future = future;
    }

    public BigDecimal minus(BigDecimal subtrahend) {
        BigDecimal amount = amount();
        LOGGER.debug("Minus: Amount: {} Subtrahend: {}", amount.toString(), subtrahend.toString());
        return amount.subtract(subtrahend);
    }

    public BigDecimal minus(Balance subtrahend) {
        BigDecimal amount = amount();
        LOGGER.debug("Minus: Amount: {} Subtrahend: {}", amount.toString(), subtrahend.toString());
        return amount.subtract(subtrahend.amount());
    }

    public BigDecimal minus(String subtrahend) {
        return minus(new BigDecimal(subtrahend));
    }

    public BigDecimal plus(String augend) {
        return plus(new BigDecimal(augend));
    }

    public BigDecimal plus(BigDecimal augend) {
        BigDecimal amount = amount();
        LOGGER.debug("Plus: Amount: {} Augend: {}", amount.toString(), augend.toString());
        return amount.add(augend);
    }

    public BigDecimal amount() {
        try {
            return future.get();
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    public Map getBalancesMap() {
        Map balancesMap = new HashMap<>();
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        balancesMap.put("", "");
        return balancesMap;
    }
}
