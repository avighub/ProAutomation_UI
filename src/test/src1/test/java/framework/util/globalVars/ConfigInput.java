package framework.util.globalVars;

import framework.util.propertiesManagement.MfsTestProperties;

/**
 * Created by Automation Team on 5/5/2017.
 */
public class ConfigInput {
    public static String URL,
            defaultPass,
            prefLanguage,
            mPin,
            tPin,
            userCreationPassword,
            superadminUserId,
            superadminPass,
            addZoneAndArea,
            channelDomainCode,
            superadminSchemaPass,
            languageCodeUI,
            menuManagerMakerUrl,
            menuManagerCheckerUrl,
            menuManagerServletUrl;

    public static boolean isCoreRelease,
            is4o14Release,
            is4o9Release,
            generateServerLog,
            isSecondryProvider = false,
            isInternetExplorer = false,
            runChannelUserCreation,
            isAssert = true,
            isConfirm = true,
            shouldClickNext = true,
            confirmReq = true,
            changePin = true,
            dbMsgVerificationRequired,
            isDBMessageEncrypted;

    public static int additionalWaitTime,
            loginSync,
            contentSwitchSync,
            smallWait;

    /**
     * CONFIGURATION INPUT
     */
    public static void init() {
        System.out.println("*** Started ConfigInput.init *** \n");
        MfsTestProperties Me = MfsTestProperties.getInstance();

        URL = Me.getProperty("web.url");
        superadminUserId = Me.getProperty("web.superadmin.username");
        superadminPass = Me.getProperty("web.superadmin.password");
        superadminSchemaPass = Me.getProperty("web.superadmin.schema.password");
        defaultPass = Me.getProperty("web.newuser.password");
        userCreationPassword = Me.getProperty("web.usercreation.password");
        prefLanguage = Me.getProperty("web.language.code");
        mPin = Me.getProperty("web.user.mpin");
        tPin = Me.getProperty("web.user.tpin");
        languageCodeUI = Me.getProperty("web.language.code.UI");
        channelDomainCode = Me.getProperty("channel.parentDomain.code");
        addZoneAndArea = Me.getProperty("run.baseset.addZone");
        menuManagerMakerUrl = Me.getProperty("menumanager.maker.url");
        menuManagerCheckerUrl = Me.getProperty("menumanager.checker.url");
        menuManagerServletUrl = Me.getProperty("menumanager.servlet.url");

        isCoreRelease = Me.isPropertySetToYes("version.isCoreRelease");
        is4o9Release = Me.isPropertySetToYes("version.is4o9Release");
        is4o14Release = Me.isPropertySetToYes("version.is4o14Release");
        generateServerLog = Me.isPropertySetToYes("generate.server.logs");
        runChannelUserCreation = Me.isPropertySetToYes("run.baseset.channeluser");
        dbMsgVerificationRequired = Me.isPropertySetToYes("db.msg.verification.required");
        isDBMessageEncrypted = Me.isPropertySetToYes("is.db.msg.encrypted");

        if (Me.getProperty("browser.name").equalsIgnoreCase("ie")) {
            isInternetExplorer = true;
            additionalWaitTime = 600; // millisecond
            loginSync = 1200;
            contentSwitchSync = 600;
            smallWait = 800;
        } else {
            additionalWaitTime = 250; // millisecond
            loginSync = 800;
            contentSwitchSync = 200;
            smallWait = 300;
        }
    }
}
