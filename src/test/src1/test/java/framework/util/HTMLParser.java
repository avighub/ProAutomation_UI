package framework.util;


import framework.entity.CITReport;
import framework.util.reportManager.ExtentReader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class HTMLParser {
    static Object[][] ReporterData;

    public HTMLParser() {
    }

    public static Object[][] generateReporterObject() throws IOException {
        ExtentReader HTMLReader = new ExtentReader();
        CITReport citReport = new CITReport();
        String fileContent = HTMLReader.readFile(CITReport.INPUTHTML_PATH);
        Document doc = Jsoup.parse(fileContent, "", Parser.xmlParser());
        int ObjSize, counter;

        Element e, TestCaseStatus_PROG;
        Iterator var6;
        String ModuleName;
        String testCaseDescription = null;
        String testCaseTime = null;
        String testCaseType = null;
        String infos = null;

        ObjSize = doc.select("div[class='node-name']").size();
        ReporterData = new Object[ObjSize][13];
        counter = 0;
        var6 = doc.select("li[class^='test displayed active has-leaf']").iterator();

        while (var6.hasNext()) {
            e = (Element) var6.next();
            ModuleName = e.select("span[class='test-name']").text();

            for (Iterator var9 = e.select("div[class='collapsible-header']").iterator(); var9.hasNext(); ++counter) {
                Element f = (Element) var9.next();
                String testCaseID = null;
                String testStatus = null;

                try {
                    Element mainNode = f.children().select("div[class='node-name']").first();
                    testCaseID = mainNode.text();
                    if (testCaseID.toLowerCase().contains("setup") || testCaseID.toLowerCase().contains("teardown") || testCaseID.contains("Load Environment Variables and Base Set") || testCaseID.contains("Base User Creation")) {
                        counter--;
                        continue;
                    }
                    TestCaseStatus_PROG = f.nextElementSibling().select("div[class='node-desc']").first();
                    testCaseDescription = TestCaseStatus_PROG.text();

                    //Additional things added
                    testCaseType = TestCaseStatus_PROG.firstElementSibling().text().replaceAll("\\s", "\n");
                    infos = TestCaseStatus_PROG.nextElementSibling().getElementsByClass("step-details").text();
                    System.out.println(infos);
                    System.out.println((char) 9679 + testCaseType);

                    Element ExecutionTime_PROG = mainNode.nextElementSibling();

                    testCaseTime = (new SimpleDateFormat(citReport.CTDATEFORMAT_INTERNAL)).format(new Date(ExecutionTime_PROG.text()));

                    Element Duration_PROG = ExecutionTime_PROG.nextElementSibling();
                    Element TestStatus_PROG = Duration_PROG.nextElementSibling();
                    testStatus = TestStatus_PROG.text();
                    testStatus = Character.toString(testStatus.charAt(0)).toUpperCase() + testStatus.substring(1);
                } catch (Exception ex) {
                    System.err.println("Error while reading HTML file");
                    ex.printStackTrace();
                }

                ReporterData[counter][0] = citReport.getBuildID();
                ReporterData[counter][1] = citReport.getLeadName();
                ReporterData[counter][2] = citReport.getTestFrameworkIP();
                ReporterData[counter][3] = citReport.getTestFrameworkName();
                ReporterData[counter][4] = citReport.getTestFrameworkSVNPath();
                ReporterData[counter][5] = testCaseTime;
                ReporterData[counter][6] = citReport.getProductInterface();
                ReporterData[counter][7] = testCaseID;
                ReporterData[counter][8] = testCaseDescription;
                ReporterData[counter][9] = testStatus;
                //sending blank value in info as it is taking more space
                ReporterData[counter][10] = "";
                ReporterData[counter][11] = testCaseType;
                ReporterData[counter][12] = ModuleName;
                //ReporterData[counter][13] = testCaseType;
            }
        }

        return ReporterData;
    }


    public static Object[][] generateReporterObject(String path) throws IOException {
        ExtentReader HTMLReader = new ExtentReader();
        CITReport citReport = new CITReport();
        String FileContent = HTMLReader.readFile(path);
        Document doc = Jsoup.parse(FileContent, "", Parser.xmlParser());
        int ObjSize;
        int counter;
        Element e;
        Iterator var6;
        String ModuleName;
        String TestCaseDescription = null;
        String TestCaseTime = null;

        Element TestCaseStatus_PROG;

        ObjSize = doc.select("div[class='node-name']").size();
        ReporterData = new Object[ObjSize][13];
        counter = 0;
        var6 = doc.select("li[class^='test displayed active has-leaf']").iterator();

        while (var6.hasNext()) {
            e = (Element) var6.next();
            ModuleName = e.select("span[class='test-name']").text();

            for (Iterator var9 = e.select("div[class='collapsible-header']").iterator(); var9.hasNext(); ++counter) {
                Element f = (Element) var9.next();
                String TestCaseID = null;
                String TestStatus = null;

                try {
                    Element TestCaseID_PROG = f.children().select("div[class='node-name']").first();
                    TestCaseID = TestCaseID_PROG.text();
                    TestCaseStatus_PROG = f.nextElementSibling().select("div[class='node-desc']").first();
                    TestCaseDescription = TestCaseStatus_PROG.text();
                    Element ExecutionTime_PROG = TestCaseID_PROG.nextElementSibling();
                    TestCaseTime = ExecutionTime_PROG.text();
                    // TestCaseDescription = ExecutionTime_PROG.text();
                    TestCaseTime = (new SimpleDateFormat(citReport.CTDATEFORMAT_INTERNAL)).format(new Date(TestCaseTime));

                    Element Duration_PROG = ExecutionTime_PROG.nextElementSibling();
                    Element TestStatus_PROG = Duration_PROG.nextElementSibling();
                    TestStatus = TestStatus_PROG.text();
                    TestStatus = Character.toString(TestStatus.charAt(0)).toUpperCase() + TestStatus.substring(1);
                } catch (Exception var20) {
                    ;
                }

                ReporterData[counter][0] = citReport.getBuildID();
                ReporterData[counter][1] = citReport.getLeadName();
                ReporterData[counter][2] = citReport.getTestFrameworkIP();
                ReporterData[counter][3] = citReport.getTestFrameworkName();
                ReporterData[counter][4] = citReport.getTestFrameworkSVNPath();
                ReporterData[counter][5] = TestCaseTime;
                //ReporterData[counter][5] = citReport.getTestExecutionDateTime();
                ReporterData[counter][6] = citReport.getProductInterface();
                ReporterData[counter][7] = TestCaseID;
                ReporterData[counter][8] = TestCaseDescription;
                ReporterData[counter][9] = TestStatus;
                ReporterData[counter][10] = "";
                ReporterData[counter][11] = "";
                ReporterData[counter][12] = ModuleName;
            }
        }

        return ReporterData;
    }
}
