package framework.util.reportManager;

import framework.util.globalConstant.FilePath;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @Author - Core team, to generate a excel based final report , executed at the end of each suite
 */
public class SimpleReport {
    private static Object[][] getSimpleReportObject(String filePath) throws IOException {

        List<String> arrStringToExclude = Arrays.asList(
                "setup",
                "load environment variables and base set",
                "teardown",
                "pre-condition",
                "condition",
                "pre",
                "channel",
                "pre-conditions for stock initiation and approval",
                "baseset user creation",
                "base user creation"
        );

        cleanUpSimpleReport(FilePath.fileSimpleReport);
        ExtentReader extentReader = new ExtentReader();
        Object[][] reporterData = null;
        try {
            String FileContent = extentReader.readFile(filePath);
            Document doc = Jsoup.parse(FileContent, "", Parser.xmlParser());

            int objSize = doc.select("div[class='node-name']").size();
            reporterData = new Object[objSize + 150][6];
            Iterator var6 = doc.select("li[class^='test displayed active has-leaf']").iterator();
            int counter = 0;

            while (var6.hasNext()) {
                Element e = (Element) var6.next();
                String moduleName = e.select("span[class='test-name']").text();
                boolean noTestFound = true;
                for (Iterator var9 = e.select("div[class='collapsible-header']").iterator(); var9.hasNext(); ++counter) {
                    Element f = (Element) var9.next();

                    Element rootId = f.children().select("div[class='node-name']").first();
                    String testCaseID = rootId.text();

                    if (arrStringToExclude.contains(testCaseID.toLowerCase())) {
                        counter--;
                        continue;
                    }
                    noTestFound = false;
                    String testCaseTime = rootId.nextElementSibling().nextElementSibling().text();
                    testCaseTime = "" + getTimeInSeconds(testCaseTime);
                    String testStatus = rootId.nextElementSibling().nextElementSibling().nextElementSibling().text().toUpperCase();

                    String testCaseDescription = "";
                    if (f.nextElementSibling() == null) {
                        // no test steps were generated fail the test
                        testCaseDescription = "No Description";
                        testStatus = "No Run";
                    } else {
                        Element rootDesc = f.nextElementSibling().select("div[class='node-desc']").first();
                        if (rootDesc != null) {
                            testCaseDescription = rootDesc.text();
                        }
                    }


                    // write data
                    reporterData[counter][0] = moduleName;
                    reporterData[counter][1] = testCaseID;
                    reporterData[counter][2] = testCaseDescription;
                    reporterData[counter][3] = testStatus;
                    reporterData[counter][4] = testCaseTime;
                }

                // check for any skipped tests
                if (noTestFound) {
                    Element skipNode = e.select("div[class='test-steps']").first();
                    if (skipNode == null) {
                        continue;
                    }
                    String testStep = skipNode.text();
                    //Test Case Executed Skipped!
                    if (testStep.contains("Test Case Execution Skipped")) {
                        String[] tests = testStep.split(": Test Case Execution Skipped!");
                        for (String testString : tests) {
                            String[] tempArr = testString.trim().split(" ");
                            String testId = tempArr[tempArr.length - 1];
                            reporterData[counter][0] = moduleName;
                            reporterData[counter][1] = testId;
                            reporterData[counter][2] = "Test Skipped";
                            reporterData[counter][3] = "Skip";
                            reporterData[counter][4] = "0";
                            counter++;
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Object[]> tempList = null;
        // determine iteration for a specific test case
        if (reporterData != null) {
            tempList = new ArrayList<Object[]>(Arrays.asList(reporterData));
            int itr = 1;
            for (int n = 0; n < tempList.size(); n++) {

                if (n + 1 == tempList.size() || tempList.get(n)[1] == null) {
                    if (tempList.get(n)[5] == null) {
                        tempList.get(n)[5] = "1";
                    }
                    continue;
                }
                if (tempList.get(n)[1].equals(tempList.get(n + 1)[1]) && tempList.get(n)[3].equals("FAIL")) {
                    itr++;
                    tempList.get(n + 1)[5] = itr + "";
                    tempList.remove(n);
                    n--;
                } else {
                    itr = 1; // reset iterator
                    if (tempList.get(n)[5] == null) {
                        tempList.get(n)[5] = itr + "";
                    }
                }
            }
        }
        Object[][] asd = (Object[][]) tempList.toArray(new Object[][]{});
        return asd;

    }

    public static void writeSimpleReport(String fileName, String suiteName) throws IOException {
        InputStream inp = null;
        FileOutputStream fileOut = null;
        try {
            Object[][] oReport = getSimpleReportObject(FilePath.dirReports + fileName);

            inp = new FileInputStream(FilePath.fileSimpleReport);
            XSSFWorkbook wbx = new XSSFWorkbook(inp);
            XSSFSheet sheet = wbx.getSheetAt(0);

            CellStyle styleNum = wbx.createCellStyle();
            styleNum.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleNum.setBottomBorderColor(IndexedColors.GREY_40_PERCENT.getIndex());
            styleNum.setAlignment(CellStyle.ALIGN_CENTER);
            styleNum.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            CellStyle styleNum1 = wbx.createCellStyle();
            styleNum1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleNum1.setBottomBorderColor(IndexedColors.GREY_40_PERCENT.getIndex());
            styleNum1.setAlignment(CellStyle.ALIGN_RIGHT);
            styleNum1.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            CellStyle styleDesc = wbx.createCellStyle();
            styleDesc.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleDesc.setBottomBorderColor(IndexedColors.GREY_40_PERCENT.getIndex());
            styleDesc.setWrapText(true);
            styleDesc.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            CellStyle stylePass = wbx.createCellStyle();
            stylePass.setFillPattern(CellStyle.SOLID_FOREGROUND);
            stylePass.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            stylePass.setBottomBorderColor(IndexedColors.GREY_40_PERCENT.getIndex());
            stylePass.setAlignment(CellStyle.ALIGN_CENTER);
            stylePass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
            Font fontPass = wbx.createFont();
            fontPass.setColor(IndexedColors.WHITE.getIndex());
            stylePass.setFont(fontPass);
            stylePass.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            CellStyle styleFail = wbx.createCellStyle();
            styleFail.setFillPattern(CellStyle.SOLID_FOREGROUND);
            styleFail.setAlignment(CellStyle.ALIGN_CENTER);
            styleFail.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleFail.setBottomBorderColor(IndexedColors.GREY_40_PERCENT.getIndex());
            styleFail.setFillForegroundColor(IndexedColors.RED.getIndex());
            Font fontFail = wbx.createFont();
            fontFail.setColor(IndexedColors.WHITE.getIndex());
            styleFail.setFont(fontFail);
            styleFail.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            CellStyle styleOther = wbx.createCellStyle();
            styleOther.setFillPattern(CellStyle.SOLID_FOREGROUND);
            styleOther.setAlignment(CellStyle.ALIGN_CENTER);
            styleOther.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleOther.setBottomBorderColor(IndexedColors.GREY_40_PERCENT.getIndex());
            styleOther.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
            Font fontOther = wbx.createFont();
            fontOther.setColor(IndexedColors.WHITE.getIndex());
            styleOther.setFont(fontOther);
            styleOther.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            for (int i = 0; i < oReport.length; i++) {
                if (Arrays.asList(oReport[i]).contains(null)) {
                    continue;
                }
                XSSFRow row = sheet.getRow(i + 9); // skip the report details
                if (row == null) {
                    row = sheet.createRow(i + 9);
                }
                int j = 0;
                while (j <= 6) {
                    Cell cell = row.getCell(j);
                    if (cell == null) {
                        cell = row.createCell(j);
                    }

                    // Set Cell Style
                    if (j == 0 || j == 2 || j == 6) {
                        cell.setCellStyle(styleNum);
                    } else if (j == 3 || j == 1) {
                        cell.setCellStyle(styleDesc);
                    } else if (j == 4) {
                        if (oReport[i][j - 1].toString().equalsIgnoreCase("pass") ||
                                oReport[i][j - 1].toString().equalsIgnoreCase("warning")) {
                            oReport[i][j - 1] = "PASS";
                            cell.setCellStyle(stylePass);
                        } else if (oReport[i][j - 1].toString().equalsIgnoreCase("fail")) {
                            cell.setCellStyle(styleFail);
                        } else {
                            cell.setCellStyle(styleOther);
                        }
                    } else if (j == 5) {
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        cell.setCellStyle(styleNum1);
                    }

                    //set data
                    if (j == 0) {
                        cell.setCellValue((i + 1));
                    } else if (j == 5) {
                        int tVal = Integer.parseInt(oReport[i][j - 1].toString());
                        cell.setCellValue(tVal);
                    } else {
                        cell.setCellValue(oReport[i][j - 1].toString());
                    }
                    j++;
                }
            }

            // write the header
            sheet.getRow(0).getCell(2).setCellValue(suiteName);

            // write the Link to Report File
            XSSFCreationHelper helper = wbx.getCreationHelper();
            XSSFHyperlink fileLink = helper.createHyperlink(Hyperlink.LINK_FILE);

            fileLink.setAddress(getSharedReportPath(fileName));
            sheet.getRow(1).getCell(2).setCellValue("Click to Open the Extent Report");
            sheet.getRow(1).getCell(2).setHyperlink(fileLink);

            Cell cTotal = sheet.getRow(2).getCell(2);
            cTotal.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cTotal.setCellFormula("COUNTA(C10:C2000)");

            Cell cPass = sheet.getRow(3).getCell(2);
            cPass.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cPass.setCellFormula("COUNTIF(E10:E2000, \"PASS\")");

            Cell cFail = sheet.getRow(4).getCell(2);
            cFail.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cFail.setCellFormula("COUNTIF(E10:E2000, \"FAIL\")");

            Cell cOther = sheet.getRow(5).getCell(2);
            cOther.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cOther.setCellFormula("C3-C4-C5");

            Cell cTime = sheet.getRow(6).getCell(2);
            cTime.setCellType(HSSFCell.CELL_TYPE_FORMULA);
            cTime.setCellFormula("SUM(F10:F2000)/86400");

            fileOut = new FileOutputStream(FilePath.fileSimpleReport);
            wbx.write(fileOut);

            // generate re run File


        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (inp != null) inp.close();
            if (fileOut != null) fileOut.close();
        }
    }

    private static int getTimeInSeconds(String strTime) {

        //0h 0m 18s+557ms
        if (strTime.indexOf("s+") > 0) {
            String[] arrTime = strTime.split(" ");
            int timeh = Integer.parseInt(arrTime[0].replaceAll("[^0-9 ]", "").replaceAll(" +", " ").trim()) * 360;
            int timem = Integer.parseInt(arrTime[1].replaceAll("[^0-9 ]", "").replaceAll(" +", " ").trim()) * 60;
            int times = Integer.parseInt(arrTime[2].split("s+")[0]);
            return timeh + timem + times;
        }
        return 0;
    }

    private static void cleanUpSimpleReport(String filename) throws IOException {
        InputStream inp = null;
        FileOutputStream fileOut = null;
        try {
            inp = new FileInputStream(filename);
            XSSFWorkbook wbx = new XSSFWorkbook(inp);
            XSSFSheet sheet = wbx.getSheetAt(0);
            int lastRowUsed = sheet.getLastRowNum() + 1;

            for (int i = 9; i <= lastRowUsed; i++) { // skip the report details

                XSSFRow row = sheet.getRow(i);
                if (row != null) {
                    sheet.removeRow(row);
                }
            }

            fileOut = new FileOutputStream(filename);
            wbx.write(fileOut);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (inp != null) inp.close();
            if (fileOut != null) fileOut.close();
        }
    }

    public static String getSharedReportPath(String fileName) {
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();

            // Check for the reports Folder
            String sharedReportFolderName = null;
            for (int i = 0; i < 25; i++) {
                // quite possible there are multiple workspace in a machine, and hence
                // shared reports folder's path may vary too
                if (i == 0) {
                    sharedReportFolderName = "reports"; // this may change based on the framework
                } else {
                    sharedReportFolderName = "reports" + i;
                }

                // now check if file exist in the network path
                Path nwPath = Paths.get("//" + inetAddress.getHostName() + "/" + sharedReportFolderName + "/" + fileName);
                if (Files.exists(nwPath)) {
                    break;
                }
            }

            return "file:////" + inetAddress.getHostName() + "/" + sharedReportFolderName + "/" + fileName;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "Extent report not available";

    }


    @Test
    public void Run() throws IOException {
        SimpleReport.writeSimpleReport("UAT_MASTER_02-05-2019_23-18.html", "UAT Master");
    }
}
