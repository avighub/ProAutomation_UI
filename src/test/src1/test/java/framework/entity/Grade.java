package framework.entity;

import framework.util.common.DataFactory;

/**
 * Created by navin.pramanik
 */
public class Grade {
    public String DomainName, DomainCode, CategoryName, CategoryCode, GradeName, GradeCode,
            CREATED;
    public boolean isUpdated = false;
    public boolean isCreated = false;


    /**
     * Constructor
     *
     * @param categoryCode
     */
    public Grade(String categoryCode) {
        this.CategoryCode = categoryCode;
        setGradeDetails();
    }

    /**
     * @param domainName
     * @param categoryName
     * @param created
     */
    public Grade(String domainName, String categoryName, String created) {
        this.DomainName = domainName;
        this.CategoryName = categoryName;
        this.CREATED = created;
    }

    /**
     * Set if the Group role is Updated
     * set to false if it has to be created
     *
     * @param flag
     */
    public void setIsUpdated(boolean flag) {
        isUpdated = flag;
    }


    /**
     * Set is created Successfully
     */
    public void setIsCreated() {
        this.CREATED = "Y";
        this.isCreated = true;
    }

    public String getDomainCode() {
        return DomainCode;
    }

    public void setDomainCode(String domainCode) {
        this.DomainCode = domainCode;
    }

    public String getCategoryCode() {
        return CategoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.CategoryCode = categoryCode;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        this.GradeName = gradeName;
    }

    public String getGradeCode() {
        return GradeCode;
    }

    public void setGradeCode(String gradeCode) {
        this.GradeCode = gradeCode;
    }

    /**
     * Set Grade Role Details
     */
    public void setGradeDetails() {
        this.CREATED = "N";
        this.CategoryName = DataFactory.getCategoryName(this.CategoryCode);
        this.DomainName = DataFactory.getDomainName(this.CategoryCode);
        this.DomainCode = DataFactory.getDomainCode(this.CategoryCode);
        this.GradeCode = this.CategoryCode + DataFactory.getRandomNumber(2);
        this.GradeName = this.CategoryName + DataFactory.getRandomNumber(2);
    }


}
