package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.NumberConstants;

import java.io.IOException;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class CustomerTCP {
    public String ProviderName;
    public String DomainName;
    public String CategoryName;
    public String RegType;
    public String ProfileName;
    public String DomainCategory;

    // Markers
    public String CREATED = "";
    public String APPROVED = "";
    public String ERROR = "";
    public String REMARK = "";
    public String DBSTATUS = "";
    public boolean isCreated = false;

    /**
     * Construct Customer TCP Object
     *
     * @param domainName
     * @param categoryName
     * @param regType
     */
    public CustomerTCP(String domainName, String categoryName, String regType) {
        this.DomainName = domainName;
        this.CategoryName = categoryName;
        this.RegType = regType;
        this.ProfileName = "CustTCP" + DataFactory.getRandomNumberAsString(5);
    }

    /**
     * Constructor Cutomer TCP for UI validation
     *
     * @param profileName
     * @param domainCategory
     * @param regType
     * @param providerName
     */
    public CustomerTCP(String profileName, String domainCategory, String regType, String providerName) {
        this.ProfileName = profileName;
        this.DomainCategory = domainCategory;
        this.RegType = regType;
        this.ProviderName = providerName;
    }

    /**
     * Set Profile Name
     *
     * @param name
     */
    public void setProfileName(String name) {
        this.ProfileName = name;
    }

    /**
     * Set Is Created
     */
    public void setIsCreated() {
        this.CREATED = "Y";
        this.isCreated = true;
    }

    /**
     * Set Is Approved
     */
    public void setIsApproved() {
        this.CREATED = "Y";
        this.APPROVED = "Y";
    }

    /**
     * Set DB Status
     */
    public void setDbStatus() {
        this.DBSTATUS = "Y";
    }

    /**
     * Set Error Message
     *
     * @param error
     */
    public void setError(String error) {
        this.ERROR = error;
    }

    /**
     * Set Remark Message
     *
     * @param remark
     */
    public void setRemark(String remark) {
        this.REMARK = remark;
    }

    /**
     * Write To Output Excel File
     */
    public void writeDataToExcel() throws IOException {
        int lastRow = ExcelUtil.getExcelLastRow(FilePath.fileConsumerTCP);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_0, "" + this.ProfileName);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_1, "" + this.DomainName);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_2, "" + this.CategoryName);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_3, "" + this.RegType);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_4, "" + this.CREATED);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_5, "" + this.APPROVED);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_6, "" + this.ERROR);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_7, "" + this.REMARK);
        ExcelUtil.WriteDataToExcel(FilePath.fileConsumerTCP, lastRow, NumberConstants.CONST_8, "" + this.DBSTATUS);
    }
}
