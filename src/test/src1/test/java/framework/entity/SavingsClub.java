package framework.entity;

import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import org.testng.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SavingsClub {
    public String ClubName, CMMsisdn, Location, ClubType, CMGradeName, ClubWalletTCP,
            ClubBankTCP, ClubWalletMobileGrpRole, ClubBankMobileGrpRole, BankName, bankId, Domain, Category, Grade, ClubId;

    public int MinMemberCount, MaxMemberCount, MinApproverCount, MaxApproverCount;

    public String isApproved = "F";

    public String UserType;

    private List<User> members = new ArrayList<>();

    /**
     * @param chairman
     * @param clubType
     * @param bankId
     * @param usePrefMemberCount   - true if the member as value set at preferences
     * @param usePrefApproverCount - true if the Approver same as value set at preferences
     * @throws Exception
     */
    public SavingsClub(User chairman, String clubType, String bankId, boolean usePrefMemberCount, boolean usePrefApproverCount) throws Exception {

        try {
            // if bank id is not provided then get the default bank of default provider
            if (bankId == null) {
                this.bankId = DataFactory.getDefaultBankId(DataFactory.getDefaultProvider().ProviderId);
            } else {
                this.bankId = bankId;
            }

            this.BankName = DataFactory.getBankName(bankId);
            this.ClubName = "sClub" + chairman.GradeCode + DataFactory.getRandomString(5);
            this.CMMsisdn = chairman.MSISDN;
            this.Location = "Gurgaon";
            this.ClubType = clubType;
            this.CMGradeName = chairman.GradeName;
            this.ClubId = null;

            // set Chairman Details
            setDetails(chairman);

            // Set Min and Max Member Count
            this.MinMemberCount = (usePrefMemberCount) ? Integer.parseInt(AppConfig.defaultMinNumMember) : 2;
            this.MaxMemberCount = (usePrefMemberCount) ? Integer.parseInt(AppConfig.defaultMaxNumMember) : 2;

            // Set Min and Max Approver Count
            this.MinApproverCount = (usePrefApproverCount) ? Integer.parseInt(AppConfig.defaultMinNumApprover) : 2;
            this.MaxApproverCount = (usePrefApproverCount) ? Integer.parseInt(AppConfig.defaultMaxNumApprover) : 2;

            this.UserType = (DataFactory.isLeafDomain(chairman.CategoryCode)) ? "SUBSCRIBER" : "CHANNEL";

            chairman.setAsSvaApprover(); // set the Chairman as first approver
            this.addMember(chairman); // add chairman as first member
        } catch (Exception e) {
            Assertion.markAsFailure(e.getMessage());
        }
    }

    public SavingsClub(String clubName, String cmMsisdn, String location, String clubType, String cmGradeName, String clubWalletTCP, String clubBankTCP, String clubWalletMobileGrpRole, String clubBankMobileGrpRole, String bankName, String isApproved, String clubId) throws Exception {
        this.ClubName = clubName;
        this.CMMsisdn = cmMsisdn;
        this.Location = location;
        this.ClubType = clubType;
        this.CMGradeName = cmGradeName;
        this.ClubWalletTCP = clubWalletTCP;
        this.ClubBankTCP = clubBankTCP;
        this.ClubWalletMobileGrpRole = clubWalletMobileGrpRole;
        this.ClubBankMobileGrpRole = clubBankMobileGrpRole;
        this.BankName = bankName;
        this.isApproved = isApproved;
        this.ClubId = clubId;
    }

    public void setMemberCount(int minCount, int maxCount) {
        this.MinMemberCount = minCount;
        this.MaxMemberCount = maxCount;
    }

    public void setApproverCount(int minCount, int maxCount) {
        this.MinApproverCount = minCount;
        this.MaxApproverCount = maxCount;
    }

    public SavingsClub addMember(User member) {
        this.members.add(member);
        return this;
    }

    public List<User> getMembers() {
        return this.members;
    }

    public void setClubId(String id) {
        this.ClubId = id;
    }

    private void setDetails(User chairman) throws Exception {
        String providerName = DataFactory.getDefaultProvider().ProviderName;
        String savingWalletName = DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletName;

        try {
            this.ClubWalletTCP = DataFactory.getInstrumentTCP(chairman.DomainName, chairman.CategoryName, chairman.GradeName,
                    providerName, savingWalletName).ProfileName;

            this.ClubBankTCP = DataFactory.getInstrumentTCP(chairman.DomainName, chairman.CategoryName, chairman.GradeName,
                    providerName, this.BankName).ProfileName;

            this.ClubWalletMobileGrpRole = DataFactory.getMobileGroupRole(chairman.DomainName, chairman.CategoryName, chairman.GradeName,
                    providerName, savingWalletName).ProfileName;

            this.ClubBankMobileGrpRole = DataFactory.getMobileGroupRole(chairman.DomainName, chairman.CategoryName, chairman.GradeName,
                    providerName, this.BankName).ProfileName;

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.markAsFailure("Failed to set Chairman Details: Check AppData");
            Assert.fail("Stopping the execution, as Failed to Set the Club Data");
        }
    }


    public void writeDataToExcel() throws IOException {
        int lastRow = 1;
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 0, this.ClubName);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 1, this.CMMsisdn);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 2, this.Location);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 3, this.ClubType);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 4, this.CMGradeName);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 5, this.ClubWalletTCP);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 6, this.ClubBankTCP);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 7, this.ClubWalletMobileGrpRole);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 8, this.ClubBankMobileGrpRole);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 9, this.BankName);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 10, this.isApproved);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempSavingClub, lastRow, 11, this.ClubId);
    }
}
