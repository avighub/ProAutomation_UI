package framework.entity;

import framework.util.globalVars.ConfigInput;

/**
 * Created by rahul.rana on 8/9/2017.
 */
public class SuperAdmin {
    public String LoginId, Password, WebGroupRole;

    public SuperAdmin(String loginId, String webGroupRole) {
        this.LoginId = loginId;
        this.Password = ConfigInput.superadminPass;
        this.WebGroupRole = webGroupRole;
    }

}
