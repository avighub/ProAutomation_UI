package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;

import java.io.IOException;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class InstrumentTCP {
    public String CurrencyProvider, DomainName, CategoryName,
            GradeName, ProfileName, PaymentInstrument, InstrumentType,
            DomainCategory, UserMinBalance, UserMaxBalance, MinTransactionAmt, MaxTransactionAmt, MaxTransferAllowed;

    // Markers
    public String CREATED = "N";
    public String APPROVED = "N";
    public String ERROR = "";
    public String REMARK = "";
    public String DBSTATUS = "";
    public boolean isCreated = false;

    /**
     * Construct Instrument TCP Object
     *
     * @param domainName
     * @param categoryName
     */
    public InstrumentTCP(String provider, String domainName, String categoryName, String grade,
                         String payInstrument, String instrumentType) {
        this.CurrencyProvider = provider;
        this.DomainName = domainName;
        this.CategoryName = categoryName;
        this.GradeName = grade;
        this.PaymentInstrument = payInstrument;
        this.InstrumentType = instrumentType;
        this.ProfileName = "InstTCP" + DataFactory.getRandomNumberAsString(5);
        this.UserMinBalance = Constants.USR_MIN_BALANCE;
        this.UserMaxBalance = Constants.USR_MAX_BALACE;
        this.MinTransactionAmt = Constants.MIN_TRANSACTION_AMT;
        this.MaxTransactionAmt = Constants.MAX_TRANSACTION_AMT;
        this.MaxTransferAllowed = Constants.MAX_TRANSACTION_ALLOWED;

    }

    /**
     * For reading data set from Excel
     *
     * @param profileName
     * @param provider
     * @param domainName
     * @param categoryName
     * @param grade
     * @param payInstrument
     * @param instrumentType
     */
    public InstrumentTCP(String profileName, String provider, String domainName, String categoryName, String grade,
                         String payInstrument, String instrumentType) {
        this.CurrencyProvider = provider;
        this.DomainName = domainName;
        this.CategoryName = categoryName;
        this.GradeName = grade;
        this.PaymentInstrument = payInstrument;
        this.InstrumentType = instrumentType;
        this.ProfileName = profileName;
    }

    /**
     * Constructor for UI verifications
     *
     * @param domainCategory
     * @param grade
     * @param provider
     * @param tcpName
     */
    public InstrumentTCP(String domainCategory, String instrumentType, String grade, String provider, String tcpName) {
        this.DomainCategory = domainCategory;
        this.InstrumentType = instrumentType;
        this.GradeName = grade;
        this.CurrencyProvider = provider;
        this.ProfileName = tcpName;
        this.DomainName = domainCategory.split(" - ")[0].trim();
        this.CategoryName = domainCategory.split(" - ")[1].trim();
    }

    /**
     * Set Profile Name
     *
     * @param name
     */
    public void setProfileName(String name) {
        this.ProfileName = name;
    }

    public void setUserMinBalance(String userMinBalance) {
        UserMinBalance = userMinBalance;
    }

    public void setUserMaxBalance(String userMaxBalance) {
        UserMaxBalance = userMaxBalance;
    }

    public void setMinTransactionAmt(String minTransactionAmt) {
        MinTransactionAmt = minTransactionAmt;
    }

    public void setMaxTransactionAmt(String maxTransactionAmt) {
        MaxTransactionAmt = maxTransactionAmt;
    }

    public void setMaxTransferAllowed(String maxTransferAllowed) {
        MaxTransferAllowed = maxTransferAllowed;
    }

    /**
     * Set Is Created
     */
    public void setIsCreated() {
        this.CREATED = "Y";
    }

    /**
     * Set Is Approved
     */
    public void setIsApproved() {
        this.APPROVED = "Y";
        this.isCreated = true;
    }

    /**
     * Set DB Status
     */
    public void setDbStatus() {
        this.DBSTATUS = "Y";
    }

    /**
     * Set Error Message
     *
     * @param error
     */
    public void setError(String error) {
        this.ERROR = error;
    }

    /**
     * Set Remark Message
     *
     * @param remark
     */
    public void setRemark(String remark) {
        this.REMARK = remark;
    }

    /**
     * Write To Output Excel File
     */
    public void writeDataToExcel() throws IOException {
        int lastRow = ExcelUtil.getExcelLastRow(FilePath.fileInstrumentTCP);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 0, "" + this.ProfileName);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 1, "" + this.CurrencyProvider);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 2, "" + this.DomainName);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 3, "" + this.CategoryName);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 4, "" + this.GradeName);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 5, "" + this.PaymentInstrument);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 6, "" + this.InstrumentType);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 7, "" + this.CREATED);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 8, "" + this.APPROVED);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 9, "" + this.ERROR);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 10, "" + this.REMARK);
        ExcelUtil.WriteDataToExcel(FilePath.fileInstrumentTCP, lastRow, 11, "" + this.DBSTATUS);
    }
}
