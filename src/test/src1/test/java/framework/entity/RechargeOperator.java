package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.GlobalData;

/**
 * Created by Dalia on 07-09-2018.
 */
public class RechargeOperator {
    public String OperatorId, OperatorName, UserCode, GradeCode, CategoryCode, ProviderName, ProviderId, Email;

    public RechargeOperator(String providerId) {
        this.OperatorName = "OPT" + DataFactory.getRandomNumberAsString(5);
        this.OperatorId = null;
        this.UserCode = null;
        this.GradeCode = Constants.ZEBRA_MER;
        this.CategoryCode = Constants.ZEBRA_MER;
        this.ProviderName = DataFactory.getProviderName(providerId);
        this.ProviderId = providerId;
        this.Email = this.OperatorName + "@mail.com";
    }

    public RechargeOperator(String optId, String optName, String userCode, String gradeCode, String categoryCode, String providerId) {
        this.OperatorName = optName;
        this.OperatorId = optId;
        this.UserCode = userCode;
        this.GradeCode = gradeCode;
        this.CategoryCode = categoryCode;
        this.ProviderId = providerId;
        this.ProviderName = DataFactory.getProviderName(providerId);
        this.Email = optName + "@mail.com";
    }

    public void writeDataToExcel() throws Exception {
        String filePath = FilePath.fileTempUsers;
        int lastRow = ExcelUtil.getExcelLastRow(filePath, 1);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 0, this.ProviderName, 1);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 1, this.OperatorId, 1);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 2, this.OperatorName, 1);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 3, this.UserCode, 1);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 4, this.GradeCode, 1);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 5, this.CategoryCode, 1);
//        ExcelUtil.WriteDataToExcel(filePath, lastRow, 6, this.Email, 1);

    }
}
