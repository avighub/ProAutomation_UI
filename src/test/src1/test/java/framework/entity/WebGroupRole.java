package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.FilePath;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/7/2017.
 */
public class WebGroupRole {
    public String DomainName, DomainCode, CategoryName, CategoryCode, GradeName, GradeCode,
            RoleType, RoleName, MobRole_MFSProvider, MobRole_PaymentInstrument, MobRole_WalletType,
            CREATED, Remarks, ErrorMessage;

    //extra dependent data:
    public List<String> ApplicableRoles;

    public int NumberOfUser;
    public boolean isUpdated = false;
    public boolean isDuplicate = false;

    /**
     * Constructor Data for Excel RnR Data
     *
     * @param categoryCode
     * @param roleName
     * @param applicableRoles
     * @param numOfUser
     */
    public WebGroupRole(String categoryCode, String roleName, List<String> applicableRoles, int numOfUser) {
        this.CategoryCode = categoryCode;
        this.RoleName = roleName;
        this.ApplicableRoles = applicableRoles;
        this.NumberOfUser = numOfUser;
        setGroupRoleDetails();
    }

    /**
     * For Testing Purpose
     *
     * @param categoryCode
     * @param roleName
     * @param applicableRoles
     */
    public WebGroupRole(String categoryCode, String roleName, List<String> applicableRoles) {
        this.CategoryCode = categoryCode;
        this.RoleName = roleName;
        this.ApplicableRoles = applicableRoles;
        setGroupRoleDetails();
    }

    /**
     * Representaion of Excel Data
     *
     * @param domainName
     * @param categoryName
     * @param roleName
     * @param created
     */
    public WebGroupRole(String domainName, String categoryName, String roleName, String created) {
        this.DomainName = domainName;
        this.CategoryName = categoryName;
        this.RoleName = roleName;
        this.CREATED = created;
    }

    /**
     * Set Group Role Details
     */
    public void setGroupRoleDetails() {
        this.RoleType = "WEB";
        this.CREATED = "N";
        this.CategoryName = DataFactory.getCategoryName(this.CategoryCode);
        this.DomainName = DataFactory.getDomainName(this.CategoryCode);
        this.DomainCode = DataFactory.getDomainCode(this.CategoryCode);
    }

    /**
     * Set if the Group role is Updated
     * set to false if it has to be created
     *
     * @param flag
     */
    public void setIsUpdated(boolean flag) {
        isUpdated = flag;
    }

    public void setIsDuplicated() {
        isDuplicate = true;
    }

    /**
     * Set is created Successfully
     */
    public void setIsCreated() {
        this.CREATED = "Y";
    }

    public boolean isGroupRoleCreated() {
        return (this.CREATED == "Y") ? true : false;
    }

    /**
     * Set The message
     *
     * @param message
     */
    public void setRemark(String message) {
        Remarks = message;
    }

    public void removeRole(String roleName) {
        this.ApplicableRoles.remove(roleName);
    }

    public void setWebRoleName(String webRoleName) {
        this.RoleName = webRoleName;
    }

    /**
     * Write To Output Excel File
     */
    public void writeDataToExcel() throws IOException {
        int lastRow = ExcelUtil.getExcelLastRow(FilePath.fileGroupRoles);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 0, "" + this.DomainName);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 1, "" + this.CategoryName);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 2, "" + this.RoleType);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 3, "" + this.RoleName);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 4, "" + this.GradeName);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 5, "" + this.MobRole_MFSProvider);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 6, "" + this.MobRole_PaymentInstrument);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 7, "" + this.MobRole_WalletType);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 8, "" + this.CREATED);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 9, "" + this.ErrorMessage);
        ExcelUtil.WriteDataToExcel(FilePath.fileGroupRoles, lastRow, 10, "" + this.Remarks);
    }
}
