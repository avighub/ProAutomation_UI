package framework.entity;

import framework.dataEntity.ServiceList;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;

/**
 * Created by rahul.rana on 5/16/2017.
 */
public class ServiceCharge {
    public ServiceList ServiceInfo;
    public User Payer, Payee;

    public String ServiceChargeName, TransferRuleName, ServiceChargeId, ServiceChargeShortCode, payerGrade, payeeGrade;
    public String ApplicableDate = null;
    public boolean isExisting = false;
    public boolean isNFSC = false;
    public String MinTransctionAmount, MultipleOfTransctionAmount, MaxTransctionAmount, minFixedServiceCharge,
            maxFixedServiceCharge, MinCommissionPCT, MaxCommissionPCT, CommissionRangePCT, MinFixedCommission,
            MaxfixedCommission, CommissionFromRange, CommissionToRange, TransferRuleID, NFSCServiceCharge, NFSCCommission, serviceChargeFixed = null, commisisonRangeFixed = null;

    public void setServiceChargeFixed(String serviceChargeFixed) {
        this.serviceChargeFixed = serviceChargeFixed;
    }

    public void setCommisisonRangeFixed(String commisisonRangeFixed) {
        this.commisisonRangeFixed = commisisonRangeFixed;
    }

    /**
     * Service Charge Object for Financial Services
     *
     * @param serviceCode
     * @param payer
     * @param payee
     * @param payerPaymenType  - id
     * @param payeePaymentType - id
     * @param payerProvider
     * @param payeeProvider
     */
    public ServiceCharge(String serviceCode, User payer, User payee, String payerPaymenType,
                         String payeePaymentType, String payerProvider, String payeeProvider) {
        this.Payer = new User(payer);
        this.Payee = new User(payee);
        setServiceChargeDetails(serviceCode, payerPaymenType, payeePaymentType, payerProvider, payeeProvider);
    }


    /**
     * Service Charge object for Non Financial Services
     *
     * @param serviceCode
     * @param payer
     * @param payee
     * @param payerPaymenType
     * @param payeePaymentType
     * @param payerProvider
     * @param payeeProvider
     */
    public ServiceCharge(String serviceCode, User payer, OperatorUser payee, String payerPaymenType,
                         String payeePaymentType, String payerProvider, String payeeProvider) {

        this.Payer = new User(payer);
        this.Payee = new User(payee);
        setServiceChargeDetails(serviceCode, payerPaymenType, payeePaymentType, payerProvider, payeeProvider);
    }

    public ServiceCharge(String serviceCode, OperatorUser payer, User payee, String payerPaymenType,
                         String payeePaymentType, String payerProvider, String payeeProvider) {
        this.Payer = new User(payer);
        this.Payee = new User(payee);
        setServiceChargeDetails(serviceCode, payerPaymenType, payeePaymentType, payerProvider, payeeProvider);
    }

    public ServiceCharge(String serviceCode, OperatorUser payer, OperatorUser payee, String payerPaymenType,
                         String payeePaymentType, String payerProvider, String payeeProvider) {
        this.Payer = new User(payer);
        this.Payee = new User(payee);
        setServiceChargeDetails(serviceCode, payerPaymenType, payeePaymentType, payerProvider, payeeProvider);
    }

    public ServiceCharge(String serviceCode, User payer, Biller payee, String payerPaymenType,
                         String payeePaymentType, String payerProvider, String payeeProvider) {
        this.Payer = new User(payer);
        this.Payee = new User(payee);
        setServiceChargeDetails(serviceCode, payerPaymenType, payeePaymentType, payerProvider, payeeProvider);
    }

    public ServiceCharge(String serviceCode, Biller payer, OperatorUser payee, String payerPaymenType,
                         String payeePaymentType, String payerProvider, String payeeProvider) {
        this.Payer = new User(payer);
        this.Payee = new User(payee);
        setServiceChargeDetails(serviceCode, payerPaymenType, payeePaymentType, payerProvider, payeeProvider);
    }

    public ServiceCharge(String serviceCode, Biller payer, User payee, String payerPaymenType,
                         String payeePaymentType, String payerProvider, String payeeProvider) {
        this.Payer = new User(payer);
        this.Payee = new User(payee);
        setServiceChargeDetails(serviceCode, payerPaymenType, payeePaymentType, payerProvider, payeeProvider);
    }


    /**
     * Set Service Charge details
     *
     * @param payerPaymenTypeId
     * @param payeePaymentTypeId
     * @param payerProvider
     * @param payeeProvider
     */
    private void setServiceChargeDetails(String serviceCode, String payerPaymenTypeId, String payeePaymentTypeId,
                                         String payerProvider, String payeeProvider) {

        this.ServiceChargeName = serviceCode + DataFactory.getRandomNumber(4) + "sc";
        this.ServiceChargeId = this.ServiceChargeName;
        this.TransferRuleName = serviceCode + DataFactory.getRandomNumber(4) + "tr";
        this.ServiceInfo = DataFactory.getServiceInfo(serviceCode);
        this.ApplicableDate = DataFactory.getCurrentDateSlash();
        this.MultipleOfTransctionAmount = "1";
        this.NFSCServiceCharge = "0.1";
        this.NFSCCommission = "0.1";
        this.payerGrade = Constants.GRADE_ALL;
        this.payeeGrade = Constants.GRADE_ALL;
        this.CommissionFromRange = "1";


        /**
         * SET PAYER DETAIL
         */
        if (this.ServiceInfo.PayerPaymentType.equals("WALLET")) {
            this.Payer.dbPaymentTypeId = "PAYER_PAYMENT_TYPE_ID";
            this.ServiceInfo.PayerPaymentTypeUI = "Wallet";

            if (payerPaymenTypeId != null) {
                this.Payer.PaymentType = DataFactory.getWalletName(payerPaymenTypeId);
            } else {
                this.Payer.PaymentType = DataFactory.getDefaultWallet().WalletName;
            }
            this.Payer.PaymentTypeID = DataFactory.getWalletId(this.Payer.PaymentType);
        } else if (this.ServiceInfo.PayerPaymentType.equals("BANK")) {
            this.Payer.dbPaymentTypeId = "PAYER_BANK_ID";
            this.ServiceInfo.PayerPaymentTypeUI = "Bank";

            if (payerPaymenTypeId != null) {
                this.Payer.PaymentType = DataFactory.getBankName(payerPaymenTypeId);
            } else {
                this.Payer.PaymentType = DataFactory.getDefaultBankNameForDefaultProvider();
            }
            this.Payer.PaymentTypeID = DataFactory.getBankId(this.Payer.PaymentType);
        } else if (this.ServiceInfo.PayerPaymentType.equals("NONE")) {
            this.Payer.dbPaymentTypeId = "PAYER_PAYMENT_TYPE_ID";
            this.ServiceInfo.PayerPaymentTypeUI = "NONE";

            if (payerPaymenTypeId != null) {
                this.Payer.PaymentType = DataFactory.getWalletName(payerPaymenTypeId);
            } else {
                this.Payer.PaymentType = DataFactory.getDefaultWallet().WalletName;
            }
            this.Payer.PaymentTypeID = DataFactory.getWalletId(this.Payer.PaymentType);
        } else {
            System.out.print("Check the Service Info Object! ConfigInput/ServiceList");
        }

        /**
         * SET PAYEE DETAIL
         */
        if (this.ServiceInfo.PayeePaymentType.equals("WALLET")) {
            this.Payee.dbPaymentTypeId = "PAYEE_PAYMENT_TYPE_ID";
            this.ServiceInfo.PayeePaymentTypeUI = "Wallet";

            if (payeePaymentTypeId != null) {
                this.Payee.PaymentType = DataFactory.getWalletName(payeePaymentTypeId);
            } else {
                this.Payee.PaymentType = DataFactory.getDefaultWallet().WalletName;
            }
            this.Payee.PaymentTypeID = DataFactory.getWalletId(this.Payee.PaymentType);
        } else if (this.ServiceInfo.PayeePaymentType.equals("BANK")) {
            setPayeePaymentType("BANK", payeePaymentTypeId);
        } else {
            System.out.print("Check the ServiceInfo object, and the ServiceList.csv Input file");
        }

        /**
         * SET THE CURRENCY PROVIDER DETAILS
         */
        if (payerProvider != null) {
            this.Payer.ProviderName = payerProvider;
            this.Payer.ProviderId = DataFactory.getProviderId(payerProvider);
        } else {
            this.Payer.ProviderName = DataFactory.getDefaultProvider().ProviderName;
            this.Payer.ProviderId = DataFactory.getDefaultProvider().ProviderId;
        }


        /*
         * Set currency Provider for PAYEE
         */
        if (payeeProvider != null) {
            this.Payee.ProviderName = payeeProvider;
            this.Payee.ProviderId = DataFactory.getProviderId(payeeProvider);
        } else {
            this.Payee.ProviderName = DataFactory.getDefaultProvider().ProviderName;
            this.Payee.ProviderId = DataFactory.getDefaultProvider().ProviderId;
        }

    }

    public void setCommissionFromRange(String commissionFromRange) {
        this.CommissionFromRange = commissionFromRange;
    }

    /**
     * Set the Service Charge Name
     *
     * @param name
     */
    public void setServiceChargeName(String name) {
        this.ServiceChargeName = name;
    }
    public void setServicePayeeProvider(String provider) {
        this.Payee.ProviderName = DataFactory.getProviderId(provider);
        this.Payee.ProviderId = provider;


    }

    public void setServicePayerProvider(String provider) {
        this.Payer.ProviderName = DataFactory.getProviderId(provider);;
        this.Payer.ProviderId = provider;
    }

    public void setShortName(String text) {
        this.ServiceChargeId = text;
    }

    public void setServiceChargeId(String id) {
        this.ServiceChargeId = id;
    }

    public void setServiceChargeShortCode(String code) {
        this.ServiceChargeShortCode = code;
    }

    public void setTransferRulName(String name) {
        this.TransferRuleName = name;
    }

    public void setTransferRuleID(String id) {
        this.TransferRuleID = id;
    }

    public void setApplicableDate(String date) {
        this.ApplicableDate = date;
    }

    public void setIsExisting() {
        this.isExisting = true;
    }

    public void setIsNFSC() {
        this.isNFSC = true;
    }

    public void setMultiplesofTransactionAmt(String rate) {
        this.MultipleOfTransctionAmount = rate;
    }

    public void setNFSCServiceCharge(String sc) {
        this.NFSCServiceCharge = sc;
    }

    public void setNFSCCommission(String sc) {
        this.NFSCCommission = sc;
    }

    public void setPayerGrade(String name) {
        this.payerGrade = name;
    }

    public void setPayeeGrade(String name) {
        this.payeeGrade = name;
    }

    public void setPayeePaymentType(String payType, String payeePaymentTypeId) {
        this.ServiceInfo.PayeePaymentType = payType;
        if (payType.equalsIgnoreCase("wallet")) {
            this.ServiceInfo.PayeePaymentTypeUI = "Wallet";
            this.Payee.dbPaymentTypeId = "PAYEE_PAYMENT_TYPE_ID";
        } else {
            this.Payee.dbPaymentTypeId = "PAYEE_BANK_ID";
            this.ServiceInfo.PayeePaymentTypeUI = "Bank";

            if (payeePaymentTypeId != null) {
                this.Payee.PaymentType = DataFactory.getBankName(payeePaymentTypeId);
            } else {
                this.Payee.PaymentType = DataFactory.getDefaultBankNameForDefaultProvider();
            }
            this.Payee.PaymentTypeID = DataFactory.getBankId(this.Payee.PaymentType);
        }

    }

}
