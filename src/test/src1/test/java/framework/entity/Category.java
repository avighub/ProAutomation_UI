package framework.entity;

import framework.util.common.DataFactory;

public class Category {

    private String categoryCode, categoryName, domainName, domainCode, parentCategory;

    public Category(String catCode) {
        categoryName = "CATEGORY" + DataFactory.getRandomNumber(3);
        categoryCode = DataFactory.getRandomNumber(5) + "C";
        domainCode = DataFactory.getDomainCode(catCode);
        domainName = DataFactory.getDomainName(catCode);
        //parentCategory = DataFactory.getParentCategoryName(this.categoryCode);
    }

    public Category() {
        categoryName = "CATEGORY" + DataFactory.getRandomNumber(3);
        categoryCode = DataFactory.getRandomNumber(5) + "C";
    }

    public Category (String categoryCode, String categoryName, String domainCode, String parentCategory){
        this.categoryCode = categoryCode;
        this.categoryName = categoryName;
        this.domainCode = domainCode;
        this.domainName = DataFactory.getDomainName(categoryCode);

        this.parentCategory = parentCategory;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setDomainDetails(String domainName, String domainCode) {
        this.domainCode = domainCode;
        this.domainName = domainName;

    }


    public void setParentCategory(String parentCategoryName) {
        this.parentCategory = parentCategoryName;
    }
}


