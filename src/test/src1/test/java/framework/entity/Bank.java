/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name:
 *  Date:
 *  Purpose:
 */
package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.AppConfig;

import java.io.IOException;


public class Bank implements Cloneable {
    public String ProviderName,
            ProviderId,
            BankName,
            BankID,
            PoolAccNum,
            PoolAccType,
            TrustType,
            CBSType,
            BranchFileCSV,
            defaultRoutingBankId,
            Currency;

    public boolean IsDefaultBank = false,
            IsTrust = false,
            IsLiquidation = false,
            IsRequiredInBaseSet = false;

    private Boolean isCreated = false,
            isExisting = false;

    public Boolean getCreated() {
        return isCreated;
    }

    public Boolean getExisting() {
        return isExisting;
    }

    public void setCreated(Boolean created) {
        isCreated = created;
    }

    public void setExisting(Boolean existing) {
        isExisting = existing;
    }

    /**
     * Used to read Input data from Excel
     *
     * @param providerName    - Provider Name - MFS1
     * @param providerId    - Provider Id - 101
     * @param bankName      - User defined Bank Name
     * @param bankId        - Bank Id, pass null to generate a random Bank ID
     * @param poolAccNum    - Pool Account num,  pass null to generate a random Pool ID
     * @param isTrust       - [boolean] true, if the bank type is trust
     * @param isLiquidation -[boolean] true, if bank tyo eis liquidation
     * @param isDefault     - [boolean] true, if the bank is default and to be set as a primary bank for user creation
     * @param isRequired    - [boolean] true, if this bank has to be included in common User creation
     *                      if both isTrust and isLiquidation are false, then the bank is Non Trust
     * @throws IOException
     */
    public Bank(String providerName,
                String providerId,
                String bankName,
                String bankId,
                String poolAccNum,
                boolean isTrust,
                boolean isLiquidation,
                boolean isDefault,
                boolean isRequired
    ) throws IOException {
        this.ProviderName = providerName;
        this.ProviderId = providerId;
        this.BankName = bankName;

        // set Bank ID
        if (bankId == null)
            this.BankID = DataFactory.getRandomNumber("12", AppConfig.bankIdLength);
        else
            this.BankID = bankId;

        // Set Pool Id
        if (poolAccNum == null)
            this.PoolAccNum = DataFactory.getRandomNumberAsString(9);
        else
            this.PoolAccNum = poolAccNum;

        this.IsTrust = isTrust;
        this.IsLiquidation = isLiquidation;
        this.IsDefaultBank = isDefault;
        this.IsRequiredInBaseSet = isRequired;

        setGeneralInfo();
    }

    /**
     * Create Bank Object
     *
     * @param providerId
     * @param bankName
     * @param bankId
     * @param poolAccNum
     * @param isTrust
     * @param isLiquidation
     * @throws IOException
     */
    public Bank(String providerId,
                String bankName,
                String bankId,
                String poolAccNum,
                boolean isTrust,
                boolean isLiquidation
    ) throws IOException {
        this.ProviderId = providerId;
        this.ProviderName = DataFactory.getProviderName(providerId);
        this.BankName = bankName;

        // set Bank Id
        if (bankId == null)
            this.BankID = DataFactory.getRandomNumber("12", AppConfig.bankIdLength);
        else
            this.BankID = bankId;

        // set Pool Id
        if (poolAccNum == null)
            this.PoolAccNum = DataFactory.getRandomNumberAsString(9);
        else
            this.PoolAccNum = poolAccNum;

        this.IsTrust = isTrust;
        this.IsLiquidation = isLiquidation;
        this.IsRequiredInBaseSet = false;

        if(DataFactory.getDefaultBankName(this.ProviderId).equalsIgnoreCase(this.BankName)){
            this.IsDefaultBank = true;
        }

        // set general Info
        setGeneralInfo();
    }

    private void setGeneralInfo() throws IOException {
        if (this.IsTrust) {
            this.TrustType = Constants.BANK_TYPE_TRUST;
        } else if (this.IsLiquidation) {
            this.TrustType = Constants.BANK_TYPE_LIQUIDATION;
        } else {
            this.TrustType = Constants.BANK_TYPE_NON_TRUST;
        }


        this.PoolAccType = Constants.SAVINGS_ACCOUNT;
        this.CBSType = Constants.BANK_CBS_SPARROW;

        if (AppConfig.isBankBranchCSVUploadRequired) {
            ExcelUtil.writeDataToAddBranchCsv();
            this.BranchFileCSV = FilePath.fileAddBranch;
        }
    }

    public void setProviderId(String id, String name) {
        this.ProviderName = name;
        this.ProviderId = id;
    }

    public void setDefaultRoutingBankId(String id) {
        this.defaultRoutingBankId = id;
    }

    public void setProviderName(String providerName) {
        this.ProviderName = providerName;
        this.ProviderId = DataFactory.getProviderId(providerName);
    }

    public void setBankID(String bankID) {
        this.BankID = bankID;
    }

    public void setPoolAccNum(String poolAccNum) {
        this.PoolAccNum = poolAccNum;
    }

    public void setPoolAccType(String poolAccType) {
        this.PoolAccType = poolAccType;
    }

    public void setTrustType(String trustType) {
        this.TrustType = trustType;
    }

    public void setCBSType(String CBSType) {
        this.CBSType = CBSType;
    }

    public String getBankName() {
        return this.BankName;
    }

    public void setBankName(String bankName) {
        this.BankName = bankName;
    }

    public void createBranchCSV() throws IOException {
        if (AppConfig.isBankBranchCSVUploadRequired) {
            ExcelUtil.writeDataToAddBranchCsv();
            this.BranchFileCSV = FilePath.fileAddBranch;
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


}
