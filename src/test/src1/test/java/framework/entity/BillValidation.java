package framework.entity;

/**
 * Created by rahul.rana on 9/21/2017.
 */
public class BillValidation {
    public String Label, Type, Min, Max, StartWith, Contains, ReferenceType;

    /**
     * Representation of Bill Validation
     */
    public BillValidation(String label, String type, String min, String max, String start, String contain, String refType) {
        this.Label = label;
        this.Type = type;
        this.Min = min;
        this.Max = max;
        this.StartWith = start;
        this.Contains = contain;
        this.ReferenceType = refType;
    }
}
