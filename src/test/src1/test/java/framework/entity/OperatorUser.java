package framework.entity;

import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class OperatorUser {
    //public int BankIndex = 1; // bank index was hardcoded initially
    public boolean isExisting = false;
    public String Email = "operator@mahindracomviva.com";
    public String Dept = "depart";
    public String Division = "division";
    public String UserPrefix = "PR_MR";
    public String LastName = "Automation";
    public String Geography = null;

    public String WebGroupRole, CreationPassword = ConfigInput.userCreationPassword,
            CategoryCode, IdNum, BankName, MSISDN, Password, ConfirmPass, Role,
            CategoryName, DomainName, DomainCode, shortName, address1, address2,
            SSN, designation, city, State, contactPerson, LoginId, ContactNum, FirstName, ExternalCode;

    public Boolean isAllowedIp = false;
    public Boolean isDateAndTime = false;

    public String from = "23:00";
    public String to = "23:58";
    public String allowedIP = "172.16.16.47";
    //Generics
    public String REMARK = "";  //store the remark
    public String ERROR = "";    // store the error occurred on the UI
    public String isCreated = "N";
    public String isApproved = "N";
    public String isPwdReset = "N";
    private String status;
    private String parentUserName = "OPT";
    public OperatorUser(String catCode, String firstName, String loginId, String msisdn, String password,
                        String bankName, String webGroupRole, String isPwdReset, String status, String parentUserName) {
        this.CategoryCode = catCode;
        this.FirstName = firstName;
        this.LoginId = loginId;
        this.MSISDN = msisdn;
        this.Password = password;
        this.BankName = bankName;
        this.WebGroupRole = webGroupRole;
        this.isPwdReset = isPwdReset;
        this.CategoryName = DataFactory.getCategoryName(this.CategoryCode);
        this.status = status;
        this.parentUserName = parentUserName;
    }

    /**
     * Constructor
     * Create User object with default Role
     *
     * @param catCode
     */
    public OperatorUser(String catCode) throws Exception {
        this.setGeneralInformation(catCode);
        this.setDomainDetails();
        this.WebGroupRole = DataFactory.getWebGroupRoleName(catCode);
    }
    /**
     * Constructor with Specific Role Name
     *
     * @param catCode
     * @param roleName
     */
    public OperatorUser(String catCode, String roleName) {
        this.setGeneralInformation(catCode);
        this.setDomainDetails();
        this.WebGroupRole = roleName;
    }
    /**
     * For Migrated User
     *
     * @param catCode
     * @param index
     */
    public OperatorUser(String catCode, String webRoleName, int index) throws SQLException {
        int resultSize = 0;
        this.WebGroupRole = webRoleName;
        ResultSet result = MobiquityGUIQueries.fetchOptUsers(catCode, this.WebGroupRole);

        if (result != null) {
            result.beforeFirst();
            result.last();
            resultSize = result.getRow();
        }

        if (index <= resultSize && result != null) {
            result.absolute(index); // switch to that Index

            this.CategoryCode = catCode;
            this.MSISDN = result.getString("MSISDN");
            this.LoginId = result.getString("LOGIN_ID");
            this.ContactNum = result.getString("CONTACT_NO");
            this.FirstName = result.getString("USER_NAME");
            this.ExternalCode = result.getString("EXTERNAL_CODE");
            this.LastName = result.getString("LAST_NAME");
            this.Password = ConfigInput.defaultPass;
            this.isExisting = true;
        } else {
            String msisdn = DataFactory.getMSISDNcounter();
            this.CategoryCode = catCode;
            this.LoginId = this.CategoryCode + msisdn;
            this.MSISDN = msisdn;
            this.ContactNum = msisdn;
            this.FirstName = catCode;
            this.ExternalCode = msisdn + "abcd";
            this.Password = ConfigInput.defaultPass;
            this.isExisting = false;
        }
        CategoryName = DataFactory.getCategoryName(CategoryCode);
        DomainName = DataFactory.getDomainName(CategoryCode);
        DomainCode = DataFactory.getDomainCode(CategoryCode);
    }

    public void setIsAllowedIpTrue() {
        isAllowedIp = true;
    }

    public void setIsDateAndTimeTrue() {
        isDateAndTime = true;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return from;
    }

    public String getAllowedIP() {
        return allowedIP;
    }

    public void setBankName(String bankName) {
        this.BankName = bankName;
    }

    private void setGeneralInformation(String catCode) {
        this.MSISDN = DataFactory.getAvailableMSISDN();
        this.CategoryCode = catCode;
        this.LoginId = DataFactory.getUniqueLoginID(this.CategoryCode, AppConfig.maxLoginIDLength);
        this.ContactNum = MSISDN;
        this.FirstName = catCode + MSISDN;
        this.ExternalCode = DataFactory.getRandomNumber(MSISDN, AppConfig.externalCodeLength);
        this.Password = ConfigInput.defaultPass;
        this.ConfirmPass = Password;
        this.shortName = FirstName;
        this.address1 = "address1";
        this.address2 = "address2";
        this.SSN = MSISDN;
        this.designation = catCode;
        this.city = this.State = "Delhi";
        this.contactPerson = MSISDN;
        this.BankName = GlobalData.defaultBankName;
    }

    private void setDomainDetails() {
        this.CategoryName = DataFactory.getCategoryName(CategoryCode);
        this.DomainName = DataFactory.getDomainName(CategoryCode);
        this.DomainCode = DataFactory.getDomainCode(CategoryCode);
    }

    private void setPrimaryDetails(String msisdn, String catCode) {
        this.UserPrefix = "PR_MR";
        this.CategoryCode = catCode;
        this.Email = "operator@mahindracomviva.com";
        this.MSISDN = msisdn;
        this.ContactNum = msisdn;
        this.LoginId = this.CategoryCode + msisdn;
        this.FirstName = catCode + msisdn;
        this.LastName = "Automation";
        this.ExternalCode = DataFactory.getRandomNumber(MSISDN, AppConfig.identificationNumLength);
        this.Password = ConfigInput.defaultPass;
        this.ConfirmPass = Password;
        this.Dept = "Automation";
        this.Division = "division";
    }

    public void setAllDetails(String msisdn, String catCode, String webGroupRole) throws IOException {
        setDomainDetails();
        setPrimaryDetails(msisdn, catCode);
        setWebGroupRole(webGroupRole);
    }


    /**
     * Set MSiSDN
     *
     * @param mSISDN
     */
    public void setMSISDN(String mSISDN) {
        this.MSISDN = mSISDN;
    }

    public void setFirstName(String firstName) {
        this.FirstName = firstName;
    }

    public void setLoginId(String loginId) {
        this.LoginId = loginId;
    }

    public void setWebGroupRole(String role) {
        this.WebGroupRole = role;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.ExternalCode = identificationNumber;
    }

    public void setContactNum(String contactNum) {
        this.ContactNum = contactNum;
    }

    public void setGeography(String geo) {
        this.Geography = geo;
    }

    public void setIsApprover() {
        this.isCreated = "Y";
        this.isApproved = "Y";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isBarred() {
        if (this.status.equals(Constants.BAR_AS_BOTH)) {
            return true;
        } else {
            return false;
        }
    }

    public void setIsPwdReset() {
        this.isPwdReset = "Y";
    }

    public void setError(String error) {
        this.ERROR = error;
        this.isCreated = "N";
        this.isApproved = "N";
        this.isPwdReset = "N";
    }

    public String getPartialName() {
        return FirstName + " " + FirstName;
    }

    public String getFirstNameAndLastName() {
        return FirstName + " " + LastName;
    }

    public void setRemark(String remark) {
        this.REMARK = remark;
    }

    public String getParentUserName() {
        return this.parentUserName;
    }

    public void setParentUserName(String parentUserName) {
        this.parentUserName = parentUserName;
    }

    /**
     * Write Data to Excel Output File
     */
    public void writeDataToExcel(boolean... isTempUser) throws IOException {

        String filePath;
        if (isTempUser.length > 0 && isTempUser[0]) {
            filePath = FilePath.fileTempOptUsers;
        } else {
            filePath = FilePath.fileOperatorUsers;
        }

        int lastRow = ExcelUtil.getExcelLastRow(filePath);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 0, this.CategoryCode);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 1, this.FirstName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 2, this.LastName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 3, this.MSISDN);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 4, this.BankName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 5, this.LoginId);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 6, this.Password);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 7, this.WebGroupRole);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 8, this.isCreated);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 9, this.isApproved);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 10, this.isPwdReset);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 11, this.ERROR);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 12, this.REMARK);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 13, this.status);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 14, this.parentUserName);
    }

    public void setIsCreated() {
        this.isCreated = "Y";
    }
}
