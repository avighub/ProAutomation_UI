package framework.entity;

/**
 * Created by rahul.rana on 9/21/2017.
 */
public class CustomerBill {
    public String ProviderId, CustomerMsisdn, BillerCode, BillAccNum, Remark = "";
    public boolean isAlreadyAssociated;

    public CustomerBill(Biller biller, String custMsisdn, String billAccNum, String... remark) {
        this.ProviderId = biller.ProviderId;
        this.CustomerMsisdn = custMsisdn;
        this.BillerCode = biller.BillerCode;
        this.BillAccNum = billAccNum;
        if (remark.length > 0) {
            this.Remark = remark[0];
        }
        this.isAlreadyAssociated = false;
    }

    public void setIsAssociated() {
        this.isAlreadyAssociated = true;
    }
}
