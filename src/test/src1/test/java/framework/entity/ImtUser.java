package framework.entity;

import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;

/**
 * Created by ravindra.dumpa on 11/22/2018.
 */
public class ImtUser {

    public String IMTIdType, IMTIdNo, PlaceOfIDIssued, IssuedCountryCode, ResidencyCountryCode, Nationality, IssuedDate, IsIDExpires, ExpireDate, PostalCode, EmployerName, Occupation, WUEnable, MoneyGramEnable, BirthCity, BirthCountry, PassportIssueCountryCode, PassportIssueCity, PassportIssueDate;

    public ImtUser() throws Exception {
        this.IMTIdType = "PAN_CARD";
        this.IMTIdNo = DataFactory.getRandomNumberAsString(9) + DataFactory.getRandomNumberAsString(5);
        this.PlaceOfIDIssued = "IN";
        this.IssuedCountryCode = "IN";
        this.Nationality = "IN";
        this.ResidencyCountryCode = "IN";
        this.IssuedDate = new DateAndTime().getDate(-3650);
        this.IsIDExpires = "True";
        this.ExpireDate = new DateAndTime().getDate(+3650);
        this.PostalCode = "560103";
        this.EmployerName = "Comviva";
        this.Occupation = "Tester";
        this.WUEnable = "";
        this.MoneyGramEnable = "";
        this.BirthCity = "Bangalore";
        this.BirthCountry = "IN";
        this.PassportIssueCountryCode = "IN";
        this.PassportIssueCity = "Bangalore";
        this.PassportIssueDate = new DateAndTime().getDate(+3650);
    }

}
