package framework.entity;

import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;

public class WalletPreference {
    public String CategoryName, DomainName, RegType, Provider, PaymentInstName, PaymentInst,
            GradeName, preferenceType, TCP, mobileRole, GradeCode;
    public boolean isPrimary = false;

    public WalletPreference(String categoryCode, String regType, String payInstName,
                            boolean isPrimary, String provide, String grade)
            throws Exception {
        this.CategoryName = DataFactory.getCategoryName(categoryCode);
        this.DomainName = DataFactory.getDomainName(categoryCode);
        this.RegType = regType;
        this.GradeName = (grade != null) ? grade : DataFactory.getGradesForCategory(categoryCode).get(0).GradeName;
        this.GradeCode = DataFactory.getGradeCode(this.GradeName);
        this.Provider = (provide != null) ? provide : DataFactory.getDefaultProvider().ProviderName;
        this.PaymentInst = Constants.PAYINST_WALLET_CONST;
        this.PaymentInstName = payInstName;
        this.isPrimary = isPrimary;
        this.preferenceType = Constants.PREFERENCE_TYPE_USER_REGISTRATION;
        this.TCP = DataFactory.getInstrumentTCP(this.DomainName, this.CategoryName, this.GradeName, this.Provider, this.PaymentInstName).ProfileName;
        this.mobileRole = DataFactory.getMobileGroupRole(this.DomainName, this.CategoryName, this.GradeName, this.Provider, this.PaymentInstName).ProfileName;
    }

    public void setPaymentInst(String paymentType) {
        this.PaymentInst = paymentType;
    }

    public void setPreferenceType(String prefType) throws Exception {
        this.preferenceType = prefType;
        this.TCP = DataFactory.getInstrumentTCP(this.DomainName, this.CategoryName, this.GradeName, this.Provider, this.PaymentInstName).ProfileName;
        this.mobileRole = DataFactory.getMobileGroupRole(this.DomainName, this.CategoryName, this.GradeName, this.Provider, this.PaymentInstName).ProfileName;

    }
}
