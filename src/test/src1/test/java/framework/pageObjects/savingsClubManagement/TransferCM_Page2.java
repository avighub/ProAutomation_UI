package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class TransferCM_Page2 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static TransferCM_Page2 page;
    /*
    Page Objects
     */
    @FindBy(name = "msisdn")
    private WebElement txtNewChairManMsisdn;
    @FindBy(name = "clubMemberMsisdn")
    private WebElement txtClubMemberMsisdn;
    @FindBy(name = "selectedTrfAdminReason")
    private WebElement selreasonForTransfer;
    @FindBy(name = "isRemoveAdmin")
    private WebElement chkIstobeRemoved;
    @FindBy(name = "action:clubRegisterationServiceBean_trfAdminModifySubmit")
    private WebElement btnSubmit;

    public static TransferCM_Page2 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return PageFactory.initElements(driver, TransferCM_Page2.class);
    }

    public TransferCM_Page2 setNewChairmanMsisdn(String text) {
        txtNewChairManMsisdn.sendKeys(text);
        pageInfo.info("Set the Net Chairman's Msisdn - " + text);
        return this;
    }

    public TransferCM_Page2 setClubMemberMsisdn(String text) {
        txtClubMemberMsisdn.sendKeys(text);
        pageInfo.info("Set the Net Chairman's Msisdn - " + text);
        return this;
    }

    public TransferCM_Page2 selectReasonForTransfer(String text) {
        Select sel = new Select(selreasonForTransfer);
        sel.selectByValue(text);
        pageInfo.info("Select the reason for CM resignation - " + text);
        return this;
    }

    public TransferCM_Page2 clickOnSubmit() {
        btnSubmit.click();
        pageInfo.info("Click on submit");
        return this;
    }

    public TransferCM_Page2 selectRemoveFromClub(boolean remove) {
        if (remove && !chkIstobeRemoved.isSelected()) {
            chkIstobeRemoved.click();
            pageInfo.info("select the chairman to be removed from the Club");
        } else if (!remove && chkIstobeRemoved.isSelected()) {
            chkIstobeRemoved.click();
            pageInfo.info("Unselect the chairman, not to be removed from the club");
        }
        return this;
    }

    public boolean getRemoveFromClubStatus() {
        if (chkIstobeRemoved.isSelected()) {
            return true;
        } else {
            return false;
        }
    }


}