package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeleteClub_Page1 extends PageInit {

    @FindBy(name = "clubId")
    WebElement clubId;
    @FindBy(id = "clubRegisterationServiceBean_delete_button_submit")
    WebElement btnSubmit;
    @FindBy(id = "clubRegisterationServiceBean_executeDeletion_button_confirm")
    WebElement btnConfirm;

    public DeleteClub_Page1(ExtentTest t1) {
        super(t1);
    }

    public static DeleteClub_Page1 init(ExtentTest t1) {
        return new DeleteClub_Page1(t1);
    }

    public DeleteClub_Page1 setClubId(String text) {
        setText(clubId, text, "Club ID");
        return this;
    }

    public DeleteClub_Page1 clickSubmit() throws Exception {
        clickOnElement(btnSubmit, "Submit");
        return this;
    }

    public DeleteClub_Page1 clickConfirm() throws Exception {
        clickOnElement(btnConfirm, "Confirm");
        return this;
    }

    public DeleteClub_Page1 navDeleteSavingClub() throws Exception {
        fl.leftNavigation("CLUBS_ALL", "CLUBS_DEL_CLUB");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Savings Club Management >  Initiate Club Delete");
        return this;
    }


}
