package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TransferCM_Page1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static TransferCM_Page1 page;
    /*
    Page Objects
     */
    @FindBy(name = "clubId")
    private WebElement txtClubId;
    @FindBy(id = "clubRegisterationServiceBean_trfAdmin_button_submit")
    private WebElement btnSubmit;

    public static TransferCM_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return PageFactory.initElements(driver, TransferCM_Page1.class);
    }

    public TransferCM_Page1 setClubId(String clubId) {
        txtClubId.sendKeys(clubId);
        pageInfo.info("Set the Club Id - " + clubId);
        return this;
    }

    public TransferCM_Page2 clickOnSubmit() {
        btnSubmit.click();
        pageInfo.info("Click on submit!");
        return TransferCM_Page2.init(pageInfo);
    }

    public TransferCM_Page1 navSavingClubCMTransferInitiate() throws Exception {
        fl.leftNavigation("CLUBS_ALL", "CLUBS_TRF_CLUB_ADM");
        pageInfo.info("Navigate to Savings Club Management >  Transfer Chairman Rights Initiate");
        return this;
    }


}