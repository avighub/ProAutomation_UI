package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.SavingsClub;
import framework.pageObjects.PageInit;
import framework.pageObjects.stockManagement.StockLiquidationApproval_page1;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by Dalia on 12-09-2017.
 */
public class AddClub_Page1 extends PageInit {


    @FindBy(name = "clubName")
    public WebElement clubName;
    @FindBy(id = "msisdn")
    private WebElement chairmanMsisdn;
    @FindBy(name = "location")
    private WebElement location;
    @FindBy(name = "selectedClubType")
    private WebElement clubType;
    @FindBy(name = "selectedGrade")
    private WebElement chairmanGrade;
    @FindBy(name = "selectedClubWalletTCP")
    private WebElement chairmanWalletTCP;
    @FindBy(name = "selectedWalletMGR")
    private WebElement chairmanWalletMobileGroupRole;
    @FindBy(name = "selectedBankTCP")
    private WebElement chairmanBankTCP;
    @FindBy(name = "selectedBankMGR")
    private WebElement chairmanBankMobileGroupRole;
    @FindBy(name = "minMembers")
    private WebElement minMembers;
    @FindBy(name = "maxMembers")
    private WebElement maxMembers;
    @FindBy(name = "minApprovers")
    private WebElement minApprovers;
    @FindBy(name = "maxApprovers")
    private WebElement maxApprovers;
    @FindBy(xpath = "//input[@type = 'submit' and @value = 'Submit']")
    private WebElement submit;
    @FindBy(id = "clubRegisterationServiceBean_validateClubModification_button_submit")
    private WebElement submit2;
    @FindBy(xpath = "clubRegisterationServiceBean_input']/table/tbody/tr[9]/td/input[2]")
    private WebElement reset;

    @FindBy(id = "fromDateStr")
    private WebElement txtFromDate;

    @FindBy(id = "toDateStr")
    private WebElement txtToDate;

    @FindBy(id = "submit")
    private WebElement viewReport;

    public AddClub_Page1(ExtentTest t1) {
        super(t1);
    }

    public static AddClub_Page1 init(ExtentTest t1) {
        return new AddClub_Page1(t1);
    }

    /**
     * Navigate to Savings Club Management >  Add Club page
     *
     * @throws Exception
     */
    public void navigateToAddClubPage() throws Exception {
        navigateTo("CLUBS_ALL", "CLUBS_ADD_CLUB", "Saving Club Management > Add Club ");
    }

    public String getMinMemberCount() {
        return minMembers.getAttribute("value");
    }

    public String getMaxMemberCount() {
        return maxMembers.getAttribute("value");
    }

    public String getMinApproverCount() {
        return minApprovers.getAttribute("value");
    }

    public String getMaxApproverCount() {
        return maxApprovers.getAttribute("value");
    }

    /**
     * Set Club Name *
     *
     * @param name
     */
    public void setClubName(String name) {
        setText(clubName, name, "Club Name");
    }

    public void setChairmanMsisdn(String msisdn) {
        setText(chairmanMsisdn, msisdn, "Chairman Msisdn");
    }

    public void setLocation(String loc) {
        setText(location, loc, "Location");
    }

    public AddClub_Page1 selectClubType(String text) {
        selectVisibleText(clubType, text, "Club Type");
        return this;
    }

    public void selectChairmanGrade(String text) {
        selectVisibleText(chairmanGrade, text, "Chairman Grade");
    }

    public void selectChairmanWalletTCP(String text) {
        selectVisibleText(chairmanWalletTCP, text, "Chairman Wallet TCP");
    }

    public void selectChairmanWalletMobileGroupRole(String text) {
        selectVisibleText(chairmanWalletMobileGroupRole, text, "Chairman Wallet Mobile Group role");
    }

    public void selectChairmanBankTCP(String text) {
        selectVisibleText(chairmanBankTCP, text, "Chairman Bank TCP");
    }

    public void selectChairmanBankMobileGroupRole(String text) {
        selectVisibleText(chairmanBankMobileGroupRole, text, "Chairman Bank Mobile Role");
    }

    public void setMinMembers(int minMem) {
        setText(minMembers, "" + minMem, "Minimum Member");
    }

    public void setMaxMembers(int maxMem) {
        setText(maxMembers, "" + maxMem, "Maximum Member");
    }

    public AddClub_Page1 setMinApprovers(int minMem) {
        setText(minApprovers, "" + minMem, "Minimum Approver");
        return this;
    }

    public void setMaxApprovers(int maxApp) {
        setText(maxApprovers, "" + maxApp, "Maximum Approver");
    }

    public void navigateToClubReportPage() throws Exception {
        navigateTo("CLUBS_ALL", "CLUBS_CLUB_VIEW_REPORT", "Saving Club Management > Club Report");
    }

    public AddClub_Page1 clickSubmit() {
        clickOnElement(submit, "Submit");
        return this;
    }

    public AddClub_Page1 clickSubmit2() {
        clickOnElement(submit2, "Submit Next");
        return this;
    }

    public void verifyClubModifyInitiatePage(SavingsClub sClub) {
        pageInfo.info("Verifying Saving club info @ Modify Page");

    }

    public void clickViewReport() {
        clickOnElement(viewReport, "View Report");
    }

    public void verifyClubId(String clubId1) throws Exception {
        int flag = 0;

        List<WebElement> clubId = driver.findElements(By.xpath("((.//*[@id='clubRegisterationServiceBean_saveToExcel']/table/tbody//td[2])[position()>1])"));
        for (WebElement i : clubId) {
            String club = i.getText();
            if (club.equalsIgnoreCase(clubId1)) {
                pageInfo.pass("Club Information is available");
                flag++;
                break;
            } else {
                continue;
            }
        }
        if (flag == 0) {
            pageInfo.fail("Club Information is not available");
        }
    }


    public void SelectFromandToDateForApproval(String fromdate, String todate) throws Exception {
        StockLiquidationApproval_page1.convertToApproveDateFormat(fromdate);
        StockLiquidationApproval_page1.convertToApproveDateFormat(todate);

        Utils.setValueUsingJs(txtFromDate, fromdate);
        pageInfo.info("Set From Date-" + fromdate);

        Utils.setValueUsingJs(txtToDate, todate);
        pageInfo.info("Set To Date-" + todate);
    }


}
