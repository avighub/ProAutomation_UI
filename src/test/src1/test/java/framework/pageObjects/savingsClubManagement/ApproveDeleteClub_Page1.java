package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ApproveDeleteClub_Page1 extends PageInit {

    @FindBy(id = "clubRegisterationServiceBean_deleteApproval_approve")
    WebElement btnApprove;

    @FindBy(id = "clubRegisterationServiceBean_deleteClubApproveOrReject_Confirm")
    WebElement btnConfirm;

    public ApproveDeleteClub_Page1(ExtentTest t1) {
        super(t1);
    }

    public static ApproveDeleteClub_Page1 init(ExtentTest t1) {
        return new ApproveDeleteClub_Page1(t1);
    }

    public ApproveDeleteClub_Page1 clickApprove() throws Exception {
        clickOnElement(btnApprove, "Submit");
        return this;
    }

    public ApproveDeleteClub_Page1 clickConfirm() throws Exception {
        clickOnElement(btnConfirm, "Confirm");
        return this;
    }

    public ApproveDeleteClub_Page1 navApproveDeleteClub() throws Exception {
        fl.leftNavigation("CLUBS_ALL", "CLUBS_DELAPP_CLUB");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Savings Club Management >  Approve Delete Club");
        return this;
    }

    public ApproveDeleteClub_Page1 selectClub(String clubId) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + clubId + "')]/ancestor::tr[1]/td/input[@type='checkbox']")).click();
        pageInfo.info("Click on Checkbox corresponding to  - " + clubId);
        return this;
    }


}
