package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Dalia on 12-09-2017.
 */
public class AddClub_Confirm_Page1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static AddClub_Confirm_Page1 page;
    /**
     * Add Page objects for valdiate confirm page  todo
     */

    @FindBy(xpath = "//input[@type = 'submit' and @value = 'Confirm']")
    private WebElement confirm;
    //@FindBy(xpath = "//input[@type = 'submit' and @value = 'Submit']")
    @FindBy(id = "clubRegisterationServiceBean_validateClubModification_button_submit")
    private WebElement submit2;

    public static AddClub_Confirm_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return PageFactory.initElements(driver, AddClub_Confirm_Page1.class);
    }

    public void clickConfirm() {
        confirm.click();
        pageInfo.info("Click on Confirm!");
    }

    public void clickSubmit2() {
        submit2.click();
        pageInfo.info("Click on Submit2!");
    }

}
