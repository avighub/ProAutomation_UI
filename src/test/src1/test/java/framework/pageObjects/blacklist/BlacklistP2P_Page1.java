package framework.pageObjects.blacklist;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class BlacklistP2P_Page1 extends PageInit {


    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/


    @FindBy(id = "recpntMsisdn")
    WebElement receiverMSISDNTbox;

    @FindBy(id = "providerUserListSel")
    WebElement receiverProviderDdown;

    @FindBy(id = "providerUserListSel1")
    WebElement senderProviderDdown;

    @FindBy(id = "blackMsisdn")
    WebElement senderMSISDNTbox;

    @FindBy(id = "p2pBlackListAction_P2PBlackListUser_remark")
    WebElement remarksTextBox;

    @FindBy(id = "p2pBlackListAction_P2PBlackListUser_button_blacklist")
    WebElement blacklistButton;

    @FindBy(id = "p2pBlackListAction_P2PBlackListUser_button_confirm")
    WebElement confirmButton;

    public BlacklistP2P_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_PBL", "Navigate to P2P BlackList");
    }

    public void setReceiverMSISDN(String msisdn) {
        setText(receiverMSISDNTbox, msisdn, "Receiver MSISDN");
    }

    public void selectRecProvider(String val) throws Exception {
        Thread.sleep(2000);
        selectVisibleText(receiverProviderDdown, val, "Receiver Provider");
    }

    public void setSenderMSISDN(String msisdn) {
        setText(senderMSISDNTbox, msisdn, "Sender MSISDN");
    }

    public void selectSenderProvider(String val) throws InterruptedException {
        Thread.sleep(2000);
        selectVisibleText(senderProviderDdown, val, "Sender Provider");
    }

    public void setRemarks(String remarks) {
        setText(remarksTextBox, remarks, "Remark");
    }

    public void clickBlacklistButton() {
        clickOnElement(blacklistButton, "Black List");
    }

    public void clickConfirm() {
        clickOnElement(confirmButton, "Confirm");
    }


}
