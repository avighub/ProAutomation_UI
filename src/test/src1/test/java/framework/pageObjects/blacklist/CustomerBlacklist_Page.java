package framework.pageObjects.blacklist;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class CustomerBlacklist_Page extends PageInit {

    /****************************************
     *       P A G E  -  O B J E C T S
     ****************************************/


    @FindBy(xpath = "//*[@id='addBlackList_add_firstName']")
    private WebElement firstNameTbox;
    @FindBy(xpath = "//*[@id='addBlackList_add_lastName']")
    private WebElement lastNameTbox;
    @FindBy(name = "dojo.dateOfBirth")
    private WebElement dateOfBirthTbox;
    @FindBy(id = "addBlackList_add_bulkUploadFile")
    private WebElement bulkUploadFile;
    @FindBy(id = "addBlackList_add_button_submit")
    private WebElement submitButton;
    @FindBy(id = "addBlackList_addSingleConfirm_button_confirm")
    private WebElement confirmButton;
    @FindBy(id = "addBlackList_addBulkConfirm_button_confirm")
    private WebElement confirmButtonBulk;
    @FindBy(xpath = "//img[@alt=\"Select a date\"]")
    private WebElement subscriberCheck;
    @FindBy(id = "addBlackList_add_reset")
    private WebElement resetButton;
    @FindBy(id = "addBlackList_addSingleConfirm_back")
    private WebElement backButton;
    @FindBy(id = "addBlackList_addSingleConfirm_firstName")
    private WebElement viewFname;
    @FindBy(id = "addBlackList_addSingleConfirm_lastName")
    private WebElement viewLname;
    @FindBy(id = "addBlackList_addSingleConfirm_dateOfBirth")
    private WebElement viewDOB;
    @FindBy(xpath = "//form[@id=\"addBlackList_addBulkConfirm\"]/table/tbody/tr[2]/td[@colspan=\"2\"][1]")
    private WebElement viewBulkFname;
    @FindBy(xpath = "//form[@id=\"addBlackList_addBulkConfirm\"]/table/tbody/tr[2]/td[@colspan=\"2\"][2]")
    private WebElement viewBulkLname;
    @FindBy(xpath = "//form[@id=\"addBlackList_addBulkConfirm\"]/table/tbody/tr[2]/td[@colspan=\"2\"][3]")
    private WebElement viewBulkDob;
    @FindBy(xpath = "//form[@id=\"addBlackList_addBulkConfirm\"]/table/tbody/tr[2]/td[@colspan=\"1\"][4]")
    private WebElement viewBulkMsisdn;
    @FindBy(xpath = "//form[@id=\"addBlackList_addBulkConfirm\"]/table/tbody/tr[2]/td[@colspan=\"4\"]")
    private WebElement viewStatus;

    public CustomerBlacklist_Page(ExtentTest t1) {
        super(t1);
    }

    /**
     * Method to Navigate to Customer blacklist page
     *
     * @throws NoSuchElementException
     */
    public void navigateToLink() throws Exception {
        fl.leftNavigation("SUBSCRIBER_ALL", "SUBSCRIBER_BLK_ABL");
        pageInfo.info("Navigate to Customer BlackList");
    }

    /**
     * Method to Enter First Name
     *
     * @param fName
     */
    public void setFirstNameTbox(String fName) {
        setText(firstNameTbox, fName, "Enter First Name: " + fName);
    }

    /**
     * Method to enter Last Name
     *
     * @param lName
     */
    public void setLastNameTbox(String lName) {
        setText(lastNameTbox, lName, "Enter Last Name: " + lName);
    }

    /**
     * method to set date of birth
     *
     * @param dob
     */
    public void setDateOfBirth(String dob) {
        setText(dateOfBirthTbox, dob, "Enter DOB : " + dob);
    }

    /**
     * method to upload File
     *
     * @param file
     */
    public void uploadFile(String file) {
        bulkUploadFile.sendKeys(file);
        pageInfo.info("Uploading file : " + file);
    }

    /**
     * Method to click on Submit button
     */
    public void clickSubmitButton() {
        clickOnElement(submitButton, "Clicked on Submit button");
    }

    /**
     * method to click on Confirm button
     */
    public void clickConfirm() {
        clickOnElement(confirmButton, "Clicked on Confirm button");
    }

    /**
     * Method to click Bulk Confirm button
     */
    public void clickBulkConfirm() {
        clickOnElement(confirmButtonBulk, "Clicked on Bulk Confirm button");
    }

    public boolean checkSubscriberManagement() {
        if (subscriberCheck.isDisplayed()) {
            pageInfo.info("Back to the Select Geographical Domain page");
            return true;
        }
        return false;
    }

    public void clickReset() {
        clickOnElement(resetButton, "Click on reset Button");
    }

    public boolean resetCheck() throws InterruptedException {
        String firstname = firstNameTbox.getText();
        Thread.sleep(1000);
        String lastname = lastNameTbox.getText();
        Thread.sleep(1000);
        String dob = dateOfBirthTbox.getText();
        if (firstname.equalsIgnoreCase("")) {
            pageInfo.info("First Name reset");
            if (lastname.equalsIgnoreCase("")) {
                pageInfo.info("Last Name reset");
                if (dob.equalsIgnoreCase("")) {
                    pageInfo.info("Date of Birth Reset");
                    return true;
                }
            }
        }
        return false;
    }

    public void clickBack() {
        clickOnElement(backButton, "Click on Back Button");
    }

    public String getFirstName() {
        String fname = viewFname.getText();
        pageInfo.info("The First Name is " + fname);
        return fname;
    }

    public String getLastName() {
        String lName = viewLname.getText();
        pageInfo.info("The Last Name is " + lName);
        return lName;
    }

    public String getDOB() {
        String dob = viewDOB.getText();
        pageInfo.info("The Date Of Birth is " + dob);
        return dob;
    }

    public String getStatus() {
        String status = viewStatus.getText();
        pageInfo.info("Status is " + status);
        return status;
    }

    public String getBulkFname() {
        String var = viewBulkFname.getText();
        pageInfo.info("First Name is " + var);
        return var;
    }

    public String getBulkLname() {
        String var = viewBulkLname.getText();
        pageInfo.info("Last Name is " + var);
        return var;
    }

    public String getBulkDob() {
        String var = viewBulkDob.getText();
        pageInfo.info("Date Of Birth is " + var);
        return var;
    }

    public String getBulkMsisdn() {
        String var = viewBulkMsisdn.getText();
        pageInfo.info("Msisdn is " + var);
        return var;
    }

}
