package framework.pageObjects.blacklist;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class UnBlacklistP2P_Page1 extends PageInit {

    @FindBy(id = "recpntMsisdn")
    WebElement receiverMSISDNTbox;
    @FindBy(id = "providerUserListSel")
    WebElement providerDdown;
    @FindBy(id = "p2pUnBlackListAction_P2PBlackListUser_button_unblacklist")
    WebElement unblacklist;
    @FindBy(xpath = "//*[@id='p2pUnBlackListAction_delete']//a")
    WebElement deleteLink;

    public UnBlacklistP2P_Page1(ExtentTest t1) {
        super(t1);
    }

    public static UnBlacklistP2P_Page1 init(ExtentTest t1) {
        return new UnBlacklistP2P_Page1(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_PUL", "P2P UnblackList");
    }

    public void setReceiverMSISDN(String msisdn) {
        setText(receiverMSISDNTbox, msisdn, "Receiver MSISDN");
    }

    public void selectProvider(String val) throws InterruptedException {
        selectVisibleText(providerDdown, val, "Provider");
    }

    public void clickUnblacklistP2P() {
        clickOnElement(unblacklist, "Un BlackList");
    }

    public void clickDeleteLink(String msisdn) {
        WebElement elem = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//tr/td[contains(text(), '" + msisdn + "')]/ancestor::tr[1]/td/a"))));
        clickOnElement(elem, "Un blackList: "+ msisdn);
    }

}
