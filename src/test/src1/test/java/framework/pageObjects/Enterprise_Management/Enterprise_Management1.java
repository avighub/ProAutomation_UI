package framework.pageObjects.Enterprise_Management;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class Enterprise_Management1 {

    private static FunctionLibrary fl;
    private static WebDriver driver;
    private static ExtentTest pageInfo;
    private static Enterprise_Management1 page;
    @FindBy(id = "addEmployee_loadEmployeeDetails_msisdn")
    private WebElement MSISDN;
    @FindBy(id = "addEmployee_loadEmployeeDetails_button_submit")
    private WebElement Submit;
    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_lastName")
    private WebElement lastnamelabel;
    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_msisdnUser")
    private WebElement Msisdnlabel;
    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_msisdnWeb")
    private WebElement Loginidlabel;
    @FindBy(id = "modifyEmployee_loadModifyEmployeeDetails_msisdn")
    private WebElement modiMSISDN;
    @FindBy(id = "modifyEmployee_loadModifyEmployeeDetails_button_submit")
    private WebElement modiSubmit;
    @FindBy(id = "deleteEmployee_loadDeleteEmployeeDetails_msisdn")
    private WebElement deleMSISDN;
    @FindBy(id = "deleteEmployee_loadDeleteEmployeeDetails_button_submit")
    private WebElement deleSubmit;

    public static Enterprise_Management1 init(ExtentTest t1) {
        pageInfo = t1;
        if (page == null) {
            driver = DriverFactory.getDriver();
            page = PageFactory.initElements(driver, Enterprise_Management1.class);
            fl = new FunctionLibrary(driver);
        }
        return page;
    }

    public Enterprise_Management1 EnterMsisdn(String value) {
        MSISDN.sendKeys(value);
        pageInfo.info("Enter Mmobile Number" + value);
        return this;
    }


    public void ClickOnSubmit() {
        Submit.click();
        pageInfo.info("Click on Submit button");
    }


    public void ClickOnmodifySubmit() {
        modiSubmit.click();
        pageInfo.info("Click on Submit button");
    }

    public Enterprise_Management1 EntermodifyMsisdn(String value) {
        modiMSISDN.sendKeys(value);
        pageInfo.info("Enter Mmobile Number" + value);
        return this;
    }


    public void ClickOnDeleteSubmit() {
        deleSubmit.click();
        pageInfo.info("Click on Submit button");
    }

    public Enterprise_Management1 EnterDeleteMsisdn(String value) {
        deleMSISDN.sendKeys(value);
        pageInfo.info("Enter Mmobile Number" + value);
        return this;
    }

    public Enterprise_Management1 NavigateToLinkADD() throws Exception {
        fl.leftNavigation("PAYROLL_ALL", "PAYROLL_ADD_EMP");
        pageInfo.info("Navigate to link Add Bulk Payee");
        return this;
    }

    public Enterprise_Management1 NavigateToLinkModify() throws Exception {
        fl.leftNavigation("PAYROLL_ALL", "PAYROLL_MOD_EMP");
        pageInfo.info("Navigate to link Modify Bulk Payee");
        return this;
    }

    public Enterprise_Management1 NavigateToLinkDelete() throws Exception {
        fl.leftNavigation("PAYROLL_ALL", "PAYROLL_DEL_EMP");
        pageInfo.info("Navigate to link Delete Bulk Payee");
        return this;
    }

    public Enterprise_Management1 NavigateToLinkViewSelfdetails() throws Exception {
        fl.leftNavigation("BPAMGT_ALL", "BPAMGT_EPTY_VSELF");
        pageInfo.info("Navigate to link View self Details");
        return this;
    }

    public String getMsisdnlabel() {
        return Msisdnlabel.getText();
    }

    public String getLoginidlabel() {
        return Loginidlabel.getText();
    }

    public String getlastnamelabellabel() {
        return lastnamelabel.getText();
    }


}
