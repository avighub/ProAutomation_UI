package framework.pageObjects.Enterprise_Management;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EntBulkPayDashBoard_Pg1 extends PageInit {

    @FindBy(xpath = "//div[@id='test1']//span[@class='active-color1 ul']")
    WebElement Newest;
    @FindBy(xpath = "//div[@id='test1']//a[@class='sort-link']")
    WebElement Oldest;
    @FindBy(xpath = "//div//select[@class='ng-pristine ng-valid ng-touched']")
    WebElement serviceType;
    @FindBy(xpath = "//span[@class='ul cta_color']")
    WebElement FileDownload;
    @FindBy(xpath = "(//span[@class='lh collapse_btnPD1'])[1]")
    WebElement entry;
    @FindBy(xpath = "//span[.='Success']/following-sibling::span")
    WebElement successCount;
    @FindBy(xpath = "//span[.='Failed']/following-sibling::span")
    WebElement failCount;
    @FindBy(xpath = "(//span[contains(text(),'Number of entries')]/span)[1]")
    WebElement noOfEntries;
    @FindBy(xpath = "//a[text()='Download status file']")
    WebElement downloadstatusFile;
    @FindBy(xpath = "//div[@id='test1']/span[2]")
    private WebElement newest;
    @FindBy(xpath = "//div[@id='test1']/a")
    private WebElement oldest;
    @FindBy(xpath = "//select[@class = 'ng-untouched ng-pristine ng-valid']")
    private WebElement dropDown;


    public EntBulkPayDashBoard_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static EntBulkPayDashBoard_Pg1 init(ExtentTest t1) {
        return new EntBulkPayDashBoard_Pg1(t1);
    }

    public EntBulkPayDashBoard_Pg1 navigateToLink() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_BULKPAY_DASHBOARD", "Ent Bulk Pay Dashboard");
        return this;
    }

    public void selectServiceType(String serviceValue) {
        WebElement elem = driver.findElements(By.xpath("//select")).get(0);
        selectValue(elem, serviceValue, "Service Type");
    }

    public void selectStatusFilter(Boolean isApproval) {
        WebElement elem = driver.findElements(By.xpath("//select")).get(0);
        if (isApproval) {
            selectValue(elem, "SUCCEEDED", "Approval Status");
        } else {
            selectValue(elem, "FAILED", "Approval Status");
        }
    }

    public EntBulkPayDashBoard_Pg1 sortNewest() {
        clickOnElement(Newest, "Link Newest");
        return this;
    }

    public EntBulkPayDashBoard_Pg1 ClickOnOldest() {
        clickOnElement(Oldest, "Link Oldert");
        return this;
    }

    public EntBulkPayDashBoard_Pg1 SelectApprovedOrFailed(int ServiceType) throws Exception {
        pageInfo.info("Selecting Failed Or Passed");
        Select select = new Select(serviceType);
        select.selectByIndex(ServiceType);
        return this;
    }

    public EntBulkPayDashBoard_Pg1 navigateToPayMentDashboard() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_BULKPAY_DASHBOARD", "Enterprise Payment Dashboard");
        return this;
    }

    public void clickNewest() {
        clickOnElement(newest, "Newest");
    }

    public void clickOldest() {
        clickOnElement(oldest, "Oldest");
    }

    public EntBulkPayDashBoard_Pg1 selectStatus(String value) {
        selectValue(dropDown, value, "Status");
        return this;
    }

    public String selectEntryAndFetchMessage(int index) {
        List<WebElement> list = driver.findElements(By.xpath("//div[@class = 'div-table-padding pd_zero']/ul/li/div/div/div[1]/span"));
        list.get(index).click();

        String message = driver.findElement(By.xpath("(//span[contains(@class,'primary-color')])[2]")).getText();
        return message;
    }

    public String downloadStatusFile() throws Exception {
        try {
            pageInfo.info("Deleting existing file with prefix - bulk-upload");
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload-"); // is hardcoded can be Generic TODO
            Thread.sleep(5000);
            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            clickOnElement(downloadstatusFile, "DownloadStatusFile");
            Utils.ieSaveDownloadUsingSikuli();
            Thread.sleep(5000);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

            if (Utils.isFileDownloaded(oldFile, newFile)) {
                return FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
            }

        } catch (Exception e) {
            Assertion.markAsFailure("Failed to download the status file");
        }
        return null;
    }


    public ArrayList<String> checkLogFile() throws Exception {

        pageInfo.info("Deleting existing file with prefix - bulk-upload");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload-"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        downloadstatusFile.click();
        pageInfo.info("Click on the download Failed Transaction link");
        Thread.sleep(5000);

        //String errorFile = FilePath.dirFileDownloads;
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000);
        String errorFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
        System.out.println(errorFile);


        BufferedReader br = null;
        ArrayList<String> errors = new ArrayList<String>();
        try {
            //Reading the csv file
            br = new BufferedReader(new FileReader(errorFile));

            String line = "";

            //Read to skip the header
            br.readLine();

            //Reading from the second line
            while ((line = br.readLine()) != null) {
                String[] details = line.split(",");

                if (details.length > 0) {
                    if (details[7].trim().equalsIgnoreCase("SUCCEEDED")) {
                        pageInfo.info(details[1] + " , " + details[7]);
                    } else {
                        pageInfo.info(details[1] + " , " + details[7] + " , " + details[9]);
                        errors.add(details[9].trim().replace("\"", " ").trim());
                    }
                }
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException ie) {
                System.out.println("Error occured while closing the BufferedReader");
                ie.printStackTrace();
            }
        }
        return errors;
    }

}


