package framework.pageObjects.Enterprise_Management;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EntBulkPayApproveReject_pg1 extends PageInit {


    @FindBy(xpath = "(//div[@class='col s6 alert alert-success padd_tb']//span)[2]")
    WebElement message;
    @FindBy(xpath = "//span[.='Newest']")
    WebElement sortNewest;

    @FindBy(id = "enterprise.remarks")
    WebElement batchRemark;

    public String getBatchRemark(){
        return batchRemark.getAttribute("value").trim();
    }

    public void setBatchRemark(String remark){
        setText(batchRemark, remark, "batch remark");
    }

    public String getBatchRemarkApprovalOne(){
        return batchRemark.getAttribute("value").trim();
    }

    public void setBatchRemarkApprovalOne(String remark){
        setText(batchRemark, remark, "batch remark");
    }

    public boolean isBatchRemarkFieldIsEnabled(){
        return batchRemark.isEnabled();
    }

    public boolean isBatchRemarkFieldAppOneIsEnabled(){
        return batchRemark.isEnabled();
    }


   /* @FindBy( = "approve_reject_btns")
    private WebElement MSISDN;*/
    @FindBy(xpath = "//a[.='APPROVE']")
    WebElement approveBtn;
    @FindBy(xpath = "//a[.='REJECT']")
    WebElement rejectBtn;

    public EntBulkPayApproveReject_pg1(ExtentTest t1) {
        super(t1);
    }

    public static EntBulkPayApproveReject_pg1 init(ExtentTest t1) {
        return new EntBulkPayApproveReject_pg1(t1);
    }

    public EntBulkPayApproveReject_pg1 NavigateToApp1Link() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_SAL_AP1", "Approve entBulk Pay L1");
        return this;
    }

    public void clickSortNewest() {
        clickOnElement(sortNewest, "sortNewest");
    }

    public void clickApprove() {
        clickOnElement(approveBtn, "approveBtn");
    }

    public void clickReject() {
        clickOnElement(rejectBtn, "rejectBtn");
    }

    public EntBulkPayApproveReject_pg1 NavigateToApp2Link() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_SAL_AP2", "Approve entBulk Pay L2");
        return this;
    }

    public String getActionMessage() {
        try {
            Thread.sleep(2500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fl.elementIsDisplayed(message)) {
            return message.getText();
        } else {
            return null;
        }
    }

    public String verfiy_allertmessage() {
        fl.waitWebElementVisible(message);
        String msg = message.getText();
        pageInfo.info("Get message");
        return msg;
    }


}
