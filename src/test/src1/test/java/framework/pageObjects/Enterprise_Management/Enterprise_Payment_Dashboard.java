package framework.pageObjects.Enterprise_Management;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Enterprise_Payment_Dashboard extends PageInit {

    @FindBy(xpath = "//div[@id='test1']//span[@class='active-color1 ul']")
    WebElement Newest;
    @FindBy(xpath = "//div[@id='test1']//a[@class='sort-link']")
    WebElement Oldest;
    @FindBy(xpath = "//div//select[@class='ng-pristine ng-valid ng-touched']") //ng-pristine ng-valid ng-touched")
            WebElement serviceType;
    @FindBy(xpath = "//span[@class='ul cta_color']")
    WebElement FileDownload;
    @FindBy(xpath = "(//span[@class='lh collapse_btnPD1'])[1]")
    WebElement entry;
    @FindBy(xpath = "//span[.='Success']/following-sibling::span")
    WebElement successCount;
    @FindBy(xpath = "//span[.='Failed']/following-sibling::span")
    WebElement failCount;
    @FindBy(xpath = "(//span[contains(text(),'Number of entries')]/span)[1]")
    WebElement noOfEntries;
    @FindBy(xpath = "//a[text()='Download status file']")
    WebElement downloadstatusFile;
    @FindBy(xpath = "//div[@id='test1']/span[2]")
    private WebElement newest;
    @FindBy(xpath = "//div[@id='test1']/a")
    private WebElement oldest;
    @FindBy(xpath = "//select[@class = 'ng-untouched ng-pristine ng-valid']")
    private WebElement dropDown;


    public Enterprise_Payment_Dashboard(ExtentTest t1) {
        super(t1);
    }

    public static Enterprise_Payment_Dashboard init(ExtentTest t1) {
        return new Enterprise_Payment_Dashboard(t1);
    }

    public Enterprise_Payment_Dashboard NavigateToLink() throws Exception {
        fl.leftNavigation("PAYROLL_ALL", "PAYROLL_BULKPAY_DASHBOARD");
        pageInfo.info("Navigate to link Enterprise Payment Dashboard");
        return this;
    }

    public Enterprise_Payment_Dashboard ClickOnNewest() {
        Newest.click();
        pageInfo.info("Click on Newest");
        return this;
    }

    public Enterprise_Payment_Dashboard ClickOnOldest() {
        Oldest.click();
        pageInfo.info("Click on Oldest");
        return this;
    }

    public Enterprise_Payment_Dashboard SelectApprovedOrFailed(int ServiceType) throws Exception {
        pageInfo.info("Selecting Failed Or Passed");
        Select select = new Select(serviceType);
        select.selectByIndex(ServiceType);
        return this;
    }

    public Enterprise_Payment_Dashboard navigateToPayMentDashboard() throws Exception {
        fl.leftNavigation("PAYROLL_ALL", "PAYROLL_BULKPAY_DASHBOARD");
        pageInfo.info("Navigate to Enterprise Payment Dashboard");
        return this;
    }

    public void clickNewest() {
        newest.click();
        pageInfo.info("click on newest");
    }

    public void clickOldest() {
        oldest.click();
        pageInfo.info("click on oldest");
    }

    public Enterprise_Payment_Dashboard selectStatus(String value) {
        Select select = new Select(dropDown);
        select.selectByValue(value.toUpperCase());

        pageInfo.info("select" + value);
        return this;
    }

    public String selectEntryAndFetchMessage(int index) {
        List<WebElement> list = driver.findElements(By.xpath("//div[@class = 'div-table-padding pd_zero']/ul/li/div/div/div[1]/span"));
        list.get(index).click();

        String message = driver.findElement(By.xpath("(//span[contains(@class,'primary-color')])[2]")).getText();
        return message;
    }

    public Enterprise_Payment_Dashboard ClickOnFileDownload() throws InterruptedException {
        pageInfo.info("Deleting existing file with prefix - bulk-upload-");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload-"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        FileDownload.click();
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Click On button Download");
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000);
        return this;

    }


    public void clickOnEntry() {
        clickOnElement(entry, "Latest Entry");
    }

    public String fetchSuccessCount() {
        return wait.until(ExpectedConditions.elementToBeClickable(successCount)).getText();
    }

    public String fetchFailedCount() {
        return wait.until(ExpectedConditions.elementToBeClickable(failCount)).getText();
    }

    public String getNoOfEntries() {
        String entries = noOfEntries.getText();
        return entries;
    }

    public ArrayList<String> checkLogFile() throws InterruptedException, IOException {

        pageInfo.info("Deleting existing file with prefix - bulk-upload");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload-"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        downloadstatusFile.click();
        pageInfo.info("Click on the download Failed Transaction link");
        Thread.sleep(5000);

        //String errorFile = FilePath.dirFileDownloads;
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000);
        String errorFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
        System.out.println(errorFile);


        BufferedReader br = null;
        ArrayList<String> errors = new ArrayList<String>();
        try {
            //Reading the csv file
            br = new BufferedReader(new FileReader(errorFile));

            String line = "";

            //Read to skip the header
            br.readLine();

            //Reading from the second line
            while ((line = br.readLine()) != null) {
                String[] details = line.split(",");

                if (details.length > 0) {
                    if (details[7].trim().equalsIgnoreCase("SUCCEEDED")) {
                        pageInfo.info(details[1] + " , " + details[7]);
                    } else {
                        pageInfo.info(details[1] + " , " + details[7] + " , " + details[9]);
                        errors.add(details[9].trim().replace("\"", " ").trim());
                    }
                }
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException ie) {
                System.out.println("Error occured while closing the BufferedReader");
                ie.printStackTrace();
            }
        }
        return errors;
    }

}


