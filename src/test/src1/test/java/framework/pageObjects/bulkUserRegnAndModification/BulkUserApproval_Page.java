package framework.pageObjects.bulkUserRegnAndModification;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by navin.pramanik on 25-Oct-17.
 */
public class BulkUserApproval_Page extends PageInit {

    @FindBy(xpath = "//a[contains(@href,'javaScript:openLogFile()')]")
    WebElement imgLogDownload;

    public BulkUserApproval_Page(ExtentTest t1) {
        super(t1);
    }

    public void navigateToBulkUsrApproval() throws Exception {
        fl.leftNavigation("BULK_ALL", "BULK_BULK_AP");
        pageInfo.info("Navigate to Bulk User Approval Page");
    }

    public void clickApprovalLink(String batchID) {
        WebElement link = DriverFactory.getDriver().findElement(By.xpath("//td[contains(text(),'" + batchID + "')]/ancestor::tr[1]/td/a[contains(@href,'requestType=approve')]"));
        link.click();
        pageInfo.info("Clicked on Approval Link having Batch ID : " + batchID);
    }

    public void alertHandle(boolean accept) {
        if (accept) {
            DriverFactory.getDriver().switchTo().alert().accept();
            pageInfo.info("Accepting the Alert !!");
        } else {
            DriverFactory.getDriver().switchTo().alert().dismiss();
            pageInfo.info("Rejecting the Alert !!");
        }
    }

    public void clickRejectLink(String batchID) {
        WebElement link = DriverFactory.getDriver().findElement(By.xpath("//td[contains(text(),'" + batchID + "')]/ancestor::tr[1]/td/a[contains(@href,'requestType=reject')]"));
        link.click();
        pageInfo.info("Clicked on reject Link having Batch ID : " + batchID);
    }


    public void downloadErrorLogs() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_USER_REGISTER);
        try {
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_USER_REGISTER);
            imgLogDownload.click();
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Click On image Download template");
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Thread.sleep(2000);
        } finally {
            Utils.closeUntitledWindows();
        }

    }
}
