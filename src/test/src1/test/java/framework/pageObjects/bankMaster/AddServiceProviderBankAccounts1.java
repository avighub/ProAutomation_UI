/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: Automation Team
 *  Date: 26-May-2017
 *  Purpose: Page Object
 */
package framework.pageObjects.bankMaster;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class AddServiceProviderBankAccounts1 extends PageInit {

    @FindBy(id = "_submit")
    public WebElement Confirm;
    //Page Object
    @FindBy(id = "optBankAccAdd_initConfirmOptBankAccount_bankId")
    private WebElement bankNameDdown;

    @FindBy(id = "optBankAccAdd_initConfirmOptBankAccount_optBankAccounts_0__bankAccNo")
    private WebElement accountNumberTbox;

    @FindBy(name = "optBankAccounts[1].bankAccNo")
    private WebElement accountNumberTbox1;

    @FindBy(id = "optBankAccAdd_initConfirmOptBankAccount_submit")
    private WebElement bankSubmitButton;

    @FindBy(name = "submit")
    private WebElement submitButton;

    @FindBy(name = "back")
    private WebElement backButton;

    @FindBy(id = "_submit")
    private WebElement confirmButton;

    @FindBy(name = "action:optBankAccAdd_addOneBankAccount")
    private WebElement addMoreBankAcButton;

    @FindBy(name = "button.remove")
    private WebElement removeBankAcButton;

    @FindBy(name = "optBankAccounts[1].bankAccNo")
    private WebElement bankAccountNo2Tbox;

    @FindBy(name = "optBankAccounts[1].bankAccType")
    private WebElement bankAccounType2Ddown;


    @FindBy(xpath = "//input[@value = 'Add More']")
    private WebElement addMore;
    @FindBy(xpath = "//table[@class = 'wwFormTableC']/tbody/tr[3]/td[1]/label")
    private WebElement newBankInputFeild;

    public AddServiceProviderBankAccounts1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate to Add Bank
     *
     * @throws NoSuchElementException
     */
    public void navAddBank() throws Exception {
        navigateTo("BANK_ALL", "BANK_OPT_BANK_ACC_ADDDM", "Add Bank Account");
    }


    //Enter Bank Name
    public void setBankName(String bankName) throws Exception {
        selectVisibleText(bankNameDdown, bankName.toUpperCase(), "bankNameDdown");
    }

    //Enter Pool Account Number
    public void setPoolAccountNumber(String accNum) throws Exception {
        setText(accountNumberTbox, accNum, "accountNumberTbox");
    }

    public void setPoolAccountNumber1(String accNum) throws Exception {
        setText(accountNumberTbox1, accNum, "accountNumberTbox1");
    }

    /**
     * clickOnBankSubmit
     */
    public void clickOnBankSubmit() {
        clickOnElement(bankSubmitButton, "bankSubmitButton");
    }


    /**
     * clickOnBankSubmit2
     */
    public void clickOnBankSubmit2() {
        clickOnElement(submitButton, "submitButton");
    }


    /**
     * clickConfirmComplete
     */
    public void clickOnConfirm() {
        clickOnElement(confirmButton, "confirmButton");
    }

    public void clickOnAddMore() {
        clickOnElement(addMore, "addMore");
    }

    public boolean isFieldToEnterNewBankDetails() {
        return fl.elementIsDisplayed(newBankInputFeild);
    }

    /**
     * clickOnBack
     */
    public void clickOnBack() {
        clickOnElement(backButton, "backButton");
    }

    /**
     * clickOnAddMoreBankAcButton
     */
    public void clickOnAddMoreBankAcButton() {
        clickOnElement(addMoreBankAcButton, "addMoreBankAcButton");
    }


    /**
     * clickOnRemoveBankAcButton
     */
    public void clickOnRemoveBankAcButton() {
        Utils.putThreadSleep(2000);
        clickOnElement(removeBankAcButton, "removeBankAcButton");

    }

    /**
     * isSubmitButtonVisible
     *
     * @return true or false
     */
    public boolean isSubmitButtonVisible() {
        return fl.elementIsDisplayed(submitButton);
    }

    /**
     * isBankAccountNo2TboxVisible
     *
     * @return true or false
     */
    public boolean isBankAccountNo2TboxVisible() {
        return fl.elementIsDisplayed(bankAccountNo2Tbox);
    }


}
