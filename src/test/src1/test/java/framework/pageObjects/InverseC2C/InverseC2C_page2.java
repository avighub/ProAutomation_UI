package framework.pageObjects.InverseC2C;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InverseC2C_page2 {

    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "inverseC2cTrf_confirm_amount")
    WebElement amount;
    @FindBy(id = "inverseC2cTrf_confirm_paymentId")
    WebElement paymentId;
    @FindBy(id = "inverseC2cTrf_confirm_button_submit")
    WebElement submitbtn;
    @FindBy(id = "inverseC2cTrf_confirm_c2c")
    WebElement confirmbtn;

    public static InverseC2C_page2 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        return PageFactory.initElements(driver, InverseC2C_page2.class);
    }

    public void enterAmount(String amt) {
        amount.sendKeys(amt);
        pageInfo.info("Entered amount as: " + amt);
    }

    public void enterPaymentId(String val) {
        if (paymentId.isDisplayed()) {
            paymentId.sendKeys(val);
            pageInfo.info("PaymentID is displayed");
            pageInfo.info("Enter paymentID as: " + val);
        } else {
            pageInfo.info("PaymentID is not displayed");
        }
    }

    public void clickSubmit() {
        submitbtn.click();
        pageInfo.info("Clicked on submit button");
    }

    public void clickConfirm() {
        confirmbtn.click();
        pageInfo.info("Clicked on confirm button");
    }
}
