package framework.pageObjects.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class EnterpriseManagementDeletePage extends PageInit {

    private static FunctionLibrary fl;
    private static WebDriver driver;
    private static ExtentTest pageInfo;
    private static EnterpriseManagementDeletePage page;

    @FindBy(name = "button.delete")
    private WebElement Submit;

    @FindBy(id = "addEmployee_add_button_confirm")
    private WebElement Confirm;

    public EnterpriseManagementDeletePage(ExtentTest test) {
        super(test);
    }

    public static EnterpriseManagementDeletePage init(ExtentTest test) {
        return new EnterpriseManagementDeletePage(test);
    }

    public EnterpriseManagementDeletePage ClickOnSubmit() {
        clickOnElement(Submit, "Click On Submit");
        return this;
    }

    public void ClickOnConfirm() {
        clickOnElement(Confirm, "Click On Confirm Button");
    }
}
