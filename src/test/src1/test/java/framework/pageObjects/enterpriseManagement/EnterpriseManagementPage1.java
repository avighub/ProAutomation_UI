/*
*  COPYRIGHT: Comviva Technologies Pvt. Ltd.
*  This software is the sole property of Comviva
*  and is protected by copyright law and international
*  treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of
*  it may result in severe civil and criminal penalties
*  and will be prosecuted to the maximum extent possible
*  under the law. Comviva reserves all rights not
*  expressly granted. You may not reverse engineer, decompile,
*  or disassemble the software, except and only to the
*  extent that such activity is expressly permitted
*  by applicable law notwithstanding this limitation.
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
*  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
*  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
*  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
*  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
*  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*  Author Name: Automation Team
*  Date: 30-10-2017.
*  Purpose: Selenium test cases
*/
package framework.pageObjects.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class EnterpriseManagementPage1 extends PageInit {


    @FindBy(id = "addEmployee_loadEmployeeDetails_msisdn")
    private WebElement MSISDN;

    @FindBy(id = "addEmployee_loadEmployeeDetails_button_submit")
    private WebElement Submit;

    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_lastName")
    private WebElement lastnamelabel;

    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_msisdnUser")
    private WebElement Msisdnlabel;

    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_msisdnWeb")
    private WebElement Loginidlabel;

    @FindBy(id = "modifyEmployee_loadModifyEmployeeDetails_msisdn")
    private WebElement modiMSISDN;

    @FindBy(id = "modifyEmployee_loadModifyEmployeeDetails_button_submit")
    private WebElement modiSubmit;

    @FindBy(id = "deleteEmployee_loadDeleteEmployeeDetails_msisdn")
    private WebElement deleMSISDN;

    @FindBy(id = "deleteEmployee_loadDeleteEmployeeDetails_button_submit")
    private WebElement deleSubmit;

    public EnterpriseManagementPage1(ExtentTest t1) {
        super(t1);
    }

    public static EnterpriseManagementPage1 init(ExtentTest t1) {
        return new EnterpriseManagementPage1(t1);
    }

    public EnterpriseManagementPage1 EnterMsisdn(String value) {
        setText(MSISDN, value, "MSISDN");
        return this;
    }

    public void ClickOnSubmit() {
        clickOnElement(Submit, "Submit");
    }

    public void ClickOnmodifySubmit() {
        clickOnElement(modiSubmit, "modiSubmit");
    }

    public EnterpriseManagementPage1 EntermodifyMsisdn(String value) {
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        setText(modiMSISDN, value, "modiMSISDN");
        return this;
    }

    /**
     * Click On Delete Button
     */
    public void ClickOnDeleteSubmit() {
        clickOnElement(deleSubmit, "deleSubmit");
    }

    /**
     * @param value
     * @return current Object instance
     */
    public EnterpriseManagementPage1 EnterDeleteMsisdn(String value) {
        setText(deleMSISDN, value, "enter: " + value + " for delete");
        return this;
    }

    /**
     * @return current Object instance
     * @throws NoSuchElementException
     */
    public EnterpriseManagementPage1 NavigateToLinkADD() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_ADD_EMP", "All Bulk Payee");
        return this;
    }

    /**
     * @return current Object instance
     * @throws NoSuchElementException
     */
    public EnterpriseManagementPage1 NavigateToLinkModify() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_MOD_EMP", "All Bulk Payee Modify");
        return this;
    }

    /**
     * @return current Object instance
     * @throws NoSuchElementException
     */
    public EnterpriseManagementPage1 NavigateToLinkDelete() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_DEL_EMP", "All Bulk Payee Delete");
        return this;
    }

    /**
     * To navigate to View Self details
     *
     * @return current Object instance
     * @throws NoSuchElementException
     */
    public EnterpriseManagementPage1 navigateToViewSelfdetails() throws Exception {
        navigateTo("BPAMGT_ALL", "BPAMGT_EPTY_VSELF", "All Bulk Payee Self Details");
        return this;
    }

    public String getMsisdnlabel() {
        return Msisdnlabel.getText();
    }

    public String getLoginidlabel() {
        return Loginidlabel.getText();
    }

    public String getlastnamelabellabel() {
        return lastnamelabel.getText();
    }


}
