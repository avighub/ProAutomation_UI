package framework.pageObjects.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class EnterpriseManagementPage2 extends PageInit {

    private static FunctionLibrary fl;
    private static WebDriver driver;
    private static ExtentTest pageInfo;
    private static EnterpriseManagementPage2 page;


    @FindBy(name = "employeeCode")
    private WebElement UniqueCode;

    @FindBy(id = "addEmployee_addConfirm_button_add")
    private WebElement Submit;

    @FindBy(id = "addEmployee_add_button_confirm")
    private WebElement Confirm;


    public EnterpriseManagementPage2(ExtentTest test) {
        super(test);
    }

    public static EnterpriseManagementPage2 init(ExtentTest test) {
        return new EnterpriseManagementPage2(test);
    }

    public EnterpriseManagementPage2 EnterUniqueCode(String value) {
        setText(UniqueCode, value, "Set Employee Unique Code");
        return this;
    }

    public EnterpriseManagementPage2 ClickOnSubmit() {
        clickOnElement(Submit, "Click On Submit Button");
        return this;
    }

    public void ClickOnConfirm() {
        clickOnElement(Confirm, "Click On Confirm Button");
    }
}
