/*
 *  COPYRIGHT: Comviva Technologies Pvt. Ltd.
 *  This software is the sole property of Comviva
 *  and is protected by copyright law and international
 *  treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of
 *  it may result in severe civil and criminal penalties
 *  and will be prosecuted to the maximum extent possible
 *  under the law. Comviva reserves all rights not
 *  expressly granted. You may not reverse engineer, decompile,
 *  or disassemble the software, except and only to the
 *  extent that such activity is expressly permitted
 *  by applicable law notwithstanding this limitation.
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 *  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 *  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 *  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 *  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *  Author Name: Automation Team
 *  Date: 02-June-2017
 *  Purpose: O2C Approval2 Page1
 */
package framework.pageObjects.ownerToChannel;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Dalia on 02-06-2017.
 */
public class O2CApproval2_Page1 extends PageInit {

    //Page Objects
    @FindBy(id = "o2cApproval2_displayTransactionDetails_button_submit")
    private WebElement submit;

    @FindBy(id = "o2cApproval2_displayTransactionDetails_button_approve")
    private WebElement approve;

    @FindBy(id = "o2cApproval2_displayTransactionDetails_button_reject")
    private WebElement reject;

    @FindBy(id = "o2cApproval2_displayTransactionDetails_remark")
    private WebElement approverTwoRemark;

    public O2CApproval2_Page1(ExtentTest t1) {
        super(t1);
    }

    public static O2CApproval2_Page1 init(ExtentTest t1) {
        return new O2CApproval2_Page1(t1);
    }

    public void navigateToOwner2ChannelApproval2Page() throws Exception {
        navigateTo("O2CTRF_ALL", "O2CTRF_O2CAPP02", "Owner to Channel Transfer Approval-2");
    }

    public void clickSubmit() {
        clickOnElement(submit, "submit");
    }

    public void clickApprove() {
        clickOnElement(approve, "approve");
    }

    /**
     * Method to selct transaction ID
     *
     * @param transactionId
     */
    public void selectTransactionId(String transactionId) {
        wait.until(ExpectedConditions.elementToBeClickable(
                driver.findElement(By.xpath(".//*[@value='" + transactionId + "' and @type='radio']"))))
                .click();
    }

    public void clickReject() {
        clickOnElement(reject, "Reject Button");
    }

    public O2CApproval2_Page1 setRemark(String remark) {
        setText(approverTwoRemark, remark, "Remark");
        return this;
    }
}
