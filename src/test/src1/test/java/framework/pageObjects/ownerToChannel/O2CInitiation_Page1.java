/*
*  COPYRIGHT: Comviva Technologies Pvt. Ltd.
*  This software is the sole property of Comviva
*  and is protected by copyright law and international
*  treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of
*  it may result in severe civil and criminal penalties
*  and will be prosecuted to the maximum extent possible
*  under the law. Comviva reserves all rights not
*  expressly granted. You may not reverse engineer, decompile,
*  or disassemble the software, except and only to the
*  extent that such activity is expressly permitted
*  by applicable law notwithstanding this limitation.
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
*  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
*  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
*  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
*  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
*  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*  Author Name: Automation Team
*  Date: 12-Dec-2017
*  Purpose: O2C initiation page 1
*/
package framework.pageObjects.ownerToChannel;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;


public class O2CInitiation_Page1 extends PageInit {
    //Page Objects
    @FindBy(id = "msisdnId")
    private WebElement mobileNumber;

    @FindBy(id = "providerListSel")
    private WebElement providerName;

    @FindBy(id = "otherWalletListId")
    private WebElement walletName;

    @FindBy(id = "o2cInitiate_confirm_amount")
    private WebElement O2CAmount;

    @FindBy(id = "o2cInitiate_confirm_referenceNumber")
    private WebElement refNumber;

    @FindBy(id = "o2cInitiate_confirm_paymentType")
    private WebElement paymentType;

    @FindBy(name = "paymentNumber")
    private WebElement paymentNumber;

    @FindBy(id = "o2cInitiate_confirm_remark")
    private WebElement remarks;

    @FindBy(xpath = "//label[@for='o2cInitiate_confirm_remark']/span")
    private WebElement remarksLabel;

    @FindBy(id = "o2cInitiate_confirm_button_submit")
    private WebElement submit;

    @FindBy(id = "o2cButt")
    private WebElement confirm;

    @FindBy(name = "action:o2cInitiate_back")
    private WebElement backButton;

    //Confirm Page Elements
    @FindBy(id = "o2cInitiate_confirm_userMap_userName")
    private WebElement confirmPageUserName;

    @FindBy(id = "o2cInitiate_confirm_msisdn")
    private WebElement confirmPagePayeeMsisdn;

    @FindBy(id = "o2cInitiate_confirm_strProviderName")
    private WebElement confirmPageProviderName;

    @FindBy(id = "o2cInitiate_confirm_strWalletName")
    private WebElement confirmPageWalletName;

    @FindBy(id = "o2cInitiate_confirm_categoryName")
    private WebElement confirmCategoryName;

    @FindBy(id = "o2cInitiate_confirm_referenceNumber")
    private WebElement confirmPageReferenceNumber;

    @FindBy(xpath = ".//*[@id='o2cInitiate_confirm']/table/tbody/tr[10]/td[3]")
    private WebElement confirmPageAmount;



    public O2CInitiation_Page1(ExtentTest t1) {
        super(t1);
    }

    public static O2CInitiation_Page1 init(ExtentTest t1) {
        return new O2CInitiation_Page1(t1);
    }

    public static void main(String[] args) {
        System.out.println("Test");
    }

    /**
     * Navigate to Owner to Channel Transfer Initiate Page
     *
     * @throws Exception
     */
    public void navigateToOwner2ChannelInitiationPage() throws Exception {
        navigateTo("O2CTRF_ALL", "O2CTRF_O2CINT001", "Owner To Channel Transfer");
    }

    /**
     * Method to enter msisdn
     *
     * @param msisdn
     * @throws Exception
     */
    public void setMobileNumber(String msisdn) throws Exception {
        setText(mobileNumber, msisdn, "mobileNumber");
    }

    /**
     * Method to select provider
     *
     * @param provider
     * @throws Exception
     */
    public void selectProviderName(String provider) throws Exception {
        clickOnElement(providerName, "ProviderName");
        Thread.sleep(1000);
        selectValue(providerName, provider, "Currency Provider");
    }

    /**
     * Method to select wallet name
     *
     * @param wallet
     * @throws Exception
     */
    public void selectWallet(String wallet) throws Exception {
        selectVisibleText(walletName, wallet, "WalletName");
    }

    /**
     * Method to enter amount
     *
     * @param amount
     * @throws Exception
     */
    public void setO2CAmount(String amount) throws Exception {
        setText(O2CAmount, amount, "Amount");
    }

    /**
     * Method to set Reference number
     *
     * @param ref
     * @throws Exception
     */
    public void setRefNumber(String ref) throws Exception {
        setText(refNumber, ref, "Reference Number");
    }

    /**
     * Method to select payment Type
     *
     * @param payType
     * @throws Exception
     */
    public void selectPaymentType(String payType) throws Exception {
        selectVisibleText(paymentType, payType, "Payment Type");
    }

    /**
     * method to enter remarks
     *
     * @param msg
     * @throws Exception
     */
    public void setRemarks(String msg) throws Exception {
        setText(remarks, msg, "Remark");
    }

    /**
     * Method to enter payment number
     *
     * @param number
     * @throws Exception
     */
    public void setPaymentNumber(String number) throws Exception {
        setText(paymentNumber, number, "Payment Number");
    }

    /**
     * method to click on submit button
     */
    public void clickSubmit() {
        clickOnElement(submit, "Submit");
        Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
    }

    /**
     * Method to click on confirm button
     */
    public void clickConfirm() {
        clickOnElement(confirm, "Confirm");
    }

    public void clickBackButton() {
        backButton.click();
        pageInfo.info("Clicked on Back button");
    }


    public WebElement getRemarksLabelWebElement(){
        return remarksLabel;
    }
    /**
     * @param payee
     * @param ReferenceNumber
     * @throws IOException
     */
    public void verifyValuesInConfirmationScreen(User payee, String ReferenceNumber) throws IOException {
        Assertion.verifyEqual(confirmPageUserName.getText(), payee.FirstName + " " + payee.LastName, "Verify User Name.", pageInfo);
        Assertion.verifyEqual(confirmPageProviderName.getText(), DataFactory.getDefaultProvider().ProviderName, "Verify Payee Provider", pageInfo);
        Assertion.verifyEqual(confirmPageWalletName.getText(), DataFactory.getDefaultWallet().WalletName, "Verify Payee Wallet Type", pageInfo);
        Assertion.verifyEqual(confirmCategoryName.getText(), payee.CategoryName, "Verify Payee Category Name", pageInfo);
        Assertion.verifyEqual(confirmPageReferenceNumber.getText(), ReferenceNumber, "Verify Reference Number", pageInfo);
    }
}
