package framework.pageObjects.svaToOwnBankTransfer;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

/**
 * Created by Dalia on 02-06-2017.
 */
public class svaToOwnBankTransfer_Page1 extends PageInit {

    @FindBy(id = "svaToOwnBank_confirmTransferNow_button_back")
    WebElement backBtn;
    @FindBy(xpath = "//input[@value='Cancel']")
    WebElement cancel;
    @FindBy(id = "remarks")
    WebElement remarkstxtField;
    //Page Objects
    @FindBy(id = "transferTypeId")
    private WebElement transferType;
    @FindBy(name = "button.next")
    private WebElement Next;
    @FindBy(id = "partyProviderList")
    private WebElement providerName;
    @FindBy(id = "paymentMethodDetailList")
    private WebElement walletName;
    @FindBy(id = "bankDetailList")
    private WebElement bankName;
    @FindBy(id = "accountDetailList")
    private WebElement accountNumber;
    @FindBy(id = "amountFIXED_AMOUNT")
    private WebElement fixedAmount;
    @FindBy(id = "tranferBalance")
    private WebElement amount;
    @FindBy(id = "svaToOwnBank_initiateTransaction_button_submit")
    private WebElement submit;
    @FindBy(id = "cashButt")
    private WebElement confirm;
    @FindBy(id = "SVATOBANAC_VSDM_SVA")
    private WebElement schedule;
    @FindBy(id = "dailyselectDAILY")
    private WebElement daily;
    @FindBy(id = "weeklyWEEKLY")
    private WebElement weekly;

    @FindBy(id = "weekselect")
    private WebElement weeklyDayddown;

    @FindBy(id = "monthlyMONTHLY")
    private WebElement monthly;

    @FindBy(id = "dayselect")
    private WebElement MonthDayDdown;

    @FindBy(id = "monthselect")
    private WebElement MonthDdown;

    @FindBy(id = "yearlyYEARLY")
    private WebElement yearly;

    @FindBy(id = "yearmonthselect")
    private WebElement YearlyMonthDdown;

    @FindBy(id = "yeardayselect")
    private WebElement YearlyDayDdown;

    @FindBy(id = "endScheduleTypeNO_OF_OCCURRENCE")
    private WebElement TransferType;

    @FindBy(id = "occurrencesTime")
    private WebElement Occurance;

    @FindBy(id = "remarks")
    private WebElement remarks;

    @FindBy(id = "svaToOwnBank_forwardPage_button_submit")
    private WebElement submitSchedule;

    @FindBy(id = "svaToOwnBank_confirmScheduleForLater_button_confirm")
    private WebElement confirmSchedule;

    @FindBy(xpath = "//input[@value='Cancel']")
    private WebElement CancelSchedule;

    @FindBy(id = "walletBalance")
    private WebElement WalletBalance;

    @FindBy(id = "amountTOTAL_BALANCE")
    private WebElement TotalAmount;

    @FindBy(id = "svaToOwnBank_confirmTransferNow_button_back")
    private WebElement ConfirmBackButton;

    @FindBy(id = "endScheduleTypeEND_DATE")
    private WebElement EndDate;

    @FindBy(xpath = "//img[@dojoattachpoint=\"buttonNode\"]")
    private WebElement calender;

    @FindBy(id = "svaToOwnBank_confirmScheduleForLater_button_back")
    private WebElement ConfirmBackSchedule;

    public svaToOwnBankTransfer_Page1(ExtentTest t1) {
        super(t1);
    }

    public static svaToOwnBankTransfer_Page1 init(ExtentTest t1) {
        return new svaToOwnBankTransfer_Page1(t1);
    }

    /**
     * Navigate to SVA To Own Bank Transfer Page
     *
     * @throws Exception
     */
    public void navigateToSVA2OwnBankTransferInWeb() throws Exception {
        navigateTo("SVATOBANAC_ALL", "SVATOBANAC_SVATO_BANKAC", "SVA To Own Bank Transfer");
    }

    //Select Transfer Type
    public void selectTransferType(String trType) throws Exception {
        selectVisibleText(transferType, trType, "transferType");
    }

    //Click on Next button
    public void clickNext() {
        //clickOnElement(Next, "Next");
        Next.click();
        pageInfo.info("Clicked on Next Button.");
    }

    //Select Provider Name
    public void selectProviderName(String provider) throws Exception {
        selectVisibleText(providerName, provider, "providerName");
    }

    //Select Wallet
    public void selectWallet(String wallet) throws Exception {
        selectVisibleText(walletName, wallet, "walletName");
    }

    //Select Bank
    public void selectBankName(String bank) throws Exception {
        selectVisibleText(bankName, bank, "bankName");
    }

    //Select Account Number
    public void selectAccountNumber(String accNum) throws Exception {
        selectVisibleText(accountNumber, accNum, "accountNumber");
    }

    public void selectAccountNumberByValue(String accNum) throws Exception {
        selectValue(accountNumber, accNum, "accountNumber");
    }

    public void selectAccountByIndex() throws Exception {
        accountNumber.click();
        selectIndex(accountNumber, 1, "accountNumber");
    }

    public void selectAccountNum(String bankAccNum) throws Exception {
        accountNumber.click();
        selectValue(accountNumber, bankAccNum, "accountNumber");
    }

    //Select Amount
    public void selectAmountType() throws Exception {
        clickOnElement(fixedAmount, "fixedAmount");
    }

    public void TotalBalance() {
        clickOnElement(TotalAmount, "TotalAmount");
    }

    public void enterRemarks() throws Exception {
        remarkstxtField.sendKeys("Selenium");
    }

    //Enter Amount
    public void setTransferAmount(String text) throws Exception {
        setText(amount, text, "amount");
    }

    //Click on Submit button
    public void clickSubmit() throws InterruptedException {
        clickOnElement(submit, "submit");
        Boolean isTure = Utils.checkElementPresent("svaToOwnBank_initiateTransaction_button_submit", Constants.FIND_ELEMENT_BY_ID);
        if (isTure) {
            Thread.sleep(1000);
            clickOnElement(submit, "Re Click Submit");
        }
    }

    //Click on Submit button
    public void clickOnSubmit() throws InterruptedException {
        clickOnElement(submit, "submit");
    }

    public void CheckSubmit() {
        if (fl.elementIsDisplayed(submit)) {
            pageInfo.pass("Submit is available");
        } else {
            pageInfo.fail("Submit is not available");
            try {
                Utils.captureScreen(pageInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void clickBack() {
        backBtn.click();
        pageInfo.info("Click on Back Button");
    }

    //Click on Confirm button
    public void clickConfirm() {
        clickOnElement(confirm, "confirm");
    }

    public void clickDaily() {
        clickOnElement(daily, "daily");
    }

    public void clickWeekly() {
        clickOnElement(weekly, "weekly");
    }

    public void weeklyDayDdrop(String day) {
        selectVisibleText(weeklyDayddown, day, "weeklyDayddown");
    }

    public void clickMonthly() {
        clickOnElement(monthly, "monthly");
    }

    public void monthlyDayDdrop(String day) {
        selectVisibleText(MonthDayDdown, day, "MonthDayDdown");
    }

    public void monthlyMonthDdrop(String Month) {
        selectVisibleText(MonthDdown, Month, "MonthDdown");
    }

    public void clickYearly() {
        clickOnElement(yearly, "yearly");
    }

    public void yearlyMonthDdrop(String Month) {
        selectVisibleText(YearlyMonthDdown, Month, "YearlyMonthDdown");
    }

    public void yearlyDayDdrop(String Day) {
        selectVisibleText(YearlyDayDdown, Day, "YearlyDayDdown");
    }

    public void setRemarks(String text) {
        setText(remarks, text, "remarks");
    }

    public void clickSubmitSchedule() throws InterruptedException {
        clickOnElement(submitSchedule, "submitSchedule");
        Boolean isTure = Utils.checkElementPresent("svaToOwnBank_forwardPage_button_submit", Constants.FIND_ELEMENT_BY_ID);
        if (isTure) {
            clickOnElement(submitSchedule, "submitSchedule");
            pageInfo.debug("Clicking on submit failed on 1st atempt ");
        }
    }

    public void setOccurance(String text) {

        setText(Occurance, text, "Occurance");
    }

    public void clickConfirmSchedule() {

        clickOnElement(confirmSchedule, "confirmSchedule");
    }

    public void clickCancel() {

        clickOnElement(CancelSchedule, "CancelSchedule");
    }

    public void WalletBalanceDisabled() {
        if (WalletBalance.isEnabled()) {
            pageInfo.fail("Waller Balance is not disabled ");
        } else {
            pageInfo.info("Wallet Balance is Disabled");
        }
    }

    public String WalledBalance() {

        return WalletBalance.getText().toString();
    }

    public void clickConfirmBack() {
        clickOnElement(ConfirmBackButton, "BackButton");
    }

    public void SVAtoBankTT_page() {
        providerName.isDisplayed();
        walletName.isDisplayed();
        bankName.isDisplayed();
        accountNumber.isDisplayed();
        remarks.isDisplayed();
        pageInfo.info("Entered the SVA to Own Bank Account page");
    }

    public void ClickEndDate() {
        clickOnElement(EndDate, "EndDate");
    }

    public void CheckCalenderVisiablity() {
        if (fl.elementIsDisplayed(calender)) {
            pageInfo.pass("calender is available");
        } else {
            pageInfo.fail("calender is not available");
            try {
                Utils.captureScreen(pageInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void ClickScheduleConfirmBack() {
        clickOnElement(ConfirmBackSchedule, "ConfirmBackSchedule");
    }

    public void CheckSubmitSchedule() {
        if (fl.elementIsDisplayed(submitSchedule)) {
            pageInfo.pass("submitSchedule is available");
        } else {
            pageInfo.fail("submitSchedule is not available");
            try {
                Utils.captureScreen(pageInfo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getRemarksTextFromGui() {
        return remarks.getAttribute("value");
    }

    public String getAmountFromGui() {
        return amount.getAttribute("value");
    }

    public boolean checkSubmitDisplayed() {
        return submit.isDisplayed();
    }
}



