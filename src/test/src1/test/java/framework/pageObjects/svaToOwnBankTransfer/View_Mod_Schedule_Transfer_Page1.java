package framework.pageObjects.svaToOwnBankTransfer;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Dalia on 02-06-2017.
 */
public class View_Mod_Schedule_Transfer_Page1 extends PageInit {


    //Page Objects
    @FindBy(id = "selectedRequestNo")
    WebElement selectTransferRadio;
    @FindBy(id = "_button_view1")
    WebElement btnView;
    @FindBy(id = "_button_modify")
    WebElement btnModify;
    @FindBy(id = "_button_stop")
    WebElement btnStop;
    @FindBy(id = "_button_delete")
    WebElement btnDelete;
    @FindBy(xpath = "//input[@value=\"Delete\"]")
    WebElement btnDeleteP2;
    @FindBy(id = "selectedRequestNo")
    WebElement lableRequestNo;
    @FindBy(id = "walletBalance")
    WebElement SVALabel;
    @FindBy(id = "accountDetailList")
    WebElement ddownAcNum;
    @FindBy(id = "svaToOwnBank_forwardPage_fromDateStr")
    WebElement lablecreationDate;
    @FindBy(id = "amount")
    WebElement lableamount;
    @FindBy(id = "noOfOccurrence")
    WebElement lableNumOfOccurance;
    @FindBy(id = "svaToOwnBank_forwardPage_nextPaymentDate")
    WebElement lableNextPaymentDate;
    @FindBy(id = "noOfPaymentsLeft")
    WebElement lableNumOfPaymentLeft;
    @FindBy(id = "svaToOwnBank_forwardPage_remarks")
    WebElement lableRemarks;
    @FindBy(id = "svaToOwnBank_forwardPage_button_modify")
    WebElement btnViewModify;
    //modify
    @FindBy(id = "partyProviderList")
    WebElement ddownProvider;
    @FindBy(id = "paymentMethodDetailList")
    WebElement ddownWalletToDebit;
    @FindBy(id = "bankDetailList")
    WebElement ddownBankName;
    @FindBy(id = "accountDetailList")
    WebElement ddownAcToCredit;
    @FindBy(id = "amount")
    WebElement lableAmount;
    @FindBy(id = "dailyselectDAILY")
    WebElement daily;
    @FindBy(id = "weeklyWEEKLY")
    WebElement weekly;
    @FindBy(id = "weekselect")
    WebElement weeklyDayddown;
    @FindBy(id = "monthlyMONTHLY")
    WebElement monthly;
    @FindBy(id = "dayselect")
    WebElement MonthDayDdown;
    @FindBy(id = "monthselect")
    WebElement MonthDdown;
    @FindBy(id = "yearlyYEARLY")
    WebElement yearly;
    @FindBy(id = "yearmonthselect")
    WebElement YearlyMonthDdown;
    @FindBy(id = "yeardayselect")
    WebElement YearlyDayDdown;
    @FindBy(id = "endScheduleTypeNO_OF_OCCURRENCE")
    WebElement TransferType;
    @FindBy(id = "noOfOccurrence")
    WebElement Occurance;
    @FindBy(id = "svaToOwnBank_forwardPage_remarks")
    WebElement remarks;
    @FindBy(id = "svaToOwnBank_forwardPage_button_submit")
    WebElement submitButton;
    @FindBy(id = "svaToOwnBank_forwardPage_button_confirm")
    WebElement buttonConfirm;

    public View_Mod_Schedule_Transfer_Page1(ExtentTest t1) {
        super(t1);
    }

    public static View_Mod_Schedule_Transfer_Page1 init(ExtentTest t1) {
        return new View_Mod_Schedule_Transfer_Page1(t1);
    }

    /**
     * Navigate to SVA To Own Bank Transfer Page
     *
     * @throws Exception
     */
    public void navigateToSVA2OwnBankTransferInWeb() throws Exception {
        fl.leftNavigation("SVATOBANAC_ALL", "SVATOBANAC_SVATO_BANKAC");
        pageInfo.info("Navigate to SVA To Own Bank Transfer Page");
    }

    public void navigateToViewModStopDelSVA() throws Exception {
        fl.leftNavigation("SVATOBANAC_ALL", "SVATOBANAC_VSDM_SVA");
        pageInfo.info("Navigate to SVA To Own Bank Transfer Page");
    }

    //Select Transfer Type
    public void selectTransferRadio() {
        clickOnElement(selectTransferRadio, "selectTransferRadio");
    }

    public void clickView() {
        clickOnElement(btnView, "btnView");
    }

    public void clickModify() {
        clickOnElement(btnModify, "btnModify");
    }

    public void clickDelete() {
        clickOnElement(btnDelete, "btnDelete");
    }

    public void clickDeleteP2() {
        clickOnElement(btnDeleteP2, "btnDeleteP2");
    }

    public void clickStop() {
        clickOnElement(btnStop, "btnStop");
    }

    public String fetchWalletBalance() {
        return SVALabel.getText();
    }

    public String fetchRequestNo() {
        pageInfo.info("Get the Request Number");
        return lableRequestNo.getText();
    }

    public String fetchAcNo() {
        pageInfo.info("Get the Account Number");
        return ddownAcNum.getText();
    }

    public String fetchCreationDate() {
        pageInfo.info("Get the Creation Date");
        return lablecreationDate.getText();
    }

    public String fetchAmount() {
        pageInfo.info("Get the Amount");
        return lableamount.getText();
    }

    public String fetchNoOfOccurance() {
        pageInfo.info("Get the Number of occurance");
        return lableNumOfOccurance.getText();
    }

    public String fetchNextPaymentDate() {
        pageInfo.info("Get the Next Payment date");
        return lableNextPaymentDate.getText();
    }

    public String fetchNumberOfPaymentLeft() {
        pageInfo.info("Checking the Number of Payment left");
        return lableNumOfPaymentLeft.getText();
    }

    public void providerIDddown(String PID) {
        selectVisibleText(ddownProvider, PID, "ddownProvider");
    }

    public void WalletToDebitDdown(String text) {
        selectVisibleText(ddownWalletToDebit, text, "ddownWalletToDebit");
    }

    public void BankNameDdown(String text) {
        selectVisibleText(ddownBankName, text, "ddownBankName");
    }

    public void ACtoBeCreditedDdown(String text) {
        selectVisibleText(ddownAcToCredit, text, "ddownAcToCredit");
    }

    public void setAmount(String text) {
        setText(lableAmount, text, "lableAmount");
    }

    public void clickWeekly() {
        clickOnElement(weekly, "weekly");
    }

    public void weeklyDayDdrop(String day) {
        selectVisibleText(weeklyDayddown, day, "weeklyDayddown");
    }

    public void setRemarks(String text) {
        setText(remarks, text, "remarks");
    }

    public void setOccurance(String text) {
        setText(Occurance, text, "Occurance");
    }

    public void clickSubmit() {
        clickOnElement(submitButton, "submitButton");
    }

    public void clickModModify() {
        clickOnElement(btnViewModify, "btnViewModify");
    }

    public void clickConfirm() {
        clickOnElement(buttonConfirm, "buttonConfirm");
    }
}