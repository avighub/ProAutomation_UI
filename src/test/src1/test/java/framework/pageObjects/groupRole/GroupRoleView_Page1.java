package framework.pageObjects.groupRole;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by shruti.gupta on 07-07-2017.
 */
public class GroupRoleView_Page1 extends PageInit {


    /*
    * Page Objects
    */

//    @FindBy(xpath = "//input[@id='grouprole_view_groupRoleCode'][@name ='groupRoleCode'][@type='text']")
//    private WebElement GroupRoleCode;

    @FindBy(id = "grouproleview_getList_button_view")
    public WebElement btnViewRole;
    @FindBy(id = "grouproleview_getList_domainName")
    public WebElement DomainNameValue;
    @FindBy(id = "grouproleview_getList_categoryName")
    public WebElement CategoryNameValue;
    @FindBy(name = "groupRoleCode")
    private WebElement GroupRoleCode;
    @FindBy(name = "groupRoleName")
    private WebElement GroupRoleName;

    public GroupRoleView_Page1(ExtentTest t1) {
        super(t1);
    }

    public String getGroupRoleCode() {
        return GroupRoleCode.getAttribute("value");
    }

    public String getGroupRoleName() {
        return GroupRoleName.getAttribute("value");
    }

    public boolean isApplicableRolesChecked(List<String> applicableRoles) throws Exception {
        for (String role : applicableRoles) {
            if (isNonEmpty(role)) {
                if (!driver.findElement(By.xpath("//input[@name='check' and @value='" + role + "']")).isSelected())
                    return false;
            }
        }
        return true;
    }

    public boolean isNonEmpty(String string) {
        return string != null && string != "";
    }

    public void clickViewRoleDetails() {
        clickOnElement(btnViewRole, "View Button");
    }

    public void selectRoleToView(String roleName) {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + roleName + "')]/ancestor::tr[1]/td/input[@type='radio']")).click();
        pageInfo.info("check " + roleName);
    }

    public String getDomainNameValue() {
        String DomainName = DomainNameValue.getText();
        return DomainName;
    }

    public String getCategoryNameValue() {
        String CatName = CategoryNameValue.getText();
        return CatName;
    }
}
