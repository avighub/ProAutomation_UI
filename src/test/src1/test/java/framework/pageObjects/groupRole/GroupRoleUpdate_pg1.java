package framework.pageObjects.groupRole;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GroupRoleUpdate_pg1 extends PageInit {
    @FindBy(id = "grouprole_modify_groupRoleCode")
    public WebElement roleCode;

    @FindBy(id = "grouprole_modify_groupRoleName")
    public WebElement roleName;

    @FindBy(id = "grouprole_modify_domainName")
    WebElement domainName;

    @FindBy(id = "grouprole_modify_categoryName")
    WebElement categoryName;

    @FindBy(id = "grouprole_modify_gradeName")
    WebElement gradeName;

    public GroupRoleUpdate_pg1(ExtentTest t1) {
        super(t1);
    }

    public static GroupRoleUpdate_pg1 init(ExtentTest t1) {
        return new GroupRoleUpdate_pg1(t1);
    }

    public String getRoleNameUI() {
        return wait.until(ExpectedConditions.visibilityOf(roleName)).getAttribute("value");
    }

    public String getRoleCodeUI() {
        return wait.until(ExpectedConditions.visibilityOf(roleCode)).getAttribute("value");
    }

    public String getDomainNameUI() {
        return wait.until(ExpectedConditions.visibilityOf(domainName)).getText();
    }

    public String getCategoryNameUI() {
        return wait.until(ExpectedConditions.visibilityOf(categoryName)).getText();
    }

    public String getGradeNameUI() {
        return wait.until(ExpectedConditions.visibilityOf(gradeName)).getText();
    }

    public WebElement getCheckboxCheckAll() {
        try {
            return driver.findElement(By.xpath("//tr/td[contains(text(), 'Check All')]/input[@type = 'checkbox']"));
        } catch (NoSuchElementException e) {
            return null;
        }
    }

}
