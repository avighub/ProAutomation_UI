package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ViewInstrumentTCPPage extends PageInit {

    /*
     * Page Objects
	 */

    @FindBy(id = "domainNameView")
    private WebElement domainNameUI;

    @FindBy(id = "categoryNameView")
    private WebElement categoryNameUI;

    @FindBy(id = "paymentNameView")
    private WebElement paymentNameUI;

    @FindBy(id = "gradeNameView")
    private WebElement gradeNameUI;

    @FindBy(id = "profileNameView")
    private WebElement profileNameUI;

    @FindBy(xpath = "//*[@type='button']")
    private WebElement backButton;


    public ViewInstrumentTCPPage(ExtentTest t1) {
        super(t1);
    }

    public String getDomainNameFromUI() throws NoSuchElementException {
        //Sleep is needed for the element to load
        Utils.putThreadSleep(5000);
        return domainNameUI.getText();
    }

    public String getCategoryNameFromUI() throws NoSuchElementException {
        return categoryNameUI.getText();
    }

    public String getPaymentInstrumentFromUI() throws NoSuchElementException {
        return paymentNameUI.getText();
    }

    public String getGradeNameFromUI() throws NoSuchElementException {
        return gradeNameUI.getText();
    }

    public String getProfileNameFromUI() throws NoSuchElementException {
        return profileNameUI.getText();
    }

    public void clickBackButton() {
        backButton.click();
    }


}
