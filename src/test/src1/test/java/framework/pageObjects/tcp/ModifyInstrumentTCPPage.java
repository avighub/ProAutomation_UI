package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


public class ModifyInstrumentTCPPage extends PageInit {

    /*
     * Page Objects
	 */

    @FindBy(id = "domainNameView")
    private WebElement domainNameUI;

    @FindBy(id = "categoryNameView")
    private WebElement categoryNameUI;

    @FindBy(id = "paymentNameView")
    private WebElement paymentNameUI;

    @FindBy(id = "gradeNameView")
    private WebElement gradeNameUI;

    @FindBy(id = "profileNameView")
    private WebElement profileNameUI;

    @FindBy(id = "edit_instrument_next")
    private WebElement editButton;

    @FindBy(id = "edit_instrument_next2")
    private WebElement nextButton;

    @FindBy(id = "edit_instrument_next2")
    private WebElement confirmButton;

    @FindBy(id = "main_success_msg")
    private WebElement SuccessMessage;


    public ModifyInstrumentTCPPage(ExtentTest t1) {
        super(t1);
    }

    public String getDomainNameFromUI() throws NoSuchElementException {
        //Sleep is needed for the element to load
        Utils.putThreadSleep(5000);
        return domainNameUI.getText();
    }

    public String getCategoryNameFromUI() throws NoSuchElementException {
        return categoryNameUI.getText();
    }

    public String getPaymentInstrumentFromUI() throws NoSuchElementException {
        return paymentNameUI.getText();
    }

    public String getGradeNameFromUI() throws NoSuchElementException {
        return gradeNameUI.getText();
    }

    public String getProfileNameFromUI() throws NoSuchElementException {
        return profileNameUI.getText();
    }

    public void clickEditButton() {
        clickOnElement(editButton, "Edit Button");
    }

    public void clickNextButton() {
        clickOnElement(nextButton, "Next Button");
    }

    public void clickConfirmButton() {
        clickOnElement(confirmButton, "Confirm Button");
    }

    public void modifyTCPThresholdValues(String thresholdType, String thresholdSubType, String bearerType, String value) {

        List<WebElement> list = driver.findElements(By.xpath("//input[@thresholdtype='" + thresholdType + "'][@thresholdsubtype='" + thresholdSubType + "'][@bearertype='" + bearerType + "']"));

        for (WebElement threshold : list) {
            setText(threshold, value, "threshold");
        }

    }

    /**
     * @param payerCount
     * @param payerAmount
     * @param payeeCount
     * @param payeeAmount
     */
    public void modifyTCPThresholdAllServices(String payerCount, String payerAmount, String payeeCount, String payeeAmount) {

        List<WebElement> listPayerCount = driver.findElements(By.xpath("//input[@class='allserv'][@typeid='PAYER'][@thresholdsubtype='count']"));
        List<WebElement> listPayerAmount = driver.findElements(By.xpath("//input[@class='allserv'][@typeid='PAYER'][@thresholdsubtype='amount']"));
        List<WebElement> listPayeeCount = driver.findElements(By.xpath("//input[@class='allserv'][@typeid='PAYEE'][@thresholdsubtype='count']"));
        List<WebElement> listPayeeAmount = driver.findElements(By.xpath("//input[@class='allserv'][@typeid='PAYEE'][@thresholdsubtype='amount']"));


        for (WebElement thresholdPayerCount : listPayerCount) {
            if (thresholdPayerCount.getAttribute("thresholdtype").equalsIgnoreCase("Per Transaction")) {
                setText(thresholdPayerCount, "1", "thresholdPayerCount");
            } else {
                setText(thresholdPayerCount, payerCount, "thresholdPayerCount");
            }

        }

        for (WebElement thresholdPayerAmount : listPayerAmount) {
            setText(thresholdPayerAmount, payerAmount, "thresholdPayerAmount");
        }

        for (WebElement thresholdPayeeCount : listPayeeCount) {
            if (thresholdPayeeCount.getAttribute("thresholdtype").equalsIgnoreCase("Per Transaction")) {
                setText(thresholdPayeeCount, "1", "thresholdPayeeCount");
            } else {
                setText(thresholdPayeeCount, payeeCount, "thresholdPayeeCount");
            }
        }

        for (WebElement thresholdPayeeAmount : listPayeeAmount) {
            setText(thresholdPayeeAmount, payeeAmount, "thresholdPayeeAmount");
        }

    }

    public String getSuccessMessage() {
        Utils.scrollToTopOfPage();
        return wait.until(ExpectedConditions.visibilityOf(SuccessMessage)).getText();
    }


}
