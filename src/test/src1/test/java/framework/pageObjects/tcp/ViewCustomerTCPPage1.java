package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class ViewCustomerTCPPage1 extends PageInit {

    /*
     * Page Objects
	 */

    @FindBy(id = "domainNameview")
    private WebElement domainNameUI;

    @FindBy(id = "categoryNameview")
    private WebElement categoryNameUI;

    @FindBy(id = "registrationTypeview")
    private WebElement regTypeUI;

    @FindBy(id = "profilenameview")
    private WebElement profileNameUI;

    @FindBy(xpath = "//*[@type='button']")
    private WebElement backButton;


    public ViewCustomerTCPPage1(ExtentTest t1) {
        super(t1);
    }

    public String getDomainNameFromUI() throws NoSuchElementException {
        //Sleep is needed for the element to load
        Utils.putThreadSleep(5000);
        return domainNameUI.getText();
    }

    public String getCategoryNameFromUI() throws NoSuchElementException {
        return categoryNameUI.getText();
    }

    public String getRegTypeFromUI() throws NoSuchElementException {
        return regTypeUI.getText();
    }

    public String getProfileNameFromUI() throws NoSuchElementException {
        return profileNameUI.getText();
    }

    public void clickBackButton() {
        backButton.click();
    }


}
