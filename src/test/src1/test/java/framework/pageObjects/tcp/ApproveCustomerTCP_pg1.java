package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.CustomerTCP;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class ApproveCustomerTCP_pg1 extends PageInit {


    /*
     * Page Objects
	 */
    @FindBy(id = "approve_button")
    public WebElement btnApproveTcp;

    @FindBy(id = "action_message")
    public WebElement ActionMessage;

    public ApproveCustomerTCP_pg1(ExtentTest t1) {
        super(t1);
    }

    public static ApproveCustomerTCP_pg1 init(ExtentTest t1) {
        return new ApproveCustomerTCP_pg1(t1);
    }

    public void selectTcpToApprove(String tcpName) {
        WebElement approveLink = driver.findElement(By.xpath("//tr/td[contains(text(), '" + tcpName + "')]/ancestor::tr[1]/td/span/a"));
        clickOnElement(approveLink, "Select to Approve {" + tcpName + "}");
    }

    public void approveTCP() {
        clickOnElement(btnApproveTcp, "btnApproveTcp");
    }

    /**
     * Assert Customer TCP is successfully Approved
     *
     * @param tcp
     * @return
     * @throws Exception
     */
    public boolean assertTCPApproved(CustomerTCP tcp) throws Exception {
        //String expected = "Request to add Customer Level TCP with name:"+ tcp.ProfileName +
        //      " for the "+ tcp.DomainName +" and "+ tcp.CategoryName +" category has been approved successfully";

        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 30);
        String expected = "category has been approved successfully";
        String actual = wait.until(ExpectedConditions.visibilityOf(ActionMessage)).getText();
        if (Assertion.verifyContains(actual, expected, "Verify if Customer TCP Successfully Approved", pageInfo)) {
            return true;
        }
        return false;
    }

    /**
     * Navigate to Customer TCP approval
     *
     * @throws Exception
     */
    public void navCustomerTCPApproval() throws Exception {
        navigateTo("TCPROFILE_ALL", "TCPROFILE_TCP_USERAPP", "Customer TCP Approval");
    }
}
