package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class EditTCP extends PageInit {

    @FindBy(xpath = "//table[@id='myTable']/tbody/tr[@class='list']/td[.='Subscriber - Subscriber']/following-sibling::td[.='Normal']/following-sibling::td[.='Gold Subscriber']/following-sibling::td[4]")
    private WebElement edittcpforsubs;

    @FindBy(id = "edit_instrument_next")
    private WebElement editbutton;

    @FindBy(id = "minimum_transaction_amount_edit")
    private WebElement mintransaction;

    @FindBy(id = "maximum_transaction_amount_edit")
    private WebElement maxtransaction;

    @FindBy(xpath = "(//input[@servicename='O2S Transfer' and @thresholdtype='Daily' and @thresholdsubtype='amount'])[1]")
    private WebElement O2SAmount1;

    @FindBy(xpath = "(//input[@servicename='O2S Transfer' and @thresholdtype='Daily' and @thresholdsubtype='amount'])[2]")
    private WebElement O2SAmount2;
    //(//input[@servicename='O2S Transfer' and @thresholdtype='Daily' and @thresholdsubtype='amount'])[1]

    @FindBy(id = "edit_instrument_next2")
    private WebElement next;

    @FindBy(id = "edit_instrument_next2")
    private WebElement confirm;

    @FindBy(id = "tcp0")
    private WebElement details;

    @FindBy(id = "approve")
    private WebElement approve;

    public EditTCP(ExtentTest t1) {
        super(t1);
    }

    public static EditTCP init(ExtentTest t1) {
        return new EditTCP(t1);
    }

    public void navAddInstrumentTcp() throws Exception {
        navigateTo("TCPROFILE_ALL", "TCPROFILE_TCP_INSTRMENT", "Add Instrument Level TCP");
    }

    public void selectontcp() throws Exception {
        clickOnElement(edittcpforsubs, "edittcpforsubs");
    }

    public void clickedit() throws Exception {
        FunctionLibrary functionLibrary = new FunctionLibrary(driver);
        functionLibrary.contentFrame();
        editbutton.click();
        pageInfo.info("Click On editbutton!");
    }

    public void selectontcpForChannelUser(String profileName, String fromUser, String toUser, String grade, String provider) throws Exception {
        WebElement tcp = driver.findElement(By.xpath("//table[@id='myTable']/tbody/tr[@class='list']/td[.='" + profileName + "']/following-sibling::td[.='" + fromUser + " - " + toUser + "']/following-sibling::td[.='Normal']/following-sibling::td[.='" + grade + "']/following-sibling::td[.='" + provider + "']/following-sibling::td[3]"));
        clickOnElement(tcp, "edittcpforchanneluser");
    }

    public void editonMintcp(String Value) throws Exception {
        setText(mintransaction, Value, "mintransaction");
    }

    public void editonMaxtcp(String Value) throws Exception {
        setText(maxtransaction, Value, "mintransaction");
    }

    public void editonServiceName(String ServiceName, String Value) throws Exception {


        List<WebElement> service = driver.findElements(By.xpath("//table[@id='edit_table']/tbody/tr/td/input[@servicename='" + ServiceName + "']"));
        for (WebElement i : service) {
            i.clear();
            i.sendKeys(Value);
        }
    }

    public void minTranstcp(String Value) throws Exception {
        setText(mintransaction, Value, "mintransaction");
        clickOnElement(next, "next");
        clickOnElement(confirm, "confirm");
    }

    public void O2SamountTCP(String value) throws Exception {

       /* driver = DriverFactory.getDriver();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,5000)", "");
        Thread.sleep(1000);*/

        O2SAmount1.clear();
        O2SAmount1.sendKeys(value);
        Thread.sleep(1000);
        O2SAmount2.clear();
        O2SAmount2.sendKeys(value);

        //Utils.setValueUsingJs(O2SAmount,value);
        pageInfo.info("Update the O2S daily limit " + value);

    }

    public void navInstrumentTcpApproval() throws Exception {
        navigateTo("TCPROFILE_ALL", "TCPROFILE_TCP_INSTRAPPR", "Approve Instrument Level TCP");
    }

    public void clickonApprove() throws Exception {
        clickOnElement(approve, "approve");
    }

    public void clickdetails() {
        clickOnElement(details, "details");
    }

    public void clickNext() {
        clickOnElement(next, "next");
    }

    public void clickConfirm() {
        clickOnElement(confirm, "confirm");
    }

    public void clickconfirm() throws Exception {
        Utils.scrollToBottomOfPage();
        Thread.sleep(2500);
        clickOnElement(confirm, "confirm");
    }


}
