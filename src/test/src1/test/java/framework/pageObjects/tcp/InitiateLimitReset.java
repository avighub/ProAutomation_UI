package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DateAndTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class InitiateLimitReset extends PageInit {

    /*
     * Page Objects
     */
    @FindBy(id = "enumId")
    public WebElement userType;

    @FindBy(id = "msisdn")
    public WebElement mobileNumber;

    @FindBy(id = "tcpInitiateLimitReset_submitList()_extensionType")
    public WebElement extensionType;

    @FindBy(id = "tcpInitiateLimitReset_submitList()_limitType")
    public WebElement limitType;

    @FindBy(id = "liquidationFrequency")
    public WebElement frequencyType;

    @FindBy(id = "bearerId")
    public WebElement bearerType;
    @FindBy(xpath = "//*[@id='tcpInitiateLimitReset_submitList()']/table/tbody/tr[3]/td[2]/span/img")
    public WebElement serviceType;
    @FindBy(name = "expiryDate")
    private WebElement txtExpiryDate;
    @FindBy(id = "tcpInitiateLimitReset_submitList()_button_submit")
    private WebElement submit;


    public InitiateLimitReset(ExtentTest t1) {
        super(t1);
    }

    public static InitiateLimitReset init(ExtentTest t1) {
        return new InitiateLimitReset(t1);
    }

    public void selectFromDojoCombo(String option) throws Exception {
        WebElement result = driver.findElement(By.xpath("//*[@id='page-home']/span/div[@resultvalue='" + option + "']"));
        Thread.sleep(1000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", result);
    }

    public void selectAllService() throws Exception {

        driver.findElement(By.xpath(".//*[@id='tcpInitiateLimitReset_submitList()']/table/tbody/tr[3]/td[2]/span/input[3]")).sendKeys("ALL");
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='page-home']/span/div[@resultname='ALL']")))).click();

        pageInfo.info("Selected All Service ");
    }

    public void clickImage(WebElement elem) throws Exception {
        elem.click();
        Thread.sleep(500);
        elem.click();
        Thread.sleep(500);
        elem.click();
    }

    public void selectUserType(String user) throws Exception {
        Select sel = new Select(userType);
        sel.selectByVisibleText(user);
        pageInfo.info("Selected UserType: " + user);
    }

    public void enterMobilerNumber(String msisdn) throws Exception {
        mobileNumber.sendKeys(msisdn);
        pageInfo.info("Entered Mobile Number: " + msisdn);
    }

    public void selectServiceName(String service) throws Exception {
        clickImage(serviceType);
        selectFromDojoCombo(service);
        Thread.sleep(2000);
        pageInfo.info("Select Service Type: " + service);
    }

    public void selectExtension(String extension) throws Exception {
        Select sel = new Select(extensionType);
        sel.selectByVisibleText(extension);
        pageInfo.info("Selected extensionType: " + extension);
    }

    public void selectLimit(String limit) throws Exception {
        Select sel = new Select(limitType);
        sel.selectByVisibleText(limit);
        pageInfo.info("Selected limitType: " + limit);
    }

    public void selectFrequency(String frequency) throws Exception {
        Select sel = new Select(frequencyType);
        sel.selectByVisibleText(frequency);
        pageInfo.info("Selected frequencyType: " + frequency);
    }

    public void selectBearer(String bearer) throws Exception {
        Select sel = new Select(bearerType);
        sel.selectByVisibleText(bearer);
        pageInfo.info("Selected bearerType: " + bearer);
    }

    public void selectExpiryDate(int day) throws Exception {
        String sDate = new DateAndTime().getDate(day);
        setDate(txtExpiryDate, sDate, "txtExpiryDate");
    }

    public void clickOnSubmit() throws Exception {
        submit.click();
        pageInfo.info("Submit Button is Clicked");
    }

    /**
     * Navigate to Initiate Limit Reset
     *
     * @throws Exception
     */


    public void navInitiateLimitReset() throws Exception {
        navigateTo("TCPROFILE_ALL", "TCPROFILE_TCP_INITLIMITRESET", "Initiate Limit Reset");
    }
}


