package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by navin.pramanik on 12/12/2018.
 */
public class CopyCustomerTCPPage extends PageInit {

    @FindBy(id = "domain_copy")
    private WebElement DomainName;
    @FindBy(id = "category_copy")
    private WebElement CategoryName;
    @FindBy(id = "registrationType_copy")
    private WebElement RegType;
    @FindBy(id = "profile_name_copy")
    private WebElement profileNameTBox;
    @FindBy(id = "description_copy")
    private WebElement descriptionTbox;
    @FindBy(id = "copy_customer")
    private WebElement copyButton;
    @FindBy(xpath = "//*[@class='error_message']/li")
    private WebElement errorMessage;

    public CopyCustomerTCPPage(ExtentTest t1) {
        super(t1);
    }

    /**
     * Set the profile Name
     *
     * @param name
     */
    public CopyCustomerTCPPage setProfileName(String name) {
        setText(profileNameTBox, name, "ProfileName");
        return this;
    }

    /**
     * Set Description
     *
     * @param desc
     */
    public CopyCustomerTCPPage setDescription(String desc) {
        setText(descriptionTbox, desc, "Description");
        return this;
    }

    /**
     * Select Domain Name
     *
     * @param domainName
     */
    public CopyCustomerTCPPage selectDomainName(String domainName) throws InterruptedException {
        selectVisibleText(DomainName, domainName, "DomainName");
        return this;
    }

    /**
     * Select Category Name
     *
     * @param catName
     */
    public CopyCustomerTCPPage selectCategoryName(String catName) throws InterruptedException {
        selectVisibleText(CategoryName, catName, "CategoryName");
        return this;
    }

    public CopyCustomerTCPPage selectRegType(String regType) throws Exception {
        selectVisibleText(RegType, regType, "RegType");
        return this;
    }

    public CopyCustomerTCPPage selectRegTypeByValue(String regTypeVal) throws Exception {
        selectValue(RegType, regTypeVal, "RegType");
        return this;
    }

    public CopyCustomerTCPPage clickOnCopyButton() {
        clickOnElement(copyButton, "Copy Button");
        return this;
    }

    public String getModalErrorMessage() {
        if (errorMessage.isDisplayed()) {
            return errorMessage.getText();
        } else {
            return null;
        }
    }

}
