package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.InstrumentTCP;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.java2d.pipe.DrawImage;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class ApproveInstrumentTCP_pg1 extends PageInit {

    /*
     * Page Objects
	 */
    @FindBy(id = "tcp0")
    public WebElement firstTcp;

    @FindBy(css = ".approveButton.toBeDisabled")
    public WebElement btnApprove;


    @FindBy(css = ".rejectButton.toBeDisabled")
    public WebElement btnReject;


    public ApproveInstrumentTCP_pg1(ExtentTest t1) {
        super(t1);
    }

    public static ApproveInstrumentTCP_pg1 init(ExtentTest t1) {
        return new ApproveInstrumentTCP_pg1(t1);
    }

    public void approveTCP() throws Exception {
        Utils.scrollToBottomOfPage();
        Thread.sleep(2000);
        clickOnElement(btnApprove, "Approve TCP");
        Thread.sleep(2000);
    }

    public void rejectTCP() throws Exception {
        Utils.scrollToBottomOfPage();
        Thread.sleep(2000);
        clickOnElement(btnReject, "Reject TCP");
        Thread.sleep(4000);
    }

    public void selectTcpToApprove(String tcpName) throws Exception {
        WebElement approveLink = driver.findElement(By.xpath("//tr/td[contains(text(), '" + tcpName + "')]/ancestor::tr[1]/td/span/a"));
        clickOnElement(approveLink, "Select to Approve {" + tcpName + "}");
        Thread.sleep(8000);
    }

    public void selectFirstTcpForApproval() {
        clickOnElement(firstTcp, "First TCP present on the Approval Page");
    }

    /**
     * Assert Customer TCP is successfully Approved
     *
     * @param tcp
     * @return
     * @throws Exception
     */
    public boolean assertTCPApproved(InstrumentTCP tcp) throws Exception {
        String expected = MessageReader.getDynamicMessage("tcprofile.instrument.success.addApproved", tcp.ProfileName,
                tcp.DomainName, tcp.CategoryName);
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 100);
        String actual = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("successMessage"))).getText();
        if (Assertion.verifyContains(actual, expected, "Verify if Instrument TCP Successfully Approved", pageInfo)) {
            return true;
        }
        return false;
    }

    public String getSuccessMessage() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("successMessage"))).getText();
    }

    /**
     * Navigate to Instrument TCP approval
     *
     * @throws Exception
     */


    public void navInstrumentTCPApproval() throws Exception {
        navigateTo("TCPROFILE_ALL", "TCPROFILE_TCP_INSTRAPPR", "Instrument TCP Approval");
    }

    public void clickOnApprovalElement(WebElement elem) {
        clickOnElement(elem, "Click for Approval");
    }
}
