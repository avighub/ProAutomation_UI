package framework.pageObjects.mfsProviderBankTypeMaster;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by Dalia on 26-05-2017.
 */
public class AddBank_Page1 extends PageInit {

    @FindBy(name = "action:MfsBankMapping_AddBankMapping")
    WebElement submit;
    //Page Objects
    @FindBy(id = "mfsProviderList")
    private WebElement providerName;
    @FindBy(name = "defaultBankType")
    private WebElement defaultBank;
    @FindBy(id = "MfsBankMapping_inputBank_check")
    private WebElement checkBox;

    public AddBank_Page1(ExtentTest t1) {
        super(t1);
    }

    public static AddBank_Page1 init(ExtentTest t1) {
        return new AddBank_Page1(t1);
    }

    /**
     * Navigate to MFS Provider Bank Type Master > Add Bank page
     *
     * @throws Exception
     */
    public void navMFSProviderBankTypeMaster() throws Exception {
        navigateTo("MFSBTM_ALL", "MFSBTM_MFSBTM01DM", "Bank Type Master > Add Bank Pages");
    }

    //Select Provider
    public void selectProviderName(String text) throws Exception {
        selectVisibleText(providerName, text, "providerName");
    }

    //Select Default Bank
    public void selectDefaultBankName(String bankName) throws Exception {
        selectVisibleText(defaultBank, bankName, "defaultBank");
    }

    //Click Submit button
    public void clickSubmit() {
        clickOnElement(submit, "Submit");
    }

    public void verifyBankNames(String providerName) throws Exception {
        List<String> banks;
        framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1 page1 = framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1.init(pageInfo);
        page1.navMFSProviderBankTypeMaster();
        page1.selectProviderName(providerName);
        banks = MobiquityGUIQueries.getLinkedBankNames(DataFactory.getProviderId(providerName));
        defaultBank.click();
        Select webBanks = new Select(defaultBank);

        List<WebElement> webBanksOptions = webBanks.getOptions();

        for (String bank : banks) {
            for (WebElement webBank : webBanksOptions) {
                if (bank.equalsIgnoreCase(webBank.getText())) {
                    pageInfo.pass("'" + webBank.getText() + "'" + " Bank Name Verified Successfully.");
                }
            }
        }
        Utils.captureScreen(pageInfo);

    }

    public void unCheckBank(String bankID) {
        if (driver.findElement(By.xpath(".//*[@id='MfsBankMapping_modifyBankMapping_checkBank' and @value = '" + bankID + "']")).isSelected()) {
            driver.findElement(By.xpath(".//*[@id='MfsBankMapping_modifyBankMapping_checkBank' and @value = '" + bankID + "']")).click();
        }
        pageInfo.info("Bank Name is unchecked.");
    }
}
