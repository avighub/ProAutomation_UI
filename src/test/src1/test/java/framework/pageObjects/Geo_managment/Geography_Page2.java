package framework.pageObjects.Geo_managment;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;

public class Geography_Page2 extends PageInit {


    //Page Objects

    @FindBy(id = "geoMgmt_addModifyDelete_btnAdd")
    private WebElement Add;

    @FindBy(id = "geoAdd_save_grphDomainCode")
    private WebElement geocode;

    @FindBy(id = "geoAdd_save_grphDomainName")
    private WebElement geoname;

    @FindBy(id = "geoAdd_save_grphDomainShortName")
    private WebElement shortname;

    @FindBy(id = "geoAdd_save_description")
    private WebElement descr;

    @FindBy(id = "geoAdd_save_btnAdd")
    private WebElement Add2;

    @FindBy(id = "geoAdd_confirm_btnAdd")
    private WebElement confirm;

    @FindBy(xpath = "//input[@value=\"Update\"][@id=\"geoMgmt_addModifyDelete_btnModify\"]")
    private WebElement Update;

    @FindBy(xpath = "//input[@value=\"Delete\"][@id=\"geoMgmt_addModifyDelete_btnModify\"]")
    private WebElement delete;

    @FindBy(id = "geoMgmt_addModifyDelete_btnBack")
    private WebElement back;

    public Geography_Page2(ExtentTest t1) {
        super(t1);
    }

    public static Geography_Page2 init(ExtentTest t1) {
        return new Geography_Page2(t1);
    }

    public Geography_Page2 selectRadio(String name) {
        wait.until(ExpectedConditions.elementToBeClickable(
                driver.findElement(By.xpath("//tr/td[contains(text(),'" + name + "')]/ancestor::tr[1]/td/input[@type='radio']"))))
                .click();
        pageInfo.info("Check radio button with name: " + name);
        return this;
    }

    public Geography_Page2 clickDeleteButton() {
        clickOnElement(delete, "delete");
        return this;
    }

    public void verifyIfBottonExists(String btnNAme) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.cssSelector("input[type='submit'][value='" + btnNAme + "']")).isDisplayed()) {
            pageInfo.pass("Successfuly verified the " + btnNAme + " button is present");
        } else {
            pageInfo.fail("Failed to verify the " + btnNAme + " button is NOT present");
            Utils.captureScreen(pageInfo);
        }
    }

    public boolean addVisible() {
        return Add.isDisplayed();
    }

    public boolean updateVisible() {
        return Update.isDisplayed();
    }

    public boolean deleteVisible() {
        return delete.isDisplayed();
    }

    public boolean backVisible() {
        return back.isDisplayed();
    }


}
