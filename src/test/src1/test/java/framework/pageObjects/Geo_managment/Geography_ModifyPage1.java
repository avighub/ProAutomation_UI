package framework.pageObjects.Geo_managment;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class Geography_ModifyPage1 extends PageInit {

    @FindBy(id = "geoModify_save_status")
    private WebElement status;
    @FindBy(id = "geoModify_save_btnModify")
    private WebElement modifyButton;
    @FindBy(id = "geoModify_confirm_btnModify")
    private WebElement confirm;
    @FindBy(id = "geoMgmt_addModifyDelete_btnModify")
    private WebElement update;
    @FindBy(xpath = "//input[@name=\"grphDomainName\"]")
    private WebElement GeoDomName;
    @FindBy(xpath = "//input[@name=\"grphDomainShortName\"]")
    private WebElement GeoShortName;
    @FindBy(id = "geoModify_save_description")
    private WebElement GeoDesc;
    @FindBy(id = "geoModify_confirm_btnModifyBack")
    private WebElement updateBackConfirm;
    @FindBy(id = "geoModify_confirm_btnCncl")
    private WebElement updateCancel;
    @FindBy(id = "geoModify_confirm_grphDomainName")
    private WebElement nameConfirm;

    //Confirm Page Elements
    @FindBy(id = "geoModify_confirm_grphDomainName")
    private WebElement shortNameConfirm;
    @FindBy(id = "geoModify_confirm_grphDomainName")
    private WebElement statusConfirm;

    public Geography_ModifyPage1(ExtentTest t1) {
        super(t1);
    }

    public Geography_ModifyPage1 clickUpdateButton() {
        clickOnElement(update, "Click on Update button");
        return this;
    }

    public Geography_ModifyPage1 selectgeo(String place) {
        String xpath = "//tr/td[contains(text(),'" + place + "')]/ancestor::tr[1]/td/input[@type='radio']";
        WebElement radio = driver.findElement(By.xpath(xpath));
        radio.click();
        pageInfo.info("Select radio button");
        return this;

    }

    public Geography_ModifyPage1 selectStatus(String stat) {
        Select option = new Select(status);
        option.selectByVisibleText(stat);
        pageInfo.info("Select Status" + stat);
        return this;
    }

    public Geography_ModifyPage1 clickModify() {
        clickOnElement(modifyButton, "Click on Modify Button");
        return this;
    }

    public Geography_ModifyPage1 clickConfirmButton() {
        if (ConfigInput.isConfirm) {
            clickOnElement(confirm, "Confirm Button");
        }
        return this;
    }

    public Geography_ModifyPage1 setDomName(String text) throws Exception {
        setText(GeoDomName, text, "set Domain Name: " + text);
        return this;
    }


    public Geography_ModifyPage1 setDomShortName(String text) throws Exception {
        setText(GeoShortName, text, "set Domain Short Name: " + text);
        return this;
    }

    public Geography_ModifyPage1 setGeoDesc(String text) throws Exception {
        setText(GeoDesc, text, "set Description: " + text);
        return this;
    }

    public Geography_ModifyPage1 clickBackUpdateConfirm() {
        clickOnElement(updateBackConfirm, "Click on the Back button");
        return this;
    }

    public boolean checkGeoUpdate() {
        if (modifyButton.isDisplayed()) {
            pageInfo.info("Back to Modify Geographical Domain Page");
            return true;
        }
        return false;
    }

    public boolean backVisible() {
        return updateBackConfirm.isDisplayed();
    }

    public boolean confirmVisible() {
        return confirm.isDisplayed();
    }

    public boolean cancelVisible() {
        return updateCancel.isDisplayed();
    }

    public boolean fieldCheck(String name, String sName, String Desc) throws InterruptedException {
        if (GeoDomName.getText().equalsIgnoreCase(name)) {
            pageInfo.info("Domain Name Entered " + name);
            if (GeoShortName.getText().equalsIgnoreCase(sName)) {
                pageInfo.info("Short Name Entered " + sName);
                if (GeoDesc.getText().equalsIgnoreCase(Desc)) {
                    pageInfo.info("Description Entered " + Desc);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean nameVisible() {
        return fl.elementIsDisplayed(nameConfirm);
    }

    public boolean shortNameVisible() {
        return fl.elementIsDisplayed(shortNameConfirm);
    }

    public boolean statusVisible() {
        return fl.elementIsDisplayed(statusConfirm);
    }

    public String getGeoDomainNameFromTbox() {
        return GeoDomName.getAttribute("value");
    }

    public String getGeoShortNameFromTbox() {
        return GeoShortName.getAttribute("value");
    }

    public String getGeoDescFromTbox() {
        return GeoDesc.getAttribute("value");
    }


}
