package framework.pageObjects.transferRule;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.ServiceList;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;


public class TransferRule_Pg1 extends PageInit{

    @FindBy(id = "serviceTypeId")
    WebElement ogSelServiceName;

    @FindBy(id = "payerProviderId")
    WebElement ogSelFromProvider;

    @FindBy(id = "payerDomainCode")
    WebElement ogSelPayerDomain;

    @FindBy(id = "payerPaymentInstrumentId")
    WebElement ogSelPayerPayInstrument;

    @FindBy(id = "payerLinkedWalletBankId")
    WebElement ogSelPayerPayInstrumentType;

    @FindBy(id = "payeeProviderId")
    WebElement ogSelPayeeProvider;

    @FindBy(id = "payeeDomainCode")
    WebElement ogSelPayeeDomain;

    @FindBy(id = "payeePaymentInstrumentId")
    WebElement ogSelPayeeInstrument;

    @FindBy(id = "payeeLinkedWalletBankId")
    WebElement ogSelPayeeInstrumenttype;

    public void setOgSelServiceName(String ogSelServiceName) throws Exception {
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectValue(this.ogSelServiceName, ogSelServiceName, "ogSelServiceName");
    }

    public void setOgSelPayerProvider(String ogSelFromProvider) throws Exception{
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectVisibleText(this.ogSelFromProvider, ogSelFromProvider, "ogSelFromProvider");
    }

    public void setOgSelPayerDomain(String ogSelPayerDomain)throws Exception {
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectVisibleText(this.ogSelPayerDomain, ogSelPayerDomain, "ogSelPayerDomain");
    }

    public void setOgSelPayerPayInstrument(String ogSelPayerPayInstrument) throws Exception{
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectVisibleText(this.ogSelPayerPayInstrument, ogSelPayerPayInstrument, "ogSelPayerPayInstrument");
    }

    public void setOgSelPayerPayInstrumentType(String ogSelPayerPayInstrumentType) throws Exception{
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectVisibleText(this.ogSelPayerPayInstrumentType, ogSelPayerPayInstrumentType, "ogSelPayerPayInstrumentType");
    }

    public void setOgSelPayeeProvider(String ogSelPayeeProvider) throws Exception{
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectVisibleText(this.ogSelPayeeProvider, ogSelPayeeProvider, "ogSelPayeeProvider");
    }

    public void setOgSelPayeeDomain(String ogSelPayeeDomain) throws Exception{
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectVisibleText(this.ogSelPayeeDomain, ogSelPayeeDomain, "ogSelPayeeDomain");
    }

    public void setOgSelPayeeInstrument(String ogSelPayeeInstrument) throws Exception{
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectVisibleText(this.ogSelPayeeInstrument, ogSelPayeeInstrument, "ogSelPayeeInstrument");
    }

    public void setOgSelPayeeInstrumenttype(String ogSelPayeeInstrumenttype) throws Exception{
        Thread.sleep(Constants.THREAD_SLEEP_1000);
        selectVisibleText(this.ogSelPayeeInstrumenttype, ogSelPayeeInstrumenttype, "ogSelPayeeInstrumenttype");
    }

    /*
         * Page Objects
         */
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[1]/td[2]/span/input[3]")
    WebElement ServiceType;

    // Page objects
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[2]/td[2]/span/input[3]")
    WebElement SenderProvider;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[3]/td[2]/span/input[3]")
    WebElement SenderDomain;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[4]/td[2]/span/input[3]")
    WebElement SenderPayInst;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[5]/td[2]/span/input[3]")
    WebElement SenderPayInstType;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[6]/td[2]/span/input[3]")
    WebElement ReceiverProvider;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[7]/td[2]/span/input[3]")
    WebElement ReceiverDomain;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[8]/td[2]/span/input[3]")
    WebElement ReceiverInst;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[9]/td[2]/span/input[3]")
    WebElement ReceiverInstType;
    @FindBy(id = "selectForm_button_submit")
    WebElement Submit;

    public TransferRule_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static TransferRule_Pg1 init(ExtentTest t1) {
        return new TransferRule_Pg1(t1);
    }

    /**
     * Navigate to Add TransferRule
     *
     * @throws Exception
     */
    public void navAddTransferRule() throws Exception {
        navigateTo("TRULES_ALL", "TRULES_T_RULES", "Transfer Rule");
    }


    /**
     * Submit The form
     */
    public void submit() {
        clickOnElement(Submit, "Subcmit");
    }

    /**
     * Select Service Type
     *
     * @param service
     * @throws Exception
     */
    public void selectServiceName(ServiceList service) throws Exception {
        String searchString = service.ServiceName.substring(0, 1);
        ServiceType.sendKeys(searchString);
        driver.findElement(By.xpath("//*[@id='page-home']/span/div[@resultvalue='" + service.ServcieType + "']")).click();
        Thread.sleep(2500);
        pageInfo.info("Select Service Type: " + service.ServiceName);
    }

    public void selectFromDojoCombo(WebElement elem, String value, String index) throws InterruptedException {

        Thread.sleep(2000);
        String searchString1 = value.substring(0, 1);
        String searchString2 = value.substring(1, 2);
        elem.sendKeys(searchString1);
        Thread.sleep(500);
        elem.sendKeys(searchString2);
        Thread.sleep(500);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//*[@id='page-home']/span[" + index + "]/div[@resultname='" + value + "']"))).click();
        Thread.sleep(4000);
    }

    /**
     * Select Provider
     *
     * @param provider
     * @return
     * @throws Exception
     */
    public boolean selectSenderProvider(String provider) throws Exception {
        try {
            selectFromDojoCombo(SenderProvider, provider, "2");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select from Domain
     *
     * @param domain
     * @return
     * @throws Exception
     */
    public boolean selectSenderDomain(String domain) throws Exception {
        try {
            selectFromDojoCombo(SenderDomain, domain, "3");
            pageInfo.info("Select Sender Domain: " + domain);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select Sender Payment Instrument
     *
     * @param payInst
     * @throws Exception
     */
    public void selectSenderPayInstrument(String payInst) throws Exception {
        Thread.sleep(3000);
        selectFromDojoCombo(SenderPayInst, payInst, "4");
        pageInfo.info("Select Sender Payment Instrument: " + payInst);
    }

    /**
     * Select Snder Payment Instrument Type
     *
     * @param payInstType
     * @return
     * @throws Exception
     */
    public boolean selectSenderPayInstrumentType(String payInstType) throws Exception {
        try {
            selectFromDojoCombo(SenderPayInstType, payInstType, "5");
            pageInfo.info("Select Sender Payment Instrument Type: " + payInstType);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select Receiver Currency Provider
     *
     * @param provider
     * @throws Exception
     */
    public void selectReceiverProvider(String provider) throws Exception {
        selectFromDojoCombo(ReceiverProvider, provider, "6");
        pageInfo.info("Select Receiver Provider: " + provider);
    }

    /**
     * Select Receiver Domain
     *
     * @param domain
     * @return
     * @throws Exception
     */
    public boolean selectReceiverDomain(String domain) throws Exception {
        try {
            selectFromDojoCombo(ReceiverDomain, domain, "7");
            pageInfo.info("Select Sender Domain: " + domain);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    /**
     * Select Receiver Payment Instrument
     *
     * @param payInst
     * @throws Exception
     */
    public void selectReceiverPayInstrument(String payInst) throws Exception {
        selectFromDojoCombo(ReceiverInst, payInst, "8");
        pageInfo.info("Select Receiver Payment Instrument: " + payInst);
    }

    /**
     * Select Receiver Payment Instrument Type
     *
     * @param payInstType
     * @return
     * @throws Exception
     */
    public boolean selectReceiverPayInstrumentType(String payInstType) throws Exception {
        try {
            selectFromDojoCombo(ReceiverInstType, payInstType, "9");
            pageInfo.info("Select Receiver Payment Instrument Type: " + payInstType);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select Div from the Combo Box
     *
     * @param option
     * @throws Exception
     * @deprecated
     */
    public void selectFromDojoCombo(String option, String index) throws Exception {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//*[@id='page-home']/span[" + index + "]/div[@resultname='" + option + "']"))).click();
        Thread.sleep(1200);
    }

    public void selectFromDojoComboByValue(String option, String index) throws Exception {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//*[@id='page-home']/span[" + index + "]/div[@resultvalue='" + option + "']"))).click();
        Thread.sleep(1200);
    }

    /**
     * Get all options of a combo box
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllServiceType() throws Exception {
        ServiceType.click();
        List<String> list = getAllOptions("1");
        Thread.sleep(300);
        return list;
    }

    /**
     * Get all options of a combo box
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllSenderProvider() throws Exception {
        SenderProvider.click();
        List<String> list = getAllOptions("2");
        Thread.sleep(300);
        return list;
    }

    /**
     * Get all options of a combo box
     *
     * @return
     */
    public List<String> getAllSenderDomain() {
        SenderDomain.click();
        return getAllOptions("3");
    }

    /**
     * Select First Sender Domain from the Dropdown Options
     *
     * @throws Exception
     */
    public void selectFirstSenderDomain() throws Exception {
        SenderDomain.click();
        List<String> list = getAllOptions("3");
        selectFromDojoCombo(list.get(0), "3");
    }

    /**
     * Select First Receiver Domain from the Dropdown Options
     *
     * @throws Exception
     */
    public void selectFirstReceiverDomain() throws Exception {
        ReceiverDomain.click();
        List<String> list = getAllOptions("7");
        selectFromDojoCombo(list.get(0), "7");
    }


    /**
     * Select First Payment Instrument Type available in UI for Sender
     *
     * @throws Exception
     */
    public void selectFirstSenderPayInstType() throws Exception {
        SenderPayInstType.click();
        List<String> list = getAllOptions("5");
        selectFromDojoCombo(list.get(0), "5");
    }

    /**
     * Select First Payment Instrument Type available in UI for Receiver
     *
     * @throws Exception
     */
    public void selectFirstreceiverPayInstType() throws Exception {
        ReceiverInstType.click();
        List<String> list = getAllOptions("9");
        selectFromDojoCombo(list.get(0), "9");
    }

    /**
     * Get all options of a combo box
     *
     * @return
     */
    public List<String> getAllSenderPayInstrument() {
        SenderPayInst.click();
        return getAllOptions("4");
    }

    /**
     * Get all options of a combo box
     *
     * @return
     */
    public List<String> getAllSenderPayInstrumentType() {
        SenderPayInstType.click();
        return getAllOptions("5");
    }

    /**
     * Get all options of a combo box
     *
     * @return
     */
    public List<String> getAllReceiverProvider() {
        ReceiverProvider.click();
        return getAllOptions("6");
    }

    /**
     * Get All receiver Domain
     *
     * @return
     */
    public List<String> getAllReceiverDomain() {
        ReceiverDomain.click();
        return getAllOptions("7");
    }

    /**
     * Get All receiver Domain
     *
     * @return
     */
    public List<String> getAllReceiverPayInstrument() {
        ReceiverInst.click();
        return getAllOptions("8");
    }

    /**
     * Get All receiver Domain
     *
     * @return
     */
    public List<String> getAllReceiverPayInstrumentType() {
        ReceiverInstType.click();
        return getAllOptions("9");
    }

    /**
     * Get All Options
     *
     * @return
     */
    public List<String> getAllOptions(String index) {
        List<String> str = new ArrayList<String>();
        try {
            List<WebElement> subLink = driver.findElements(By.xpath("//*[@id='page-home']/span[" + index + "]/div"));

            for (WebElement sl : subLink) {
                str.add(sl.getAttribute("resultname"));
            }

            return str;
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public void selectfirstall() throws Exception {
        ServiceType.click();
        List<String> list = getAllOptions("1");
        selectFromDojoCombo(list.get(0), "1");
        SenderProvider.click();
        list = getAllOptions("2");
        selectFromDojoCombo(list.get(0), "2");
        SenderDomain.click();
        list = getAllOptions("3");
        selectFromDojoCombo(list.get(0), "3");
        SenderPayInst.click();
        list = getAllOptions("4");
        selectFromDojoCombo(list.get(0), "4");
        SenderPayInstType.click();
        list = getAllOptions("5");
        selectFromDojoCombo(list.get(0), "5");
        ReceiverProvider.click();
        list = getAllOptions("6");
        selectFromDojoCombo(list.get(0), "6");
        ReceiverDomain.click();
        list = getAllOptions("7");
        selectFromDojoCombo(list.get(0), "7");
        ReceiverInst.click();
        list = getAllOptions("8");
        selectFromDojoCombo(list.get(0), "8");
        ReceiverInstType.click();
        list = getAllOptions("9");
        selectFromDojoCombo(list.get(0), "9");
        submit();
    }


    public void selectfirstAll(String service, String payInst) throws Exception {
        List<String> list;
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[1]/td[2]/span/img")).click();
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        //list = getAllOptions("1");
        selectFromDojoComboByValue(service, "1");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[2]/td[2]/span/img")).click();
        list = getAllOptions("2");
        selectFromDojoCombo(list.get(0), "2");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[3]/td[2]/span/img")).click();
        list = getAllOptions("3");
        selectFromDojoCombo(list.get(0), "3");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[4]/td[2]/span/img")).click();
        list = getAllOptions("4");
        selectFromDojoCombo(list.get(0), "4");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[5]/td[2]/span/img")).click();
        list = getAllOptions("5");
        selectFromDojoComboByValue(payInst, "5");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[6]/td[2]/span/img")).click();
        list = getAllOptions("6");
        selectFromDojoCombo(list.get(0), "6");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[7]/td[2]/span/img")).click();
        list = getAllOptions("7");
        selectFromDojoCombo(list.get(0), "7");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[8]/td[2]/span/img")).click();
        list = getAllOptions("8");
        selectFromDojoCombo(list.get(0), "8");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[9]/td[2]/span/img")).click();
        list = getAllOptions("9");
        selectFromDojoComboByValue(payInst, "9");
        submit();
    }

    /**
     * Click on image
     *
     * @param elem
     * @throws Exception
     */
    public void clickImage(WebElement elem) throws Exception {
        elem.click();
        Thread.sleep(500);
        elem.click();
        Thread.sleep(500);
        elem.click();
    }

    public void checkServiceName(ServiceList service) throws Exception {
        String searchString = service.ServiceName.substring(0, 1);
        ServiceType.sendKeys(searchString);
        try {
            driver.findElement(By.xpath("//*[@id='page-home']/span/div[@resultvalue='" + service.ServcieType + "']"));
            Thread.sleep(2500);
            pageInfo.fail("Service Type : " + service.ServiceName + " is displayed");
        } catch (NoSuchElementException n) {
            pageInfo.pass("Service Type : " + service.ServiceName + " is not displayed");
        }
    }
}
