package framework.pageObjects.transferRule;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class TransferRule_Pg3 extends PageInit {


    // Page objects
    @FindBy(id = "trRule_confirmCoU_statusId")
    private WebElement Status;

    // Page objects
    @FindBy(id = "trRule_confirmCoU_transferType")
    private WebElement TransferType;

    // Page objects
    @FindBy(id = "trRule_confirmCoU_grphDomainCode")
    private WebElement GeoDomain;

    // Page objects
    @FindBy(id = "trRule_confirmCoU_directTransferAllowedtrue")
    private WebElement DirectTransferAllowed;

    // Page objects
    @FindBy(id = "trRule_confirmCoU_controlledTransferLevel")
    private WebElement ControlTrLevel;

    // Page objects
    @FindBy(id = "trRule_confirmCoU_fixedTransferLevel")
    private WebElement FixedTxLevel;

    @FindBy(id = "trRule_confirmCoU_button_submit")
    private WebElement Submit;

    @FindBy(id = "trSelect_modifyTransferRule_buttonTR")
    private WebElement Confirm;

    @FindBy(className = "actionMessage")
    private WebElement ActionMessage;

    @FindBy(className = "error_message")
    private WebElement ErrorMessage;

    @FindBy(id = "trSelect_modifyTransferRule_button_back")
    private WebElement BackButton;


    /**
     * Assert Creation
     */
    /*public void assertCreation(){
        fl.waitWebElementVisible(ActionMessage);
		Assert.assertTrue(ActionMessage.getText().contains(en_US.TRANSFER_RULE_SUCCESSFULLY_ADDED));
	}*/
    public TransferRule_Pg3(ExtentTest t1) {
        super(t1);
    }

    public static TransferRule_Pg3 init(ExtentTest t1) {
        return new TransferRule_Pg3(t1);
    }

    /**
     * Set Status
     *
     * @param status
     */
    public void selectStatus(String status) {
        selectVisibleText(Status, status, "Status");
    }

    public TransferRule_Pg3 selectStatusByVal(String status) {
        selectValue(Status, status, "Status");
        return this;
    }

    /**
     * Set Transfer Type
     *
     * @param type
     */
    public void selectTransferType(String type) {
        selectVisibleText(TransferType, type, "TransferType");
    }

    /**
     * Set Geographical Domain
     *
     * @param geoDomain
     */
    public void selectGeoDomain(String geoDomain) {
        selectVisibleText(GeoDomain, geoDomain, "GeoDomain");
    }

    /**
     * set Direct Transfer
     */
    public void setDirectTransfer() {
        clickOnElement(DirectTransferAllowed, "DirectTransferAllowed");
    }

    /**
     * Set Contol Level
     *
     * @param level
     */
    public void selectControlTxLevel(String level) {
        //selectVisibleText(ControlTrLevel, level, "ControlTrLevel");
        // for some services, System is not populated as transfer level, and Tf level is already selected
        try{
            new Select(ControlTrLevel).selectByVisibleText(level);
            pageInfo.info("Select Control Transfer Level:" + level);
        }catch (Exception e){
            ;
        }
    }

    /**
     * Set Fixed Transfer Level
     *
     * @param level
     */
    public void selectFixedTxLevel(String level) {
        selectVisibleText(FixedTxLevel, level, "FixedTxLevel");

    }

    public void selectAlldata() {
        Select Option1 = new Select(Status);
        Select Option2 = new Select(TransferType);
        Select Option3 = new Select(GeoDomain);
        Select Option4 = new Select(ControlTrLevel);
        Select Option5 = new Select(FixedTxLevel);
        Option1.selectByIndex(1);
        Option2.selectByIndex(1);
        Option3.selectByIndex(1);
        Option4.selectByIndex(1);
        Option5.selectByIndex(1);

    }

    /**
     * Submit
     */
    public TransferRule_Pg3 submit() {
        clickOnElement(Submit, "Submit");
        return this;
    }

    /**
     * Confirm action
     *
     * @throws Exception
     */
    public TransferRule_Pg3 confirm() throws Exception {
        clickOnElement(Confirm, "Confirm");
        return this;
    }

    /**
     * Get Action Message
     */
    public String getActionMessage() {
        try {
            return ActionMessage.getText();
        } catch (NoSuchElementException e) {
            return ErrorMessage.getText();
        }
    }

    public String getServiceNameFromViewPage() {
        WebElement label = DriverFactory.getDriver().findElement(By.id("trSelect_modifyTransferRule_serviceName"));
        return label.getText();
    }

    public String getCategoryNameFromViewPage() {
        WebElement label = DriverFactory.getDriver().findElement(By.id("trSelect_modifyTransferRule_payeeCategoryName"));
        return label.getText();
    }

    public String getFromCategoryNameFromViewPage() {
        WebElement label = DriverFactory.getDriver().findElement(By.id("trSelect_modifyTransferRule_payerCategoryName"));
        return label.getText();
    }

    public String getFromGradeFromViewPage() {
        WebElement label = DriverFactory.getDriver().findElement(By.id("trSelect_modifyTransferRule_payerGradeName"));
        return label.getText();
    }

    public String getToGradeFromViewPage() {
        WebElement label = DriverFactory.getDriver().findElement(By.id("trSelect_modifyTransferRule_payeeGradeName"));
        return label.getText();
    }
}
