package framework.pageObjects.bulkPayoutTool;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BulkPayoutApprove_page1 extends PageInit {

    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/

    @FindBy(id = "rejectId")
    WebElement rejectComment;

    @FindBy(id = "bulkApproveButt")
    WebElement approveButton;

    @FindBy(xpath = "//select")
    WebElement selService;

    /*****************************************************************************************************
     * ############################ Page Objects for New Bulk Payout Approval #############################
     *****************************************************************************************************/

    @FindBy(xpath = "//span[.='Newest']")
    WebElement sortNewest;

    @FindBy(xpath = "(//span[.='Initiated by'])[1]")
    WebElement request;

    @FindBy(xpath = "//a[.='APPROVE']")
    WebElement approveBtn;

    @FindBy(xpath = "//a[.='REJECT']")
    WebElement rejectBtn;

    @FindBy(xpath = "(//div[@class='col s6 alert alert-success padd_tb']//span)[2]")
    WebElement message;

    @FindBy(id = "Liquidationbank")
    WebElement selectBank;

    @FindBy(xpath = "//div[@class='col s6 alert alert-info padd_tb']//span[1]")
    WebElement message1;

    public BulkPayoutApprove_page1(ExtentTest t1) {
        super(t1);
    }

    public static BulkPayoutApprove_page1 init(ExtentTest t1) {
        return new BulkPayoutApprove_page1(t1);
    }

    /***************************************************************************
     * ############################ Page Operations ############################
     ***************************************************************************/

    public void navigateToLink() throws Exception {
        navigateTo("BULKPAY_ALL", "BULKPAY_BULKAPPROVE", "Buld Payout Tool");
    }

    public void rejectComment_SetText(String text) {
        setText(rejectComment, text, "rejectComment");
    }

    public void approveButton_Click() {
        clickOnElement(approveButton, "approveButton");
    }

    public void selectTransactionID(String id) {
        WebElement el = driver.findElement(By.xpath("//input[@value='" + id + "']"));
        clickOnElement(el, "Transaction id: " + id);
    }

    public void ClickOnUsernametoApprove(String username) {
        driver.findElement(By.xpath("//span[text()='" + username + "']")).click();
    }

    public void selectBank(String Bankname) {
        selectVisibleText(selectBank, Bankname, "BankName");
    }

    /*****************************************************************************************************
     * ############################ New Bulk Payout Approval #############################
     *****************************************************************************************************/
    public void navigateNewToLink() throws Exception {
        navigateTo("BULKPAY_ALL", "BULKPAY_NEW_BULK_APPROVE", "Buld Payout Tool Approval Level 1");
    }

    public void navApproveLevelTwo() throws Exception {
        navigateTo("BULKPAY_ALL", "BULKPAY_NEW_BULK_APPROVE_2", "Buld Payout Tool Approval Level 2");
    }

    public void navApproveLevelThree() throws Exception {
        navigateTo("BULKPAY_ALL", "BULKPAY_NEW_BULK_APPROVE_3", "Buld Payout Tool Approval Level 3");
    }

    public void clickSortNewest() {
        clickOnElement(sortNewest, "sortNewest");
    }

    public void clickRequest() {
        clickOnElement(request, "Initiate Entry");
    }

    public void clickApprove() {
        clickOnElement(approveBtn, "approveBtn");
    }

    public void clickReject() {
        clickOnElement(rejectBtn, "rejectBtn");
    }

    public String getActionMessage() {
        try {
            Thread.sleep(2500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fl.elementIsDisplayed(message)) {
            return message.getText();
        } else {
            return null;
        }
    }

    public void selectService(String service) {
        selectVisibleText(selService, service, "Service Type");
    }

    public String verfiy_errorMsg() {
        String msg = message1.getText();
        pageInfo.info("Get message");
        return msg;
    }
}
