package framework.pageObjects.bulkPayoutTool;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class BulkPayoutInitiate_page1 extends PageInit {


    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/

    @FindBy(id = "bulkPayoutToolAction_inputForUploadCSV_transactionType")
    WebElement service;
    @FindBy(id = "bulkPayoutToolAction_inputForUploadCSV_commonRemarks")
    WebElement remark;

    @FindBy(id = "bulkPayoutButtIni")
    WebElement submitButton;
    /***************************************************************************
     *####################### Page Objects for new Bulk Payout Initiate #####
     ***************************************************************************/
    @FindBy(xpath = "//select")
    WebElement serviceName;
    @FindBy(xpath = "//span[.='UPLOAD']")
    WebElement uploadbtn;
    @FindBy(id = "remarks")
    WebElement remarks;
    @FindBy(name = "action")
    WebElement submitBtn;
    @FindBy(css = ".alert.alert-success")
    WebElement message;
    @FindBy(xpath = "//span/a[text()='Download template']")
    WebElement download;

    @FindBy(css = ".uploader_details.cursor_default")
    WebElement txtUploadInfo;
    @FindBy(xpath = "//a[@class='text_decoration']")
    WebElement errorStatus;
    @FindBy(xpath = "(//div[@class='alert alert-danger']//span)[2]")
    WebElement errorMsg;
    @FindBy(xpath = "//span[@class='download_template']/a")
    WebElement bulkSampleFile;
    @FindBy(xpath = "//input[@id='file-upload1']")
    private WebElement upload;

    public BulkPayoutInitiate_page1(ExtentTest t1) {
        super(t1);
    }

    public static BulkPayoutInitiate_page1 init(ExtentTest t1) {
        return new BulkPayoutInitiate_page1(t1);
    }

    /***************************************************************************
     * ############################ Page Operations ############################
     ***************************************************************************/


    public void navigateToLink() throws Exception {
        navigateTo("BULKPAY_ALL", "BULKPAY_BULKADD", "Bulk Payout Tool");
    }

    public void service_SelectValue(String text) {
        selectValue(service, text, "Service");
    }

    public void service_SelectText(String text) {
        clickOnElement(service, "Service");

    }

    public void remark_SetText(String text) {
        setTextUsingJs(remark, text, "Remark");
    }
    /****************************************************************************/
    //New Initiate Bulk Payout Methods//

    public void submitButton_Click() {
        clickOnElement(submitButton, "Submit");
    }

    /****************************************************************************/

    public void navigateToNewLink() throws Exception {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 120);
        navigateTo("BULKPAY_ALL", "BULKPAY_BULK_INITIATE", "Bulk Payout New Page");
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".btn.waves-effect.waves-light.submitBtn")));
    }

    public void serviceSelectText(String text) throws InterruptedException {
        selectVisibleText(serviceName, text, "serviceName");
    }

    public void uploadFile(String filepath) {
        //setText(upload, filepath, "Upload File");
        upload.sendKeys(filepath);
        pageInfo.info("Uploading file" + filepath);
    }

    public boolean fileUpload(String filePath) throws Exception {
        uploadFile(filePath);
        Utils.captureScreen(pageInfo);
        Thread.sleep(1500);
        if (ConfigInput.isAssert) {
            if (fl.elementIsDisplayed(txtUploadInfo)) {
                if (txtUploadInfo.getText().toLowerCase().contains("initiated")) {
                    pageInfo.pass("Successfully uploaded the file:" + filePath);
                    return true;
                } else {
                    pageInfo.fail("Failed to uploaded the file:" + filePath);
                    return false;
                }
            } else {
                pageInfo.fail("Failed to uploaded the file:" + filePath);
            }
        }

        return false;
    }

    public void enterRemark(String text) {
        setTextUsingJs(remarks, text, "Remark");
    }

    public void clickSubmitButton() {
        clickOnElement(submitBtn, "Submit");
    }

    public String getActionMessage() {
        if (fl.elementIsDisplayed(message)) {
            return message.getText();
        } else {
            return null;
        }
    }

    public void fileDownload() {
        clickOnElement(download, "Download:" + FilePath.dirFileDownloads);
    }

    public ArrayList<String> downloadErrorStatusFile(ExtentTest t) throws InterruptedException, IOException {

        pageInfo.info("Deleting existing file with prefix - bulk-upload");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-error-"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        errorStatus.click();
        pageInfo.info("Click on the download Failed Transaction link");
        Thread.sleep(5000);

        //String errorFile = FilePath.dirFileDownloads;
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000);
        String errorFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-error");

        ArrayList<String> errorList = new ArrayList<String>();

        try {

            BufferedReader br = null;

            try {
                //Reading the csv file
                br = new BufferedReader(new FileReader(errorFile));

                String line = "";

                //Read to skip the header
                br.readLine();

                //Reading from the second line
                while ((line = br.readLine()) != null) {

                    String[] details = line.split(",");

                    if (details.length > 0) {

                        t.info("RowNumber: " + details[details.length - 2] + " , " + "Message: " + details[details.length - 1]);
                        errorList.add(details[details.length - 1].trim().replace("\"", " ").trim());
                    }
                }

            } catch (Exception ee) {
                ee.printStackTrace();
            } finally {
                try {
                    br.close();
                } catch (IOException ie) {
                    System.out.println("Error occured while closing the BufferedReader");
                    ie.printStackTrace();
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }
        return errorList;
    }

    public String verfiy_Errormessage() {
        wait.until(ExpectedConditions.visibilityOf(errorMsg));
        String msg = errorMsg.getText();
        pageInfo.info("get error message");
        return msg;
    }

    public boolean downloadTemplateBulkCSV(String prefix) throws Exception {
        try {
            pageInfo.info("Deleting existing file with prefix - " + prefix);
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, prefix);
            Thread.sleep(5000);
            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            clickOnElement(bulkSampleFile, "bulkSampleFile");
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Thread.sleep(5000);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            return Utils.isFileDownloaded(oldFile, newFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String verfiy_ErrorStatusLog() throws IOException {
        String msg = null;
        try {

            msg = errorMsg.getText();
            pageInfo.info("Check whether Download Error Status link is visible or not");

            Boolean link = driver.findElement(By.xpath("//a[contains(.,'Download error status')]")).isDisplayed();
            String text = driver.findElement(By.xpath("//a[contains(.,'Download error status')]")).getText();

            pageInfo.info(text);
            Assertion.verifyEqual(true, link, "Download Error Status Link is visble", pageInfo);

        } catch (NoSuchElementException e) {
            e.printStackTrace();

            pageInfo.fail("Failed to find the 'DownloadErrorStatus' Link in Error Message ");
            Utils.captureScreen(pageInfo);
        }
        return msg;
    }
}
