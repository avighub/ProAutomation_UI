package framework.pageObjects.bulkPayoutTool;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class NewBulkPayoutInitiatePage extends PageInit {


    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/

    @FindBy(xpath = "//select[@class='browser-default ng-untouched ng-pristine ng-valid']")
    WebElement serviceType;
    @FindBy(id = "remarks")
    WebElement remarkTxtBox;
    @FindBy(xpath = "//input[@type='file']")
    WebElement fileUpload;
    @FindBy(xpath = "//button[@type='submit']")
    WebElement submitButton;
    @FindBy(xpath = "//select")
    WebElement serviceName;
    @FindBy(xpath = "//span[.='UPLOAD']")
    WebElement uploadbtn;
    //select[@class='browser-default ng-pristine ng-valid ng-touched']
    @FindBy(name = "action")
    WebElement submitBtn;
    @FindBy(xpath = "(//div[@class='alert alert-success']//span)[2]")
    WebElement message;

    public NewBulkPayoutInitiatePage(ExtentTest t1) {
        super(t1);
    }

    /***************************************************************************
     * ############################ Page Operations ############################
     ***************************************************************************/

    public void navigateToLink() throws Exception {
        fl.leftNavigation("BULKPAY_ALL", "BULKPAY_BULK_INITIATE");
        pageInfo.info("Navigate to New Bulk Payout Tool Initiate Link..");
    }


    public void selectServiceByValue(String text) {
        serviceType.click();
        Select s_service = new Select(serviceType);
        s_service.selectByValue(text);
        pageInfo.info("Select Service by value: " + text);
    }


    public void setRemarks(String text) {
        remarkTxtBox.clear();
        remarkTxtBox.sendKeys(text);
        pageInfo.info("Set Remarks : " + text);
    }


    public void uploadFile(String text) {
        fileUpload.clear();
        fileUpload.sendKeys(text);
        pageInfo.info("Uploading file: " + text);
    }


    public void fileUpload(String text) throws AWTException, InterruptedException {

        uploadbtn.click();
        Thread.sleep(1000);
        StringSelection s = new StringSelection(text);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(s, null);
        Robot r = new Robot();
        Thread.sleep(2000);
        r.keyPress(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_V);
        r.keyRelease(KeyEvent.VK_CONTROL);
        r.keyRelease(KeyEvent.VK_V);
        Thread.sleep(2000);
        r.keyPress(KeyEvent.VK_ENTER);
        r.keyRelease(KeyEvent.VK_ENTER);
        //uploadbtn.sendKeys(text);
        pageInfo.info("Uploading file: " + text);
    }


    public void clickSubmitButton() {
        submitBtn.click();
        pageInfo.info("Click on Submit Button");
    }

    public String getWebMessage() {
        String msg = message.getText();
        pageInfo.info("get message");
        return msg;
    }
}
