package framework.pageObjects.pricingEng;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import static framework.util.common.Utils.putThreadSleep;

import java.util.List;


public class PricingEngine_Page1 extends PageInit {

    @FindBy(xpath = "//*[@id='delete-confirmation']/div[2]/div[1]/a")
    WebElement okButton;

    @FindBy(id = "SHULKA_ALL")
    private WebElement link;

    @FindBys({@FindBy(id = "service-selector")})
    private List<WebElement> services;

    @FindBy(xpath = "//a[contains(@href,'login_gethomepage.action')][@class='top-menu-item-link parent-page-link']")
    private WebElement backToHome;

    @FindBy(id = "submenu-service-charge-taxRule")
    private WebElement taxMenu;

    @FindBy(id = "main-menu-approval")
    private WebElement approvalMenu;
    @FindBy(xpath = "//div[.='Commission Policy']")
    private WebElement approvalCommission;
    @FindBy(xpath = "//div/button[.='Submit']")
    private WebElement submit;
    @FindBy(xpath = "//a[.='CONFIRM']")
    private WebElement submitConfirm;
    @FindBy(xpath = "(//span[@class='switch-label'])[2]")
    private WebElement inactive;
    @FindBy(xpath = "//a[contains(@class,'add-new-charge-rule-btn')]")
    private WebElement addNewRuleBtn;
    @FindBy(xpath = "//a/img[contains(@src,'approve.png')]")
    private WebElement approveImage;
    @FindBy(xpath = "//tbody/tr[2]/td[.='Discount Rule']")
    private WebElement discountRule;
    @FindBy(xpath = "//tbody/tr[3]/td/span[.='Taxation Rule']")
    private WebElement taxationRule;
    @FindBy(xpath = "//div[@class='modified-rule-background']")
    private WebElement approvePolicyImage;
    @FindBy(id = "remove-charge-rule-0")
    private WebElement deleteBtn;
    @FindBy(xpath = "//*[contains(@class,\"add-new-tax-rule-btn\")]")
    private WebElement addNewRuleBtnForTax;
    @FindBy(xpath = " //*[contains(@class,\"add-new-rule-btn\")]")
    private WebElement addNewRuleBtnForTaxation;
    @FindBy(xpath = "//*[@id='charge-rules']/div[6]/ul/li[1]/div/div[1]/div/div[1]/div[1]/span")
    private WebElement reorderElement;
    @FindBy(xpath = "//*[@id='charge-rules']/div[6]/ul/li[2]/div/div[1]/div/div[1]/div[2]/span[1]")
    private WebElement reorderDestination;
    @FindBy(xpath = "//button[@class='btn reorder-btn reorder-btn-fill']")
    private WebElement doneBtn;
    @FindBy(xpath = "//button[@class='btn reorder-btn']")
    private WebElement reOrderBtn;
    @FindBy(id = "currency-for-policy")
    private WebElement currency;
    @FindBy(xpath = "//a[@data-bind[contains(.,'saveDraft')]]")
    private WebElement saveDraft;
    @FindBy(xpath = "//*[@id='submenu-service-charge-policy-charge-rule']")
    private WebElement service_ChargeRule;
    @FindBy(xpath = "//*[@id='submenu-service-charge-policy-waive-rule']")
    private WebElement service_DiscountRule;
    @FindBy(xpath = "//*[@id='submenu-service-charge-policy-tax-rule']")
    private WebElement service_TaxationRule;
    @FindBy(xpath = "//*[@id='submenu-commission-policy-charge-rule']")
    private WebElement commission_ChargeRule;
    @FindBy(xpath = "//*[@id='submenu-commission-policy-tax-rule']")
    private WebElement commission_TaxationRule;
    @FindBy(xpath = "//*[@id='main-menu-commission-service-selector']")
    private WebElement commissionMainMenu;
    @FindBy(xpath = "//*[@id='main-menu-service-charge-service-selector']")
    private WebElement serviceChargeMainMenu;
    @FindBy(xpath = "//*[@id='policy-calculator-serviceSelector']")
    private WebElement pricingCalculator;
    @FindBy(xpath = "//a[contains(@href,'pendingForUpdate')]")
    private WebElement rejectedPoliciesLink;
    @FindBy(xpath = "//a[contains(text(),'PROCEED')] ")
    private WebElement proceed;
    @FindBy(id = "calculator-transactionAmount")
    private WebElement txnAmount;
    @FindBy(xpath = "//select[contains(@data-bind,'bearerCode')]")
    private WebElement bearerCode;
    @FindBy(xpath = "//button[@class='btn calculator-btn']")
    private WebElement calculate;
    @FindBy(xpath = "//*[@id=\"charge-rules\"]/div[2]/div[4]/div/a")
    private WebElement DisplayedVersion;
    @FindBy(xpath = "//span[@data-on='Active']")
    private WebElement activeButton;
    @FindBy(xpath = "//a[@data-bind='click: openVersionsDialog']")
    private WebElement viewPreviousVersions;
    @FindBy(id = "policy-version")
    private WebElement enterVersion;
    @FindBy(id = "open-policy-version")
    private WebElement submitVersiontoView;
    @FindBy(id = "rule-search-container")
    private WebElement searchRules;
    @FindBy(xpath = "//*[@id=\"search-rules\"]/div//button")
    private WebElement searchButton;
    @FindBy(id = "submenu-service-charge-taxRule")
    private WebElement TaxationRule;
    @FindBy(xpath = "//a/img[contains(@src,'reject.png')]")
    private WebElement rejectImage;

    @FindBy(xpath = "//textarea[@class='reject-feedback-text']")
    private WebElement feedbackText;

    @FindBy(xpath = "//button[@class='btn feedback-save-btn feedback-done-enabled']")
    private WebElement saveButton;

    @FindBy(xpath="//div[@data-bind='if: reviewablePriorityChanges.hasChanges']")
    private WebElement priorityChanges;

    public PricingEngine_Page1(ExtentTest t1) {
        super(t1);
    }

    public static PricingEngine_Page1 init(ExtentTest t1) {
        return new PricingEngine_Page1(t1);
    }

    public void navToPricingEngine() throws Exception {
        fl.clickLink("SHULKA_ALL");
    }

    public void selectServiceType(String ServiceName) throws Exception {
        fl.contentFrame();
        driver.findElement(By.xpath("//a[.='" + ServiceName + "']")).click();
        pageInfo.info(ServiceName + "is Selected");
        putThreadSleep(2000);
    }

    public PricingEngine_Page1 selectService(String serviceName) {
        driver.findElement(By.xpath("//*[contains(@href,'" + serviceName + "')]")).click();
        pageInfo.info(serviceName + " Service Selected.");
        return this;
    }

    public void selectCurrency(String currencyType) {
        selectVisibleText(currency, currencyType, "Currency");
    }

    public void selectProceed() {
        clickOnElement(proceed, "Select Proceed");
    }

    public void clickBackToHome() throws Exception {
        clickOnElement(backToHome, "Back to home button");
    }

    public void clickTax() throws Exception {
        clickOnElement(taxMenu, "Taxation Rule");
    }

    public void clickOnRejectImage() throws Exception {
        clickOnElement(rejectImage, "Reject Image");
        putThreadSleep(Constants.TWO_SECONDS);
        feedbackText.sendKeys("test");

    }

    public void clickOnSave() throws Exception {
        clickOnElement(saveButton, "Save Button");
    }

        public void clickOnTaxationRuleFromService()
        {
            clickOnElement(TaxationRule, "Taxation Rule");

        }
     public void ClickOnaddNewRuleBtnForTaxation()
     {
         clickOnElement(addNewRuleBtnForTaxation, "Add new Rule");
     }


    public void clickOnApproveImage() throws Exception {
        clickOnElementUsingJs(approveImage, "Approve Image");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
    }

    public boolean clickOnDiscountRule() throws Exception {
        boolean flag =false;
        try{
            if(discountRule.isDisplayed()){
                clickOnElement(discountRule, "Discount Rule");
                Thread.sleep(2000);
                clickOnApproveImage();
                flag=true;
            }}catch(Exception e){
            pageInfo.info("No discount Rule Displayed");
        }
        return flag;
    }

    public boolean clickOnTaxationRule() throws Exception {
        boolean flag=false;
        try{
            if (taxationRule.isDisplayed()) {
                clickOnElement(taxationRule, "Taxation Rule");
                Thread.sleep(2000);
                clickOnApproveImage();
                flag=true;
            }}catch(Exception e){
            pageInfo.info("No tax Rule Displayed");
        }
        return flag;
    }


    public void clickOnApprovePolicyImage() throws Exception {
        clickOnElement(approvePolicyImage, "Approve Policy Image");
    }

    public void clickApproval() throws Exception {
        clickOnElement(approvalMenu, "Approval");
    }

    public void clickCommissionApproval() throws Exception {
        clickOnElement(approvalCommission, "CommissionApproval");
    }

    public void clickSubmitApproval() throws Exception {
        clickOnElement(submit, "SubmitApproval");
    }

    public void clickSubmitConfirmationApproval() throws Exception {
        clickOnElement(submitConfirm, "SubmitConfirmationApproval");
    }

    public void clickOnService(String serviceName, String user) throws Exception {
        driver.findElement(By.xpath("//div[.='" + serviceName + "']/..//span[.='" + user + " Automation']")).click();
    }

    public void changeStatus() throws Exception {
        clickOnElement(inactive, "Suspend Service");
    }

    public List<WebElement> getServiceList() {
        return services;
    }

    public void clickOnAddNewRuleButton() {
        clickOnElement(addNewRuleBtn, "Add New Rule");
    }

    public void clickOnDeleteButton() {
        clickOnElement(deleteBtn, "Delete");
    }

    public void savePolicy() throws InterruptedException {
        WebElement draft = driver.findElement(By.xpath("//a[contains(@data-bind,'openSubmitDialog')]"));
        actions.moveToElement(draft).perform();
        actions.click(draft).perform();
    }

    public void clickSubmit() throws InterruptedException {
        Thread.sleep(7000);
        WebElement submit = driver.findElement(By.xpath("//a[contains(@data-bind,'apply')]"));
        submit.click();
        Thread.sleep(5000);
        //new WebDriverWait(DriverFactory.getDriver(), Constants.EXPLICIT_WAIT_TIME).until(ExpectedConditions.elementToBeClickable(finalSubmit));
        //finalSubmit.click();
    }


    public void deleteAllServiceCharge() {
        try {
            List<WebElement> serviceCharges = driver.findElements(By.xpath("//i[contains(@class,'remove-charge-rule')]"));
            for (WebElement ele : serviceCharges) {
                clickOnElement(ele, "Delete Button");
                Utils.putThreadSleep(Constants.TWO_SECONDS);
                clickOnElement(okButton, "OK Button");
                clickSaveDraft();
                Thread.sleep(3000);
                savePolicy();
                clickSubmit();

            }
        } catch (Exception e) {
            pageInfo.info("Service Charge is not defined");
        }

    }

    public void clickOnAddNewRuleButtonForTax() {
        clickOnElement(addNewRuleBtnForTax, "Add New Rule For Tax");
    }

    public boolean isBackToHomeVisible() {
        boolean isVisible = false;
        try {
            isVisible = backToHome.isDisplayed();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isVisible;
    }

    public int reorderServiceRule() {
        int listOfRules = driver.findElements(By.xpath("//*[@id='charge-rules']/div[6]/ul/li")).size();
        if (listOfRules > 1) {
            Actions builder = new Actions(driver);
            Action dragAndDrop = builder.clickAndHold(reorderElement)
                    .moveToElement(reorderDestination)
                    .release(reorderDestination).build();
            dragAndDrop.perform();
        } else if (listOfRules == 1) {
            pageInfo.skip("As there is only single service rule available in the system, " +
                    "the reordering of rules is not required.");
        } else
            pageInfo.info("No Service rule found to reorder.");
        return listOfRules;
    }

    public void clickReOrderBtn() {
        clickOnElement(reOrderBtn, "Reorder button");
    }

    public void clickDoneBtn() {
        clickOnElement(doneBtn, "Done button");
    }

    public String[] priorityCheck(int numofRules, boolean pReq) throws InterruptedException {

        if (numofRules > 1 && pReq) {
            Thread.sleep(5000);
            WebElement priority;
            String prePriorityPath = "//div[@data-bind[contains(.,'visibleReviewableRules')]]/div[2]//" +
                    "span[@data-bind[contains(.,'oldPriority')]][@class[contains(.,'priority-change')]]";
            String postPriorityPath = "//div[@data-bind[contains(.,'visibleReviewableRules')]]/div[2]//" +
                    "span[@data-bind[contains(.,'newPriority')]][@class[contains(.,'priority-change')]]";
            priority = driver.findElement(By.xpath(prePriorityPath));
            String Priority[] = new String[2];
            Priority[0] = priority.getText();
            priority = driver.findElement(By.xpath(postPriorityPath));
            Priority[1] = priority.getText();
            return Priority;
        } else return null;
    }

    public boolean changeStatus(String rulename, String status) throws InterruptedException {
        Thread.sleep(3000);
        boolean changed = false;
        WebElement statusChng = driver.findElement(By.xpath("//span[@title='" + rulename + "']/../.." +
                "/following-sibling::div//span[@class='switch-handle']"));
        int size = 0;
        if (status == "A") {
            size = driver.findElements(By.xpath("//span[@title='" + rulename + "']/../../../../../.." +
                    "//preceding-sibling::div[@class='service-charge-rule']")).size();
            if (size == 0) {
                statusChng.click();
                changed = true;
                pageInfo.info("Service Charge policy[" + rulename + "] is activated.");
            } else {
                pageInfo.info("Service Charge policy is already in activate state.");
            }
        } else if (status == "S") {
            size = driver.findElements(By.xpath("//span[@title='" + rulename + "']/../../../../../.." +
                    "//preceding-sibling::div[@class='service-charge-rule suspended']")).size();
            if (size == 0) {
                statusChng.click();
                changed = true;
                pageInfo.info("Service Charge policy[" + rulename + "] is suspended.");
            } else {
                pageInfo.info("Service Charge policy is already in suspended state.");
            }
        }
        return changed;
    }

    public void clickSaveDraft() {
        pageInfo.info("Trying to click on Save Draft");
        //saveDraft.click();
        clickOnElement(saveDraft, "Save Draft");
        pageInfo.info("Clicked Save Draft.");
    }

    public boolean isPolicyExist(String policyName) {
        int policyCount = 0;
        boolean policyExist = false;
        pageInfo.info("Trying to check the Policy [" + policyName + "] is saved.");
        policyCount = driver.findElements(By.xpath("//span[@title='" + policyName + "']")).size();
        if (policyCount == 1) {
            pageInfo.info("Policy is saved and visible on GUI.");
            policyExist = true;
        } else if (policyCount > 1) {
            pageInfo.info("More than once policy exists with the same provided name.");
            policyExist = true;
        } else if (policyCount == 0)
            pageInfo.info("No Policy exists with name " + policyName + ", Policy might not saved or discarded.");
        return policyExist;
    }

    public boolean checkSubMenusOfServiceCharge() {
        int numOfSubMenus = driver.findElements(By.xpath("//li[contains(@id,'submenu-service-charge-policy')]")).size();
        pageInfo.info("Total SubMenus for Service Charge: " + numOfSubMenus);
        boolean linksExist = false;
        if (numOfSubMenus == 3) {
            try {
                pageInfo.info("Checking for sub menu ChargeRule");
                service_ChargeRule.isDisplayed();
                pageInfo.info("Checking for sub menu DiscountRule");
                service_DiscountRule.isDisplayed();
                pageInfo.info("Checking for sub menu TaxRule");
                service_TaxationRule.isDisplayed();
                linksExist = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            pageInfo.fail("Expected number of SubMenus of Service Charge: 3 | Found: " + numOfSubMenus);
        }
        return linksExist;
    }

    public void clickCommissionMainMenu() {
        pageInfo.info("Navigation to Commission Main menu.");
        commissionMainMenu.click();
        pageInfo.info("Commission main menu clicked.");
    }

    public void clickServiceChargeMainMenu() {
        pageInfo.info("Navigation to Service Charge Main menu.");
        serviceChargeMainMenu.click();
        pageInfo.info("Service Charge main menu clicked.");
    }

    public boolean checkSubMenusOfCommission() {
        int numOfSubMenus = driver.findElements(By.xpath("//li[contains(@id,'submenu-commission-policy')]")).size();
        pageInfo.info("Total SubMenus for Commission: " + numOfSubMenus);
        boolean linksExist = false;
        if (numOfSubMenus == 2) {
            try {
                pageInfo.info("Checking for sub menu ChargeRule");
                commission_ChargeRule.isDisplayed();
                pageInfo.info("Checking for sub menu DiscountRule");
                commission_TaxationRule.isDisplayed();
                linksExist = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            pageInfo.fail("Expected number of SubMenus of Commission: 2 | Found: " + numOfSubMenus);
        }
        return linksExist;
    }

    public void createCloneOfPolicy(String policyName) {
        pageInfo.info("Trying to clone policy : " + policyName);
        WebElement clone = driver.findElement(By.xpath("//span[@title='" + policyName + "']/../../following-sibling" +
                "::div/a[contains(@id,'clone-charge-rule')]"));
        clickOnElement(clone, "Clone");
        pageInfo.info("Clone button clicked successfully.");
    }

    public int checkPolicyCount(String policyName) {
        return driver.findElements(By.xpath("//span[@title='" + policyName + "']")).size();
    }

    public void clickOnRule(String policyName) {
        pageInfo.info("Trying to open rule : " + policyName);
        driver.findElement(By.xpath("//span[@title='" + policyName + "']")).click();
        pageInfo.info("Clicked to open the rule.");
    }

    public void clickOnPricingCalculator() {
        pageInfo.info("Trying to click on Pricing Calculator.");
        pricingCalculator.click();
        pageInfo.info("Pricing Calculator clicked.");
    }

    public void selectPricingCalculatorItems(String type, String target, String value) {
        pageInfo.info("Selecting the " + type.toUpperCase() + " details: " + target.toUpperCase());
        WebElement element = driver.findElement(By.xpath("//div[contains(@class,'" + type.toLowerCase() + "-details')]/..//select[contains(@data-bind,'" + target + "')]"));
        selectValue(element, value, target.toUpperCase());
    }

    public void enterTxnAmount(String amount) {
        pageInfo.info("Trying to enter transaction amount.");
        txnAmount.sendKeys(amount);
        pageInfo.info("Transaction amount entered: " + amount);
    }

    public void enterBearerCode(String value) {
        pageInfo.info("Trying to select bearer code: " + value);
        selectValue(bearerCode, value, "Bearer Code");
        pageInfo.info("Bearer selected.");
    }

    public void clickCalculateBtn() {
        clickOnElement(calculate, "Calculate Btn");
    }

    public String[] checkForCaluclatedValue(String type) {
        String[] applicableValue = new String[2];
        if (type.equalsIgnoreCase("SERVICE")) {
            WebElement servChrge = driver.findElement(By.xpath("//div[@data-bind='foreach: calculatorResponse().serviceCharges']//span[contains(@data-bind,'amount')]"));
            applicableValue[0] = servChrge.getText();
            WebElement servChrgeName = driver.findElement(By.xpath("//span[contains(@data-bind,'calculatorResponse().serviceCharges[0].metadata.ruleName')]"));
            applicableValue[1] = servChrgeName.getText();
        }
        return applicableValue;
    }


    public void clickViewVersions() throws Exception {
        clickOnElement(viewPreviousVersions, "View Previous Version");
    }

    public void enterVersion(String version) throws Exception {
        enterVersion.sendKeys(version);
        pageInfo.info("Entered Version: " + version);
    }

    public void clickSubmitViewVersions() throws Exception {
        clickOnElement(submitVersiontoView, "Submit Version to view");
    }

    /**
     *
     * @return
     */
    public String getDisplayedVersion() {
        String text = DisplayedVersion.getText();
        return text;
    }


    public void searchRuleName(String ruleName) throws Exception {
        setText(searchRules,ruleName,"Search Rule :");
    }


    public void clickSearchButton() throws Exception {
        searchButton.click();
    }

    public void clickActiveButton() throws Exception {
        activeButton.click();
    }


    /**
     * Click On Rejected Policies Link
     * The link is on the Pricing Engine main Page
     *
     * @throws Exception
     */
    public void clickOnRejectedPoliciesLink() throws Exception {
        clickOnElement(rejectedPoliciesLink, "Rejected Policies Link");
    }

    public void priorityReview(){
        try{if(priorityChanges.isDisplayed()){
            clickOnElement(approveImage, "Approve Image");
            Utils.putThreadSleep(Constants.TWO_SECONDS);
        }}catch(Exception e){
            pageInfo.info("Priority Changes element not found.");
        }
    }


    public boolean checkIfAlreadyInPricingEnginePage(){
        return isBackToHomeVisible();
    }

}
