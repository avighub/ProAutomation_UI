package framework.pageObjects.transactionCorrection;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

public class TxnCorrectionInitiation_page1 extends PageInit {


    @FindBy(id = "txnCorrection_loadDetails_txnId")
    WebElement transID;

    @FindBy(id = "txnCorrection_loadDetails_button_submit")
    WebElement submitButton;

    @FindBy(id = "txnCorrection_loadDetails_remarks")
    WebElement remark;

    @FindBy(name = "reverseSCRequired")
    WebElement reverseSChargeRequiredCheckBox;

    @FindBy(name = "reverseCOMRequired")
    WebElement reverseCommRequiredCheckBox;

    @FindBy(id = "txnCorrection_loadDetails_isTCPCheckReq")
    WebElement reverseTcpCheckBox;

    @FindBy(id = "txnCorrection_viewTxnDetails_button_confirm")
    WebElement confirmButton;

    @FindBy(className = "wwFormTableC")
    WebElement webTable;

    public TxnCorrectionInitiation_page1(ExtentTest t1) {
        super(t1);
    }

    public static TxnCorrectionInitiation_page1 init(ExtentTest t1) {
        return new TxnCorrectionInitiation_page1(t1);
    }

    public TxnCorrectionInitiation_page1 navigateToTxnCorrection() throws Exception {
        navigateTo("TXN_CORR_ALL", "TXN_CORR_TXN_CORRECTION", "Initiate Transaction Correction");
        return this;
    }

    public TxnCorrectionInitiation_page1 enterTransactionID(String trID) {
        setText(transID, trID, "Transaction Id");
        return this;
    }

    public TxnCorrectionInitiation_page1 clickRevScharge() {
        clickOnElement(reverseSChargeRequiredCheckBox, "reverseSChargeRequiredCheckBox");
        return this;
    }

    public TxnCorrectionInitiation_page1 clickRevCommission() {
        clickOnElement(reverseCommRequiredCheckBox, "reverseCommRequiredCheckBox");
        return this;
    }
    public TxnCorrectionInitiation_page1 clickRevTCP() {
        clickOnElement(reverseTcpCheckBox, "reverseTcpCheckBox");
        return this;
    }
    public TxnCorrectionInitiation_page1 clickSubmit() {
        clickOnElement(submitButton, "submitButton");
        return this;
    }

    public void clickConfirm() {
        clickOnElement(confirmButton, "confirmButton");
    }

    public TxnCorrectionInitiation_page1 setRemarks(String text) {
        setText(remark, text, "Remark");
        return this;
    }

    public String getTransactionIdFromUI() throws IOException {
        return getTableDataUsingHeaderName(webTable, "Transaction ID");
    }

    public String getTransferValueFromUI() throws IOException {
        return getTableDataUsingHeaderName(webTable, "Transfer Value").split(" ")[0].trim();
    }

    public String getServicTypeFromUI() throws IOException {
        return getTableDataUsingHeaderName(webTable, "Service Type");
    }

    public String getTransactionDateFromUI() throws IOException {
        return getTableDataUsingHeaderName(webTable, "Transaction Date");
    }

    public String getPayerMsisdnFromUI() throws IOException {
        return getTableDataUsingHeaderName(webTable, "Payer MSISDN");
    }

    public String getPayeeMsisdnFromUI() throws IOException {
        return getTableDataUsingHeaderName(webTable, "Payee MSISDN");
    }

    public String getTransferedAmountFromUI() throws IOException {
        return getTableDataUsingHeaderName(webTable, "Amount");
    }


}
