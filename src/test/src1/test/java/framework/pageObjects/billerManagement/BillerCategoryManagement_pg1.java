/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 2-Jan-2018
 *  Purpose: Biller Category Management System Cases
 */
package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BillerCategoryManagement_pg1 extends PageInit {

    @FindBy(id = "MerchantCategoryAction_ShowMerchantCategoryDetail_button_add")
    private WebElement addButton;
    @FindBy(id = "MerchantCategoryAction_ShowMerchantCategoryDetail_button_update")
    private WebElement updateButton;
    @FindBy(id = "MerchantCategoryAction_showAddMerCategoryPage_categoryName")
    private WebElement nameTbox;
    @FindBy(id = "MerchantCategoryAction_showAddMerCategoryPage_categoryCode")
    private WebElement codeTbox;
    @FindBy(id = "MerchantCategoryAction_showAddMerCategoryPage_description")
    private WebElement descTbox;
    @FindBy(id = "MerchantCategoryAction_showAddMerCategoryPage_button_add")
    private WebElement add2Button;
    @FindBy(id = "MerchantCategoryAction_showAddMerConfirmPage_button_confirm")
    private WebElement confirmButton;
    @FindBy(id = "MerchantCategoryAction_showAddMerConfirmPage_button_back")
    private WebElement confirmBackButton;
    @FindBy(id = "MerchantCategoryAction_showAddMerCategoryPage_button_back")
    private WebElement backButton;
    @FindBy(id = "MerchantCategoryAction_showAddMerConfirmPage_categoryName")
    private WebElement confirmPageCategoryName;
    @FindBy(id = "MerchantCategoryAction_showAddMerConfirmPage_categoryCode")
    private WebElement confirmPageCategoryCode;
    @FindBy(id = "MerchantCategoryAction_showAddMerConfirmPage_description")
    private WebElement confirmPageDescription;

    public BillerCategoryManagement_pg1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate to add Biller Category
     *
     * @throws NoSuchElementException NoSuchElementException
     */
    public void navCreateBillerCategory() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_MCAT");
        pageInfo.info("Navigate to Biller Category Management Page!");
    }

    /**
     * clickOnAdd
     */
    public void clickOnAdd() {
        addButton.click();
        pageInfo.info("Click on Button Add");
    }

    /**
     * clickOnUpdate
     */
    public void clickOnUpdate() {
        updateButton.click();
        pageInfo.info("Click on button update");
    }

    /**
     * addBillerCategory
     */
    public void addBillerCategory() {
        add2Button.click();
        pageInfo.info("Click on Add Biller Button!");
    }

    /**
     * confirm Button click
     */
    public void confirm() {
        confirmButton.click();
        pageInfo.info("Click On Confirm Category!");
    }

    /**
     * clickOnBack
     * Back button click
     */
    public void clickOnBack() {
        backButton.click();
        pageInfo.info("Click On Back Button!");
    }

    /**
     * clickOnConfirmBack
     * Confirm Page Back button click
     */
    public void clickOnConfirmBack() {
        confirmBackButton.click();
        pageInfo.info("Click On Confirm Page Back Button!");
    }

    /**
     * Set Category Name
     *
     * @param categoryName Biller Category Name
     */
    public void setCategoryName(String categoryName) {
        nameTbox.sendKeys(categoryName);
        pageInfo.info("Set Category Name - " + categoryName);
    }

    /**
     * setCategoryCode
     *
     * @param catCode catCode
     */
    public void setCategoryCode(String catCode) {
        codeTbox.sendKeys(catCode);
        pageInfo.info("Set Category Code :- " + catCode);
    }

    /**
     * set Biller Description
     *
     * @param desc Biller Description
     */
    public void setDescription(String desc) {
        descTbox.sendKeys(desc);
        pageInfo.info("Set Category's Description - " + desc);
    }

    /**
     * verifyRoleExists using Category Name
     *
     * @param catName Category Name
     * @return row number of existing biller category
     */
    public int verifyRoleExists(String catName) {
        if (driver.findElements(By.xpath("//tr/td[contains(text(),'" + MessageReader.getMessage("pseudo.emptyList.label", null) + "')]")).size() > 0) {
            pageInfo.info("No Category Exist");
            return 0;
        }

        int rowCount = driver.findElements(By.xpath("//*[@id='MerchantCategoryAction_ShowMerchantCategoryDetail']/table/tbody/tr")).size();

        // iterate through all the cells
        for (int i = 2; i <= rowCount - 1; i++) {

            String roleName = driver.findElement(By.xpath("//*[@id='MerchantCategoryAction_ShowMerchantCategoryDetail']/table/tbody/tr[" + i + "]/td[2]")).getText();
            if (roleName.trim().equals(catName)) {
                pageInfo.info("Biller Category for Automation already exists : " + catName);
                return i;
            }
        }
        return 0;
    }


    /**
     * selectBillerCategoryRadio
     *
     * @param catCode catCode
     */
    public void selectBillerCategoryRadio(String catCode) {
        WebElement billRadio = driver.findElement(By.xpath("//input[contains(@value,'" + catCode + "')]"));
        billRadio.click();
        pageInfo.info("Clicked on Category having code : " + catCode);
    }


    /**
     * isAddButtonDisplayed
     *
     * @return true or false based on the operation
     */
    public boolean isAddButtonDisplayed() {
        return addButton.isDisplayed();

    }

    /**
     * isBackButtonDisplayed
     *
     * @return true or false based on the operation
     */
    public boolean isFirstPageBackButtonDisplayed() {
        return backButton.isDisplayed();

    }

    /**
     * getConfirmPageCatName
     *
     * @return ConfirmPageCatName
     */
    public String getConfirmPageCatName() {
        return confirmPageCategoryName.getText();
    }

    /**
     * getConfirmPageCatCode
     *
     * @return Confirm PageCatCode
     */
    public String getConfirmPageCatCode() {
        return confirmPageCategoryCode.getText();
    }

    /**
     * getConfirmPageDesc
     *
     * @return ConfirmPageDesc
     */
    public String getConfirmPageDesc() {
        return confirmPageDescription.getText();
    }

}
