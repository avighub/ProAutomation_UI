package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class BillerModification_Page2 extends PageInit {

    @FindBy(id = "enumId")
    WebElement serviceLevel;
    @FindBy(id = "billerType")
    WebElement selBillerType;
    @FindBy(id = "billAmount")
    WebElement selbillAmountTypr;
    @FindBy(id = "partpaysubtype")
    WebElement selPartPaySubYype;
    @FindBy(id = "bankPaymentEffTo")
    WebElement selPaymenteffected;
    @FindBy(id = "autoform3_button_next")
    WebElement nextButton;
    @FindBy(id = "autoform3_button_confirm")
    WebElement confirmButton;
    @FindBy(id = "autoform3_button_confirm")
    WebElement submit;
    @FindBy(id = "modifyMerchant_modifyConfirm_submit' and @value='Submit")
    WebElement submitAfterChanges;
    @FindBy(id = "interfaceId")
    WebElement interfaceID;
    @FindBy(id = "processType")
    private WebElement selProccesType;

    public BillerModification_Page2(ExtentTest t1) {
        super(t1);
    }

    public void clickNextButton() {
        nextButton.click();
        pageInfo.info("Click on Next button");
    }

    public void clickConfirmButton() {
        confirmButton.click();
        pageInfo.info("Click on Confirm button");
    }

    public void clickSubmitButtonAfterChanges() {
        WebElement submitAfterChanges = driver.findElement(By.xpath(".//*[@id='modifyMerchant_addMoreLiquidationBankMod_submit' and @value='Submit']"));
        submitAfterChanges.click();
        pageInfo.info("Click on Submit button");
    }

    public void selectServiceLevel(String value) {

        Select s = new Select(serviceLevel);
        s.selectByValue(value);
        pageInfo.info("update service level to " + value);
    }

    public void selectProcessType(String processType) {

        selectValue(selProccesType, processType, "Process Type");
    }

    public void selectPayementEffectedTo(String text) {
        Select sel = new Select(selPaymenteffected);
        sel.selectByValue(text);
        pageInfo.info("select Payement Effected To - " + text);
    }

    public void selectPartPaymentSubType(String text) {
        Select sel = new Select(selPartPaySubYype);
        sel.selectByValue(text);
        pageInfo.info("Select Part Payement Sub Type - " + text);
    }

    public void selectBillAmountType(String text) {
        Select sel = new Select(selbillAmountTypr);
        sel.selectByValue(text);
        pageInfo.info("Select Bill Amount Type - " + text);
    }


    public void selectBillerType(String text) {
        Select sel = new Select(selBillerType);
        sel.selectByVisibleText(text);
        pageInfo.info("Select Biller Type as " + text);
    }

    public void modifyInterfaceID(String interfaceid) {
        interfaceID.sendKeys(interfaceid);
    }
}
