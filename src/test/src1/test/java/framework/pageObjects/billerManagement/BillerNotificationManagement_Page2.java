package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class BillerNotificationManagement_Page2 extends PageInit {


    @FindBy(id = "addSystemNotification_addNotification_inputNotificationName")
    WebElement notificationNameTbox;
    @FindBy(id = "genDateBtn")
    WebElement genDateRadio;
    @FindBy(id = "dueDateBtn")
    WebElement dueDateRadio;
    @FindBy(id = "addSystemNotification_addNotification_notificationDays")
    WebElement notificationDaysTbox;
    @FindBy(id = "addSystemNotification_addNotification_submit")
    WebElement submitButton;
    @FindBy(id = "addSystemNotification_insertNotificationDetail_confirm")
    WebElement confirmButton;
    @FindBy(id = "addSystemNotification_updateNotification_inputNotificationName")
    WebElement updateNotificationNameTbox;

    //Update page Elements
    @FindBy(id = "addSystemNotification_updateNotification_statusId")
    WebElement updateStatusDdown;
    @FindBy(id = "addSystemNotification_updateNotification_submit")
    WebElement updateSubmitButton;
    @FindBy(id = "addSystemNotification_confirmUpdateNotificationDetail_confirm")
    WebElement updateConfirmButton;

    public BillerNotificationManagement_Page2(ExtentTest t1) {
        super(t1);
    }

    public void clickGenDateRadio() {
        genDateRadio.click();
        pageInfo.info("Click on Generate Bill Date Radio button");
    }

    public void clickDueDateRadio() {
        dueDateRadio.click();
        pageInfo.info("Click on Due Date Radio button");
    }

    public void setNotificationName(String name) {
        notificationNameTbox.sendKeys(name);
        pageInfo.info("Set Notification name : " + name);
    }

    public void setNotificationDays(String days) {
        notificationDaysTbox.sendKeys(days);
        pageInfo.info("Set Notification days : " + days);
    }

    public void clickSubmitButton() {
        submitButton.click();
        pageInfo.info("Click on Submit button");
    }

    public void clickConfirmButton() {
        confirmButton.click();
        pageInfo.info("Click on Confirm button");
    }

    public void updateNotificationName(String name) {
        updateNotificationNameTbox.clear();
        updateNotificationNameTbox.sendKeys(name);
        pageInfo.info("Set Notification name : " + name);
    }

    public void clickUpdateSubmitButton() {
        updateSubmitButton.click();
        pageInfo.info("Click on Submit button");
    }

    public void selectStatusByValue(String status) {
        Select select = new Select(updateStatusDdown);
        select.selectByValue(status);
        pageInfo.info("Select Status by value : " + status);
    }

    public void clickUpdateConfirmButton() {
        updateConfirmButton.click();
        pageInfo.info("Click on Confirm button");
    }

}
