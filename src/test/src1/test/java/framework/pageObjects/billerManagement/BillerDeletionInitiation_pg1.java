package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class BillerDeletionInitiation_pg1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static BillerDeletionInitiation_pg1 page;
    @FindBy(id = "deleteMerchant_input_button_delete")
    WebElement Add;
    @FindBy(id = "autoform3_button_confirm")
    WebElement confirm;
    @FindBy(id = "autoform3_deletionRemark")
    WebElement remarks;

    public static BillerDeletionInitiation_pg1 init(ExtentTest t1) throws Exception {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        page = PageFactory.initElements(driver, BillerDeletionInitiation_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void NavigateToLink() {

        try {
            fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CDEL");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail("link not found");
        }


    }


    public void selectuser(String bcode) {

        driver.findElement(By.xpath("//tr/td[contains(text(),'" + bcode + "')]/ancestor::tr[1]/td/input[@type='radio']")).click();
        Add.click();

    }

    public void confirm() {
        remarks.clear();
        remarks.sendKeys("remarks");
        confirm.click();

    }


}
