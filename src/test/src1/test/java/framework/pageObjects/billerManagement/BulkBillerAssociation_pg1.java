package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rahul.rana on 9/21/2017.
 */
public class BulkBillerAssociation_pg1 extends PageInit {


    @FindBy(xpath = "//*[@id='billerAssociationUpload_inputBulkUpload']/table/tbody/tr[2]/td/a/img")
    WebElement imgDownloadTemplate;

    @FindBy(name = "bulkUploadFile")
    WebElement btnUpload;

    @FindBy(id = "billerAssociationUpload_inputBulkUpload_button_submit")
    WebElement btnSubmit;

    public BulkBillerAssociation_pg1(ExtentTest t1) {
        super(t1);
    }

    public static BulkBillerAssociation_pg1 init(ExtentTest t1) throws Exception {
        return new BulkBillerAssociation_pg1(t1);
    }

    public BulkBillerAssociation_pg1 navBillerRegistration() throws Exception {
        navigateTo("UTIL_ALL", "UTIL_UTL_BLKBILASSOC", "Bulk BIller Association");
        return this;
    }

    public BulkBillerAssociation_pg1 downloadBulkTemplate() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_BILLER_ASSOC);
        try {
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_BILLER_ASSOC);
            imgDownloadTemplate.click();
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Click On image Download template");
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Thread.sleep(5000);
        } finally {
            Utils.closeUntitledWindows();
        }
        return this;
    }

    public BulkBillerAssociation_pg1 uploadBulkFile(String file) throws InterruptedException {
        btnUpload.sendKeys(file);
        pageInfo.info("Set the file for upload - " + file);
        btnSubmit.click();
        pageInfo.info("click on submit button");
        Thread.sleep(3000);
        return this;
    }


}
