package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by rahul.rana on 9/20/2017.
 */
public class AddBillerValidation_pg1 extends PageInit {
    @FindBy(id = "addBillerVal_input_button_addValidations")
    WebElement btnAddValidation;

    public AddBillerValidation_pg1(ExtentTest t1) {
        super(t1);
    }

    public static AddBillerValidation_pg1 init(ExtentTest t1) {
        return new AddBillerValidation_pg1(t1);
    }

    public AddBillerValidation_pg1 navAddBillerValidation() throws Exception {
        navigateTo("UTIL_ALL", "UTIL_UTL_CAVAL", "Add Biller Validation");
        return this;
    }

    public AddBillerValidation_pg1 selectBillerToAddValidation(String bcode) {
        WebElement elem = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(),'" + bcode + "')]/ancestor::tr[1]/td/input[@type='radio']")));
        clickOnElement(elem, "Radio Button, Biller Code: "+ bcode);
        return this;
    }

    public AddBillerValidation_pg2 clickAddValidation() throws Exception {
        clickOnElement(btnAddValidation, "Button Add Validation");
        return AddBillerValidation_pg2.init(pageInfo);
    }

}
