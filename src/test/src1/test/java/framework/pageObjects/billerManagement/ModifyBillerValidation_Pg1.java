package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ModifyBillerValidation_Pg1 extends PageInit {

    @FindBy(id = "modifyBillerVal_input_button_modifyValidations")
    WebElement btnModifyValidation;


    public ModifyBillerValidation_Pg1(ExtentTest t1) {
        super(t1);
    }

    public void navModifyBillerValidation() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CMVAL");
        pageInfo.info("Navigate to Modify Biller Validation");
    }

    public void selectBillerToModifyValidation(String bCode) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + bCode + "')]/ancestor::tr[1]/td/input[@type='radio']")).click();
        pageInfo.info("Click on Radio button corresponding to Biller Code - " + bCode);
    }

    public void clickModifyBtn() {
        btnModifyValidation.click();
        pageInfo.info("Clicked on Modify Button");
    }

}
