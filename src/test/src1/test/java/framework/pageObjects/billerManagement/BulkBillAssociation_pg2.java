package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BulkBillAssociation_pg2 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static BulkBillAssociation_pg2 page;
    @FindBy(xpath = "//*[contains(@id,'inputBulkUpload_action')]/table/tbody/tr[1]/td/a/img")
    WebElement imgDownloadLog;

    public static BulkBillAssociation_pg2 init(ExtentTest t1) throws Exception {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        page = PageFactory.initElements(driver, BulkBillAssociation_pg2.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void clickOnDownloadLogs() throws InterruptedException {
        imgDownloadLog.click();
        pageInfo.info("Click on Download Logs Image");
        Thread.sleep(5000); // TODO - improve this static wait
    }

    public BulkBillAssociation_pg2 downloadLogs() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_BILLER_ASSOC);
        try {
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_BILLER_ASSOC);
            imgDownloadLog.click();
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Click On image Download template");
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        } finally {
            Utils.closeUntitledWindows();
        }
        return this;
    }

}
