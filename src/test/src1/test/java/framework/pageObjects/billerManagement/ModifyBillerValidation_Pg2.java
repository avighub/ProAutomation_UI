package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class ModifyBillerValidation_Pg2 extends PageInit {

    @FindBy(id = "validationList1type")
    WebElement validationtypeDdown;
    @FindBy(id = "modifyBillerConfirm_validationList_0__min")
    WebElement minTbox;
    @FindBy(id = "modifyBillerConfirm_validationList_0__max")
    WebElement maxTbox;
    @FindBy(name = "action:modifyBillerVal_confirmValDetails")
    WebElement submitButton;
    @FindBy(id = "modifyBillerVal_confirmValDetails_submit")
    WebElement confirmButton;

    public ModifyBillerValidation_Pg2(ExtentTest t1) {
        super(t1);
    }

    public void selectValidationType(String type) {
        Select sel = new Select(validationtypeDdown);
        sel.selectByValue(type);
        pageInfo.info("Selecting Validation Type" + type);
    }


    public void setMinValue(String val) {
        minTbox.clear();
        minTbox.sendKeys(val);
        pageInfo.info("Set value to Min Text Box : " + val);
    }

    public void setMaxValue(String val) {
        maxTbox.clear();
        maxTbox.sendKeys(val);
        pageInfo.info("Set value to Max Text Box : " + val);
    }

    public void clickSubmitBtn() {
        submitButton.click();
        pageInfo.info("Clicked on Submit Button");
    }

    public void clickConfirmBtn() {
        confirmButton.click();
        pageInfo.info("Clicked on Confirm Button");
    }


}
