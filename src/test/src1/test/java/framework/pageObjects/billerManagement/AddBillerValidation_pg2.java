package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.BillValidation;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by rahul.rana on 9/20/2017.
 */
public class AddBillerValidation_pg2 extends PageInit {
    @FindBy(name = "action:addBillerVal_addMoreValidations")
    WebElement addMoreValidation;
    @FindBy(name = "action:addBillerVal_getValidationDetails")
    WebElement btnSubmit;
    @FindBy(id = "addBillerVal_getValidationDetails_submit")
    WebElement btnConfirm;
    @FindBy(id = "validationList1type")
    WebElement defaultValidationType;
    @FindBy(id = "addBillerForm_validationList_0__min")
    WebElement defaultMinTbox;
    @FindBy(id = "addBillerForm_validationList_0__max")
    WebElement defaultMaxTbox;
    @FindBy(id = "validationList1startsWith")
    WebElement defaultStartsTbox;
    @FindBy(id = "validationList1contains")
    WebElement defaultContainsTbox;
    @FindBy(id = "addBillerForm_validationList_0__referenceType")
    WebElement defaultRefTypeDdown;

    public AddBillerValidation_pg2(ExtentTest t1) {
        super(t1);
    }

    public static AddBillerValidation_pg2 init(ExtentTest t1) throws Exception {
        return new AddBillerValidation_pg2(t1);
    }

    public void clickOnAddMore() {
        clickOnElement(addMoreValidation, "Add More Validation");
    }

    public void clickOnSubmit() {
        clickOnElement(btnSubmit, "Button Submit");
    }

    public void clickOnConfirm() {
        clickOnElement(btnConfirm, "Button Confirm");
    }

    /**
     * @param validationList
     */
    public AddBillerValidation_pg2 addBillerValidation(List<BillValidation> validationList) {
        int index = 0;
        for (BillValidation data : validationList) {
            if (!data.Label.equals("DEFAULT")) {
                clickOnAddMore();
            }

            // set the label excluding the default 'AccountNumber'
            if (index != 0) {
                driver.findElement(By.id("addBillerForm_validationList_" + index + "__label")).sendKeys(data.Label);
            }

            new Select(driver.findElement(By.name("validationList[" + index + "].type"))).selectByValue(data.Type);
            driver.findElement(By.name("validationList[" + index + "].min")).sendKeys(data.Min);
            driver.findElement(By.name("validationList[" + index + "].max")).sendKeys(data.Max);

            if (data.StartWith != null)
                driver.findElement(By.name("validationList[" + index + "].startsWith")).sendKeys(data.StartWith);

            if (data.Contains != null)
                driver.findElement(By.name("validationList[" + index + "].contains")).sendKeys(data.Contains);

            new Select(driver.findElement(By.name("validationList[" + index + "].referenceType"))).selectByValue(data.ReferenceType);
            index++;
        }

        // complete the Validation
        clickOnSubmit();
        return this;

    }


    public void selectValidationType(String validationType) {
        selectValue(defaultValidationType, validationType, "Default Validation Type");
    }

    public void selectRefType(String refType) {
        selectValue(defaultRefTypeDdown, refType, "Default Reference Type");
    }

    public void setMinValue(String minValue) {
        setText(defaultMinTbox, minValue, "Default Min Value");
    }

    public void setMaxValue(String maxValue) {
        setText(defaultMaxTbox, maxValue, "Default Max Value");
    }

    public void setStartsWith(String startsWith) {
        setText(defaultMinTbox, startsWith, "Start With");
    }

    public void setContains(String contains) {
        setText(defaultMinTbox, contains, "Contains");
    }


}
