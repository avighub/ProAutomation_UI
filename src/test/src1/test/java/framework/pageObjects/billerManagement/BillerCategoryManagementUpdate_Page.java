/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: automation.team
 *  Date: 7-Dec-2017
 *  Purpose: Biller Management Update Page Objects
 */
package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class BillerCategoryManagementUpdate_Page extends PageInit {

    @FindBy(id = "MerchantCategoryAction_ShowMerchantCategoryDetail_button_update")
    private WebElement updateButton;
    @FindBy(id = "MerchantCategoryAction_updateMerCategory_categoryName")
    private WebElement updateNameTbox;
    @FindBy(id = "MerchantCategoryAction_updateMerCategory_description")
    private WebElement updateDescTbox;
    @FindBy(id = "MerchantCategoryAction_updateMerCategory_status")
    private WebElement updateStatusDdown;
    @FindBy(id = "MerchantCategoryAction_updateMerCategory_button_confirm")
    private WebElement confirmButton;
    @FindBy(id = "MerchantCategoryAction_confirmUpdateMerCategory_button_confirm")
    private WebElement finalConfirmButton;
    @FindBy(id = "MerchantCategoryAction_goBack_button_back")
    private WebElement backButton;
    @FindBy(id = "MerchantCategoryAction_confirmUpdateMerCategory_button_back")
    private WebElement finalBackButton;
    @FindBy(name = "categoryName")
    private WebElement confirmPageCategoryName;
    @FindBy(name = "categoryCode")
    private WebElement confirmPageCategoryCode;
    @FindBy(name = "description")
    private WebElement confirmPageDescription;

    public BillerCategoryManagementUpdate_Page(ExtentTest t1) {
        super(t1);
    }

    public void setCategoryName(String text) {
        updateNameTbox.clear();
        updateNameTbox.sendKeys(text);
        pageInfo.info("Set caegory Name - " + text);
    }

    public void setDescription(String text) {
        updateDescTbox.clear();
        updateDescTbox.sendKeys(text);
        pageInfo.info("Set Category's Description - " + text);
    }

    public void clickConfirmButton() {
        confirmButton.click();
        pageInfo.info("Click On Confirm Category!");
    }

    public void clickFinalConfirmButton() {
        finalConfirmButton.click();
        pageInfo.info("Click On Final Confirm Button!");
    }

    public void selectStatus(String status) {
        Select stat = new Select(updateStatusDdown);
        stat.selectByValue(status);
        pageInfo.info("Select Status :" + status);
    }

    public void verifyUpdatePageDetails(String name, String code, String desc) {
        //TODO

    }

    public String getConfirmPageCatName() {
        return confirmPageCategoryName.getAttribute("value");
    }

    public String getConfirmPageCatCode() {
        return confirmPageCategoryCode.getAttribute("value");
    }

    public String getConfirmPageDesc() {
        return confirmPageDescription.getAttribute("value");
    }

    public void clickOnConfirmBack() {
        finalBackButton.click();
        pageInfo.info("Click On Confirm Page Back Button!");
    }

    public void clickOnBack() {
        backButton.click();
        pageInfo.info("Click On Update Page Back Button!");
    }

    public boolean isUpdateButtonDisplayed() {
        return updateButton.isDisplayed();
    }

    public boolean isBackButtonDisplayed() {
        return backButton.isDisplayed();
    }


}
