package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.NumberConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BillUpload_pg1 extends PageInit {

    @FindBy(name = "bulkUploadFile")
    private WebElement uploadBtn;

    @FindBy(id = "manualUpload_inputBulkUpload_button_submit")
    private WebElement submit;

    @FindBy(className = "wwFormTableC")
    private WebElement webTable;


    public BillUpload_pg1(ExtentTest t1) {
        super(t1);
    }

    public static BillUpload_pg1 init(ExtentTest t1) throws Exception {
        return new BillUpload_pg1(t1);
    }

    public boolean isUploadSuccessful() throws Exception {
        Utils.putThreadSleep(NumberConstants.SLEEP_5000);
        if (webTable.findElements(By.xpath(".//tr/td[contains(text(), 'Download Upload Report')]")).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public BillUpload_pg1 navBillUpload() throws Exception {
        navigateTo("UTIL_ALL", "UTIL_BILL_UP", "Bill Upload Page");
        return this;
    }

    public BillUpload_pg1 uploadBulkAssociationFile(String filename) {
        setText(uploadBtn, filename, "Upload File");
        return this;
    }

    public BillUpload_pg1 clickSubmit() {
        clickOnElement(submit, "Submit");
        return this;
    }

    public void clickOnDownloadReport() {
        WebElement elem = webTable.findElement(By.tagName("a"));
        clickOnElement(elem, "Download report");
    }

}
