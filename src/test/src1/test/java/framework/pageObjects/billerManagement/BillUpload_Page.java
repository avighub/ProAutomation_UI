/*
package common.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import common.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import tests.core.base.TestInit;

*/
/**
 * Created by surya.dhal on 11/19/2017.
 *//*

public class BillUpload_Page extends PageInit{

    public BillUpload_Page(ExtentTest t1) {
        super(t1);
    }



        @FindBy(id = "activateMerchant_input_button_activate")
        WebElement activateButton;

        @FindBy(id = "manualUpload_inputBulkUpload_button_submit")
        WebElement submitButton;

        @FindBy(name = "bulkUploadFile")
        WebElement fileUpload;

        public void navBillUpload() throws Exception {
            fl.leftNavigation("UTIL_ALL", "UTIL_BILL_UP");
            pageInfo.info("Navigate to Bill Upload Page!");
        }

        public void uploadFile(String bill){
            fileUpload.sendKeys(bill);
            pageInfo.info("Uploading bill." + bill);
        }

        public void clickSubmitButton() {
            submitButton.click();
            pageInfo.info("Click on Submit button");
        }


}
*/
