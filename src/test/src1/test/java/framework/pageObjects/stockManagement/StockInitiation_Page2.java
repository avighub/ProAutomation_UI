package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by vandana.rattan on 12/12/2018.
 */
public class StockInitiation_Page2 extends PageInit {

    @FindBy(id = "stockButt")
    private WebElement confirm;
    @FindBy(xpath = "//*[@id='stockInit_confirmInitiate']/table/tbody/tr[1]/td[2]")
    private WebElement requesterName;
    @FindBy(xpath = "//*[@id='stockInit_confirmInitiate']/table/tbody/tr[2]/td[2]")
    private WebElement refNo;
    @FindBy(xpath = "//*[@id='stockInit_confirmInitiate']/table/tbody/tr[5]/td[2]")
    private WebElement stockFrom;
    @FindBy(xpath = "//*[@id='stockInit_confirmInitiate']/table/tbody/tr[6]/td[2]")
    private WebElement Amount;
    @FindBy(xpath = "//*[@id='stockInit_confirmInitiate']/table/tbody/tr[5]/td[3]")
    private WebElement Wallet;
    @FindBy(xpath = "//*[@id='stockInit_confirmInitiate']/table/tbody/tr[7]/td[2]")
    private WebElement Remarks;

    public StockInitiation_Page2(ExtentTest t1) {
        super(t1);
    }

    public static StockInitiation_Page2 init(ExtentTest t1) {
        return new StockInitiation_Page2(t1);
    }

    public void clickConfirm() {
        confirm.click();
        ;
    }

    public String getRequesterName() {
        return requesterName.getText();
    }

    public String getRefNo() {
        return refNo.getText();
    }

    public String getstockFrom() {
        return stockFrom.getText();
    }

    public String getAmount() {
        return Amount.getText();
    }

    public String getRemarks() {
        return Remarks.getText();
    }

  /*  public String getWallet()
    {
        return Wallet.getText();
    }*/


}
