/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Limit Page 1
*/


package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.math.BigDecimal;

public class StockLimit_Page1 extends PageInit {


    //Page Objects
    @FindBy(id = "mfsProvidersList")
    private WebElement providerName;

    @FindBy(id = "StockApprovalTransferLimitAction_insertApprovalLimit_approvalLimit1")
    private WebElement stockLimit;

    @FindBy(id = "StockApprovalTransferLimitAction_insertApprovalLimit_button_submit")
    private WebElement submit;


    public StockLimit_Page1(ExtentTest t1) {
        super(t1);
    }

    public static StockLimit_Page1 init(ExtentTest t1) {
        return new StockLimit_Page1(t1);
    }

    /**
     * Navigate to Stock Management >  Stock Limit page
     *
     * @throws Exception
     */
    public void navigateToStockLimitPage() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_LMT", "Stock Limit Page");
    }

    //Select Provider
    public void selectProviderName(String provider) throws Exception {
        selectVisibleText(this.providerName, provider, "providerName");
    }

    /**
     * Setting Stock Limit
     *
     * @param text
     * @throws Exception
     */
    public void setStockLimit(String text) throws Exception {
        setText(stockLimit, text, "stockLimit");
    }

    public BigDecimal getStockLimitUI() throws Exception {
        return new BigDecimal(stockLimit.getAttribute("value"));
    }

    //Click Submit button
    public void clickSubmit() {
        clickOnElement(submit, "Submit");
    }
}
