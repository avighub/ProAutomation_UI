/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of Reimbursement Page1
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.Constants;
import gherkin.lexer.Th;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class Reimbursement_page1 extends PageInit {

    @FindBy(id = "optInstTypeId1")
    WebElement optInstTypeWallet;
    @FindBy(id = "optInstTypeId2")
    WebElement optInstTypeBank;
    @FindBy(id = "availableBanksId")
    WebElement bank;
    @FindBy(id = "userType1")
    private WebElement type;
    @FindBy(id = "accountNumber1")
    private WebElement msisdn;
    @FindBy(id = "paymentMethodNumber2")
    private WebElement companyCode;
    @FindBy(id = "paymentMethodNumber1")
    private WebElement accountNumber;
    @FindBy(id = "providerListSel")
    private WebElement provider;
    @FindBy(id = "otherWalletType")
    private WebElement wallet;
    @FindBy(id = "withdraw_confirmDisplay_referenceNumber")
    private WebElement refNumber;
    @FindBy(id = "withdraw_confirmDisplay_remark")
    private WebElement remark;

    @FindBy(xpath = "//label[@for='withdraw_confirmDisplay_remark']/span")
    private WebElement remarkLabel;


    @FindBy(id = "withdraw_confirmDisplay_button_submit")
    private WebElement submit;


    public Reimbursement_page1(ExtentTest t1) {
        super(t1);
    }

    public Reimbursement_page1 navigateToLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_REMB", "Stock Reimbursement");
        return this;
    }

    public Reimbursement_page1 type_SelectIndex(int index) {
        selectIndex(type, index, "User Type");
        return this;
    }

    public Reimbursement_page1 type_SelectValue(String text) throws Exception {
        selectValue(type, text, "User Type");
        Thread.sleep(Constants.MAX_WAIT_TIME);
        return this;
    }

    public Reimbursement_page1 msisdn_SetText(String text) {
        setText(msisdn, text, "msisdn Field");
        return this;
    }

    public Reimbursement_page1 companyCodeSetText(String text) {
        setText(companyCode, text, "companyCode");
        return this;
    }

    public Reimbursement_page1 accountNumber_SetText(String text) {
        setText(accountNumber, text, "accountNumber");
        return this;
    }

    public void clickOnProvider() {
        clickOnElement(provider, "Provider");
    }

    /**
     * Element neet to be click to load it
     *
     * @throws InterruptedException
     */
    public Reimbursement_page1 provider_SelectIndex() throws Exception {
        Thread.sleep(2000);
        provider.click();

        Select s_provider = new Select(provider);
        if (s_provider.getOptions().size() > 1)
            s_provider.selectByIndex(1);
        else
            s_provider.selectByIndex(0);
        pageInfo.info("Selected Provider @Index");
        return this;
    }

    public void provider_SelectValue(String value) {
        clickOnElement(provider,"Provider Dropdown");
        selectValue(provider, value, "Provider");
    }

    public void wallet_SelectValue(String text) {
        selectValue(wallet, text, "wallet");
    }

    public void wallet_SelectIndex(int index) {
        selectIndex(wallet, index, "wallet");
    }

    public Reimbursement_page1 refNumber_SetText(String text) {
        setText(refNumber, text, "refNumber");
        return this;
    }

    public Reimbursement_page1 remark_SetText(String text) {
        setText(remark, text, "remark");
        return this;
    }

    public Reimbursement_page1 submit_Click() throws Exception {
        Thread.sleep(2000);
        clickOnElement(submit, "submit");
        return this;
    }

    public String getMSISDNValue() {
        String value = msisdn.getAttribute("value");
        return value;
    }

    public String getRefNoValue() {
        String value = refNumber.getAttribute("value");
        return value;
    }

    public String getRemarksValue() {
        String value = remark.getAttribute("value");
        return value;
    }

    public void selectOptInstTypeWallet() {
        clickOnElement(optInstTypeWallet, "Select Wallet As Operator Payment Instrument");
    }

    public void selectOptInstTypeBank() {
        clickOnElement(optInstTypeBank, "Select Wallet As Operator Payment Instrument");
    }

    public void selectBank(String bankId) {
        selectVisibleText(bank, bankId, "Select Bank");
    }

    public void wallet_CheckValue(String text) {
        wallet.click();
        Select sel = new Select(wallet);
        List<WebElement> opt = sel.getOptions();
        for (WebElement option : opt) {
            if (option.getText().equalsIgnoreCase(text)) {
                pageInfo.fail("Deleted wallet is displayed in drop down");
            } else {
                pageInfo.pass("Deleted wallet is not displayed in drop down");
            }
        }
    }

    public WebElement getRemarkLabelWebElement(){
        return remarkLabel;
    }

}
