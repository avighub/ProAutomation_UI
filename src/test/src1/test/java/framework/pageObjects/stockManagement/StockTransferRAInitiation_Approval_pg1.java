package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StockTransferRAInitiation_Approval_pg1 extends PageInit {


    //Initiate Page Objects
    @FindBy(name = "_refNo")
    WebElement refNo;
    @FindBy(name = "button.submit")
    WebElement submitBtn;
    @FindBy(id = "stockButt")
    WebElement confirmBtn;
    @FindBy(id = "stockInitLoyalty_confirmInitiate_providerName")
    WebElement provider;
    @FindBy(id = "stockLoyaltyApprove1_approve__approverRemarks")
    WebElement remarks;
    @FindBy(id = "stockLoyaltyApprove2_approve__approverRemarks")
    WebElement remarks2;
    @FindBy(name = "button.submit")
    WebElement confirmSubmit;
    @FindBy(id = "stockLoyaltyApprove1_approve_button_approve")
    WebElement confirmApprove;
    @FindBy(id = "stockInitLoyalty_confirmInitiate_bankId")
    WebElement bankAccount;
    //Approve Page Objects
    @FindBy(name = "_requestedQuantity")
    private WebElement reqAmount;
    @FindBy(xpath = "//input[@id=\"stockLoyaltyApprove2_approve_button_approve\"]")
    private WebElement confirmApprove2;

    public StockTransferRAInitiation_Approval_pg1(ExtentTest t1) {
        super(t1);
    }

    public static StockTransferRAInitiation_Approval_pg1 init(ExtentTest t1) {
        return new StockTransferRAInitiation_Approval_pg1(t1);
    }

    public void selectBankAccount(String value) {
        selectVisibleText(bankAccount, value, "bank Account");
    }

    public void selectProvider(String value) {
        selectVisibleText(provider, value, "Provider Name");
    }

    public void navigateToInitLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_LOYALTY_INI", "Stock Transfer to RA Initiation");
    }

    public void refNo_SetText(String text) {
        setText(refNo, text, "Reference Number");
    }

    public void requestAmount_SetText(String text) {
        setText(reqAmount, text, "RequestAmount");
    }

    public void clickOnSubmit() {
        clickOnElement(submitBtn, "Submit Button");
    }

    public void clickOnConfirm() {
        clickOnElement(confirmBtn, "Confirm Button");
    }


    public void navigateToApproveLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_LOYALTY_APPR1", " Stock Transfer to RA Approval");
    }

    public void clickRadioBtn(String txnid) {
        driver.findElement(By.xpath("//input[@name='check' and contains(@value,'" + txnid + "')]")).click();
        pageInfo.info("Click on the iniated stock");
    }

    public void clickOnSubmitBtn() {
        clickOnElement(confirmSubmit, "Submit Button");
    }


    public void clickOnApproveBtn() {
        clickOnElement(confirmApprove, "Confirm Button");

    }

    public void setRemarks(String value) {
        setText(remarks, value, "Remarks");
    }

}
