/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Limit EA Page1
*/


package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class StockLimitEA_Page1 extends PageInit {


    //Page Objects
    @FindBy(id = "mfsProvidersList")
    private WebElement providerName;

    @FindBy(name = "approvalLimit1")
    private WebElement stockLimit;

    @FindBy(name = "button.submit")
    private WebElement submit;


    public StockLimitEA_Page1(ExtentTest t1) {
        super(t1);
    }

    public static StockLimitEA_Page1 init(ExtentTest t1) {
        return new StockLimitEA_Page1(t1);
    }

    /**
     * Navigate to Stock Management >  Stock Limit page
     *
     * @throws Exception
     */
    public void navigateToStockLimitPage() throws Exception {
        fl.leftNavigation("STOCK_ALL", "STOCK_STR_LMT");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Stock Management >  Stock Limit EA Page");
    }

    //Select Provider
    public void selectProviderName(String providerName) throws Exception {
        Select sel = new Select(this.providerName);
        sel.selectByVisibleText(providerName);
        pageInfo.info("Selected Provider: " + providerName);
    }

    /**
     * Setting Stock Limit
     *
     * @param text
     * @throws Exception
     */
    public void setStockLimit(String text) throws Exception {
        String value = stockLimit.getAttribute("value");
        if (value != null) {
            stockLimit.clear();
        }
        stockLimit.sendKeys(text);
        pageInfo.info("Set Stock Limit:" + text);
    }

    public String getStockLimitFromUI() throws Exception {
        return stockLimit.getAttribute("value");
    }

    //Click Submit button
    public void clickSubmit() {
        submit.click();
        pageInfo.info("Click on Submit");
    }
}
