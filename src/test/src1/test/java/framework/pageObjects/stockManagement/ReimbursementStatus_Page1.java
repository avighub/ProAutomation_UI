/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of Reimbursement Staus Page1
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ReimbursementStatus_Page1 extends PageInit {

    @FindBy(id = "withdraw_confirmDisplay_button_submit")
    private WebElement Submit;

    public ReimbursementStatus_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_REST", "Reimburstment");
    }

    public void clickSubmitButton() {
        clickOnElement(Submit, "Submit Button");
    }

    public void selectTransactionID(String trID) {
        WebElement tid = driver.findElement(By.xpath("//tr/td[contains(text(),'" + trID + "')]/ancestor::tr[1]/td[4]"));
        tid.getText();
    }

    public String getTransactionID(String trID) {
        WebElement tid = driver.findElement(By.xpath("//tr/td[contains(text(),'" + trID + "')]/ancestor::tr[1]/td[4]"));
        return tid.getText();
    }

    public String getTransactionStatusWeb(String trID) {
        WebElement txnStatus = driver.findElement(By.xpath("//tr/td[contains(text(),'" + trID + "')]/ancestor::tr[1]/td[6]"));
        return txnStatus.getText();
    }

    public String getRequesterName(String trID) {
        WebElement requesterName = driver.findElement(By.xpath("//tr/td[contains(text(),'" + trID + "')]/ancestor::tr[1]/td[1]"));
        return requesterName.getText();
    }

    public String getTransferDate(String trID) {
        WebElement transferDate = driver.findElement(By.xpath("//tr/td[contains(text(),'" + trID + "')]/ancestor::tr[1]/td[2]"));
        return transferDate.getText();
    }

    public String getReferenceNum(String trID) {
        WebElement referenceNum = driver.findElement(By.xpath("//tr/td[contains(text(),'" + trID + "')]/ancestor::tr[1]/td[3]"));
        return referenceNum.getText();
    }

    public String getTransferAmount(String trID) {
        WebElement transferamount = driver.findElement(By.xpath("//tr/td[contains(text(),'" + trID + "')]/ancestor::tr[1]/td[5]"));
        return transferamount.getText();
    }

  /* public String getCurrentStatus(String trID) {
        WebElement currentStatus = driver.findElement(By.xpath("//tr/td[contains(text(),'" + trID + "')]/ancestor::tr[1]/td[6]"));
        return currentStatus.getText();
    }*/

}
