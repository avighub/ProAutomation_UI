/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Transfer Initiate Page1
*/


package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StockTransferInitiatePage1 extends PageInit {


    @FindBy(id = "mfsProvidersList")
    private WebElement mfsProvidersList;
    @FindBy(id = "stockInit_confirmInitiate__refNo")
    private WebElement refNo;
    @FindBy(id = "stockInit_confirmInitiate__requestedQuantity")
    private WebElement requestAmount;
    @FindBy(id = "stockInit_confirmInitiate__requesterRemarks")
    private WebElement remark;
    @FindBy(id = "stockInit_confirmInitiate_button_submit")
    private WebElement submitButtonStockInitiate;
    @FindBy(id = "stockButt")
    private WebElement confirmButtonStockInitiate;

    public StockTransferInitiatePage1(ExtentTest t1) {
        super(t1);
    }

    public void navStockTransferInitiatePage() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_INIT", "Stock Initiate Page");
    }

    public void selectMfsProvider(String provider) {
        selectValue(mfsProvidersList, provider, "mfsProvidersList");
    }

    public void refNo_SetText(String text) {
        setText(refNo, text, "Reference Number");
    }

    public void requestAmount_SetText(String text) {
        setText(requestAmount, text, "Request Amount");
    }

    public void remark_SetText(String text) {
        setText(remark, text, "Remarks");
    }

    public void clickOnSubmitTransferInitiate() {
        clickOnElement(submitButtonStockInitiate, "Submit Button");
    }

    public void clickOnConfirmTransferInitiate() {
        clickOnElement(confirmButtonStockInitiate, "confirm Button");
    }
}
