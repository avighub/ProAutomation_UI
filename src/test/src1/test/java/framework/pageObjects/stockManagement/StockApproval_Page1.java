/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of Stock Approval 1
*/
package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class StockApproval_Page1 extends PageInit {


    //Page Objects
    @FindBy(id = "stockApprove1_approve_button_submit")
    private WebElement submit;

    @FindBy(id = "stockApprove1_confirmApproval_button_approve")
    private WebElement approve;

    @FindBy(xpath = "//input[@id='stockApprove1_confirmApproval__approvedStock]")

    private WebElement amount;


    @FindBy(id = "stockApprove1_confirmApproval_button_reject")
    private WebElement reject;

    @FindBy(id = "stockApprove1_confirmApproval__approverRemarks")
    private WebElement remarks;


    @FindBy(id = "stockApprove1_confirmApproval_back")
    private WebElement back;


    public StockApproval_Page1(ExtentTest t1) {
        super(t1);
    }

    public static StockApproval_Page1 init(ExtentTest t1) {
        return new StockApproval_Page1(t1);
    }

    public void clickOnBackButton() {
        clickOnElement(back, "Back Button");
    }

    /**
     * Navigate to Stock Management >  Stock Approval-1 page
     *
     * @throws Exception
     */
    public void navigateToStockApproval1Page() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_APP1", "Stock Approval-1 Page");
    }

    //Select transaction ID
    public void selectRadioButton(String transactionId) {
        driver.findElement(By.xpath(".//*[@value='" + transactionId + "']")).click();
        pageInfo.info("Select Raadio Button of Transaction id " + transactionId);
    }

    public String getDate(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[2]"));
        return el.getText();
    }

    public String getReferenceNumber(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[3]"));
        return el.getText();
    }

    public String getTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[4]"));
        return el.getText();
    }

    public String getCurrentStatus(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[6]"));
        return el.getText();
    }

    //Click Submit button
    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }

    //Click Approve button
    public void clickApprove() {
        clickOnElement(approve, "Approve Button");
    }

    public void clickOnReject() {
        clickOnElement(reject, "Reject Button");
    }

    public void enterRemarks(String remark) {
        setText(remarks, remark, "Remarks");
    }

    public List<String> getTransactionIDList() throws Exception {
        List<String> txnIds = new ArrayList<String>();
        List<WebElement> txnIdList = driver.findElements(By.xpath("//table/tbody/tr/td/input[@type='radio']/following::td[4]"));
        for (WebElement txn : txnIdList) {
            txnIds.add(txn.getText());
        }
        return txnIds;
    }

    public Boolean verifyAmount() {
        try {
            String old = amount.getText();
            amount.sendKeys("50");
            if (old.contains(amount.getText())) {
                pageInfo.info("Amount Field is not Editable");
                return true;
            } else {
                pageInfo.fail("Amount Should not be Editable ");
                return false;
            }

        } catch (Exception e) {
            pageInfo.info("Amount Field is not Editable");
        }
        return true;
    }
}
