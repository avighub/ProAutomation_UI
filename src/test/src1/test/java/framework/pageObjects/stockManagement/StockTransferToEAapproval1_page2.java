/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Approval EA level 1 page 2
*/


package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StockTransferToEAapproval1_page2 extends PageInit {

    @FindBy(id = "stockTransferApprove1_confirmApproval_back")
    WebElement back;
    @FindBy(id = "stockTransferApprove1_confirmApproval_button_approve")
    private WebElement submitBtn;

    @FindBy(id = "stockTransferApprove1_confirmApproval_button_reject")
    private WebElement rejectBtn;


    @FindBy(id = "stockTransferApprove1_confirmApproval__approverRemarks")
    private WebElement remarks;


    public StockTransferToEAapproval1_page2(ExtentTest t1) {
        super(t1);
    }

    public void submitBtn_Click_pg2() {
        clickOnElement(submitBtn, "Submit Button");
    }

    public void backClick() {
        clickOnElement(back, "back Button");

    }

    public void enterRemarks(String remark) {
        setText(remarks, remark, "Remarks");
    }

    public void rejectBtn_Click() {
        clickOnElement(rejectBtn, "Reject Button");

    }


}