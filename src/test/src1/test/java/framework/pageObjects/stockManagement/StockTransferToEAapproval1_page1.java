/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Approval EA level 1
*/


package framework.pageObjects.stockManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class StockTransferToEAapproval1_page1 extends PageInit {


    @FindBy(id = "stockTransferApprove1_confirmApproval__partyAccessId")
    private WebElement requester;
    @FindBy(id = "stockTransferApprove1_confirmApproval__stockDateView")
    private WebElement requestedDate;
    @FindBy(id = "stockTransferApprove1_confirmApproval__requestedValue")
    private WebElement requestedAmount;
    @FindBy(id = "stockTransferApprove1_confirmApproval__refNumber")
    private WebElement refNo;
    @FindBy(id = "stockTransferApprove1_confirmApproval_stock_label_transfer_payee")
    private WebElement payeeAccount;
    @FindBy(xpath = "//*[@id='stockTransferApprove1_confirmApproval']/table/tbody/tr[5]/td[3]")
    private WebElement mainAccount;
    @FindBy(xpath = "//*[@id='stockTransferApprove1_confirmApproval']/table/tbody/tr[6]/td[3]")
    private WebElement earningAccount;
    @FindBy(xpath = "//*[@id='stockTransferApprove1_confirmApproval']/table/tbody/tr[7]/td[2]")
    private WebElement oldRemarks;
    @FindBy(xpath = "//*[@type='radio']")
    private WebElement radioBtn;
    @FindBy(id = "stockTransferApprove1_confirmApproval__requestedValue")
    private WebElement amount;
    @FindBy(id = "stockTransferApprove1_approve_button_submit")
    private WebElement submitBtn;
    @FindBy(id = "stockTransferApprove1_confirmApproval_back")
    private WebElement back;

    public StockTransferToEAapproval1_page1(ExtentTest t1) {
        super(t1);
    }

    public void clickOnBackButton() {
        clickOnElement(back, "Back Button");
    }


    public StockTransferToEAapproval1_page1 navigateToLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STR_APP1", "Stock Approval Level 1");
        return this;
    }


    public void radioBtn_Click() {
        clickOnElement(radioBtn, "Radio Button");
    }


    public StockTransferToEAapproval1_page1 selectTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//input[@value='" + transID + "']"));
        clickOnElement(el, "TID:" + transID);
        return this;
    }

    public String getDate(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[2]"));
        return el.getText();
    }

    public String getReferenceNumber(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[3]"));
        return el.getText();
    }

    public String getTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[4]"));
        return el.getText();
    }

    public String getCurrentStatus(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[6]"));
        return el.getText();
    }


    public Boolean verifyAmount() {
        try {
            String old = amount.getText();
            amount.sendKeys("50");
            if (old.contains(amount.getText())) {
                pageInfo.pass("Amount Field is not Editable");
                return false;
            } else {
                pageInfo.fail("Amount Should not be Editable ");
                return true;
            }

        } catch (Exception e) {
            pageInfo.pass("Amount Field is not Editable");
        }
        return true;
    }

    public String getRequester() {
        return getElementText(requester, "Requester");
    }

    public String getRequestedDate() {
        return getElementText(requestedDate, "Requested Date");
    }

    public String getRequestedAmount() {
        return getElementText(requestedAmount, "Requested Amount");
    }

    public String getRefNo() {
        return getElementText(refNo, "Reference Number");
    }

    public String getPayeeAccount() {
        return getElementText(payeeAccount, "Payee Account");
    }

    public String getMainAccountBal() {
        return getElementText(mainAccount, "Main Account Balance");
    }

    public String getEarningAccountBal() {
        return getElementText(earningAccount, "Earning Account Balance");
    }

    public String getOldRemarks() {
        return getElementText(oldRemarks, "Old Remarks");
    }


    /**
     *
     */
    public StockTransferToEAapproval1_page2 submitBtn_Click() {
        clickOnElement(submitBtn, "submitBtn");
        return new StockTransferToEAapproval1_page2(pageInfo);
    }

}