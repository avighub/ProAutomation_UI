package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StockLiquidationApproval_page1 extends PageInit {

        /*
         * Page Objects
         */

    @FindBy(id = "bankId")
    WebElement bankId;

    @FindBy(id = "selectedProviderId")
    WebElement providerId;

    @FindBy(name = "dojo.fromDate")
    WebElement fromDate;

    @FindBy(name = "dojo.toDate")
    WebElement toDate;

    @FindBy(id = "apprSearch_button_download")
    private WebElement btnDownload;


    public StockLiquidationApproval_page1(ExtentTest t1) {
        super(t1);
    }

    public static String convertToApproveDateFormat(String date) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
        Date d = sdf.parse(date);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd'T'HH:MM:ssXXX");

        String formatTime = format.format(d);
        return formatTime;

    }

    /**
     * INIT
     *
     * @return
     */
    public static StockLiquidationApproval_page1 init(ExtentTest t1) {
        return new StockLiquidationApproval_page1(t1);
    }

    public void NavigateToLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_LIQ_APPROVAL1", "Stock Liquidation Approval 1");
    }

    public void SelectFromandToDateForApproval(String fromdate, String todate) throws Exception {
        StockLiquidationApproval_page1.convertToApproveDateFormat(fromdate);
        StockLiquidationApproval_page1.convertToApproveDateFormat(todate);
        WebElement fromDate1 = driver.findElement(By.name("fromDate"));
        WebElement toDate1 = driver.findElement(By.name("toDate"));
        Utils.setValueUsingJs(fromDate1, fromdate);
        Utils.setValueUsingJs(fromDate, fromdate);
        pageInfo.info("Set From Date-" + fromdate);
        Utils.setValueUsingJs(toDate1, todate);
        Utils.setValueUsingJs(toDate, todate);
        pageInfo.info("Set To Date-" + todate);
    }

    public Boolean downloadLiquidationFile() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_STOCK_LIQUIDATION_LOGS);
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_STOCK_LIQUIDATION_LOGS); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
        clickOnElement(btnDownload, "Download Stock Liquidation Entries");
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000);
        String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
        return Utils.isFileDownloaded(oldFile, newFile);
    }

    public void selectBankName(String bank) {
        selectVisibleText(bankId, bank, "Bank Name");
    }

    public void selectProvider(String providerId) {
        selectValue(this.providerId, providerId, "Provider");
    }


}
