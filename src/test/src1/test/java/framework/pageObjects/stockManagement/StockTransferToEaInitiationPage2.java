package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class StockTransferToEaInitiationPage2 extends PageInit {

    @FindBy(id = "stockButt")
    private WebElement submitButton;

    public StockTransferToEaInitiationPage2(ExtentTest t1) {
        super(t1);
    }

    public void submitButton_Click() {
        clickOnElement(submitButton, "submit Button");
    }


}