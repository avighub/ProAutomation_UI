package framework.pageObjects.customerCareManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.rmi.CORBA.Util;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by gurudatta.praharaj on 2/27/2018.
 */
public class CustomerCareExecutive_page1 extends PageInit {

    @FindBy(id = "global_search_transaction_details_id")
    WebElement TransactionTab;

    @FindBy(id = "search_role")
    WebElement externalRefId;

    @FindBy(id = "search_transaction_id")
    WebElement searchByFtxnId;

    @FindBy(id = "search_role")
    WebElement txnId;

    @FindBy(id = "search_transaction_id")
    WebElement searchTxnId;

    @FindBy(id = "td_txnId")
    WebElement fetchTxnId;

    @FindBy(id = "td_txnStatus")
    WebElement fetchTxnStatus;

    @FindBy(id = "user")
    private WebElement user;

    @FindBy(id = "filter")
    private WebElement filter;

    @FindBy(id = "filter_data")
    private WebElement filterData;

    @FindBy(id = "global_search_btn")
    private WebElement button;

    @FindBy(id = "displayDropDown")
    private WebElement down;

    @FindBy(id = "changed_status")
    private WebElement status;

    @FindBy(id = "current_status_form_btn")
    private WebElement finalSubmit;

    @FindBy(id = "reason")
    private WebElement reason;

    @FindBy(xpath = ".//*[@id='BarServicePopUp']/div/div/div[2]/div/div/div/button")
    private WebElement barOkButton;
    @FindBy(xpath = ".//*[@id='UnBarServicePopUp']/div/div/div[2]/div/div/div/button")
    private WebElement unbarOkButton;
    @FindBy(xpath = ".//*[@id='suspendServicePopUp']/div/div/div[2]/div/div/div/button")
    private WebElement suspend_resumr_ok;

    public CustomerCareExecutive_page1(ExtentTest test) {
        super(test);
    }

    public static CustomerCareExecutive_page1 init(ExtentTest test) {
        return new CustomerCareExecutive_page1(test);
    }

    public CustomerCareExecutive_page1 navigateToCCE() throws Exception {
        navigateTo("ENQUIRY_ALL", "ENQUIRY_CCE_ENQUIRY", "Customer Care Executive");
        pageInfo.info("Navigate to Customer Care Executive");

        return this;
    }

    public CustomerCareExecutive_page1 switchToWindow(int index) {
        List<String> winID = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(winID.get(index));
        return this;
    }

    public void clickSuspend_Resume_Ok() {
        clickOnElement(suspend_resumr_ok, "Ok button");
    }

    public CustomerCareExecutive_page1 selectUser(String value) {
        selectValue(user, value, "Select User Dropdown");
        return this;
    }

    public CustomerCareExecutive_page1 selectFilter(String value) {
        selectValue(filter, value, "Select Filter Dropdown");
        return this;
    }

    public CustomerCareExecutive_page1 setFilterData(String filterText) {
        setText(filterData, filterText, "Filter Data Textbox");
        return this;
    }

    public CustomerCareExecutive_page1 clickSubmit() {
        clickOnElement(button, "Submit Button");
        return this;
    }

    public void clickDown() {
        clickOnElement(down, "Down Arrow");
    }

    public CustomerCareExecutive_page1 changeStatus(String statusType) {
        Utils.scrollToBottomOfPage();
        selectValue(status, statusType, "Status Type Dropdown");
        return this;
    }

    public void clickFinalSubmit() throws Exception {
        ((JavascriptExecutor) driver).executeScript("window.scroll(0,10000);");
        clickOnElement(finalSubmit, "Submit Button");
    }

    public void clickOkButton_Bar() {
        clickOnElement(barOkButton, "Ok Button");
    }

    public void clickOkButtonUnbar() {
        clickOnElement(unbarOkButton, "click Ok button");
    }

    public CustomerCareExecutive_page1 verifyNotificationMessage(String expectedMessage) throws Exception {
        String text = driver.findElement(By.xpath("//div[@class='modal fade in']/div/div/div[2]/p")).getText();
        if (text.contains(expectedMessage)) {
            pageInfo.pass(text);
        } else {
            pageInfo.fail("validation failed");
        }
        return this;
    }

    public CustomerCareExecutive_page1 clickOnTransactionsTab() {
        clickOnElement(TransactionTab, "Transactions Tab");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        return this;
    }

    public void searchExternalRefId(String referenceId) {
        setText(externalRefId, referenceId, "Search External Reference Id");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        clickOnElement(searchByFtxnId, "Search Button");
    }

    public void searchTxnId(String transactionId) {
        setText(txnId, transactionId, "Search Transaction");
        clickOnElement(searchTxnId, "Search Button");
    }

    public List<String> getTransactionStatus() {
        List<String> txnStatus = new LinkedList<>();
        List<WebElement> list = driver.findElements(By.xpath("//table[@id='search_by_transaction_id_content']/tbody/tr[2]/td"));
        for (WebElement element : list) {
            String data = element.getText();
            txnStatus.add(data);
        }
        return txnStatus;
    }

    public void setReason(String reasonText) {
        Utils.scrollToBottomOfPage();
        setText(reason, reasonText, "Reason");
    }
}
