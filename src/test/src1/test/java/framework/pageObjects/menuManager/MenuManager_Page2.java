package framework.pageObjects.menuManager;

import com.aventstack.extentreports.ExtentTest;
import framework.features.menuManager.MenuManager;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class MenuManager_Page2 extends PageInit {

    @FindBy(id="addNewMenuAndSubmenu")
    private WebElement addMenuBtn;

    @FindBy(xpath="//*[@id='menuDiv']/input")
    private WebElement enterMenuName;

    @FindBy(xpath="//*[@id='menuSaveDiv']")
    private WebElement saveMenuItem;

    @FindBy(xpath="//*[@id='menuListTable']/tbody/tr[1]/td[12]/img")
    private WebElement saveEditMenu;

    @FindBy(xpath="//*[@id='menuListTable']//td//button[@id='idAssSubMenuLink2']")
    private WebElement assoStaticMenuListSubMenu;

    @FindBy(xpath="//table[@id='menuListTable']//*[@id='idExecServiceLink2']")
    private WebElement terminateLink;

    @FindBy(xpath="//div[@id='headerEntireTextAreaDiv']//*[@id='headerTextValue']")
    private WebElement addSuccessText;

    @FindBy(xpath="//div[@id='headerEntireTextAreaDiv']//*[@id='headerTextValueError']")
    private WebElement addErrorText;

    @FindBy(xpath="//*[@id='saveHeaderTextId']")
    private WebElement saveSuccessText;

    @FindBy(xpath="//*[@id='saveHeaderTextIdError']")
    private WebElement saveErrorText;

    @FindBy(xpath="//*[@id='saveHeaderTextId']/following-sibling::*[@id='editLanguageId']")
    private WebElement editSuccessTextLanguage;

    @FindBy(xpath="//*[@id='saveHeaderTextIdError']/following-sibling::*[@id='editLanguageIdError']")
    private WebElement editErrorTextLanguage;

    @FindBy(xpath="//button[@onclick[contains(.,'validateAndForwardMAndSM')]]")
    private WebElement nextBtnMAndSM;

    @FindBy(xpath="//*[@id='alertify-ok']")
    private WebElement alertOKBtn;

    @FindBy(xpath="//*[@id='menuListTable']/tbody/tr/td[4]/img")
    private WebElement editMenuLabel;

    @FindBy(xpath="//*[@id='saveEditMenu']")
    private WebElement reorderBtn;

    @FindBy(xpath="//*[@id='menuListTable']/tbody/tr[1]/td[2]/div/input")
    private WebElement reorderElement;

    @FindBy(xpath="//*[@id='menuListTable']/tbody/tr[2]/td[2]/div/input")
    private WebElement reorderDestination;

    @FindBy(xpath="//*[@id='approvedMenusTable']/tbody/tr/td[5]")
    private List<WebElement> menuVersions;

    @FindBy(xpath="//*[@id='cloneApprovedMenu'][@onclick='cloneApprovedMenuByCateg();']")
    private WebElement cloneApprovedMenu;

    @FindBy(xpath="//section[@id='alertify-logs']/article")
    private WebElement alertMessage;

    @FindBy(xpath="//*[@id='approvedMenusTable']/tbody/tr[2]/td[6]/button")
    private WebElement cloneButton;

    @FindBy(xpath="//*[@id='associateServiceId']")
    private WebElement associateServiceForGotoMenu;

    @FindBy(xpath="//*[@id='addAssociateServiceId']/img")
    private WebElement plusIconToAddService;

    @FindBy(xpath = "//*[@id='goToMenuButton']/img")
    private WebElement gotoMenu;

    @FindBy(xpath="//*[@id='goToMenuResCodes']")
    private WebElement goToMenuResCode;

    @FindBy(xpath="//*[@id='makerTree']/ul/ul/ul/li/button")
    private WebElement makerTree;

    @FindBy(xpath="//*[@id='select2-assocServicesDropdown-container']")
    private WebElement serviceSelect;

    @FindBy(xpath="//*[@id='select2-assocServicesDropdown-result-n2wl-2050']")
    private WebElement serviceName;

    @FindBy(xpath="//*[@id='addAssociateServicesTable']/tbody/tr[2]/td[1]/button")
    private WebElement constructServiceRequest;

    @FindBy(xpath="//*[@id='addAssociateServicesTable']/tbody/tr[2]/td[4]/img")
    private WebElement saveConstructServiceRequest;

    @FindBy(xpath="//*[@id='saveReqResMappingId']")
    private WebElement saveReqResMapBtn;

    public MenuManager_Page2(ExtentTest t1) {
        super(t1);
    }

    public static MenuManager_Page2 init(ExtentTest t1) {
        return new MenuManager_Page2(t1);
    }

    public void clickAddMenuBtn(){
        pageInfo.info("Trying to click Add Menu button");
        addMenuBtn.click();
    }

    public void enterMenuName(String menuName){
        pageInfo.info("Trying to enter menu name.");
        enterMenuName.clear();
        enterMenuName.sendKeys(menuName);
    }

    public void clickOnSaveMenuItem(){
        pageInfo.info("Trying to click save menu item.");
        saveMenuItem.click();
    }

    public void clickAssociateStaticMenu(){
        pageInfo.info("Trying to click Associate Static Menu List link.");
        assoStaticMenuListSubMenu.click();
    }

    public void clickTerminateLink(){
        pageInfo.info("Trying to click Terminate link");
        terminateLink.click();
        pageInfo.info("Trying to click OK button.");
        clickOnElement(alertOKBtn,"OK");
    }

    public void enterSuccessText(String text){
        pageInfo.info("Trying to enter Success message: "+text);
        addSuccessText.clear();
        addSuccessText.sendKeys(text);
    }

    public void enterErrorText(String text){
        pageInfo.info("Trying to enter Error message: "+text);
        addErrorText.clear();
        addErrorText.sendKeys(text);
    }

    public void clickToSaveSuccessText(){
        pageInfo.info("Trying to click button to save success message.");
        saveSuccessText.click();
    }


    public void clickToSaveErrorText(){
        pageInfo.info("Trying to click button to save error message");
        saveErrorText.click();
    }

    public void clickToEditSuccessTextLang(){
        pageInfo.info("Trying to click button to edit success message in other languages.");
        editSuccessTextLanguage.click();
    }

    public void clickToEditErrorTextLang(){
        pageInfo.info("Trying to click button to edit error message in other languages.");
        editErrorTextLanguage.click();
    }

    public void clickNextMAndSM(){
        pageInfo.info("Trying to click NextMandSM button.");
        nextBtnMAndSM.click();
    }

    public void clickOnEditMenuBtn(){
        clickOnElement(editMenuLabel,"Modify Menu label");
    }

    public void clickReorderBtn(){
        clickOnElement(reorderBtn,"Reorder button");
    }

    public int reorderMenu() {
        List<WebElement> wbs = driver.findElements(By.xpath("//tr//*[@id='menuDiv']/input"));
        int listOfRules = wbs.size();
        if (listOfRules > 1) {
            Actions builder = new Actions(driver);
            Action dragAndDrop = builder.clickAndHold(reorderElement)
                    .moveToElement(reorderDestination)
                    .release(reorderDestination).build();
            dragAndDrop.perform();
        } else if (listOfRules == 1) {
            pageInfo.skip("As there is only single menu available in the system, " +
                    "the reordering of menu is not required.");
        } else
            pageInfo.info("No Menu found to reorder.");
        return listOfRules;
    }

    public void clickCloneApprovedMenu(){
        clickOnElement(cloneApprovedMenu,"Clone Approved Menu");
    }

    public boolean uniqueMenuVersions(){
        String version; boolean found=false;
        ArrayList<String> arrList = new ArrayList<String>();
        for(WebElement x:menuVersions){
            version = x.getText();
            arrList.add(version);
        }
        for (String str : arrList) {
            pageInfo.info("[Version]:"+str);
        }
        for (int i = 0; i < arrList.size(); i++) {
            for (int j = i+1; j < arrList.size(); j++) {
                if(arrList.get(i).equals(arrList.get(j))){
                    pageInfo.info("Duplicate version found: "+arrList.get(i));
                    found = true;
                    break;
                }
                if(found) break;
            }
        }
        return found;
    }

    public String getAlertMessage(){
        String text = alertMessage.getText();
        pageInfo.info("Message found:"+text);
        return text;
    }

    public void clickCloneButton(){
        clickOnElement(cloneButton,"Clone button");
    }

    public void goToMenu(){
        clickOnElement(gotoMenu,"Go to Menu");
    }

    public void enterGoToMenuResCode(String code){
        pageInfo.info("Trying to enter go to Menu response code.");
        goToMenuResCode.sendKeys(code);
    }

    public void selectFromMakerTree(){
        clickOnElement(makerTree,"Select option from maker tree");
    }

    public void clickToSelectService(){
        clickOnElement(serviceSelect,"Service Select");
    }

    public void clickServiceName(){
        clickOnElement(serviceName,"Service Name Select");
    }

    public void clickConstructServiceRequest(){
        clickOnElement(constructServiceRequest,"Contruct Service button");
    }

    public void clickSaveConstructService(){
        clickOnElement(saveConstructServiceRequest,"Save button for service");
    }

    public void enterdetailinmandatory(String value){
       List<WebElement> lwbs = driver.findElements(By.xpath("//button[@ismandatory='Y']/../following-sibling::td[1]"));
       for(WebElement x:lwbs){
           x.clear();
           x.sendKeys(value);
       }
    }

    public void clickSaveReqResBtn(){
        clickOnElement(saveReqResMapBtn,"Save Request Response button");
    }

    public void clickAssociateServiceForGotoMenu(){
        clickOnElement(associateServiceForGotoMenu,"Associate Service Button");
    }

    public void clickPlusIconToAddService(){
        clickOnElement(plusIconToAddService,"Add icon for service");
    }

    public void clickToSaveEditMenu(){
        clickOnElement(saveEditMenu,"Save Changes");
    }
}