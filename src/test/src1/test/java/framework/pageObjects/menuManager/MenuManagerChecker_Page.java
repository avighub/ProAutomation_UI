package framework.pageObjects.menuManager;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MenuManagerChecker_Page extends PageInit {

    @FindBy(xpath="//*[@id='makerListTable']//button[@onclick[contains(.,'validateView')]]")
    private WebElement proceedBtn;

    @FindBy(xpath="//*[@id='makerListTable']//button[@onclick[contains(.,'checkerAction')]]")
    private WebElement checkerAction;

    @FindBy(xpath="//div[@id='approvalModelButtons']//*[@id='submitForApprovalAC']")
    private WebElement approveBtn;

    @FindBy(xpath="//*[@id='continueApprovalModelButtons']/button[@id='submitForApprovalAC']")
    private WebElement continueApprovalBtn;

    @FindBy(xpath="//div[@id='approvalModelButtons']//*[@id='submitForReview']")
    private WebElement rejectBtn;

    @FindBy(xpath="//*[@id='select2-selectDefLangForCheckingDropdown-container']")
    private WebElement selectLanguage;

    @FindBy(xpath="//*[@id='menuTreeDiv']/div[1]/button")
    private WebElement checkerActionAtDiff;

    @FindBy(xpath="//*[@id='makerListTable']/tbody/tr[2]/td[3]")
    private WebElement remarks;

    @FindBy(xpath = "//textarea[@id='remarks']")
    private WebElement enterRemarks;

    public MenuManagerChecker_Page(ExtentTest t1) {
        super(t1);
    }

    public static MenuManagerChecker_Page init(ExtentTest t1) {
        return new MenuManagerChecker_Page(t1);
    }


    public void selectCategory(String category) {
        pageInfo.info("Trying to select Category: "+category);
        driver.findElement(By.xpath("//button[text()='" + category.toUpperCase() + "']")).click();
    }

    public void clickProceedBtn(){
        clickOnElement(proceedBtn,"Proceed button");
    }

    public void clickCheckerActionBtn(){
        clickOnElement(checkerAction,"Approve/Reject button");
    }

    public void clickApproveBtn(){
        clickOnElement(approveBtn,"Approve button");
    }

    public void clickContinueApprovalBtn(){
        clickOnElement(continueApprovalBtn,"Continue Approval Button");
    }

    public void clickRejectBtn(){
        clickOnElement(rejectBtn,"Reject button");
    }
    public void clickSelectLanguage(){
        clickOnElement(selectLanguage,"Select Language");
    }

    public void selectLanguageFromList(String lang){
        WebElement wb = driver.findElement(By.xpath("//ul[@id='select2-selectDefLangForCheckingDropdown-results']//li[text()='"+lang.toUpperCase()+"']"));
        clickOnElement(wb,"Select Language:"+lang);
    }

    public String checkMenuAtChecker(String[] menuElements){
        String flag="false";
        WebElement tree = driver.findElement(By.xpath("//*[@id='checkerTree']/center/u/h5/strong/../../../following-sibling::ul/li/*[text()='Root']/" +
                "../ul/li/*[text()='"+menuElements[0]+"']/../ul/li/*[text()='"+menuElements[1]+"']/" +
                "../ul/li/*[text()='"+menuElements[2]+"']/../ul/li/*[@src='../images/terminate.png']"));
        try{
            tree.isDisplayed();
            flag="true";
        }catch(Exception e){
            pageInfo.info(e);
        }
        return flag;
    }

    public void selectCheckerActionAtDiff(){
        clickOnElement(checkerActionAtDiff,"Approve/Reject Button at Diff");
    }

    public String getRemarks(){
        return remarks.getText();
    }

    public void enterRemarks(String text) {
        pageInfo.info("Trying to enter remarks: "+text);
        enterRemarks.clear();
        enterRemarks.sendKeys(text);
    }
}
