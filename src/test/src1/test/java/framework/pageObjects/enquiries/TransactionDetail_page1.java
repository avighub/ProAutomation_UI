/*
*  COPYRIGHT: Comviva Technologies Pvt. Ltd.
*  This software is the sole property of Comviva
*  and is protected by copyright law and international
*  treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of
*  it may result in severe civil and criminal penalties
*  and will be prosecuted to the maximum extent possible
*  under the law. Comviva reserves all rights not
*  expressly granted. You may not reverse engineer, decompile,
*  or disassemble the software, except and only to the
*  extent that such activity is expressly permitted
*  by applicable law notwithstanding this limitation.
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
*  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
*  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
*  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
*  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
*  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*  Author Name: Automation Team
*  Date: 12-Dec-2017
*  Purpose: Selenium Test Cases
*/

package framework.pageObjects.enquiries;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


/**
 * Page object of Transaction Detail
 *
 * @author navin.pramanik
 */
public class TransactionDetail_page1 extends PageInit {

    //PAGE OBJECTS
    @FindBy(id = "txnAction_loadTxnDetails_txnId")
    private WebElement transactionIDTBox;

    @FindBy(id = "txnAction_loadTxnDetails_button_submit")
    private WebElement submitBtn;

    @FindBy(id = "txnAction_loadTxnDetails_back")
    private WebElement backBtn;

    @FindBy(id = "txnAction_loadTxnDetails_referenceId")
    private WebElement externalReferenceIDTBox;

    public TransactionDetail_page1(ExtentTest t1) {
        super(t1);
    }

    public static String getTransactionID() {
        String txnID = DriverFactory.getDriver().findElement(By.xpath(".//*[@id='txnAction_loadTxnDetails']/table/tbody/tr[3]/td[2]")).getText();
        return txnID;
    }

    /**
     * Method for set transaction ID
     *
     * @param tid
     */
    public void setTransactionID(String tid) {
        setText(transactionIDTBox,tid,"Transaction ID Text Box");
    }

    /**
     * Method for navigate to Link
     */
    public void navigateToLink() throws Exception {
        navigateTo("ENQUIRY_ALL", "ENQUIRY_TXN_DETAILS","Enquiries-> Transaction Details");
    }

    /**
     * Method to click on submit button
     */
    public void clickSubmitButton() {
        submitBtn.click();
        pageInfo.info("Click on Submit Button");
    }

    /**
     * Method to click on back button
     */
    public void clickBackButton() {
        backBtn.click();
        pageInfo.info("Click on Back Button");
    }

    /**
     * Get Transaction Status
     *
     * @param tid
     * @return
     */
    public String getTxnStatus(String tid) {
        return DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + tid + "')]/ancestor::tr[1]/td[3]")).getText();
        }

    public void setExternalReferenceID(String eid) {
        setText(externalReferenceIDTBox, eid, "External ReferenceID");

    }


}
