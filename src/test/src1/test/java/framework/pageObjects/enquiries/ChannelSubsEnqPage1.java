package framework.pageObjects.enquiries;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class ChannelSubsEnqPage1 extends PageInit {

    public ChannelSubsEnqPage1(ExtentTest t1) {
        super(t1);
    }

    @FindBy(name = "userType")
    private WebElement selUserType;

    @FindBy(name = "accessId")
    private WebElement msisdnWebElement;

    @FindBy(id = "partyProviderListSel")
    private WebElement providerDDown;

    @FindBy(xpath = "//input[@type='button']")
    private WebElement submit;

    public void setMSISDN(String msisdn) {
        msisdnWebElement.sendKeys(msisdn);
        pageInfo.info("Set MSISDN as: "+msisdn);
        //* use Methods setText
    }

    @FindBy(xpath = "//a[contains(@href,'userEnquir_generateQRCode.action')]")
    private WebElement generateQRCodeLink;

    public void navigateToChannelSubsEnquiry() throws Exception {
        fl.leftNavigation("ENQUIRY_ALL", "ENQUIRY_ENQ_US");
        pageInfo.info("Navigated to Channel/Subscriber Enquiry Link");
        //* use Methods navigateTo()
    }

    public void selectUserTypeByValue(User usr) {
        if(usr.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)){
            selectValue(selUserType, Constants.USER_TYPE_SUBS, "User Type Subscriber");
        }else{
            selectValue(selUserType, Constants.USER_TYPE_CHANNEL, "User Type Channel");
        }
        /* todo - remove this code
        Select sel = new Select(selUserType);
        sel.selectByValue(text);*/
        //* use Methods selectValue()
    }

    public void clickonSubmit() {
        clickOnElement(submit, "Submit");
    }

    public void clickProviderDdown() {
        providerDDown.click();
        //* use clickOnElement()
    }

    public boolean isGenerateQRCodeLinkVisible(){
        return generateQRCodeLink.isDisplayed();
    }

    public void clickOnGenerateQRCodeLink(){
        generateQRCodeLink.click();
        //* use clickOnElement()
    }
}
