/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 14-Dec-2017
 *  Purpose: page of Channel & subscriber Enquiries
 */
package framework.pageObjects.enquiries;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class ChannelSubsEnq_pg1 extends PageInit {

    @FindBy(name = "userType")
    private WebElement selUserType;
    @FindBy(name = "reset_password")
    private WebElement btnResetPwd;
    @FindBy(name = "accessId")
    private WebElement msisdnWebElement;
    @FindBy(name = "agentCode")
    private WebElement agentCode;
    @FindBy(id = "partyProviderListSel")
    private WebElement providerDDown;
    @FindBy(id = "userEnquir_showUserDetails_userMap_userName")
    private WebElement cWindowFName;
    @FindBy(id = "userEnquir_showUserDetails_accessId")
    private WebElement cWindowMsisdn;
    @FindBy(id = "userEnquir_showUserDetails_accessId")
    private WebElement cWindowSubscriberMsisdn;
    @FindBy(id = "userEnquir_showUserDetails_userMap_categoryCode")
    private WebElement cWindowCatCode;
    @FindBy(id = "userEnquir_showTrnsControlUnits_proId")
    private WebElement cWindowTCPID;
    @FindBy(id = "userEnquir_showTrnsControlUnits_trnCntMap_profileName")
    private WebElement cWindowTCPName;
    @FindBy(id = "userEnquir_showBalance_userBalanceMap_balance")
    private WebElement cWindowUserBalance;
    @FindBy(xpath = "//input[@type='button']")
    private WebElement submit;
    @FindBy(id = "userEnquir_forwardUserPage_button_back")
    private WebElement backButton;
    @FindBy(xpath = "//a[contains(@href,'showUserDetails.action')]")
    private WebElement showUserDetailsLink;
    @FindBy(xpath = "//a[contains(@href,'showUserAccessDetail.action')]")
    private WebElement showUserAccessDetailLink;
    @FindBy(xpath = "//a[contains(@href,'showTrnsControlUnits.action')]")
    private WebElement showTransferControlLink;
    @FindBy(xpath = "//a[contains(@href,'showBalance.action')]")
    private WebElement showBalanceLink;
    /**
     * -------------------------------------- Customer Care Executive ----------------------------------
     */

    @FindBy(id = "ENQUIRY_CCE_ENQUIRY")
    private WebElement CceLink;
    @FindBy(xpath = "//*[@id='user']")
    private WebElement userTypeDropDown;
    @FindBy(id = "filter")
    private WebElement accountIdentifierDropDown;
    @FindBy(id = "global_search_btn")
    private WebElement submitBtn;
    @FindBy(id = "filter_data")
    private WebElement filterDataTxtBox;
    @FindBy(id = "incorrectInputMessage")
    private WebElement errorMsg;
    @FindBy(id = "pinSuccessMessage")
    private WebElement pinSuccessMsg;
    @FindBy(id = "headerOk2")
    private WebElement pinokBtn;
    @FindBy(xpath = "//*[contains(text(),'successfully barred')]")
    private WebElement barStatusSuccessMsg;
    @FindBy(xpath = "//*[contains(text(),'Unbarred')]")
    private WebElement unBarStatusSuccessMsg;
    @FindBy(xpath = "//*[contains(text(),'SUCCESS')]")
    private WebElement suspendStatusSuccessMsg;
    @FindBy(css = ".modal.fade.in")
    private WebElement modalFade;
    @FindBy(xpath = "//a[.='Reset Credentials']")
    private WebElement resetCredentialsBtn;
    @FindBy(id = "sim_swapped_message")
    private WebElement reSetCredentialsErrorMsg;
    @FindBy(id = "mobile_pin_reset")
    private WebElement mobilePinResetRadioBtn;
    @FindBy(id = "m_pin")
    private WebElement mPinRadioBtn;
    @FindBy(id = "t_pin")
    private WebElement tPinRadioBtn;
    @FindBy(id = "reset_pin")
    private WebElement reset_pinBtn;
    @FindBy(id = "msisdnLabel")
    private WebElement MSISDN;
    @FindBy(id = "nameLabel")
    private WebElement Name;

    //---Fields---
    @FindBy(id = "date_of_birth_label")
    private WebElement DateOfBirth;
    @FindBy(id = "addressLabel")
    private WebElement Address;
    @FindBy(id = "agent_code_label")
    private WebElement AgentCodeMerchantCode;
    @FindBy(id = "registered_on_label")
    private WebElement RegisteredOn;
    @FindBy(id = "registered_by_label")
    private WebElement RegisteredBy;
    @FindBy(id = "last_transaction_status_label")
    private WebElement LastTransactionStatus;
    @FindBy(id = "last_transaction_on_label")
    private WebElement LastTransactionOn;
    @FindBy(id = "last_transaction_type_label")
    private WebElement LastTransactionType;
    @FindBy(xpath = "//li[@id='global_search_transaction_details_id']/a")
    private WebElement transactionDetails;
    @FindBy(xpath = "//*[@data-target='#last_transactions_container']")
    private WebElement LastFiveTransactionsDropDown;
    @FindBy(id = "current_customer_status_label")
    private WebElement CurrentStatusOfTheConsumer;
    @FindBy(id = "displayDropDown")
    private WebElement displayDropDown;
    @FindBy(id = "reason")
    private WebElement reasonTxtField;
    @FindBy(id = "changed_status")
    private WebElement StatusDropDown;
    @FindBy(xpath = "//*[@id='current_status_form_btn']")
    private WebElement statuSubmitBtn;
    private String MainWindow;
    @FindBy(id = "resend_sms_btn_txn_id")
    private WebElement resendNotificationButton;
    @FindBy(id = "resend_sms_btn_txn_id_both")
    private WebElement resendNotificationToBothPartiesButton;

    // ----------------------  Enquiries > CCE > Account Information  ------------------------------

    @FindBy(xpath = "//a[.='Account Information']")
    private WebElement accountInformationTab;

    @FindBy(xpath = "//a[.='SVA']")
    private WebElement svaLink;

    @FindBy(xpath = "//a[.='Bank Info']")
    private WebElement bankInfoLink;

    @FindBy(xpath = "//*[.='Normal']/../td[@id='grade_name']")
    private WebElement gradeName;

    @FindBy(id = "global_search_user_details_id")
    private WebElement headerTitle;

    @FindBy(id = "search_role")
    private WebElement searchTxnId;

    @FindBy(id = "search_transaction_id")
    private WebElement searchButton;


    public void clickSubmitResetPassword() {
        clickOnElement(btnResetPwd, "Reset Password");
    }
    public void waitTillHeaderTitleIsShown() {
        wait.until(ExpectedConditions.elementToBeClickable(headerTitle));
    }


    public void clickAccountInformationTab() {
        clickOnElement(accountInformationTab, "Account Information Tab");
    }

    public void clickOnSvaLink() {
        clickOnElement(svaLink, "clickOnSvaLink");
    }

    public void clickOnBankInfoLink() {
        clickOnElement(bankInfoLink, "clickOnSvaLink");
    }

    public String getGradeName() {
        String txt = getElementText(gradeName, "getGradeName");
        return txt;
    }

    public void clickOnAccordionLastFiveTxns() {
        LastFiveTransactionsDropDown.click();
        pageInfo.info("Clicked on LastFiveTransactionsDropDown Button");
    }

    public ChannelSubsEnq_pg1 clickOnTransactionDetails() {
        transactionDetails.click();
        pageInfo.info("Clicked on Transactions Button");
        return this;
    }

    public ChannelSubsEnq_pg1 searchTxnId(String txnId) {
        setText(searchTxnId, txnId, "Search Txn Id");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        clickOnElement(searchButton, "Search Button");
        return this;
    }

    public ChannelSubsEnq_pg1 clickResendNotification() {
        clickOnElement(resendNotificationButton, "Resend Notification");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        return this;
    }

    public ChannelSubsEnq_pg1 clickResendToBothNotification() {
        clickOnElement(resendNotificationToBothPartiesButton, "Resend Notification to Both Parties");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        return this;
    }

    public ChannelSubsEnq_pg1 clickOkButton() {
        clickOnElement(okButton, "Ok Button");
        return this;
    }

    public String getActionMsg() {
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        return notificationMsg.getText();
    }

    //-------------------- CCE > Reset Credentials > Reset Employe Pin ------------------------

    @FindBy(id = "notificationMessage")
    WebElement notificationMsg;

    @FindBy(id = "headerOk1")
    WebElement okButton;

    @FindBy(id = "employeeRole")
    WebElement resetEmployePin;

    @FindBy(id = "mobile_pin_reset5")
    WebElement empMobilePinReSetRadioBtn;

    @FindBy(id = "web_password_reset_radio")
    WebElement passwordRadio;

    public void clickOnResetpwdRadio() {
        clickOnElement(passwordRadio, "Reset Password");
    }

    @FindBy(id = "m_pin5")
    WebElement empMPinRadioBtn;

    @FindBy(id = "t_pin5")
    WebElement empTPinRadioBtn;

    @FindBy(id = "reset_employee_pin")
    WebElement empResetPinBtn;


    //-------------------- CCE > Transaction ---------------------------------------

    @FindBy(xpath = "//li[.='Transaction']")
    WebElement transactionTab;

    @FindBy(xpath = "//*[@class='   accordian_icon collapsed']")
    WebElement last5Transactions;

    @FindBy(id = "resend_sms_btn")
    WebElement resendNotificationBtn;

    public void clickOnTransactionTab() {
        clickOnElement(transactionTab, "Transaction Tab");
    }

    public void clickOnLast5Transactions() {
        clickOnElement(last5Transactions, "last 5 Transactions");
    }

    public void clickOnResendNotificationBtn() {
        clickOnElement(resendNotificationBtn, "Resend Notification");
    }

    //resend_sms_btn
    //-----------------------------------------------------------------------------
    public ChannelSubsEnq_pg1(ExtentTest t1) {
        super(t1);
    }

    public void navChannelSubsEnquiry() throws Exception {
        navigateTo("ENQUIRY_ALL", "ENQUIRY_ENQ_US", "Channel/Subscriber Enquiry");
    }

    public String getNotificationMessage() throws Exception {
        List<WebElement> msgs = driver.findElements(By.className("notification_message"));

        for (WebElement elem : msgs) {
            if (!elem.getText().equals("")) {
                return elem.getText();
            }
        }

        return null;
    }


    public String fetchErrorMessage() {
        return errorMsg.getText();
    }

    public String fetchBarredSuccessMessage() {
        return barStatusSuccessMsg.getText();
    }

    public String fetchUnbarSuccessMessage() {
        return unBarStatusSuccessMsg.getText();
    }

    public String fetchSuspendSuccessMessage() {
        return suspendStatusSuccessMsg.getText();
    }

    public String fetchResetCredentialsErrorMsg() {
        String textValue = reSetCredentialsErrorMsg.getText();
        return textValue;
    }

    public void clickOK() throws Exception {
        if (fl.elementIsDisplayed(modalFade)) {
            clickOnElement(modalFade, "Button OK"); // todo, ok button is inconsistent, hence clicking on modal to fade it away
        } else {
            pageInfo.info("Ok button is not displayed");
        }
    }

    public void mobilePinResetRadioBtn() {
        clickOnElement(mobilePinResetRadioBtn, "Mobile Pin Reset");
    }

    public void mPinRadioBtn() {
        clickOnElement(mPinRadioBtn, "mPinRadioBtn");
    }

    public void tPinRadioBtn() {
        clickOnElement(tPinRadioBtn, "tPinRadioBtn");
    }

    public void submitResetPin() {
        clickOnElement(reset_pinBtn, "submitResetPin");
    }

    public String pinSuccessMsg() {
        String txt = getElementText(pinSuccessMsg, "pinSuccessMsg");
        return txt;
    }

    public void pinOkBtn() {
        clickOnElement(pinokBtn, "pinOkBtn");
    }

    public void clickOnResetCredentials() {
        clickOnElement(resetCredentialsBtn, "resetCredentialsBtn");
    }

    public void navCustomerCareExecutive() throws Exception {
        navigateTo("ENQUIRY_ALL", "ENQUIRY_CCE_ENQUIRY", "Customer Care executive Page");
        Thread.sleep(5000);
        //wait.until(ExpectedConditions.elementToBeClickable(userTypeDropDown));
    }

    public void selectUserTypeByVisibleText(String userType) {
        selectVisibleText(userTypeDropDown, userType, "User Type");
    }

    public void selectAccountIdentifierByVisibleText(String accountIdentifier) {
        selectVisibleText(accountIdentifierDropDown, accountIdentifier, "Account Identifier");
    }

    public void enterFilterData(String FilterData) {
        setText(filterDataTxtBox, FilterData, "Choose filter Data");
    }

    public void clickOnSubmit() throws Exception {
        clickOnElement(submitBtn, "Submit btn");
        Thread.sleep(4500);
    }

    public void selectUserType(String text) {
        selectVisibleText(selUserType, text, "selUserType");
    }

    /**
     * Method to Select User Type
     *
     * @param text
     */
    public void selectUserTypeByValue(String text) {
        selectValue(selUserType, text, "selUserType");
    }

    /**
     * Method to click on Submit
     */
    public void clickonSubmit() {
        clickOnElement(submit, "Submit");
    }

    /**
     * Method to click Show User Details Link
     */
    public void selectDetailType(String detailType) throws Exception {
        if (detailType.equalsIgnoreCase(Constants.SHOW_USER_DETAILS))
            clickOnElement(showUserDetailsLink, "Show User Details Link");
        else if (detailType.equalsIgnoreCase(Constants.SHOW_USER_ACCESS_DETAILS))
            clickOnElement(showUserAccessDetailLink, "Show User Details Link");
        else if (detailType.equalsIgnoreCase(Constants.SHOW_TRANS_CONTROL))
            clickOnElement(showTransferControlLink, "Show Transfer Control Link");
        else if (detailType.equalsIgnoreCase(Constants.SHOW_USER_BALANCE))
            clickOnElement(showBalanceLink, "Show Balance Link");

        Thread.sleep(Constants.THREAD_SLEEP_1000);
    }

    public void clickShowUserDetailsLink(){
        clickOnElement(showUserDetailsLink, "Show User Details Link");
    }

    /**
     * Method to click Show User Access Detail Link
     */
    public void clickShowUserAccessDetailLink() {
        clickOnElement(showUserAccessDetailLink, "Show User Details Link");
    }

    /**
     * Method to click on Show Balance Link
     */
    public void clickShowBalanceLink() {
        clickOnElement(showBalanceLink, "Show Balance Link");
    }

    /**
     * Method to click Show Transfer Link
     */
    public void clickShowTransferControlLink() {
        clickOnElement(showTransferControlLink, "Show Transfer Control Link");
    }

    /**
     * Method to select provider
     */
    public void clickProviderDdown() {
        providerDDown.click();
    }

    /**
     * Method to get First Name
     *
     * @return
     */
    public String getFirstName() {
        return cWindowFName.getText();
    }

    public String getMSISDN() {
        return cWindowMsisdn.getText();
    }

    public void setMSISDN(String msisdn) {
        setText(msisdnWebElement, msisdn, "MSISDN");
    }

    public String getSubscriberMSISDN() {
        return cWindowSubscriberMsisdn.getText();
    }

    public String getcategory() {
        return cWindowCatCode.getText();
    }

    public String getCategory() {
        return cWindowCatCode.getText();
    }

    /**
     * Method to get category Code
     *
     * @return
     */
    public String extractCategory() {
        Boolean isTrue = Utils.checkElementPresent("userEnquir_showUserDetails_categoryName", Constants.FIND_ELEMENT_BY_ID);
        if (!isTrue)
            return cWindowCatCode.getText();
        else
            cWindowCatCode = driver.findElement(By.id("userEnquir_showUserDetails_categoryName"));
        return cWindowCatCode.getText();
    }

    /**
     * Method to get MSISDN
     *
     * @return
     */
    public String extractMSISDN() {
        Boolean isTrue = Utils.checkElementPresent(".//*[contains(@id,'msisdn')]", Constants.FIND_ELEMENT_BY_XPATH);
        if (!isTrue)
            return cWindowMsisdn.getText();
        else
            cWindowMsisdn = driver.findElement(By.xpath(".//*[contains(@id,'msisdn')]"));
        return cWindowMsisdn.getText();
    }

    /**
     * Method to handle Window
     */
    public void handleWindow() throws Exception {
        MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                driver.switchTo().window(ChildWindow);
                pageInfo.info("Switching to Child Window..");
            }
        }
        // Switching to Parent window i.e Main Window.
        //driver.switchTo().window(MainWindow);
        Thread.sleep(Constants.THREAD_SLEEP_1000);
    }

    /**
     * Method to switch to main Window
     */
    public void switchToMainWindow() {
        driver.switchTo().window(MainWindow);
        pageInfo.info("Switch to main Window");
    }

    /**
     * Method to close Child Window
     */
    public void closeChildWindow() {
        driver.close();
        pageInfo.info("Closing Child Window.");
    }

    /*
     * ------------------------------------------Change the Status ------------------------------------
     */
    public void selectStatusByValue(String status) {
        selectValue(StatusDropDown, status, "Status");
    }

    public void setDisplayDropDown() {
        clickOnElement(displayDropDown, "Status");
    }

    public void setReasonToChangeStatus() throws InterruptedException {
        setText(reasonTxtField, "Testing", "Reason");
    }

    public void clickOnStatusSubmit() throws Exception {
        Utils.scrollToBottomOfPage();
        Thread.sleep(1500);
        clickOnElement(statuSubmitBtn, "Submit Status");
        wait.until(ExpectedConditions.elementToBeClickable(modalFade));
    }

    /**
     * Method to set Agent Code
     *
     * @param agent
     */
    public void setAgentCode(String agent) {
        setText(agentCode, agent, "Agent Code");
    }

    /**
     * Method to get TCP name
     *
     * @return
     */
    public String extractTCPName() {
        return cWindowTCPName.getText();
    }

    /**
     * Method to TCP Id
     *
     * @return
     */
    public String extractTCPId() {
        return cWindowTCPID.getText();
    }

    /**
     * Method to get Balance
     *
     * @return
     */
    public String extractBalance() {
        return cWindowUserBalance.getText();
    }

    /**
     * Method to click on Back button
     */
    public void clickOnBackBtn() {
        clickOnElement(backButton, "Back Button");
    }
}
