/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of commissionDisbursement page 1
*/

package framework.pageObjects.commissionDisbursement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class Commission_Disbursement_Page1 extends PageInit {


    @FindBy(id = "commissionDisbursementAction_channelUserDetails_providerId")
    private WebElement Provider;
    @FindBy(id = "commissionDisbursementAction_channelUserDetails_msisdn")
    private WebElement MSISDN;
    @FindBy(id = "commissionDisbursementAction_channelUserDetails_button_submit")
    private WebElement Submit;
    @FindBy(id = "domainCode")
    private WebElement domainDropDown;
    @FindBy(id = "categoryCode")
    private WebElement catDropDown;
    @FindBy(id = "zoneCode")
    private WebElement zoneDropDown;
    @FindBy(id = "areaCode")
    private WebElement areaDropDown;
    @FindBy(id = "commissionDisbursementAction_channelUserDetails_amount")
    private WebElement minAmount;
    @FindBy(xpath = "(//td//b[contains(text(),'MSISDN')]/../../..//label)[2]")
    private WebElement userName;

    public Commission_Disbursement_Page1(ExtentTest t1) {
        super(t1);
    }

    public Commission_Disbursement_Page1 selectCat() {
        selectDropdown(catDropDown, "Select Category ");
        return this;
    }

    public Commission_Disbursement_Page1 selectZone() {
        selectDropdown(zoneDropDown, "Select Zone");
        return this;
    }

    public Commission_Disbursement_Page1 selectArea() {
        selectDropdown(areaDropDown, "Select Area");
        return this;
    }

    public Commission_Disbursement_Page1 selectArea(String areaName) {
        selectVisibleText(areaDropDown, areaName, "Area Name");
        return this;
    }

    public Commission_Disbursement_Page1 selectZone(String zoneName) {
        selectVisibleText(zoneDropDown, zoneName, "Zone Name");
        return this;
    }

    public Commission_Disbursement_Page1 selectDomain() {
        selectDropdown(domainDropDown, "Select Domain");
        return this;
    }

    public Commission_Disbursement_Page1 SelectDomain(String domainName) {
        selectVisibleText(domainDropDown, domainName, "DomainName");
        return this;
    }

    private void selectDropdown(WebElement e, String msg) {
        Select option = new Select(e);
        if (option.getOptions().size() > 1)
            option.selectByIndex(1);
        else {
            option.selectByIndex(0);
        }
        pageInfo.info(msg + " : " + option.getFirstSelectedOption().getText());
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
    }

    public Commission_Disbursement_Page1 selectProvider() {

        Select option = new Select(Provider);
        option.selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
        pageInfo.info("Select Provider");
        return this;
    }

    public Commission_Disbursement_Page1 selectProvider(String providerName) {
        selectVisibleText(Provider, providerName, "ProviderName");
        return this;
    }

    public Commission_Disbursement_Page1 enterMsisdn(String value) {
        setText(MSISDN, value, "MSISDN");
        return this;
    }

    public void clickOnSubmit() {
        clickOnElement(Submit, "Submit");
    }

    public Commission_Disbursement_Page1 navigateToLink() throws Exception {
        navigateTo("COMMDIS_ALL", "COMMDIS_COMMDISINITIATE", "Commission Disbursement");
        return this;

    }

    public Commission_Disbursement_Page1 enterMinAmount(String value) {
        setText(minAmount, value, "Min Amount");
        return this;
    }

    public List<String> getAllProvider() throws Exception {
        return fl.getOptionText(Provider);
    }

    public Commission_Disbursement_Page1 SelectCategory(String categoryName) {
        selectVisibleText(catDropDown, categoryName, "Category");
        return this;
    }

    public boolean isSubmitButtonDisplayed() {
        return Submit.isDisplayed();
    }
}
