package framework.pageObjects.commissionDisbursement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

public class Commission_Withdrawal_Page1 extends PageInit {
    @FindBy(xpath = "//font[@class='heading']")
    WebElement heading;
    @FindBy(xpath = "//td[@class='btn1']")
    WebElement formheading;
    @FindBy(xpath = " //a[contains(text(),'Commission Management')]")
    private WebElement CommissionManagementLink;
    @FindBy(id = "commissionDisbursementAction_channelUserDetails_providerId")
    private WebElement Provider;

    @FindBy(id = "domainCode")
    private WebElement domain;

    @FindBy(id = "categoryCode")
    private WebElement Category;

    @FindBy(id = "commissionDisbursementAction_channelUserDetails_msisdn")
    private WebElement MSISDN;

    @FindBy(id = "commissionDisbursementAction_channelUserDetails_button_submit")
    private WebElement Submit;

    public Commission_Withdrawal_Page1(ExtentTest t1) {
        super(t1);
    }

    public static Commission_Withdrawal_Page1 init(ExtentTest t1) {
        return new Commission_Withdrawal_Page1(t1);
    }

    public Commission_Withdrawal_Page1 NavigateToLink() throws Exception {
        fl.leftNavigation("COMMDIS_ALL", "COMMDIS_COM_WITHDRW");
        pageInfo.info("Navigate to Commission Management link");
        return this;
    }

    public Commission_Withdrawal_Page1 SelectProvider() {

        Select option = new Select(Provider);
        option.selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
        pageInfo.info("Select Provider");
        return this;
    }

    public Commission_Withdrawal_Page1 SelectDomain(String domainName) {

        Select option = new Select(domain);
        option.selectByVisibleText(domainName);
        pageInfo.info("Select Domain");
        return this;
    }

    public Commission_Withdrawal_Page1 SelectCategory(String categoryName) {

        Select option = new Select(Category);
        option.selectByVisibleText(categoryName);
        pageInfo.info("Select Category");
        return this;
    }

    public Commission_Withdrawal_Page1 EnterMsisdn(String value) {
        MSISDN.sendKeys(value);
        pageInfo.info("Enter Mmobile Number" + value);
        return this;
    }

    public void ClickOnSubmit() {
        Submit.click();
        pageInfo.info("Click on Submit button");
    }

    public void VerifyCommissionManagementLink(ExtentTest t) throws Exception {
        pageInfo.info("Check whether Commission management link is visible or not");
        Boolean link = CommissionManagementLink.isDisplayed();
        String text = CommissionManagementLink.getText();
        System.out.println(text);
        Assertion.verifyEqual(true, link, "Commission Management Link is visble", t);
        Assertion.verifyMessageContain(text, "Commission.Management.UI", "Name of the link is " + text, t);
    }

    public Commission_Withdrawal_Page1 VerifyBreadcrumbs(ExtentTest t, String pageNo) throws IOException {

        String heading1 = heading.getText();
        System.out.println(heading1);
        Assertion.verifyEqual(heading1, "Commission Management", "Verify Commission management in page " + pageNo, t);
        return this;
    }

    public Commission_Withdrawal_Page1 VerifyFormHeading(ExtentTest t) throws IOException {
        String heading2 = formheading.getText();
        System.out.println(heading2);
        Assertion.verifyEqual(heading2, "Commission Management", "Verify Commission management in page 1 form heading:", t);
        return this;
    }

}
