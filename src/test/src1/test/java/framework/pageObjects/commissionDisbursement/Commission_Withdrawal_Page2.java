package framework.pageObjects.commissionDisbursement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

public class Commission_Withdrawal_Page2 extends PageInit {
    private static String commissionWithdrawalFile;

    @FindBy(xpath = "//input[@name='action:commissionDisbursementAction_disburseSelected']")
    private WebElement CommissionWithdrawalBtn;


    public Commission_Withdrawal_Page2(ExtentTest t1) {
        super(t1);
    }

    public static Commission_Withdrawal_Page2 init(ExtentTest t1) {
        return new Commission_Withdrawal_Page2(t1);
    }

    public Commission_Withdrawal_Page2 verifyIfBottonExists(String value, ExtentTest t1) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.cssSelector("input[type='submit'][value='" + value + "']")).isDisplayed()) {
            t1.pass("Successfuly verified the " + value + " button is present");
        } else {
            t1.fail("Failed to verify the " + value + " button is NOT present");
            Utils.captureScreen(t1);
        }
        return this;
    }

    public Commission_Withdrawal_Page2 downloadCommissionWithdrawal() throws InterruptedException {
        pageInfo.info("Deleting existing file with prefix - CommissionWithdrawal");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "CommissionWithdrawal");
        Thread.sleep(5000L);
        this.CommissionWithdrawalBtn.click();
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Click On Export button");
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000L);
        return this;
    }

    public String getCommissionWithdrawalFile() {
        return commissionWithdrawalFile;
    }

    public String overrideCommissionAmount(String msisdn, String changedCommissionAmount) {
        driver.findElement(By.xpath("//label[text()='" + msisdn + "']/../..//input[@type='text']")).clear();
        pageInfo.info("Clear the Commission amount corresponding to user - " + msisdn);

        String actualCommissionAmount = driver.findElement(By.xpath("//label[text()='" + msisdn + "']/../..//input[@type='text']")).getText();
        pageInfo.info("Getting the commission amount corresponding to user - " + msisdn);

        driver.findElement(By.xpath("//label[text()='" + msisdn + "']/../..//input[@type='text']")).sendKeys(changedCommissionAmount);
        pageInfo.info("Override the commission amount corresponding to user - " + msisdn);

        driver.findElement(By.xpath("//label[text()='" + msisdn + "']/../..//input[@type='checkbox']")).click();
        pageInfo.info("Click on check box corresponding to user - " + msisdn);
        return actualCommissionAmount;
    }

    public Commission_Withdrawal_Page2 selectCommissionDisbursement(String msisdn) {
        driver.findElement(By.xpath("//label[text()='" + msisdn + "']/../..//input[@type='checkbox']")).click();
        pageInfo.info("Click on check box corresponding to user - " + msisdn);
        return this;
    }
}
