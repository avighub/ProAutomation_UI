package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BarUser_pg1 extends PageInit {

    @FindBy(id = "userTypeId")
    private WebElement userType;

    @FindBy(id = "onlyMsisdn")
    private WebElement msisdn;

    @FindBy(id = "onlyLogin")
    private WebElement loginIDTbox;

    @FindBy(id = "barType")
    private WebElement barType;

    @FindBy(id = "barReason")
    private WebElement reasonForBar;

    @FindBy(id = "barAction_confirmBlackListUser_button_submit")
    private WebElement submit;

    @FindBy(id = "barAction_insertBlackListUser_button_confirm")
    private WebElement confirm;

    @FindBy(id = "remarks")
    private WebElement barRemarks;

    @FindBy(className = "actionMessage")
    private WebElement ActionMessage;

    public BarUser_pg1(ExtentTest t1) {
        super(t1);
    }

    public static BarUser_pg1 init(ExtentTest t1) {
        return new BarUser_pg1(t1);
    }

    public void navigateNAToBarUserLink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_BLKL", "Bar User");
    }

    public void selectUserTypeByValue(String text) {
        selectValue(userType, text, "userType");
    }

    public void setMsisdn(String mobileNo) {
        setText(msisdn, mobileNo, "msisdn");
    }

    public void setLoginIDTbox(String login) {
        setText(loginIDTbox, login, "Login ID");
    }

    public void barType(String baringType) {
        selectValue(barType, baringType, "Bar AS");
    }

    public void reasonForBar(String reason) {
        selectValue(reasonForBar, reason, "Reason For Bar");
    }

    public void remarksForBar(String text) {
        setText(barRemarks, text, "barRemarks");
    }

    public void clickSubmit() {
        clickOnElement(submit, "Submit");
    }

    public void clickConfirm() {
        clickOnElement(confirm, "Confirm");
    }

    public String getMessage() {
        return String.valueOf(ActionMessage.getText());
    }

    public void navigateToBarUserLink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_ALL", "Bar User Page");
    }


}
