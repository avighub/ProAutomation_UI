package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class AddChannelUser_pg3 extends PageInit {


    /*
     * Page Objects
	 */
    @FindBy(id = "confirm2_loadGroupRoles_entLogo")
    WebElement uploadlogo;

    @FindBy(id = "confirm2_loadGroupRoles_button_next")
    private WebElement Next;

    @FindBy(id = "subsRegistrationServiceBean_output_button_next")
    private WebElement subscriberNext;

    @FindBy(id = "editCH_loadGroupRoles_button_next")
    private WebElement modifyGroupRoleNext;


    public AddChannelUser_pg3(ExtentTest t1) {
        super(t1);
    }

    public static AddChannelUser_pg3 init(ExtentTest t1) {
        return new AddChannelUser_pg3(t1);
    }

    /**
     * Select Web group Role
     *
     * @param roleName
     */
    public void selectWebGroupRole(String roleName) {
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("input[value='" + roleName + "']")))).click();
        pageInfo.info("Click on Web role: "+ roleName);
    }

    /**
     * For enterprise upload the logo file
     */
    public void uploadLogo() {
        setText(uploadlogo, FilePath.uploadFile, "Upload Logo");
    }

    public void clickNext() {
        if (fl.elementIsDisplayed(Next))
            clickOnElement(Next, "Next");
        else if (fl.elementIsDisplayed(modifyGroupRoleNext))
            clickOnElement(modifyGroupRoleNext, "modifyGroupRoleNext");
    }

    public void clickNextSUBS() {
        clickOnElement(subscriberNext, "subscriberNext");
    }
}
