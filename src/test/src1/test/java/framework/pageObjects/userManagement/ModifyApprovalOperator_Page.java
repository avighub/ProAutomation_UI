package framework.pageObjects.userManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifyApprovalOperator_Page extends PageInit {


    @FindBy(name = "partyTypeId")
    WebElement userType;
    @FindBy(name = "msisdnWeb")
    WebElement loginID;
    @FindBy(name = "button.submit")
    WebElement submitButton;
    @FindBy(id = "loadUserUpdate_submit")
    WebElement finalSubmitButton;
    @FindBy(id = "loadUserUpdate_loadSystemPartyForUpdate_approve")
    WebElement approveButton;
    @FindBy(id = "loadUserUpdate_loadSystemPartyForUpdate_reject")
    WebElement rejectButton;

    public ModifyApprovalOperator_Page(ExtentTest t1) {
        super(t1);
    }

    /**
     * INIT
     *
     * @return
     */
    public static ModifyApprovalOperator_Page init(ExtentTest t1) {
        return new ModifyApprovalOperator_Page(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */
    public void navigateToNALink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_MSUAP", "Approve Modify Operator User");
    }


    /**
     * Navigation Link for the BULK PAYER ADMIN MANAGEMENT
     *
     * @throws Exception
     */
    public void navigateToBPALink() throws Exception {
        fl.leftNavigation("BPAMGT_ALL", "BPAMGT_EPTY_MSUAP");

    }

    public void selectUserTypeByValue(String text) {
        selectValue(userType, text, "User Type");
    }


    public void enterLoginID(String login) {
        setText(loginID, login, "Login Id");
    }

    public void selectUserFromList(String msisdn) {
        WebElement radio = driver.findElement(By.xpath("//tr/td[contains(text(),'" + msisdn + "')]/ancestor::tr[1]/td/input[@type='radio']"));
        radio.click();
        pageInfo.info("Select user having MSISDN: " + msisdn);
    }

    public void clickSubmitButton() {
        clickOnElement(submitButton, "Submit Button");
    }

    public void clickFinalSubmitButton() {
        clickOnElement(finalSubmitButton, "Final Submit Button");
    }


    public void clickApproveButton() {
        clickOnElement(approveButton, "Approve Button");
    }

    public void clickRejectButton() {
        clickOnElement(rejectButton, "Reject Button");
    }

}
