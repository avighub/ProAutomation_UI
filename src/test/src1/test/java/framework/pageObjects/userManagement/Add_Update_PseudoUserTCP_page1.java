package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

/**
 * Created by gurudatta.praharaj on 7/3/2018.
 */
public class Add_Update_PseudoUserTCP_page1 extends PageInit {

    @FindBy(id = "partyMsisdnId")
    private WebElement setMsisdn;
    @FindBy(id = "mfsProvidersList")
    private WebElement MFSProvider;
    @FindBy(id = "wallet")
    private WebElement wallet;
    @FindBy(id = "selectForm_profileName")
    private WebElement profileName;
    @FindBy(id = "selectForm_profileShortName")
    private WebElement shortName;
    @FindBy(id = "selectForm_button_submit")
    private WebElement submitBtn1;
    @FindBy(id = "pseudoDetails_confirm_button_submit")
    private WebElement submitBtn2;
    @FindBy(id = "pseudoDetails_confirm_button_confirm")
    private WebElement finalConfirm;

    public Add_Update_PseudoUserTCP_page1(ExtentTest test) {
        super(test);
    }

    public static Add_Update_PseudoUserTCP_page1 init(ExtentTest test) {
        return new Add_Update_PseudoUserTCP_page1(test);
    }

    public Add_Update_PseudoUserTCP_page1 navToModifyPseudoTcp() throws Exception {
        navigateTo("PSEUDO_ALL", "PSEUDO_PSEUDO_MODIFY_TCP", "Modify Psuedo TCP");
        return this;
    }

    public Add_Update_PseudoUserTCP_page1 setMsisdn(String msisdn) throws Exception {
        setText(setMsisdn, msisdn, "Enter Pseudo MSISDN");
        Thread.sleep(10000);
        return this;
    }

    public Add_Update_PseudoUserTCP_page1 setMFSProvider(String providerID) throws Exception {
        clickOnElement(MFSProvider,"MFS Provoder");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        selectValue(MFSProvider, providerID, "Select MFS Provider");

//        try{
//            Thread.sleep(5000);
//            wait.until(ExpectedConditions.elementToBeClickable(MFSProvider));
//            selectValue(MFSProvider, providerID, "Select MFS Provider");
//        }catch (Exception e){
//            Thread.sleep(5000);
//            wait.until(ExpectedConditions.elementToBeClickable(MFSProvider));
//            selectValue(MFSProvider, providerID, "Select MFS Provider");
//        }
        return this;
    }

    public Add_Update_PseudoUserTCP_page1 selectWallet(String walletType) {
        selectValue(wallet, walletType, "Select Wallet Type");
        return this;
    }

    public Add_Update_PseudoUserTCP_page1 setProfileName(String name) {
        setText(profileName, name, "Set TCP Profile Name: ");
        return this;
    }

    public Add_Update_PseudoUserTCP_page1 setProileShortName(String name) {
        setText(shortName, name, "Set TCP Profile Short Name: ");
        return this;
    }

    public Add_Update_PseudoUserTCP_page1 clickSubmitBtn1() {
        clickOnElement(submitBtn1, "1st Submit Button");
        return this;
    }

    public Add_Update_PseudoUserTCP_page1 clickSubmitBtn2() {
        clickOnElement(submitBtn2, "2nd Submit Button");
        return this;
    }

    public Add_Update_PseudoUserTCP_page1 clickConfirm() throws Exception {
        clickOnElement(finalConfirm, "Confirm Button");
        Thread.sleep(10000);
        return this;
    }

    public void modifyTCPThresholdAllServices(String payerCount, String payerAmount)
    {

        List<WebElement> listCount = driver.findElements(By.xpath("//label[starts-with(text(),'Cash in')]/ancestor::tr/td/input[@id=\"pseudoDetails_confirm_counts\"][@name=\"counts\"]"));
        List<WebElement> listAmount = driver.findElements(By.xpath("//label[starts-with(text(),'Cash in')]/ancestor::tr/td/input[@id=\"pseudoDetails_confirm_amounts\"][@name='amounts']"));

        for (WebElement thresholdPayerCount : listCount)
        {

                if(thresholdPayerCount.isEnabled())
                setText(thresholdPayerCount, payerCount, "thresholdPayerCount");
            }



        for (WebElement thresholdPayerAmount : listAmount)
        {
            if(thresholdPayerAmount.isEnabled())
            setText(thresholdPayerAmount, payerAmount, "thresholdPayerAmount");
        }
    }

       /* for (WebElement thresholdPayeeCount : listPayeeCount) {
            if (thresholdPayeeCount.getAttribute("thresholdtype").equalsIgnoreCase("Per Transaction")) {
                setText(thresholdPayeeCount, "1", "thresholdPayeeCount");
            } else {
                setText(thresholdPayeeCount, payeeCount, "thresholdPayeeCount");
            }
        }

        for (WebElement thresholdPayeeAmount : listPayeeAmount) {
            setText(thresholdPayeeAmount, payeeAmount, "thresholdPayeeAmount");
        }

    }
    */


}

