package framework.pageObjects.userManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifyOperatorUser_page2 extends PageInit {


    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_lastName")
    WebElement lastName;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_partyNamePrefixId")
    WebElement prefix;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_userName")
    WebElement firstName;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_departmentId")
    WebElement department;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_divisionId")
    WebElement division;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_contactNo")
    WebElement contactNo;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_msisdnWeb")
    WebElement webloginID;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_externalCode")
    WebElement externalCode;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_msisdnUser")
    WebElement msisdn;
    @FindBy(id = "dispPassword")
    WebElement password;
    @FindBy(id = "dispConfirmPassword")
    WebElement confirmPassword;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_button_update")
    WebElement updateButton;
    @FindBy(className = "actionMessage")
    WebElement ActionMessage;
    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_delete")
    WebElement deleteButton;

    public ModifyOperatorUser_page2(ExtentTest t1) {
        super(t1);
    }

    /**
     * INIT
     *
     * @return
     */
    public static ModifyOperatorUser_page2 init(ExtentTest t1) {
        return new ModifyOperatorUser_page2(t1);
    }

    public void externalCode_SetText(String text) {
        setText(externalCode, text, "External Code");

    }

    public void msisdn_SetText(String text) {
        setText(msisdn, text, "MSISDN");
    }

    public void password_SetText(String text) {
        setText(password, text, "Password");
    }


    public void confirmPassword_SetText(String text) {
        setText(confirmPassword, text, "Confirm Password");
    }


    public void webloginID_SetText(String text) {
        setText(webloginID, text, "Web Login ID");
    }

    public void contactNo_SetText(String text) {
        setText(contactNo, text, "Contact Number");
    }

    public void division_SetText(String text) {
        setText(division, text, "Division");
    }

    public void department_SetText(String text) {
        setText(department, text, "Department");
    }

    public void prefix_SelectValue(String text) {
        selectValue(prefix, text, "Prefix");
    }

    public void firstName_SetText(String text) {
        setText(firstName, text, "First Name");
    }


    public void lastName_SetText(String text) {
        lastName.clear();
        lastName.sendKeys(text);
    }


    public void updateButton_Click() {
        updateButton.click();
    }

    public String getActionMessage() {
        try {
            return ActionMessage.getText();
        } catch (NoSuchElementException e) {
            return ErrorMessage.getText();
        }
    }

    public void deleteButton_Click() {
        deleteButton.click();
    }

    /**
     * get error message
     *
     * @return
     */
    public String getErrorMessage() {
        try {
            return ErrorMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }


    public boolean checkPasswordFieldIsDisplayed() {
        return fl.elementIsDisplayed(password);
    }

}
