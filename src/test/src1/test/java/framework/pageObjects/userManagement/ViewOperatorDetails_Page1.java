package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.List;

public class ViewOperatorDetails_Page1 extends PageInit {


    @FindBy(id = "viewopt_loadSystemPartyView_partyTypeId")
    WebElement userTypeDDown;
    @FindBy(id = "viewopt_loadSystemPartyView_userName")
    WebElement userFirstName;
    @FindBy(id = "viewopt_input_msisdnUser")
    WebElement msisdnLabel;
    @FindBy(id = "viewopt_input_msisdnWeb")
    WebElement webLoginLabel;
    @FindBy(id = "viewopt_input_userName")
    WebElement userNameLabel;
    @FindBy(name = "button.submit")
    WebElement submit;

    public ViewOperatorDetails_Page1(ExtentTest t1) {
        super(t1);
    }

    public static ViewOperatorDetails_Page1 init(ExtentTest t1) {
        return new ViewOperatorDetails_Page1(t1);
    }

    public void navigateToLink() throws Exception {
        fl.leftNavigation("PARTY_ALL", "PARTY_PTY_VSU");
        pageInfo.info("Navigate to View Operator Details Page");
    }

    public void selectUserTypeByValue(String text) {
        Select ut = new Select(userTypeDDown);
        ut.selectByValue(text);
        pageInfo.info("Select user type: " + text);
    }


    public void setUserFirstName(String firstName) {
        userFirstName.sendKeys(firstName);
        pageInfo.info("Set First Name: " + firstName);

    }

    public WebElement getUserType() {
        return userTypeDDown;
    }

    public String getMSISDNLabelText() {
        return msisdnLabel.getText();
    }

    public String getUserNameLabelText() {
        return userNameLabel.getText();
    }


    public String getWebLoginIDLabelText() {
        return webLoginLabel.getText();
    }

    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }

    public boolean checkUserType(String userType) throws IOException {
        boolean found = false;
        Select se = new Select(userTypeDDown);
        List<WebElement> userTypes = se.getOptions();
        for (WebElement option : userTypes) {
            String value = option.getText();
            pageInfo.info("Existing value is : " + value);
            if (option.getAttribute("value").contains(userType)) {
                found = false;
            }
        }
        if (found) {
            pageInfo.fail(userType + " Does Exist's");
        } else {
            userTypeDDown.click();
            Utils.captureScreen(pageInfo);
            pageInfo.pass(userType + " Does not Exist");
        }
        return found;
    }


}
