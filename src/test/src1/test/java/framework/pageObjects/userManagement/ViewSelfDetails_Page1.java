package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ViewSelfDetails_Page1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    /***********************************************************************
     *  V I E W  - S E L F -  D E T A I L S - P A G E  - O B J E C T S
     ***********************************************************************/

    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_msisdnUser")
    WebElement msisdnLabel;
    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_msisdnWeb")
    WebElement webLoginLabel;
    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_userName")
    WebElement firstName;
    @FindBy(id = "selfDet_loadSelfDetails_systemparty_label_lastName")
    WebElement lastName;

    public static ViewSelfDetails_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        ViewSelfDetails_Page1 page = PageFactory.initElements(driver, ViewSelfDetails_Page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void navigateToLink() throws Exception {
        fl.leftNavigation("PARTY_ALL", "PARTY_PTY_VSELF");
        pageInfo.info("Navigate to View Self Details Page");
    }

    /***********************************************************************
     *  V I E W  - S E L F -  D E T A I L S - P A G E  - O P E R A T I O N S
     ***********************************************************************/

    public String getMSISDNLabelText() {
        return msisdnLabel.getText();
    }

    public String getWebLoginIDText() {
        return webLoginLabel.getText();
    }

    public String getFirstName() {
        return firstName.getText();
    }

    public String getLastName() {
        return lastName.getText();
    }
}
