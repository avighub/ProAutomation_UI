package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.rmi.CORBA.Util;
import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/17/2017.
 */
public class AddModifyApproveBank extends PageInit {


    @FindBy(id = "mfsProviderListForAppr")
    private WebElement Provider;

    @FindBy(id = "walletOrBank")
    private WebElement LinkedBank;

    @FindBy(name = "fromDateForBanks")
    private WebElement FromDate;

    @FindBy(name = "toDateForBanks")
    private WebElement ToDate;

    @FindBy(id = "selectForm_button_submit")
    private WebElement Submit;

    @FindBy(name = "checkall")
    private WebElement CheckAll;

    @FindBy(id = "approve")
    private WebElement Approve;

    @FindBy(id = "table")
    private WebElement webTable;

    @FindBy(id = "rejectButton")
    private WebElement Reject;


    public AddModifyApproveBank(ExtentTest t1) {
        super(t1);
    }

    public static AddModifyApproveBank init(ExtentTest t1) {
        return new AddModifyApproveBank(t1);
    }

    /**
     * Navigate to Add/Modify?delet Bank detail Page
     *
     * @throws Exception
     */
    public void navAddModifyDeleteBank() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_BNK_APR", "Add/Modify/delete Bank");
    }

    public void selectProvider() {
        selectVisibleText(Provider, "ALL", "Currency Provider");
    }

    public void selectProvider(String providerName) {
        selectVisibleText(Provider, providerName, "Currency Provider");
    }

    public void selectBank() throws InterruptedException {
        selectVisibleText(LinkedBank, "ALL", "Linked Banks");
    }

    public void selectBank(String bank) {
        selectVisibleText(LinkedBank, bank, "Linked Banks");
    }

    public void setFromDate() throws Exception {
        Utils.setValueUsingJs(FromDate, "");
        Utils.setValueUsingJs(FromDate, new DateAndTime().getDate(+0));
        Thread.sleep(2500);
        pageInfo.info("Set From Date: " + DataFactory.getCurrentDateSlash());
    }

    public void reject() {
        clickOnElement(Reject, "Reject btn");
    }

    public void setToDate() throws Exception {
        Utils.setValueUsingJs(ToDate, "");
        Utils.setValueUsingJs(ToDate, new DateAndTime().getDate(+0));
        pageInfo.info("Set To Date: " + DataFactory.getCurrentDateSlash());
    }

    public void setFromDate(int day) throws Exception {
        Utils.setValueUsingJs(FromDate, "");
        Utils.setValueUsingJs(FromDate, new DateAndTime().getDate(day));
        Thread.sleep(2500);
        pageInfo.info("Set From Date: " + DataFactory.getCurrentDateSlash());
    }

    public void setToDate(int day) throws Exception {
        Utils.setValueUsingJs(ToDate, "");
        Utils.setValueUsingJs(ToDate, new DateAndTime().getDate(day));
        pageInfo.info("Set To Date: " + DataFactory.getCurrentDateSlash());
    }

    public void clickSubmit() {
        clickOnElement(Submit, "Submit");
    }

    public boolean checkBanksForUser(String reference) throws IOException {
        List<WebElement> checkBoxes = webTable.findElements(By.xpath(".//tr/td[contains(text(), '" + reference + "')]"));

        for (WebElement elem : checkBoxes) {
            WebElement check = elem.findElement(By.xpath("ancestor::tr[1]/td/input[@type='checkbox']"));
            if (!check.isSelected()) {
                clickOnElement(check, "CheckBox " + elem.getText());
            }
        }

        if (checkBoxes.size() > 0) {
            return true;
        }
        pageInfo.info("Could not find the Entry for msisdn/custId:"+ reference);
        Utils.captureScreen(pageInfo);
        return false;
    }

    public void approve() throws IOException {
        Utils.captureScreen(pageInfo);
        clickOnElement(Approve, "Approve btn");
    }
}
