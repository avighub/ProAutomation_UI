package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewBarUser_pg2 extends PageInit {

    @FindBy(className = "wwFormTableC")
    WebElement tableBarUser;
    @FindBy(id = "viewBlackListAction_viewBlackListUser_back")
    WebElement btnUnbar;
    @FindBy(name = "action:viewBlackListAction_confirmUnBlackListUser")
    WebElement btnConfirmUnBar;
    @FindBy(id = "viewBlackListAction_viewBlackListUser_back")
    WebElement btnBack;

    public ViewBarUser_pg2(ExtentTest t1) {
        super(t1);
    }

    public static ViewBarUser_pg2 init(ExtentTest t1) {
        return new ViewBarUser_pg2(t1);
    }

    public void selectBarredUser(String msisdn) {
        WebElement elem = DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + msisdn + "')]/ancestor::tr[1]/td/input[@type='checkbox']"));
        clickOnElement(elem, "Select Barred User with MSISDN: " + msisdn);
    }

    public void clickOnUnbbar() {
        clickOnElement(btnUnbar, "UnBar");
    }

    public void clickOnUnbbarConfirm() {
        clickOnElement(btnConfirmUnBar, "UnBar Confirm");
    }

    public HashMap<String, HashMap<String, String>> getBarredUserTable() {
        List<Integer> excludeRow = new ArrayList<>(), excludeColumn = new ArrayList<>();
        excludeRow.add(0); // exclude first Row
        excludeRow.add(1); // exclude first Row
        excludeRow.add(-1);
        excludeColumn.add(0);

        HashMap<String, HashMap<String, String>> data = getTableData(this.tableBarUser, 2, excludeRow, excludeColumn);
        return data;
    }


}
