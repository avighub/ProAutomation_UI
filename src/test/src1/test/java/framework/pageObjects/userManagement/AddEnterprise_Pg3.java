package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by rahul.rana on 6/16/2017.
 */
public class AddEnterprise_Pg3 extends PageInit {
    @FindBy(id = "add1_addChannelUser_submit")
    WebElement confirmButton;
    @FindBy(name = "enterpriselimit")
    WebElement enterpriseLimit2;
    @FindBy(id = "confirm2_assignBankOrEnteprise_isBPRegRegquiredY")
    WebElement bulkRegReguired_Y;
    @FindBy(id = "confirm2_assignBankOrEnteprise_isBPRegRegquiredN")
    WebElement bulkRegReguired_N;
    @FindBy(id = "confirm2_assignBankOrEnteprise_bulkpayercategory")
    WebElement bulkPayerType;
    @FindBy(id = "transactionTypeList")
    WebElement unRegBPmode;
    @FindBy(id = "confirm2_assignBankOrEnteprise_companyCode")
    WebElement compCode;
    @FindBy(id = "confirm2_assignBankOrEnteprise_button_next")
    private WebElement Next;

    public AddEnterprise_Pg3(ExtentTest t1) {
        super(t1);
    }

    public static AddEnterprise_Pg3 init(ExtentTest t1) {
        return new AddEnterprise_Pg3(t1);
    }

    /**
     * set the enterpise limit
     *
     * @param text
     */
    public void setEnterpriseLimit2(String text) {
        setTextUsingJs(enterpriseLimit2, text, "Level 2 Approval Limit");

    }

    /**
     * set the bulk registration to Y
     */
    public void clickOnBulkRegistrationIsRequired() {
        clickOnElement(bulkRegReguired_Y, "bulkRegReguired_Y");
    }

    /**
     * set the bulk registration to N
     */
    public void No_bulkRegi() {
        clickOnElement(bulkRegReguired_N, "bulkRegReguired_N");
    }

    /**
     * Select the bulk payer type
     */
    public void selectBulkPayerType() {
        selectIndex(bulkPayerType, 1, "Bulk Payer Type");
    }

    /**
     * Select unregi BP mode
     */
    public void selectUnregiBPmode() {
        selectIndex(unRegBPmode, 2, "Unregistered Bulk Payer Mode");
    }


    /**
     * set the company code
     *
     * @param text
     */
    public void setCompCode(String text) {
        setText(compCode, text, "Company Code");
    }

    /**
     * Click Next Button
     *
     * @return
     */
    public void clickNext() {
        clickOnElement(Next, "Next");
    }

    /**
     * set the enterrpise limit
     */
    public void confirmEnterprise() {
        clickOnElement(confirmButton, "Confirm button");
    }
}
