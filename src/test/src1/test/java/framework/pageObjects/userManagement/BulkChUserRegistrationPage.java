package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Scanner;

/**
 * Created by rahul.rana on 5/27/2017.
 */
public class BulkChUserRegistrationPage extends PageInit {
    @FindBy(xpath = "//*[@id='bulkUserReg_register']/table/tbody/tr[1]/td[1]/a/img")
    private static WebElement DownloadImage;
    @FindBy(id = "submitButton")
    private static WebElement SubmitCsv;
    @FindBy(xpath = "//*[@id=\"bulkUserReg_register\"]/table/tbody/tr[4]/td/a/img")
    private static WebElement logFileImage;
    @FindBy(name = "bulkUploadFile")
    private WebElement Upload;

    public BulkChUserRegistrationPage(ExtentTest t1) {
        super(t1);
    }

    public static BulkChUserRegistrationPage init(ExtentTest t1) {
        return new BulkChUserRegistrationPage(t1);
    }

    public static void clickOnStartDownload() throws Exception {
        pageInfo.info("Deleting existing file with prefix - BulkChannelRegistrationCore");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkChannelRegistrationCore"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        DownloadImage.click();
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        pageInfo.info("Clicked on Download Image!");
    }

    /**
     * Navigate to Operator User Creation
     *
     * @throws Exception
     */
    public void navBulkUserRegistration() throws Exception {
        navigateTo("BULK_ALL", "BULK_BLK_CHUSR", "Navigate to Bulk User Registration");
    }

    /**
     * Navigate to Operator User Creation
     *
     * @throws Exception
     */
    public void navBulkUserRegistrationApproval() throws Exception {
        navigateTo("BULK_ALL", "BULK_BULK_AP", "Navigate to Bulk User Approval");
    }

    public void submitCsv() throws Exception {
        clickOnElement(SubmitCsv ,"Upload Csv");
        Thread.sleep(Constants.MAX_WAIT_TIME);
        Utils.captureScreen(pageInfo);
    }

    public void uploadFile(String file) {
        setText(Upload, file, "Upload File");
    }

    public void clickOnStartDownloadLogFile() throws Exception {
        pageInfo.info("Deleting existing file with prefix - BulkUserRegistrationLogs");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserRegistrationLogs"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        logFileImage.click();
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        pageInfo.info("Clicked on Download Image!");
    }

    public void checkLogFileDetails(String msisdn) throws Exception {
        Thread.sleep(500);
        try {
            File dir = new File(FilePath.dirFileDownloads);
            String logFile = "";
            String[] types = {"log"};
            Collection<File> collection = FileUtils.listFiles(dir, types, true);
            for (File s : collection) {
                if (s.getName().contains("BulkUserRegistrationLogs")) {
                    logFile = s.getName();
                    break;
                }
            }
            File file = new File(FilePath.dirFileDownloads + "/" + logFile);
            Scanner s = new Scanner(file);
            int lineIndex = 1;
            boolean success = true;
            String[] matcherArray = {"2", "The given MSISDN" + msisdn + "is already registered in the system as different user"};
            //we know that the log will start from 9th line so moving to 9th line
            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (lineIndex == 9) {
                    //reading the 3rd line for log
                    for (int i = 0; i < matcherArray.length; i++) {
                        //replacing all the spaces in the log file and then comparing
                        if (line.replaceAll("\\s+", "").contains(matcherArray[i].replaceAll("\\s", ""))) {
                            pageInfo.pass("found text as " + matcherArray[i]);
                        } else {
                            success = false;
                            pageInfo.fail("couldn't match the details of the log file as desired");
                            break;
                        }
                    }
                    if (success) {
                        pageInfo.pass("log file contents matched as desired");
                    }
                    //now we don't want to read further
                    break;
                }
                lineIndex++;
            }
            s.close();
        } catch (IOException ioex) {
            // handle exception...
        }

    }


}

