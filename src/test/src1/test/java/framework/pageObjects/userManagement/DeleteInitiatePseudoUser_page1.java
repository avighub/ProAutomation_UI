package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by gurudatta.praharaj on 7/3/2018.
 */
public class DeleteInitiatePseudoUser_page1 extends PageInit {

    @FindBy(id = "categoryCodeList")
    private WebElement catCode;
    @FindBy(id = "parentId")
    private WebElement parentID;
    @FindBy(id = "ownerId")
    private WebElement ownerID;
    @FindBy(id = "msisdn")
    private WebElement msisdn;
    @FindBy(id = "delPseudo_delete_button_submit")
    private WebElement submit;
    @FindBy(id = "delPseudo_delConfirm_button_delete")
    private WebElement confirmDelete;

    public DeleteInitiatePseudoUser_page1(ExtentTest test) {
        super(test);
    }

    public static DeleteInitiatePseudoUser_page1 init(ExtentTest test) {
        return new DeleteInitiatePseudoUser_page1(test);
    }

    public DeleteInitiatePseudoUser_page1 navToDeletePseudoUser() throws Exception {
        navigateTo("PSEUDO_ALL", "PSEUDO_PSEUDO_DEL", "Delete Pseudo User");
        return this;
    }

    public DeleteInitiatePseudoUser_page1 setPseudoCatCode(String code) {
        selectValue(catCode, code, "Select Pseudo Category Code");
        return this;
    }

    public DeleteInitiatePseudoUser_page1 setParentCatCode() {
        selectIndex(parentID, 1, "Select Parent Category Code");
        return this;
    }

    public DeleteInitiatePseudoUser_page1 setOwnerCatCode() {
        selectIndex(ownerID, 1, "Select Owner Category Code");
        return this;
    }

    public DeleteInitiatePseudoUser_page1 setMsisdn(String pseudoMsisdn) {
        setText(msisdn, pseudoMsisdn, "enter Pseudo Msisdn");
        return this;
    }

    public DeleteInitiatePseudoUser_page1 clickSubmit() {
        clickOnElement(submit, "click submit button");
        return this;
    }

    public void confirmDelete() {
        clickOnElement(confirmDelete, "Confirm Delete");
    }
}
