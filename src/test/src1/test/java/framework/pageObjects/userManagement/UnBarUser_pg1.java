package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UnBarUser_pg1 extends PageInit {


    @FindBy(id = "_uTypeId")
    WebElement userType;
    @FindBy(id = "onlyMsisdn")
    WebElement msisdn;
    @FindBy(id = "viewBlackListAction_viewBlackListUser_button_submit")
    WebElement submit;
    @FindBy(id = "viewBlackListAction_viewBlackListUser_check1")
    WebElement selectUser;
    @FindBy(id = "onlyLogin")
    WebElement loginID;
    @FindBy(id = "viewBlackListAction_viewBlackListUser_back")
    WebElement unbarSubmit;
    @FindBy(name = "action:viewBlackListAction_confirmUnBlackListUser")
    WebElement unbarConfirm;
    @FindBy(className = "actionMessage")
    private WebElement ActionMessage;

    public UnBarUser_pg1(ExtentTest t1) {
        super(t1);
    }

    public void navigateNAToUnBarUserLink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_VBLK", "Bar User");
    }

    public void selectUserTypeByValue(String text) {
        selectValue(userType, text, "User Type");
    }

    public void setMsisdn(String mobileNo) {
        setText(msisdn, mobileNo, "MSISDN");
    }

    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }

    public void selectUserFromList() {
        clickOnElement(selectUser, "Select User");
    }

    public void clickOnUnbarSubmit() {
        clickOnElement(unbarSubmit, "Submit Button");
    }

    public void clickOnUnbarConfirm() throws Exception {
        clickOnElement(unbarConfirm, "Confirm Button");
    }

    public String getMessage() {
        pageInfo.info("Message");
        return String.valueOf(ActionMessage.getText());
    }


    public void setLoginID(String loginId) {
        setText(loginID, loginId, "Login ID");
    }
}
