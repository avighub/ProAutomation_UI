package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class CommonChannelUserPage extends PageInit {

    @FindBy(name = "action:editCH_addMoreLiquidationBankMod")
    public WebElement addLiquidationBank;
    @FindBy(name = "action:editCH_addMoreBankRowsForEdit")
    public WebElement addMoreBank;
    /**
     * NAVIGATION
     */
    @FindBy(id = "confirm_button_next")
    public WebElement nextUserInfo;
    @FindBy(id = "editCH_assignGroupRole_button_next")
    public WebElement nextHierarcy;
    @FindBy(id = "editCH_loadGroupRoles_button_next")
    public WebElement nextGroupRole;
    @FindBy(id = "editCH_getEnterpriseDepositCategoryEdit_button_next")
    public WebElement nextEnterprisePreference;
    @FindBy(id = "editCH_loadEnterpriseCategoryWalletEdit_button_next")
    public WebElement nextWalletCombination;
    @FindBy(id = "add1_addChannelUser_submit")
    public WebElement nextWalletType;
    @FindBy(name = "action:editCH_assignBankOrEntepriseEdit")
    public WebElement nextWalletMap;
    @FindBy(name = "action:editCH_assignBankForEntepriseEdit")
    public WebElement nextEnterprisePreferenceApproval;
    @FindBy(id = "editCH_addChannelUser_submit")
    public WebElement nextBankPref;
    @FindBy(name = "action:editCH_confirm")
    public WebElement nextBankPrefModification;
    @FindBy(name = "action:editCH_addMoreBankRowsForEdit")
    public WebElement addMoreBankPrefModification;
    @FindBy(name = "action:confirm2_save")
    public WebElement finalSubmitModification;
    @FindBy(name = "action:editCH_loadEnterpriseCategoryWalletEdit")
    public WebElement selectEnterprisePreferences;
    @FindBy(name = "action:editCH_associateWalletInEdit")
    public WebElement assosicateWalletNext;
    @FindBy(className = "actionMessage")
    WebElement ActionMessage;
    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;
    @FindBy(css = "input[name = 'msisdn'][type = 'text']")
    WebElement msisdnCommon;
    @FindBy(className = "wwFormTableC")
    WebElement webTable;
    /*
     * COMMON
	 */
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr/td[2]/span/img")
    private WebElement domainDownArrow;
    /**
     * APPROVE PAGE
     */
    @FindBy(xpath = "//*[@id='level2ApprCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement approveCategoryDownArrow;
    @FindBy(xpath = ".//*[@id='level2ApprCH_loadChannelUser']/table/tbody/tr[1]/td[3]/a/img")
    private WebElement approveFirstNameArrow;
    @FindBy(xpath = "//input[@name='msisdn' and @type='text']")
    private WebElement approveMsisdn;
    @FindBy(id = "level2ApprCH_loadChannelUser_button_submit")
    private WebElement approveSubmit;
    @FindBy(id = "viewForm_button_approve")
    private WebElement confirmApprove;
    @FindBy(id = "viewForm_reject")
    private WebElement rejectApprove;
    /**
     * MODIFY APPROVE PAGE
     */
    @FindBy(xpath = "//*[@id=\"editApprCH_loadChannelUser\"]/table/tbody/tr/td[2]/span/img")
    private WebElement modApproveCategoryDownArrow;
    @FindBy(xpath = "//*[@id='editApprCH_loadChannelUser']/table/tbody/tr[1]/td[3]/a/img")
    private WebElement modApproveFirstNameArrow;
    @FindBy(id = "editApprCH_loadChannelUser_button_submit")
    private WebElement modApproveSubmit;
    @FindBy(id = "viewForm_button_approve")
    private WebElement modFinalApproveSubmit;
    /**
     * VIEW CHANNEL USER PAGE
     */
    @FindBy(xpath = "//*[@id='viewCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement viewChannelUserCategoryDownArrow;
    @FindBy(xpath = ".//*[@id='viewCH_loadChannelUser']/table/tbody/tr[1]/td[3]/a/img")
    private WebElement viewFirstNameArrow;
    @FindBy(id = "viewCH_loadChannelUser_button_submit")
    private WebElement viewChannelSubmit;
    @FindBy(xpath = "//*[@id='viewForm_usersMap_userName']")
    private WebElement viewChannelFirstName;
    @FindBy(xpath = "//*[@id='viewForm_usersMap_externalCode']")
    private WebElement viewChannelExternalCode;
    @FindBy(xpath = "//*[@id='viewForm_usersMap_loginId']")
    private WebElement viewChannelLoginId;
    @FindBy(xpath = "//*[@id='viewForm_idExpiryDate']")
    private WebElement viewChannelExpiryDate;
    /**
     * BANK MAPPING PAGE IN CASE OF MODIFY USER
     */
    @FindBy(id = "walletTypeID1")
    private WebElement walletType;
    @FindBy(name = "bankCounterList[1].providerSelected")
    private WebElement providerType;
    @FindBy(id = "grade1")
    private WebElement grade;
    @FindBy(id = "walletRoles1")
    private WebElement walletRole;
    @FindBy(id = "tcp1")
    private WebElement tcp;
    @FindBy(id = "customerId1")
    private WebElement customerId;
    @FindBy(id = "accountNumber1")
    private WebElement accountNumber;
    @FindBy(id = "editCH_addChannelUser_bankCounterList_1__primaryAccountSelected")
    private WebElement primaryAccountSelected;
    @FindBy(id = "liquidationStatus10")
    private WebElement liquidationStatus;
    /**
     * DELETE PAGE
     */
    @FindBy(xpath = "//*[@id='deleteCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement deleteCategoryDownArrow;
    @FindBy(xpath = "//*[@id='deleteCH_loadChannelUser']/table/tbody/tr[1]/td[3]/a/img")
    private WebElement deleteFirstNameArrow;
    @FindBy(id = "deleteCH_loadChannelUser_button_submit")
    private WebElement deleteSubmit;
    @FindBy(id = "viewForm_button_submit")
    private WebElement confirmSuspend;
    /**
     * DELETE Approval PAGE
     */
    @FindBy(xpath = "//*[@id='deleteApprCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement deleteCategoryApprovalDownArrow;
    @FindBy(xpath = "//*[@id='deleteApprCH_loadChannelUser']/table/tbody/tr[1]/td[3]/a/img")
    private WebElement deleteFirstNameApprovalDownArrow;
    @FindBy(id = "deleteApprCH_loadChannelUser_button_submit")
    private WebElement deleteApprovalSubmit;
    /**
     * MODIFY PAGE
     */
    @FindBy(xpath = "//*[@id='edit_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement modifyCategoryDownArrow;
    @FindBy(xpath = "//*[@id='edit_loadChannelUser']/table/tbody/tr[1]/td[3]/a/img")
    private WebElement modifyFirstNameArrow;
    @FindBy(id = "edit_loadChannelUser_button_submit")
    private WebElement modifySubmit;
    @FindBys({@FindBy(xpath = "(//form[@id='add1_addChannelUser']/table[1]/tbody/tr/td[2])[position()>1]")})
    private List<WebElement> selectedWalletType;
    @FindBys({@FindBy(xpath = "(//form[@id='editCH_addChannelUser']/table[1]/tbody/tr/td[7])[position()>1]")})
    private List<WebElement> selectedAccountType;
    @FindBy(id = "confirm_webLoginId")
    private WebElement loginId;
    @FindBy(id = "confirm_webPassword")
    private WebElement password;
    @FindBy(id = "confirm_confWebPassword")
    private WebElement confirmPassword;
    @FindBy(id = "confirm_button_next")
    private WebElement nextBtn1;
    @FindBy(id = "editCH_assignGroupRole_button_next")
    private WebElement nextBtn2;
    @FindBy(id = "domainCodeList")
    private WebElement modPageDomain;
    @FindBy(id = "categoryCodeList")
    private WebElement modPageCategory;


    /* @FindBy(id = "editCH_loadGroupRoles_button_next")
     private WebElement nextBtn3;
 */
    @FindBy(xpath = "//td[2]/input[@id='add1_addChannelUser_submit']")
    private WebElement nextBtn4;
    @FindBy(xpath = "//td[2]/input[@id='editCH_addChannelUser_submit']")
    private WebElement confirm1;
    @FindBy(xpath = "//td[1]/input[@id='add1_addChannelUser_submit']")
    private WebElement confirn2;
    /*
    * MODIFY REJECT
    * */
    @FindBy(id = "viewForm_reject")
    private WebElement modifyRejectBtn;
    @FindBy(xpath = "//span[@class='actionMessage']")
    private WebElement modifyRejectMsg;
    @FindBy(css = "input[name='msisdn'][id'confirm_msisdn']")
    private WebElement modifyMSISDNTBox;
    /*
    * SUSPEND CHANNEL USER APPROVAL
    * */
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr/td[2]/span/input[3]")
    private WebElement suspendDomainDropDown;
    @FindBy(xpath = "//*[@id='suspendCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement suspendCategoryDownArray;

    @FindBy(xpath = "//*[@id='suspendApprCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement suspendApprovalCategoryDownArrow;

    @FindBy(xpath = "//*[@id=\"suspendApprCH_loadChannelUser\"]/table/tbody/tr[1]/td[3]/a/img")
    private WebElement firstNameSearch;
    @FindBy(id = "suspendCH_loadChannelUser_button_submit")
    private WebElement submitSuspendFirstPage;
    @FindBy(id = "suspendApprCH_loadChannelUser_button_submit")
    private WebElement submitSuspendApproval;
    @FindBy(id = "viewForm_button_approve")
    private WebElement approveSuspendButton;
    @FindBy(id = "viewForm_reject")
    private WebElement rejectSuspendButton;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr/td[2]/span/img")
    private WebElement suspendDomainDropDown1;
    @FindBy(xpath = "//*[@id='suspendCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement suspendCategoryDropDown1;


    public CommonChannelUserPage(ExtentTest t1) {
        super(t1);
    }

    /**
     * Initiate Providers Verification
     *
     * @param user
     * @return
     * @throws Exception
     */
    public static void verifyProvidersDetails(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyProvidersDetails", ExtentColor.BLUE);
        pageInfo.info(m); // Method Start Marker

        try {

            //now iterating the table for reading the contents
            WebElement subscriberTable = driver.findElement(By.className("wwFormTableC"));
            List<WebElement> tableRows = subscriberTable.findElements(By.tagName("tr"));
            int column = 0;
            int count = 0;
            boolean isValid = true;
            List<String> providerTypes = DataFactory.getAllProviderNames();

            outerloop:
            for (int i = 0; i < tableRows.size(); i++) //this for loop increments each of these tr's
            {
                if (i == 0)
                    continue;
                List<WebElement> rowTDs = tableRows.get(i).findElements(By.tagName("td"));//this gets every td in the current tr and puts it into a list
                for (WebElement singleTD : rowTDs) //this increments through that list of td's
                {
                    //increment the column counter
                    if (column == 0) {
                        Select select = new Select(singleTD.findElement(By.tagName("select")));
                        if (!providerTypes.contains(select.getFirstSelectedOption().getText())) {
                            isValid = false;
                            break outerloop;
                        }
                    } else if (column >= 1) {
                        continue;
                    }
                    column++;
                    count++;
                }
                column = 0; //reset the column
            }
            //checking if the size of provider list and the number of elements in dropdown are the same
            if (isValid && (count == providerTypes.size())) {
                pageInfo.pass("Providers verified for channel user");

            } else {
                pageInfo.fail("Providers coming wrong for channel user");

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }


    }

    /**
     * Initiate Subscriber Approval
     *
     * @param user
     * @return
     * @throws Exception
     */
    public static void verifyWalletPageDetails(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyWalletPageDetails", ExtentColor.BLUE);
        pageInfo.info(m); // Method Start Marker

        try {

            //now iterating the table for reading the contents
            WebElement subscriberTable = driver.findElement(By.className("wwFormTableC"));
            List<WebElement> tableRows = subscriberTable.findElements(By.tagName("tr"));
            int column = 0;
            int count = 0;
            boolean isValid = true;
            List<String> walletTypes = DataFactory.getWalletForChannelUser();

            outerloop:
            for (int i = 0; i < tableRows.size(); i++) //this for loop increments each of these tr's
            {
                if (i == 0)
                    continue;
                List<WebElement> rowTDs = tableRows.get(i).findElements(By.tagName("td"));//this gets every td in the current tr and puts it into a list
                for (WebElement singleTD : rowTDs) //this increments through that list of td's
                {
                    //increment the column counter
                    if (column == 1) {
                        Select select = new Select(singleTD.findElement(By.tagName("select")));
                        if (!walletTypes.contains(select.getFirstSelectedOption().getText())) {
                            isValid = false;
                            break outerloop;
                        }
                    } else if (column >= 1) {
                        continue;
                    }
                    column++;
                    count++;
                }
                column = 0; //reset the column
            }
            if (isValid) {
                pageInfo.pass("Wallets verified for channel user");

            } else {
                pageInfo.fail("Wallets coming wrong for channel user");

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }
    }

    /**
     * NAVIGATION
     */

    public static CommonChannelUserPage init(ExtentTest t1) {
        return new CommonChannelUserPage(t1);
    }

    public void setMsisdnForAction(String msisdn) {
        setText(msisdnCommon, msisdn, "MSISDN");
    }

    public boolean isFinalSubmitAvailable() {
        return fl.elementIsDisplayed(finalSubmitModification);
    }

    public void clickonSuspendReject() {
        clickOnElement(rejectSuspendButton, "rejectSuspendButton");
    }

    /**
     * NAVIGATIONs
     */

    public void navigateToChannelUserApproval() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_CUAP02", "Channel User Approval");
    }

    public void navModifyChannelUserApproval() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_MCUAP1", "Modify Channel User Approval");
    }

    public void navViewChannelUser() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_VCU", "View Channel User");
    }

    public void navDeleteChannelUser() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_DCU", "Delete Channel User");
    }

    public void navDeleteChannelUserApproval() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_DCUAP1", "Approve Delete Channel User");
    }

    public void navModifyChannelUser() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_MCU", "Modify Channel User");
    }

    public void navSuspendChannelUserApproval() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_SCUAP1", "Suspend Channel User Approval");
    }

    public void navSuspendChannelUser() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_SCU", "Suspend Channel User");
    }

    public void navResumeChannelUser() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_RCU", "Resume Channel User");
    }

    public void navResumeChannelUserApproval() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_RCUAP1", "Resume Channel User Approval");
    }

    /***********************************************
     *  E N D   O F   P A G E   N A V I G A T I O N S
     ***********************************************/

    public CommonChannelUserPage clickNextUserDetail() {
        clickOnElement(nextUserInfo, "nextUserInfo");
        return this;
    }

    public CommonChannelUserPage clickNextUserHeirarcy() {
        clickOnElement(nextHierarcy, "nextHierarcy");
        return this;
    }

    public CommonChannelUserPage clickNextWebRole() {
        clickOnElement(nextGroupRole, "nextGroupRole");
        return this;
    }

    public void clickNextWalletType() {
        clickOnElement(nextWalletType, "nextWalletType");
    }

    public void clickNextEnterprisePreference() {
        clickOnElement(nextEnterprisePreference, "nextEnterprisePreference");
    }

    public void clickNextEnterprisePreferenceApproval() {
        clickOnElement(nextEnterprisePreferenceApproval, "nextEnterprisePreferenceApproval");
    }

    public void clickNextWalletCombination() {
        clickOnElement(nextWalletCombination, "nextWalletCombination");
    }

    public CommonChannelUserPage clickNextWalletMap() {
        clickOnElement(nextWalletMap, "nextWalletMap");
        return this;
    }
    /*public void clickNxtModify2() throws InterruptedException {
        modifyHierarchyNextBtn.click();
        Thread.sleep(3000);
        pageInfo.info("Click on Next Button on Modify Channel User Page");
    }*/

    public void clickNextBankMap() {
        clickOnElement(nextBankPref, "nextBankPref");
    }

    public CommonChannelUserPage clickNextBankMapModification() {
        clickOnElement(nextBankPrefModification, "nextBankPrefModification");
        return this;
    }

    public void finalSubmitForModification() {
        clickOnElement(finalSubmitModification, "finalSubmitModification");
    }

    public void clickEnterprisePreference() {
        clickOnElement(selectEnterprisePreferences, "selectEnterprisePreferences");
    }

    public void clickEnterpriseAssociateWallet() {
        clickOnElement(assosicateWalletNext, "assosicateWalletNext");
    }

    public void clickOnModifyReject() {
        modifyRejectBtn.click();
        pageInfo.info("Click on Reject Button on Modify Channel User Approval Page");
    }

    public CommonChannelUserPage clickNxtModify() throws InterruptedException {
        nextBtn1.click();
        Thread.sleep(3000);
        pageInfo.info("Click on Next Button on Modify Channel User Page");
        return this;
    }

    /**
     * methods written for adding bank account in case of modification of user
     */

    public void clickNxtModify1() throws InterruptedException {
        nextBtn2.click();
        //nextBtn3.click();
        Thread.sleep(3000);
        pageInfo.info("Click on Next Button on Modify Channel User Page");
    }

    public void clickConfirm() throws InterruptedException {
        confirm1.click();
        Thread.sleep(3000);

        confirn2.click();
        pageInfo.info("Click on Confirm Button on Modify Channel User Page");
    }

    public void clickNxt1Modify() throws InterruptedException {
        nextBtn1.click();
        pageInfo.info("Click on Next Button on Modify Channel User Page");
        Thread.sleep(1000);
    }

    public void clickNxt2Modify() throws InterruptedException {
        nextBtn2.click();
        pageInfo.info("Click on Next Button on Modify Channel User Page");
        Thread.sleep(1000);
    }

    /*public void clickNxt3Modify()throws InterruptedException {
        nextBtn3.click();
        pageInfo.info("Click on Next Button on Modify Channel User Page");
        Thread.sleep(2000);
    }*/
    public void clickNxt4Modify() throws InterruptedException {
        nextBtn4.click();
        pageInfo.info("Click on Next Button on Modify Channel User Page");
        Thread.sleep(2000);
    }

    public void clickConfirm1Modify() throws InterruptedException {
        confirm1.click();
        pageInfo.info("Click on Confirm Button on Modify Channel User Page");
        Thread.sleep(1000);
    }

    public void clickConfirm2Modify() throws InterruptedException {
        confirn2.click();
        pageInfo.info("Click on Confirm Button on Modify Channel User Page");
        Thread.sleep(1000);
    }

    /**
     * select provider type
     * *@throws Exception
     */
    public void addMoreBankModification() throws Exception {
        addMoreBankPrefModification.click();
        pageInfo.info("Clicked Add more button in bank page of user modification ");
    }

    /**
     * select provider type
     * *@throws Exception
     */
    public void selectProviderType(int index) throws Exception {
        Select selectProviderType = new Select(driver.findElement(By.name("bankCounterList[" + index + "].providerSelected")));
        String selectedValue = selectProviderType.getFirstSelectedOption().getText();
        selectProviderType.selectByVisibleText(selectedValue);
        pageInfo.info("Selected Wallet Type: " + selectedValue);
        Thread.sleep(2000);
    }

    /**
     * Select linked bank
     */
    public void selectLinkedBank(String bankName) throws Exception {

        Select selectLinkedBank = new Select(driver.findElement(By.id("walletTypeID2")));
        //String selectedValue = selectLinkedBank.getFirstSelectedOption().getText();
        selectLinkedBank.selectByVisibleText(bankName);
        pageInfo.info("Selected linked bank Type: " + bankName);
        Thread.sleep(2000);
    }

    /**
     * Change Bank Status
     */
    public void changeBankStatus(String status, int bankID) {
        WebElement bank = driver.findElement(By.name("bankCounterList[" + bankID + "].statusSelected"));
        selectValue(bank, status, "Bank Status");
    }

    public void changeBankStatus(String bankName, String status) {
        WebElement bank = driver.findElement(By.xpath("//select[contains(@name,'paymentType')]/option[ text() = '"+bankName+"' and @selected ='selected']/following::select[contains(@name,'statusSelected')][1]"));
        selectValue(bank, status, bankName +" Bank's  Status");
    }

    /**
     * Select Grade
     */
    public void selectGrade(int index, String grade) throws Exception {
        Select selectGrade = new Select(driver.findElement(By.name("bankCounterList[" + index + "].channelGradeSelected")));
        selectGrade.selectByVisibleText(grade);
        pageInfo.info("Selected grade Type: " + grade);
        Thread.sleep(2000);
    }

    /**
     * Select Grade
     */
    public void selectMobileRole(int index) throws Exception {
        Select selectMobileRole = new Select(driver.findElement(By.name("bankCounterList[" + index + "].groupRoleSelected")));
        String selectedValue = selectMobileRole.getFirstSelectedOption().getText();
        Select dropdown = new Select(walletRole);
        dropdown.selectByVisibleText(selectedValue);
        pageInfo.info("Selected mobile role Type: " + selectedValue);
    }

    /**
     * Select Tcp
     */
    public void selectTcp(int index) throws Exception {

        Select selectTcp = new Select(driver.findElement(By.name("bankCounterList[" + index + "].tcpSelected")));
        selectTcp.selectByIndex(1);
        pageInfo.info("Selected tcp Type: " + 1);
        Thread.sleep(2000);
    }

    public void setCustomerId() {
        customerId.sendKeys("45345345");
    }

    public void setAccountNumber() {
        accountNumber.sendKeys("45345345");
    }

    public void selectPrimaryAccount() throws Exception {
        Select dropdown = new Select(primaryAccountSelected);
        dropdown.selectByVisibleText("No");
        pageInfo.info("Selected Primary account Type:No");
    }

    /**
     * select Domain name
     *
     * @param domainCode
     * @throws Exception
     */
    public void selectDomainName(String domainCode) throws Exception {
        domainDownArrow.click();
        Thread.sleep(500);
        domainDownArrow.click();
        Thread.sleep(500);
        domainDownArrow.click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//div[@resultvalue='" + domainCode + "']")).click();
        pageInfo.info("Selected Domain Name: " + domainCode);
        Thread.sleep(3500); // static wait, as selecting category is not consistent!
    }

    public void selectCategoryforApproval(String categoryCode) throws Exception {
        selectCategory(approveCategoryDownArrow, categoryCode);
    }

    public void selectFirstNameforApproval(String firstName) throws Exception {
        selectFirstName(approveFirstNameArrow, firstName);
    }

    public void setApproveMsisdn(String Msisdn) throws Exception {
        Utils.setValueUsingJs(approveMsisdn, Msisdn);

    }

    public void selectFirstNameforDeletion(String firstName) throws Exception {
        selectFirstName(deleteFirstNameArrow, firstName);
    }

    public void selectFirstNameforModification(String firstName) throws Exception {
        selectFirstName(modifyFirstNameArrow, firstName);
    }

    public void selectFirstNameforModifiedApproval(String firstName) throws Exception {
        selectFirstName(modApproveFirstNameArrow, firstName);
    }

    public void selectFirstNameforViewChannelUser(String firstName) throws Exception {
        selectFirstName(viewFirstNameArrow, firstName);
    }

    public void selectCategoryforDeletion(String categoryCode) throws Exception {
        selectCategory(deleteCategoryDownArrow, categoryCode);
    }

    public void selectCategoryforModification(String categoryCode) throws Exception {
        selectCategory(modifyCategoryDownArrow, categoryCode);
    }

    public void selectCategoryforModifiedApproval(String categoryCode) throws Exception {
        selectCategory(modApproveCategoryDownArrow, categoryCode);
    }

    public void selectCategoryforViewChannelUser(String categoryCode) throws Exception {
        selectCategory(viewChannelUserCategoryDownArrow, categoryCode);
    }

    public void submitApprovalDetail() throws InterruptedException {
        clickOnElement(approveSubmit, "approveSubmit");
    }

    public void submitModifiedApprovalDetail() throws InterruptedException {
        clickOnElement(modApproveSubmit, "modApproveSubmit");
    }

    public void submitModifiedFinalApprovalDetail() throws InterruptedException {
        clickOnElement(modFinalApproveSubmit, "modFinalApproveSubmit");
    }

    public void viewUserDetail() throws InterruptedException {
        clickOnElement(viewChannelSubmit, "viewChannelSubmit");
    }

    public void confirmApproval() throws InterruptedException {
        Utils.scrollToBottomOfPage();
        Utils.putThreadSleep(5000);
        clickOnElement(confirmApprove, "confirmApprove");
        Utils.putThreadSleep(5000);
    }

    public void rejectApproval() throws InterruptedException {
        clickOnElement(rejectApprove, "rejectApprove");
    }

    public void submitDeleteDetail() {
        clickOnElement(deleteSubmit, "deleteSubmit");
    }

    public void confirmDelete() {
        clickOnElement(confirmSuspend, "Confirm");
    }

    public void submitModifyDetail() {
        clickOnElement(modifySubmit, "modifySubmit");
    }

    /**
     * Select Category Name
     *
     * @param categoryCode
     * @throws Exception
     */
    private void selectCategory(WebElement elem, String categoryCode) throws Exception {
        try {
            elem.click();
            Thread.sleep(1000);
            elem.click();
            Thread.sleep(1000);
            elem.click();
            Thread.sleep(500);
            driver.findElement(By.xpath("//*[@id='page-home']/span[2]/div[@resultvalue='" + categoryCode + "']")).click();
            pageInfo.info("Selected Category Name: " + categoryCode);
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * selectFirstName
     *
     * @throws Exception
     */
    private void selectFirstName(WebElement elem, String firstName) throws Exception {
        elem.click();
        pageInfo.info("Click on select first name icon");

        Thread.sleep(1000);
        String MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String ChildWindow = i1.next();
            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                driver.switchTo().window(ChildWindow);
                WebElement wFirstName = driver.findElement(By.xpath(".//*[@id='searchCH_setSearchedChannelUser_userName']"));
                WebElement wSearch = driver.findElement(By.xpath("html/body/table[1]/tbody/tr[2]/td/table[2]/tbody/tr/td[3]/a/img"));

                wFirstName.sendKeys(firstName);
                wSearch.click();

                WebElement wUserlist = driver.findElement(By.xpath(".//*[@id='searchCH_setSearchedChannelUser_userId']"));
                Select wSelect = new Select(wUserlist);
                wSelect.selectByIndex(1);
                driver.findElement(By.xpath("//input[@value='Submit']")).click();
                driver.switchTo().window(MainWindow);
                fl.contentFrame();
                return;
            }
        }
    }

    public void setMsisdn(User user) {
        List<WebElement> wSubmit = driver.findElements(By.id("editApprCH_loadChannelUser_msisdn"));
        for (WebElement webElement : wSubmit) {
            if (webElement.getTagName().equalsIgnoreCase("input") && webElement.isDisplayed()) {
                setText(webElement, user.MSISDN, "MSISDN");
                break;
            }
        }
    }

    /**
     * Get Action Message
     *
     * @return
     * @throws Exception
     */
    public String getActionMessage() throws Exception {
        return wait.until(ExpectedConditions.elementToBeClickable(ActionMessage)).getText();
    }

    /**
     * Get Error message
     *
     * @return
     * @throws Exception
     */
    public String getErrorMessage() throws Exception {
        return wait.until(ExpectedConditions.elementToBeClickable(ErrorMessage)).getText();
    }

    /**
     * function to compare the values present in the view channel user page
     *
     * @param user
     * @throws InterruptedException
     */
    public void viewChannelUserPageValidation(User user) throws Exception {
        Assertion.verifyEqual(viewChannelFirstName.getText(), user.FirstName, "Verify First Name", pageInfo);
        Assertion.verifyEqual(viewChannelLoginId.getText(), user.LoginId, "Verify Login ID", pageInfo);

       /* if (user.FirstName.equals(viewChannelFirstName.getText()) &&
                user.LoginId.equals(viewChannelLoginId.getText())) {
            pageInfo.pass("Successfully Verified First name of Channel User - " + user.LoginId);
            pageInfo.pass("Successfully Verified Login Id of Channel User - " + user.LoginId);
            return true;
        } else {
            Assertion.verifyEqual(user.FirstName, viewChannelFirstName.getText(), "first name verification", pageInfo);
            Assertion.verifyEqual(user.LoginId, viewChannelLoginId.getText(), "login id verification", pageInfo);
            return false;
        }*/


        // TODO
    }

    /**
     * Select from Dojo Combo
     * TODO- move to PageInit
     *
     * @param elem
     * @param text
     * @throws Exception
     */
    public void selectFromDojoCombo(WebElement elem, String text, String description) throws Exception {
        try {
            setText(elem, text.substring(0, 2), description);
            Thread.sleep(3000);
            setText(elem, text.substring(0, 2), description);
            wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='page-home']/span/div[@resultname='" + text + "']")))).click();
            Thread.sleep(2500);
            pageInfo.info("Select From Dojo Combo: " + description + ", Text: " + text);
        } catch (Exception e) {
            try {
                pageInfo.info("Failed to select from DojoCombo, retrying");
                setText(elem, text.substring(0, 2), description);
                wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='page-home']/span[2]/div[@resultname='" + text + "']")))).click();
                Thread.sleep(2500);
                pageInfo.info("Select From Dojo Combo: " + description + ", Text: " + text);
            } catch (Exception e1) {
                pageInfo.fail("Failed to select from DojoCombo due to exception");
            }
        }

    }

    public void selectSuspendDomain(String domainCode) throws Exception {
        selectDomainName(domainCode);
    }

    @FindBy(xpath = "//*[@id='suspendCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    WebElement imgSuspendCategoy;

    public void selectSuspendCategory(String cateGoryCode) throws Exception {
        selectCategory(imgSuspendCategoy, cateGoryCode);
    }

    public void selectSuspendApprovalCategory(String cateGoryCode) throws Exception {
        selectCategory(suspendApprovalCategoryDownArrow, cateGoryCode);
    }

    public void selectSuspendFirstName(String firstName) throws Exception {
        if (driver.findElements(By.xpath("//*[@id=\"suspendApprCH_loadChannelUser\"]/table/tbody/tr[1]/td[3]/a/img")).size() != 0)
            selectFirstName(firstNameSearch, firstName);
        else if (driver.findElements(By.xpath("//*[@id=\"suspendCH_loadChannelUser\"]/table/tbody/tr[1]/td[3]/a/img")).size() != 0)
            selectFirstName(driver.findElement(By.xpath("//*[@id=\"suspendCH_loadChannelUser\"]/table/tbody/tr[1]/td[3]/a/img")), firstName);
        pageInfo.info("Selected First Name " + firstName);
    }

    public void suspendSubmitFirstPageBtn() {
        if (fl.elementIsDisplayed(submitSuspendFirstPage))
            clickOnElement(submitSuspendFirstPage, "submitSuspendFirstPage");
        else if (fl.elementIsDisplayed(submitSuspendApproval))
            clickOnElement(submitSuspendApproval, "submitSuspendApproval");
    }

    public void clickSuspendApprove() {
        approveSuspendButton.click();
        pageInfo.info("Clicked on Approve Button");
    }

    public void clickSuspendReject() {
        rejectSuspendButton.click();
        pageInfo.info("Clicked on Approve Button");
    }

    /*Select Category for deletion approval
       * */
    public void selectCategoryForDeletionApproval(String categoryCode) throws Exception {
        selectCategory(deleteCategoryApprovalDownArrow, categoryCode);
        pageInfo.info("Selected Category " + categoryCode);
    }

    /**
     * Select first Name for Approval
     *
     * @param firstName
     * @throws Exception
     */
    public void selectFirstNameforApprovalDeletion(String firstName) throws Exception {
        selectFirstName(deleteFirstNameApprovalDownArrow, firstName);
    }

    /**
     * Submit Delete Approval detail
     */
    public void submitDeleteApprovalDetail() {
        deleteApprovalSubmit.click();
        pageInfo.info("Click on Submit");
    }

    public void navigateToAddSubscriberPage() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSADD", "Add Subscriber Page");
    }

    public void navigateToModSubscriberPage() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSMOD", "Modify Subscriber Page");
    }

    public void clickAddLiquidationBank() {
        addLiquidationBank.click();
        pageInfo.info("Clicked Add more Liquidation Bank button in bank page of user modification ");
    }

    public void clickAddMoreBank() {
        addMoreBank.click();
        pageInfo.info("Clicked Add more Bank button in bank page of user modification ");
    }

    public void setMsisdninSubModPage() throws Exception {

    }

    public void selectStatusofLiquidationBank(String status) throws Exception {

        Select dropdown = new Select(liquidationStatus);
        dropdown.selectByVisibleText(status);
        pageInfo.info("Selected Status: " + status);
    }

    public void selectStatusOfBank(String status, String accountNumber) throws Exception {

        for (int i = 0; i < selectedAccountType.size(); i++) {
            String accNo = driver.findElement(By.name("bankCounterList[" + i + "].accountNumber")).getAttribute("value");
            if (accNo.equalsIgnoreCase(accountNumber)) {
                Select stat = new Select(driver.findElement(By.name("bankCounterList[" + i + "].statusSelected")));
                stat.selectByVisibleText(status);
                pageInfo.info("selected status as " + status);
            }
        }
    }

    public void selectDomainNameforsuspend(String domainName) throws Exception {
        suspendDomainDropDown1.click();
        Thread.sleep(500);
        suspendDomainDropDown1.click();
        Thread.sleep(500);
        suspendDomainDropDown1.click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//body[@id='page-home']/span/div[@resultname='" + domainName + "']")).click();
        Thread.sleep(2500); // static wait, as selecting category is not consistent!
        pageInfo.info("Selected Domain Name: " + domainName);
    }

    public void selectCategoryforsuspend(String categoryCode) throws Exception {
        try {
            suspendCategoryDropDown1.click();
            Thread.sleep(1000);
            suspendCategoryDropDown1.click();
            Thread.sleep(1000);
            suspendCategoryDropDown1.click();
            Thread.sleep(500);
            driver.findElement(By.xpath("(//body[@id='page-home']/span/div[@resultname='" + categoryCode + "'])[2]")).click();
            pageInfo.info("Selected Category Name: " + categoryCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateWalletStatus(String status, String walletId) throws Exception {

        String walletName = DataFactory.getWalletName(walletId);
        for (int i = 0; i < selectedWalletType.size(); i++) {
            Select sel = new Select(driver.findElement(By.name("counterList[" + i + "].paymentTypeSelected")));
            if (sel.getFirstSelectedOption().getText().equalsIgnoreCase(walletName)) {
                Select stat = new Select(driver.findElement(By.name("counterList[" + i + "].statusSelected")));
                stat.selectByVisibleText(status);
                pageInfo.info("selected status as " + status);
            }
        }
    }

    public boolean setMsisdnInUserModificationPage(String msisdn) throws Exception {
        boolean status = false;
        if (modifyMSISDNTBox.getAttribute("readonly").equalsIgnoreCase("true")) {
            pageInfo.pass("MSISDN field is Read-Only.");
            Utils.scrollToAnElement(modifyMSISDNTBox);
            Utils.captureScreen(pageInfo);
            status = true;
        } else {
            pageInfo.fail("MSISDN field is editable.");
            modifyMSISDNTBox.sendKeys(msisdn);
            Utils.captureScreen(pageInfo);
            submitApprovalDetail();
            Assertion.checkErrorMessageContain("channel.modify.msisdn.txn.pending", "Verify Error Message", pageInfo);
            pageInfo.info("Refer Screenshot for More Detail.", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        }

        return status;
    }

    public WebElement getDomain() {
        return modPageDomain;
    }
    public WebElement getCategory() {
        return modPageCategory;
    }
    public boolean isSelectedTextIsShownOnConfirmPage(String text) {
        if (webTable.findElements(By.xpath("//tr/td[contains(text(), '" + text + "')]")).size() > 0) {
            return true;
        }
        return false;
    }

    public void modifyLoginId(String text) {
        setText(loginId, text, "Web Login ID");
    }

    public void modifyPassword(String text) {
        setText(password, text, "Password");
        setText(confirmPassword, text, "Confirm Password");
    }


    public List<String> getAllValuesFromBankStatusDropdown() {
        return fl.getOptionValues(driver.findElement(By.name("bankCounterList[0].statusSelected")));
    }
}
