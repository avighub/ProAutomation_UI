package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.util.common.DataFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by ravindra.dumpa on 11/3/2017.
 */
public class ModifyChannelUser_pg1 extends PageInit {


    @FindBy(name = "webLoginId")
    WebElement webLoginId;

    @FindBy(id = "confirm_webPassword")
    WebElement webPassword;

    @FindBy(id = "confirm_confWebPassword")
    WebElement webConfirmPassword;

    @FindBy(id = "confirm_userNamePrefixId")
    private WebElement UserPrefix;

    @FindBy(id = "confirm_userName")
    private WebElement FirstName;

    @FindBy(id = "confirm_lastName")
    private WebElement LastName;
    @FindBy(id = "confirm_externalCode")
    private WebElement IdentificationNum;
    @FindBy(id = "confirm_email")
    private WebElement Email;
    @FindBy(id = "confirm_genderId")
    private WebElement Gender;
    @FindBy(id = "confirm_idType")
    private WebElement IDType;
    @FindBy(id = "confirm_idNo")
    private WebElement IDNumber;
    @FindBy(id = "confirm_webLoginId")
    private WebElement LoginId;
    @FindBy(id = "confirm_msisdn")
    private WebElement modifyMSISDN;
    @FindBy(id = "confirm_button_next")
    private WebElement next;

    @FindBy(id = "confirm_idIssueCountry")
    private WebElement IssueCountry;

    @FindBy(id = "confirm_nationality")
    private WebElement Nationality;

    @FindBy(id = "confirm_residenceCountry")
    private WebElement CurrentResidence;

    @FindBy(id = "confirm_employerName")
    private WebElement EmployeeName;

    @FindBy(id = "confirm_postalCode")
    private WebElement PostalCode;

    @FindBy(id = "isIdExpires")
    WebElement expireCheckBox;


    public ModifyChannelUser_pg1(ExtentTest t1) {
        super(t1);
    }

    public static ModifyChannelUser_pg1 init(ExtentTest t1) {
        return new ModifyChannelUser_pg1(t1);
    }

    public void setLastName(String name) {
        setText(LastName, name, "LastName");
    }

    public void navModChPage1() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_MCU", "ModifyC Channel User");
    }

    public ModifyChannelUser_pg1 setEmailId(String email) {
        setText(Email, email, "Set Email");
        return this;
    }

    public ModifyChannelUser_pg1 setMsisdn(String msisdn) {
        setText(modifyMSISDN, msisdn, "Set Msisdn");
        return this;
    }

    public void setwebloginID(String text) {

        webLoginId.clear();
        webLoginId.sendKeys(text);
        webPassword.clear();
        webPassword.sendKeys("Com@135");
        webConfirmPassword.clear();
        webConfirmPassword.sendKeys("Com@135");

    }

    public void clearAllMandatoryFields() {
        Select sel = new Select(UserPrefix);
        sel.selectByIndex(0);
        FirstName.clear();
        LastName.clear();
        IdentificationNum.clear();
        Email.clear();
        Select gen = new Select(Gender);
        gen.selectByIndex(0);
        Select id = new Select(IDType);
        id.selectByIndex(0);
        //IDNumber.clear();
        LoginId.clear();
        next.click();
    }

    public void clickNext() {
        next.click();
        pageInfo.info("Click on Button next!");
    }

    public void setPostalCode() {
        setText(PostalCode, "122001", "Postal Code");
    }

    public void setEmployeeName() {
        setText(EmployeeName, "EMP" + DataFactory.getRandomNumber(3), "Employee Name");
    }

    public void setIssueCountry() {
        selectVisibleText(IssueCountry, "India", "IssueCountry");
    }

    public void setNationality() {
        selectVisibleText(Nationality, "India", "Nationality");
    }

    public void setCurrentResidence() {
        selectVisibleText(CurrentResidence, "Australia", "CurrentResidence");
    }

    public ModifyChannelUser_pg1 expiryCheckBox() {
        clickOnElement(expireCheckBox, "Id Expiry Checkbox");
        return this;
    }
}
