package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by rahul.rana on 6/16/2017.
 */
public class AddEnterprise_pg1 {

    private static ExtentTest pageInfo;
    private static WebDriver driver;
    /*
     * Page Objects
	 */
    @FindBy(xpath = "//*[@type='checkbox']")
    List<WebElement> WebGroupRoles;
    @FindBy(id = "confirm2_loadEnterpriseCategoryWallet_enterpriseSubTypeName")
    WebElement walletSelect;
    @FindBys({@FindBy(xpath = "//select[@id='confirm2_loadEnterpriseCategoryWallet_enterpriseSubTypeName']")})
    List<WebElement> wallets;
    @FindBy(id = "confirm2_getEnterpriseDepositCategory_button_next")
    private WebElement Next;
    @FindBy(id = "confirm2_loadEnterpriseCategoryWallet_button_next")
    private WebElement NextLoadEntCat;

    public static AddEnterprise_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        AddEnterprise_pg1 page = PageFactory.initElements(driver, AddEnterprise_pg1.class);
        return page;
    }

    /**
     * Select Web group Role
     */
    public void selectallpref() {
        System.out.println("Select all pref");
        for (int i = 0; i < WebGroupRoles.size(); i++) {
            System.out.println("Got values for the web roles " + WebGroupRoles.get(i).getAttribute("value").toString());
            WebGroupRoles.get(i).click();
            pageInfo.info("Select checkbox: " + WebGroupRoles.get(i).getText());
        }
    }

    /**
     * Click Next Button
     *
     * @return
     */
    public void clickNext() {
        Next.click();
        pageInfo.info("Click On Next!");
    }

    public void selectType() {
        Select sel = new Select(walletSelect);
        sel.selectByIndex(1);
        pageInfo.info("Select Wallet Type");
    }

    public void selectTypeNormal() {
        Select sel = new Select(walletSelect);
        sel.selectByValue("12");
        pageInfo.info("Select Wallet Type");
    }

    /**
     * Click Next Button
     *
     * @return
     */
    public void clickNextLoadenterprise() {
        NextLoadEntCat.click();
        pageInfo.info("Click On Next!");
    }

    public void selectAllType() {
        for (WebElement wallet : wallets) {
            Select sel = new Select(wallet);
            sel.selectByValue(DataFactory.getDefaultWallet().WalletId);
            pageInfo.info("Select Wallet Type");
        }
    }
}
