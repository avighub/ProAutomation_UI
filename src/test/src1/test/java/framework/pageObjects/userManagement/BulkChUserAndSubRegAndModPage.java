package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ravindra.dumpa on 10/25/2017.
 */
public class BulkChUserAndSubRegAndModPage  extends PageInit {

    @FindBy(xpath = "//td[contains(text(),'Download Template')]/a/img")
    private static WebElement DownloadImage;
    @FindBy(id = "remark")
    private static WebElement remark;
    @FindBy(id = "submitBPM")
    private static WebElement SubmitCsv;
    @FindBy(xpath = "//td[contains(text(),'Download Template')]/a/img")
    private static WebElement logFileImage;
    @FindBy(name = "dojo.fromDate")
    private static WebElement fromDate;
    @FindBy(name = "dojo.toDate")
    private static WebElement toDate;
    @FindBy(id = "apprReginit_button_submit")
    private static WebElement clickSubmit;
    @FindBy(id = "appr1init_button_submit")
    private static WebElement clickOnBatchSubmit;
    @FindBy(name = "serviceType")
    private WebElement serviceType;
    @FindBy(name = "fileUpload")
    private WebElement Upload;
    @FindBy(className = "actionMessage")
    private List<WebElement> actionMessages;
    @FindBy(className = "errorMessage")
    private List<WebElement> errorMessages;

    public BulkChUserAndSubRegAndModPage(ExtentTest t1) {
        super(t1);
    }

    public static BulkChUserAndSubRegAndModPage init(ExtentTest t1) {
        return new BulkChUserAndSubRegAndModPage(t1);
    }

    public static void clickOnStartDownload() throws Exception {
        pageInfo.info("Deleting existing file with prefix - BulkChannelRegistrationCore");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkChannelRegistrationCore"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        DownloadImage.click();
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        pageInfo.info("Clicked on Download Image!");
    }

    public static String convertToApproveDateFormat(String date) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
        Date d = sdf.parse(date);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd'T'HH:MM:ssXXX");

        String formatTime = format.format(d);
        return formatTime;

    }

    /**
     * Navigate to Operator User Creation
     *
     * @throws Exception
     */
    public BulkChUserAndSubRegAndModPage navBulkChUserAndSubsciberRegistrationAndModification() throws Exception {
        navigateTo("BULKREG_ALL", "BULKREG_ALL", "Bulk Registration / modification Page");
        return this;
    }

    /**
     * Navigate to Operator User Creation
     *
     * @throws Exception
     */
    public void navBulkRegistrationAndModificationApproval() throws Exception {
        navigateTo("BULKREG_ALL", "BULKREG_BULK_REG_APP", "Bulk Registration Approval Page");
    }

    public BulkChUserAndSubRegAndModPage navBulkRegistrationAndModificationMyBatchesPage() throws Exception {
        navigateTo("BULKREG_ALL", "BULKREG_BULK_MYBATCH_REG", "Bulk Registration My Batch Page");
        return this;
    }

    public BulkChUserAndSubRegAndModPage submitCsv() throws Exception {
        clickOnElement(SubmitCsv, "Submit CSV");
        Thread.sleep(Constants.MAX_WAIT_TIME);
        Utils.captureScreen(pageInfo);
        return this;
    }

    public BulkChUserAndSubRegAndModPage uploadFile(String file) {
        Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
        setText(Upload, file, "Upload");
        return this;
    }

    public void clickOnStartDownloadLogFile() throws Exception {
        pageInfo.info("Deleting existing file with prefix - BulkUserRegistrationLogs");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserRegistrationLogs"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        logFileImage.click();
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        pageInfo.info("Clicked on Download Image!");
    }

    public void checkLogFileDetails(String msisdn) throws Exception {
        Thread.sleep(500);
        try {
            File dir = new File(FilePath.dirFileDownloads);
            String logFile = "";
            String[] types = {"log"};
            Collection<File> collection = FileUtils.listFiles(dir, types, true);
            for (File s : collection) {
                if (s.getName().contains("BulkUserRegistrationLogs")) {
                    logFile = s.getName();
                    break;
                }
            }
            File file = new File(FilePath.dirFileDownloads + "/" + logFile);
            Scanner s = new Scanner(file);
            int lineIndex = 1;
            boolean success = true;
            String[] matcherArray = {"2", "The given MSISDN" + msisdn + "is already registered in the system as different user"};
            //we know that the log will start from 9th line so moving to 9th line
            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (lineIndex == 9) {
                    //reading the 3rd line for log
                    for (int i = 0; i < matcherArray.length; i++) {
                        //replacing all the spaces in the log file and then comparing
                        if (line.replaceAll("\\s+", "").contains(matcherArray[i].replaceAll("\\s", ""))) {
                            pageInfo.pass("found text as " + matcherArray[i]);
                        } else {
                            success = false;
                            pageInfo.fail("couldn't match the details of the log file as desired");
                            break;
                        }
                    }
                    if (success) {
                        pageInfo.pass("log file contents matched as desired");
                    }
                    //now we don't want to read further
                    break;
                }
                lineIndex++;
            }
            s.close();
        } catch (IOException ioex) {
            // handle exception...
        }

    }

    public void SelectChannelUserRegistrationService() throws Exception {
        selectVisibleText(serviceType, "Bulk Channel Registration", "Registration");
        setText(remark, "Bulk Channel Registration", "Remark");
    }

    public BulkChUserAndSubRegAndModPage SelectSubscriberRegistrationService() throws Exception {
        selectVisibleText(serviceType, "Bulk Subscriber Registration", "Registration");
        setText(remark, "Bulk Subscriber Registration", "Remark");
        return this;
    }

    public void selectDateRangeForApproval(String fromdate, String todate) throws Exception {
        fromdate = BulkChUserAndSubRegAndModPage.convertToApproveDateFormat(fromdate);
        todate = BulkChUserAndSubRegAndModPage.convertToApproveDateFormat(todate);
        WebElement fromDate = driver.findElement(By.name("fromDate"));
        WebElement toDate = driver.findElement(By.name("toDate"));
        Utils.setValueUsingJs(fromDate, fromdate);
        pageInfo.info("Set From Date-" + fromdate);
        Utils.setValueUsingJs(toDate, todate);
        pageInfo.info("Set To Date-" + todate);
    }

    public BulkChUserAndSubRegAndModPage ClickOnBatchSubmit() throws IOException {
        Utils.captureScreen(pageInfo);
        clickOnElement(clickOnBatchSubmit, "Submit Batch");
        return this;
    }

    public void ClickOnSubmit() {
        clickOnElement(clickSubmit, "Submit");
        pageInfo.info("Click on Submit");
    }

    public void verifyFieldInCsvFile(String filename, String fieldValue) throws Exception {
        Thread.sleep(500);
        try {
            File dir = new File(FilePath.dirFileDownloads);
            String csvFile = "";
            String[] types = {"csv"};
            Collection<File> collection = FileUtils.listFiles(dir, types, true);
            for (File s : collection) {
                if (s.getName().contains(filename)) {
                    csvFile = s.getName();
                    break;
                }
            }
            File file = new File(FilePath.dirFileDownloads + "/" + csvFile);
            Scanner s = new Scanner(file);
            int lineIndex = 1;
            boolean success = true;

            while (s.hasNextLine()) {
                String line = s.nextLine();
                String[] fieldsArray = line.split(",");


                for (int i = 0; i < fieldsArray.length; i++) {
                    if (fieldsArray[i].equals(fieldValue)) {
                        pageInfo.info(fieldValue + "is verified in Csv File");
                        System.out.println("worked");
                        break;
                    }


                    lineIndex++;
                }
            }
            s.close();
        } catch (IOException ioex) {
            // handle exception...
        }

    }

    public String getBatchId() {
        String batchIdMessage = actionMessages.get(1).getText();
        String batchId = batchIdMessage.substring(batchIdMessage.indexOf(":") + 2, batchIdMessage.length());
        return batchId;
    }

    public BulkChUserAndSubRegAndModPage approveUsingBatchId(String batchId) throws Exception {
        driver.findElement(By.xpath("//*[text()='" + batchId + "']/..//a[text()='Approve']")).click();
        Thread.sleep(10000);
        driver.switchTo().alert().accept();
        Thread.sleep(10000);
        driver.switchTo().alert().sendKeys("good");
        Thread.sleep(10000);
        driver.switchTo().alert().accept();
        pageInfo.info("Approve Using Batch Id - " + batchId);
        return this;
    }

    public BulkChUserAndSubRegAndModPage rejectUsingBatchId(String batchId) {
        driver.findElement(By.xpath("//*[text()='" + batchId + "']/..//a[text()='Reject']")).click();
        driver.switchTo().alert().accept();
        driver.switchTo().alert().sendKeys("bad");
        driver.switchTo().alert().accept();
        return this;
    }

    public BulkChUserAndSubRegAndModPage clickOnSummaryandVerifyFields(String batchId) {
        driver.findElement(By.xpath("//*[text()='" + batchId + "']/..//a[text()='Summary']")).click();
        Set<String> windowhandles = driver.getWindowHandles();
        Iterator<String> itr = windowhandles.iterator();
        String parent = itr.next();
        String child = itr.next();
        driver.switchTo().window(child);
        driver.switchTo().window(parent);

        return this;
    }

    public BulkChUserAndSubRegAndModPage clickOnDetailsandVerifyFields(String batchId) {
        driver.findElement(By.xpath("//*[text()='" + batchId + "']/..//a[text()='Details']")).click();
        Set<String> windowhandles = driver.getWindowHandles();
        Iterator<String> itr = windowhandles.iterator();
        String parent = itr.next();
        String child = itr.next();
        driver.switchTo().window(child);
        driver.switchTo().window(parent);
        return this;
    }

    public BulkChUserAndSubRegAndModPage downloadLogFileUsingBatchId(String batchId) {
        driver.findElement(By.xpath("//*[text()='" + batchId + "']/..//a[text()='Download']")).click();
        return this;
    }

    public BulkChUserAndSubRegAndModPage verifyErrorMessageInLogFile(String expected, String batchId, ExtentTest node, String... variation) throws Exception {

        String code = null;
        Markup m = null;
        boolean flag = true;
        String expectedMsg;
        Thread.sleep(5000);
        File batchIdFile = new File(FilePath.dirFileDownloads + "/" + batchId.replace(".", "_") + ".log");
        Scanner s = new Scanner(batchIdFile);
        if (variation.length > 0) {
            expectedMsg = MessageReader.getDynamicMessage(expected, variation);
        } else {
            expectedMsg = MessageReader.getMessage(expected, null);
        }

        while (s.hasNextLine()) {
            String actual = s.nextLine();
            if (actual.contains(expectedMsg)) {
                code = "Verified Successfully - " + expected + ":\nExpected: " + expectedMsg + "\nActual  : " + actual;
                m = MarkupHelper.createCodeBlock(code);
                node.pass(m);
                flag = false;
                break;
            }
        }
        if (flag) {
            code = "verification failure";
            m = MarkupHelper.createCodeBlock(code);
            node.fail(m);
            node.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            Assert.fail();
        }

        s.close();
        return this;
    }

}

