package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by amarnath.vb on 5/24/2017.
 */
public class modifyPseudoUser_pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(xpath = ".//*[@id='modPseudo_modify_button_back']")
    private WebElement pseudoModifyBack;

    public static modifyPseudoUser_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        modifyPseudoUser_pg1 page = PageFactory.initElements(driver, modifyPseudoUser_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }


}
