package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ViewBarUser_pg1 extends PageInit {

    @FindBy(id = "_uTypeId")
    WebElement userType;
    @FindBy(id = "onlyMsisdn")
    WebElement msisdn;
    @FindBy(id = "viewBlackListAction_viewBlackListUser_button_submit")
    private WebElement submit;
    @FindBy(name = "partyTypeIdWithDate")
    private WebElement selBarTypeWithDate;
    @FindBy(name = "blackListTypeIdWithDate")
    private WebElement selBarReasonWithDate;
    @FindBy(name = "partyTypeId")
    private WebElement selBarType;
    @FindBy(name = "blackListTypeId")
    private WebElement selBarReason;
    @FindBy(name = "inputFromDate")
    private WebElement txtFromDate;
    @FindBy(name = "inputToDate")
    private WebElement txtToDate;

    public ViewBarUser_pg1(ExtentTest t1) {
        super(t1);
    }

    public static ViewBarUser_pg1 init(ExtentTest t1) {
        return new ViewBarUser_pg1(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_VBLK", "Operator User >  View Barred User");
    }

    public void setFromDate(String dateString) {
        setDate(txtFromDate, dateString, "From Date");
    }

    public void setToDate(String dateString) {
        setDate(txtToDate, dateString, "To Date");
    }

    public WebElement getSelBarType() {
        return selBarTypeWithDate;
    }

    public WebElement getSelBarReason() {
        return selBarReasonWithDate;
    }

    public List<String> getSelectOptionBarReasonWithDate() {
        return fl.getOptionText(selBarReasonWithDate);
    }

    public List<String> getSelectOptionBarReason() {
        return fl.getOptionText(selBarReason);
    }

    public List<String> getSelectOptionBarTypeWithDate() {
        return fl.getOptionText(selBarTypeWithDate);
    }

    public List<String> getSelectOptionBarType() {
        return fl.getOptionText(selBarType);
    }

    public void selectBarTypeWithDate(String text) {
        selectVisibleText(selBarTypeWithDate, text, "Bar Type with Date");
    }

    public void selectBarReasonWithDate(String text) {
        selectVisibleText(selBarReasonWithDate, text, "Bar Reason with Date");
    }

    public void selectUserTypeByValue(String text) {
        selectValue(userType, text, "User Type");
    }


    public void setMsisdn(String mobileNo) {
        setText(msisdn, mobileNo, "MSISDN");
    }

    public void clickSubmit() {
        clickOnElement(submit, "Submit");
    }


    public String getUserMSISDN(String msisdn) {
        WebElement text = driver.findElement(By.xpath("//tr/td[contains(text(),'" + msisdn + "')]/ancestor::tr[1]/td[2]"));
        return text.getText();
    }

    public String getUserName(String msisdn) {
        WebElement text = driver.findElement(By.xpath("//tr/td[contains(text(),'" + msisdn + "')]/ancestor::tr[1]/td[5]"));
        return text.getText();
    }


}
