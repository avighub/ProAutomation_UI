package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class AddOperatorUser_pg1 extends PageInit {

    @FindBy(id = "add_addSystemParty_partyTypeId")
    WebElement userTypeDDown;

    @FindBy(id = "add_addSystemParty_loadPartySubmit")
    WebElement submitButton;

    @FindBy(className = "actionMessage")
    WebElement ActionMessage;

    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;


    public AddOperatorUser_pg1(ExtentTest t1) {
        super(t1);
    }

    /**
     * INIT
     *
     * @return
     */
    public static AddOperatorUser_pg1 init(ExtentTest t1) {
        return new AddOperatorUser_pg1(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */
    public void NavigateNA() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_ASU", "Create Network Admin Page");
    }

    /**
     * Navigation Link for the BULK PAYER ADMIN MANAGEMENT
     *
     * @throws Exception
     */
    public void NavigateBPA() throws Exception {
        navigateTo("BPAMGT_ALL", "BPAMGT_EPTY_ASU", "Create Bank Admin Page");
    }

    public void selectUserTypeByValue(String text) {
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        selectValue(userTypeDDown, text, "userType");
    }

    public boolean checkUserType(String userType) throws IOException {
        boolean found = false;
        Select se = new Select(userTypeDDown);
        List<WebElement> userTypes = se.getOptions();
        for (WebElement option : userTypes) {
            String value = option.getText();
            pageInfo.info("Existing value is : " + value);
            if (option.getAttribute("value").contains(userType)) {
                found = false;
            }
        }
        if (found) {
            pageInfo.fail(userType + " Does Exist's");
        } else {
            userTypeDDown.click();
            Utils.captureScreen(pageInfo);
            pageInfo.pass(userType + " Does not Exist");
        }
        return found;
    }

    public void clickSubmitButton() {
        clickOnElement(submitButton, "Submit Button");
    }

    public void checkSubmitButtonIsavailable() {
        Utils.checkElementPresent("add_addSystemParty_loadPartySubmit", Constants.FIND_ELEMENT_BY_ID);
    }

    public String getActionMessage() {
        try {
            return ActionMessage.getText();
        } catch (NoSuchElementException e) {
            return ErrorMessage.getText();
        }
    }

    /**
     * get error message
     *
     * @return TODO - remove exception block
     */
    public String getErrorMessage() {
        try {
            return ErrorMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}
