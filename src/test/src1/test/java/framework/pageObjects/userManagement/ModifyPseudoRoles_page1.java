package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by gurudatta.praharaj on 7/2/2018.
 */
public class ModifyPseudoRoles_page1 extends PageInit {

    @FindBy(id = "partyMsisdnId")
    private WebElement setMsisdn;
    @FindBy(id = "mfsProvidersList")
    private WebElement MFSProvider;
    @FindBy(id = "wallet")
    private WebElement wallet;
    @FindBy(id = "selectForm_button_add")
    private WebElement addBtn;
    @FindBy(id = "selectForm_button_confirm")
    private WebElement confirmButton;
    @FindBy(id = "selectForm_pseudoGroupRoleCode")
    private WebElement roleCode;
    @FindBy(id = "selectForm_pseudoGroupRoleName")
    private WebElement roleName;

    public ModifyPseudoRoles_page1(ExtentTest test) {
        super(test);
    }

    public static ModifyPseudoRoles_page1 init(ExtentTest test) {
        return new ModifyPseudoRoles_page1(test);
    }

    public ModifyPseudoRoles_page1 navToModifyPseudoRoles() throws Exception {
        navigateTo("PSEUDO_ALL", "PSEUDO_PSEUDO_MODIFY_ROLES", "Modify Pseudo Roles");
        return this;
    }

    public ModifyPseudoRoles_page1 setMsisdn(String msisdn) {
        setText(setMsisdn, msisdn, "Enter Pseudo MSISDN");
        return this;
    }

    public ModifyPseudoRoles_page1 setMFSProvider(String providerID) {
        clickOnElement(MFSProvider, "click On MFS Provider Dropdown");
        selectValue(MFSProvider, providerID, "Select MFS Provider");
        return this;
    }

    public ModifyPseudoRoles_page1 selectWallet(String walletType) {
        selectValue(wallet, walletType, "Select Wallet Type");
        return this;
    }

    public ModifyPseudoRoles_page1 clickAddButton() {
        clickOnElement(addBtn, "Add Button");
        return this;
    }

    public ModifyPseudoRoles_page1 clickConfirm() {
        clickOnElement(confirmButton, "Confirm Button");
        return this;
    }

    public ModifyPseudoRoles_page1 setRoleCode(String code) {
        setText(roleCode, code, "Set Pseudo Role Code");
        return this;
    }

    public ModifyPseudoRoles_page1 setRoleName(String name) {
        setText(roleName, name, "Set Pseudo Role Name");
        return this;
    }
}
