package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by gurudatta.praharaj on 7/4/2018.
 */
public class DeleteApprovalPseudoUser_page1 extends PageInit {

    @FindBy(id = "delPseudo_deleteApproveConfirm_button_confirm")
    private WebElement confirmDelete;

    public DeleteApprovalPseudoUser_page1(ExtentTest test) {
        super(test);
    }

    public static DeleteApprovalPseudoUser_page1 init(ExtentTest test) {
        return new DeleteApprovalPseudoUser_page1(test);
    }

    public DeleteApprovalPseudoUser_page1 navToDeletePsuedoUserApprove() throws Exception {
        navigateTo("PSEUDO2_ALL", "PSEUDO2_PSEUDO_DELETE2", "Delete Approve Pseudo User");
        return this;
    }

    public DeleteApprovalPseudoUser_page1 clickApprove(String msisdn) {
        driver.findElement(By.xpath("//tr[td[contains(text(),'" + msisdn + "')]]/td/a[contains(@href,'approve')]")).click();
        return this;
    }

    public DeleteApprovalPseudoUser_page1 clickReject(String msisdn) {
        driver.findElement(By.xpath("//tr[td[contains(text(),'" + msisdn + "')]]/td/a[contains(@href,'reject')]")).click();
        return this;
    }

    public void confirmDelete() throws Exception {
        clickOnElement(confirmDelete, "Confirm Delete Button");
        Thread.sleep(Constants.WAIT_TIME);
    }
}
