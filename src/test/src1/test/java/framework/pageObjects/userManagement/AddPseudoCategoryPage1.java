package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by amarnath.vb on 5/23/2017.
 */
public class AddPseudoCategoryPage1 extends PageInit{



    /************************************************************
     * Pseudo User Category Creation PAGE 1
     *************************************************************/
    @FindBy(id = "pseudoCat_input_button_add")
    private WebElement pseudoAddInitiate;

    @FindBy(id = "selectForm_button_add")
    private WebElement AddPseudoCategory;

    @FindBy(id = "parentCategoryList")
    private WebElement ParentCategoryList;

    @FindBy(id = "selectForm_categoryCode")
    private WebElement NewCategoryCode;

    @FindBy(id = "selectForm_categoryName")
    private WebElement NewCategoryName;
    @FindBy(id = "selectForm_button_back")
    private WebElement Back;
    /**
     * Navigate to Pseudo User Category Creation
     *
     * @throws Exception
     **/
    @FindBy(id = "selectForm_button_confirm")
    private WebElement AddPseudoCategoryConfirm;
    @FindBy(id = "selectForm_button_back")
    private WebElement AddPseudoCategoryBack;
    @FindBy(className = "actionMessage")
    private WebElement ActionMessage;

    public AddPseudoCategoryPage1(ExtentTest t1) {
        super(t1);
    }



    /**
     * Navigate to Pseudo User Category Creation
     *
     * @throws Exception
     **/
    public void navPseudoUserCategoryCreation() throws Exception {
        fl.leftNavigation("PSEUDO_ALL", "PSEUDO_PSEUDO_CAT_MGMT");
        pageInfo.info("Navigate to Pseudo User Category Management Page!");
    }

    public void clickAddInitiate() {
        pseudoAddInitiate.click();
        pageInfo.info("Click on Category Add:");
    }

    public void setNewCategoryCode(String code) {
        NewCategoryCode.sendKeys(code);
        pageInfo.info("Set Pseudo Category Code " + code);
    }

    /************************************************************
     * Pseudo User Category Creation PAGE 2
     *************************************************************/

    public void setNewCategoryName(String name) {
        NewCategoryName.sendKeys(name);
        pageInfo.info("Set Pseudo Category Name" + name);
    }

    public void setParentCategoryList(String category) {
        Select sel = new Select(ParentCategoryList);
        sel.selectByVisibleText(category);
    }

    public void clickAdd() {
        AddPseudoCategory.click();
        pageInfo.info("Click on Button next!");

    }

    public void clickPseudoAddConfirm() {
        AddPseudoCategoryConfirm.click();
        pageInfo.info("Click on Button next!");

    }

    public String getActionMessage() {
        return ActionMessage.getText();
    }

    /**
     * Added method to check if Pseudo Category is already present in the System
     * @param psCatCode
     * @return
     */
    public boolean checkPseudoCategoryExist(String psCatCode){
        if(driver.findElement(By.xpath("//tr/td[contains(text(),'"+psCatCode+"')][1]")).isDisplayed()) {
            return true;
        }else {
            return false;
        }
    }


}
