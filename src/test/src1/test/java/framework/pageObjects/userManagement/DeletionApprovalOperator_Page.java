package framework.pageObjects.userManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class DeletionApprovalOperator_Page extends PageInit {

    @FindBy(id = "add_addSystemParty_partyTypeId")
    WebElement userType;
    @FindBy(name = "msisdnWeb")
    WebElement loginID;
    @FindBy(name = "button.submit")
    WebElement submitButton;
    @FindBy(id = "loadUserDelete_loadSystemPartyForDelete_submit")
    WebElement finalSubmitButton;
    @FindBy(id = "loadUserDelete_loadSystemPartyForDelete_approve")
    WebElement approveButton;
    @FindBy(id = "loadUserDelete_loadSystemPartyForDelete_reject")
    WebElement rejectButton;

    public DeletionApprovalOperator_Page(ExtentTest t1) {
        super(t1);
    }

    /**
     * INIT
     *
     * @return
     */
    public static DeletionApprovalOperator_Page init(ExtentTest t1) {
        return new DeletionApprovalOperator_Page(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */
    public void navigateToNALink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_ASUD", "Approve Deletion Operator User");

    }

    public void navigateToNALinkBPA() throws Exception {
        fl.leftNavigation("BPAMGT_ALL", "BPAMGT_EPTY_ASUD");
        pageInfo.info("Navigate to Approve Deletion Operator User Link");
    }


    public void selectUserTypeByValue(String text) {
        selectValue(userType, text, "User Type");
        clickOnElement(driver.findElement(By.id("add_addSystemParty_loadPartySubmit")), "Submit");

    }


    public void enterLoginID(String login) {
        setText(loginID, login, "LoginID");
    }

    public void selectUserFromList(String msisdn) {
        WebElement radio = driver.findElement(By.xpath("//tr/td[contains(text(),'" + msisdn + "')]/ancestor::tr[1]/td/input[@type='radio']"));
        radio.click();
        pageInfo.info("Select user having MSISDN: " + msisdn);
    }

    public void clickSubmitButton() {
        clickOnElement(submitButton, "Submit Button");
    }

    public void clickFinalSubmitButton() {
        clickOnElement(finalSubmitButton, "finalSubmitButton");
    }


    public void clickApproveButton() {
        clickOnElement(approveButton, "Approve Button");
    }

    public void clickRejectButton() {
        clickOnElement(rejectButton, "Reject Button");
    }


}
