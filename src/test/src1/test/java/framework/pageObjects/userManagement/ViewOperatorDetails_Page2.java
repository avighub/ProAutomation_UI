package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by vandana.rattan on 12/11/2018.
 */
public class ViewOperatorDetails_Page2 extends PageInit {

    @FindBy(id = "viewopt_input_partyNamePrefixId")
    WebElement UserNamePrefix;
    @FindBy(id = "viewopt_input_statusItemId")
    WebElement StatusType;
    @FindBy(id = "viewopt_input_userName")
    WebElement FirstName;
    @FindBy(id = "viewopt_input_lastName")
    WebElement LastName;
    @FindBy(id = "viewopt_input_msisdnUser")
    WebElement Msisdn;
    @FindBy(id = "viewopt_input_divisionId")
    WebElement Division;
    @FindBy(id = "viewopt_input_departmentId")
    WebElement Department;
    @FindBy(id = "viewopt_input_msisdnWeb")
    WebElement WebLogin;
    @FindBy(id = "viewopt_input_localeName")
    WebElement PrefLanguage;
    @FindBy(id = "viewopt_input_contactNo")
    WebElement ContactNo;


    public ViewOperatorDetails_Page2(ExtentTest t1) {
        super(t1);
    }

    public static ViewOperatorDetails_Page2 init(ExtentTest t1) {
        return new ViewOperatorDetails_Page2(t1);
    }

    public boolean UserNamePrefixDisabled() {
        String status = UserNamePrefix.getAttribute("disabled");
        if (status.equals("true"))
            return true;
        else
            return false;
    }

    public boolean statusTypeDisabled() {
        String status = StatusType.getAttribute("disabled");
        if (status.equals("true"))
            return true;
        else
            return false;
    }

    public boolean VerifyfirstNameTag() {
        String status = FirstName.getTagName();
        if (status.equals("label"))
            return true;
        else
            return false;

    }

    public boolean VerifyLastNameTag() {
        String status = LastName.getTagName();
        if (status.equals("label"))
            return true;
        else
            return false;

    }

    public boolean VerifyMsisdnTag() {
        String status = Msisdn.getTagName();
        if (status.equals("label"))
            return true;
        else
            return false;

    }


    public boolean VerifyDivisionTag() {
        String status = Division.getTagName();
        if (status.equals("label"))
            return true;
        else
            return false;

    }


    public boolean VerifyDepartmentTag() {
        String status = Department.getTagName();
        if (status.equals("label"))
            return true;
        else
            return false;

    }

    public boolean VerifyWebLoginTag() {
        String status = WebLogin.getTagName();
        if (status.equals("label"))
            return true;
        else
            return false;

    }

    public boolean VerifyPrefLanguageTag() {
        String status = PrefLanguage.getTagName();
        if (status.equals("label"))
            return true;
        else
            return false;

    }

    public boolean VerifyContactNoTag() {
        String status = ContactNo.getTagName();
        if (status.equals("label"))
            return true;
        else
            return false;

    }

}
