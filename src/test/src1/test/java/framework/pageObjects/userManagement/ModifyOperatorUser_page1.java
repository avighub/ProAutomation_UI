package framework.pageObjects.userManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class ModifyOperatorUser_page1 extends PageInit {


    @FindBy(id = "view_loadSystemParty_partyTypeId")
    WebElement userTypeDDown;
    @FindBy(id = "view_loadSystemParty_userName")
    WebElement firstName;
    @FindBy(id = "view_loadSystemParty_button_submit")
    WebElement submitButton;
    @FindBy(className = "actionMessage")
    WebElement ActionMessage;
    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;
    @FindBy(xpath = "//*[@id=\"view_loadSystemParty\"]/table/tbody/tr[2]/td[3]/a/img")
    WebElement searchImage;
    @FindBy(id = "view_setSearchedUser_userId")
    WebElement searchSelect;

    public ModifyOperatorUser_page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * INIT
     *
     * @return
     */
    public static ModifyOperatorUser_page1 init(ExtentTest t1) {

        return new ModifyOperatorUser_page1(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */
    public void NavigateNA() throws Exception {
        fl.leftNavigation("PARTY_ALL", "PARTY_PTY_MSU");

    }


    /**
     * Navigation Link for the BULK PAYER ADMIN MANAGEMENT
     *
     * @throws Exception
     */
    public void NavigateBPA() throws Exception {
        fl.leftNavigation("BPAMGT_ALL", "BPAMGT_EPTY_MSU");

    }

    public void selectUserTypeByValue(String text) {
        selectValue(userTypeDDown, text, "User type");

    }

    public boolean checkUserType(String userType) throws IOException {
        boolean found = false;
        Select se = new Select(userTypeDDown);
        List<WebElement> userTypes = se.getOptions();
        for (WebElement option : userTypes) {
            String value = option.getText();
            pageInfo.info("Existing value is : " + value);
            if (option.getAttribute("value").contains(userType)) {
                found = false;
            }
        }
        if (found) {
            pageInfo.fail(userType + " Does Exist's");
        } else {
            userTypeDDown.click();
            Utils.captureScreen(pageInfo);
            pageInfo.pass(userType + " Does not Exist");
        }
        return found;
    }

    public void clickSubmitButton() {
        clickOnElement(submitButton, "submitButton");
    }

    public String getActionMessage() {
        try {
            return ActionMessage.getText();
        } catch (NoSuchElementException e) {
            return ErrorMessage.getText();
        }
    }

    public void firstName_SetText(String text) {
        setText(firstName, text, "First name");
    }

    public void searchFirstName(String text) throws Exception {
        searchImage.click();
        Thread.sleep(2000);
        String mainWindow = driver.getWindowHandle();
        Set<String> winHandle = driver.getWindowHandles();
        Iterator<String> winIterator = winHandle.iterator();
        //loop through windows

        while (winIterator.hasNext()) {
            String handle = winIterator.next();
            if (!mainWindow.equalsIgnoreCase(handle)) {
                driver.switchTo().window(handle);
                Select sel = new Select(searchSelect);
                sel.selectByIndex(1); //0 is "select"
            }
        }

    }


    /**
     * get error message
     *
     * @return
     */
    public String getErrorMessage() {
        try {
            return ErrorMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }


    public ModifyOperatorUser_page1 fillFirstPageDetails(OperatorUser optUsr) throws Exception {
        if (!optUsr.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
            NavigateNA();
        } else {
            NavigateBPA();
        }

        selectUserTypeByValue(optUsr.CategoryCode);
        firstName_SetText(optUsr.FirstName);
        clickSubmitButton();

        return this;
    }

}
