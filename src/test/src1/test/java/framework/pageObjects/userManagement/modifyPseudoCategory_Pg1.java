package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class modifyPseudoCategory_Pg1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "pseudoCat_input_button_update")
    private WebElement pseudoCategoryUpdate;

    /************************************************************
     * Pseudo User Category Modification PAGE 1
     *************************************************************/
    @FindBy(id = "pseudoCat_input_button_delete")
    private WebElement pseudoCategoryDelete;
    @FindBy(id = "selectForm_button_modify")
    private WebElement modifyPseudoCategory;
    @FindBy(id = "parentCategoryList")
    private WebElement ParentCategoryList;
    @FindBy(id = "selectForm_categoryCode")
    private WebElement NewCategoryCode;
    @FindBy(id = "selectForm_categoryName")
    private WebElement NewCategoryName;
    @FindBy(id = "selectForm_button_back")
    private WebElement Back;
    /**
     * Navigate to Pseudo User Category Creation
     *
     * @throws Exception
     **/
    @FindBy(id = "selectForm_button_confirm")
    private WebElement modifyPseudoCategoryConfirm;
    @FindBy(id = "selectForm_button_back")
    private WebElement AddPseudoCategoryBack;
    @FindBy(className = "actionMessage")
    private WebElement ActionMessage;

    public static modifyPseudoCategory_Pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        modifyPseudoCategory_Pg1 page = PageFactory.initElements(driver, modifyPseudoCategory_Pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    /**
     * Navigate to Pseudo User Category Modification
     *
     * @throws Exception
     **/
    public void navPseudoUserCategoryManagement() throws Exception {
        fl.leftNavigation("PSEUDO_ALL", "PSEUDO_PSEUDO_CAT_MGMT");
        pageInfo.info("Navigate to Pseudo User Category Management Page!");
    }

    public void clickUpdate() {
        pseudoCategoryUpdate.click();
        pageInfo.info("Click on Update category Button");
    }

    public void clickDelete() {
        pseudoCategoryDelete.click();
        pageInfo.info("Click on Delete category Button");
    }

    public void setNewCategoryCode(String code) {
        NewCategoryCode.clear();
        NewCategoryCode.sendKeys(code);
        pageInfo.info("Set Pseudo Category Code " + code);
    }

    /************************************************************
     * Pseudo User Category Creation PAGE 2
     *************************************************************/

    public void setNewCategoryName(String name) {
        NewCategoryName.clear();
        NewCategoryName.sendKeys(name);
        pageInfo.info("Set Pseudo Category Name" + name);
    }

    public void setParentCategoryList(String category) {
        Select sel = new Select(ParentCategoryList);
        sel.selectByVisibleText(category);
    }

    public void clickModify() {
        modifyPseudoCategory.click();
        pageInfo.info("Click on Button next!");

    }

    public void clickPseudomodifyConfirm() {
        modifyPseudoCategoryConfirm.click();
        pageInfo.info("Click on Button next!");

    }

    public void selectCategoryForUpdation(String categoryName) throws Exception {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + categoryName + "')]/../td/input[@type='radio']")).click();
        pageInfo.info("Select category Name" + categoryName);
    }

    public String getActionMessage() {
        return ActionMessage.getText();
    }


}
