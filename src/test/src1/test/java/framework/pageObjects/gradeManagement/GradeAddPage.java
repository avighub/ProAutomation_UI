/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name:
 *  Date: 18-Nov-2017
 *  Purpose: Grade Management -> Add Grade Page Objects
 */
package framework.pageObjects.gradeManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class GradeAddPage extends PageInit {

    @FindBy(id = "grades_addGrades_domainCode")
    private WebElement domainCodeDdown;



    /*
     ****************************************************
     *             P A G E   O B J E C T S
     ***************************************************/
    @FindBy(id = "grades_addGrades_categoryCode")
    private WebElement categoryCodeDdown;
    @FindBy(id = "grades_addGrades_submit")
    private WebElement add1;
    @FindBy(id = "grades_addGrades_gradeList_0__gradeCode")
    private WebElement gradeCodeTBox;
    @FindBy(id = "grades_addGrades_gradeList_0__gradeName")
    private WebElement gradeNameTBox;
    @FindBy(name = "action:grades_addMoreGrades")
    private WebElement addMoreGradeButton;
    @FindBy(name = "action:grades_displayGrades")
    private WebElement backButton;
    @FindBy(name = "action:grades_addGrades")
    private WebElement confirmBackButton;
    @FindBy(name = "action:grades_deleteGradeRow")
    private WebElement deleteGradeRowButton;
    @FindBy(name = "action:grades_saveGrades")
    private WebElement saveButton;
    @FindBy(id = "grades_saveGrades_submit")
    private WebElement confirmButton;
    @FindBy(name = "gradeList[1].gradeCode")
    private WebElement extraGradeRow;

    public GradeAddPage(ExtentTest t1) {
        super(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("CHGRADES_ALL", "CHGRADES_CHGRADES_TR", "Add Grade");
    }


    public void selectDomain(String val) {
        selectVisibleText(domainCodeDdown, val, "Domain");
    }

    public void selectDomainByValue(String domainCode) {
        selectValue(domainCodeDdown, domainCode, "Category Name");
    }

    public void selectCategoryByValue(String catCode) {
        selectValue(categoryCodeDdown, catCode, "Category Name");
    }

    public void clickAddButton() {
        clickOnElement(add1, "Add Button");
    }

    public void enterGradeCode(String gradeCode) {
        setText(gradeCodeTBox, gradeCode, "Grade Code Text Box");
    }

    public void enterGradeName(String gradeName) {
        setText(gradeNameTBox, gradeName, "Grade Name Text Box");
    }


    public List<String> getAllOptionsFromDomainCodeDdown(){
        return fl.getOptionValues(domainCodeDdown);
    }
    /**
     * Click On Save Button
     */
    public void clickSaveButton() {
        clickOnElement(saveButton, "Save Button");
    }

    /**
     * click Confirm Button
     */
    public void clickConfirmButton() {
        clickOnElement(confirmButton, "Confirm Button");
    }

    public String findRowNoOfGrade(String value) {
        WebElement table = driver.findElement(By.xpath("//*[@id='grades_addGrades']/table"));
        WebElement tbody = table.findElement(By.tagName("tbody"));
        List<WebElement> rows = tbody.findElements(By.tagName("tr"));

        String rowNo = "";
        for (int i = 0; i < rows.size(); i++) {
            WebElement row = tbody.findElement(By.xpath("//*[@id='grades_addGrades']/table/tbody/tr[" + (i + 1) + "]"));

            if (row.getText().trim().contains(value)) {
                rowNo = Integer.toString(i);
                break;
            }
        }

        return rowNo;
    }


    /**
     * Click On Save Button
     */
    public void clickAddMoreGradeButton() {
        clickOnElement(addMoreGradeButton, "Add More Grade Button");
    }

    public void clickBackButton() {
        clickOnElement(backButton, "Back Button");
    }

    public void clickConfirmBackButton() {
        clickOnElement(confirmBackButton, "Confirm Page Back Button");
    }

    public boolean isSaveButtonVisible() {
        return fl.elementIsDisplayed(saveButton);
    }

    public boolean isAddButtonVisible() {
        return add1.isDisplayed();
    }

    public boolean isExtraGradeRowVisible() {
        return extraGradeRow.isDisplayed();
    }

    public boolean isConfirmButtonVisible() {
        return confirmButton.isDisplayed();
    }
}
