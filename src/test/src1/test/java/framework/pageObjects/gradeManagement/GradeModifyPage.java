package framework.pageObjects.gradeManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GradeModifyPage extends PageInit {

    /*
     ****************************************************************
     *      P A G E   O B J E C T S
     ****************************************************************
     */
    @FindBy(id = "modifyGrades_modify_submit")
    private WebElement modifyButton;
    @FindBy(name = "action:modifyGrades_modifyConfirm")
    private WebElement confirmButton;
    @FindBy(name = "action:modifyGrades_displayForModify")
    private WebElement backButton;

    public GradeModifyPage(ExtentTest t1) {
        super(t1);
    }

    /**
     * To click on Modify Grade Link
     *
     * @throws NoSuchElementException Throw NoSuchElementException when Link not found
     */
    public void navigateToLink() throws Exception {
        navigateTo("CHGRADES_ALL", "CHGRADES_MODITYGRADES_TR", "Modify Grade");
    }

    /**
     * Select Grade to Modify
     *
     * @param gradeCode Grade Code to Modify
     */
    public void selectGrade(String gradeCode) {
        driver.findElement(By.xpath("//*[@value='" + gradeCode + "' ][ @type='checkbox']")).click();
        pageInfo.info("Selecting the user having gradeCodeTbox: " + gradeCode);
    }


    /**
     * To modify the Grade Name
     *
     * @param code        Code of the Grade to Modify
     * @param modifyValue Value to Modify
     */
    public void modifyGradeName(String code, String modifyValue) {
        //String xpath = "//*[contains(text(),'" + code + "' )]/following-sibling::td/input";
        String xpath = "//td[text()='" + code + "']/following-sibling::td/input";
        WebElement option = driver.findElement(By.xpath(xpath));
        setText(option, modifyValue, "Modify Grade Name");
    }

    /**
     * To Click on Modify Button
     */
    public void modifyButtonClick() {
        clickOnElement(modifyButton, "Modify Button");
    }

    public void confirmButtonClick() {
        clickOnElement(confirmButton, "Confirm Button");
    }

    public boolean checkGradeCodeEditable() {
        String xpath = "//table[@class='wwFormTableC']/tbody/tr[2]/td[4]/input";
        List<WebElement> option = driver.findElements(By.xpath(xpath));

        if (option.size() > 0) {
            pageInfo.info("Grade Code is Editable. User can modify Grade Code");
            return false;
        } else {
            pageInfo.info("Grade Code is not editable. User can only modify Grade Name.");
            return true;
        }
    }

    public boolean isModifyButtonDisplayed() {
        return modifyButton.isDisplayed();
    }

    public boolean isCheckBoxDisplayed() {
        WebElement element = driver.findElement(By.xpath("//input[@type='checkbox'][@name='check']"));
        return fl.elementIsDisplayed(element);
    }

    public boolean isBackButtonDisplayed() {
        return fl.elementIsDisplayed(backButton);
    }

    public boolean isConfirmButtonDisplayed() {
        return fl.elementIsDisplayed(confirmButton);
    }

}
