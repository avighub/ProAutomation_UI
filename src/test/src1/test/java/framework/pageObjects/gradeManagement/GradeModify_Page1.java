package framework.pageObjects.gradeManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GradeModify_Page1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    /***************************************************************************
     * Page Objects
     ***************************************************************************/
    @FindBy(id = "modifyGrades_modify_submit")
    public WebElement modify;

    public static GradeModify_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        GradeModify_Page1 page = PageFactory.initElements(driver, GradeModify_Page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void navigateToLink() {

        try {
            fl.leftNavigation("CHGRADES_ALL", "CHGRADES_MODITYGRADES_TR");
            pageInfo.info("Navigate to Modify Grade ..");
        } catch (Exception e) {
            pageInfo.error("Navigate to Modify Grade Failed..");
        }


    }

    public void selectuser(String val) {
        WebElement option = driver.findElement(By.xpath("//*[@value='" + val + "' ][ @type='checkbox']"));
        option.click();
        pageInfo.info("Selecting the user having code: " + val);

    }


    public void modifydetail(String code, String modifyvalue) {
        String xpath = "//*[contains(text(),'" + code + "' )]/following-sibling::td/input";
        WebElement option = driver.findElement(By.xpath(xpath));
        option.clear();
        option.sendKeys(modifyvalue);
        pageInfo.info("Modifying the name of user having code :" + code + " with value " + modifyvalue);

    }

    public void modifyButtonClick() {
        modify.click();
        pageInfo.info("Click on Modify Button..");
    }

}
