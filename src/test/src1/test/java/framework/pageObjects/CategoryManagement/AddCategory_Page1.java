package framework.pageObjects.CategoryManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.dbManagement.MobiquityGUIQueries;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class AddCategory_Page1 extends PageInit {


    //Page Objects
    @FindBy(id = "newCategory")
    private WebElement categoryName;
    @FindBy(id = "newCategoryCode")
    private WebElement categoryCode;
    @FindBy(id = "domainCode")
    private WebElement domainCodeDDown;
    @FindBy(id = "categoryCode")
    private WebElement parentCategoryDDown;
    @FindBy(id = "addcat_button_submit")
    private WebElement submitButton;
    @FindBy(id = "addcat_conf")
    private WebElement confirmButton;
    @FindBy(id = "addcat_back1")
    private WebElement backButton;
    @FindBy(name = "button.reset")
    private WebElement resetButton;

    public AddCategory_Page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Method to navigate Category Management Page.
     *
     * @throws Exception
     */
    public void navigateCategoryManagementPage() throws Exception {
        fl.leftNavigation("CATADD_ALL", "CATADD_ADD_CAT");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Add Category Page");
    }

    /**
     * Method to Enter category Code
     *
     * @param cat
     * @return
     */
    public void enterCategoryCode(String cat) {
        setText(categoryCode, cat, "Category Code");
    }

    /**
     * Method to Enter Category Name
     *
     * @param cat
     * @return
     */
    public void enterCategoryName(String cat) {
        setText(categoryName, cat, "Category Name");
    }

    /**
     * Method to Select Domain By Visible text
     *
     * @param val
     * @return
     */
    public void selectDomain(String val) {
        selectVisibleText(domainCodeDDown, val, "Domain Name");
    }


    /**
     * Method to select Parent Category By Visible text
     *
     * @param val
     * @return
     */
    public void selectParentCategory(String val) {
        selectVisibleText(parentCategoryDDown, val, "Parent Category");
    }

    public void selectParentCategory() {
        String selected = new Select(parentCategoryDDown).getFirstSelectedOption().getText();
        if (selected.trim().equalsIgnoreCase("Select")) {
            pageInfo.info("Parent Category is not selected by itself");
            selectIndex(parentCategoryDDown, 1, "Parent Category");
        }
    }

    /**
     * Method to click On Submit Button
     *
     * @return
     */
    public void clickOnSubmitButton() {
        clickOnElement(submitButton, "Submit Button");
    }

    /**
     * Method to Click On Confirm button
     *
     * @return
     */
    public void clickOnConfirmButton() {
        clickOnElement(confirmButton, "Confirm Button");
    }


    /**
     * Method to click on Back button.
     */
    public void clickBack() {
        clickOnElement(backButton, "Back button");
    }

    /**
     * Method to Check Back Button Functionality.
     */
    public void checkNavigateBackOrNot() {
        if (fl.elementIsDisplayed(resetButton))
            pageInfo.info("Successfully Navigated to Back Page.");
        else
            pageInfo.info("Failed To Navigate Back Page.");
    }

    /**
     * Method to select Domain By Value.
     *
     * @param domainCode
     */
    public void selectDomainByValue(String domainCode) {
        selectValue(domainCodeDDown, domainCode.toUpperCase(), "Domain name");
    }

    /**
     * Method to Select Parent Category By value.
     */
    public void selectParentCategoryByValue(String domainCode) {
        String parentCategory = MobiquityGUIQueries.fetchParentCategoryCode(domainCode.toUpperCase());
        if (parentCategory != null) {
            selectValue(parentCategoryDDown, parentCategory, "Parent category");
        } else {
            selectIndex(parentCategoryDDown, 1, "Parent Category");
        }
        pageInfo.info("Parent Category Selected.");
    }
}
