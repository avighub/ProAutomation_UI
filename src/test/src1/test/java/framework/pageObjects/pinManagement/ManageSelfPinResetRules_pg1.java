package framework.pageObjects.pinManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ManageSelfPinResetRules_pg1 extends PageInit {

    @FindBy(xpath = "//button[.='Add New Rule']")
    private WebElement addNewRule;
    @FindBy(xpath = "(//td[text()='Domain']/..//td/div/button)[1]")
    private WebElement domain;
    @FindBy(xpath = "(//td/div/button)[2]")
    private WebElement category;
    @FindBy(id = "max_amount_allowed")
    private WebElement maxAccBalAllowed;
    @FindBy(xpath = "//button[text()='Submit']")
    private WebElement submit;

    public ManageSelfPinResetRules_pg1(ExtentTest t1) {
        super(t1);
    }

    public static ManageSelfPinResetRules_pg1 init(ExtentTest t1) {
        return new ManageSelfPinResetRules_pg1(t1);
    }

    public void navManageSelfRestPinRules() throws Exception {
        navigateTo("PINMANAGE_ALL", "PINMANAGE_RESETRULE", "Manage Self rest PIN rules");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
    }

    public void clickOnEdit() throws Exception {
        WebElement edit = driver.findElement(By.xpath("//span[@class='glyphicon glyphicon-edit']"));
        clickOnElement(edit, "Edit");
        Thread.sleep(2000);
    }//button[text() = 'Add New Rule']

    public void clickAddNewRule() throws Exception {
        clickOnElement(addNewRule, "Add New Rule Button");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
    }

    public void selectDomain(String categoryCode) throws Exception {
        clickOnElement(domain, "Domin drop down");
        driver.findElement(By.xpath("(//input[@type='checkbox' and @value='" + categoryCode + "'])[1]")).click();
        pageInfo.info("Selected Domain: " + categoryCode);
    }

    public void selectCategory(String categoryCode) throws Exception {
        clickOnElement(category, "Category drop down");
        driver.findElement(By.xpath("(//input[@type='checkbox' and @value='" + categoryCode + "'])[1]")).click();
        pageInfo.info("Selected Category: " + categoryCode);
    }

    public void selectQuestions(String question) throws Exception {
        driver.findElement(By.xpath("//span[contains(text(),'" + question + "')]/..//input[@type='checkbox']")).click();
        pageInfo.info("Selected question is: " + question);
    }

    public void setMaxAccBalance(String amt) throws Exception {
        setText(maxAccBalAllowed, amt, "Maximum Account Balance Allowed :");
        Thread.sleep(2000);
    }

    public void clickSubmit() throws Exception {
        clickOnElement(submit, "Submit Button");
    }

    public boolean isMaxAccountBalanceTextBoxVisible(){
        return fl.elementIsDisplayed(maxAccBalAllowed);
    }

}
