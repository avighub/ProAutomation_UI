package framework.pageObjects.pinManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResetPinPage extends PageInit {

    @FindBy(id = "skip_for_later")
    public WebElement SkipForLater;

    @FindBy(id = "question")
    private WebElement question;

    @FindBy(id = "ans")
    private WebElement answer;

    @FindBy(xpath = "//button[@id='skip_for_later']")
    private WebElement nextButton;

    @FindBy(id = "newPin")
    private WebElement newPin;

    @FindBy(id = "confirmNewPin")
    private WebElement confirmPin;

    @FindBy(id = "pinType")
    private WebElement pinType;

    @FindBy(xpath = "//button[@id='formSubmit']")
    private WebElement submit;

    public ResetPinPage(ExtentTest t1) {
        super(t1);
    }


    public void navRestPin() throws Exception {
        navigateTo("PINMANAGE_ALL", "PINMANAGE_PINRESET", "Reset PIN");
    }

    public void setAnswer(String ans) throws Exception {
        setText(answer, ans, "Answer");
    }

    public String getQuestion() throws Exception {
        return question.getText();
    }

    public void clickNext() throws Exception {
        clickOnElement(nextButton, "Next_Button");
    }

    public void setNewPin(String pin) throws Exception {
        setText(newPin, pin, "New PIN");
    }

    public void setConfirmPin(String pin) throws Exception {
        setText(confirmPin, pin, "Confirm New PIN");
    }

    public void selectPinType(String pType) throws Exception {
        selectVisibleText(pinType, pType, "Pin Type to Reset");
    }

    public void clickSubmit() throws Exception {
        clickOnElement(submit, "Submit Button");
    }
}
