package framework.pageObjects.pinManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.NumberConstants;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class AddQnToMasterList extends PageInit {

    @FindBy(xpath = "//form[@name='fileUploadForm']/table/tbody/tr[1]/td/a/img")
    private WebElement downloadImg;

    @FindBy(name = "file")
    private WebElement chooseFileBtn;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submit;

    @FindBy(xpath = "//form[@name='fileUploadForm']/table/tbody/tr[3]/td/a/img")
    private WebElement logImg;


    public AddQnToMasterList(ExtentTest t1) {
        super(t1);
    }

    public static AddQnToMasterList init(ExtentTest t1) {
        return new AddQnToMasterList(t1);
    }

    public void navAddQnsToMasterList() throws Exception {
        navigateTo("PINMANAGE_ALL", "PINMANAGE_ADDQUEST", "Add Questions to Master List");
    }

    public AddQnToMasterList downloadQuestionTemplate() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_ADD_MASTER_QUESTIONS_LIST);
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_ADD_MASTER_QUESTIONS_LIST); // is hardcoded can be Generic TODO
        Utils.putThreadSleep(NumberConstants.SLEEP_3000);
        downloadImg.click();
        pageInfo.info("Click On button Download");
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Utils.putThreadSleep(NumberConstants.SLEEP_3000);
        Utils.closeUntitledWindows();
        return this;
    }

    public ArrayList<String> getHeadersFromTemplate() throws Exception {

        ArrayList<String> headers = new ArrayList<String>();
        String file = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_ADD_MASTER_QUESTIONS_LIST);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String header = br.readLine();
        if (header != null) {
            String[] columns = header.split(",");
            for (int i = 0; i < columns.length; i++) {
                headers.add(columns[i]);
            }
        }
        return headers;
    }

    public String setQuestionsInTemplate(HashMap<String, String> questions, String languageCode) throws IOException, InterruptedException {
        Markup m = MarkupHelper.createLabel("setQuestionsInTemplate", ExtentColor.TEAL);
        pageInfo.info(m); // Method Start Marker
        Set<String> quesCode = questions.keySet();
        String file = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_ADD_MASTER_QUESTIONS_LIST);

        try {
            BufferedWriter e = new BufferedWriter(new FileWriter(file, true));
            //e.newLine();
            for (String qnCode : quesCode) {
                e.write(qnCode + "," + languageCode + "," + questions.get(qnCode));
                e.newLine();
            }

            e.close();
        } catch (Exception var4) {
            Assertion.raiseExceptionAndContinue(var4, pageInfo);
        }

        return file;
    }

    public void setUploadFile(String path) {
        this.chooseFileBtn.sendKeys(new CharSequence[]{path});
        pageInfo.info("Set File for upload at Path - " + path);
    }

    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }

    public AddQnToMasterList checkLogFileSpecificMessage(String questionCode, String msgCode, String... params) throws Exception {
        logImg.click();
        Utils.putThreadSleep(NumberConstants.SLEEP_1000);
        try {
            String logFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_ADD_MASTER_QUESTIONS_LOG);

            File file = new File(logFile);
            Scanner s = new Scanner(file);

            boolean isVerified = false;
            boolean isFound = false;
            String expectedMsg = MessageReader.getMessage(msgCode, null);


            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (line.contains(questionCode)) {
                    isFound = true;
                    String code[][] = {{"Verify Log File", "Add question to Master list"}, {"Actual", line.toString()}, {"Expected QuestionCode", questionCode}, {"Expected Message", expectedMsg}};
                    Markup m = MarkupHelper.createTable(code);
                    if (line.contains(expectedMsg)) {
                        pageInfo.pass(m);
                    } else {
                        pageInfo.fail(m);
                        Assertion.markAsFailure("Failed to verify Log File.");
                    }

                    if (params.length > 1) {
                        for (String data : params) {
                            if (!line.contains(data)) {
                                pageInfo.fail("Fail to find the data:" + data + " in Log Entry:" + line.toString());
                                Assertion.markAsFailure("Failed to verify Log File.");
                            } else {
                                pageInfo.pass("Successfully found the data: '" + data + "' in Log Entry:" + line.toString());
                            }
                        }
                    }
                    break;
                }
            }
            if (!isFound) {
                pageInfo.fail("No Data for Question: " + questionCode + " & message: " + expectedMsg);
            }
            s.close();
        } catch (IOException ioex) {
            System.err.println("IO Exception Occurred" + ioex);
        }
        return this;
    }


}
