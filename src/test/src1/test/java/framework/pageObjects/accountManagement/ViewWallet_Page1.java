package framework.pageObjects.accountManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by surya.dhal on 9/5/2018.
 */
public class ViewWallet_Page1 extends PageInit {
    String MainWindow;
    @FindBy(id = "multipleWalletMgmtDelete_detailsOfSelectedWallet_button_confirm")
    private WebElement confirmButton;


    public ViewWallet_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navViewWalletLink() throws Exception {
        navigateTo("MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_VIEW", "View Wallet");
    }

    public void clickOnViewDetailsLink(String walletID) {
        driver.findElement(By.xpath("//a[contains(@href,'" + walletID + "')]")).click();
        pageInfo.info("Clicked On Wallet Details Link of ID : " + walletID);
    }

    /**
     * Method to handle Window
     */
    public void switchToChildWindow() {
        MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                driver.switchTo().window(ChildWindow);
                pageInfo.info("Switching to Child Window..");
            }
        }
        // Switching to Parent window i.e Main Window.
        //driver.switchTo().window(MainWindow);
    }

    /**
     * Method to switch to main Window
     */
    public void switchToMainWindow() {
        driver.switchTo().window(MainWindow);
        pageInfo.info("Switch to main Window");
    }


    /**
     * Method to close Child Window
     */
    public void closeChildWindow() {
        driver.close();
        pageInfo.info("Closing Child Window.");
    }
}
