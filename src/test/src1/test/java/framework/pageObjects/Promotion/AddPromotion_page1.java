package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class AddPromotion_page1 extends PageInit {


    @FindBy(id = "promotionName")
    private WebElement promotionName;
    @FindBy(id = "applicableFromStr")
    private WebElement from;
    @FindBy(id = "applicableToStr")
    private WebElement to;
    @FindBy(id = "serviceTypeList")
    private WebElement serviceTypeList;
    @FindBy(id = "create_confirm_button_next")
    private WebElement next;

    @FindBy(name = "daystoexpire")
    private WebElement promotionDuration;

    public AddPromotion_page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */


    public void NavigateNA() throws Exception {
        navigateTo("PROMOTION_ALL", "PROMOTION_C_PRO", "Create Promotion");
    }

    public void sendname(String text) {
        setText(promotionName, text, "Promotion Name");
    }

    public void selectStartDate(String Month, String Day) {
        driver.findElement(By.xpath("//*[@id='create_confirm']/table/tbody/tr[3]/td[2]/img")).click();
        pageInfo.info("Click On Start Date Image");
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        WebElement m = driver.findElement(By.name("MonthSelector"));
        Select month = new Select(m);
        month.selectByVisibleText(Month);
        pageInfo.info("Select Month " + Month);
        driver.findElement(By.xpath("//*[@id='c" + Day + "']")).click();
        pageInfo.info("Click On Day " + Day);
        driver.findElement(By.xpath("//input[@value='OK']")).click();
        pageInfo.info("Click On OK Button");
    }

    public void selectEndDate(String Month, String Day) {
        driver.findElement(By.xpath("//*[@id='create_confirm']/table/tbody/tr[4]/td[2]/img")).click();
        pageInfo.info("Click On End Date Image");
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        WebElement m = driver.findElement(By.name("MonthSelector"));
        Select month = new Select(m);
        month.selectByVisibleText(Month);
        pageInfo.info("Select Month " + Month);
        driver.findElement(By.xpath("//*[@id='c" + Day + "']")).click();
        pageInfo.info("Click On Day " + Day);
        driver.findElement(By.xpath("//input[@value='OK']")).click();
        pageInfo.info("Click On OK Button");
    }

    public String getPromotionDuration() {
        wait.until(ExpectedConditions.attributeToBeNotEmpty(promotionDuration, "value"));
        String value = promotionDuration.getAttribute("value");
        pageInfo.info("Promotion Duration is: " + value);
        return value;
    }

    public void sendTodate(String text) {
        removeto();
        setText(to, text, "To date");
    }


    public void sendFromdate(String text) {
        removefrom();
        setText(from, text, "from date");
    }


    public void selectServiceType(String text) {
        selectValue(serviceTypeList, text, "Service Type");
    }


    public void clickOnNextButton() {
        clickOnElement(next, "Next");

    }

    public void removefrom() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('applicableFromStr').removeAttribute('readonly');");
    }


    public void removeto() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('applicableToStr').removeAttribute('readonly');");
    }

}
