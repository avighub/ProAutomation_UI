package framework.pageObjects.Promotion;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class SuspendLMS_page extends PageInit {

    @FindBy(id = "modify_saveConfirm_button_confirm")
    WebElement confirm;

    public SuspendLMS_page(ExtentTest t1) {
        super(t1);
    }

    public static SuspendLMS_page init(ExtentTest t1) {
        return new SuspendLMS_page(t1);
    }

    public void clickonConfirm() {
        clickOnElement(confirm, "Confirm");
    }

    public void NavigateNA() throws Exception {
        navigateTo("PROMOTION_ALL", "PROMOTION_MC_PRO", "Suspend");
    }

    public void selectprofile(String profilename) {
        String xpath = "//*[contains(text(), '" + profilename + "')]/following::a[contains(text(), 'Suspend')]";
        WebElement profile = driver.findElement(By.xpath(xpath));
        profile.click();
        pageInfo.info("Select LMS Profile To suspendLMSProfile it");
        Assertion.alertAssertion("promotion.confirmation.suspend", pageInfo);

    }

}
