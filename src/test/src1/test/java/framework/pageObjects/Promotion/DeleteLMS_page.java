package framework.pageObjects.Promotion;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class DeleteLMS_page extends PageInit {

    @FindBy(id = "modify_saveConfirm_button_confirm")
    WebElement confirm;

    public DeleteLMS_page(ExtentTest t1) {
        super(t1);
    }

    public static DeleteLMS_page init(ExtentTest t1) {
        return new DeleteLMS_page(t1);
    }

    public void clickonConfirm() {
        clickOnElement(confirm, "Confirm");
    }

    public void NavigateNA() throws Exception {
        navigateTo("PROMOTION_ALL", "PROMOTION_MC_PRO", "Deletion");
    }

    public void selectprofile(String profilename) {
        String xpath = "//*[contains(text(), '" + profilename + "')]/following::a[contains(text(), 'Delete')]";
        WebElement profile = driver.findElement(By.xpath(xpath));
        profile.click();
        pageInfo.info("Select Profile for deletion");
        driver.switchTo().alert().accept();
        pageInfo.info("Accept the alert message");
    }

}
