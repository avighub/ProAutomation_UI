package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class AddPromotion_page3 extends PageInit {

    @FindBy(id = "selectForm_button_save")
    WebElement save;

    public AddPromotion_page3(ExtentTest t1) {
        super(t1);
    }

    public static AddPromotion_page3 init(ExtentTest t1) {
        return new AddPromotion_page3(t1);
    }

    public void enterdata(String text1, String text2) {
        int i = 0, j = 0, k = 0;
        for (i = 0; i < 3; i++) {
            WebElement from = driver.findElement(By.xpath("//*[@name='rewardPOJOList[" + i + "].fromAmount']"));                                //rewardPOJOList[0].toAmount
            WebElement to = driver.findElement(By.xpath("//*[@name='rewardPOJOList[" + i + "].toAmount']"));
            WebElement rewardto = driver.findElement(By.xpath("//*[@name='rewardPOJOList[" + i + "].rewardedTo']"));
            WebElement rewardtype = driver.findElement(By.xpath("//*[@name='rewardPOJOList[" + i + "].serviceOrRewardId']"));
            WebElement fixed = driver.findElement(By.xpath("//*[@name='rewardPOJOList[" + i + "].fixed']"));
            WebElement percentage = driver.findElement(By.xpath("//*[@name='rewardPOJOList[" + i + "].percentage']"));
            WebElement mutiplicationfaceter = driver.findElement(By.xpath("//*[@name='rewardPOJOList[" + i + "].multiplicationFactor']"));

            Select option1 = new Select(rewardto);
            option1.selectByVisibleText(text1);

            Select option2 = new Select(rewardtype);
            option2.selectByVisibleText(text2);

            from.sendKeys("" + ((1 + j) + i));
            to.sendKeys("" + (51 + j));
            fixed.sendKeys("" + ((10 * i) + 5));
            percentage.sendKeys("" + ((10 * i) + 5));
            mutiplicationfaceter.sendKeys("" + (2 * i));
            j = j + 50;
        }

    }


    public void clickonsavebutton() {
        save.click();
        pageInfo.info("Click on Save Button");
    }


}
