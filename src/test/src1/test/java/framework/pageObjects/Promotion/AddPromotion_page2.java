package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class AddPromotion_page2 extends PageInit {


    @FindBy(id = "mfsProvidersList")
    WebElement payermfsProviders;
    @FindBy(id = "domainList")
    WebElement payerdomain;
    @FindBy(id = "categoryList")
    WebElement payercat;
    @FindBy(id = "paymentInstrument")
    WebElement payerPI;
    @FindBy(id = "linkedWalletBankList")
    WebElement payerlinkWB;
    @FindBy(id = "gradeList")
    WebElement payergrade;
    @FindBy(id = "payeeMfsProvidersList")
    WebElement payeemfsProviders;
    @FindBy(id = "payeeDomainList")
    WebElement payeedomain;
    @FindBy(id = "payeeCategoryList")
    WebElement payeecat;
    @FindBy(id = "payeePaymentInstrument")
    WebElement payeePI;
    @FindBy(id = "payeeLinkedWalletBankList")
    WebElement payeelinkWB;
    @FindBy(id = "payeeGradeList")
    WebElement payeegrade;
    @FindBy(id = "selectForm_button_next")
    WebElement next;

    public AddPromotion_page2(ExtentTest t1) {
        super(t1);
    }

    public static AddPromotion_page2 init(ExtentTest t1) {
        return new AddPromotion_page2(t1);
    }

    public void selectPayerMfsProvider() {
        selectIndex(payermfsProviders, 0, "Payer MFS Provider");
    }
    public void selectPayerMfsProvider(String providerId) {
        selectValue(payermfsProviders, providerId, "Payer MFS Provider");
    }


    public void selectPayerDomain(String payer) {
        selectVisibleText(payerdomain, payer, "Payer Domain");
    }

    public void selectPayerCat(String payer) {
        selectVisibleText(payercat, payer, "Payer Category");
    }

    public void selectPayerDomain() {
        selectIndex(payeedomain, 0, "Payee Domain");
    }

    public void selectPayerCat() {
        selectIndex(payeecat, 0, "Payee category");
    }

    public void selectPayerPaymentInstruction() {
        selectIndex(payerPI, 0, "Payer Instruction");
    }

    public void selectPayerPaymentInstruction(String payInst) {
        selectVisibleText(payerPI, payInst, "Payer Payment Instrument");
    }

    public void selectPayerWB() {
        selectIndex(payerlinkWB, 0, "Payer WB");
    }

    public void selectPayerWB(String payId) {
        selectValue(payerlinkWB, payId, "Payer WB");
    }

    public void selectPayerGrade() {
        selectIndex(payergrade, 0, "Payer Grade");
    }

    public void selectPayerGrade(String gradeName) {
        selectVisibleText(payergrade, gradeName, "Payer Grade");
    }

    public void selectPayeeMfsProvider() {
        selectIndex(payeemfsProviders, 0, "Payee MFS Provider");
    }
    public void selectPayeeMfsProvider(String id) {
        selectValue(payeemfsProviders, id, "Payee MFS Provider");
    }

    public void selectPayeeDomain(String payee) {
        selectVisibleText(payeedomain, payee, "Payee Domain");
    }

    public void selectPayeeCat(String payee) {
        selectVisibleText(payeecat, payee, "Payee category");
    }

    public void selectPayeeDomain() {
        selectIndex(payeedomain, 0, "Payee Domain");
    }

    public void selectPayeeCat() {
        selectIndex(payeecat, 0, "Payee category");
    }

    public void selectPayeePaymentInstruction() {
        selectIndex(payeePI, 0, "Payee Payment Instruction");
    }

    public void selectPayeePaymentInstruction(String payInst) {
        selectVisibleText(payeePI, payInst, "Payee Payment Instruction");
    }

    public void selectPayeeWB() {
        selectIndex(payeelinkWB, 0, "Payee WB");
    }

    public void selectPayeeWB(String payId) {
        selectValue(payeelinkWB, payId, "Payee WB");
    }

    public List<WebElement> clickPayeeWB() {
        clickOnElement(payerlinkWB,"Payee WB");
        Select webBanks = new Select(payerlinkWB);
        List<WebElement> opt = webBanks.getOptions();
        return opt;
    }

    public void selectPayeeGrade() {
        selectIndex(payeegrade, 0, "Payee Grade");
    }
    public void selectPayeeGrade(String gradeName) {
        selectVisibleText(payeegrade, gradeName, "Payee Grade");
    }


    public void clickOnNextButton() {
        clickOnElement(next, "Next");

    }


}
