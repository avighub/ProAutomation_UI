package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;


public class ModifyLMS_page2 extends PageInit {

    @FindBy(id = "modify_showPromotionAssociation_button_next")
    WebElement next;
    @FindBy(id = "serviceTypeList")
    WebElement serviceType;

    public ModifyLMS_page2(ExtentTest t1) {
        super(t1);
    }

    public static ModifyLMS_page2 init(ExtentTest t1) {
        ;
        return new ModifyLMS_page2(t1);
    }

    public void checkServiceType() throws IOException {
        if (serviceType.isEnabled()) {
            pageInfo.fail("serviceType box is enabled..");
        } else {
            pageInfo.pass("serviceType box is not enabled..");
        }
        Assertion.attachScreenShotAndLogs(pageInfo);
    }

    public void clickonnext() {
        clickOnElement(next, "Next");
    }

}
