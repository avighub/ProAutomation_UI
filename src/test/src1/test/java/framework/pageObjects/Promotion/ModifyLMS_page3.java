package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifyLMS_page3 extends PageInit {

    @FindBy(id = "selectForm_button_next")
    WebElement next;

    public ModifyLMS_page3(ExtentTest t1) {
        super(t1);
    }

    public static ModifyLMS_page3 init(ExtentTest t1) {

        return new ModifyLMS_page3(t1);
    }

    public void clickonnext() {
        clickOnElement(next, "Next");
    }

}
