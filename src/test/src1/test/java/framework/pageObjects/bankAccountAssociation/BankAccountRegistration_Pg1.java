package framework.pageObjects.bankAccountAssociation;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.UserType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class BankAccountRegistration_Pg1 extends PageInit {

    @FindBy(id = "bankAdd_view_msisdn")
    private WebElement msisdn;

    @FindBy(css = "[name='msisdn'][type='text']")
    private WebElement msisdn1;

    @FindBy(id = "bankAdd_view_selectedProviderId")
    private WebElement provider;

    @FindBy(id = "bankTypeId")
    private WebElement bankType;

    @FindBy(id = "bankAdd_view_submit")
    private WebElement submit;

    @FindBy(id = "bankAdd_displayBankAccountRegistration_submit")
    private WebElement submit1;

    @FindBy(id= "userType1")
    private WebElement userType;

    @FindBy(id= "bankAdd_displayBankAccountRegistration_selectedProviderId")
    private WebElement providerType1;

    @FindBy(id= "bankAdd_displayBankAccountDeRegistration_submit")
    private WebElement bankType1;

    /****Bank DeRegistration******/

    @FindBy(id = "bankAdd_displayBankAccountDeRegistration_msisdn")
    private WebElement msisdnDeReg;

    @FindBy(id = "typeId")
    private WebElement typeIdDeReg;

    @FindBy(id = "bankAdd_displayBankAccountDeRegistration_submit")
    private WebElement submitBtnDeReg;

    @FindBy(id = "mfsProvidersList")
    private WebElement providerType;

    @FindBy(id = "optWalletId1")
    private WebElement walletId;

    @FindBy(id = "userType1")
    private WebElement user;

    @FindBy(name = "selectedOperatorId")
    private WebElement optName;

    @FindBy(id= "bankAdd_displayBankAccountDeRegistration_selectedProviderId")
    private WebElement providerTypeForDeRegistration;

    public BankAccountRegistration_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static BankAccountRegistration_Pg1 init(ExtentTest test) {
        return new BankAccountRegistration_Pg1(test);
    }

    public BankAccountRegistration_Pg1 navigateToLink() throws Exception {
        navigateTo("BNKACCLINK_ALL", "BNKACCLINK_BANK_ACC_REG", "Bank Account Registration");
        return this;
    }

    public BankAccountRegistration_Pg1 selectRechargeOperator(String operatorId) {
        selectVisibleText(optName, operatorId, "Recharge Operator Dropdown");
        return this;
    }

    public void selectProviderType(String type) {
        selectVisibleText(providerType1, type, "Provider Type");
    }

    public BankAccountRegistration_Pg1 selectUser(String userType) {
        selectValue(user, userType, "Select User Type");
        return this;
    }

    public BankAccountRegistration_Pg1 selectProvider(String providerName) {
        selectVisibleText(providerType, providerName, "Select Provider");
        return this;
    }

    public BankAccountRegistration_Pg1 setWalletId(String walletType) {
        setText(walletId, walletType, "Set Wallet");
        return this;
    }

    public void setMSISDNPrefN(String text) {
        setText(msisdn1, text, "MSISDN");
    }

    public void clickSubmitBtnPrefN() {
        clickOnElement(submit1, "Submit 1");
    }

    public BankAccountRegistration_Pg1 selectUserType(String category) {
        if (category.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            selectValue(userType, Constants.USR_TYPE_SUBS, "User Type");
        } else {
            selectValue(userType, Constants.USR_TYPE_CHANNEL, "User Type");
        }

        return this;
    }



    public BankAccountRegistration_Pg1 selectUserTypeByValue(UserType typeOfUser) {
        selectValue(userType,typeOfUser.toString(),"User Type");
        return this;
    }


    public static void main(String[] args) {
        System.out.println(UserType.CHANNEL.name());
    }

    /****Bank DeRegistration******/

    public void navigateToLinkDeReg() throws Exception {
        navigateTo("BNKACCLINK_ALL", "BNKACCLINK_BANK_ACC_DREG", "Bank Account de registration");
    }

    public void selectProviderTypeForDeRegistration(String type) {
        selectVisibleText(providerTypeForDeRegistration, type, "Provider for De Reg");
    }

    public void setMSISDNdeReg(String text) {
        setText(msisdnDeReg, text, "Msisdn for De reg");
    }

    public void selectUserTypeDeReg(String type) {
        selectVisibleText(typeIdDeReg, type, "User Type");
    }

    public void clickSubmitBtnDeReg() {
        clickOnElement(submitBtnDeReg, "Submit De Registration");
    }


}
