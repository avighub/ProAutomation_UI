package framework.pageObjects.bankAccountAssociation;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;


public class BankAccountRegistration_Pg2 extends PageInit {

    @FindBy(id = "bankAdd_view_button_confirm")
    private WebElement confirmBtn;
    @FindBy(xpath = "//input[@id='bankAdd_displayBankAccountRegistration_submit' and @value='Add More']")
    private WebElement addMoreBtn;
    @FindBy(xpath = "//input[@id='bankAdd_addMoreBankRows_submit' and @value='Add More']")
    private WebElement addMoreButton;
    @FindBy(name = "bankCounterList[0].paymentTypeSelected")
    private WebElement linkedBank;
    @FindBy(name = "bankCounterList[0].customerId")
    private WebElement customerId;
    @FindBy(name = "bankCounterList[0].accountNumber")
    private WebElement accNo;
    @FindBy(xpath = "//input[@id='bankAdd_addMoreBankRows_submit' and @value='Submit']")
    private WebElement submit;
    @FindBy(xpath = "//input[@id='bankAdd_addMoreBankRows_submit' and @value='Submit']")
    private WebElement bankList;

    public BankAccountRegistration_Pg2(ExtentTest t1) {
        super(t1);
    }

    public void clickConfirmBtn() {
        confirmBtn.click();
        pageInfo.info("Click On ConfirmBtn");
    }

    public void clickAddMoreBtn() {
        addMoreBtn.click();
        pageInfo.info("Click On Add More Btn");
    }

    public void clickAddMoreBtnAgain() {
        addMoreButton.click();
        pageInfo.info("Click On Add More Btn again");
    }

    public void selectLinkedBank(String bankName) {
        linkedBank.click();
        Select stat1 = new Select(linkedBank);
        stat1.selectByVisibleText(bankName);
        pageInfo.info("Select Linked Bank :" + bankName);
    }

    public void clickSubmitBtn() {
        submit.click();
        pageInfo.info("Click On Submit Btn");
    }

    public List<String> getLinkedBankList() {
        List<WebElement> rows = driver.findElements(By.xpath("(//form[@id='bankAdd_addMoreBankRows']/table/tbody/tr)[position()>1 and position()<last()]"));
        List<String> list = new ArrayList<String>();
        if (rows.size() >= 1) {
            int size = rows.size() - 1;
            WebElement bank = driver.findElement(By.name("bankCounterList[" + size + "].paymentTypeSelected"));
            bank.click();
            Select stat1 = new Select(bank);

            List<WebElement> opt1 = stat1.getOptions();
            for (WebElement options1 : opt1) {
                list.add(options1.getText());
            }
            pageInfo.info("Get all the bank names");
            return list;

        } else {
            linkedBank.click();

            Select stat2 = new Select(linkedBank);

            List<WebElement> opt = stat2.getOptions();
            for (WebElement options : opt) {
                list.add(options.getText());
            }
            pageInfo.info("Get all the bank names");
            return list;
        }
    }

    public String getCustomerId() {
        String custId = customerId.getAttribute("value");
        pageInfo.info("Customer Id - " + custId);
        return custId;
    }

    public void setCustomerId(String text) {
        customerId.sendKeys(text);
        pageInfo.info("Set Customer Id - " + text);
    }

    public String getAccountNo() {
        String acc = accNo.getAttribute("value");
        pageInfo.info("Account Number - " + acc);
        return acc;
    }

    public void setAccountNo(String text) {
        accNo.sendKeys(text);
        pageInfo.info("Set Account Number - " + text);
    }

    /****Bank DeRegistration******/

    public void clickDeleteBtn(String accNo) {
        driver.findElement(By.xpath("//td[contains(text(),'" + accNo + "')]/..//a[text()='Delete']")).click();
        pageInfo.info("Click On delete button for account " + accNo);
    }

    public void checkForDeletedBank(String accNo, ExtentTest t) {
        try {
            driver.findElement(By.xpath("//td[contains(text(),'" + accNo + "')]"));
            t.fail("Deleted Bank is displayed in Deregistration page");
        } catch (NoSuchElementException e) {
            t.pass("Deleted Bank is not displayed in Deregistration page");
        }
    }

}
