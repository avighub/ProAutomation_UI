package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class SuspendResumeSubscriber_Page1 extends PageInit {


    @FindBy(name = "userType")
    WebElement userTypeDdown;

    @FindBy(name = "accessId")
    WebElement msisdnTbox;

    @FindBy(name = "nickname")
    WebElement nicknameTbox;

    @FindBy(name = "suspendSubStatus")
    WebElement suspendStatusDdown;

    @FindBy(id = "suspendResume_suspend_button_suspend")
    WebElement suspendButton;

    @FindBy(id = "suspendResume_suspend_button_resume")
    WebElement resumeButton;

    @FindBy(name = "button.confirm")
    WebElement confirmButton;


    public SuspendResumeSubscriber_Page1(ExtentTest t1) {
        super(t1);
    }

    public static SuspendResumeSubscriber_Page1 init(ExtentTest t1) {
        return new SuspendResumeSubscriber_Page1(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_SR_USR001", "Suspend Subscriber");
    }

    public void selectUserTypeByValue(String uType) {
        selectValue(userTypeDdown, uType, "User Type");
    }

    public void selectSuspendStatus(String status) {
        selectValue(suspendStatusDdown, status, "suspendStatusDdown");
    }

    public void setMSISDN(String msisdn) {
        setText(msisdnTbox, msisdn, "msisdnTbox");
    }

    public void clickSuspendButton() {
        clickOnElement(suspendButton, "suspendButton");
    }

    public void clickResumeButton() {
        clickOnElement(resumeButton, "resumeButton");
    }

    public void clickConfirmButton() {
        clickOnElement(confirmButton, "confirmButton");
    }

}
