package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by vandana.rattan on 11/30/2018.
 */
public class AddSubcriber_Page3 extends PageInit {

    @FindBy(name = "counterList[0].providerSelected")
    WebElement selectprovider;
    @FindBy(name = "counterList[0].paymentTypeSelected")
    WebElement selectWalletType;
    @FindBy(name = "counterList[0].statusSelected")
    WebElement selectStatus;
    @FindBy(xpath = "//form[@id='subsRegistrationServiceBean_output']/table[2]/tbody/tr/td[2]")
    WebElement next;
    //form[@id='subsRegistrationServiceBean_assignBankToUser']/table[2]/tbody/tr/td[2]
    @FindBy(xpath = "//form[@id='subsRegistrationServiceBean_assignBankToUser']/table[2]/tbody/tr/td[2]")
    WebElement next1;
    @FindBy(id = "grade0")
    WebElement grade;
    @FindBy(id = "tcp0")
    WebElement tcpProfile;
    @FindBy(id = "subsRegistrationServiceBean_confirm_button_confirm")
    WebElement confirm;

    //confirmation page
    @FindBy(id = "subsRegistrationServiceBean_assignBankToUser_bankCounterList_0__statusSelected")
    WebElement LinkedBankstatus;

    public AddSubcriber_Page3(ExtentTest t1) {
        super(t1);
    }

    public void selectGrade() {
        selectVisibleText(grade, "Gold Subscriber", "Grade");
    }

    public void selectMFSProvider() {
        selectVisibleText(selectprovider, "MFS1", "MFS Provider");
    }

    public void selectWallet() {
        selectVisibleText(selectWalletType, "SAVINGCLUB", "select wallet type");
    }

    public void selectStatus() {
        selectVisibleText(selectStatus, "Suspended", "Status");
    }

    public void clickSubmit() {
        clickOnElement(next, "Next");
    }

    public void clickSubmit2() {
        clickOnElement(next1, "Next");
    }

    public void clickConfirm() {
        clickOnElement(confirm, "Confirm");
    }

    public void selectBankStatus() {
        selectVisibleText(LinkedBankstatus, "Suspended", "Status");
    }

    public void selectTcp() {
        selectVisibleText(tcpProfile, "", "Status");
    }



  /*  public ModifySubscriber_page2 clickOnNextPg2() {
        clickOnElement(nextPageButton, "nextPageButton");
        return new ModifySubscriber_page2(pageInfo);
    }*/


}
