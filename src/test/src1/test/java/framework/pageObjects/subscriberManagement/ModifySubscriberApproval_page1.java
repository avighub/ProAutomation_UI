package framework.pageObjects.subscriberManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifySubscriberApproval_page1 extends PageInit {

    @FindBy(name = "approve")
    private WebElement approveButton;
    @FindBy(name = "rejectReason")
    private WebElement reasonBox;
    @FindBy(id = "subsRegistrationModifyChecker_modifySubsApproveOrReject_action_submit")
    private WebElement finalSubmitButton;
    @FindBy(name = "reject")
    private WebElement rejectButton;

    public ModifySubscriberApproval_page1(ExtentTest t1) {
        super(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSMODAP", "Modify Subscriber Approval");
    }


    public void selectUsertoApprove(String MSISDN) {
        WebElement el = driver.findElement(By.xpath("//tr/td[contains(text(),'" + MSISDN + "')]/ancestor::tr[1]/td/input[@type='checkbox']"));
        el.click();
        pageInfo.info("Selecting Check box having MSISDN:" + MSISDN);
    }


    public void approveButton_Click() {
        clickOnElement(approveButton, "Approve Button");
    }

    public void clickFinalSubmitButton() {
        clickOnElement(finalSubmitButton, "Final Page Submit Button");
    }

    public void setReason(String reason) {
        setText(reasonBox, reason, "Reason Text Box");
    }

    public void clickOnRejectButton() {
        clickOnElement(rejectButton, "Reject Button");
    }
}
