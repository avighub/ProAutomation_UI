package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class DeleteSubscriber_Page1 extends PageInit {

    @FindBy(name = "msisdn")
    WebElement msisdnTbox;

    @FindBy(name = "mfsProvider")
    WebElement mfsProviderDdown;

    @FindBy(id = "subsDelete_deleteOutput_button_submit")
    WebElement submitButton;

    @FindBy(name = "button.submit")
    WebElement subsDeleteByAgt;

    @FindBy(id = "subsDelete_save_button_confirmdelete")
    WebElement confirmButton;

    @FindBy(id = "subsDelete_save_button_confirmdelete")
    WebElement confirmDeleteByAgent;

    public DeleteSubscriber_Page1(ExtentTest t) {
        super(t);
    }

    public static DeleteSubscriber_Page1 init(ExtentTest t) {
        return new DeleteSubscriber_Page1(t);
    }

    public DeleteSubscriber_Page1 navSubsDeleteInitiateByagent() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSDELBYAGT", "Subscriber Delete Initiate By Agent");
        return this;
    }

    public DeleteSubscriber_Page1 navSubscriberDeleteInitiate() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSDEL", "Subscriber Delete Initiate");
        return this;
    }

    public DeleteSubscriber_Page1 selectProviderByValue(String providerID) {
        selectValue(mfsProviderDdown, providerID, "MFS Provider");
        return this;
    }

    public DeleteSubscriber_Page1 selectProviderByName(String providerName) {
        selectVisibleText(mfsProviderDdown, providerName, "MFS Provider");
        return this;
    }

    public DeleteSubscriber_Page1 setMSISDN(String msisdn) {
        setText(msisdnTbox, msisdn, "MSISDN");
        return this;
    }

    public DeleteSubscriber_Page1 confirmDelete() {
        clickOnElement(confirmButton, "Confirm");
        return this;
    }

    public DeleteSubscriber_Page1 confirmDeleteByAgent() {
        clickOnElement(confirmDeleteByAgent, "Confirm Delete By Agent");
        return this;
    }

    public DeleteSubscriber_Page1 confirmSubmit() {
        clickOnElement(submitButton, "Submit");
        return this;
    }

    public DeleteSubscriber_Page1 clickDeleteSubsByAgentSubmit() {
        clickOnElement(subsDeleteByAgt, "subsDeleteByAgt");
        return this;
    }

}
