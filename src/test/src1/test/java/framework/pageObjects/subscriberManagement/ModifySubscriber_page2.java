package framework.pageObjects.subscriberManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;


public class ModifySubscriber_page2 extends PageInit {


    @FindBy(id = "_button_nextPage")
    WebElement nextPageButton;

    @FindBy(id = "_userName")
    WebElement firstnameTbox;

    @FindBy(id = "_lastName")
    WebElement lastnameTbox;

    @FindBy(id = "msisdn")
    WebElement msisdnTbox;

    @FindBy(id = "externalCode")
    WebElement externalCode;

    @FindBy(id = "_userNamePrefix")
    WebElement namePrefix;

    @FindBy(id = "_idType")
    WebElement idType;

    @FindBy(id = "_idNo")
    WebElement idNumber;

    @FindBy(id = "isIdExpires")
    WebElement idExpire;
    String idNum = DataFactory.getRandomNumberAsString(5);
    @FindBy(id = "_imtidType")
    WebElement imtIdType;
    @FindBy(id = "_idNo")
    WebElement imtIdNumber;
    @FindBy(id = "_idIssuePlace")
    WebElement idIssuePlace;
    @FindBy(id = "_idIssueCountry")
    WebElement idIssueCountry;
    @FindBy(id = "_postalCode")
    WebElement postalCode;
    @FindBy(id = "_nationality")
    WebElement nationality;
    @FindBy(id = "_birthPlace")
    WebElement birthPlace;
    @FindBy(id = "_birthCountry")
    WebElement birthCountry;
    @FindBy(id = "_residenceCountry")
    WebElement residencyCountry;
    @FindBy(id = "_passportIssueCountry")
    WebElement passportCountry;
    @FindBy(id = "_passportIssueCity")
    WebElement passportIssueCity;
    @FindBy(id = "_occupation")
    WebElement occupation;
    @FindBy(id = "_employerName")
    WebElement employeeName;
    @FindBy(name = "dojo.displayIssueDate")
    WebElement idIssueDate;
    @FindBy(name = "displayIssueDate")
    WebElement displayDate;
    @FindBy(name = "dojo.displayExpiryDate")
    WebElement expiryDate;

    @FindBy(name = "doc1")
    WebElement uploadIdentityProof;
    @FindBy(name = "proofType1")
    WebElement selectIdentityProof;
    @FindBy(name = "doc2")
    WebElement uploadAddressProof;
    @FindBy(name = "proofType2")
    WebElement selectAddressProof;
    @FindBy(name = "doc3")
    WebElement uploadPhotoProof;
    @FindBy(name = "proofType3")
    WebElement selectPhotoProof;

    public ModifySubscriber_page2(ExtentTest t1) {
        super(t1);
    }

    public static ModifySubscriber_page2 init(ExtentTest t1) {
        return new ModifySubscriber_page2(t1);
    }

    public ModifySubscriber_page2 setNamePrefix() {
        selectIndex(namePrefix, 1, "Set Name Prefix");
        return this;
    }

    public ModifySubscriber_page2 setIdType() {
        selectIndex(idType, 1, "Set ID Type");
        return this;
    }

    public ModifySubscriber_page2 setIdNumber() {
        setText(idNumber, idNum, "Set ID Number");
        return this;
    }

    public void setFirstnameTbox(String text) {
        setText(firstnameTbox, text, "firstnameTbox");
    }

    public String lastname_GetText() {
        return lastnameTbox.getText().toString();
    }

    public ModifySubscriber_page2 lastname_SetText(String text) {
        setText(lastnameTbox, text, "lastnameTbox");
        return this;
    }

    public ModifySubscriber_page2 msisdn_SetText(String text) {
        setText(msisdnTbox, text, "msisdnTbox");
        return this;
    }

    public ModifySubscriber_page2 setNewMSISDN(String text) {
        setText(msisdnTbox, text, "Set New MSISDN");
        return this;
    }

    public ModifySubscriber_page2 externalCode_SetText(String text) {
        setText(externalCode, text, "externalCode");
        return this;
    }

    public ModifySubscriber_page2 clickOnNextPg2() {
        clickOnElement(nextPageButton, "nextPageButton");
        return new ModifySubscriber_page2(pageInfo);
    }

    public ModifySubscriber_page2 unCheckIdExpire() {
        if (idExpire.isSelected()) {
            clickOnElement(idExpire, "Un check ID Expire");
        }
        return new ModifySubscriber_page2(pageInfo);
    }

    public ModifySubscriber_page2 selectImtIdType() {
        selectIndex(imtIdType, 1, "IMT ID Type");
        return this;
    }

    public void setImtIdNumber(String idNumber) {
        setText(imtIdNumber, idNumber, "Set Imt Id Number");
    }

    public void setIdIssuePlace() {
        setText(idIssuePlace, "Banglore", "Id Issue Place");
    }

    public void selectIdIssueCountry() {
        selectValue(idIssueCountry, "IN", "Id Issue Country");
    }

    public void setPostalCode() {
        setText(postalCode, "560067", "Postal Code");
    }

    public void selectNationality() {
        selectValue(nationality, "IN", "Nationality");
    }

    public void setBirthPlace() {
        setText(birthPlace, "Banglore", "Birth Place");
    }

    public void selectBirthCountry() {
        selectValue(birthCountry, "IN", "Birth Country");
    }

    public void selectResidencyCountry() {
        selectValue(residencyCountry, "IN", "Residency Country");
    }

    public void selectPassportCountry() {
        selectValue(passportCountry, "IN", "Passport Country");
    }

    public void setPassportIssueCity() {
        setText(passportIssueCity, "Banglore", "Passport Issue City");
    }

    public void setOccupation() {
        setText(occupation, "QA", "Occupation");
    }

    public void setEmployeeName() {
        setText(employeeName, "Comviva", "Employee Name");
    }

    public ModifySubscriber_page2 setIdIssueDate(String date) {
        if (displayDate.getAttribute("value").equals("")) {
            new Actions(driver).sendKeys(idIssueDate, date).build().perform();
        }
        return this;
    }

    public ModifySubscriber_page2 setExpiryDate(String date) {
        new Actions(driver).sendKeys(expiryDate, date).build().perform();
        return this;
    }


    public void uploadIdentityProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadIdentityProof, uploadFile, "uploadIdentityProof");
    }

    public void selectIdentityProof() {
        selectIndex(selectIdentityProof, 1, "selectIdentityProof");
    }

    public void uploadAddressProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadAddressProof, uploadFile, "uploadAddressProof");
    }

    public void selectAddressProof() {
        selectIndex(selectAddressProof, 2, "selectAddressProof");
    }

    public void uploadPhotoProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadPhotoProof, uploadFile, "uploadPhotoProof");
    }

    public void selectPhotoProof() {
        selectIndex(selectPhotoProof, 3, "selectPhotoProof");
    }


}
