package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class DeleteSubscriberByAgent_Page1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(name = "msisdn")
    WebElement msisdnTbox;
    @FindBy(name = "mfsProvider")
    WebElement mfsProviderDdown;
    @FindBy(id = "subsDelete_deleteOutput_button_submit")
    WebElement submitButton;
    @FindBy(id = "subsDelete_save_button_confirmdelete")
    WebElement confirmButton;

    public static DeleteSubscriberByAgent_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        DeleteSubscriberByAgent_Page1 page = PageFactory.initElements(driver, DeleteSubscriberByAgent_Page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public DeleteSubscriberByAgent_Page1 navSubsDeleteInitiateByagent() throws Exception {
        fl.leftNavigation("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSDELBYAGT");
        pageInfo.info("Navigate to Subscriber Delete By Agent Page");
        return this;
    }

    public DeleteSubscriberByAgent_Page1 navSubscriberDeleteInitiate() throws Exception {
        fl.leftNavigation("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSDEL");
        pageInfo.info("Navigate to Subscriber Delete");
        return this;
    }

    public DeleteSubscriberByAgent_Page1 selectProviderByValue(String providerID) {
        Select prov = new Select(mfsProviderDdown);
        prov.selectByValue(providerID);
        pageInfo.info("Select Provider By Value: " + providerID);
        return this;
    }

    public DeleteSubscriberByAgent_Page1 selectProviderByName(String providerName) {
        Select prov = new Select(mfsProviderDdown);
        prov.selectByValue(providerName);
        pageInfo.info("Select Provider By Name: " + providerName);
        return this;
    }


    public DeleteSubscriberByAgent_Page1 setMSISDN(String msisdn) {
        msisdnTbox.clear();
        msisdnTbox.sendKeys(msisdn);
        pageInfo.info("Enter MSISDN: " + msisdn);
        return this;
    }

    public DeleteSubscriberByAgent_Page1 confirmDelete() {
        confirmButton.click();
        pageInfo.info("Click on Confirm");
        return this;
    }

    public DeleteSubscriberByAgent_Page1 confirmSubmit() {
        submitButton.click();
        pageInfo.info("Click on Submit Button !");
        return this;
    }

}
