/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 27-Nov-2017
*  Purpose: Page1 of C2C
*/
package framework.pageObjects.c2c;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Page for creating the channel user
 *
 * @author sapan.dang
 */
public class ChannelToChannelTransfer_page1 extends PageInit {


    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/


    String channeluserprovider_id = "channelProviderList";
    String channelWalletList_id = "channelWalletList";
    String domainName_id = "sel";
    String payeeCategory_id = "ops1";
    /**
     * Get all the combinaton of top four comboBOX
     *
     * @return
     */
    int time = 500;
    @FindBy(id = "channelProviderList")
    private WebElement channelUserProvider;
    @FindBy(id = "channelWalletList")
    private WebElement channelUserWallet;
    @FindBy(id = "sel")
    private WebElement domainNameTBox;
    @FindBy(id = "ops1")
    private WebElement payeeCategory;
    @FindBy(id = "otherProviderListSel")
    private WebElement payeeProviderDDown;
    @FindBy(id = "otherWalletListId")
    private WebElement payeeWalletTypeDDown;
    @FindBy(name = "accessId")
    private WebElement MsisdnTBox;
    @FindBy(id = "selectForm_button_submit")
    private WebElement submitBtn;
    @FindBy(id = "c2cTrf_confirm_button_submit")
    private WebElement confirmBtn;

    public ChannelToChannelTransfer_page1(ExtentTest t1) {
        super(t1);
    }

    public static ChannelToChannelTransfer_page1 init(ExtentTest t1) {
        return new ChannelToChannelTransfer_page1(t1);
    }

    /**
     * Method to Navigate to C2C page
     */
    public void navigateToLink() throws Exception {
        navigateTo("C2C_ALL", "C2C_C2C_TR", "Channel to Channel Transfer");
    }

    /**
     * Method to select channel user provider
     *
     * @param provider
     */
    public void channelUserProviderSelectValue(String provider) {
        selectValue(channelUserProvider, provider, "channelUserProvider");
    }

    /**
     * Method to select Wallet
     *
     * @param text
     */
    public void channelUserWalletSelectValue(String text) {
        selectValue(channelUserWallet, text, "channelUserWallet");
    }

    /**
     * Method to select Domain Name
     *
     * @param provider
     */
    public void domainNameSelectValue(String provider) {
        selectValue(domainNameTBox, provider, "domainNameTBox");
    }

    /**
     * Method to select payee Category Type
     *
     * @param text
     */
    public void payeeCategorySelectValue(String text) {
        selectValue(payeeCategory, text, "payeeCategory");
    }

    /**
     * Method to select payee provider
     *
     * @param text
     */
    public void payeeProviderSelectValue(String text) {
        selectValue(payeeProviderDDown, text, "payeeProviderDDown");
    }

    /**
     * Method to select payee wallet type
     *
     * @param wallet
     */
    public void payeeWalletTypeSelectValue(String wallet) {
        selectValue(payeeWalletTypeDDown, wallet, "payeeWalletTypeDDown");
    }

    /**
     * Method to Set MSISDN
     *
     * @param msisdn
     */
    public void msisdnSetText(String msisdn) {
        setText(MsisdnTBox, msisdn, "MsisdnTBox");
    }

    //-------------------------------------------------------------------------------------------
    //-------------------- Data Provider
    //-------------------------------------------------------------------------------------------u
    public List<String> getAllchanneluserprovider() {
        /*
    //	channeluserprovider = driver.findElement(By.id(channeluserprovider_id));
		channeluserprovider.click();
		Select s = new Select(channeluserprovider);

		List<WebElement> l = s.getOptions();
		List<String> l_channeluserprovider = new ArrayList<String>();

		for(WebElement a : l)
		{
			if(!a.getText().equals("Select"))
				l_channeluserprovider.add(a.getAttribute("value"));

		}
		System.out.println("All the Providers"+l_channeluserprovider);
		*/

        List<String> l_channeluserprovider = getDropDownValue(channeluserprovider_id);
        return l_channeluserprovider;

    }

    public List<String> getAllchannelUserWalletValue() {
        /*
        //channelUserWallet = driver.findElement(By.id(channelWalletList_id));
		channelUserWallet.click();
		Select s = new Select(channelUserWallet);

		List<WebElement> l = s.getOptions();
		List<String> l_channelUserWallet = new ArrayList<String>();

		for (WebElement a : l) {
			if (!a.getText().equals("Select"))
				l_channelUserWallet.add(a.getAttribute("value"));

		}
		System.out.println("All the wallet" + l_channelUserWallet);
		*/
        List<String> l_channelUserWallet = getDropDownValue(channelWalletList_id);
        return l_channelUserWallet;

    }

    public List<String> getAlldomainNameValue() {
		/*
	//	domainName = driver.findElement(By.id(domainName_id));
		domainName.click();
		Select s = new Select(domainName);

		List<WebElement> l = s.getOptions();
		List<String> l_domainName = new ArrayList<String>();

		for (WebElement a : l) {
			if (!a.getText().equals("Select"))
				l_domainName.add(a.getAttribute("value"));

		}
		System.out.println("All the domain" + l_domainName);
		*/
        List<String> l_domainName = getDropDownValue(domainName_id);
        return l_domainName;

    }

    public List<String> getAllpayeeCategoryValue() {
		/*
	//	payeeCategory = driver.findElement(By.id(payeeCategory_id));
		payeeCategory.click();
		Select s = new Select(payeeCategory);

		List<WebElement> l = s.getOptions();
		List<String> l_payeeCategory = new ArrayList<String>();

		for (WebElement a : l) {
			if (!a.getText().equals("Select"))
				l_payeeCategory.add(a.getAttribute("value"));

		}
		System.out.println("All the category" + l_payeeCategory);
		*/
        List<String> l_payeeCategory = getDropDownValue(payeeCategory_id);
        return l_payeeCategory;

    }

    //---------------------------------------------------------------------------------------
    //----------------------- Data Provider methods
    //---------------------------------------------------------------------------------------

    public List<String> getAllPayeeProviderValue() {
        payeeProviderDDown.click();
        Select s = new Select(payeeProviderDDown);

        List<WebElement> l = s.getOptions();
        List<String> l_payeeProvider = new ArrayList<String>();

        for (WebElement a : l) {
            if (!a.getText().equals("Select"))
                l_payeeProvider.add(a.getAttribute("value"));

        }
        System.out.println("All the Providers" + l_payeeProvider);
        return l_payeeProvider;

    }

    public List<String> getAllPayeeWalletTypeValue() {
        payeeWalletTypeDDown.click();
        Select s = new Select(payeeWalletTypeDDown);

        List<WebElement> l = s.getOptions();
        List<String> l_payeeWalletType = new ArrayList<String>();

        for (WebElement a : l) {
            if (!a.getText().equals("Select"))
                l_payeeWalletType.add(a.getAttribute("value"));

        }
        System.out.println("All the Providers" + l_payeeWalletType);
        return l_payeeWalletType;

    }

    public void clickSubmitButton() {
        submitBtn.click();
    }

    public List<String> getDropDownValue(String id) {
        //String contents = (String)((JavascriptExecutor)driver).executeScript("return document.getElementById('sel').innerHTML;", el);

        String js = "var dn = document.getElementById('" + id + "');var str ='';for(i=0;i<dn.length;i++){tmp_s = dn.options[i].text; if(tmp_s!='Select')str+= dn.options[i].value+'|';}console.log(str);return str;";
        String contents = (String) ((JavascriptExecutor) driver).executeScript(js);
        System.out.println("got  string " + contents);
        String[] splitted = contents.split("\\|");

        for (String a : splitted) {
            System.out.println("splitted string " + a);
        }

        return Arrays.asList(splitted);

    }

    /**
     * return combination of
     * Payee Provider
     * Payee Wallet Type*
     *
     * @return
     * @throws InterruptedException
     */
    public List<String> getCombination_payeeProviderAndWallet() throws InterruptedException {
        List<String> combination = new ArrayList<>();

        List<String> pp = getAllPayeeProviderValue();

        for (String s_pp : pp) {
            payeeProviderSelectValue(s_pp);
            Thread.sleep(time);
            List<String> pwt = getAllPayeeWalletTypeValue();

            for (String s_pwt : pwt) {
                //payeeWalletType_SelectValue(s_pwt);
                String data = s_pp + "|" + s_pwt;
                combination.add(data);
            }

        }
        return combination;
    }
//-------- CLASS END
}
