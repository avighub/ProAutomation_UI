package framework.pageObjects.bulkUserDeletion;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.PageInit;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.NumberConstants;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.io.File;
import java.util.Scanner;

public class BulkUserDeletion_pg1 extends PageInit {

    @FindBy(xpath = "//form[@id='bulkUserDel_delete']/table/tbody/tr[1]/td/a/img")
    private WebElement downloadImg;
    @FindBy(name = "bulkUploadFile")
    private WebElement chooseFileBtn;
    @FindBy(id = "submitButton")
    private WebElement submit;
    @FindBy(id = "//form[@id='bulkUserDel_delete']/table/tbody/tr[5]/td/a/img")
    private WebElement logImg;

    @FindBy(xpath = "//a[@href='javaScript:openLogFile()']")
    private WebElement logFileDownloadLink;

    public BulkUserDeletion_pg1(ExtentTest t1) {
        super(t1);
    }

    public static BulkUserDeletion_pg1 init(ExtentTest t1) {
        return new BulkUserDeletion_pg1(t1);
    }

    public void navBulkUserDelete() throws Exception {
        fl.contentFrame();
        driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
        fl.clickLink("BULK_DEL_ALL");
    }

    public BulkUserDeletion_pg1 downloadBulkTemplate() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_USER_DELETION);
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_USER_DELETION); // is hardcoded can be Generic TODO
        Utils.putThreadSleep(NumberConstants.SLEEP_3000);
        downloadImg.click();
        pageInfo.info("Click On button Download");
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Utils.putThreadSleep(NumberConstants.SLEEP_3000);
        Utils.closeUntitledWindows();
        return this;
    }

    public void setUploadFile(String path) {
        this.chooseFileBtn.sendKeys(new CharSequence[]{path});
        pageInfo.info("Set File for upload at Path - " + path);
    }

    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }


    public BulkUserDeletion_pg1 downloadLogFile() throws Exception {
        clickOnElement(logFileDownloadLink, "Submit Button");
        Utils.attachFileAsExtentLog(FilePath.dirFileDownloads+Utils.getLatestFilefromDir(FilePath.dirFileDownloads),pageInfo);
        return this;
    }
}
