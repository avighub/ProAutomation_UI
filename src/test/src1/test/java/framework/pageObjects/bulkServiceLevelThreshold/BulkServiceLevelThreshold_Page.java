package framework.pageObjects.bulkServiceLevelThreshold;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BulkServiceLevelThreshold_Page extends PageInit {

    @FindBy(xpath = "//a[@href='javaScript:openSheet()']")
    private WebElement iconImg;

    public BulkServiceLevelThreshold_Page(ExtentTest t1) {
        super(t1);
    }

    public void clickOnDownloadLink() throws Exception {
        iconImg.click();
        pageInfo.info("Click on DownloadLink");
        Thread.sleep(2000);
    }

    public void navigateToLink() throws Exception {
        navigateTo("NEWSVCCHRG_ALL", "NEWSVCCHRG_ALL", "Bulk ServiceLevel Threshold");
        Thread.sleep(2000);
    }
}
