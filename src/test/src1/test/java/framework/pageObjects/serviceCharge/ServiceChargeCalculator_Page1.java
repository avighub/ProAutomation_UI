package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by surya.dhal on 10/24/2018.
 */
public class ServiceChargeCalculator_Page1 extends PageInit {

    @FindBy(id = "selectMFSSenderProviderId")
    WebElement senderProviderDDown;
    @FindBy(id = "senderPaymentInstrumentId")
    WebElement senderInstrumentDDown;
    @FindBy(id = "senderLinkedBanksOrWalletTypesId")
    WebElement senderInstrumentTypeDDown;
    @FindBy(id = "selectMFSReceiverProviderId")
    WebElement receiverProviderDDown;
    @FindBy(id = "receiverPaymentInstrumentId")
    WebElement receiverInstrumentDDown;
    @FindBy(id = "receiverLinkedBanksOrWalletTypesId")
    WebElement receiverInstrumentTypeDDown;
    @FindBy(id = "paymentTypeServicesId")
    WebElement serviceTypeDDown;
    @FindBy(name = "reqValue")
    WebElement txnAmountTBox;
    @FindBy(name = "submitCal")
    WebElement submitButton;
    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;
    @FindBy(className = "actionMessage")
    WebElement ActionMessage;
    @FindBy(id = "payeeProfileGradeCodeId")
    private WebElement payeeGradeDDown;
    @FindBy(id = "payerProfileGradeCodeId")
    private WebElement payerGradeDDown;

    public ServiceChargeCalculator_Page1(ExtentTest t1) {
        super(t1);
    }

    public static ServiceChargeCalculator_Page1 init(ExtentTest t1) {
        return new ServiceChargeCalculator_Page1(t1);
    }

    /**
     * Navigate to Add Service Charge
     *
     * @throws Exception
     */
    public ServiceChargeCalculator_Page1 navigateCalculateServiceCharge() throws Exception {
        navigateTo("SERVCHARG_ALL", "SERVCHARG_SVC_CALC", "Calculate Service Charge");
        return this;
    }

    public ServiceChargeCalculator_Page1 selectSenderProvider(String provider) throws Exception {
        selectVisibleText(senderProviderDDown, provider, "SenderProvider");
        return this;
    }

    public ServiceChargeCalculator_Page1 selectSenderInstrument(String instrumentName) throws InterruptedException {
        selectVisibleText(senderInstrumentDDown, instrumentName, "SenderInstrument");
        return this;
    }

    public ServiceChargeCalculator_Page1 selectSenderInstrumentType(String instrumentType) throws InterruptedException {
        selectVisibleText(senderInstrumentTypeDDown, instrumentType, "SenderInstrumentType");
        return this;
    }

    /**
     * This method is used to select Receiver provider.
     *
     * @param provider
     * @return
     */
    public ServiceChargeCalculator_Page1 selectReceiverProvider(String provider) {
        selectVisibleText(receiverProviderDDown, provider, "ReceiverProvider");
        return this;
    }

    /**
     * This method is used to select Instrument of Receiver.
     *
     * @param instrumentName
     * @return
     */
    public ServiceChargeCalculator_Page1 selectReceiverInstrument(String instrumentName) {
        selectVisibleText(receiverInstrumentDDown, instrumentName, "ReceiverInstrument");
        return this;
    }

    /**
     * This method is used to select Receiver Instrument type.
     *
     * @param instrumentType
     * @return
     */
    public ServiceChargeCalculator_Page1 selectReceiverInstrumentType(String instrumentType) {
        selectVisibleText(receiverInstrumentTypeDDown, instrumentType, "ReceiverInstrumentType");
        return this;
    }

    /**
     * This methood is used to select Service Type
     *
     * @param service
     * @return
     * @throws Exception
     */
    public ServiceChargeCalculator_Page1 selectServiceType(String service) throws Exception {
        Thread.sleep(1200);
        selectValue(serviceTypeDDown, service, "ServiceType");
        return this;
    }

    /**
     * This method is used to set Transaction Amount.
     *
     * @param amount
     * @return
     * @throws Exception
     */
    public ServiceChargeCalculator_Page1 setTransactionAmount(String amount) throws Exception {
        Thread.sleep(1200);
        setText(txnAmountTBox, amount, "Transaction Amount");
        return this;
    }

    /**
     * This method is used  to click on Submit Button..
     *
     * @return
     */
    public ServiceChargeCalculator_Page1 clickSubmit() {
        clickOnElement(submitButton, "Submit");
        return new ServiceChargeCalculator_Page1(pageInfo);
    }

    public void selectSenderGrade(String gradeCode) {
        selectValue(payeeGradeDDown, gradeCode, "Payee Grade");
    }

    public void selectReceiverGrade(String gradeCode) {
        selectValue(payerGradeDDown, gradeCode, "Payer Grade");
    }
}
