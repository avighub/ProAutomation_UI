package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ServiceChargeApprove_Pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(className = "actionMessage")
    WebElement ActionMessage;
    // Page objects
    @FindBy(id = "profileApp_saveApproval_button_confirm")
    private WebElement Confirm;
    @FindBy(xpath = "//div[@id='content']/table/tbody/tr[@class = 'tableData'][1]/td[2]")
    private WebElement listOfNFS;

    public static ServiceChargeApprove_Pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        ServiceChargeApprove_Pg1 page = PageFactory.initElements(driver, ServiceChargeApprove_Pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    /**
     * Approve Service Charge
     */
    public void clickConfirm() {
        Confirm.click();
        pageInfo.info("Click on Confirm");
    }

    /**
     * Get Error message
     *
     * @throws Exception
     */
    public String getActionMessage() throws Exception {
        try {
            Thread.sleep(1000);
            return ActionMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    //===========================used in p1 tc=========================//

    /**
     * Navigate to Add Service Charge
     *
     * @throws Exception
     */
    public void navApproveServiceCharge() throws Exception {
        fl.leftNavigation("SERVCHARG_ALL", "SERVCHARG_SVC_APP");
        pageInfo.info("Navigate to Approve Service Charge Page");
    }

    public WebElement isNFSPresentForApproval() {
        return listOfNFS;
    }

    public WebElement isActionButtonsPresent(String actionName) {
        return driver.findElement(By.xpath("//tr[@class = 'tableData']/td/a[text() = '" + actionName + "']"));
    }
}
