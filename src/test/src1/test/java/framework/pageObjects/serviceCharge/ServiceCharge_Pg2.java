package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ServiceCharge_Pg2 extends PageInit {
    // Page objects
    @FindBy(id = "payerProfileGradeId")
    public WebElement PayerGrade;
    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;
    @FindBy(id = "payeeProfileGradeId")
    private WebElement PayeeGrade;
    @FindBy(id = "multipleOf")
    private WebElement MultipleOf;
    @FindBy(id = "confirmPopAdd_refreshServiceChargeProfile_servChrgMap_minTransferValue")
    private WebElement MinTxAmount;
    @FindBy(id = "confirmPopAdd_refreshServiceChargeProfile_servChrgMap_maxTransferValue")
    private WebElement MaxTxAmount;
    @FindBy(id = "payingEntityID")
    private WebElement PayingEntityID;
    @FindBy(id = "crID")
    private WebElement CreditedEntity;
    @FindBy(id = "confirmPopAdd_refreshServiceChargeProfile_servChrgDetails_minFixedServiceCharge")
    private WebElement MinFixedSCharge;
    @FindBy(id = "confirmPopAdd_refreshServiceChargeProfile_servChrgDetails_maxFixedServiceCharge")
    private WebElement MaxFixedSCharge;
    @FindBy(id = "minperText")
    private WebElement MinSChargePCT;
    @FindBy(id = "confirmPopAdd_refreshServiceChargeProfile_servChrgDetails_maxPctServiceCharge")
    private WebElement MaxSChargePCT;
    @FindBy(id = "confirmPopAdd_refreshServiceChargeProfile_endRangeAsString")
    private WebElement ToRange;
    @FindBy(id = "confirmPopAdd_refreshServiceChargeProfile_servChargePerList")
    private WebElement ServiceChargePCT;
    @FindBy(name = "payerCommCatList")
    private WebElement PayerCommCatList;
    @FindBy(name = "payeeCommCatList")
    private WebElement PayeeComMCatList;
    @FindBy(name = "addDetails")
    private WebElement Next;

    public ServiceCharge_Pg2(ExtentTest t1) {
        super(t1);
    }

    public static ServiceCharge_Pg2 init(ExtentTest t1) {
        return new ServiceCharge_Pg2(t1);
    }

    /**
     * Get Error message
     *
     * @throws Exception
     */
    public String getErrorMessage() throws Exception {
        try {
            Thread.sleep(1200);
            return ErrorMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    /**
     * Select Payer Grade
     *
     * @param payerGrade
     */
    public void selectSenderGrade(String payerGrade) throws Exception {
        Thread.sleep(2500);
        selectVisibleText(PayerGrade, payerGrade, "PayerGrade");
    }

    /**
     * Select Payee Grade
     *
     * @param payeeGrade
     * @throws Exception
     */

    public void selectReceiverGrade(String payeeGrade) throws Exception {
        Thread.sleep(2500);
        selectVisibleText(PayeeGrade, payeeGrade, "PayeeGrade");
    }

    /**
     * set multiple of transasction amount
     *
     * @param text
     */
    public void setMultipleOfTransctionAmount(String text) {
        setText(MultipleOf, text, "MultipleOf");
    }

    /**
     * Set the minium transaction Amount
     *
     * @param text
     * @throws Exception
     */
    public void setMinTransctionAmount(String text) throws Exception {
        setText(MinTxAmount, text, "MinTxAmount");
    }

    /**
     * set the max transaction amount
     *
     * @param text
     */
    public void setMaxTransctionAmount(String text) {
        setText(MaxTxAmount, text, "MaxTxAmount");
    }

    /**
     * get set the paying entity
     *
     * @param text
     */
    public void selectPayingEntity(String text) throws Exception {
        Thread.sleep(2500);
        selectVisibleText(PayingEntityID, text, "PayingEntityID");
    }

    public void selectPayingEntityByValue(String text) throws Exception {
        Thread.sleep(2500);
        selectValue(PayingEntityID, text, "PayingEntityID");
    }

    /**
     * get set the credited entity
     *
     * @param text
     * @throws Exception
     */
    public void selectCreditedEntity(String text) throws Exception {
        Thread.sleep(2500);
        selectVisibleText(CreditedEntity, text, "CreditedEntity");
    }

    /**
     * set the min fixed service charge
     *
     * @param text
     */
    public void setminFixedServiceCharged(String text) {
        setText(MinFixedSCharge, text, "MinFixedSCharge");
    }

    /**
     * set min service charge %
     *
     * @param text
     */
    public void setminServiceChargedPCT(String text) {
        setText(MinSChargePCT, text, "MinSChargePCT");
    }

    /**
     * set the max fixed service charged
     *
     * @param text
     */
    public void setmaxFixedServiceCharged(String text) {
        setText(MaxFixedSCharge, text, "MaxFixedSCharge");
    }

    /**
     * set the max fixed service charged %
     *
     * @param text
     */
    public void setMaxServiceChargedPCT(String text) {
        setText(MaxSChargePCT, text, "MaxSChargePCT");
    }

    /**
     * Select To Range
     *
     * @param text
     */
    public void setServiceToRange(String text) {
        setText(ToRange, text, "ToRange");
    }

    /**
     * Set Service Charge PCT
     *
     * @param text
     */
    public void setServiceChargePCT(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_servChargePerList']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("select service charge pct:" + text);
    }

    /**
     * Set Service Charge Fixed
     *
     * @param text
     */
    public void setServiceChargeFixed(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_servChargeFixList']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("select service charge Fixed:" + text);
    }

    /**
     * Set the commission charge
     *
     * @param text
     */
    public void setMinFixedCommission(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_commDetails_minFixedCommission']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("set min fixed commission:" + text);
    }

    public void setMinCommissionPCT(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_commDetails_minPctCommission']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("set min commission PCT:" + text);
    }

    public void setMaxfixedCommission(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_commDetails_maxFixedCommission']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("set max fixed commission:" + text);
    }

    public void setMaxCommissionPCT(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_commDetails_maxPctCommission']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("set max commission PCT:" + text);
    }

    /**
     * Set the commission slab
     *
     * @param text
     */
    public void setCommissionFromRange(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_commFromRange']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("set commission from range:" + text);

    }

    public void setCommissionToRange(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_commToRange']")).get(0);
        se.sendKeys(text);
        pageInfo.info("set commission to range:" + text);
    }

    public void selectCommissionPayerCategory(String text) throws Exception {
        Thread.sleep(1500);
        selectVisibleText(PayerCommCatList, text, "PayerCommCatList");
    }

    public void selectCommissionPayeeCategory(String text) throws Exception {
        Thread.sleep(1500);
        selectVisibleText(PayeeComMCatList, text, "PayeeComMCatList");
    }

    public void selectCommissionPayeeCategoryByValue(String text) throws Exception {
        Thread.sleep(1500);
        selectValue(PayeeComMCatList, text, "PayeeComMCatList");
    }

    public void setCommissionRangePCT(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_commisionPerList']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("set commission range PCT:" + text);
    }

    public void setCommissionRangeFixed(String text) {
        WebElement se = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_commisionFixList']"))
                .get(0);
        se.sendKeys(text);
        pageInfo.info("select commission range fixed:" + text);
    }

    public void clickNextButton() {
        clickOnElement(Next, "Next");
    }

    /**
     * checks if the page 2 has successfully executed
     *
     * @return
     */
    public boolean isSucess() {
        boolean result = false;
        result = driver.findElements(By.xpath("//*[@id='confirmPopAdd_refreshServiceChargeProfile_addDetails']")).size() > 0;
        return !result; //flip if result true i.e element exit in page
    }
}
