package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.ServiceCharge;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Services;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ServiceCharge_Pg3 extends PageInit {
    // Page objects
    @FindBy(name = "addDetails")
    public WebElement btnAddDetail;
    @FindBy(id = "confirmadd_confirmAdd_button_save")
    public WebElement Save;
    @FindBy(id = "confirmadd_save_button_confirm")
    private WebElement Confirm;

    public ServiceCharge_Pg3(ExtentTest t1) {
        super(t1);
    }

    /**
     * Click btnAddDetail
     */
    public void addDetails() throws InterruptedException {
        Thread.sleep(2000);
        wait.until(ExpectedConditions.elementToBeClickable(btnAddDetail)).click();
        pageInfo.info("Click on Add");
    }

    public void saveServiceCharge() {
        wait.until(ExpectedConditions.elementToBeClickable(Save)).click();
        pageInfo.info("Click on Save");
    }

    public void confirmServiceCharge() {
        wait.until(ExpectedConditions.elementToBeClickable(Confirm)).click();
        pageInfo.info("Click on Confirm");
    }

    /**
     * Fill tax Page
     */
    public void fillTaxPage(ServiceCharge sCharge) throws InterruptedException {

        Utils.putThreadSleep(5000);

        List<WebElement> taxAmtPct = driver.findElements(By.xpath("//input[contains(@name,'TaxPerList')]"));
        List<WebElement> taxAmtFixed = driver.findElements(By.xpath("//input[contains(@name,'TaxFixedList')]"));

        String constantVal = (sCharge.ServiceInfo.ServcieType.equals(Services.ACQFEE)) ? "0" : "1";
        // get all the checkbox
        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));

        for (WebElement we : checkboxes) {
            Thread.sleep(500);
            we.click();
        }

        for (WebElement we : taxAmtPct) {
            we = getElementAfterWaitForDisplay(we);
            if (sCharge.ServiceInfo.ServcieType.equals(Services.ACQFEE)) {
                pageInfo.info("Not Filling Tax PCT for Service Acquisition Fee");
            } else {
                setTextUsingJs(we, constantVal, "Tax Percentage Slab");
            }
        }
        for (WebElement we : taxAmtFixed) {
            setTextUsingJs(we, constantVal, "Tax Fixed Slab");
        }
    }


    public WebElement getElementAfterWaitForDisplay(final WebElement we) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(5, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);

        return wait.until(ExpectedConditions.visibilityOf(we));
    }

}
