package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rahul.rana on 6/22/2017.
 */
public class ServiceChargeModify_Pg4 extends PageInit {

    @FindBy(name = "txnAmtTaxPerList")
    private WebElement txtTxnAmtPerList;

    @FindBy(name = "servChTaxPerList")
    private WebElement txtServiceChargePerList;

    @FindBy(name = "commTaxPerList")
    private WebElement txtCommissionTaxPerList;

    @FindBy(id = "confirmPopAdd_modifyTax_modifyDetails")
    private WebElement btnModify;

    @FindBy(id = "confirmPopAdd_modifyTax_back")
    private WebElement btnBack;

    @FindBy(name = "cancel")
    private WebElement btnCancel;

    @FindBy(id = "confirmModify_confirmModify_button_save")
    private WebElement btnSaveModify;
    @FindBy(name = "button.confirm")
    private WebElement btnConfirmModify;

    public ServiceChargeModify_Pg4(ExtentTest t1) {
        super(t1);
    }

    public static ServiceChargeModify_Pg4 init(ExtentTest t1) {
        return new ServiceChargeModify_Pg4(t1);
    }

    public void clickSaveModify() {
        clickOnElement(btnSaveModify, "Save Modify");
    }

    public void clickConfirmModify() {
        clickOnElement(getBtnConfirmModify(), "Confirm Modify");
    }

    public WebElement getBtnSaveModify() {
        return btnSaveModify;
    }

    public WebElement getBtnConfirmModify() {
        return btnConfirmModify;
    }

    public ServiceChargeModify_Pg4 clickModify() {
        clickOnElement(btnModify, "Modify");
        return this;
    }

    public WebElement getTxtTxnAmtPerList() {
        return txtTxnAmtPerList;
    }

    public WebElement getTxtServiceChargePerList() {
        return txtServiceChargePerList;
    }

    public WebElement getTxtCommissionTaxPerList() {
        return txtCommissionTaxPerList;
    }

    public WebElement getBtnModify() {
        return btnModify;
    }

    public WebElement getBtnBack() {
        return btnBack;
    }

    public WebElement getBtnCancel() {
        return btnCancel;
    }

}
