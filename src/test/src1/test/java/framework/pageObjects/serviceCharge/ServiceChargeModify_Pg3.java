package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rahul.rana on 6/22/2017.
 */
public class ServiceChargeModify_Pg3 extends PageInit {

    @FindBy(name = "servChrgDetails.payingEntityCode")
    private WebElement selPayingEntity;

    @FindBy(id = "crID")
    private WebElement selCreditEntity;

    @FindBy(name = "servChrgDetails.minPctServiceCharge")
    private WebElement txtMinServiceCharge;

    @FindBy(name = "servChrgDetails.maxPctServiceCharge")
    private WebElement txtMaxServiceCharge;

    @FindBy(name = "commDetails.minPctCommission")
    private WebElement txtMinPercentageCommission;

    @FindBy(name = "commDetails.maxPctCommission")
    private WebElement txtMaxPercentageCommission;

    @FindBy(id = "confirmPopAdd_refreshServiceChargeProfileEdit_modifyDetails")
    private WebElement btnModify;

    @FindBy(name = "cancel")
    private WebElement btnCancel;

    public ServiceChargeModify_Pg3(ExtentTest t1) {
        super(t1);
    }

    public static ServiceChargeModify_Pg3 init(ExtentTest t1) {
        return new ServiceChargeModify_Pg3(t1);
    }

    public ServiceChargeModify_Pg4 clickOnModify() {
        clickOnElement(btnModify, "Button Modify");
        return ServiceChargeModify_Pg4.init(pageInfo);
    }

    public WebElement getSelPayingEntity() {
        return selPayingEntity;
    }

    public WebElement getSelCreditEntity() {
        return selCreditEntity;
    }

    public WebElement getTxtMinServiceCharge() {
        return txtMinServiceCharge;
    }

    public WebElement getTxtMaxServiceCharge() {
        return txtMaxServiceCharge;
    }

    public WebElement getTxtMinPercentageCommission() {
        return txtMinPercentageCommission;
    }

    public WebElement getTxtMaxPercentageCommission() {
        return txtMaxPercentageCommission;
    }

    public WebElement getBtnModify() {
        return btnModify;
    }

    public WebElement getBtnCancel() {
        return btnCancel;
    }

}
