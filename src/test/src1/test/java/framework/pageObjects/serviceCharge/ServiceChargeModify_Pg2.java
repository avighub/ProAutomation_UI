package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rahul.rana on 6/22/2017.
 */
public class ServiceChargeModify_Pg2 extends PageInit {

    @FindBy(id = "confirmModify_confirmModify_servChrgMap_serviceChargeName")
    private WebElement txtServiceName;

    @FindBy(id = "confirmModify_confirmModify_servChrgMap_shortCode")
    private WebElement txtShortName;

    @FindBy(name = "dojo.applicableFromStr")
    private WebElement txtApplicableDate;

    @FindBy(xpath = "//img[contains(@onclick,'addCommissionSlabs')]")
    private WebElement imgEditCommission;

    public ServiceChargeModify_Pg2(ExtentTest t1) {
        super(t1);
    }

    public static ServiceChargeModify_Pg2 init(ExtentTest t1) {
        return new ServiceChargeModify_Pg2(t1);
    }

    public WebElement getTxtServiceName() {
        return txtServiceName;
    }

    public WebElement getTxtShortName() {
        return txtShortName;
    }

    public WebElement getTxtApplicableDate() {
        return txtApplicableDate;
    }

    public WebElement getImgEditCommission() {
        return imgEditCommission;
    }

    public ServiceChargeModify_Pg3 clickEditCommissionSlabs() {
        clickOnElement(imgEditCommission, "Add Commission Slab");
        return ServiceChargeModify_Pg3.init(pageInfo);
    }

}
