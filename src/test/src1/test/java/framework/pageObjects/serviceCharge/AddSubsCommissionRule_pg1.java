package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rahul.rana on 8/30/2017.
 */
public class AddSubsCommissionRule_pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    // Page objects
    @FindBy(id = "addSubsComRule_addSubsCommRule_commRuleName")
    WebElement txtRuleName;
    @FindBy(id = "profileName")
    WebElement txtCount;
    @FindBy(id = "shortCode")
    WebElement txtValidity;
    @FindBy(name = "roleList1")
    WebElement ceckAll;
    @FindBy(id = "addSubsComRule_addSubsCommRule_button_save")
    WebElement btnSave;

    public static AddSubsCommissionRule_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        AddSubsCommissionRule_pg1 page = PageFactory.initElements(driver, AddSubsCommissionRule_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void navAddSubsCommissionRule() throws Exception {
        fl.leftNavigation("SERVCHARG_ALL", "SERVCHARG_SVC_ADDSUBCOM");
        pageInfo.info("Navigate to Add Subscriber Commission rule");
    }

    public void setRuleName(String text) {
        txtRuleName.sendKeys(text);
        pageInfo.info("Set Subscriber Commission Rule Name - " + text);
    }

    public void setCount(String text) {
        txtCount.sendKeys(text);
        pageInfo.info("Set count - " + text);
    }

    public void setValidityDays(String text) {
        txtValidity.sendKeys(text);
        pageInfo.info("Set Validity Date - " + text);
    }

    public void clickCheckAll() {
        ceckAll.click();
        pageInfo.info("Check All Services");
    }

    public void clickSave() {
        btnSave.click();
        pageInfo.info("Check on Save");
    }


}
