package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResumeServiceCharge_Pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    // Page objects
    @FindBy(id = "suspend_listView_button_save")
    WebElement save;
    @FindBy(id = "suspend_saveSuspend_button_confirm")
    WebElement confirm;

    public static ResumeServiceCharge_Pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        ResumeServiceCharge_Pg1 page = PageFactory.initElements(driver, ResumeServiceCharge_Pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void navigatetoLink() throws Exception {
        try {
            fl.leftNavigation("SERVCHARG_ALL", "SERVCHARG_SVC_SUS");
            pageInfo.info("Navigate to Service Charge Resume Link");
        } catch (ElementNotFoundException e) {
            pageInfo.error("Link not found.");
        }
    }


    public void selectprofiletoresume(String text) {
        try {
            WebElement profile = driver.findElement(By.xpath("//*[contains(text(), '" + text + "')]/following-sibling::td/*[@type='checkbox']"));

            if (!profile.isSelected()) {
                profile.click();
                pageInfo.info("Click on Profile to Resume");
            }

        } catch (Exception e) {
            pageInfo.info("Profile not found");
        }


    }

    public Boolean clickonsave() {
        try {
            save.click();
            pageInfo.info("Click on Save Button");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void clickConfirm() {
        confirm.click();
        pageInfo.info("Click on Confirm Button");
    }
}
