package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by rahul.rana on 6/23/2017.
 */
public class ViewServiceCharge_Pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    // Page objects
    @FindBy(id = "confirmView_loadVersionList_profileName")
    WebElement ServiceName;
    @FindBy(name = "numberOfDays")
    WebElement NumberOfDay;
    @FindBy(id = "confirmView_loadVersionList_button_submit")
    WebElement Submit;
    @FindBy(id = "confirm_loadDetail_submit")
    WebElement Confirm;
    @FindBy(xpath = "//td/label[contains(text(),'Profile Name')]/following-sibling::td/label")
    WebElement ProfileName;
    @FindBy(xpath = "//td/label[contains(text(),'Last Days')]/following-sibling::td")
    WebElement LastDate;
    @FindBy(xpath = "//*[@id=\"content\"]/form/table/tbody/tr[2]/td[4]")
    WebElement ProfileCode;
    @FindBy(xpath = "//*[@id=\"content\"]/form/table/tbody/tr[5]/td[2]")
    WebElement ApplicableDate;

    public static ViewServiceCharge_Pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        ViewServiceCharge_Pg1 page = PageFactory.initElements(driver, ViewServiceCharge_Pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    /**
     * Navigate to Add Service Charge
     *
     * @throws Exception
     */
    public void navAddServiceCharge() throws Exception {
        fl.leftNavigation("SERVCHARG_ALL", "SERVCHARG_SVC_VIEW");
        pageInfo.info("Navigate to View Service Charge Page");
    }

    /**
     * Select Service Name
     *
     * @param sName
     */
    public void selectServiceName(String sName) {
        Select sel = new Select(ServiceName);
        sel.selectByVisibleText(sName);
        pageInfo.info("Select Service Name: " + sName);
    }

    public void selectServiceNameByIndex() {
        Select sel = new Select(ServiceName);
        sel.selectByIndex(1);
        pageInfo.info("Select Service Name ");
    }

    public void setNumberOfDays(String days) {
        NumberOfDay.sendKeys(days);
        pageInfo.info("Set number of days: " + days);
    }

    public void clickSubmit() {
        Submit.click();
        pageInfo.info("Click On Submit");
    }

    public void clickConfirm() {
        Confirm.click();
        pageInfo.info("Click On Confirm");
    }

    public String fetchProfileName() {
        String name = ProfileName.getText();
        pageInfo.info("Profile Name : " + name);
        return name;
    }

    public String fetchLastDate() {
        String name = LastDate.getText();
        pageInfo.info("Last Date : " + name);
        return name;
    }

    public String fetchProfileCode() {
        String name = ProfileCode.getText();
        pageInfo.info("Profile Code : " + name);
        return name;
    }

    public String fetchApplicableDate() {
        String name = ApplicableDate.getText();
        pageInfo.info("Applicable Date : " + name);
        return name;
    }

    /**
     * Select the radio with max version
     * TODO - can be more specific
     */
    public void selectLatestVersion() {
        List<WebElement> Chbk = driver.findElements(By.xpath("//input[@type='radio']"));
        for (WebElement we : Chbk) {
            we.click();
        }
    }


}
