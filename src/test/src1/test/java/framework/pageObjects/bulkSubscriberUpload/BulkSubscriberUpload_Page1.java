package framework.pageObjects.bulkSubscriberUpload;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by navin.pramanik on 25-Oct-17.
 */
public class BulkSubscriberUpload_Page1 extends PageInit {

    @FindBy(id = "uplFile")
    WebElement uploadFileWebElement;
    @FindBy(id = "submitButton")
    WebElement submitButton;
    @FindBy(xpath = "//*[@id='bulkSubReg_register']/table/tbody/tr[5]/td/a/img")
    WebElement imgLogDownload;

    public BulkSubscriberUpload_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navigateToBulkSubscriber() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBBUREG", "Subscriber Bulk Upload");
    }

    public void clickSubmit() {
        clickOnElement(submitButton, "Submit");
    }

    /*public void uploadFile(String fileName) throws Exception {
        clickOnElement(uploadFileWebElement, "Upload");
        Utils.uploadFile_01(fileName);
    }*/
    public void uploadFile(String fileName) {
        uploadFileWebElement.sendKeys(fileName);
        pageInfo.info("Uploading file :" + fileName);
    }

    /*public void downloadErrorLogs() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_SUBS_REGISTER);
        try {
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_SUBS_REGISTER);
            imgLogDownload.click();
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Click On image Download template");
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Thread.sleep(2000);
        } finally {
            Utils.closeUntitledWindows();
        }

    }*/
    public void downloadErrorLogs() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_SUBS_REGISTER);
        try {
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_SUBS_REGISTER);
            imgLogDownload.click();
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Click On image Download template");
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Thread.sleep(2000);
            Utils.attachFileAsExtentLog(FilePath.dirFileDownloads+Utils.getLatestFilefromDir(FilePath.dirFileDownloads),pageInfo);
        } finally {
            Utils.closeUntitledWindows();
        }
    }

}
