package framework.pageObjects.nonFinancialServiceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Dalia on 29-05-2017.
 */
public class NonFinancialServiceCharge_Page4 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "confirmAddNon_confirmPageAddNFT_addDetails")
    public WebElement Add;
    @FindBy(id = "_addDetails")
    public WebElement Confirm;
    @FindBy(id = "confirmAddNon_confirmPageAddNFT_addDetails")
    WebElement btnAddServiceCharge;
    @FindBy(id = "_addDetails")
    WebElement btnConfirm;
    // Page objects
    @FindBy(id = "check0")
    WebElement CheckBoxForTaxOnSC;
    @FindBy(id = "confirmAddNon_confirmPageAddNFT_srvChrgFixedAmtMap_fixedamt0")
    WebElement FixedTaxForSC;
    @FindBy(id = "confirmAddNon_confirmPageAddNFT_commnCheckBoxMap_commnCheck0")
    WebElement CheckBoxForTaxOnCom;
    @FindBy(id = "confirmAddNon_confirmPageAddNFT_fixedAmtMap_fixedAmt0")
    WebElement FixedTaxForCom;

    public static NonFinancialServiceCharge_Page4 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        NonFinancialServiceCharge_Page4 page = PageFactory.initElements(driver, NonFinancialServiceCharge_Page4.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public NonFinancialServiceCharge_Page4 clickOnAddServiceCharge() {
        btnAddServiceCharge.click();
        pageInfo.info("Click on Add Service Charge");
        return this;
    }

    public void clickOnConfirm() {
        btnConfirm.click();
        pageInfo.info("Click on Confirm");
    }

    public NonFinancialServiceCharge_Page4 fillAddTaxPage() {
        // get all the checkbox
        List<WebElement> Chbk = driver.findElements(By.xpath("//input[@type='checkbox']"));

        for (WebElement we : Chbk) {
            we.click();
        }

        List<WebElement> taxamtpct = driver.findElements(By.xpath("//input[@type='text']"));

        for (WebElement we : taxamtpct) {
            we.sendKeys("0");
        }
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        return this;
    }

    /**
     * Setting Tax on Service Charge
     *
     * @param text
     * @throws Exception
     */
    public void setFixedTaxForSC(String text) throws Exception {
        FixedTaxForSC.sendKeys(text);
        pageInfo.info("set fixed tax on service charge:" + text);
    }

    /**
     * Setting Tax Commission value
     *
     * @param text
     * @throws Exception
     */
    public void setFixedTaxForCom(String text) throws Exception {
        FixedTaxForCom.sendKeys(text);
        pageInfo.info("set fixed tax on commission:" + text);
    }
}
