package framework.pageObjects.nonFinancialServiceCharge;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class ModifyNFSC_Page5 extends PageInit {


    @FindBy(id = "check0")
    WebElement check;

    @FindBy(id = "check1")
    WebElement check1;

    @FindBy(id = "chargingMod_modifyConfirm_commnCheckBoxMap_commnCheck0")
    WebElement com_check;


    @FindBy(id = "chargingMod_modifyConfirm_srvChrgPerAmtMap_peramt0")
    WebElement senderST;

    @FindBy(id = "chargingMod_modifyConfirm_srvChrgFixedAmtMap_fixedamt0")
    WebElement senderFT;

    @FindBy(id = "chargingMod_modifyConfirm_srvChrgPerAmtMap_peramt1")
    WebElement reciverST;

    @FindBy(id = "chargingMod_modifyConfirm_srvChrgFixedAmtMap_fixedamt1")
    WebElement reciverFT;

    @FindBy(id = "chargingMod_modifyConfirm_percenAmtMap_percenAmt0")
    WebElement addST;

    @FindBy(id = "chargingMod_modifyConfirm_fixedAmtMap_fixedAmt0")
    WebElement addFT;


    @FindBy(id = "chargingMod_modifyConfirm_addDetails")
    WebElement add;

    @FindBy(id = "chargingMod_modifyConfirm_back")
    WebElement back;

    @FindBy(id = "chargingMod_modifyConfirm_modifyDetails")
    WebElement submit;


    public ModifyNFSC_Page5(ExtentTest t1) {
        super(t1);
    }

    public static ModifyNFSC_Page5 init(ExtentTest t1) {
        return new ModifyNFSC_Page5(t1);
    }

    public boolean add() {
        try {
            add.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkbutton1() {
        try {
            check.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkbutton2() {
        try {
            check1.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void submit_Click() {
        submit.click();
    }

    public ModifyNFSC_Page6 clickOnSubmit() {
        clickOnElement(submit, "Submit");
        return ModifyNFSC_Page6.init(pageInfo);
    }

    public boolean checkbutton3() {
        try {
            com_check.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean BACK() {
        try {
            back.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void checkall() {
        List<WebElement> el = driver.findElements(By.xpath("//input[@type='checkbox']"));

        for (WebElement e : el) {
            if (!e.isSelected()) {
                e.click();
            }

        }

    }

    public boolean entersenderFtax(String val) {
        try {
            senderFT.clear();
            senderFT.sendKeys(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean entersenderStax(String val) {
        try {
            senderST.clear();
            senderST.sendKeys(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean enterreciverFtax(String val) {
        try {
            reciverFT.clear();
            reciverFT.sendKeys(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean enterreciverStax(String val) {
        try {
            reciverST.clear();
            reciverST.sendKeys(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean entercommissionFtax(String val) {
        try {
            addFT.clear();
            addFT.sendKeys(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean entercommissionStax(String val) {
        try {
            addST.clear();
            addST.sendKeys(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}


	

