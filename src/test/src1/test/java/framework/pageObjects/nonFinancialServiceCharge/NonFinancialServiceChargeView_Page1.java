package framework.pageObjects.nonFinancialServiceCharge;


import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class NonFinancialServiceChargeView_Page1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "chargingView_loadVersionList_numberOfDays")
    WebElement noOfdaysTbox;
    @FindBy(id = "chargingView_loadVersionList_button_submit")
    WebElement submit;
    @FindBy(id = "chargingView_loadVersionList_profileName")
    WebElement profileNameDdown;
    @FindBy(id = "chargingView_loadNonFinancialDetail_submit")
    WebElement page2SubmitButton;

    public static NonFinancialServiceChargeView_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        NonFinancialServiceChargeView_Page1 page = PageFactory.initElements(driver, NonFinancialServiceChargeView_Page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void navigateToViewNFSC() throws Exception {
        fl.leftNavigation("NONFIN_ALL", "NONFIN_CHARGENON_VIEW");
        pageInfo.info("Navigating to View Non Financial Service Charge...");
    }


    public void selectProfile(String profile) {
        Select option = new Select(profileNameDdown);
        option.selectByVisibleText(profile);
        pageInfo.info("Selected Profile : " + profile);
    }

    public boolean profilename_SelectIndex(int index) {

        try {
            profileNameDdown.click();
            Select s_profilename = new Select(profileNameDdown);
            s_profilename.selectByIndex(index);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean noofdays(String val) {
        try {
            noOfdaysTbox.sendKeys(val);
            pageInfo.info("Setting No. of Days :" + val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void setNoOfdays(String days) {
        noOfdaysTbox.sendKeys(days);
        pageInfo.info("Setting No. of Days : " + days);
    }


    public boolean submit() {
        try {
            submit.click();
            pageInfo.info("Clicked on Submit button..");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void clickPage2Submit() {
        page2SubmitButton.click();
        pageInfo.info("Clicked on Submit button..");
    }

    public String fetchProfileName() {
        WebElement proName = driver.findElement(By.id("chargingView_loadVersionList_servChrgMap_serviceChargeName"));

        return proName.getText();
    }

    public String fetchProfileCode() {

        return null;
    }
}
