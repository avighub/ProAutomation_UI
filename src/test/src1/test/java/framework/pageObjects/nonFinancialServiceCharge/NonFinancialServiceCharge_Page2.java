package framework.pageObjects.nonFinancialServiceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by Dalia on 29-05-2017.
 */
public class NonFinancialServiceCharge_Page2 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(xpath = "//*[@id='confirmAddNon_goCommissionScreen_next']")
    WebElement btnNext;
    // Page objects
    @FindBy(name = "commnDetailsMap.commnAmt")
    WebElement txtCommissionAmount;
    @FindBy(name = "commnDetailsMap.commn1")
    WebElement CommissionAmount;
    @FindBy(id = "commnAmt")
    WebElement commAmount;
    @FindBy(id = "payee1")
    WebElement payee1;
    @FindBy(id = "payeeWallet1")
    WebElement payee1wallet;

    public static NonFinancialServiceCharge_Page2 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        NonFinancialServiceCharge_Page2 page = PageFactory.initElements(driver, NonFinancialServiceCharge_Page2.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public NonFinancialServiceCharge_Page3 clickNextToBonusSetup() {
        btnNext.click();
        pageInfo.info("Click on Next Button to go to Bonus Setup Page");
        return NonFinancialServiceCharge_Page3.init(pageInfo);
    }

    public NonFinancialServiceCharge_Page2 setCommisionValue(String commissionAmount) {
        txtCommissionAmount.clear();
        txtCommissionAmount.sendKeys(commissionAmount);
        pageInfo.info("Set The Commission value");
        return this;
    }

    public void setComissionAmount(String amount) {
        commAmount.clear();
        commAmount.sendKeys(amount);
        pageInfo.info("Set The Commission amount");
    }

    public NonFinancialServiceCharge_Page2 setCommisionamount(String text) {
        CommissionAmount.clear();
        CommissionAmount.sendKeys(text);
        pageInfo.info("Set The Commission amount");
        return this;
    }

    public void selectPayee(String payee) throws Exception {
        Select sel = new Select(payee1);
        fl.waitSelectOptionsSCharge(sel);
        sel.selectByVisibleText(payee);
        pageInfo.info("Select Category: " + payee);
    }

    public void selectPayeewallet(String payeewallet) throws Exception {
        payee1wallet.click();
        Select sel = new Select(payee1wallet);
        fl.waitSelectOptionsSCharge(sel);
        sel.selectByVisibleText(payeewallet);
        pageInfo.info("Select Category: " + payeewallet);
    }


}
