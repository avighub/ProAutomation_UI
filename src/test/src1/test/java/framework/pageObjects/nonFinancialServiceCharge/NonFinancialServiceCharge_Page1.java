package framework.pageObjects.nonFinancialServiceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


/**
 * Created by Dalia on 29-05-2017.
 */
public class NonFinancialServiceCharge_Page1  extends PageInit {
    // Page objects
    @FindBy(id = "selectCategoryId")
    WebElement CategoryName;
    @FindBy(id = "selectSenderGradeId")
    WebElement GradeName;
    @FindBy(id = "servCharge")
    WebElement ServiceCharge;
    @FindBy(id = "confirmAddNon_addDetails_next")
    WebElement Next;

    public NonFinancialServiceCharge_Page1(ExtentTest t1) {
        super(t1);
    }

    public static NonFinancialServiceCharge_Page1 init(ExtentTest t1) {
        return new NonFinancialServiceCharge_Page1(t1);
    }

    /**
     * Navigate to Add Non Financial Service Charge Page-I
     *
     * @throws Exception
     */
    public void navigateToAddNonFinancialServiceCharge() throws Exception {
        navigateTo("NONFIN_ALL", "NONFIN_CHARGENON_FIN", "Non Financial Service Charge");
    }

    /**
     * Select Sender Category Name
     *
     * @param category
     * @throws Exception
     */
    public void selectSenderCategoryName(String category) throws Exception {
        selectVisibleText(CategoryName, category, "Category");
    }

    /**
     * Select Sender Category Name
     *
     * @param grade
     * @throws Exception
     */
    public void selectSenderGradeName(String grade) throws Exception {
        selectVisibleText(GradeName, grade, "Grade Name");
    }

    /**
     * Click on next
     */
    public void clickNext() {
        clickOnElement(Next, "Next");
    }

    /**
     * Setting service charge value
     *
     * @param text
     * @throws Exception
     */
    public void setServiceCharge(String text) throws Exception {
        setText(ServiceCharge, text, "Service Charge");
    }


}
