package framework.pageObjects.nonFinancialServiceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifyNFSC_Page2 extends PageInit {


    @FindBy(id = "servCharge")
    WebElement service;
    @FindBy(name = "action:chargingMod_modifyCommissionScreen")
    WebElement next;
    @FindBy(id = "chargingMod_loadNonFinancialDetail_update")
    WebElement update;

    public ModifyNFSC_Page2(ExtentTest t1) {
        super(t1);
    }

    public static ModifyNFSC_Page2 init(ExtentTest t1) {
        return new ModifyNFSC_Page2(t1);
    }

    public boolean isPageTwoOpen() {
        return fl.elementIsDisplayed(service);
    }

    public ModifyNFSC_Page2 service(String val) {
        setText(service, val, "service");
        return this;
    }

    public ModifyNFSC_Page3 next() {
        clickOnElement(next, "Next");
        return ModifyNFSC_Page3.init(pageInfo);
    }


}
