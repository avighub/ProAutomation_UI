package framework.pageObjects.autoDebit;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AutoDebit_pg1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static AutoDebit_pg1 page;
    @FindBy(id = "msisdn")
    WebElement customerMSISDN;
    @FindBy(id = "msisdnId")
    WebElement customerMSISDNID;
    @FindBy(id = "enableAuto_confirmAutoDebit_amount")
    WebElement maxAmtForDebit;
    @FindBy(id = "enableAuto_confirmAutoDebit_button_submit")
    WebElement submitBtn;
    @FindBy(xpath = "//select[@name ='providerName']")
    WebElement provider;
    @FindBy(id = "accountNumberSelected")
    WebElement accountNumber;
    @FindBy(id = "billerTypeSelected")
    WebElement benificiary;
    @FindBy(id = "beneficiarySel")
    WebElement benificiary1;
    @FindBy(id = "disableAuto_disableconfirm_button_submit")
    WebElement disableconfirmBtn;

    public static AutoDebit_pg1 init(ExtentTest t1) throws Exception {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        page = PageFactory.initElements(driver, AutoDebit_pg1.class);
        return page;
    }

    public AutoDebit_pg1 navEnableAutoDebit() throws Exception {
        fl.leftNavigation("AUTODEBIT_ALL", "AUTODEBIT_AUTO_EAD");
        pageInfo.info("Navigate to EnableAutoDebit");
        return this;
    }


    public AutoDebit_pg1 navDisableAutoDebit() throws Exception {
        fl.leftNavigation("AUTODEBIT_ALL", "AUTODEBIT_AUTO_DIS");
        pageInfo.info("Navigate to EnableAutoDebit");
        return this;
    }

    public void setCustomerMSISDN(String text) {
        customerMSISDN.sendKeys(text);
        pageInfo.info("Set Customer msisdn - " + text);
    }

    public void setMaxAmtForDebit(String text) {
        maxAmtForDebit.sendKeys(text);
        pageInfo.info("Set Max amount - " + text);
    }

    public void confirm() {
        submitBtn.click();
        pageInfo.info("Click On Confirm Category!");
    }

    public void selectProvider(String status) {
        provider.click();
        Select stat1 = new Select(provider);
        stat1.selectByVisibleText(status);
        pageInfo.info("Select Provider :" + status);
    }

    public void selectAccountNumber(String accno) {
        accountNumber.click();
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        Select stat2 = new Select(accountNumber);
        stat2.selectByValue(accno);
        pageInfo.info("Select account number :" + accno);
    }

    public void setCustomerMSISDNID(String text) {
        customerMSISDNID.sendKeys(text);
        pageInfo.info("Set CustomerMsisdnNumber - " + text);
    }

    public void selectbenificiary(String bCode) {
        benificiary.click();
        Select stat3 = new Select(benificiary);
        stat3.selectByValue(bCode);
        pageInfo.info("Select Status :benificiary" + bCode);
    }

    public void selectbenificiary1(String bCode) {
        benificiary1.click();
        Select stat3 = new Select(benificiary1);
        stat3.selectByValue(bCode);
        pageInfo.info("Select Status :benificiary" + bCode);
    }

    public void clickDisableconfirmBtn() {
        disableconfirmBtn.click();
        pageInfo.info("Click On disableconfirmBtn");
    }

}
