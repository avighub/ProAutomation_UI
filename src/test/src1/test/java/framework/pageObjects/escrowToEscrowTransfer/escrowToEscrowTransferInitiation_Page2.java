package framework.pageObjects.escrowToEscrowTransfer;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.math.BigDecimal;

public class escrowToEscrowTransferInitiation_Page2 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    //Page Objects
    @FindBy(id = "escrowInit_confirmInitiate_fromBankName")
    WebElement fromBank;
    @FindBy(xpath = "(//label[@id='escrowInit_confirmInitiate_toBankName'])")
    WebElement toBank;
    @FindBy(id = "escrowInit_confirmInitiate_frombalance")
    WebElement fromBalance;
    @FindBy(id = "escrowInit_confirmInitiate_tobalance")
    WebElement toBalance;
    @FindBy(id = "escrowInit_confirmInitiate__requestedQuantity")
    WebElement reqAmt;
    @FindBy(id = "escrowInit_confirmInitiate_refNumber")
    WebElement refNo;

    public static escrowToEscrowTransferInitiation_Page2 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        escrowToEscrowTransferInitiation_Page2 page = PageFactory.initElements(driver, escrowToEscrowTransferInitiation_Page2.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public String getFromBankName() throws Exception {
        pageInfo.info("From Bank is: " + fromBank.getText());
        return fromBank.getText();
    }

    public String getToBankName() throws Exception {
        pageInfo.info("From Bank is: " + toBank.getText());
        return toBank.getText();
    }

    public BigDecimal getFromBalance() throws Exception {
        pageInfo.info("From Balance is: " + fromBalance.getText());
        return new BigDecimal(fromBalance.getText());
    }

    public BigDecimal getToBalance() throws Exception {
        pageInfo.info("To Balance is: " + toBalance.getText());
        return new BigDecimal(toBalance.getText());
    }

    public String getReqAmt() throws Exception {
        pageInfo.info("Requested Amount is: " + reqAmt.getText());
        return reqAmt.getText();
    }

    public String getRefNo() throws Exception {
        pageInfo.info("Remarks is: " + refNo.getText());
        return refNo.getText();
    }

}
