package framework.components.shulka;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Predicate;
import com.jayway.jsonpath.internal.JsonContext;
import framework.util.JsonPathOperation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestJsonContext extends JsonContext {
    Map<String, Object> fields = new HashMap<>();

    @Override
    public DocumentContext set(JsonPath path, Object newValue) {
        return this.set(path.getPath(), newValue);
    }

    @Override
    public DocumentContext delete(JsonPath path) {
       this.fields.put(path.toString(), null);
       return this;
    }

    @Override
    public DocumentContext delete(String path, Predicate... filters) {
        return super.delete(path);
    }

    public Map<String, Object> getFields() {
        return fields;
    }

    @Override
    public DocumentContext set(String path, Object newValue, Predicate... filters) {
        fields.put(path, newValue);
        return this;
    }

    public void evaluate(List<JsonPathOperation> operationList) {
            TestJsonContext context = this;
        for (JsonPathOperation operation: operationList) {
            context = (TestJsonContext) operation.perform(context);
        }
    }
}
