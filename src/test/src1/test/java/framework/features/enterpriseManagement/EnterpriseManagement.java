/*
 *  COPYRIGHT: Comviva Technologies Pvt. Ltd.
 *  This software is the sole property of Comviva
 *  and is protected by copyright law and international
 *  treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of
 *  it may result in severe civil and criminal penalties
 *  and will be prosecuted to the maximum extent possible
 *  under the law. Comviva reserves all rights not
 *  expressly granted. You may not reverse engineer, decompile,
 *  or disassemble the software, except and only to the
 *  extent that such activity is expressly permitted
 *  by applicable law notwithstanding this limitation.
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 *  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 *  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 *  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 *  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *  Author Name: Prashant Kumar
 *  Date: 30-10-2017
 *  Purpose: Selenium Test Cases
 */
package framework.features.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.BulkBillPaymentCSV;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.dataEntity.BulkRechargeOtherCSV;
import framework.dataEntity.EnterpriseIndividualRecordRejectCSV;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.pageObjects.Enterprise_Management.EntBulkPayApproveReject_pg1;
import framework.pageObjects.Enterprise_Management.EntBulkPayDashBoard_Pg1;
import framework.pageObjects.Enterprise_Management.EntBulkPayRejectIndividualRecordPage;
import framework.pageObjects.Enterprise_Management.Enterprise_Management1;
import framework.pageObjects.bulkPayoutTool.BulkPayoutDashboard_page1;
import framework.pageObjects.enterpriseManagement.*;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class EnterpriseManagement {
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    public static EnterpriseManagement page;
    protected static WebDriver driver;
    private static ExtentTest pNode;
    private static WebDriverWait wait;

    public static EnterpriseManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        wait = new WebDriverWait(driver, 10);
        return new EnterpriseManagement();
    }


    /**
     * @param msisdn -MSISDN
     * @param Amount -Amount
     * @return FileName
     * @throws IOException - IO Exception
     */
    public static String generateBulkFile(String msisdn, String Amount) throws IOException {
        String csvFileHeader = "Mobile Number*,Amount*,First Name,Last Name,Id Number,Remarks";

        String filePath = "uploads/" + "salary" + DataFactory.getTimeStamp();

        File directory = new File("uploads");
        if (!directory.exists()) {
            directory.mkdir();
        }

        String fileName = filePath + ".csv";

        File f = new File(fileName);
        FileWriter fileWriter = new FileWriter(f);

        try {


            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append(msisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Amount);
            fileWriter.append("\n");

        } catch (Exception e) {
            System.err.println("Error in CsvFileWriter !!!");
            pNode.error("Error in CsvFileWriter !!!");
        } finally {
            fileWriter.flush();
            fileWriter.close();
        }
        return f.getAbsolutePath();
    }

    /**
     * @param msisdn - MSISDN
     * @param code   -Code
     * @return - Bulk FileName
     * @throws IOException -IOException
     */
    public static String generateBulkPayee(String msisdn, String code) throws IOException {

        String csvFileHeader = "Mobile Number*,Unique Code*";
        String filePath = "uploads/" + "BULK_PAYEE_" + DataFactory.getTimeStamp();

        File directory = new File("uploads");
        if (!directory.exists()) {
            directory.mkdir();
        }

        String fileName = filePath + ".csv";

        File f = new File(fileName);
        FileWriter fileWriter = new FileWriter(f);

        try {

            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append(msisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(code);
            fileWriter.append("\n");

        } catch (IOException io) {
            System.err.println("Error in CsvFileWriter !!!");
            pNode.error("Error in CsvFileWriter !!!");
        } finally {
            fileWriter.flush();
            fileWriter.close();
        }
        return f.getAbsolutePath();
    }

    /**
     * @param msisdn -MSISDN
     * @param amount -Amount
     * @return FileName
     * @throws IOException - IO Exception
     */
    public static String generateBulkFile(String providerId, String msisdn, String amount) throws IOException {
        String csvFileHeader = "MFS Provider*,Mobile Number*,Amount*,First Name,Last Name,National Id,ID Number";

        String filePath = "uploads/" + msisdn + DataFactory.getTimeStamp();

        File directory = new File("uploads");
        if (!directory.exists()) {
            directory.mkdir();
        }

        String fileName = filePath + ".csv";

        File f = new File(fileName);
        FileWriter fileWriter = new FileWriter(f);

        try {

            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);
            fileWriter.append(providerId);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(msisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append("\n");

        } catch (Exception e) {
            System.err.println("Error in CsvFileWriter !!!");
            pNode.error("Error in CsvFileWriter !!!");
        } finally {
            fileWriter.flush();
            fileWriter.close();
        }
        return f.getAbsolutePath();
    }

    /**
     * @param user -User
     * @return current instance
     * @throws Exception
     */
    public EnterpriseManagement addBulkPayeeBatch(User user, String uniqueCode) throws Exception {

        BatchBulkPayee p1 = new BatchBulkPayee(pNode);

        p1.navigateToLink()
                .uploadFile(generateBulkPayee(user.MSISDN, uniqueCode))
                .clickOnSubmit()
                .clickOnConfirm();

        String message = MessageReader.getDynamicMessage("employee.success.employeesCreated", "1");

        Assertion.verifyEqual(Assertion.getActionMessage(), message, "Add Bulk Payee Batch", pNode);

        return this;
    }

    public EnterpriseManagement viewBulkPayerAdmin(OperatorUser user) throws Exception {
        try {

            ViewBulkPayerAdmin viewPage = new ViewBulkPayerAdmin(pNode);

            viewPage.navigateToLink()
                    .selectType(Constants.BULK_PAYER_ADMIN)
                    .enterName(user.FirstName)
                    .clickOnSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(viewPage.getMSISDNLabelText(), user.MSISDN, "Verify MSISDN", pNode);
                Assertion.verifyEqual(viewPage.getLoginIdLabelText(), user.LoginId, "Verify LOGIN", pNode);
                Assertion.verifyEqual(viewPage.getNameLabelText(), user.FirstName, "Verify NAME", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public EnterpriseManagement viewSelfDetails(User user) throws Exception {
        try {
            EnterpriseManagementPage1 obj = EnterpriseManagementPage1.init(pNode).navigateToViewSelfdetails();
            Assertion.verifyEqual(obj.getMsisdnlabel(), user.MSISDN, "Verify MSISDN", pNode);
            Assertion.verifyEqual(obj.getLoginidlabel(), user.LoginId, "Verify LOGIN", pNode);
            Assertion.verifyEqual(obj.getlastnamelabellabel(), user.LastName, "Verify NAME", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Method to Add Bulk Payee
     *
     * @param value - value to Add
     * @return current instance of the Object
     * @throws Exception
     */
    public EnterpriseManagement addBulkPayee(String value) throws IOException {
        try {
            String ran = DataFactory.getRandomNumberAsString(5);
            EnterpriseManagementPage1.init(pNode).NavigateToLinkADD().EnterMsisdn(value).ClickOnSubmit();
            EnterpriseManagementPage2.init(pNode).EnterUniqueCode(ran).ClickOnSubmit().ClickOnConfirm();

            String message = MessageReader.getDynamicMessage("enterprise.label.addSuccess", ran);
            Assertion.verifyEqual(Assertion.getActionMessage(), message, "Add Bulk Payee", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Method to Add Bulk Payee
     *
     * @param msisdn - value to Add
     * @return current instance of the Object
     * @throws Exception
     */
    public EnterpriseManagement addBulkPayee(String msisdn, String uniqueCode) throws IOException {
        try {

            EnterpriseManagementPage1.init(pNode).
                    NavigateToLinkADD().
                    EnterMsisdn(msisdn).
                    ClickOnSubmit();

            String error = Assertion.checkErrorPresent();
            if (error != null) {
                if (Assertion.checkErrorMessageContain("employee.error.msisdnAssociated.Other", "Already associated subs", pNode)) {
                    pNode.info("Skipping the case as Subscriber already associated. Continue to next Test..");
                } else {
                    Assertion.verifyErrorMessageContain("employee.error.msisdnAssociated", "Already associated subs", pNode);
                    pNode.info("Skipping the case as Subscriber already associated. Continue to next Test..");
                }
            } else {
                EnterpriseManagementPage2.init(pNode)
                        .EnterUniqueCode(uniqueCode)
                        .ClickOnSubmit()
                        .ClickOnConfirm();
                Assertion.verifyActionMessageContain("enterprise.label.addSuccess",
                        "Verify Payee is successfully added", pNode, uniqueCode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param value -  value to Modify
     * @return current Object instance
     * @throws Exception
     */
    public EnterpriseManagement modifyBulkPayee(String value) throws Exception {
        try {
            String ran = DataFactory.getRandomNumberAsString(5);
            EnterpriseManagementPage1.init(pNode).NavigateToLinkModify().EntermodifyMsisdn(value).ClickOnmodifySubmit();
            EnterpriseManagementModifyPage.init(pNode).EnterUniqueCode(ran).ClickOnModify().ClickOnConfirm();
            String message = MessageReader.getDynamicMessage("enterprise.label.modifySuccess", DataFactory.getRandomNumberAsString(5));
            Assertion.verifyEqual(Assertion.getActionMessage(), message, "Add Bulk Payee", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param msisdn -  value to Modify
     * @return current Object instance
     * @throws Exception
     */
    public EnterpriseManagement modifyBulkPayee(String msisdn, String uniqueCode, String updated) throws Exception {
        try {

            EnterpriseManagementPage1.init(pNode).
                    NavigateToLinkModify().
                    EntermodifyMsisdn(msisdn).
                    ClickOnmodifySubmit();

            EnterpriseManagementModifyPage.init(pNode).
                    EnterUniqueCode(updated).
                    ClickOnModify().
                    ClickOnConfirm();

            if (ConfigInput.isAssert) {
                String message = MessageReader.getDynamicMessage("enterprise.label.modifySuccess", updated);
                Assertion.verifyDynamicActionMessageContain(message, "Modify Bulk Payee", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * @param msisdn - value to Delete
     * @return current Object instance
     * @throws Exception
     */
    public EnterpriseManagement deleteBulkPayee(String msisdn, String updatedCode) throws Exception {
        try {
            EnterpriseManagementPage1.
                    init(pNode).
                    NavigateToLinkDelete().
                    EnterDeleteMsisdn(msisdn).
                    ClickOnDeleteSubmit();

            EnterpriseManagementDeletePage.
                    init(pNode).
                    ClickOnSubmit();

            if (ConfigInput.isAssert) {
                String message = MessageReader.getDynamicMessage("enterprise.label.deleteSuccess", updatedCode);
                Assertion.verifyDynamicActionMessageContain(message, "Delete Bulk Payee", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * Approve Reject Bulk Pay
     *
     * @param enterpriseUser [User] enterprise user who has initited the transaction
     * @param batchId
     * @param amount
     * @param isApprove
     * @return
     * @throws Exception
     */
    public EnterpriseManagement approveRejectEnterpriseBulkPay(User enterpriseUser, String batchId, BigDecimal amount, boolean isApprove) throws Exception {
        Markup m = MarkupHelper.createLabel("approveRejectEnterpriseBulkPay", ExtentColor.BLUE);
        pNode.info(m);
        try {
            OperatorUser bpAdminAppL1 = DataFactory.getBulkPayerWithAccess("SAL_AP1", enterpriseUser);
            Login.init(pNode).login(bpAdminAppL1); // login as bpa level 1 approver

            EntBulkPayApproveReject_pg1 page = EntBulkPayApproveReject_pg1.init(pNode);
            page.NavigateToApp1Link();
            approveRejectPendingEntry(batchId, isApprove);

            // MAX_TRANSACTION_ALLOWED is default set as Enterprise Levell 2 approval, refer Entity User
            if (amount.compareTo(new BigDecimal(Constants.MAX_TRANSACTION_ALLOWED)) > 0) {
                OperatorUser bpAdminAppL2 = DataFactory.getBulkPayerWithAccess("SAL_AP2", enterpriseUser);
                Login.init(pNode).login(bpAdminAppL2); // login as bpa level 1 approver
                page.NavigateToApp2Link();
                approveRejectPendingEntry(batchId, isApprove);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    public void approveRejectPendingEntry(String batchId, boolean isApprove) throws IOException {
        try {
            EntBulkPayApproveReject_pg1 page = EntBulkPayApproveReject_pg1.init(pNode);
            page.clickSortNewest();

            // get all the requests and check for the bacthc Id
            List<WebElement> pendingReqs = driver.findElements(By.cssSelector(".pending-policy-name"));

            boolean isFound = false;
            for (WebElement elem : pendingReqs) {
                elem.findElement(By.xpath("parent::*")).click();
                Thread.sleep(1000);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    isFound = true;
                    // approve the entry
                    if (isApprove) {
                        page.clickApprove();
                    } else {
                        page.clickReject();
                    }
                    break;
                }
            }

            if (ConfigInput.isAssert && isFound) {
                String actual = page.getActionMessage();

                if (isApprove) {
                    Assertion.verifyMessageContain(actual, "bulkupload.success.approve", "Bulk Payout Approval", pNode);
                } else {
                    Assertion.verifyMessageContain(actual, "bulkupload.reject.approve", "Bulk Payout request has been rejected successfully", pNode, batchId);
                }
            }
            if (!isFound) {
                pNode.fail("Could not find the request for Bulk Batch Id:" + batchId);
                Utils.captureScreen(pNode);
                Assert.fail("Failed to approve Bulk Payout Request with id:" + batchId);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public String rejectIndividualRecordEnterpriseBulkPay(User enterpriseUser, String rejectFileName) throws Exception {
        Markup m = MarkupHelper.createLabel("entRejectRecord", ExtentColor.BLUE);
        ExtentTest t1 = pNode.createNode("rejectEntBulkPay",
                "Enterprise/bulk payer admin should be able to reject individual records within a batch for Enterprise bulk payment");

        pNode.info(m);
        try {
            Login.init(t1).login(enterpriseUser);
            EntBulkPayRejectIndividualRecordPage page = new EntBulkPayRejectIndividualRecordPage(pNode);
            page.NavigateToLink();

            if (page.uploadFile(rejectFileName)) {
                page.EnterRemarks("Test").
                        ClickOnSubmit();

                if (ConfigInput.isAssert) {
                    String actualMessage = page.getActionMessage();
                    if (actualMessage != null) {
                        if (Assertion.verifyMessageContain(actualMessage, "bulkupload.success.initiate", "Bulk Payout Initiate", pNode)) {
                            return actualMessage.split("batch ID:")[1].trim();
                        } else {
                            return null;
                        }


                    } else {
                        Utils.captureScreen(pNode);
//                        pNode.fail("Failed to initiate Bulk Payout for Service:" + service);
                        Assert.fail("Failed to Initiate Bulk Payout, exiting the test");
                    }
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return null;
    }


    //Creating this method because in DeleteBulkPayee() EnterMSISDN is used but EnterDeleteMsisdn needs to be used
    public EnterpriseManagement DeleteBulkpayee(String value) throws Exception {
        try {

            Enterprise_Management1.init(pNode).NavigateToLinkDelete().EnterDeleteMsisdn(value).ClickOnDeleteSubmit();
            EnterpriseManagementDeletePage.init(pNode).ClickOnSubmit();
            Assertion.assertActionMessageContain("enterprise.label.deleteSuccess.UI", "Delete Bulk payee", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Verify Bulk Payout Dashboard page
     *
     * @param serviceName
     * @param batchId
     * @param isApproved    - is the transaction is Approved
     * @param expectSuccess - true if transaction is expected to success
     * @return
     * @throws IOException
     */
    public String verifyBulkPayoutDashboard(String serviceName, String batchId, boolean isApproved, boolean expectSuccess) throws IOException {
        String strApproveReject = (expectSuccess) ? "Approved" : "Rejected";
        ArrayList<String> errors = new ArrayList<String>();
        String fileName = null;
        try {
            Markup m = MarkupHelper.createLabel("verifyBulkPayoutDashboard", ExtentColor.BLUE);
            pNode.info(m);

            EntBulkPayDashBoard_Pg1 page = EntBulkPayDashBoard_Pg1.init(pNode);

            page.navigateToLink();
            page.sortNewest();
            Thread.sleep(ConfigInput.smallWait);
            //UI got changed in new build
            // page.selectServiceType(serviceName);
            //Thread.sleep(ConfigInput.smallWait);
            page.selectStatusFilter(isApproved);

            // get all the requests and check for the batch Id
            List<WebElement> avaibaleEntry = driver.findElements(By.cssSelector(".pending-policy-list"));

            boolean isFound = false;
            for (WebElement elem : avaibaleEntry) {
                elem.click();
                Thread.sleep(Constants.MAX_WAIT_TIME);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    isFound = true;
                    // approve the entry
                    // todo, verify for the Success 1 and Failure 0  and vice versa fro Failure
                    break;
                }
            }


            if (isFound) {
                pNode.pass("Successfully found the entry for Bulk Batch Id:" + batchId + ". Approval status: " + strApproveReject);
                fileName = page.downloadStatusFile();
                checkStatusFile(expectSuccess);
            } else {
                Utils.captureScreen(pNode);
                Assertion.markAsFailure("Failed to find entry for Bulk Batch Id:" + batchId + ". Approval status: " + strApproveReject);
            }
            Utils.captureScreen(pNode);


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return fileName;
    }

    public void checkStatusFile(boolean status) throws Exception {
        Markup m = MarkupHelper.createLabel("checkStatusFile", ExtentColor.BLUE);
        pNode.info(m);
        String matchStr = (status) ? "SUCCEEDED" : "FAILED";
        Scanner s = null;
        try {
            String status_File = FilePath.dirFileDownloads + "/bulk-upload-" + BulkPayoutDashboard_page1.init(pNode).getbatchID() + ".csv";
            File file = new File(status_File);
            s = new Scanner(file);
            int lineIndex = 1;
            boolean success = true;
            String[] matcherArray = {matchStr};

            while (s.hasNextLine()) {
                String line = s.nextLine();

                if (lineIndex != 2) {
                    lineIndex++;
                    continue;
                }

                for (int i = 0; i < matcherArray.length; i++) {
                    if (line.contains(matcherArray[i])) {
                        pNode.pass("Found text as '" + matcherArray[i] + "'");
                    } else {
                        pNode.fail("Bulk Enterprise Pay failed '" + matcherArray[i] + "'");
                        break;
                    }
                }
            }

        } catch (IOException ioex) {
            System.out.println(ioex);
        } finally {
            s.close();
        }

    }

    public String downloadAndUpdateBulkBillPaymentCsv(List<BulkBillPaymentCSV> entryList) throws IOException {

        try {
            BulkPaymentPage1 p1 = new BulkPaymentPage1(pNode);

            p1.NavigateToLink();
            p1.serviceSelectText(Constants.BULK_PAYOUT_BILL_PAY_ENTERPRISE);

            if (p1.downloadTemplateBulkCSV(Constants.FILEPREFIX_BILL_PAY_ENTERPRISE)) {
                BufferedWriter out = new BufferedWriter(new FileWriter(FilePath.fileBulkBillPayTemplate, true));
                for (BulkBillPaymentCSV row : entryList) {
                    out.newLine();
                    out.write(row.serialNum + "," +
                            row.billerCode + "," +
                            row.amount + "," +
                            row.billAccountNum + "," +
                            row.providerId + "," +
                            row.remark);
                }

                out.newLine();
                out.close();
                return FilePath.fileBulkBillPayTemplate;
            } else {
                Assert.fail("Failed to download Template file");
                Assertion.markAsFailure("Failed to download Template file");
                return null;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public String downloadAndUpdateBulkRechargeOtherCsv(List<BulkRechargeOtherCSV> entryList) throws IOException {

        try {
            BulkPaymentPage1 p1 = new BulkPaymentPage1(pNode);

            p1.NavigateToLink();
            p1.serviceSelectText(Constants.BULK_PAYOUT_RECHARGE_OTHER_ENTERPRISE);

            if (p1.downloadTemplateBulkCSV(Constants.FILEPREFIX_RECHARGE_OTHER_ENTERPRISE)) {
                BufferedWriter out = new BufferedWriter(new FileWriter(FilePath.fileBulkRechargeOtherTemplate, true));
                for (BulkRechargeOtherCSV row : entryList) {
                    out.newLine();
                    out.write(row.serialNum + "," +
                            row.operatorId + "," +
                            row.amount + "," +
                            row.rechargeReceiverMsisdn + "," +
                            row.providerId + "," +
                            row.remark);
                }

                out.newLine();
                out.close();
                return FilePath.fileBulkRechargeOtherTemplate;
            } else {
                Assert.fail("Failed to download Template file");
                Assertion.markAsFailure("Failed to download Template file");
                return null;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    /**
     * @return
     * @throws IOException
     */
    public String downloadAndUpdateEnterprisePaymentCsv(List<BulkEnterprisePaymentCSV> entryList) throws IOException {

        try {
            BulkPaymentPage1 p1 = new BulkPaymentPage1(pNode);

            p1.NavigateToLink();
            p1.serviceSelectText(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT);

            if (p1.downloadTemplateBulkCSV(Constants.FILEPREFIX_ENT_PAYMENT)) {
                BufferedWriter out = new BufferedWriter(new FileWriter(FilePath.fileEntPaymentTemplate, true));
                for (BulkEnterprisePaymentCSV row : entryList) {
                    out.newLine();
                    out.write(row.providerId + "," +
                            row.msisdn + "," +
                            row.amount + "," +
                            row.firstName + "," +
                            row.lastName + "," +
                            row.nationalId + "," +
                            row.individualRemark);
                }

                out.newLine();
                out.close();
                return FilePath.fileEntPaymentTemplate;
            } else {
                Assert.fail("Failed to download Template file");
                Assertion.markAsFailure("Failed to download Template file");
                return null;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public String downloadAndUpdateEntRejectIndividualRecordCsv(List<EnterpriseIndividualRecordRejectCSV> entryList) throws IOException {

        try {
            EntBulkPayRejectIndividualRecordPage p1 = new EntBulkPayRejectIndividualRecordPage(pNode);
            p1.NavigateToLink();
//            page.serviceSelectText(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT);
            if (p1.downloadTemplateBulkCSV(Constants.FILEPREFIX_ENT_INDIVIDUAL_REC_REJECT)) {
                BufferedWriter out = new BufferedWriter(new FileWriter(FilePath.fileEntIndividualRecordReject, true));
                for (EnterpriseIndividualRecordRejectCSV row : entryList) {
                    out.newLine();
                    out.write(row.batchID + "," +
                            row.recordNum);
                }

                out.newLine();
                out.close();
                return FilePath.fileEntIndividualRecordReject;
            } else {
                Assert.fail("Failed to download Template file");
                Assertion.markAsFailure("Failed to download Template file");
                return null;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    /**
     * Perform Bulk Enterprise Pay
     *
     * @param service
     * @param fileName
     * @param isNameRequired       - false, if name is not mandatory or not applicable
     * @param isNationalIDRequired [subs.ExternalId]- false, if name is National ID mandatory or not applicable
     * @return
     * @throws Exception
     */

    public String initiateEnterpriseBulkPay(String service, String fileName, boolean isNameRequired, boolean isNationalIDRequired, String... batchRemark) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateEnterpriseBulkPay", ExtentColor.BLUE);
        pNode.info(m);
        String remark = (batchRemark.length > 0) ? batchRemark[0] : "automation remark";
        try {
            BulkPaymentPage1 p1 = new BulkPaymentPage1(pNode);
            p1.NavigateToLink();
            p1.serviceSelectText(service);

            if (isNameRequired)
                p1.checkNameAsMandatory();

            if (isNationalIDRequired)
                p1.checkNationalIdAsMandatory();

            if (p1.UploadFile(fileName)) {
                p1.EnterRemarks(remark).
                        ClickOnSubmit();

                if (!ConfigInput.isAssert)
                    return null;

                String actualMessage = p1.getActionMessage();
                if (actualMessage != null) {
                    if (Assertion.verifyMessageContain(actualMessage, "bulkupload.success.initiate", "Bulk Payout Initiate", pNode)) {
                        return actualMessage.split("batch ID:")[1].trim();
                    } else {
                        return null;
                    }


                } else {
                    Utils.captureScreen(pNode);
                    pNode.fail("Failed to initiate Bulk Payout for Service:" + service);
                    Assert.fail("Failed to Initiate Bulk Payout, exiting the test");
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return null;
    }


    /**
     * Verify Error Logs For negative Tests
     *
     * @param messageCode
     * @throws Exception
     */
    public void verifyErrorLogs(String... messageCode) throws Exception {

        if (messageCode.length == 0) {
            Assertion.markAsFailure("Expected message is required");
            Assert.fail("Expected message is required");
        }
        try {

            BulkPaymentPage1 p1 = new BulkPaymentPage1(pNode);
            String actualMsg = p1.getErrorString();
            Assertion.verifyContains(actualMsg, "File processing has failed", "Verify Error Message From UI", pNode);
            String filePath = p1.downloadErrorLogFile();
            if (filePath != null) {
                for (String msgCode : messageCode) {
                    String expectedMsg = MessageReader.getMessage(msgCode, null);
                    Utils.checkLogFileSpecificMessage(filePath, expectedMsg, pNode);
                }

            } else {
                Assertion.markAsFailure("Failed to download error logs");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }


    }

}
