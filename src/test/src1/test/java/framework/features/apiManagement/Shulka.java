package framework.features.apiManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.SfmResponse;
import framework.entity.User;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.concurrent.Future;

import static framework.components.shulka.PricingPolicyOperations.saveServiceChargePolicy;
import static framework.components.shulka.PricingPolicyOperations.saveServiceChargePolicyAsync;
import static framework.util.JsonPathOperation.delete;
import static framework.util.JsonPathOperation.set;
import static org.hamcrest.Matchers.is;

public class Shulka {
    private static WebDriver driver;
    private static ExtentTest pNode;

    public static Shulka init(ExtentTest t1) throws Exception {
        driver = DriverFactory.getDriver();
        pNode = t1;
        return new Shulka();
    }

    public static Future<ValidatableResponse> setServiceChargePolicy(String sCode, String currencyCode, String payInstId, String chargeAmount, String taxAmount){
        return saveServiceChargePolicyAsync(
                set("$.serviceCode", sCode),
                set("$.currency", currencyCode),
                set("$.chargeRules[0].condition", null),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.fixedAmount", chargeAmount),
                set("$.chargeRules[0].chargeStatements[0].chargePayerProductId", payInstId),
                delete("$.chargeRules[0].chargeStatements[0].pricingMethod.percentage"),
                set("$.taxRules[0].taxStatements[0].pricingMethod.percentage", 0),
                set("$.taxRules[0].taxStatements[0].pricingMethod.fixedAmount", taxAmount)
        );
    }

}
