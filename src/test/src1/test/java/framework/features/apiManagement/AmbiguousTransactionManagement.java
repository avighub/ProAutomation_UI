package framework.features.apiManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.*;
import framework.features.common.Login;
import framework.pageObjects.ambiguousTransaction.AmbiguousTransaction_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.junit.Assert;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static framework.util.jigsaw.CommonOperations.getTransactionDetails;

/**
 * Created by rahul.rana on 9/6/2017.
 */
public class AmbiguousTransactionManagement {
    private static ExtentTest pNode;
    private static OperatorUser naUser;

    public static AmbiguousTransactionManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        if (naUser == null) {
            naUser = DataFactory.getOperatorUsersWithAccess("AMBGTXN_UPLOAD").get(0);
        }

        return new AmbiguousTransactionManagement();
    }

    /**
     * Delete a row
     * TODO - move to Excel Util
     *
     * @param sheet
     * @param rowToDelete
     */
    public static void deleteRow(Sheet sheet, Row rowToDelete) {
        // if the row contains merged regions, delete them
        List<Integer> mergedRegionsToDelete = new ArrayList<>();
        int numberMergedRegions = sheet.getNumMergedRegions();
        for (int i = 0; i < numberMergedRegions; i++) {
            CellRangeAddress mergedRegion = sheet.getMergedRegion(i);

            if (mergedRegion.getFirstRow() == rowToDelete.getRowNum()
                    && mergedRegion.getLastRow() == rowToDelete.getRowNum()) {
                mergedRegionsToDelete.add(i);
            }
        }
        for (Integer indexToDelete : mergedRegionsToDelete) {
            sheet.removeMergedRegion(indexToDelete);
        }

        int rowIndex = rowToDelete.getRowNum();
        sheet.removeRow(rowToDelete);
        int lastRowNum = sheet.getLastRowNum();
        if (rowIndex >= 0 && rowIndex < lastRowNum) {
            sheet.shiftRows(rowIndex + 1, lastRowNum, -1);
        }
    }

    /**
     * Verify Balance Post Processing the Ambiguous Transaction .xlsx file
     *
     * @param isSuccess
     * @param prePayer
     * @param prePayee
     * @param postPayer
     * @param postPayee
     * @throws IOException
     */
    public void initiatePostProcessBalanceValidation(boolean isSuccess, UsrBalance prePayer, UsrBalance prePayee, UsrBalance postPayer, UsrBalance postPayee, BigDecimal amountTxLong) throws IOException {
        pNode.info("Payer: Pre Balance/FIC: " + prePayer.Balance + "/" + prePayer.FIC + " Post Balance/FIC: " + postPayer.Balance + "/" + postPayer.FIC);
        pNode.info("Payee: Pre Balance/FIC: " + prePayee.Balance + "/" + prePayee.FIC + " Post Balance/FIC: " + postPayee.Balance + "/" + postPayee.FIC);

        if (isSuccess) {
            // if the process is 'TS' : Transaction Successful
            if (prePayer.Balance.subtract(amountTxLong).equals(postPayer.Balance) &&
                    prePayee.Balance.add(amountTxLong).equals(postPayee.Balance)) {
                pNode.pass("Verified Successfully that the Balance From Payee's FIC has been moved to Payee's Balance");
            } else {
                pNode.fail("Failed to verify that the Balance From Payee's FIC has been moved to Payee's Balance");
            }
        } else {
            // if the process is 'TF' : Transaction Failure
            if (prePayer.Balance.equals(postPayer.Balance) &&
                    prePayee.Balance.equals(postPayee.Balance)) {
                pNode.pass("Verified Successfully that the Balance From Payee's FIC " +
                        "has been moved to Payer's Balance, as the Process Status was Set to 'TF': Transaction Failure");
            } else {
                pNode.fail("Failed to verify that the Balance From Payee's FIC " +
                        "has been moved to Payer's Balance, as the Process Status was Set to 'TF': Transaction Failure");
            }
        }

        Assertion.verifyEqual(prePayer.FIC, postPayer.FIC,
                "Verify that Payer Post Process FIC has Zero Difference", pNode);

        Assertion.verifyEqual(prePayee.FIC, postPayee.FIC,
                "Verify that Payee Post Process FIC has Zero Difference", pNode);

    }

    /**
     * Validate Balance Post Ambiguous Transaction , intermediate Validation, the process is yet in ambiguous state
     *
     * @param prePayer
     * @param prePayee
     * @param postPayer
     * @param postPayee pass true to throughApi only if you have got the balance using API for the user because in API
     *                  the available balance is recieved as response and not the total available balance
     * @throws IOException
     */
    public void initiateAmbiguousBalanceValidation(UsrBalance prePayer, UsrBalance prePayee, UsrBalance postPayer, UsrBalance postPayee, BigDecimal amountTxLong, boolean... throughApi) throws IOException {
        pNode.info("Payer: Pre Balance/FIC: " + prePayer.Balance + "/" + prePayer.FIC + " Post Balance/FIC: " + postPayer.Balance + "/" + postPayer.FIC);
        pNode.info("Payee: Pre Balance/FIC: " + prePayee.Balance + "/" + prePayee.FIC + " Post Balance/FIC: " + postPayee.Balance + "/" + postPayee.FIC);

        if (prePayer.Balance.subtract(amountTxLong).equals(postPayer.Balance) &&
                prePayee.FIC.add(amountTxLong).equals(postPayee.FIC)) {
            pNode.pass("Verified Successfully that the Balance From Payer's balance has been moved to Payee's FIC");
        } else {
            pNode.fail("Failed to verify that the Balance from Payer's Balance to Payee's FIC");
        }

        Assertion.verifyEqual(prePayer.FIC, postPayer.FIC,
                "Verify that Payer FIC has Not Changed", pNode);

        if (throughApi.length > 0) {
            Assertion.verifyEqual(prePayee.Balance, postPayee.Balance,
                    "Verify that Payee's available Balance has Not Changed", pNode);

        } else {
            Assertion.verifyEqual(prePayee.Balance.add(amountTxLong), postPayee.Balance,
                    "Verify that Payee's account is Debited", pNode);
        }

        Assertion.verifyEqual(prePayee.FIC.add(amountTxLong), postPayee.FIC,
                "Verify that Payee's FIC has been credited", pNode);
    }

    public AmbiguousTransactionManagement downloadAmbiguousTxSheet(Object payee) throws IOException {
        Markup m = MarkupHelper.createLabel("downloadAmbiguousTxSheet", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode)
                    .login(naUser);
            AmbiguousTransaction_pg1 page = AmbiguousTransaction_pg1.init(pNode);
            page.navAmbiguousTransaction();

            if (payee instanceof User) {
                User payeeNew = (User) payee;
                page.selectPartyType(payeeNew.DomainName);
                page.selectPartyName(payeeNew.LoginId);
            } else if (payee instanceof SystemBankAccount) {
                SystemBankAccount payeeNew = (SystemBankAccount) payee;
                page.selectPartyType(payeeNew.Type);
                page.selectPartyName(payeeNew.BankName);
            } else if (payee instanceof Biller) {
                Biller payeeNew = (Biller) payee;
                page.selectPartyType("Biller");
                page.selectPartyName(payeeNew.LoginId);
            }

            page.selectFromDate(-2);
            page.selectToDate(0);
            page.downloadAmbiguousTransactionFile();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Verify That the Downloaded Excel report has the details for the last Ambiguous Transaction
     *
     * @param response
     * @param payerMsisdn
     * @param payeeMsisdn
     * @return
     * @throws Exception
     */
    public AmbiguousTransactionManagement verifyAmbiguousTransactionFile(SfmResponse response, String payerMsisdn, String payeeMsisdn) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyAmbiguousTransactionFile", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        try {
            DataFormatter formatter = new DataFormatter();
            InputStream inp = new FileInputStream(FilePath.fileBulkAmbiguousTransaction);
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0); // Last Used Row
            int lastUsedRow = sheet.getLastRowNum();
            int lastUsedColumn = sheet.getRow(0).getLastCellNum();// Last Used Column
            boolean isFound = false;
            int myRow = 1;

            for (int rowNum = 1; rowNum <= lastUsedRow; rowNum++) {
                String data = formatter.formatCellValue(sheet.getRow(rowNum).getCell(0)).trim();
                if (data.equals(response.TransactionId)) {
                    myRow = rowNum;
                    isFound = true;
                    break;
                }
            }

            if (!isFound) {
                pNode.fail("Could not find relevant Data in the Downloaded File");
                Assert.fail();
                return this;
            }

            for (int colNum = 0; colNum < lastUsedColumn; colNum++) {
                String colName = formatter.formatCellValue(sheet.getRow(0).getCell(colNum)).trim();
                String cellValue = formatter.formatCellValue(sheet.getRow(myRow).getCell(colNum)).trim();

                if (cellValue.isEmpty())
                    continue;

                if (colName.equalsIgnoreCase("Transaction ID*") && cellValue.equals(response.TransactionId)) {
                    Assertion.verifyEqual(cellValue, response.TransactionId, "Verify Transaction Id", pNode);
                } else if (colName.equalsIgnoreCase("Reference Id")) {
                    Assertion.verifyEqual(cellValue, response.ServiceRequestId, "Verify Reference Number", pNode);
                } else if (colName.equalsIgnoreCase("External Reference ID")) {
                    ValidatableResponse transactionDetails = getTransactionDetails(response.TransactionId);
                    JsonPath responsePath = transactionDetails.extract().jsonPath();
                    String externalReferenceIdAtAmbiguous = responsePath.getString("ftxnId");
                    Assertion.verifyEqual(cellValue, externalReferenceIdAtAmbiguous, "Verify ExternalReferenceId", pNode);
                } else if (colName.equalsIgnoreCase("Payer msisdn") && !response.ServiceFlow.equals(Services.BILL_PAYMENT)) {
                    Assertion.verifyEqual(cellValue, payeeMsisdn, "Verify Payee MSISDN", pNode);
                } else if (colName.equalsIgnoreCase("Payer msisdn") && !response.ServiceFlow.equals(Services.DEBIT_SC)) {
                    Assertion.verifyEqual(cellValue, payerMsisdn, "Verify Payer MSISDN", pNode);
                }
            }

            int cellId = (response.ServiceFlow.equals(Services.BILL_PAYMENT)) ? 16 : 8;
            String extReferenceId = formatter.formatCellValue(sheet.getRow(myRow).getCell(cellId));
            if(extReferenceId.equalsIgnoreCase("")){
                pNode.warning("External Reference Id is empty");
            }
            pNode.info("Ext Reference Id: " + extReferenceId);
            Assertion.verifyEqual(extReferenceId.length() >= 16, true,
                    "Verify that Length of External reference ID is greater than or equal to 16", pNode);

            Assertion.verifyEqual(extReferenceId.length() <= 20, true,
                    "Verify that Length of External reference ID is Less than or equal to 20", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Set Transaction Status as TS/TF
     * Only one transaction can be set, all other entries are removed
     *
     * @param response
     * @param bStatus
     * @return
     * @throws Exception
     */
    public AmbiguousTransactionManagement setTransactionStatus(SfmResponse response, boolean bStatus) throws Exception {
        int statusColumn = 7;
        int statusColumnBillPay = 13;
        Markup m = MarkupHelper.createLabel("setTransactionStatus", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        String statusVal = (bStatus) ? "TS" : "TF";
        try {
            DataFormatter formatter = new DataFormatter();
            InputStream inp = new FileInputStream(FilePath.fileBulkAmbiguousTransaction);
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            // Last Used Row
            int lastUsedRow = sheet.getLastRowNum();
            boolean isFound = false;
            int myRow = 0;
            for (int rowNum = 1; rowNum <= lastUsedRow; rowNum++) {
                String data = formatter.formatCellValue(sheet.getRow(rowNum).getCell(0)).trim();
                if (data.equals(response.TransactionId)) {
                    if (response.ServiceFlow.equals(Services.BILL_PAYMENT)) {
                        //doing this since the transaction status column is different in case of biller
                        sheet.getRow(rowNum).getCell(statusColumnBillPay).setCellValue(statusVal);
                    } else {
                        sheet.getRow(rowNum).getCell(statusColumn).setCellValue(statusVal);
                    }
                    pNode.info("Set the Status for Transaction ID - " + response.TransactionId + " to '" + statusVal + "'");
                    isFound = true;
                } else {
                    // remove the Row, as transaction without status would cause error
                    Row delRow = sheet.getRow(rowNum);
                    deleteRow(sheet, delRow);
                    lastUsedRow -= 1;
                    rowNum -= 1;
                }
            }
            if (!isFound) {
                pNode.fail("Could not find relevant Data in the Downloaded File");
                Assert.fail();
            }
            inp.close();
            FileOutputStream fileOut = new FileOutputStream(FilePath.fileBulkAmbiguousTransaction);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public AmbiguousTransactionManagement downloadAmbiguousTxSheetforRecharge(Object payee, RechargeOperator operator) throws IOException {
        Markup m = MarkupHelper.createLabel("downloadAmbiguousTxSheetforrecharge", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode)
                    .login(naUser);
            AmbiguousTransaction_pg1 page = AmbiguousTransaction_pg1.init(pNode);
            page.navAmbiguousTransaction();
            page.selectPartyType("TopUp Operator");
            page.selectPartyName(operator.OperatorName);
            page.selectFromDate(-2);
            page.selectToDate(0);
            page.downloadAmbiguousTransactionFile();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public AmbiguousTransactionManagement uploadUpatedAmbiguousTxnFileforRecharge(SfmResponse response, Object payee) throws Exception {
        Markup m = MarkupHelper.createLabel("uploadUpatedAmbiguousTxnFileforRecharge", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode)
                    .login(naUser);
            AmbiguousTransaction_pg1 page = AmbiguousTransaction_pg1.init(pNode);
            page.navAmbiguousTransaction();

            page.selectPartyType("TopUp Operator");
            page.selectFromDate(0);
            page.selectToDate(0);
            page.setUploadFile(FilePath.fileBulkAmbiguousTransaction);
            page.clickBtnUpload();
            Thread.sleep(5000);
            page.clickImgDownloadLog();
            verifyLogSuccessUpload(response);
            //TODO - ASSERTIONS
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        } finally {
            Utils.closeUntitledWindows();
        }
        return this;
    }


    /**
     * Upload Ambiguous Transaction .xls File
     *
     * @param response
     * @param payee
     * @return
     */
    public AmbiguousTransactionManagement uploadUpatedAmbiguousTxnFile(SfmResponse response, Object payee) throws Exception {
        Markup m = MarkupHelper.createLabel("uploadUpatedAmbiguousTxnFile", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode).login(naUser);
            AmbiguousTransaction_pg1 page = AmbiguousTransaction_pg1.init(pNode);
            page.navAmbiguousTransaction();
            page.setUploadFile(FilePath.fileBulkAmbiguousTransaction);
            page.clickBtnUpload();
            Thread.sleep(Constants.MAX_WAIT_TIME);
            page.clickImgDownloadLog();
            verifyLogSuccessUpload(response);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        } finally {
            Utils.closeUntitledWindows();
        }
        return this;
    }

    /**
     * Verify that Ambiguous Transaction .xlx File is Successfully Uploaded
     *
     * @param response
     * @throws IOException
     */
    public void verifyLogSuccessUpload(SfmResponse response) throws IOException {
        Markup m = MarkupHelper.createLabel("verifyLogSuccessUpload", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        try {
            DataFormatter formatter = new DataFormatter();
            InputStream inp = new FileInputStream(FilePath.fileBulkAmbiguousTransactionLog);
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            // Last Used Row
            int lastUsedRow = sheet.getLastRowNum();
            // Last Used Column
            Row r = sheet.getRow(1);
            for (int rowNum = 1; rowNum <= lastUsedRow; rowNum++) {
                String data = formatter.formatCellValue(sheet.getRow(rowNum).getCell(0)).trim();
                if (data.equals(response.TransactionId)) {
                    Assertion.verifyEqual("Successfully uploaded",
                            formatter.formatCellValue(sheet.getRow(rowNum).getCell(1)).trim(), "Verify Successful Upload",
                            pNode);
                    return;
                }
            }
            pNode.fail("Could not find Data - " + response.TransactionId);
            Assert.fail();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * @param transferId
     * @param isSuccess  - true if verifying for success
     */
    public void dbVerifyAmbiguousIsProcessed(String transferId, boolean isSuccess) throws SQLException, IOException {
        Markup m = MarkupHelper.createLabel("dbVerifyAmbiguousIsProcessed", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        String txStatus1 = null, txStatus2 = null, isProcessed = null;
        String statusVal = (isSuccess) ? "TS" : "TF";
        MobiquityGUIQueries dbCon = new MobiquityGUIQueries();
        ResultSet result = dbCon.fetch_MTX_AMBIGUOUS_TXN(transferId);
        while (result.next()) {
            txStatus1 = result.getString("TRANSFER_STATUS");
        }
        ResultSet result1 = dbCon.fetch_MTX_AMBIGUOUS_TXN_DETAILS(transferId);
        while (result1.next()) {
            txStatus2 = result1.getString("TRANSFER_STATUS");
            isProcessed = result1.getString("IS_PROCESSED");
        }

        if (txStatus2 == null || isProcessed == null) {
            pNode.fail("Please check the test, as the DB table is not updated!");
            return;
        }
        Assertion.verifyContains(txStatus1, statusVal, "DB Verification, TX:- " + transferId + " Has Status:-" + txStatus1, pNode);
        Assertion.verifyContains(txStatus2, statusVal, "DB Verification, TX:- " + transferId + " Has Status:-" + txStatus2, pNode);
        Assertion.verifyContains(isProcessed, "Y", "DB Verification, TX:- " + transferId + " is Processed", pNode);
    }

    /**
     * @param transferId
     */
    public void dbVerifyAmbiguousIsProcessedForInternationalRemittance(String transferId) throws SQLException, IOException {
        Markup m = MarkupHelper.createLabel("dbVerifyAmbiguousIsProcessed", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker
        String txStatus1 = null;
        String statusVal = "TA";
        MobiquityGUIQueries dbCon = new MobiquityGUIQueries();
        ResultSet result = dbCon.fetch_MTX_AMBIGUOUS_TXN(transferId);
        while (result.next()) {
            txStatus1 = result.getString("TRANSFER_STATUS");
        }
        Assertion.verifyContains(txStatus1, statusVal, "DB Verification, TX:- " + transferId + " Has Status:-" + txStatus1, pNode);
    }

    /**
     * Verify That the Downloaded Excel report has the details for the last Ambiguous Transaction for party type as bank
     * pass false to isExternalIdGenerated only if you are doing transaction through UI and when external ID isn't generated
     * else don't pass any value
     *
     * @param response
     * @return
     * @throws Exception
     */
    public AmbiguousTransactionManagement verifyAmbiguousTransactionFileForBank(SfmResponse response, boolean... isExternalIdGenerated) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyAmbiguousTransactionFileForBank", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker

        try {
            boolean checkForExtId = false;
            if (isExternalIdGenerated.length > 0)
                checkForExtId = isExternalIdGenerated[0];

            DataFormatter formatter = new DataFormatter();
            InputStream inp = new FileInputStream(FilePath.fileBulkAmbiguousTransaction);
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);// Last Used Row
            int lastUsedRow = sheet.getLastRowNum();// Last Used Column
            int lastUsedColumn = sheet.getRow(0).getLastCellNum();
            boolean isFound = false;

            int myRow = 1;
            for (int rowNum = 1; rowNum <= lastUsedRow; rowNum++) {
                String data = formatter.formatCellValue(sheet.getRow(rowNum).getCell(0)).trim();
                if (data.equals(response.TransactionId)) {
                    myRow = rowNum;
                    isFound = true;
                    break;
                }
            }

            if (!isFound) {
                pNode.fail("Could not find relevant Data in the Downloaded File");
                Assert.fail();
                return this;
            }

            for (int colNum = 0; colNum < lastUsedColumn; colNum++) {
                String colName = formatter.formatCellValue(sheet.getRow(0).getCell(colNum)).trim();
                String cellValue = formatter.formatCellValue(sheet.getRow(myRow).getCell(colNum)).trim();

                if (colName.equalsIgnoreCase("Transaction ID*") && cellValue.equals(response.TransactionId) && !cellValue.isEmpty()) {
                    Assertion.verifyEqual(cellValue, response.TransactionId, "Verify Transaction Id", pNode);
                } else if (colName.equalsIgnoreCase("External Reference ID") && !cellValue.isEmpty()) {
                    ValidatableResponse transactionDetails = getTransactionDetails(response.TransactionId);
                    JsonPath responsePath = transactionDetails.extract().jsonPath();
                    String externalReferenceIdAtAmbiguous = responsePath.getString("ftxnId");
                    Assertion.verifyEqual(cellValue, externalReferenceIdAtAmbiguous, "Verify ExternalReferenceId", pNode);
                }
            }

            if (!checkForExtId)
                return this; // return if extReference number validation is not required!

            String extReferenceId = formatter.formatCellValue(sheet.getRow(myRow).getCell(8));
            if (extReferenceId.equalsIgnoreCase("")){
                pNode.fail("Third Party Reference Id is Not generated / Check for Column mismatch");
                return this;
            }

            pNode.info("Ext Reference Id: " + extReferenceId);
            Assertion.verifyEqual(extReferenceId.length() >= 16, true,
                    "Verify that Length of External reference ID is greater than or equal to 16", pNode);

            Assertion.verifyEqual(extReferenceId.length() <= 20, true,
                    "Verify that Length of External reference ID is Less than or equal to 20", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

}
