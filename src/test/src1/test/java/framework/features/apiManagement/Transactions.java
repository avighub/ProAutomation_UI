package framework.features.apiManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.TxnResponse;
import framework.entity.*;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.JsonPathOperation;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.TransactionType;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.jigsaw.JigsawOperations;
import framework.util.jigsaw.ServiceRequestContracts;
import org.apache.commons.lang.RandomStringUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.core.IsEqual;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.Future;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.JsonPathOperation.*;
import static framework.util.common.DataFactory.*;
import static framework.util.common.ProcessingHelper.fireCallable;
import static framework.util.jigsaw.CommonOperations.*;
import static framework.util.jigsaw.JigsawOperations.*;
import static framework.util.jigsaw.JigsawOperations.performServiceRequestAndWaitForSuccessForSyncAPI;
import static framework.util.jigsaw.ServiceRequestContracts.serviceRequestDetails;
import static framework.util.jigsaw.serviceFlowOperation.createFlowForCommissionDisbursementToBankTransferAsync;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Automation Team on 8/31/2017.
 */
public class Transactions {
    private static WebDriver driver;
    private static ExtentTest pNode;
    private static CurrencyProvider defaultProvider, nondefaultProvider;


    public static Transactions init(ExtentTest t1) throws Exception {
        driver = DriverFactory.getDriver();
        defaultProvider = DataFactory.getDefaultProvider();
        nondefaultProvider = DataFactory.getNonDefaultProvider();
        pNode = t1;
        return new Transactions();
    }

    public static void validateResponseMessageForAmbiguousTransaction(ValidatableResponse response, String serviceName, String transactionId, String language) {
        if (language == "en") {
            response.body("message", equalTo(serviceName + " transaction with " + transactionId +
                    " has been placed in ambiguous state, as it is currently awaiting response from external system"));
        }
        if (language == "fr") {
            response.body("message", equalTo(serviceName + " transaction avec " + transactionId +
                    " a été placé dans un état ambigu,comme il est en ce moment en attente d'une réponse par le système externe"));
        }

    }

    /**
     * Perform Cash In
     *
     * @param receiver
     * @param transactor
     * @param txnAmt
     */
    public static SfmResponse initiateCashIn(User receiver, User transactor, String txnAmt, String... productId) throws IOException {
        Markup m = MarkupHelper.createLabel("Perform Cash In: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker
        String payID = productId.length > 0 ? productId[0] : "12";

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.CASHIN,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", receiver.Password),
                set("receiver.identificationNo", receiver.ExternalCode),
                set("receiver.productId", payID),
                delete("depositor")
        ).body("txnStatus", is("TS"));

        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);
        }
        return new SfmResponse(response, pNode);
    }

    private static ValidatableResponse performServiceRequestAndWaitForSuccessForSyncAPI(String serviceCode, JsonPathOperation... operations) {
        return performServiceRequestAndWaitForCompletionStatusForSyncAPI(serviceCode, operations, Matchers.equalTo("SUCCEEDED"))
                //.body("errors", nullValue())
                //.body("serviceRequestId", notNullValue())
                // .body("message", notNullValue())
                //.body("transactionId", notNullValue())
                //.body("transactionAmount", IsEqual.equalTo(null))
                //.body("transferAmount", IsEqual.equalTo(null))
                .body("serviceFlow", Matchers.equalTo(serviceCode));
    }

    public static ValidatableResponse performServiceRequestAndWaitForCompletionStatusForSyncAPI(String serviceCode, JsonPathOperation[] operations, Matcher<String> matcher) {
        String text = ServiceRequestContracts.serviceRequestJson(true, operations);
        pNode.info("Body: " + text);
        ValidatableResponse response = postServiceRequest(serviceCode, operations);
        return response;
    }

    public static ValidatableResponse performServiceRequestResumeAndWaitForCompletionStatusForSyncAPI(String serviceCode, JsonPathOperation[] operations, Matcher<String> matcher) {
        String text = ServiceRequestContracts.serviceRequestJson(true, operations);
        pNode.info("Body: " + text);
        ValidatableResponse response = postServiceRequestResume(serviceCode, operations).log().all().statusCode(200);
        CoreMatchers.anyOf(CoreMatchers.is("SUCCEEDED"), CoreMatchers.is("FAILED"), CoreMatchers.is("PAUSED"));
        return response;
    }

    /**
     * Change MPIN TPin
     *
     * @param user
     * @return
     * @throws IOException TODO -  if default Mpin Tpin is not set then need to fetch users MPIN using Message
     *                     TODO - Specific Message
     */
    public Transactions changeChannelUserMPinTPin(User user) throws IOException {
        Markup m = MarkupHelper.createLabel("changeChannelUserMPinTPin: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        try {
            if (AppConfig.defaultPin != null) {
                defaultPin = AppConfig.defaultPin;
            } else {
                // TODO
                pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
            }

            OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_MPIN_CHANNEL_USR,
                    set("COMMAND.MSISDN", user.MSISDN),
                    set("COMMAND.MPIN", defaultPin),
                    set("COMMAND.NEWMPIN", user.getmPin()),
                    set("COMMAND.CONFIRMMPIN", user.getmPin()),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
            );
            // post the request
            pNode.info(x.bodyString);
            TxnResponse mPinChange = x.postOldTxnURLForHSB(pNode)
                    .verifyStatus(Constants.TXN_SUCCESS);

//            String actualMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "DESC");
//            String txnID = MobiquityGUIQueries.getLastTransactionID(Services.CHANGE_MPIN);
//            DBAssertion.verifyDBMessageContains(actualMessage, "000111", "Verify Channel User Change mPIN Message", pNode, txnID);

            // if successfully changed the mPin then Change the tPin
            if (mPinChange.TxnStatus.equals(Constants.TXN_SUCCESS) &&
                    AppConfig.isTPinAuthentication &&
                    !user.CategoryCode.equals(Constants.ENTERPRISE)) {
                OldTxnOperations y = new OldTxnOperations(TransactionType.CHANGE_TPIN_CHANNEL_USR,
                        set("COMMAND.MSISDN", user.MSISDN),
                        set("COMMAND.MPIN", user.getmPin()),
                        set("COMMAND.PIN", defaultPin),
                        set("COMMAND.NEWPIN", user.gettPin()),
                        set("COMMAND.CONFIRMPIN", user.gettPin()),
                        set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
                );
                pNode.info(y.bodyString);
                TxnResponse tPinChange = y.postOldTxnURLForHSB(pNode)
                        .verifyStatus(Constants.TXN_SUCCESS);

//                Utils.putThreadSleep(Constants.TWO_SECONDS);
//                actualMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "DESC");
//                String txnId = MobiquityGUIQueries.getLastTransactionID(Services.CHANGE_TPIN);
//                DBAssertion.verifyDBMessageContains(actualMessage, "00111", "Verify Channel User Change TPIN Message", pNode, txnId);

            } else {
                // set the user object's flag
                user.setError("Failed to Change The MPIN");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Change Customers Mpin TPIN
     *
     * @param subs
     * @return
     * @throws Exception
     */
    public Transactions changeCustomerMpinTpin(User subs) throws Exception {
        Markup m = MarkupHelper.createLabel("changeCustomerMpinTpin: " + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        try {
            if (AppConfig.defaultPin != null) {
                defaultPin = AppConfig.defaultPin;
            } else {
                // TODO
                pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
            }

            OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_MPIN_CUSTOMER_USR,
                    set("COMMAND.MSISDN", subs.MSISDN),
                    set("COMMAND.MPIN", defaultPin),
                    set("COMMAND.NEWMPIN", ConfigInput.mPin),
                    set("COMMAND.CONFIRMMPIN", ConfigInput.mPin),
                    set("COMMAND.LANGUAGE1", "1")
            );
            // post the request
            pNode.info(x.bodyString);
            TxnResponse mPinChange = x.postOldTxnURLForHSB(pNode);
            TxnResponse mTpinChangeStatus = mPinChange
                    .verifyStatus(Constants.TXN_SUCCESS);

            if (mPinChange.TxnStatus.equals(Constants.TXN_SUCCESS)) {
                subs.setIsPasswordChanged();
            }

            // if successfully changed the mPin then Change the tPin
            if (mPinChange.TxnStatus.equals(Constants.TXN_SUCCESS) && AppConfig.isTPinAuthentication) {
                OldTxnOperations y = new OldTxnOperations(TransactionType.CHANGE_TPIN_CUSTOMER_USR,
                        set("COMMAND.MSISDN", subs.MSISDN),
                        set("COMMAND.MPIN", ConfigInput.mPin),
                        set("COMMAND.PIN", defaultPin),
                        set("COMMAND.NEWPIN", ConfigInput.tPin),
                        set("COMMAND.CONFIRMPIN", ConfigInput.tPin),
                        set("COMMAND.LANGUAGE1", "1")
                );
                pNode.info(y.bodyString);
                TxnResponse rTpinChange = y.postOldTxnURLForHSB(pNode)
                        .verifyStatus(Constants.TXN_SUCCESS);
            } else {
                // set the user object's flag
                subs.setError("Failed to Change The MPIN");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Subscriber Acquisition to System
     *
     * @param subs
     * @return
     * @throws Exception
     */
    public Transactions subscriberAcquisition(User subs) throws Exception {
        Markup m = MarkupHelper.createLabel("subscriberAcquisition: " + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBSCRIBER_AQUISITION,
                    set("COMMAND.MSISDN", subs.MSISDN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    set("COMMAND.PROVIDER", defaultProvider.ProviderId)
            );
            pNode.info(operation.bodyString);
            operation.postOldTxnURLForHSB(pNode)
                    .assertStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public Transactions subscriberAcquisitionfornondefaultprovide(User subs) throws Exception {
        Markup m = MarkupHelper.createLabel("subscriberAcquisition: " + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBSCRIBER_AQUISITION,

                    set("COMMAND.MSISDN", subs.MSISDN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    set("COMMAND.PROVIDER", nondefaultProvider.ProviderId)
            );
            pNode.info(operation.bodyString);
            operation.postOldTxnURLForHSB(pNode)
                    .assertStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Merchant Payment
     *
     * @param merchant
     * @param subscriber
     * @return
     */
    public SfmResponse initiateMerchantPayment(User merchant, User subscriber, String amount, String... paymentId) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateMerchantPayment: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String productId = (paymentId.length > 0) ? paymentId[0] : DataFactory.getDefaultWallet().WalletId;

        String serviceFlow = "MERCHPAY";

        setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("transactor.idValue", merchant.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.pin", ConfigInput.tPin),
                set("transactor.productId", productId),
                delete("receiver"),
                delete("depositor"),
                set("initiator", "sender"),
                set("transactionAmount", amount),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", subscriber.MSISDN),
                        set("tpin", ConfigInput.tPin),
                        set("mpin", ConfigInput.mPin),
                        set("identificationNo", subscriber.ExternalCode)
                ))
        );
        return new SfmResponse(response, pNode);
    }

    /**
     * Resume Merchant Payment
     *
     * @param response
     * @return
     * @throws IOException
     */
    public SfmResponse resumeMerchantPayment(SfmResponse response, String serviceFlowResumeName) throws IOException {
        Markup m = MarkupHelper.createLabel("resumeMerchantPayment: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        //String serviceFlowResume = "RESUME_MERCHPAY";

        ValidatableResponse resumeResponse = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlowResumeName,
                delete("receiver"),
                delete("depositor"),
                delete("transactor"),
                delete("transactionAmount"),
                delete("currency"),
                delete("bearerCode"),
                delete("initiator"),
                put("$", "resumeServiceRequestId", response.ServiceRequestId),
                put("$", "transactionStatus", "true")
                // status is false means 3rd party response is failure
        );
        Assertion.verifyResponseSucceeded(resumeResponse, pNode);
        return new SfmResponse(resumeResponse, pNode);
    }

    /**
     * Subscriber Biller Association by Retailer
     *
     * @param retailer
     * @param bill     - bill to be associated and approved
     * @return
     */
    public TxnResponse subsBillerRegistrationByRetailer(User retailer, CustomerBill bill, String walletId) throws IOException {
        Markup m = MarkupHelper.createLabel("subscriberBillerAssociationByRetailer: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBS_BILLER_ASSOC_BY_RETAILER,
                set("COMMAND.MSISDN", retailer.MSISDN),
                set("COMMAND.MSISDN1", bill.CustomerMsisdn),
                set("COMMAND.BPCODE", bill.BillerCode),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.LANGUAGE2", "1"),
                set("COMMAND.PROVIDER", bill.ProviderId),
                set("COMMAND.PROVIDER1", bill.ProviderId),
                set("COMMAND.PAYID1", walletId),
                set("COMMAND.PREF1", bill.BillAccNum)
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * Delete Subscriber Biller Registration
     *
     * @param retailer
     * @param bill
     * @param walletId
     * @return
     * @throws IOException
     */
    public TxnResponse delBillerRegistrationByRetailer(User retailer, CustomerBill bill, String walletId) throws IOException {
        Markup m = MarkupHelper.createLabel("delBillerRegistrationByRetailer: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DELETE_BILLER_REG_BY_RETAILER,
                set("COMMAND.MSISDN", retailer.MSISDN),
                set("COMMAND.MSISDN1", bill.CustomerMsisdn),
                set("COMMAND.BPCODE", bill.BillerCode),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.LANGUAGE2", "1"),
                set("COMMAND.PROVIDER", bill.ProviderId),
                set("COMMAND.PROVIDER1", bill.ProviderId),
                set("COMMAND.PAYID1", walletId),
                set("COMMAND.PREF1", bill.BillAccNum)
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse delBillerRegistrationByRetailerApproval(CustomerBill bill, String txnid) throws IOException {
        Markup m = MarkupHelper.createLabel("delBillerRegistrationByRetailerApproval: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DELETE_BILLER_REG_BY_RETAILER_APPROVAL,
                set("COMMAND.MSISDN", bill.CustomerMsisdn),
                set("COMMAND.PROVIDER", bill.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.TXNID", txnid),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.FTXNID", "FTxnId345")
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * Subs Biller Association Approval
     *
     * @param subMsisdn
     * @param txnid
     * @return
     */
    public TxnResponse subsBillerAssociationApproval(String subMsisdn, String txnid) throws IOException {
        Markup m = MarkupHelper.createLabel("subscriberBillerAssociationByRetailer: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBS_BILLER_APPROVAL_BY_RETAILER,
                set("COMMAND.MSISDN", subMsisdn),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.TXNID", txnid),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.FTXNID", "FTxnId345")
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * Self registration for Subscriber
     *
     * @param subscriber
     * @return
     */
    public TxnResponse selfRegistrationForSubscriber(User subscriber, String... payID) throws IOException {
        Markup m = MarkupHelper.createLabel("Self-Registration For Subscriber: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String paymentId = payID.length > 0 ? payID[0] : Constants.NORMAL_WALLET;
        OldTxnOperations operations = new OldTxnOperations(TransactionType.SELF_SUBSCRIBER_REGISTRATION,
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", paymentId),
                set("COMMAND.MSISDN", subscriber.MSISDN),
                set("COMMAND.FNAME", subscriber.FirstName),
                set("COMMAND.LNAME", subscriber.LastName),
                set("COMMAND.EMAIL", subscriber.Email),
                set("COMMAND.DOB", subscriber.DateOfBirth.replace("/", "")),
                set("COMMAND.IDNUMBER", subscriber.ExternalCode),
                set("COMMAND.REGTYPEID", "NO_KYC"),
                set("COMMAND.LOGINID", subscriber.LoginId),
                set("COMMAND.OTP", generateOTPService(subscriber.MSISDN)));
        return operations.postOldTxnURLForHSB(pNode);
    }

    /**
     * Perform Merhcant Payout
     *
     * @param transactor - Wholesaler / Retailer
     * @param party      - Unknown Party whose MSISDN is not registered in the sysytem
     * @param receiver   - Merchant/ Head Merchant
     * @return
     */
    public SfmResponse initiateMerchantPayOut(User transactor, User party, User receiver, String amount, String walletId) {
        Markup m = MarkupHelper.createLabel("merchantPayOut: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = "MERCHPAYOT";

        setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.currency", defaultProvider.ProviderId),
                set("transactor.productId", walletId),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.pin", ConfigInput.tPin),
                set("depositor.idValue", party.MSISDN),
                set("depositor.mpin", ConfigInput.mPin),
                set("depositor.tpin", ConfigInput.tPin),
                set("depositor.pin", ConfigInput.tPin),
                set("depositor.identificationNo", party.ExternalCode),
                set("initiator", "transactor"),
                delete("receiver"),
                put("$", "merchantReceiver", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", receiver.MSISDN),
                        set("productId", walletId),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("employeeId")))
        );

        return new SfmResponse(response, pNode);
    }

    /**
     * Perform Debit Service Charge from System Wallet to Bank
     *
     * @param payer - Sender Wallet
     * @param payee - Receiver Wallet
     * @return
     * @throws IOException
     */
    public SfmResponse performDebitServiceCharge(String payer, SystemBankAccount payee) throws IOException {
        Markup m = MarkupHelper.createLabel("Debit Service Charge: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        setTxnConfig(of("txn.numericId.applicable.services", Services.DEBIT_SC));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.DEBIT_SC,
                set("bearerCode", "web"),
                set("initiator", "sender"),
                set("currency", "101"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", "NWADM7792045712"),
                set("receiver.idType", "systemId"),
                set("receiver.idValue", payee.WalletCode),
                set("receiver.bankId", payee.BankId),
                set("receiver.bankAccountNumber", "1109033854"),
                delete("depositor"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "systemId"),
                        set("idValue", payer)
                )),
                put("$", "_signedblock", serviceRequestDetails(
                        set("$", "getColumnPaths"),
                        put("$", "HMAC-SHA1-SIGN", "cdff6ff32360bb7894f846bf0d13f946d7cdffba")))).body("txnStatus", is("TA"));

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    public SfmResponse initiateCashInWithRole(User receiver, User transactor, String txnAmt, String... productId) throws IOException {

        Markup m = MarkupHelper.createLabel("Perform Cash In: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker
        String payID = productId.length > 0 ? productId[0] : "12";

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.CASHIN,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("receiver.idValue", receiver.MSISDN),
                put("receiver", "userRole", "Customer"),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.identificationNo", receiver.ExternalCode),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", receiver.Password),
                set("receiver.productId", payID),
                delete("depositor")
        ).body("txnStatus", is("TS"));

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);

    }

    /**
     * Remittance Partner Payment
     *
     * @param remittancePartnerDetails
     * @param subscriber
     * @return
     */
    public SfmResponse initiateRemittancePartnerPayment(User remittancePartnerDetails, User subscriber, BigDecimal txnAmt) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateRemittancePartnerPayment: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.INTERNAT_SEND_MONEY;

        setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                delete("receiver"),
                delete("depositor"),
                delete("transactor"),
                set("initiator", "sender"),
                set("transactionAmount", txnAmt),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("tpin", ConfigInput.tPin),
                        set("mpin", ConfigInput.mPin),
                        set("password", subscriber.Password),
                        set("pin", ConfigInput.mPin),
                        set("txnPassword", null),
                        set("idValue", subscriber.MSISDN),
                        set("productId", "12")
                )),
                put("$", "remittancePartner", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("productId", "12"),
                        delete("employeeId"),
                        set("idValue", remittancePartnerDetails.MSISDN)
                )),
                put("$", "remittanceReceiver", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", "12345678")
                ))
        ).body("txnStatus", is("TA"));

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    /**
     * Remittance Partner Payment
     *
     * @param remittancePartnerDetails
     * @param subscriber
     * @param serviceRequestId
     * @return
     */
    public SfmResponse initiateRemittancePartnerPaymentFailFromEig(User remittancePartnerDetails, User subscriber, String serviceRequestId) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateRemittancePartnerPayment: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlowResume = "RESUME_INTLSEND";

        setTxnConfig(of("txn.numericId.applicable.services", serviceFlowResume));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlowResume,
                delete("receiver"),
                delete("depositor"),
                delete("transactor"),
                delete("transactionAmount"),
                delete("currency"),
                delete("bearerCode"),
                delete("initiator"),
                put("$", "resumeServiceRequestId", serviceRequestId),
                put("$", "transactionStatus", "false")
        );

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    /**
     * @param subs
     * @return
     * @throws Exception
     */
    public TxnResponse selfRegistrationForSubscriberWithKinDetails(User subs, String... payID) throws Exception {
        Markup m = MarkupHelper.createLabel("selfRegistrationForSubscriberWithKinDetails: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String paymentId = payID.length > 0 ? payID[0] : DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId;

        if (subs.KinUser == null) {
            subs.setKinUser(new KinUser()); // assign a kin user with empty details
            subs.KinUser.KinNumber = "";
            subs.KinUser.DateOfBirth = "";
            subs.KinUser.FirstName = "";
            subs.KinUser.LastName = "";
            subs.KinUser.MiddleName = "";
            subs.KinUser.RelationShip = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Age = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Nationality = "";
        }

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SELF_SUBSCRIBER_REGISTRATION,
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.FNAME", subs.FirstName),
                set("COMMAND.LNAME", subs.LastName),
                set("COMMAND.EMAIL", subs.Email),
                set("COMMAND.PAYID", paymentId),
                set("COMMAND.IDNUMBER", subs.ExternalCode),
                set("COMMAND.DOB", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.OTP", generateOTPService(subs.MSISDN)),
                set("COMMAND.LOGINID", subs.LoginId),
                set("COMMAND.REGTYPEID", subs.RegistrationType),
                set("COMMAND.KINFIRSTNAME", subs.KinUser.FirstName),
                set("COMMAND.KINLASTNAME", subs.KinUser.LastName),
                set("COMMAND.KINAGE", subs.KinUser.Age),
                set("COMMAND.KINCONTACTNO", subs.KinUser.ContactNum),
                set("COMMAND.KINNATIONALITY", subs.KinUser.Nationality),
                set("COMMAND.KINNATIONALITYNO", subs.KinUser.KinNumber),
                set("COMMAND.KINRELATIONSHIP", subs.KinUser.RelationShip),
                set("COMMAND.REGTYPEID", "NO_KYC")

        );
        // post the request
        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
        }
        return response;
    }

    public TxnResponse subsSelfRegistrationWithPayId(User subs, String payID) throws Exception {
        Markup m = MarkupHelper.createLabel("subsSelfRegistrationWithPayId: API", ExtentColor.AMBER);
        pNode.info(m); // Method Start Marker

        if (subs.KinUser == null) {
            subs.setKinUser(new KinUser()); // assign a kin user with empty details
            subs.KinUser.KinNumber = "";
            subs.KinUser.DateOfBirth = "";
            subs.KinUser.FirstName = "";
            subs.KinUser.LastName = "";
            subs.KinUser.MiddleName = "";
            subs.KinUser.RelationShip = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Age = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Nationality = "";
            subs.KinUser.NationalityAPIVal = "";
            subs.KinUser.AgeAPIVal = "";
        }

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SELF_SUBSCRIBER_REGISTRATION,
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.FNAME", subs.FirstName),
                set("COMMAND.LNAME", subs.LastName),
                set("COMMAND.EMAIL", subs.Email),
                set("COMMAND.PAYID", payID),
                set("COMMAND.IDNUMBER", subs.ExternalCode),
                set("COMMAND.DOB", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.OTP", generateOTPService(subs.MSISDN)),
                set("COMMAND.LOGINID", subs.LoginId),
                set("COMMAND.REGTYPEID", subs.RegistrationType),
                set("COMMAND.KINFIRSTNAME", subs.KinUser.FirstName),
                set("COMMAND.KINMIDDLENAME", subs.KinUser.MiddleName),
                set("COMMAND.KINLASTNAME", subs.KinUser.LastName),
                set("COMMAND.KINAGE", subs.KinUser.AgeAPIVal),
                set("COMMAND.KINCONTACTNO", subs.KinUser.ContactNum),
                set("COMMAND.KINNATIONALITY", subs.KinUser.NationalityAPIVal),
                set("COMMAND.KINNATIONALITYNO", subs.KinUser.KinNumber),
                set("COMMAND.KINRELATIONSHIP", subs.KinUser.RelationShip),
                set("COMMAND.REGTYPEID", "NO_KYC")

        );
        // post the request
        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
        }
        return response;
    }

    public TxnResponse selfRegistrationForSubscriberWithAdditionalFields(User subs, String idType) throws Exception {
        Markup m = MarkupHelper.createLabel("selfRegistrationForSubscriberWithAdditionalFields: API", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        if (subs.KinUser == null) {
            subs.setKinUser(new KinUser()); // assign a kin user with empty details
            subs.KinUser.KinNumber = "";
            subs.KinUser.DateOfBirth = "";
            subs.KinUser.FirstName = "";
            subs.KinUser.LastName = "";
            subs.KinUser.MiddleName = "";
            subs.KinUser.RelationShip = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Age = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Nationality = "";
        }

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SELF_SUBSCRIBER_REGISTRATION,
                set("COMMAND.IDTYPE", idType),
                set("COMMAND.EXPIRYDATE", subs.ExpiryDate),
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.FNAME", subs.FirstName),
                set("COMMAND.LNAME", subs.LastName),
                set("COMMAND.IDNUMBER", subs.ExternalCode),
                set("COMMAND.DOB", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.OTP", generateOTPService(subs.MSISDN)),
                set("COMMAND.LOGINID", subs.LoginId),
                set("COMMAND.KINFIRSTNAME", subs.KinUser.FirstName),
                set("COMMAND.KINLASTNAME", subs.KinUser.LastName),
                set("COMMAND.KINAGE", subs.KinUser.Age),
                set("COMMAND.KINCONTACTNO", subs.KinUser.ContactNum),
                set("COMMAND.KINNATIONALITY", subs.KinUser.Nationality),
                set("COMMAND.KINNATIONALITYNO", subs.KinUser.KinNumber),
                set("COMMAND.KINRELATIONSHIP", subs.KinUser.RelationShip)

        );
        // post the request
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * @param subs
     * @return
     * @throws Exception
     */
    public TxnResponse subRegistrationByAS400(User subs, String... payId) throws Exception {
        Markup m = MarkupHelper.createLabel("subRegistrationByAS400: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String payInstId = (payId.length > 0) ? payId[0] : DataFactory.getDefaultWallet().WalletId;

        if (subs.KinUser == null) {
            subs.setKinUser(new KinUser()); // assign a kin user with empty details
            subs.KinUser.KinNumber = "";
            subs.KinUser.DateOfBirth = "";
            subs.KinUser.FirstName = "";
            subs.KinUser.LastName = "";
            subs.KinUser.MiddleName = "";
            subs.KinUser.RelationShip = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Age = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Nationality = "";
        }


        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBSCRIBER_REG_AS400,
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.PAYID", payInstId),
                set("COMMAND.FNAME", subs.FirstName),
                set("COMMAND.LNAME", subs.LastName),
                set("COMMAND.IDNUMBER", subs.ExternalCode),
                set("COMMAND.DOB", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.LOGINID", subs.LoginId),
                set("COMMAND.KINFIRSTNAME", subs.KinUser.FirstName),
                set("COMMAND.KINMIDDLENAME", subs.KinUser.MiddleName),
                set("COMMAND.KINLASTNAME", subs.KinUser.LastName),
                set("COMMAND.KINAGE", subs.KinUser.AgeAPIVal),
                set("COMMAND.KINCONTACTNO", subs.KinUser.ContactNum),
                set("COMMAND.KINNATIONALITY", subs.KinUser.NationalityAPIVal),
                set("COMMAND.KINNATIONALITYNO", subs.KinUser.KinNumber),
                set("COMMAND.KINRELATIONSHIP", subs.KinUser.RelationShip)

        );
        // post the request
        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
        }
        return response;
    }

    /**
     * Generate OTP Service
     *
     * @param msisdn
     * @return
     */
    public String generateOTPService(String msisdn) {
        Markup m = MarkupHelper.createLabel("generateOTPService: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SELF_SUBSCRIBER_REGISTRATION_OTP,
                set("COMMAND.MSISDN", msisdn),
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.SRVREQTYPE", "CUSTREG"),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

        );
        // post the request
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode).OTP;
    }

    /**
     * @param sClub
     * @return
     * @throws IOException
     */
    public TxnResponse addSavingClub(SavingsClub sClub) throws IOException {
        Markup m = MarkupHelper.createLabel("addSavingClub: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.ADD_SAVING_CLUB,
                set("COMMAND.MSISDN", sClub.CMMsisdn),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.CLUBNAME", sClub.ClubName),
                set("COMMAND.CLUBLOCATION", sClub.Location),
                set("COMMAND.CLUBTYPE", sClub.ClubType),
                set("COMMAND.USER_GRADE", DataFactory.getGradeCode(sClub.CMGradeName)),
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.FTXNID", "FTxnId345")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * Subscriber registration
     *
     * @param subs
     * @return
     * @throws Exception
     */
    public TxnResponse SubscriberRegistrationByChannelUserWithKinDetails(User subs, String... payId) throws Exception {
        CurrencyProviderMapping.init(pNode)
                .mapPrimaryWalletPreference(subs);

        Markup m = MarkupHelper.createLabel("SubscriberRegistrationByChannelUserWithKinDetails: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        User usrWholeSaler = DataFactory.getChannelUserWithAccess("SUBSADD");
        String payInstId = (payId.length > 0) ? payId[0] : DataFactory.getDefaultWallet().WalletId;
        if (subs.KinUser == null) {
            subs.setKinUser(new KinUser()); // assign a kin user with empty details
            subs.KinUser.KinNumber = "";
            subs.KinUser.DateOfBirth = "";
            subs.KinUser.FirstName = "";
            subs.KinUser.LastName = "";
            subs.KinUser.MiddleName = "";
            subs.KinUser.RelationShip = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Age = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Nationality = "";
        }

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBSCRIBER_REGISTRATION_CHANNEL_USER,
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.MSISDN", usrWholeSaler.MSISDN),
                set("COMMAND.MSISDN2", subs.MSISDN),
                set("COMMAND.PAYID2", payInstId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.FNAME", subs.FirstName),
                set("COMMAND.LNAME", subs.LastName),
                set("COMMAND.EMAIL", subs.Email),
                set("COMMAND.IDNUMBER", subs.ExternalCode),
                set("COMMAND.DOB", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.LOGINID", subs.LoginId),
                set("COMMAND.KINFIRSTNAME", subs.KinUser.FirstName),
                set("COMMAND.KINLASTNAME", subs.KinUser.LastName),
                set("COMMAND.KINAGE", subs.KinUser.Age),
                set("COMMAND.KINCONTACTNO", subs.KinUser.ContactNum),
                set("COMMAND.KINNATIONALITY", subs.KinUser.Nationality),
                set("COMMAND.KINNATIONALITYNO", subs.KinUser.KinNumber),
                set("COMMAND.KINRELATIONSHIP", subs.KinUser.RelationShip)

        );
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
            response.assertMessage("subscriber.agent.registration.success", subs.MSISDN);
        }
        // post the request
        pNode.info(operation.bodyString);
        return response;
    }

    public TxnResponse SubscriberRegistrationByChannelWithAdditionalParameters(User subs, String idType) throws Exception {
        Markup m = MarkupHelper.createLabel("SubscriberRegistrationByChannelWithAdditionalParameters: API", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        User usrWholeSaler = DataFactory.getChannelUserWithAccess("SUBSADD", 0);

        if (idType == null) {
            idType = "PASSPORT";
        }

        if (subs.KinUser == null) {
            subs.setKinUser(new KinUser()); // assign a kin user with empty details
            subs.KinUser.KinNumber = "";
            subs.KinUser.DateOfBirth = "";
            subs.KinUser.FirstName = "";
            subs.KinUser.LastName = "";
            subs.KinUser.MiddleName = "";
            subs.KinUser.RelationShip = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Age = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Nationality = "";
        }

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBSCRIBER_REGISTRATION_CHANNEL_USER,
                set("COMMAND.IDTYPE", idType),
                set("COMMAND.EXPIRYDATE", subs.ExpiryDate),
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.MSISDN", usrWholeSaler.MSISDN),
                set("COMMAND.MSISDN2", subs.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.FNAME", subs.FirstName),
                set("COMMAND.LNAME", subs.LastName),
                set("COMMAND.IDNUMBER", subs.ExternalCode),
                set("COMMAND.DOB", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.LOGINID", subs.LoginId),
                set("COMMAND.KINFIRSTNAME", subs.KinUser.FirstName),
                set("COMMAND.KINLASTNAME", subs.KinUser.LastName),
                set("COMMAND.KINAGE", subs.KinUser.Age),
                set("COMMAND.KINCONTACTNO", subs.KinUser.ContactNum),
                set("COMMAND.KINNATIONALITY", subs.KinUser.Nationality),
                set("COMMAND.KINNATIONALITYNO", subs.KinUser.KinNumber),
                set("COMMAND.KINRELATIONSHIP", subs.KinUser.RelationShip)

        );
        // post the request
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public Transactions webCashOutApprovalBySubs(String subMsisdn, String txnid, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("webCashOutApprovalBySubs: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.WEB_CASHOUT,
                set("COMMAND.MSISDN", subMsisdn),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.TXNID", txnid),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.STATUS", "0"),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId)
        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        operation.postOldTxnURLForHSB(pNode)
                .verifyStatus(Constants.TXN_SUCCESS);
        return this;
    }

    /**
     * @param subs
     * @param txnid
     * @param status Status can be either 0(Accept) or 1(Reject)
     * @return
     * @throws IOException
     */
    public TxnResponse subsAccountClosure(User subs, String txnid, String... status) throws IOException {
        Markup m = MarkupHelper.createLabel("subsAccountClosure: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String closureStatus;

        if (status.length > 0) {
            closureStatus = status[0];
        } else {
            closureStatus = "0";
        }

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBS_ACCOUNT_CLOSURE,
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.TXNID", txnid),
                set("COMMAND.STATUS", closureStatus),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * Join Saving Club
     *
     * @param sClub
     * @param member
     * @param join   - true to join, false to resign from a club
     * @return
     * @throws IOException
     */
    public TxnResponse joinOrResignSavingClub(SavingsClub sClub, User member, boolean join) throws IOException {
        Markup m = MarkupHelper.createLabel("joinOrResignSavingClub: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String transactionType = join ? TransactionType.JOIN_SAVING_CLUB : TransactionType.RESIGN_SAVING_CLUB;
        OldTxnOperations operation = new OldTxnOperations(transactionType,
                set("COMMAND.MSISDN", member.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.USER_ID", MobiquityGUIQueries.getCustomerUserId(member.MSISDN)),
                set("COMMAND.CLUBNAME", sClub.ClubName),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse createSavingClubBySubscriberWithSingleStep(SavingsClub sClub, String providerId, String payId) throws IOException {
        Markup m = MarkupHelper.createLabel("joinOrResignSavingClub: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String transactionType = TransactionType.ADD_SAVING_CLUB;
        OldTxnOperations operation = new OldTxnOperations(transactionType,
                set("COMMAND.MSISDN", sClub.CMMsisdn),
                set("COMMAND.PROVIDER", providerId),
                set("COMMAND.USER_GRADE", sClub.Grade),
                set("COMMAND.CLUBTYPE", payId),
                set("COMMAND.CLUBLOCATION", sClub.Location),
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.CLUBNAME", sClub.ClubName),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );


        pNode.info("Post Request: " + operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        if (ConfigInput.isAssert) {
            response
                    .verifyMessage("1000")
                    .verifyStatus(Constants.TXN_SUCCESS);
        }
        return response;
    }

    /**
     * Deposit amount to Saving Club
     *
     * @param sClub
     * @param member
     * @return
     * @throws IOException
     */
    public TxnResponse depositSavingClub(SavingsClub sClub, User member, String amount) throws Exception {
        // make Sure that the User is having sufficient Balance
        TransactionManagement.init(pNode)
                .makeSureLeafUserHasBalance(member, AppConfig.minSVCDepositAmount);

        Markup m = MarkupHelper.createLabel("depositSavingClub: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DEPOSIT_SAVING_CLUB,
                set("COMMAND.MSISDN", member.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYID", GlobalData.defaultWallet.WalletId),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.CLUBNAME", sClub.ClubName)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * Club Disbursement Initiation
     *
     * @param sClub
     * @param chairman
     * @return
     * @throws IOException
     */
    public String clubDisbursementInitiation(SavingsClub sClub, User chairman, User member1, String amount) throws Exception {

        Markup m = MarkupHelper.createLabel("clubDisbursementInitiation", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DISBURSEMENT_INITIATION_CLUB,
                set("COMMAND.MSISDN", chairman.MSISDN),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.MSISDN2", member1.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.AMOUNT", amount)

        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode).TxnId;
    }

    /**
     * Club Disbursement Confirmation
     *
     * @param sClub
     * @param chairman
     * @return
     * @throws IOException
     */
    public TxnResponse clubDisbursementConfirmation(SavingsClub sClub, User chairman, String txnId, String amount, String action) throws Exception {

        Markup m = MarkupHelper.createLabel("clubDisbursementConfirmation", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DISBURSEMENT_CONFIRMATION_CLUB,
                set("COMMAND.MSISDN", chairman.MSISDN),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.ACTION", action),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.TXNID", txnId)

        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * Club Settlement Initiation
     *
     * @param sClub
     * @param chairman
     * @return
     * @throws IOException
     */
    public String clubSettlementInitiation(SavingsClub sClub, User chairman, User resignMember, String amount) throws Exception {

        Markup m = MarkupHelper.createLabel("clubSettlementInitiation", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SETTLEMENT_INITIATION_CLUB,
                set("COMMAND.MSISDN", chairman.MSISDN),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.MSISDN2", resignMember.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.AMOUNT", amount)

        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode).TxnId;
    }

    /**
     * Club Settlement Confirmation
     *
     * @param sClub
     * @param chairman
     * @return
     * @throws IOException
     */
    public TxnResponse clubSettlementConfirmation(SavingsClub sClub, User chairman, String txnId, String amount, String action) throws Exception {

        Markup m = MarkupHelper.createLabel("clubSettlementConfirmation", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SETTLEMENT_CONFIRMATION_CLUB,
                set("COMMAND.MSISDN", chairman.MSISDN),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.ACTION", action),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.TXNID", txnId)

        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * To get list of clubs associated with a User
     *
     * @param msisdn
     * @param -      list of optional parameters in sequence for negative Test
     * @return
     * @throws IOException
     */
    public TxnResponse getSVCList(String msisdn, String providerId) throws IOException {
        Markup m = MarkupHelper.createLabel("getSVCList: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.GET_SVC_LIST,
                set("COMMAND.MSISDN", msisdn),
                set("COMMAND.PROVIDER", providerId)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * NOTE: Make Sure Country Code Prefix Must Be Available Under NETWORK_PREFIX Table
     * Before Performing This Action
     *
     * @param gradeCode:   User Grade
     * @param countryCode: User Country Code
     * @return TxnResponse
     */
    public TxnResponse gradeNetworkAssociation(String gradeCode, String countryCode) {
        Markup m = MarkupHelper.createLabel("Grade Network Association: API", ExtentColor.PINK);
        pNode.info(m);
        TxnResponse response = null;
        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.gradeNetworkAssociation,
                    set("COMMAND.GRADE_CODE", gradeCode),
                    set("COMMAND.COUNTRY_CODE", countryCode)
            );

            pNode.info("Post Request: " + operation.bodyString);
            response = operation.postOldTxnURLForHSB(pNode);

            if (ConfigInput.isAssert) {
                if (response.isMessageContains("201541")) {
                    pNode.info("Grade Lookup Entry Already exists");
                } else {
                    response.verifyMessage("201544");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /**
     * @param gradeCode:   User Grade
     * @param countryCode: User Country Code
     * @return TxnResponse
     */
    public TxnResponse gradeNetworkDissociation(String gradeCode, String countryCode) {
        Markup m = MarkupHelper.createLabel("Grade Network Dissociation: API", ExtentColor.PINK);
        pNode.info(m);
        TxnResponse response = null;
        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.gradeNetworkAssociation,
                    set("COMMAND.GRADE_CODE", gradeCode),
                    set("COMMAND.COUNTRY_CODE", countryCode),
                    set("COMMAND.GRADE_STATUS", "D")
            );

            pNode.info("Post Request: " + operation.bodyString);
            response = operation.postOldTxnURLForHSB(pNode);

            if (ConfigInput.isAssert) {
                response.verifyMessage("201545");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /**
     * Get the SVC Wallet Details
     *
     * @param clubId - Club Id
     * @param msisdn -  Member MSISDN
     * @return
     * @throws IOException
     */
    public TxnResponse getSVCWalletDetails(String clubId, String msisdn) throws IOException {
        Markup m = MarkupHelper.createLabel("getSVCWalletDetails: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.GET_SVC_WALLET_DETAILS,
                set("COMMAND.MSISDN", msisdn),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.CLUBID", clubId)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * SVC Balance enquiry
     *
     * @return
     * @throws IOException
     */
    public TxnResponse svcBalanceEnquiry(SavingsClub sClub) throws IOException {
        Markup m = MarkupHelper.createLabel("svcBalanceEnquiry: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_BALANCE_ENQUIRY,
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId),
                set("COMMAND.CLUB_ID", sClub.ClubId),
                set("COMMAND.MSISDN", sClub.CMMsisdn),
                set("COMMAND.USER_ID", MobiquityGUIQueries.getCustomerUserId(sClub.CMMsisdn)),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * initiateOnlineBillerPayment
     *
     * @param biller
     * @param subscriber
     * @return
     */
    public SfmResponse initiateOnlineBillerPayment(Biller biller, User subscriber, String txnAmt, boolean... success) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateOnlineBillerPayment: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        boolean isSuccess = success.length > 0 ? success[0] : true;
        String serviceFlow = Services.BILL_PAYMENT;

        setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));

        if (isSuccess) {
            ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                    set("transactionAmount", txnAmt),
                    set("transactor.idType", "mobileNumber"),
                    set("transactor.idValue", subscriber.MSISDN),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.password", subscriber.Password),
                    set("transactor.pin", ConfigInput.mPin),
                    delete("depositor"),
                    set("receiver.idType", "billerCode"),
                    set("receiver.idValue", biller.BillerCode),
                    delete("receiver.identificationNo"),
                    delete("receiver.productId"),
                    delete("receiver.pin"),
                    delete("receiver.mpin"),
                    delete("receiver.tpin"),
                    delete("receiver.password"),
                    delete("receiver.txnPassword"),
                    put("$", "billDetails", serviceRequestDetails(
                            set("billAccountNumber", "56789"),
                            delete("productId"),
                            delete("idValue"),
                            delete("identificationNo"),
                            delete("mpin"),
                            delete("tpin"),
                            delete("password"),
                            delete("pin"),
                            delete("txnPassword"),
                            delete("passcode"),
                            delete("employeeId")))
            ).body("txnStatus", is("TA"));
            //pNode.info("Post Request: " + response);
            Assertion.verifyResponseSucceeded(response, pNode);
            return new SfmResponse(response, pNode);
        } else {
            ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceFlow,
                    set("transactionAmount", txnAmt),
                    set("transactor.idType", "mobileNumber"),
                    set("transactor.idValue", subscriber.MSISDN),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.password", subscriber.Password),
                    set("transactor.pin", ConfigInput.mPin),
                    delete("depositor"),
                    set("receiver.idType", "billerCode"),
                    set("receiver.idValue", biller.BillerCode),
                    delete("receiver.identificationNo"),
                    delete("receiver.productId"),
                    delete("receiver.pin"),
                    delete("receiver.mpin"),
                    delete("receiver.tpin"),
                    delete("receiver.password"),
                    delete("receiver.txnPassword"),
                    put("$", "billDetails", serviceRequestDetails(
                            set("billAccountNumber", "56789"),
                            delete("productId"),
                            delete("idValue"),
                            delete("identificationNo"),
                            delete("mpin"),
                            delete("tpin"),
                            delete("password"),
                            delete("pin"),
                            delete("txnPassword"),
                            delete("passcode"),
                            delete("employeeId")))
            );//.body("txnStatus", is("TA"));
            SfmResponse response1 = new SfmResponse(response, pNode);
            if (ConfigInput.isAssert) {
                response1.verifyMessage("savingclub.withdraw.min.allowed");
                // Assertion.verifyResponseFailed(response, pNode);
            }
            return response1;
        }

    }

    /**
     * @param subscriber
     * @param txnAmount
     * @param operatorID
     * @return
     * @throws Exception
     */
    public SfmResponse subscriberSelfRecharge(User subscriber, String txnAmount, String operatorID) throws Exception {

        Markup m = MarkupHelper.createLabel("Initiate Self Recharge For " + subscriber.LoginId + " : API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String serviceType = Services.SELF_RECHARGE;
        SfmResponse res = null;

        ValidatableResponse response = performServiceRequestAndWaitForPauseForSyncAPI(serviceType,
                set("transactionAmount", txnAmount),
                delete("language"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("depositor"),
                set("transactor.idValue", subscriber.MSISDN),
                set("transactor.identificationNo", subscriber.ExternalCode),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.txnPassword", null),
                set("receiver.idType", "operatorId"),
                set("receiver.identificationNo", subscriber.ExternalCode),
                set("receiver.idValue", operatorID));

        res = new SfmResponse(response, pNode);
        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);
        }
        return res;
    }

    /**
     * @param sender
     * @param receiver
     * @param txnAmount
     * @return
     * @throws Exception
     */
    public SfmResponse performP2PTransfer(User sender, User receiver, String txnAmount) throws Exception {
        Markup m = MarkupHelper.createLabel("Initiate P2P Transfer Form " + sender.LoginId + " To " + receiver.LoginId + ": API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String serviceType = Services.P2P_REG;
        SfmResponse res = null;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceType,
                set("transactionAmount", txnAmount),
                set("initiator", "sender"),
                delete("language"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("depositor"),
                delete("transactor"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("tpin", ConfigInput.tPin),
                        set("mpin", ConfigInput.mPin),
                        set("pin", ConfigInput.tPin),
                        set("password", ConfigInput.mPin),
                        delete("identificationNo"),
                        delete("encryptedPassword"),
                        delete("EmployeeId"),
                        delete("passcode"),
                        delete("billAccountNumber"),
                        delete("billNumber")
                )),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.identificationNo", receiver.ExternalCode),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.txnPassword", null));

        res = new SfmResponse(response, pNode);
        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);
        }
        return res;

    }

    /**
     * @param user
     * @param txnId
     * @param productId
     * @param serviceToBeReversed
     * @param isServiceChargeReversible
     * @param isCommissionReversible
     * @return
     * @throws IOException
     */
    public SfmResponse initiateTransactionReversalByOperator(OperatorUser user, String txnId, String productId, String serviceToBeReversed, String isServiceChargeReversible, String isCommissionReversible) throws IOException {
        Markup m = MarkupHelper.createLabel("initiate transaction reversal for transaction ID: " + txnId + " API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.TXN_CORRECTION;

        pNode.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", serviceToBeReversed));

        ValidatableResponse response = performServiceRequestAndWaitForPauseForSyncAPI(serviceFlow,
                delete("serviceFlowId"),
                delete("depositor"),
                delete("receiver"),
                delete("transactionAmount"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("externalReferenceId"),
                delete("requestedServiceCode"),
                delete("transactionMode"),
                delete("remarks"),
                set("bearerCode", "WEB"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", user.LoginId),
                set("transactor.password", user.Password),
                put("$", "isServiceChargeReversible", isServiceChargeReversible),
                put("$", "isCommissionReversible", isCommissionReversible),
                put("$", "originalTransaction", serviceRequestDetails(
                        set("idType", "transferId"),
                        set("idValue", txnId),
                        set("productId", productId)
                ))
        );
        if (ConfigInput.isAssert) {
            Assertion.verifyResponsePaused(response, pNode);
        }
        pNode.info("Post Request: " + response);
        return new SfmResponse(response, pNode);
    }

    /**
     * @param serviceId
     * @param approver
     * @return
     * @throws Exception
     */
    public SfmResponse approveTransactionReversal(String serviceId, OperatorUser approver) throws Exception {
        Markup m = MarkupHelper.createLabel("approve transaction reversal for service ID: " + serviceId + " API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.TXN_CORRECTION;

        ValidatableResponse response = performServiceRequestResumeAndWaitForSuccessForSyncAPI(serviceFlow,
                set("resumeServiceRequestId", serviceId),
                set("bearerCode", "WEB"),
                set("party.idType", "loginId"),
                set("party.idValue", approver.LoginId));
        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);
        }
        return new SfmResponse(response, pNode);
    }

    /**
     * @param serviceType
     * @param serviceId
     * @param Status
     * @return
     * @throws Exception
     */
    public SfmResponse resumeAmbiguousTransaction(String serviceType, String serviceId, String Status) throws Exception {

        Markup m = MarkupHelper.createLabel("EIG Transaction", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceCodeResume = serviceType;
        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceCodeResume,
                put("$", "resumeServiceRequestId", serviceId),
                put("$", "transactionStatus", Status),
                delete("bearerCode"),
                delete("initiator"),
                delete("language"),
                delete("currency"),
                delete("transactionAmount"),
                delete("remarks"),
                delete("transactionMode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("requestedServiceCode"),
                delete("externalReferenceId"),
                delete("mfsTenantId"),
                delete("serviceFlowId"),
                delete("depositor"),
                delete("transactor"),
                delete("receiver"));
        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    /**
     * initiateSubscriberPaymentINTLRECV
     *
     * @param remittancePartnerDetails
     * @param subscriber
     * @param txnAmt
     * @return
     */
    public SfmResponse initiateSubscriberPaymentINTLRECV(User remittancePartnerDetails, User subscriber, BigDecimal txnAmt) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateSubscriberPaymentINTLRECV: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.INTERNAT_REC_MONEY;

        setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                delete("depositor"),
                delete("transactor"),
                set("initiator", "remittancePartner"),
                set("externalReferenceId", RandomStringUtils.randomAlphanumeric(10)),
                put("$", "remittancePartner", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        delete("employeeId"),
                        set("productId", "12"),
                        set("tpin", ConfigInput.tPin),
                        set("mpin", ConfigInput.mPin),
                        set("password", remittancePartnerDetails.Password),
                        set("pin", ConfigInput.mPin),
                        set("idValue", remittancePartnerDetails.MSISDN)
                )),
                set("receiver.idValue", subscriber.MSISDN),
                set("transactionAmount", txnAmt)
        ).body("txnStatus", is("TS"));

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    /**
     * Perform cash in with the function returning SfmResponse
     */
    public SfmResponse initiateCashIn(User receiver, User transactor, BigDecimal txnAmt) throws Exception {

        SfmResponse res = null;
        Markup m = MarkupHelper.createLabel("initiateCashIn: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.CASHIN;

        pNode.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", serviceFlow));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", receiver.Password),
                set("receiver.identificationNo", receiver.ExternalCode),
                delete("depositor")
        );

        res = new SfmResponse(response, pNode);
        if (ConfigInput.isAssert) {
            res.verifyMessage("txn.cashin.successful", DataFactory.getDefaultProvider().Currency, txnAmt.toString(), transactor.MSISDN, receiver.MSISDN, res.TransactionId);
        }

        return res;
    }

    /**
     * Perform cash in with the function returning SfmResponse
     */

    public SfmResponse initiateCashOut(User sender, User transactor, BigDecimal txnAmt) throws Exception {

        Markup m = MarkupHelper.createLabel("Initiate CashOut: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.CASHOUT;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("initiator", "transactor"),
                delete("receiver"),
                delete("depositor"),
                put("$", "withdrawer", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12")
                ))
        ).body("txnStatus", is("TI"));

        Assertion.verifyResponsePaused(response, pNode);
        return new SfmResponse(response, pNode);
    }

    /**
     * SVC Withdraw Cash
     *
     * @param clubId
     * @param msisdn
     * @param amount
     * @return
     * @throws IOException
     */
    public TxnResponse svcWithdrawCash(String clubId, String msisdn, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("svcWithdrawCash: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_WITHDRAW_CLUB,
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.CLUBID", clubId),
                set("COMMAND.MSISDN", msisdn),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.AMOUNT", amount)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse SVCbankToWallet(SavingsClub sClub, String msisdn, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("SVCbankToWallet: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_BANK_TO_WALLET,
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MSISDN", msisdn)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * SVC Stock Disbursement  - for Resigned Members
     *
     * @param sClub
     * @param memberMsisdn
     * @param amount
     * @return
     * @throws IOException
     */
    public TxnResponse svcStockDisbursement(SavingsClub sClub, String memberMsisdn, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("svcStockDisbursement: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_DISBURSE_STOCK,
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.MSISDN", sClub.CMMsisdn),
                set("COMMAND.MSISDN2", memberMsisdn),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.USER_ID", MobiquityGUIQueries.getCustomerUserId(memberMsisdn)),
                set("COMMAND.AMOUNT", amount)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * @param clubId
     * @param approverMsisdn
     * @param amount
     * @param txnId
     * @return
     * @throws IOException
     */
    public TxnResponse svcStockDisbursementConfirm(String clubId, String approverMsisdn, String amount, String txnId) throws IOException {
        Markup m = MarkupHelper.createLabel("svcStockDisbursementConfirm: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_DISBURSE_CONFIRM,
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.CLUBID", clubId),
                set("COMMAND.MSISDN", approverMsisdn),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.USER_ID", MobiquityGUIQueries.getCustomerUserId(approverMsisdn)),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.TXNID", txnId)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * SVC Approval
     *
     * @param clubId
     * @param approverMsisdn
     * @param txnId
     * @return
     * @throws IOException
     */
    public TxnResponse svcApproverWithdraw(String clubId, String approverMsisdn, String txnId) throws IOException {
        Markup m = MarkupHelper.createLabel("svcApproverWithdraw: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_WITHDRAW_CLUB_APPROVAL,
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.CLUBID", clubId),
                set("COMMAND.MSISDN", approverMsisdn),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.TXNID", txnId)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse changeChannelUserTPin(User user, String... tPin) throws IOException {
        Markup m = MarkupHelper.createLabel("changeChannelUserTPin: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        TxnResponse response = null;

        String TPIN = tPin.length > 0 ? tPin[0] : ConfigInput.tPin;
        try {
            if (AppConfig.defaultPin != null) {
                defaultPin = AppConfig.defaultPin;
            } else {
                // TODO
                pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
            }

            OldTxnOperations y = new OldTxnOperations(TransactionType.CHANGE_TPIN_CHANNEL_USR,
                    set("COMMAND.MSISDN", user.MSISDN),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", defaultPin),
                    set("COMMAND.NEWPIN", TPIN),
                    set("COMMAND.CONFIRMPIN", TPIN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
            );
            pNode.info(y.bodyString);
            response = y.postOldTxnURLForHSB(pNode);
            if (ConfigInput.isAssert) {
                response.verifyStatus(Constants.TXN_SUCCESS);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return response;
    }

    public Transactions createEmployee(Employee employee) throws Exception {
        Markup m = MarkupHelper.createLabel("createEmployee: " + employee.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.CREATE_EMPLOYEE,
                    set("COMMAND.MSISDN", employee.UserMSISDN),
                    set("COMMAND.EMPID", employee.ID),
                    set("COMMAND.FNAME", employee.FirstName),
                    set("COMMAND.LNAME", employee.LastName),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

            );
            pNode.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);
            employee.setIsCreated();
            return this;

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public Transactions getEmployeeList(Employee employee) throws Exception {
        Markup m = MarkupHelper.createLabel("Get_EMPLOYEEList: " + employee.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.Get_EMPLOYEEList,
                    set("COMMAND.MSISDN", employee.UserMSISDN),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

            );
            pNode.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);
            return this;

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public Transactions AutoDebitConfirmation_Customer(String CustomerMsisdn, String txnId) throws Exception {
        Markup m = MarkupHelper.createLabel("Auto_Debit_Confirmation_by_Customer", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.AUTO_DEBIT_CONFIRMATION_BY_CUSTOMER,
                set("COMMAND.MSISDN", CustomerMsisdn),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.TXNID", txnId),
                set("COMMAND.STATUS", "0"),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );
        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        response.verifyStatus(Constants.TXN_SUCCESS);
        return this;

    }

    public TxnResponse Last_n_Transaction(String CustomerMsisdn) throws IOException {
        Markup m = MarkupHelper.createLabel("Last_N_Transaction", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.Last_N_Transaction,
                set("COMMAND.MSISDN", CustomerMsisdn),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", "12"),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );

        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public Transactions changeChannelUserMPinSecondTime(User user, String newMpin) throws IOException {
        Markup m = MarkupHelper.createLabel("changeChannelUserMPin: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        try {
            if (AppConfig.defaultPin != null) {
                defaultPin = AppConfig.defaultPin;
            } else {
                // TODO
                pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
            }

            OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_MPIN_CHANNEL_USR,
                    set("COMMAND.MSISDN", user.MSISDN),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.NEWMPIN", newMpin),
                    set("COMMAND.CONFIRMMPIN", newMpin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
            );
            // post the request
            pNode.info(x.bodyString);
            TxnResponse mPinChange = x.postOldTxnURLForHSB(pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * SVC Pronote Demote A member
     *
     * @param sClub
     * @param member
     * @param promoteOrDemote
     * @return
     * @throws IOException
     */
    public TxnResponse svcPromoteDemoteMember(SavingsClub sClub, User member, boolean promoteOrDemote) throws IOException {
        Markup m = MarkupHelper.createLabel("svcPromoteDemoteMember: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String role = promoteOrDemote ? "Promote" : "Demote";
        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_PROMOTE_DEMOTE,
                set("COMMAND.USER_GRADE", member.GradeCode),
                set("COMMAND.ROLE", role),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.MSISDN2", member.MSISDN),
                set("COMMAND.MSISDN", sClub.CMMsisdn),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.USER_ID", MobiquityGUIQueries.getCustomerUserId(member.MSISDN)),
                set("COMMAND.ACTION", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    // Change channel user MPIN only
    public TxnResponse changeChannelUserMPin(User user, String... mPin) throws IOException {
        Markup m = MarkupHelper.createLabel("changeChannelUserMPin: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        String MPIN = mPin.length > 0 ? mPin[0] : ConfigInput.mPin;

        if (AppConfig.defaultPin != null) {
            defaultPin = AppConfig.defaultPin;
        } else {
            // TODO
            pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
        }


        OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_MPIN_CHANNEL_USR,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.MPIN", defaultPin),
                set("COMMAND.NEWMPIN", MPIN),
                set("COMMAND.CONFIRMMPIN", MPIN),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );
        // post the request
        pNode.info(x.bodyString);
        return x.postOldTxnURLForHSB(pNode);

    }

    // Change subscriber MPIN only
    public TxnResponse changeSubscriberMPin(User user, String... mPin) throws IOException {
        Markup m = MarkupHelper.createLabel("changeSubscriberMPin: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        String MPIN = mPin.length > 0 ? mPin[0] : ConfigInput.mPin;

        if (AppConfig.defaultPin != null) {
            defaultPin = AppConfig.defaultPin;
        } else {
            // TODO
            pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
        }

        OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_MPIN_CUSTOMER_USR,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.MPIN", defaultPin),
                set("COMMAND.NEWMPIN", MPIN),
                set("COMMAND.CONFIRMMPIN", MPIN),
                set("COMMAND.LANGUAGE1", "1")
        );

        // post the request
        pNode.info(x.bodyString);
        return x.postOldTxnURLForHSB(pNode);

    }

    // Change subscriber TPIN only
    public TxnResponse changeSubscriberTPin(User user, String... tPin) throws IOException {
        Markup m = MarkupHelper.createLabel("changeSubscriberTPin: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        String TPIN = tPin.length > 0 ? tPin[0] : ConfigInput.mPin;

        if (AppConfig.defaultPin != null) {
            defaultPin = AppConfig.defaultPin;
        } else {
            // TODO
            pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
        }

        OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_TPIN_CUSTOMER_USR,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", defaultPin),
                set("COMMAND.NEWPIN", TPIN),
                set("COMMAND.CONFIRMPIN", TPIN),
                set("COMMAND.LANGUAGE1", "1")
        );

        // post the request
        pNode.info(x.bodyString);
        return x.postOldTxnURLForHSB(pNode);

    }

    /**
     * For Day Transaction Summary
     **/

    public Transactions dayTxnSummary(User channeluser, boolean flag) throws Exception {
        Markup m = MarkupHelper.createLabel("dayTxnSummary: " + channeluser.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        try {
            if (AppConfig.defaultPin != null) {
                defaultPin = AppConfig.defaultPin;
            } else {
                // TODO
                pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
            }

            if (flag) {

                OldTxnOperations operation = new OldTxnOperations(TransactionType.Day_Txn_Summary,
                        set("COMMAND.MSISDN", channeluser.MSISDN),
                        set("COMMAND.MPIN", ConfigInput.mPin),
                        set("COMMAND.PIN", ConfigInput.tPin)
                );
                pNode.info(operation.bodyString);
                TxnResponse response = operation.postOldTxnURLForHSB(pNode);
                response.assertStatus(Constants.TXN_SUCCESS);
            } else {
                OldTxnOperations operation = new OldTxnOperations(TransactionType.Day_Txn_Summary,
                        set("COMMAND.MSISDN", channeluser.MSISDN),
                        set("COMMAND.MPIN", ConfigInput.mPin),
                        set("COMMAND.PIN", defaultPin)
                );
                pNode.info(operation.bodyString);
                TxnResponse response = operation.postOldTxnURLForHSB(pNode);
                response.assertStatus("00359");
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public Transactions myAccountDetails(User channeluser) throws IOException {
        Markup m = MarkupHelper.createLabel("My Account Details", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.My_Account_Details,
                set("COMMAND.MSISDN", channeluser.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin)

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        response.verifyStatus(Constants.TXN_SUCCESS);
        return this;
    }

    public void initiateCashInThroughEmployee(User receiver, User transactor) {

        saveServiceChargePolicy(
                set("$.serviceCode", "CASHIN"),
                set("$.chargeRules[0].condition", null),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.fixedAmount", "10"),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.percentage", "0"),
                set("$.taxRules", emptyList())
        );

        performServiceRequestAndWaitForSuccessForSyncAPI(Services.CASHIN,

                set("transactor.idValue", transactor.MSISDN),
                set("transactor.employeeId", 1),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", "10"),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", receiver.Password),
                delete("depositor")
        ).body("txnStatus", is("TS"));

    }

    public SfmResponse initiateCashInwithInvalidTpin(User receiver, User transactor, String txnAmt, String tpin) throws IOException {
        Markup m = MarkupHelper.createLabel("Perform Cash In: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(Services.CASHIN,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", tpin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", receiver.Password),
                delete("depositor")
        );
        return new SfmResponse(response, pNode);


    }

    public Transactions resetEmployeeMpin_Tpin(Employee employee) throws Exception {

        Markup m = MarkupHelper.createLabel("changeEmployeeMPinTPin: " + employee.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        try {
            if (AppConfig.defaultPin != null) {
                defaultPin = AppConfig.defaultPin;
            } else {
                // TODO
                pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
            }

            OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_EMPLOYEE_MPIN,
                    set("COMMAND.MSISDN", employee.UserMSISDN),
                    set("COMMAND.EMPID", employee.ID),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
            );
            // post the request
            pNode.info(x.bodyString);
            TxnResponse mPinChange = x.postOldTxnURLForHSB(pNode);

            // if successfully changed the mPin then Change the tPin
            if (mPinChange.TxnStatus.equals(Constants.TXN_SUCCESS) &&
                    AppConfig.isTPinAuthentication) {
                OldTxnOperations y = new OldTxnOperations(TransactionType.CHANGE_EMPLOYEE_PIN,
                        set("COMMAND.MSISDN", employee.UserMSISDN),
                        set("COMMAND.EMPID", employee.ID),
                        set("COMMAND.MPIN", ConfigInput.mPin),
                        set("COMMAND.PIN", ConfigInput.tPin),
                        set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
                );
                pNode.info(y.bodyString);
                y.postOldTxnURLForHSB(pNode)
                        .verifyStatus(Constants.TXN_SUCCESS);
            } else {
                // set the user object's flag
                employee.setError("Failed to Change The MPIN");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public Transactions suspendEmployee(Employee employee) throws Exception {
        Markup m = MarkupHelper.createLabel("suspendEmployee: " + employee.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.SUSPEND_EMPLOYEE,
                    set("COMMAND.MSISDN", employee.UserMSISDN),
                    set("COMMAND.EMPID", employee.ID),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

            );
            pNode.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);
            return this;

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public Transactions resumeEmployee(Employee employee) throws Exception {
        Markup m = MarkupHelper.createLabel("resumeEmployee: " + employee.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.RESUME_EMPLOYEE,
                    set("COMMAND.MSISDN", employee.UserMSISDN),
                    set("COMMAND.EMPID", employee.ID),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

            );
            pNode.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);
            return this;

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public void initiateCashInthroughEmployee(User receiver, User transactor, Employee emp, int txnAmt) throws IOException {
        Markup m = MarkupHelper.createLabel("Perform Cash In: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker
        deleteAllPricingPolicies();
        saveServiceChargePolicy(
                set("$.serviceCode", "CASHIN"),
                set("$.chargeRules[0].condition", null),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.fixedAmount", "5"),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.percentage", "0"),
                set("$.taxRules", emptyList())
        );

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.CASHIN,
                delete("language"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.employeeId", emp.ID),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.tPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", ""),
                set("transactionAmount", txnAmt),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.tPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", ""),
                delete("receiver.bankId"),
                delete("receiver.bankAccountNumber"),
                delete("receiver.partyId"),
                delete("depositor")
        ).body("txnStatus", is("TS"));

        Assertion.verifyResponseSucceeded(response, pNode);


    }


    public TxnResponse selfReimbursement(User channeluser, double amount, String... payID) throws IOException {

        Markup m = MarkupHelper.createLabel("Self Reimbursement", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String paymentId = payID.length > 0 ? payID[0] : Constants.NORMAL_WALLET;
        OldTxnOperations operation = new OldTxnOperations(TransactionType.SELF_REIMBURSEMENT,
                set("COMMAND.MSISDN", channeluser.MSISDN),
                set("COMMAND.PAYID", paymentId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.AMOUNT", amount)
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse selfReimbursementWithInvalidMpinTpin(User channeluser, double amount, String mpin, String tpin) throws IOException {

        Markup m = MarkupHelper.createLabel("Self Reimbursement", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        OldTxnOperations operation = new OldTxnOperations(TransactionType.SELF_REIMBURSEMENT,
                set("COMMAND.MSISDN", channeluser.MSISDN),
                set("COMMAND.MPIN", mpin),
                set("COMMAND.PIN", tpin),
                set("COMMAND.AMOUNT", amount)
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public Transactions changeChannelUserMPinTPinforPseudoUser(PseudoUser user) throws IOException {
        Markup m = MarkupHelper.createLabel("changeChannelUserMPinTPin: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        try {
            if (AppConfig.defaultPin != null) {
                defaultPin = AppConfig.defaultPin;
            } else {
                // TODO
                pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
            }

            OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_MPIN_CHANNEL_USR,
                    set("COMMAND.MSISDN", user.MSISDN),
                    set("COMMAND.MPIN", defaultPin),
                    set("COMMAND.NEWMPIN", user.Mpin),
                    set("COMMAND.CONFIRMMPIN", user.Mpin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
            );
            // post the request
            pNode.info(x.bodyString);
            TxnResponse mPinChange = x.postOldTxnURLForHSB(pNode);

            // if successfully changed the mPin then Change the tPin
            if (mPinChange.TxnStatus.equals(Constants.TXN_SUCCESS) &&
                    AppConfig.isTPinAuthentication) {
                OldTxnOperations y = new OldTxnOperations(TransactionType.CHANGE_TPIN_CHANNEL_USR,
                        set("COMMAND.MSISDN", user.MSISDN),
                        set("COMMAND.MPIN", user.Mpin),
                        set("COMMAND.PIN", defaultPin),
                        set("COMMAND.NEWPIN", user.Tpin),
                        set("COMMAND.CONFIRMPIN", user.Tpin),
                        set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
                );
                pNode.info(y.bodyString);
                y.postOldTxnURLForHSB(pNode)
                        .verifyStatus(Constants.TXN_SUCCESS);
            } else {
                // set the user object's flag
                user.setError("Failed to Change The MPIN");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public void selfReimbursementforPseudoUser(PseudoUser user, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("Self Reimbursement", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SELF_REIMBURSEMENT,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.MPIN", user.Mpin),
                set("COMMAND.PIN", user.Tpin),
                set("COMMAND.AMOUNT", amount)
        );
        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        response.assertStatus("01013");
    }

    public SfmResponse initiateCashInthroughEmployeeWithIncorrectMpin(User receiver, User transactor, Employee emp, String txnAmt, String Mpin) throws IOException {
        Markup m = MarkupHelper.createLabel("Perform Cash In With Incorrect mPin: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(Services.CASHIN,
                delete("language"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.employeeId", emp.ID),
                set("transactor.mpin", Mpin),
                set("transactor.pin", ConfigInput.tPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", ""),
                set("transactionAmount", txnAmt),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.tPin),
                set("receiver.tpin", ConfigInput.tPin),
                delete("receiver.identificationNo"),
                set("receiver.password", ""),
                delete("receiver.bankId"),
                delete("receiver.bankAccountNumber"),
                delete("receiver.partyId"),
                delete("depositor")
        );

        return new SfmResponse(response, pNode);


    }

    public Transactions unBarEmployee(Employee employee) throws Exception {
        Markup m = MarkupHelper.createLabel("unBarEmployee: " + employee.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.UnBar_EMPLOYEE,
                    set("COMMAND.MSISDN", employee.UserMSISDN),
                    set("COMMAND.EMPID", employee.ID),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

            );
            pNode.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);
            return this;

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public SfmResponse initiateCashInthroughPseudoUser(User receiver, PseudoUser transactor, BigDecimal txnAmt) throws IOException {
        Markup m = MarkupHelper.createLabel("Perform Cash In: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker
        deleteAllPricingPolicies();
        saveServiceChargePolicy(
                set("$.serviceCode", "CASHIN"),
                set("$.chargeRules[0].condition", null),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.fixedAmount", "2"),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.percentage", "0"),
                set("$.taxRules", emptyList())
        );

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.CASHIN,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt.toString()),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", receiver.Password),
                delete("receiver.identificationNo"),
                delete("depositor")
        ).body("txnStatus", is("TS"));

        return new SfmResponse(response, pNode);
    }

    /*public TxnResponse BalanceEnquiryWithInvalidMpinTpin(User chanlUser,String mpin,String tpin) throws IOException {
        Markup m = MarkupHelper.createLabel("Channel User's Balance Enquiry: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BALANCE_ENQUIRY,
                set("COMMAND.MSISDN", chanlUser.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.MPIN", mpin),
                set("COMMAND.PIN", tpin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }*/

    public SfmResponse initiateCashInOthers(User receiver, User transactor, User depositor, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("Perform Cash In Others: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker
        SfmResponse res = null;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.Cash_In_Others1,
                delete("language"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                set("depositor.idValue", depositor.MSISDN),
                set("depositor.identificationNo", depositor.ExternalCode),
                delete("depositor.mpin"),
                delete("depositor.pin"),
                delete("depositor.tpin"),
                delete("depositor.password"),
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                delete("transactor.txnPassword"),
                delete("transactor.EmployeeId"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                set("transactionAmount", amount),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.identificationNo", receiver.ExternalCode),
                delete("receiver.mpin"),
                delete("receiver.pin"),
                delete("receiver.tpin"),
                delete("receiver.password")

        ).body("txnStatus", is("TS"));
        res = new SfmResponse(response, pNode);

        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);
        }
        return res;
    }

    public SfmResponse rechargeMobilebychanneluser(OperatorUser receiver, User transactor, User rechargereceiver, int txnAmt, String idoperator) throws IOException {
        Markup m = MarkupHelper.createLabel("RechargeMobile", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker
        System.out.println(idoperator);
        deleteAllPricingPolicies();
        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.Recharge_Others,
                delete("language"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.identificationNo", transactor.ExternalCode),
                set("transactionAmount", txnAmt),
                set("receiver.idValue", idoperator),
                set("receiver.idType", "operatorId"),
                delete("receiver.mpin"),
                delete("receiver.pin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.identificationNo"),
                put("$", "rechargeReceiver", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", rechargereceiver.MSISDN),
                        delete("mpin"),
                        delete("pin"),
                        delete("EmployeeId"),
                        delete("tpin"))),


                delete("depositor")
        ).body("txnStatus", is("TA"));

        // Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    public String bankCashOut(User subs, User channeluser, String channaccount, String subsaccount, String amount) throws Exception {
        Markup m = MarkupHelper.createLabel("BankCashOut " + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String txnid = null;
        CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
        try {
            OldTxnOperations x = new OldTxnOperations(TransactionType.BANK_CASH_OUT,
                    set("COMMAND.MSISDN", channeluser.MSISDN),
                    set("COMMAND.MSISDN2", subs.MSISDN),

                    set("COMMAND.BANKID", DataFactory.getDefaultBankIdForDefaultProvider()),
                    set("COMMAND.BANKID2", DataFactory.getDefaultBankIdForDefaultProvider()),
                    set("COMMAND.ACCNO", channaccount),
                    set("COMMAND.ACCNO2", subsaccount),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.AMOUNT", amount),
                    set("COMMAND.LANGUAGE1", "1")
            );
            // post the request
            pNode.info(x.bodyString);

            TxnResponse bankCashOut = x.postOldTxnURLForHSB(pNode);
            txnid = bankCashOut.TxnId;

            // if successfully changed the initiated bankcashout then confirm it
            if (bankCashOut.TxnStatus.equals(Constants.TXN_SUCCESS)) {
                OldTxnOperations y = new OldTxnOperations(TransactionType.BANK_CASH_OUT_CONFIRM,
                        set("COMMAND.MSISDN", subs.MSISDN),
                        set("COMMAND.MPIN", ConfigInput.mPin),
                        set("COMMAND.PIN", ConfigInput.tPin),
                        set("COMMAND.TXNID", txnid)

                );
                pNode.info(y.bodyString);
                TxnResponse BankCashOutConfirm = y.postOldTxnURLForHSB(pNode)
                        .verifyStatus(Constants.TXN_SUCCESS);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnid;
    }

    public TxnResponse BalanceEnquiry(User chanlUser) throws IOException {
        Markup m = MarkupHelper.createLabel("BalanceEnquiry: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BALANCE_ENQUIRY,
                set("COMMAND.MSISDN", chanlUser.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse svcGetMiniStatement(SavingsClub sClub) throws IOException {
        Markup m = MarkupHelper.createLabel("svcGetMiniStatement: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_BANK_MINI_STATEMENT,
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.CLUB_ID", sClub.ClubId),
                set("COMMAND.MSISDN", sClub.CMMsisdn),
                set("COMMAND.BANKID", DataFactory.getBankId(sClub.BankName)),
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.ACCNO", sClub.ClubId)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse getAccountStatement(User user, String bankId) throws IOException {
        Markup m = MarkupHelper.createLabel("GetAccountStatement: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.CUSTOMER_BNK_MINI_STATEMENT,
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.BANKID", bankId),
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.ACCNO", user.DefaultAccNum)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * Service Charge Policy Defined
     *
     * @param serviceChargeFixAmt
     * @param percentage
     */
    public void defineServiceCharge(int serviceChargeFixAmt, int percentage) {
        Markup m = MarkupHelper.createLabel("Define Service Charge: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker

        setShulkaConfig(of("pricingPolicy.approval.required", "false"));

        saveServiceChargePolicy(
                set("$.serviceCode", "CASHIN"),
                set("$.chargeRules[0].condition", null),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.fixedAmount", serviceChargeFixAmt),
                set("$.chargeRules[0].chargeStatements[0].pricingMethod.percentage", percentage),
                set("$.taxRules", emptyList())
        );
    }

    /**
     * p2pNonRegTransaction
     *
     * @param subscriber
     * @param unregistered_subs
     * @param txnAmount
     * @return
     * @throws Exception
     */
    public SfmResponse p2pNonRegTransaction(User subscriber, User unregistered_subs, BigDecimal txnAmount) throws Exception {
        Markup m = MarkupHelper.createLabel("p2pNonRegTransaction: API", ExtentColor.PURPLE);
        pNode.info(m);
        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI("P2PNONREG",
                set("transactionAmount", txnAmount),
                delete("transactor"),
                delete("depositor"),
                set("initiator", "sender"),
                set("receiver.idValue", unregistered_subs.MSISDN),
                set("receiver.identificationNo", null),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", subscriber.MSISDN),
                        set("tpin", ConfigInput.tPin),
                        set("mpin", ConfigInput.mPin),
                        set("productId", "12"),
                        set("identificationNo", subscriber.ExternalCode)
                ))
        );

        return new SfmResponse(response, pNode);
    }

    /**
     * TODO Move to transaction
     *
     * @param sender
     * @param transactor
     * @return
     * @throws Exception
     */
    public SfmResponse performCompleteCashOut(String initiator, User sender, User transactor) throws Exception {

        Markup m = MarkupHelper.createLabel("Perform Complete CashOut: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        SfmResponse res = null;
        String serviceFlow = Services.COMPLETECASHOUT;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("initiator", initiator),
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.tPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                delete("transactionAmount"),
                delete("receiver"),
                delete("depositor"),
                put("$", "withdrawer", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("identificationNo", sender.ExternalCode),
                        set("mpin", ConfigInput.mPin),
                        set("pin", ConfigInput.tPin),
                        set("tpin", ConfigInput.tPin),
                        set("productId", "12")
                )));

        res = new SfmResponse(response, pNode);
        if (ConfigInput.isAssert) {
            res.assertStatus(Constants.TXN_STATUS_SUCCEEDED);
        }
        return res;
    }

    /**
     * TODO Move to transaction
     *
     * @param sender
     * @param transactor
     * @param txnAmt
     * @return
     * @throws Exception
     */
    public SfmResponse performCashOut(User sender, User transactor, BigDecimal txnAmt) throws Exception {

        Markup m = MarkupHelper.createLabel("performCashOut: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.CASHOUT;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("initiator", "transactor"),
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                delete("receiver"),
                delete("depositor"),
                put("$", "withdrawer", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12")
                ))
        ).body("txnStatus", is("TI"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse cashOutPendingForEmployee(User sender, User transactor, String empId, BigDecimal txnAmt, ExtentTest pNode) throws Exception {

        Markup m = MarkupHelper.createLabel("cashOutPendingForEmployee: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.CASHOUT;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactor.employeeId", empId),
                set("transactionAmount", txnAmt),
                set("initiator", "withdrawer"),
                delete("receiver"),
                delete("depositor"),
                put("$", "withdrawer", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12")
                ))
        ).body("txnStatus", is("TI"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse performCashOutToPseudoUser(User sender, User transactor, PseudoUser pseudoUser, BigDecimal txnAmt, ExtentTest pNode) throws Exception {

        Markup m = MarkupHelper.createLabel("performCashOutToPseudoUser: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.CASHOUT;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("transactor.idValue", pseudoUser.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("initiator", "withdrawer"),
                delete("receiver"),
                delete("depositor"),
                put("$", "withdrawer", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12")
                ))
        ).body("txnStatus", is("TI"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse performCashOutUsingIVRAndValidateOTP(User sender, User transactor, BigDecimal txnAmt, ExtentTest node) throws Exception {

        Markup m = MarkupHelper.createLabel("performCashOutUsingIVRAndValidateOTP: API", ExtentColor.PINK);
        node.info(m); // Method Start Marker

        String serviceFlow = Services.CASHOUT;

        ValidatableResponse response = performServiceRequestAndWaitForPauseForSyncAPI(serviceFlow,
                set("bearerCode", "IVR"),
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("initiator", "withdrawer"),
                delete("receiver"),
                delete("depositor"),
                put("$", "withdrawer", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12")
                ))
        ).assertThat().body("code", IsEqual.equalTo("otp.validation.required"));

        String serviceRequestId = response.extract().jsonPath().getString("serviceRequestId");

        String OTPPauseTransactionId = response.extract().jsonPath().getString("transactionId");

        ValidatableResponse resp = getOTP(serviceRequestId);

        String otp = resp.extract().jsonPath().getString("[0].otp");

        ValidatableResponse response1 = performServiceRequestOTPResumeAndWaitForPauseForSyncAPI(serviceFlow,
                set("resumeServiceRequestId", serviceRequestId),
                set("otp", otp)
        );

        String OTPResumeTransactionId = response1.extract().jsonPath().getString("transactionId");
        assertThat(OTPResumeTransactionId, IsEqual.equalTo(OTPPauseTransactionId));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse performCashOutToPseudoUserUsingIVRAndValidateOTP(User sender, User transactor, PseudoUser pseudoUser, BigDecimal txnAmt, ExtentTest node) throws Exception {

        Markup m = MarkupHelper.createLabel("performCashOutToPseudoUserUsingIVRAndValidateOTP: API", ExtentColor.PINK);
        node.info(m); // Method Start Marker

        String serviceFlow = Services.CASHOUT;

        ValidatableResponse response = performServiceRequestAndWaitForPauseForSyncAPI(serviceFlow,
                set("bearerCode", "IVR"),
                set("transactor.idValue", pseudoUser.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("initiator", "withdrawer"),
                delete("receiver"),
                delete("depositor"),
                put("$", "withdrawer", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12")
                ))
        ).assertThat().body("code", IsEqual.equalTo("otp.validation.required"));

        String serviceRequestId = response.extract().jsonPath().getString("serviceRequestId");

        String OTPPauseTransactionId = response.extract().jsonPath().getString("transactionId");

        ValidatableResponse resp = getOTP(serviceRequestId);

        String otp = resp.extract().jsonPath().getString("[0].otp");

        ValidatableResponse response1 = performServiceRequestOTPResumeAndWaitForPauseForSyncAPI(serviceFlow,
                set("resumeServiceRequestId", serviceRequestId),
                set("otp", otp)
        );

        String OTPResumeTransactionId = response1.extract().jsonPath().getString("transactionId");
        assertThat(OTPResumeTransactionId, IsEqual.equalTo(OTPPauseTransactionId));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse performCashOutWithInvalidSubsMpinTpin(User sender, User transactor, BigDecimal txnAmt, String mpin, String tpin) throws Exception {

        Markup m = MarkupHelper.createLabel("performCashOut: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.CASHOUT;

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceFlow,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("initiator", "withdrawer"),
                delete("receiver"),
                delete("depositor"),
                put("$", "withdrawer", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12"),
                        set("mpin", mpin),
                        set("tpin", tpin)
                ))
        );

        return new SfmResponse(response, pNode);
    }

    /**
     * TODO - Js docs and Move this to transaction
     *
     * @param wholesaler
     * @param empId
     * @throws Exception
     */
    public void createEmployee(User wholesaler, String empId) throws Exception {
        Markup m = MarkupHelper.createLabel("createEmployee: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        //create employee
        OldTxnOperations x = new OldTxnOperations(TransactionType.CREATE_SHIFTBASED_EMPLOYEE,
                set("COMMAND.MSISDN", wholesaler.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", "1"),
                set("COMMAND.EMPID", empId)
        );
        // post the request
        pNode.info(x.bodyString);

        TxnResponse employeeCreation = x.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert) {
            if (employeeCreation.TxnStatus.equals(Constants.TXN_SUCCESS)) {
                pNode.pass("Successfully created employee with txnId: " + employeeCreation.TxnId + " and employeeID: " + empId);
            } else {
                pNode.fail("Employee crestion wasn't successful" + employeeCreation.Message);
                Assert.fail("Employee crestion wasn't successful");
            }
        }

    }

    public TxnResponse subscriberBalanceEnquiry(User sub) throws IOException {
        Markup m = MarkupHelper.createLabel("Subscriber Balance Enquiry: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUB_BALANCE_ENQUIRY,
                set("COMMAND.MSISDN", sub.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse subscriberBalanceEnquiryWithInvalidMpinTipn(User sub, String mpin, String tpin) throws IOException {
        Markup m = MarkupHelper.createLabel("Subscriber Balance Enquiry: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUB_BALANCE_ENQUIRY,
                set("COMMAND.MSISDN", sub.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.MPIN", mpin),
                set("COMMAND.PIN", tpin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public Transactions channelUsrBalanceEnquiryBANK(User chnlUser, String providerId, String bankId, String accNo) throws IOException {
        Markup m = MarkupHelper.createLabel("Channel User's balance Enquiry for Bank: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BALANCE_ENQ_BANK_CHANNELUSR,
                set("COMMAND.MSISDN", chnlUser.MSISDN),
                set("COMMAND.PROVIDER", providerId),
                set("COMMAND.BANKID", bankId),
                set("COMMAND.ACCNO", accNo),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        response.verifyStatus(Constants.TXN_SUCCESS);
        return this;
    }

    public TxnResponse enableStandardInstructionsByChannelUser(User chnlUser, User sub) throws IOException {
        Markup m = MarkupHelper.createLabel("enable Standard Instructions By Channel User: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.ENABLE_STANDARD_INSTRUCTIONS_CHANLUSER,
                set("COMMAND.MSISDN", chnlUser.MSISDN),
                set("COMMAND.MSISDN2", sub.MSISDN),
                set("COMMAND.MSISDN3", chnlUser.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PROVIDER2", defaultProvider.ProviderId),
                set("COMMAND.PROVIDER3", defaultProvider.ProviderId),
                set("COMMAND.PAYID2", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.PAYID3", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse enableStandardInstructionsBySubscriber(User sub1, User sub2, String amount, String frequency) throws IOException {
        Markup m = MarkupHelper.createLabel("enableStandardInstructionsBySubscriber", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.ENABLE_STANDARD_INSTRUCTIONS_SUBSCRIBER,
                set("COMMAND.MSISDN", sub1.MSISDN),
                set("COMMAND.MSISDN2", sub2.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PROVIDER2", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.PAYID2", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.FREQUENCY", frequency),
                set("COMMAND.DAY", " "),
                set("COMMAND.MONTH", " "),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse disableStandardInstructionsBySubscriber(User sub1, User sub2) throws IOException {
        Markup m = MarkupHelper.createLabel("disableStandardInstructionsBySubscriber", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DISABLE_STANDARD_INSTRUCTIONS_SUBSCRIBER,
                set("COMMAND.MSISDN", sub1.MSISDN),
                set("COMMAND.MSISDN2", sub2.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public Transactions accountClosurebyAgentConfirmation(String subMsisdn, String txnid) throws IOException {
        Markup m = MarkupHelper.createLabel("subsAccountClosurebyAgent: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBS_ACCOUNT_CLOSURE_BY_AGENT,
                set("COMMAND.MSISDN", subMsisdn),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.TXNID", txnid),
                set("COMMAND.STATUS", "0"),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        response.verifyStatus(Constants.TXN_SUCCESS);
        return this;
    }

    public SfmResponse inversec2cConfirmation(User payer, String requestId, boolean... success) throws Exception {
        Markup m = MarkupHelper.createLabel("inversec2cConfirmation: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        boolean isSuccess = success.length > 0 ? success[0] : true;
        ValidatableResponse response;
        if (isSuccess) {
            response = performServiceRequestResumeAndWaitForSuccessForSyncAPI(Services.INVERSEC2C,
                    set("resumeServiceRequestId", requestId),
                    set("currency", defaultProvider.ProviderId),
                    put("$", "mfsTenantId", "mfsPrimaryTenant"),
                    set("party.idValue", payer.MSISDN),
                    set("party.mpin", ConfigInput.mPin),
                    set("party.tpin", ConfigInput.tPin),
                    set("party.password", ConfigInput.mPin)
            );
            //Assertion.verifyResponseSucceeded(response, pNode);
            return new SfmResponse(response, pNode);
        } else {
            response = performServiceRequestResumeAndWaitForFailedForSyncAPI(Services.INVERSEC2C,
                    set("resumeServiceRequestId", requestId),
                    set("currency", defaultProvider.ProviderId),
                    put("$", "mfsTenantId", "mfsPrimaryTenant"),
                    set("party.idValue", payer.MSISDN),
                    set("party.mpin", ConfigInput.mPin),
                    set("party.tpin", ConfigInput.tPin),
                    set("party.password", ConfigInput.mPin)
            );

            //Assertion.verifyResponseFailed(response, pNode);
            return new SfmResponse(response, pNode);
        }

    }

    public SfmResponse inversec2cConfirmationChangingMPINTPIN(User payer, String requestId, String mpin, String tpin) throws Exception {

        Markup m = MarkupHelper.createLabel("inversec2cConfirmation: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        ValidatableResponse response = performServiceRequestResumeAndWaitForFailedForSyncAPI(Services.INVERSEC2C,
                set("resumeServiceRequestId", requestId),
                set("currency", defaultProvider.ProviderId),
                put("$", "mfsTenantId", "mfsPrimaryTenant"),
                set("party.idValue", payer.MSISDN),
                set("party.mpin", mpin),
                set("party.tpin", tpin),
                set("party.password", ConfigInput.mPin)
        );

        return new SfmResponse(response, pNode);

    }

    public Transactions startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public Transactions autoDebitSubsSelfReq(String billerCode, String customerMsisdn, String billAccNum, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("autoDebitSubsSelfReq", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.AUTO_DEBIT_SUBS_SELFREQ,
                set("COMMAND.MSISDN", customerMsisdn),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.MAXAMT", amount),
                set("COMMAND.ACCNO", billAccNum),
                set("COMMAND.BENCODE", billerCode),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        operation.postOldTxnURLForHSB(pNode)
                .verifyStatus(Constants.TXN_SUCCESS);
        return this;
    }

    public String AutoDebitEnableByChannelUser(User usr, Biller biller, String customerMsisdn, String billAccNum, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("AutoDebitEnableByChannelUser", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.AUTO_DEBIT_ENABLE_BY_CHANNELUSER,
                set("COMMAND.MSISDN", usr.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MSISDN1", customerMsisdn),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.MAXAMT", amount),
                set("COMMAND.ACCNO", billAccNum),
                set("COMMAND.BENCODE", biller.BillerCode),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        response.verifyStatus(Constants.TXN_SUCCESS);
        return response.TxnId;
    }

    public Transactions authorizationRequest(User usr1, User usr2, String serviceType, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("authorizationRequest", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.AUTHORIZATION_REQUEST,
                set("COMMAND.REQ", serviceType),
                set("COMMAND.MSISDN", usr1.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MSISDN2", usr2.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.IDNO", usr2.ExternalCode)

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
        }
        return this;
    }

    public SfmResponse initiateCashInOthers(User receiver, User transactor, User depositor, String amount, boolean success) throws IOException {
        Markup m = MarkupHelper.createLabel("Perform Cash In Others: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker
        ValidatableResponse response;

        if (success) {
            response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.CASH_IN_OTHERS,
                    delete("language"),
                    delete("externalReferenceId"),
                    delete("remarks"),
                    delete("transactionMode"),
                    delete("requestedServiceCode"),
                    delete("productOwnerCode"),
                    delete("productBrand"),
                    delete("productCategory"),
                    set("depositor.idValue", depositor.MSISDN),
                    set("depositor.identificationNo", depositor.ExternalCode),
                    delete("depositor.mpin"),
                    delete("depositor.pin"),
                    delete("depositor.tpin"),
                    delete("depositor.password"),
                    set("transactor.idValue", transactor.MSISDN),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.pin", ConfigInput.mPin),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.password", transactor.Password),
                    delete("transactor.txnPassword"),
                    delete("transactor.EmployeeId"),
                    delete("transactor.bankId"),
                    delete("transactor.bankAccountNumber"),
                    set("transactionAmount", amount),
                    set("receiver.idValue", receiver.MSISDN),
                    set("receiver.identificationNo", receiver.ExternalCode),
                    delete("receiver.mpin"),
                    delete("receiver.pin"),
                    delete("receiver.tpin"),
                    delete("receiver.password"),
                    delete("receiver.partyId"),
                    delete("receiver.txnPassword"),
                    delete("receiver.bankId"),
                    delete("receiver.bankAccountNumber")

            ).body("txnStatus", is("TS"));
            Assertion.verifyResponseSucceeded(response, pNode);
            return new SfmResponse(response, pNode);
        } else {
            response = performServiceRequestAndWaitForFailedForSyncAPI(Services.CASH_IN_OTHERS,
                    delete("language"),
                    delete("externalReferenceId"),
                    delete("remarks"),
                    delete("transactionMode"),
                    delete("requestedServiceCode"),
                    delete("productOwnerCode"),
                    delete("productBrand"),
                    delete("productCategory"),
                    set("depositor.idValue", depositor.MSISDN),
                    set("depositor.identificationNo", depositor.ExternalCode),
                    delete("depositor.mpin"),
                    delete("depositor.pin"),
                    delete("depositor.tpin"),
                    delete("depositor.password"),
                    set("transactor.idValue", transactor.MSISDN),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.pin", ConfigInput.mPin),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.password", transactor.Password),
                    delete("transactor.txnPassword"),
                    delete("transactor.EmployeeId"),
                    delete("transactor.bankId"),
                    delete("transactor.bankAccountNumber"),
                    set("transactionAmount", amount),
                    set("receiver.idValue", receiver.MSISDN),
                    set("receiver.identificationNo", receiver.ExternalCode),
                    delete("receiver.mpin"),
                    delete("receiver.pin"),
                    delete("receiver.tpin"),
                    delete("receiver.password"),
                    delete("receiver.partyId"),
                    delete("receiver.txnPassword"),
                    delete("receiver.bankId"),
                    delete("receiver.bankAccountNumber")

            ).body("txnStatus", is("TI"));
            Assertion.verifyResponseFailed(response, pNode);
            return new SfmResponse(response, pNode);
        }


    }

    public String automaticO2C(User usr, String amount, String bankId, String refId) throws IOException {
        Markup m = MarkupHelper.createLabel("automaticO2C", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.AUTOMATIC_O2C,
                set("COMMAND.MSISDN", usr.MSISDN),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.BANKID", bankId),
                set("COMMAND.REFERENCEID", refId)

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
            pNode.info("RESPONSE : " + response.getResponse().extract().asString());
        }
        return response.Message;
    }

    public Transactions autoReimbursement(User usr, String amount, String bankId, String refId) throws IOException {
        Markup m = MarkupHelper.createLabel("autoReimbursement", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.AUTOMATIC_REIMBURSEMENT,
                set("COMMAND.MSISDN", usr.MSISDN),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.BANKID", bankId),
                set("COMMAND.REFERENCEID", refId)

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
        }
        return this;
    }

    public SfmResponse walletToBankService(User subscriber,
                                           String txnAmt,
                                           String bankId,
                                           boolean... success) throws IOException {
        Markup m = MarkupHelper.createLabel("initiate wallet to Bank service: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(subscriber, DataFactory.getDefaultProvider().ProviderId, bankId);
        String subaccount = AccountNum1.get("ACCOUNT_NO");

        String decsubacc = Utils.decryptMessage(subaccount);
        ValidatableResponse response = null;
        try {
            String serviceFlow = Services.WALLET_TO_BANK;

            //setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));
            boolean isSuccess = success.length > 0 ? success[0] : true;

            if (isSuccess) {
                response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                        set("serviceFlowId", ""),
                        set("transactionAmount", txnAmt),
                        set("initiator", "transactor"),
                        set("currency", GlobalData.defaultProvider.ProviderId),
                        set("bearerCode", "USSD"),
                        set("language", "en"),
                        set("externalReferenceId", DataFactory.getRandomNumberAsString(8)),
                        set("remarks", ""),
                        set("transactionMode", ""),
                        set("requestedServiceCode", ""),
                        delete("productOwnerCode"),
                        delete("productBrand"),
                        delete("productCategory"),
                        set("transactor.idType", "mobileNumber"),
                        set("transactor.productId", "12"),
                        set("transactor.tpin", ConfigInput.tPin),
                        set("transactor.idValue", subscriber.MSISDN),
                        set("transactor.mpin", ConfigInput.mPin),
                        set("transactor.password", subscriber.Password),
                        set("transactor.pin", ConfigInput.mPin),
                        set("transactor.txnPassword", null),
                        set("transactor.employeeId", null),
                        set("transactor.bankId", null),
                        set("transactor.bankAccountNumber", null),
                        delete("depositor"),
                        set("receiver.bankId", bankId),
                        set("receiver.bankAccountNumber", decsubacc),
                        delete("receiver.idType"),
                        delete("receiver.idValue"),
                        delete("receiver.productId"),
                        delete("receiver.identificationNo"),
                        delete("receiver.pin"),
                        delete("receiver.mpin"),
                        delete("receiver.tpin"),
                        delete("receiver.password"),
                        delete("receiver.txnPassword"),
                        delete("receiver.partyId")
                ).body("txnStatus", is("TA"));
                pNode.info("Post Request: " + response);
                SfmResponse response1 = new SfmResponse(response, pNode);
                if (ConfigInput.isAssert) {
                    response1.verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
                    response1.verifyMessage("sva.to.bank", response1.TransactionId);
                }

            } else {
                response = performServiceRequestAndWaitForFailedForSyncAPI(serviceFlow,
                        set("serviceFlowId", ""),
                        set("transactionAmount", txnAmt),
                        set("initiator", "transactor"),
                        set("currency", "101"),
                        set("bearerCode", "USSD"),
                        set("language", "en"),
                        set("externalReferenceId", ""),
                        set("remarks", ""),
                        set("transactionMode", ""),
                        set("requestedServiceCode", ""),
                        delete("productOwnerCode"),
                        delete("productBrand"),
                        delete("productCategory"),
                        set("transactor.idType", "mobileNumber"),
                        set("transactor.productId", "12"),
                        set("transactor.tpin", ConfigInput.tPin),
                        set("transactor.idValue", subscriber.MSISDN),
                        set("transactor.mpin", ConfigInput.mPin),
                        set("transactor.password", subscriber.Password),
                        set("transactor.pin", ConfigInput.mPin),
                        set("transactor.txnPassword", null),
                        set("transactor.employeeId", null),
                        set("transactor.bankId", null),
                        set("transactor.bankAccountNumber", null),
                        delete("depositor"),
                        set("receiver.bankId", bankId),
                        set("receiver.bankAccountNumber", decsubacc),
                        delete("receiver.idType"),
                        delete("receiver.idValue"),
                        delete("receiver.productId"),
                        delete("receiver.identificationNo"),
                        delete("receiver.pin"),
                        delete("receiver.mpin"),
                        delete("receiver.tpin"),
                        delete("receiver.password"),
                        delete("receiver.txnPassword"),
                        delete("receiver.partyId")
                );
                pNode.info("Post Request: " + response);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new SfmResponse(response, pNode);
    }

    public SfmResponse initiateOfflineBillPayment(Biller biller, User user, String txnAmt, String accNo) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateOfflineBillPayment: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.AgentAssistedPresentmentBillPayment,
                set("transactionAmount", txnAmt),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                set("transactor.idType", "mobileNumber"),
                set("transactor.idValue", user.MSISDN),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.mpin", ConfigInput.mPin),
                delete("transactor.password"),
                delete("transactor.pin"),
                delete("transactor.txnPassword"),
                delete("transactor.EmployeeId"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("depositor"),
                set("receiver.idType", "billerCode"),
                set("receiver.idValue", biller.BillerCode),
                delete("receiver.identificationNo"),
                delete("receiver.productId"),
                delete("receiver.pin"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.txnPassword"),
                delete("receiver.bankId"),
                delete("receiver.bankAccountNumber"),
                delete("receiver.partyId"),
                put("$", "billDetails", serviceRequestDetails(
                        set("billAccountNumber", accNo),
                        set("billNumber", accNo),
                        delete("productId"),
                        delete("idType"),
                        delete("idValue"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("encryptedPassword"),
                        delete("employeeId")))
        );
        if (ConfigInput.isAssert)
            Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    public SfmResponse initiateOfflineBillPaymentForAdhoc(Biller biller, User user, String txnAmt, String accNo) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateOfflineBillPayment: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.AgentAssistedBillPayment,
                set("transactionAmount", txnAmt),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                set("transactor.idType", "mobileNumber"),
                set("transactor.idValue", user.MSISDN),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.mpin", ConfigInput.mPin),
                delete("transactor.password"),
                delete("transactor.pin"),
                delete("transactor.txnPassword"),
                delete("transactor.EmployeeId"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("depositor"),
                set("receiver.idType", "billerCode"),
                set("receiver.idValue", biller.BillerCode),
                delete("receiver.identificationNo"),
                delete("receiver.productId"),
                delete("receiver.pin"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.txnPassword"),
                delete("receiver.bankId"),
                delete("receiver.bankAccountNumber"),
                delete("receiver.partyId"),
                put("$", "billDetails", serviceRequestDetails(
                        set("billAccountNumber", accNo),
                        delete("productId"),
                        delete("idType"),
                        delete("idValue"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("encryptedPassword"),
                        delete("employeeId")))
        );

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    public String bankCashOutInitiate(User subs, User channeluser, String channaccount, String subsaccount, int amount) throws Exception {
        Markup m = MarkupHelper.createLabel("BankCashOutInitiate " + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String txnid = null;
        OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_CASH_OUT,
                set("COMMAND.MSISDN", channeluser.MSISDN),
                set("COMMAND.MSISDN2", subs.MSISDN),

                set("COMMAND.BANKID", DataFactory.getDefaultBankIdForDefaultProvider()),
                set("COMMAND.BANKID2", DataFactory.getDefaultBankIdForDefaultProvider()),
                set("COMMAND.ACCNO", channaccount),
                set("COMMAND.ACCNO2", subsaccount),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.LANGUAGE1", "1")
        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
        }
        return response.TxnId;
    }

    public String bankCashInInitiate(User subs, User channeluser, String channaccount, String subsaccount, int amount) throws Exception {
        Markup m = MarkupHelper.createLabel("BankCashInInitiate " + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String txnid = null;
        CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_CASH_IN,
                set("COMMAND.MSISDN", channeluser.MSISDN),
                set("COMMAND.MSISDN2", subs.MSISDN),

                set("COMMAND.BANKID", DataFactory.getDefaultBankIdForDefaultProvider()),
                set("COMMAND.BANKID2", DataFactory.getDefaultBankIdForDefaultProvider()),
                set("COMMAND.ACCNO", channaccount),
                set("COMMAND.ACCNO2", subsaccount),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.LANGUAGE1", "1")
        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
        }
        return response.TxnId;
    }

    public SfmResponse cashoutApprovalBysubs(User subs, String requestId) throws IOException {
        Markup m = MarkupHelper.createLabel("cashoutApprovalBysubs: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker

        JigsawOperations.setUseServiceInBody(false);
        ValidatableResponse response = performServiceRequestResumeAndWaitForSuccessForSyncAPI(Services.CASHOUT,
                set("resumeServiceRequestId", requestId),
                set("currency", defaultProvider.ProviderId),
                set("party.idValue", subs.MSISDN),
                set("party.mpin", ConfigInput.mPin),
                set("party.tpin", ConfigInput.tPin),
                set("party.password", ConfigInput.mPin)
        );

        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);
        }

        return new SfmResponse(response, pNode);
    }

   /* public Transactions Last_Pending_Transaction(User user, String noOfTransactions) throws IOException {

       try{ Markup m = MarkupHelper.createLabel("Last_Pending_Transaction", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.Last_N_Pending_Transaction,
                set("COMMAND.MSISDN", user.MSISDN),
                //set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.NOOFTXNREQ", noOfTransactions),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin)
        );

        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        response.verifyStatus(Constants.TXN_SUCCESS);
        return this;

    } catch (Exception e) {
        Assertion.raiseExceptionAndContinue(e, pNode);
    }
        return null;
    }*/

    public SfmResponse cashoutTwoStepApprovalBysubs(User subs, String requestId) throws IOException {
        Markup m = MarkupHelper.createLabel("cashoutApprovalBysubs: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker

        JigsawOperations.setUseServiceInBody(false);
        ValidatableResponse response = performServiceRequestResumeAndWaitForSuccessForSyncAPI(Services.CASHOUT,
                set("resumeServiceRequestId", requestId),
                set("currency", defaultProvider.ProviderId),
                set("party.idValue", subs.MSISDN),
                set("party.mpin", ConfigInput.mPin),
                set("party.tpin", ConfigInput.tPin),
                set("party.password", ConfigInput.mPin)
        );

        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);
        }

        return new SfmResponse(response, pNode);
    }

    public Transactions changeempUserMPinTPin(User user, Employee employee) throws IOException {
        Markup m = MarkupHelper.createLabel("changeempChannelUserMPinTPin: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String defaultPin = null;
        try {
            if (AppConfig.defaultPin != null) {
                defaultPin = AppConfig.defaultPin;
            } else {
                // TODO
                pNode.warning("Default Pin is not set, TODO - please complete the Fetching of MPIN TPIN Code!!");
            }

            OldTxnOperations x = new OldTxnOperations(TransactionType.CHANGE_MPIN_CHANNEL_USR,
                    set("COMMAND.MSISDN", user.MSISDN),
                    set("COMMAND.MPIN", employee.ID + defaultPin),
                    set("COMMAND.NEWMPIN", employee.ID + ConfigInput.mPin),
                    set("COMMAND.CONFIRMMPIN", employee.ID + ConfigInput.mPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
            );
            // post the request
            pNode.info(x.bodyString);
            TxnResponse mPinChange = x.postOldTxnURLForHSB(pNode);

            // if successfully changed the mPin then Change the tPin
            if (mPinChange.TxnStatus.equals(Constants.TXN_SUCCESS) &&
                    AppConfig.isTPinAuthentication &&
                    !user.CategoryCode.equals(Constants.ENTERPRISE)) {
                OldTxnOperations y = new OldTxnOperations(TransactionType.CHANGE_TPIN_CHANNEL_USR,
                        set("COMMAND.MSISDN", user.MSISDN),
                        set("COMMAND.MPIN", employee.ID + ConfigInput.mPin),
                        set("COMMAND.PIN", employee.ID + defaultPin),
                        set("COMMAND.NEWPIN", employee.ID + ConfigInput.tPin),
                        set("COMMAND.CONFIRMPIN", employee.ID + ConfigInput.tPin),
                        set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
                );
                pNode.info(y.bodyString);
                y.postOldTxnURLForHSB(pNode)
                        .verifyStatus(Constants.TXN_SUCCESS);
            } else {
                // set the user object's flag
                user.setError("Failed to Change The MPIN");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public String bankCashOutConfirmation(User subs, String txnid) throws Exception {
        Markup m = MarkupHelper.createLabel("BankCashOutInitiate " + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m);

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_CASH_OUT_CONFIRM,
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.TXNID", txnid)

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert) {
            response.verifyStatus(Constants.TXN_SUCCESS);
        }
        return response.Message;

    }

    public TxnResponse Last_Pending_Transaction(User user, String noOfTransactions) throws Exception {
        Markup m = MarkupHelper.createLabel("Last_Pending_Transaction: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        TxnResponse response = null;
        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.Last_N_Pending_Transaction,
                    set("COMMAND.MSISDN", user.MSISDN),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.NOOFTXNREQ", noOfTransactions)

            );
            pNode.info(operation.bodyString);
            response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);

            if (ConfigInput.isAssert) {
                response.assertStatus(Constants.TXN_SUCCESS);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return response;
    }

    /**
     * @param subs
     * @param Beneficiary
     * @param Accountno
     * @return
     * @throws IOException
     */
    public TxnResponse addBeneficiaryfordefaultprovider(User subs, User Beneficiary, String Accountno) throws IOException {
        Markup m = MarkupHelper.createLabel("addBeneficiary", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String defaultBankId = DataFactory.getDefaultBankIdForDefaultProvider();
        OldTxnOperations operation = new OldTxnOperations(TransactionType.ADD_BENIFICIARY_BANK,
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYEENICKNAME", subs.LastName),
                set("COMMAND.PAYEEMOBILE", Beneficiary.MSISDN),
                set("COMMAND.ACCNO", Accountno),
                set("COMMAND.BRANCHCODE", defaultBankId),
                set("COMMAND.BANKID", defaultBankId),
                set("COMMAND.IFSCCODE", defaultBankId)
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse addBeneficiaryfornondefaultprovider(User subs, User Beneficiary, String Accountno) throws IOException {
        Markup m = MarkupHelper.createLabel("addBeneficiary", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String defaultBankIdForNonDefaultProvider = DataFactory.getDefaultBankIdForNonDefaultProvider();
        OldTxnOperations operation = new OldTxnOperations(TransactionType.ADD_BENIFICIARY_BANK,
                set("COMMAND.PROVIDER", DataFactory.getNonDefaultProvider().ProviderId),
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYEENICKNAME", Beneficiary.FirstName),
                set("COMMAND.PAYEEMOBILE", Beneficiary.MSISDN),
                set("COMMAND.ACCNO", Accountno),
                set("COMMAND.BRANCHCODE", defaultBankIdForNonDefaultProvider),
                set("COMMAND.BANKID", defaultBankIdForNonDefaultProvider),
                set("COMMAND.IFSCCODE", defaultBankIdForNonDefaultProvider)
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse deleteBeneficiary(User subs) throws IOException {
        Markup m = MarkupHelper.createLabel("deleteBeneficiary", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DELETE_BENIFICIARY,
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PAYEENICKNAME", subs.LastName)

        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public SfmResponse commissionDisbursement(OperatorUser channelAdmin, User channelUser, String accno) throws IOException {
        Markup m = MarkupHelper.createLabel("CommissionDisbursement", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker
        CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
        createFlowForCommissionDisbursementToBankTransferAsync(Services.Commission_Disbursement, Services.Commission_Disbursement);

        deleteAllPricingPolicies();
        String serviceFlow = "COMDISB";
        setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));
        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.Commission_Disbursement,
                set("bearerCode", "WEB"),
                set("initiator", "transactor"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", channelAdmin.LoginId),
                set("transactor.password", channelAdmin.Password),
                set("receiver.idValue", channelUser.MSISDN),
                delete("receiver.productId"),
                set("receiver.bankId", DataFactory.getDefaultBankIdForDefaultProvider()),
                set("receiver.bankAccountNumber", accno),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", channelUser.MSISDN),
                        set("productId", "13"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("employeeId"),
                        delete("bankId"),
                        delete("bankAccountNumber")
                )),
                delete("depositor")
        ).body("txnStatus", is("TA"));

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);
    }

    public SfmResponse initiateEscrowToEscrowTransfer(SuperAdmin user, String senderBankId, String recieverBankId, String encryptedPwd, String txnAmt) throws IOException {
        Markup m = MarkupHelper.createLabel("initiate Escrow To Escrow Transfer: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForPauseForSyncAPI(Services.ESCROW_TO_ESCROW,
                set("transactionAmount", txnAmt),
                set("bearerCode", "WEB"),
                put("$", "refNumber", String.valueOf(getRandomNumber(3))),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", user.LoginId),
                put("transactor", "encryptedPassword", encryptedPwd),
                delete("transactor.tpin"),
                delete("transactor.mpin"),
                delete("transactor.password"),
                delete("transactor.pin"),
                delete("transactor.txnPassword"),
                delete("transactor.employeeId"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("depositor"),
                set("receiver.idType", "systemId"),
                set("receiver.idValue", "IND04"),
                set("receiver.productId", "12"),
                set("receiver.bankId", recieverBankId),
                delete("receiver.identificationNo"),
                delete("receiver.pin"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.txnPassword"),
                delete("receiver.bankAccountNumber"),
                delete("receiver.partyId"),
                put("$", "sender", serviceRequestDetails(
                        delete("pin"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("txnPassword"),
                        delete("bankAccountNumber"),
                        delete("partyId"),
                        delete("identificationNo"),
                        delete("encryptedPassword"),
                        delete("passcode"),
                        delete("EmployeeId"),
                        delete("billAccountNumber"),
                        delete("billNumber"),
                        set("idType", "systemId"),
                        set("idValue", "IND04"),
                        set("productId", "12"),
                        set("bankId", senderBankId))

                ));

        pNode.info("Post Request: " + response);
        return new SfmResponse(response, pNode);
    }

    public SfmResponse escrowToEscrowTransferApproval(SuperAdmin user, String serviceReqId) throws IOException {
        Markup m = MarkupHelper.createLabel("Escrow To Escrow Transfer Approval: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse approvalResponse = performServiceRequestResumeAndWaitForSuccessForSyncAPI(Services.ESCROW_TO_ESCROW,
                set("resumeServiceRequestId", serviceReqId),
                set("bearerCode", "WEB"),
                set("party.idValue", user.LoginId),
                set("party.idType", "loginId"),
                set("party.password", user.Password)
        );

        pNode.info("Post Request: " + approvalResponse);
        return new SfmResponse(approvalResponse, pNode);
    }

    public SfmResponse initiateTransactionReversalByOperator(OperatorUser user, String txnId, String productId, String serviceToBeReversed) throws IOException {
        Markup m = MarkupHelper.createLabel("initiate transaction reversal: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.TXN_CORRECTION;

        setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", serviceToBeReversed));

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceFlow,
                delete("serviceFlowId"),
                delete("depositor"),
                delete("receiver"),
                delete("transactionAmount"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("externalReferenceId"),
                delete("requestedServiceCode"),
                delete("transactionMode"),
                delete("remarks"),
                set("bearerCode", "WEB"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", user.LoginId),
                set("transactor.password", user.Password),
                put("$", "isServiceChargeReversible", "false"),
                put("$", "isCommissionReversible", "false"),
                put("$", "originalTransaction", serviceRequestDetails(
                        set("idType", "transferId"),
                        set("idValue", txnId),
                        set("productId", productId)
                ))
        );

        pNode.info("Post Request: " + response);
        return new SfmResponse(response, pNode);
    }

    public SfmResponse initiateMerchantPaymentForFailure(User merchant, User subscriber, String amount, String... walletNo) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateMerchantPayment: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String productId = (walletNo.length > 0) ? walletNo[0] : DataFactory.getDefaultWallet().WalletId;

        String serviceFlow = "MERCHPAY";

        setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceFlow,
                set("transactor.idValue", merchant.MSISDN),
                set("transactor.productId", productId),
                delete("receiver"),
                delete("depositor"),
                set("initiator", "sender"),
                set("transactionAmount", amount),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", subscriber.MSISDN),
                        set("tpin", ConfigInput.tPin),
                        set("mpin", ConfigInput.mPin),
                        set("identificationNo", subscriber.ExternalCode)
                ))
        );
        return new SfmResponse(response, pNode);
    }

    public SfmResponse initiateTransactionReversalByChannelUser(User user, String txnId, String productId, String serviceToBeReversed) throws IOException {
        Markup m = MarkupHelper.createLabel("initiate transaction reversal: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.TXN_CORRECTION;

        setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", serviceToBeReversed));

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceFlow,
                delete("depositor"),
                delete("receiver"),
                delete("transactionAmount"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("externalReferenceId"),
                delete("requestedServiceCode"),
                delete("transactionMode"),
                delete("remarks"),
                set("bearerCode", "WEB"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", user.LoginId),
                set("transactor.password", user.Password),
                set("transactor.productId", productId),
                put("$", "isServiceChargeReversible", "false"),
                put("$", "isCommissionReversible", "false"),
                put("$", "originalTransaction", serviceRequestDetails(
                        set("idType", "transferId"),
                        set("idValue", txnId),
                        set("productId", productId)
                ))
        );

        pNode.info("Post Request: " + response);
        return new SfmResponse(response, pNode);
    }

    public SfmResponse stockLiquidationGeneration(User sender, String batchId) throws Exception {

        Markup m = MarkupHelper.createLabel("StockLiquidationGeneration: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPIWithoutTransactionid(Services.STOCK_LIQUIDATION,
                put("$", "batchId", batchId),
                set("initiator", "sender"),
                delete("transactionAmount"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("language"),
                delete("requestedServiceCode"),
                delete("transactionMode"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("depositor"),
                delete("transactor"),
                delete("receiver"),
                delete("sender"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        delete("productId"),
                        delete("billNumber"),
                        delete("billAccountNumber"),
                        delete("encryptedPassword"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("EmployeeId"))));
        ;
        return new SfmResponse(response, pNode);
    }

    public SfmResponse stockLiquidationGenerationForBiller(Biller biller, String batchId) throws Exception {

        Markup m = MarkupHelper.createLabel("StockLiquidationGenerationForBiller: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPIWithoutTransactionid(Services.STOCK_LIQUIDATION,
                put("$", "batchId", batchId),
                set("initiator", "sender"),
                delete("transactionAmount"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("language"),
                delete("requestedServiceCode"),
                delete("transactionMode"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("depositor"),
                delete("transactor"),
                delete("receiver"),
                delete("sender"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "billerCode"),
                        set("idValue", biller.BillerCode),
                        delete("productId"),
                        delete("billNumber"),
                        delete("billAccountNumber"),
                        delete("encryptedPassword"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("EmployeeId"))));
        ;
        return new SfmResponse(response, pNode);
    }

    public SfmResponse stockLiquidationGenerationForBillerForFail(Biller biller) throws Exception {

        Markup m = MarkupHelper.createLabel("StockLiquidationGenerationForBiller: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(Services.STOCK_LIQUIDATION,
                put("$", "batchId", DataFactory.getRandomNumberAsString(8)),
                set("initiator", "sender"),
                delete("transactionAmount"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("language"),
                delete("requestedServiceCode"),
                delete("transactionMode"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("depositor"),
                delete("transactor"),
                delete("receiver"),
                delete("sender"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "billerCode"),
                        set("idValue", biller.BillerCode),
                        delete("productId"),
                        delete("billNumber"),
                        delete("billAccountNumber"),
                        delete("encryptedPassword"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("employeeId"))));
        ;
        return new SfmResponse(response, pNode);
    }

    public SfmResponse stockLiquidationServiceforFail(User sender, OperatorUser transactor, String LiquidationId, String BankId) throws Exception {

        Markup m = MarkupHelper.createLabel("StockLiquidationServiceforFail: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(Services.STOCK_LIQUIDATION_SERVICE,
                set("bearerCode", "WEB"),
                set("initiator", "transactor"),
                set("transactionAmount", "50"),
                set("remarks", "StockLiquidationService"),
                set("transactionMode", "transactionMode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("requestedServiceCode"),
                delete("externalReferenceId"),
                put("$", "liqTransactionId", LiquidationId),
                put("$", "approvedStatus", "Y"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", transactor.LoginId),
                set("transactor.productId", "12"),
                set("transactor.password", transactor.Password),
                delete("transactor.mpin"),
                delete("transactor.tpin"),
                delete("transactor.pin"),
                delete("transactor.txnPassword"),
                delete("transactor.txnPassword"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("transactor.employeeId"),
                delete("depositor"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12"),
                        delete("billNumber"),
                        delete("billAccountNumber"),
                        delete("encryptedPassword"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("bankAccountNumber"),
                        delete("bankId"),
                        delete("employeeId"))),
                set("receiver.idType", "systemId"),
                set("receiver.idValue", "IND04"),
                set("receiver.bankId", BankId),
                delete("receiver.productId"),
                delete("receiver.identificationNo"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.pin"),
                delete("receiver.txnPassword"),
                delete("receiver.employeeId"),
                delete("receiver.partyId"));

        return new SfmResponse(response, pNode);
    }

    public TxnResponse BalanceEnquiryWithInvalidMpinTpin(User chanlUser, String mpin, String tpin) throws IOException {
        Markup m = MarkupHelper.createLabel("Channel User's Balance Enquiry: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BALANCE_ENQUIRY,
                set("COMMAND.MSISDN", chanlUser.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.PAYID", DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId),
                set("COMMAND.MPIN", mpin),
                set("COMMAND.PIN", tpin),
                set("COMMAND.LANGUAGE1", "1")
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public SfmResponse stockLiquidationServiceforBillerFail(Biller biller, OperatorUser transactor, String LiquidationId, String BankId) throws Exception {

        Markup m = MarkupHelper.createLabel("StockLiquidationServiceforFail: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(Services.STOCK_LIQUIDATION_SERVICE,
                set("bearerCode", "WEB"),
                set("initiator", "transactor"),
                set("transactionAmount", "50"),
                set("remarks", "StockLiquidationService"),
                set("transactionMode", "transactionMode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("requestedServiceCode"),
                delete("externalReferenceId"),
                put("$", "liqTransactionId", LiquidationId),
                put("$", "approvedStatus", "Y"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", transactor.LoginId),
                set("transactor.productId", "12"),
                set("transactor.password", transactor.Password),
                delete("transactor.mpin"),
                delete("transactor.tpin"),
                delete("transactor.pin"),
                delete("transactor.txnPassword"),
                delete("transactor.txnPassword"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("transactor.employeeId"),
                delete("depositor"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "billerCode"),
                        set("idValue", biller.BillerCode),
                        set("productId", "12"),
                        delete("billNumber"),
                        delete("billAccountNumber"),
                        delete("encryptedPassword"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("bankAccountNumber"),
                        delete("bankId"),
                        delete("employeeId"))),
                set("receiver.idType", "systemId"),
                set("receiver.idValue", "IND04"),
                set("receiver.bankId", BankId),
                delete("receiver.productId"),
                delete("receiver.identificationNo"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.pin"),
                delete("receiver.txnPassword"),
                delete("receiver.employeeId"),
                delete("receiver.partyId"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse stockLiquidationService(User sender, OperatorUser transactor, String LiquidationId, String BankId) throws Exception {

        Markup m = MarkupHelper.createLabel("StockLiquidationService: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker


        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.STOCK_LIQUIDATION_SERVICE,
                set("bearerCode", "WEB"),
                set("initiator", "transactor"),
                set("transactionAmount", "30"),
                set("remarks", "StockLiquidationService"),
                set("transactionMode", "transactionMode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("requestedServiceCode"),
                delete("externalReferenceId"),
                put("$", "liqTransactionId", LiquidationId),
                put("$", "approvedStatus", "Y"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", transactor.LoginId),
                set("transactor.productId", "12"),
                set("transactor.password", transactor.Password),
                delete("transactor.mpin"),
                delete("transactor.tpin"),
                delete("transactor.pin"),
                delete("transactor.txnPassword"),
                delete("transactor.txnPassword"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("transactor.employeeId"),
                delete("depositor"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", "12"),
                        delete("billNumber"),
                        delete("billAccountNumber"),
                        delete("encryptedPassword"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("bankAccountNumber"),
                        delete("bankId"),
                        delete("employeeId"))),
                set("receiver.idType", "systemId"),
                set("receiver.idValue", "IND04"),
                set("receiver.bankId", BankId),
                delete("receiver.productId"),
                delete("receiver.identificationNo"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.pin"),
                delete("receiver.txnPassword"),
                delete("receiver.employeeId"),
                delete("receiver.partyId"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse stockLiquidationServiceForBiller(Biller biller, OperatorUser transactor, String LiquidationId, String BankId) throws Exception {

        Markup m = MarkupHelper.createLabel("StockLiquidationService: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.STOCK_LIQUIDATION_SERVICE,
                set("bearerCode", "WEB"),
                set("initiator", "transactor"),
                set("transactionAmount", "50"),
                set("remarks", "StockLiquidationService"),
                set("transactionMode", "transactionMode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("requestedServiceCode"),
                delete("externalReferenceId"),
                put("$", "liqTransactionId", LiquidationId),
                put("$", "approvedStatus", "Y"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", transactor.LoginId),
                set("transactor.productId", "12"),
                set("transactor.password", transactor.Password),
                delete("transactor.mpin"),
                delete("transactor.tpin"),
                delete("transactor.pin"),
                delete("transactor.txnPassword"),
                delete("transactor.txnPassword"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("transactor.employeeId"),
                delete("depositor"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "billerCode"),
                        set("idValue", biller.BillerCode),
                        set("productId", "12"),
                        delete("billNumber"),
                        delete("billAccountNumber"),
                        delete("encryptedPassword"),
                        delete("identificationNo"),
                        delete("mpin"),
                        delete("tpin"),
                        delete("password"),
                        delete("pin"),
                        delete("txnPassword"),
                        delete("passcode"),
                        delete("bankAccountNumber"),
                        delete("bankId"),
                        delete("employeeId"))),
                set("receiver.idType", "systemId"),
                set("receiver.idValue", "IND04"),
                set("receiver.bankId", BankId),
                delete("receiver.productId"),
                delete("receiver.identificationNo"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.pin"),
                delete("receiver.txnPassword"),
                delete("receiver.employeeId"),
                delete("receiver.partyId"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse performEIGTransactionforFail(String serviceId, String Status, String service) throws Exception {

        Markup m = MarkupHelper.createLabel("performEIGTransactionforFail", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker


        String serviceCodeResume = Services.RESUME_STOCK_LIQUIDATION_SERVICE;
        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceCodeResume,
                put("$", "resumeServiceRequestId", serviceId),
                put("$", "transactionStatus", Status),
                delete("bearerCode"),
                delete("initiator"),
                delete("language"),
                delete("currency"),
                delete("transactionAmount"),
                delete("remarks"),
                delete("transactionMode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("requestedServiceCode"),
                delete("externalReferenceId"),
                delete("mfsTenantId"),
                delete("serviceFlowId"),
                delete("depositor"),
                delete("transactor"),
                delete("receiver"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse performEIGTransaction(String serviceId, String Status, String service) throws Exception {

        Markup m = MarkupHelper.createLabel("EIG Transaction", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceCodeResume = service;
        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceCodeResume,
                put("$", "resumeServiceRequestId", serviceId),
                put("$", "transactionStatus", Status),
                delete("bearerCode"),
                delete("initiator"),
                delete("language"),
                delete("currency"),
                delete("transactionAmount"),
                delete("remarks"),
                delete("transactionMode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("requestedServiceCode"),
                delete("externalReferenceId"),
                delete("mfsTenantId"),
                set("serviceFlowId", service),
                delete("depositor"),
                delete("transactor"),
                delete("receiver"));

        return new SfmResponse(response, pNode);
    }

    public TxnResponse userenquiryapi(User user, String type) throws IOException {
        Markup m = MarkupHelper.createLabel("userenquiryapi", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.UserenquiryAPI,
                set("COMMAND.IDENTIFICATION", user.MSISDN),
                set("COMMAND.USERROLE", type)

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse fetchListOfCurrenciesAssociatedWithUser(String msisdn, String userType) throws IOException {
        Markup m = MarkupHelper.createLabel("fetchListOfCurrenciesAssociatedWithUser", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.FETCH_LIST_OF_CURRENCIES,
                set("COMMAND.MSISDN", msisdn),
                set("COMMAND.LANGUAGE1", "1"),
                set("COMMAND.USERROLE", userType)
        );

        Assertion.logAsInfo(operation.bodyString, pNode);
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse changeCurrencyCode(User user, String newCurrencyCode, String providerID) throws IOException {
        Markup m = MarkupHelper.createLabel("changeCurrencyCode", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String userType = null;
        if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {

            userType = Constants.CUSTOMER_REIMB;

        } else {
            userType = Constants.CHANNEL_REIMB;
        }

        OldTxnOperations operation = new OldTxnOperations(TransactionType.CHANGE_CURRENCY_CODE,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.LANGUAGE1", "1"),
                set("COMMAND.USERROLE", userType),
                set("COMMAND.NEWCURRENCY", newCurrencyCode),
                set("COMMAND.PROVIDERID", providerID),
                set("COMMAND.MPIN", ConfigInput.mPin)

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse userInfo(User user, String userType) throws IOException {
        Markup m = MarkupHelper.createLabel("userInfo", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DEFAULT_CURRENCY_CODE,
                set("COMMAND.IDENTIFICATION", user.MSISDN),
                set("COMMAND.LEVEL", "HIGH"),
                set("COMMAND.USERROLE", userType)

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse SIMSwap(String msisdn) throws IOException {
        Markup m = MarkupHelper.createLabel("SIMSwapp", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SIM_SWAP,
                set("COMMAND.MSISDN", msisdn),
                set("COMMAND.LANGUAGE1", "1")

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse userEnquiryApiWithouUserRoletab(User user) throws IOException {
        Markup m = MarkupHelper.createLabel("My Account Details", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.UserenquiryAPI,
                set("COMMAND.IDENTIFICATION", user.MSISDN),
                delete("COMMAND.USERROLE")

        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    /**
     * This Method is used to associate a user with a bank
     *
     * @return
     * @throws Exception
     */
    public int associateMSISDNWithBank(User user, OperatorUser networkAdmin, String userType) throws Exception {
        Markup m = MarkupHelper.createLabel("associateMSISDNWithBank: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m);
        int accountNumber = getRandomNumber(8);

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_ASSOCIATION,
                set("COMMAND.BANK_ID", getDefaultBankId(defaultProvider.ProviderId)),
                set("COMMAND.USERTYPE", userType),
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.CREATEDBY", MobiquityGUIQueries.getUserId(networkAdmin.LoginId))

        );
        pNode.info(operation.bodyString);
        TxnResponse res = operation.postOldTxnURLForHSB(pNode);
        if (ConfigInput.isAssert && res.isTxnSuccessful(Constants.TXN_SUCCESS)) {
            return accountNumber;
        }
        return 0;
    }

    /**
     * Bank Association With User
     *
     * @return
     * @throws Exception
     * @deprecated use bankRegistration
     */
    public int bankAssociation(User user, String userType) throws Exception {
        Markup m = MarkupHelper.createLabel("bankRegistration: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        int accountNumber = getRandomNumber(8);
        int customerId = getRandomNumber(5);

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.BANKID", getDefaultBankId(defaultProvider.ProviderId)),
                set("COMMAND.BANKNAME", getDefaultBankName(defaultProvider.ProviderId)),
                set("COMMAND.FNAME", user.FirstName),
                set("COMMAND.LNAME", user.LastName),
                set("COMMAND.IDNO", user.ExternalCode),
                set("COMMAND.BANKACCNUMBER", accountNumber),
                set("COMMAND.CUSTOMERID", customerId),
                set("COMMAND.USERROLE", userType)

        );
        pNode.info(operation.bodyString);

        TxnResponse res = operation.postOldTxnURLForHSB(pNode);
        if (ConfigInput.isAssert && res.isTxnSuccessful(Constants.TXN_SUCCESS)) {
            return accountNumber;
        }

        return 0;
    }

    /**
     * Bank Registration via API
     *
     * @param user
     * @param bank
     * @param userType
     * @return
     * @throws Exception
     */
    public TxnResponse bankRegistration(User user, Bank bank, String userType, String accNum, String custId) throws Exception {
        Markup m = MarkupHelper.createLabel("bankRegistration: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        if (accNum == null)
            accNum = DataFactory.getRandomNumberAsString(8);

        if (custId == null)
            custId = DataFactory.getRandomNumberAsString(8);

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_REGISTRATION,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.PROVIDER", bank.ProviderId),
                set("COMMAND.BANKID", bank.BankID),
                set("COMMAND.BANKNAME", bank.BankName),
                set("COMMAND.FNAME", user.FirstName),
                set("COMMAND.LNAME", user.LastName),
                set("COMMAND.IDNO", user.ExternalCode),
                set("COMMAND.BANKACCNUMBER", accNum),
                set("COMMAND.CUSTOMERID", custId),
                set("COMMAND.USERROLE", userType)

        );
        pNode.info(operation.bodyString);
        TxnResponse res = operation.postOldTxnURLForHSB(pNode);
        if (ConfigInput.isAssert) {
            if (res.isTxnSuccessful(Constants.TXN_SUCCESS))
                res.setSuccess();
        }
        return res;
    }

    /**
     * Bank Association With User
     *
     * @return
     * @throws Exception
     */
    public TxnResponse bankDeAssociation(User user, Bank bank, String accNumber) throws Exception {
        Markup m = MarkupHelper.createLabel("bankDeAssociation: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.BANK_DEREGISTRATION,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.PROVIDER", bank.ProviderId),
                set("COMMAND.BANKID", bank.BankID),
                set("COMMAND.BANKNAME", bank.BankName),
                set("COMMAND.FNAME", user.FirstName),
                set("COMMAND.LNAME", user.LastName),
                set("COMMAND.IDNO", user.ExternalCode),
                set("COMMAND.BANKACCNUMBER", accNumber),
                set("COMMAND.USERROLE", user.getUserRole())

        );
        pNode.info(operation.bodyString);
        TxnResponse res = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert)
            res.verifyStatus(Constants.TXN_SUCCESS);

        return res;
    }

    public SfmResponse bankInitiatedBankToWallet(User sender, String bankId, String accountNumber) throws Exception {

        Markup m = MarkupHelper.createLabel("BankInitiatedBankToWallet: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.BANK_INITIATED_BANK_TO_WALLET,
                set("bearerCode", "USSD"),
                set("initiator", "transactor"),
                set("transactionAmount", "10"),
                delete("requestedServiceCode"),
                delete("externalReferenceId"),
                set("transactor.idType", "mobileNumber"),
                set("transactor.idValue", sender.MSISDN),
                set("transactor.bankId", bankId),
                set("transactor.bankAccountNumber", accountNumber),
                delete("transactor.mpin"),
                delete("transactor.tpin"),
                delete("transactor.pin"),
                delete("transactor.txnPassword"),
                delete("transactor.productId"),
                delete("transactor.employeeId"),
                delete("depositor"),
                set("receiver.idType", "mobileNumber"),
                set("receiver.idValue", sender.MSISDN),
                delete("receiver.identificationNo"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.pin"),
                delete("receiver.txnPassword"),
                delete("receiver.employeeId"),
                delete("receiver.bankId"),
                delete("receiver.partyId"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse bankInitiatedWalletToBank(User sender, String bankId, String accountNumber) {
        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.BANK_INITIATED_WALLET_TO_BANK_,
                set("transactionAmount", "10"),
                set("transactor.idValue", sender.MSISDN),
                delete("transactor.pin"),
                delete("transactor.mpin"),
                delete("transactor.tpin"),
                delete("transactor.password"),
                delete("transactor.txnPassword"),
                delete("transactor.employeeId"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("depositor"),
                set("receiver.bankId", bankId),
                set("receiver.bankAccountNumber", accountNumber),
                set("receiver.idValue", sender.MSISDN),
                delete("receiver.identificationNo"),
                delete("receiver.txnPassword"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.pin"),
                delete("receiver.password"),
                delete("receiver.partyId"));

        return new SfmResponse(response, pNode);
    }

    public SfmResponse initiateTransactionRefundByChannelUser(User user, String refundamount, String txnId, String productId, String serviceToBeReversed) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateTransactionRefundByChannelUser: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.MERCHANT_REFUND;

        setTxnConfig(ImmutableMap.of("initiateTransactionRefundByChannelUser", serviceToBeReversed));

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceToBeReversed,
                delete("depositor"),
                delete("receiver"),
                delete("transactionAmount"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("externalReferenceId"),
                delete("requestedServiceCode"),
                delete("transactionMode"),
                delete("remarks"),
                delete("currency"),
                set("bearerCode", "WEB"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", user.LoginId),
                set("transactor.password", user.Password),
                set("transactor.productId", productId),
                put("$", "refundAmount", refundamount),
                put("$", "isServiceChargeReversible", "false"),
                put("$", "isCommissionReversible", "false"),
                put("$", "originalTransaction", serviceRequestDetails(
                        set("idType", "transferId"),
                        set("idValue", txnId)
                        // set("productId", productId)
                ))
        );
        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);

        }
        return new SfmResponse(response, pNode);

    }

    public SfmResponse initiateTransactionRefundByChannelUserReverseServiceCharge(User user, String refundamount, String txnId, String productId, String serviceToBeReversed) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateTransactionRefundByChannelUserReverseServiceCharge: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.MERCHANT_REFUND;

        setTxnConfig(ImmutableMap.of("initiateTransactionRefundByChannelUser", serviceToBeReversed));

        ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceToBeReversed,
                delete("depositor"),
                delete("receiver"),
                delete("transactionAmount"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                delete("externalReferenceId"),
                delete("requestedServiceCode"),
                delete("transactionMode"),
                delete("remarks"),
                delete("currency"),
                set("bearerCode", "WEB"),
                set("transactor.idType", "loginId"),
                set("transactor.idValue", user.LoginId),
                set("transactor.password", user.Password),
                set("transactor.productId", productId),
                put("$", "refundAmount", refundamount),
                put("$", "isServiceChargeReversible", "true"),
                put("$", "isCommissionReversible", "true"),
                put("$", "originalTransaction", serviceRequestDetails(
                        set("idType", "transferId"),
                        set("idValue", txnId)
                        // set("productId", productId)
                ))
        );
        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);

        }
        return new SfmResponse(response, pNode);

    }

    public SfmResponse bankToWalletService(User usr, String txnAmt, String bankId, String bankAccNo, boolean... isSuccess) throws IOException {
        Markup m = MarkupHelper.createLabel("initiate bank to wallet service: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        boolean successOrFailure = (isSuccess.length > 0) ? isSuccess[0] : true;

        String serviceFlow = Services.BANK_TO_WALLET;

        //setTxnConfig(of("txn.numericId.applicable.services", serviceFlow));
        if (successOrFailure) {
            ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                    set("serviceFlowId", ""),
                    set("transactionAmount", txnAmt),
                    set("initiator", "transactor"),
                    set("currency", "101"),
                    set("bearerCode", "USSD"),
                    set("language", "en"),
                    delete("externalReferenceId"),
                    delete("remarks"),
                    delete("transactionMode"),
                    delete("requestedServiceCode"),
                    delete("productOwnerCode"),
                    delete("productBrand"),
                    delete("productCategory"),
                    set("transactor.idType", "mobileNumber"),
                    set("transactor.productId", "12"),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.idValue", usr.MSISDN),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.password", usr.Password),
                    set("transactor.pin", ConfigInput.mPin),
                    delete("transactor.txnPassword"),
                    delete("transactor.employeeId"),
                    set("transactor.bankId", bankId),
                    set("transactor.bankAccountNumber", bankAccNo),
                    delete("depositor"),
                    delete("receiver.bankId"),
                    delete("receiver.bankAccountNumber"),
                    set("receiver.idType", "mobileNumber"),
                    set("receiver.idValue", usr.MSISDN),
                    set("receiver.productId", "12"),
                    delete("receiver.identificationNo"),
                    delete("receiver.pin"),
                    delete("receiver.mpin"),
                    delete("receiver.tpin"),
                    delete("receiver.password"),
                    delete("receiver.txnPassword"),
                    delete("receiver.partyId")
            ).body("txnStatus", is("TA"));

            pNode.info("Post Request: " + response);
            return new SfmResponse(response, pNode);
        } else {
            ValidatableResponse response = performServiceRequestAndWaitForFailedForSyncAPI(serviceFlow,
                    set("serviceFlowId", ""),
                    set("transactionAmount", txnAmt),
                    set("initiator", "transactor"),
                    set("currency", "101"),
                    set("bearerCode", "USSD"),
                    set("language", "en"),
                    delete("externalReferenceId"),
                    delete("remarks"),
                    delete("transactionMode"),
                    delete("requestedServiceCode"),
                    delete("productOwnerCode"),
                    delete("productBrand"),
                    delete("productCategory"),
                    set("transactor.idType", "mobileNumber"),
                    set("transactor.productId", "12"),
                    set("transactor.tpin", ConfigInput.tPin),
                    set("transactor.idValue", usr.MSISDN),
                    set("transactor.mpin", ConfigInput.mPin),
                    set("transactor.password", usr.Password),
                    set("transactor.pin", ConfigInput.mPin),
                    delete("transactor.txnPassword"),
                    delete("transactor.employeeId"),
                    set("transactor.bankId", bankId),
                    set("transactor.bankAccountNumber", bankAccNo),
                    delete("depositor"),
                    delete("receiver.bankId"),
                    delete("receiver.bankAccountNumber"),
                    set("receiver.idType", "mobileNumber"),
                    set("receiver.idValue", usr.MSISDN),
                    set("receiver.productId", "12"),
                    delete("receiver.identificationNo"),
                    delete("receiver.pin"),
                    delete("receiver.mpin"),
                    delete("receiver.tpin"),
                    delete("receiver.password"),
                    delete("receiver.txnPassword"),
                    delete("receiver.partyId")
            );

            pNode.info("Post Request: " + response);
            return new SfmResponse(response, pNode);
        }
    }

    public Transactions userBarUnbar(User user, String action, String barType) throws Exception {
        Markup m = MarkupHelper.createLabel("userBarUnbar: " + user.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        OldTxnOperations operation;
        try {
            if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                operation = new OldTxnOperations(TransactionType.BARorUNBAR,
                        set("COMMAND.ACTION", action),
                        set("COMMAND.USERTYPE", "CUSTOMER"),
                        set("COMMAND.MSISDN", user.MSISDN),
                        set("COMMAND.BARTYPE", barType),
                        set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

                );
            } else {
                operation = new OldTxnOperations(TransactionType.BARorUNBAR,
                        set("COMMAND.ACTION", action),
                        set("COMMAND.USERTYPE", "CHANNEL"),
                        set("COMMAND.MSISDN", user.MSISDN),
                        set("COMMAND.BARTYPE", barType),
                        set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

                );
            }

            pNode.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);
            return this;

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public Transactions optUserBarUnbar(OperatorUser user, String action, String barType) throws Exception {
        Markup m = MarkupHelper.createLabel("userBarUnbar: " + user.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.BARorUNBAR,
                    set("COMMAND.ACTION", action),
                    set("COMMAND.USERTYPE", "OPERATOR"),
                    set("COMMAND.LOGINID", user.LoginId),
                    set("COMMAND.BARTYPE", barType),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

            );

            pNode.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);
            return this;

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    /**
     * TODO for (Navin) to Complete the API
     *
     * @param subs1
     * @param subs2
     * @return
     * @throws IOException
     */
    public TxnResponse performP2PSendMoneySubs(User subs1, User subs2, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("performP2PSendMoneySubs: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.P2P_SEND_MONEY_SUBS,
                set("COMMAND.MSISDN", subs1.MSISDN),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.PAYID", Constants.NORMAL_WALLET),
                set("COMMAND.PROVIDER2", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.PAYID2", Constants.NORMAL_WALLET),
                set("COMMAND.MSISDN2", subs2.MSISDN),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.LANGUAGE2", Constants.LANGUAGE1)
        );
        Assertion.logAsInfo(operation.bodyString, pNode);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);

        if (ConfigInput.isAssert)
            response.verifyStatus(Constants.TXN_SUCCESS);
        return response;
    }

    /**
     * @param subs
     * @param status Pass status as 0(For Accepting) or 1 (For Reject)
     * @return
     * @throws Exception
     */
    public TxnResponse performSubscriberAcquisition(User subs, String status, String... providerID) throws Exception {
        Markup m = MarkupHelper.createLabel("subscriberAcquisition: " + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String providerIDForAcq;
        if (providerID.length > 0) {
            providerIDForAcq = providerID[0];
        } else {
            providerIDForAcq = defaultProvider.ProviderId;
        }

        TxnResponse response = null;
        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.SUBSCRIBER_AQUISITION,
                    set("COMMAND.MSISDN", subs.MSISDN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    set("COMMAND.PROVIDER", providerIDForAcq),
                    set("COMMAND.STATUS", status)
            );
            pNode.info(operation.bodyString);

            response = operation.postOldTxnURLForHSB(pNode);

            if (ConfigInput.isAssert)
                response.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return response;
    }

    public TxnResponse viewBillBySubscriber(Biller biller, User subscriber) throws Exception {
        Markup m = MarkupHelper.createLabel("viewBillBySubscriber: " + subscriber.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        TxnResponse response = null;
        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.VIEW_BILL,
                    set("COMMAND.MSISDN", subscriber.MSISDN),
                    set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                    set("COMMAND.BILLCCODE", biller.BillerCode),
                    set("COMMAND.BILLANO", biller.BillerCode),
                    set("COMMAND.BILLNO", biller.BillerCode)

            );
            pNode.info(operation.bodyString);

            response = operation.postOldTxnURLForHSB(pNode);

            if (ConfigInput.isAssert)
                response.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return response;
    }

    public TxnResponse viewBillBySubscriber(Biller biller, User subscriber, String bill) throws Exception {
        Markup m = MarkupHelper.createLabel("viewBillBySubscriber: " + subscriber.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        TxnResponse response = null;
        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.VIEW_BILL,
                    set("COMMAND.MSISDN", subscriber.MSISDN),
                    set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                    set("COMMAND.BILLCCODE", biller.BillerCode),
                    set("COMMAND.BILLANO", bill),
                    set("COMMAND.BILLNO", bill),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin)

            );
            pNode.info(operation.bodyString);

            response = operation.postOldTxnURLForHSB(pNode);

            if (ConfigInput.isAssert)
                response.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return response;
    }

    /**
     * @param sub
     * @return
     * @throws Exception
     */
    public TxnResponse subscriberLogin(User sub) throws Exception {
        Markup m = MarkupHelper.createLabel("Subscriber Login: " + sub.LoginId, ExtentColor.BLUE);
        pNode.info(m);
        TxnResponse response = null;

        try {
            OldTxnOperations operation = new OldTxnOperations(TransactionType.VALIDATE_mPIN,
                    set("COMMAND.MSISDN", sub.MSISDN),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                    set("COMMAND.MPIN", ConfigInput.mPin)
            );
            pNode.info(operation.bodyString);

            response = operation.postOldTxnURLForHSB(pNode);

            if (ConfigInput.isAssert) {
                response.verifyStatus(Constants.TXN_SUCCESS);
                Assertion.verifyEqual(response.getMessage(), "valid", "" + sub.FirstName + ": mPin authentication successful: Login Success", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return response;

    }

    public SfmResponse initiateO2C(OperatorUser transactor, User receiver, String amount, String productId, String providerId) throws IOException {

        Markup m = MarkupHelper.createLabel("initiate O2C: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.O2C;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("serviceFlowId", ""),
                set("transactionAmount", amount),
                set("initiator", "transactor"),
                set("currency", providerId),
                set("bearerCode", "WEB"),
                set("language", "en"),
                set("transactor.idType", "loginId"),
                set("transactor.productId", productId),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.idValue", transactor.LoginId),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.password", ConfigInput.defaultPass),
                set("transactor.pin", ConfigInput.mPin),
                delete("depositor"),
                delete("receiver.bankId"),
                delete("receiver.bankAccountNumber"),
                set("receiver.idType", "mobileNumber"),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.productId", productId),
                set("receiver.identificationNo", receiver.ExternalCode),
                set("receiver.pin", ConfigInput.tPin),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin));

        Assertion.verifyResponsePaused(response, pNode);
        return new SfmResponse(response, pNode);

    }

    public SfmResponse approveO2C(OperatorUser party, String serviceReqId) throws IOException {

        Markup m = MarkupHelper.createLabel("Approve O2C: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.O2C;
        SfmResponse res = null;

        ValidatableResponse response = performServiceRequestResumeAndWaitForSuccessForSyncAPI(serviceFlow,
                set("resumeServiceRequestId", serviceReqId),
                set("bearerCode", "WEB"),
                set("party.idType", "loginId"),
                set("party.idValue", party.LoginId),
                set("party.password", party.Password),
                set("party.pin", ConfigInput.tPin),
                set("party.mpin", ConfigInput.mPin),
                set("party.tpin", ConfigInput.tPin));

        res = new SfmResponse(response, pNode);

        if (ConfigInput.isAssert) {
            res.verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            Assertion.verifyResponseSucceeded(response, pNode);
        }
        return res;
    }

    public SfmResponse approveCompleteCashOut(String service, User user, String serviceReqId) throws IOException {

        Markup m = MarkupHelper.createLabel("Approve CompleteCashOut: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker


        ValidatableResponse response = performServiceRequestResumeAndWaitForSuccessForSyncAPI(service,
                set("resumeServiceRequestId", serviceReqId),
                set("bearerCode", "USSD"),
                set("party.idType", "mobileNumber"),
                set("party.idValue", user.MSISDN),
                set("party.pin", ConfigInput.tPin),
                set("party.mpin", ConfigInput.mPin),
                set("party.tpin", ConfigInput.tPin));

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);

    }

    public SfmResponse rechargeSelf(User transactor, String operatorId, String amount, String productId) throws IOException {

        Markup m = MarkupHelper.createLabel("initiate O2C: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.SELF_RECHARGE;

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("serviceFlowId", ""),
                set("transactionAmount", amount),
                set("initiator", "transactor"),
                set("currency", "101"),
                set("bearerCode", "USSD"),
                set("language", "en"),
                delete("externalReferenceId"),
                delete("remarks"),
                delete("transactionMode"),
                delete("requestedServiceCode"),
                delete("productOwnerCode"),
                delete("productBrand"),
                delete("productCategory"),
                set("transactor.idType", "mobileNumber"),
                set("transactor.productId", productId),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.identificationNo", transactor.ExternalCode),
                set("transactor.pin", ConfigInput.mPin),
                delete("transactor.txnPassword"),
                delete("transactor.employeeId"),
                delete("transactor.bankId"),
                delete("transactor.bankAccountNumber"),
                delete("depositor"),
                delete("receiver.bankId"),
                delete("receiver.bankAccountNumber"),
                set("receiver.idType", "operatorId"),
                set("receiver.idValue", operatorId),
                delete("receiver.productId"),
                delete("receiver.identificationNo"),
                delete("receiver.pin"),
                delete("receiver.mpin"),
                delete("receiver.tpin"),
                delete("receiver.password"),
                delete("receiver.txnPassword"),
                delete("receiver.partyId"));

        Assertion.verifyResponseSucceeded(response, pNode);
        return new SfmResponse(response, pNode);

    }

    public TxnResponse getAnswerForUserQns(User user, String qnCode) throws IOException {

        Markup m = MarkupHelper.createLabel("Self Reimbursement", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker
        String usrType = null;
        if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            usrType = "SUBSCRIBER";
        } else {
            usrType = "CHANNEL";
        }
        OldTxnOperations operation = new OldTxnOperations(TransactionType.GET_ANSWER_FOR_USER_QUESTIONS,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.USERTYPE", usrType),
                set("COMMAND.QNSCODE", qnCode)
        );
        pNode.info(operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }


    public TxnResponse viewListOfBeneficiaryForADSI(User sub, String benType) throws IOException {
        Markup m = MarkupHelper.createLabel("viewListOfBeneficiaryForADSI: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.VIEW_LIST_OF_BENEFICIARY_AD_SI,
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.MSISDN", sub.MSISDN),
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.BENTYPE", benType)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public Transactions disableAutoDebitByCustomer(String CustomerMsisdn, String accnum, String benecode) throws Exception {
        Markup m = MarkupHelper.createLabel("Auto_Debit_Confirmation_by_Customer", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.AUTO_DEBIT_DISABLE_BY_CUSTOMER,
                set("COMMAND.MSISDN", CustomerMsisdn),
                set("COMMAND.BENCODE", benecode),
                set("COMMAND.ACCNO", accnum),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );
        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        response.verifyStatus(Constants.TXN_SUCCESS);
        return this;
    }

    /**
     * @param subs
     * @param isWuEnable
     * @param isMoneyGramEnable
     * @param payID
     * @return
     * @throws Exception
     */
    public TxnResponse selfSubscriberRegistrationWithIMTDetails(User subs, String isWuEnable, String isMoneyGramEnable, String... payID) throws Exception {
        Markup m = MarkupHelper.createLabel("Self Subscriber Registration With IMT Details", ExtentColor.PINK);
        pNode.info(m);

        String paymentId = payID.length > 0 ? payID[0] : Constants.NORMAL_WALLET;

        if (subs.KinUser == null) {
            subs.setKinUser(new KinUser()); // assign a kin user with empty details
            subs.KinUser.KinNumber = "";
            subs.KinUser.DateOfBirth = "";
            subs.KinUser.FirstName = "";
            subs.KinUser.LastName = "";
            subs.KinUser.MiddleName = "";
            subs.KinUser.RelationShip = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Age = "";
            subs.KinUser.ContactNum = "";
            subs.KinUser.Nationality = "";
        }

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SELF_SUBSCRIBER_REGISTRATION,
                set("COMMAND.PROVIDER", DataFactory.getDefaultProvider().ProviderId),
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.FNAME", subs.FirstName),
                set("COMMAND.LNAME", subs.LastName),
                set("COMMAND.EMAIL", subs.Email),
                set("COMMAND.PAYID", paymentId),
                set("COMMAND.IDNUMBER", subs.ExternalCode),
                set("COMMAND.DOB", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.OTP", generateOTPService(subs.MSISDN)),
                set("COMMAND.LOGINID", subs.LoginId),
                set("COMMAND.REGTYPEID", subs.RegistrationType),
                set("COMMAND.KINFIRSTNAME", subs.KinUser.FirstName),
                set("COMMAND.KINLASTNAME", subs.KinUser.LastName),
                set("COMMAND.KINAGE", subs.KinUser.Age),
                set("COMMAND.KINCONTACTNO", subs.KinUser.ContactNum),
                set("COMMAND.KINNATIONALITY", subs.KinUser.Nationality),
                set("COMMAND.KINNATIONALITYNO", subs.KinUser.KinNumber),
                set("COMMAND.KINRELATIONSHIP", subs.KinUser.RelationShip),
                set("COMMAND.ISIMTENABLE", "Y"),
                set("COMMAND.IDTYPE", "PASSPORT"),
                set("COMMAND.IDISSUEPLACE", "BANGLORE"),
                set("COMMAND.IDISSUECOUNTRY", "IN"),
                set("COMMAND.ISIDEXPIRES", "N"),
                set("COMMAND.RCOUNTRY", "IN"),
                set("COMMAND.NATIONALITY", "IN"),
                set("COMMAND.IDISSUEDATE", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.POSTAL_CODE", "560037"),
                set("COMMAND.EMPLOYER_NAME", subs.FirstName),
                set("COMMAND.OCCUPATION", "Tester"),
                set("COMMAND.IMTIDTYPE", "PASSPORT"),
                set("COMMAND.IMTIDNO", subs.ExternalCode),
                set("COMMAND.WUENABLE", isWuEnable),
                set("COMMAND.MONEYGRAMENABLE", isMoneyGramEnable),
                set("COMMAND.BIRTHCITY", "BANGLORE"),
                set("COMMAND.BIRTHCOUNTRY", "IN"),
                set("COMMAND.PASSPORTISSUECOUNTRY", "IN"),
                set("COMMAND.PASSPORTISSUECITY", "BANGLORE"),
                set("COMMAND.PASSPORTISSUEDATE", subs.DateOfBirth.replace("/", "")),
                set("COMMAND.OTP", generateOTPService(subs.MSISDN))

        );
        // post the request
        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        if (ConfigInput.isAssert) {
            response.verifyMessage("subscriber.self.registration.success", subs.MSISDN)
                    .verifyStatus(Constants.TXN_SUCCESS);
        }
        return response;
    }

    public TxnResponse billAssociationBySubscriber(User subs, Biller biller) throws Exception {
        Markup m = MarkupHelper.createLabel("BillAssociation" + subs.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker


        OldTxnOperations operation = new OldTxnOperations(TransactionType.BILLER_ASSOCIATION_SUBSCRIBER,
                set("COMMAND.MSISDN", subs.MSISDN),
                set("COMMAND.BPCODE", biller.BillerCode),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PREF1", subs.MSISDN)

        );
        pNode.info(operation.bodyString);
        TxnResponse response = operation.postOldTxnURLForHSB(pNode);
        return response;

    }

    public TxnResponse SVCWalletToBank(SavingsClub sClub, String msisdn, String amount) throws IOException {
        Markup m = MarkupHelper.createLabel("SVCWalletToBank: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_WALLET_TO_BANK,
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.BANKID", DataFactory.getDefaultBankIdForDefaultProvider()),
                set("COMMAND.AMOUNT", amount),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MSISDN", msisdn)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse SVCWalletToBankConfirmation(SavingsClub sClub, String msisdn, String transactionId, String trId) throws IOException {
        Markup m = MarkupHelper.createLabel("SVCWalletToBankConfirmation: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_WALLET_TO_BANK_CONFIRMATION,
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.TXNID", transactionId),
                set("COMMAND.TRID", trId),
                set("COMMAND.MSISDN", msisdn)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse SVCBankToWalletConfirmation(SavingsClub sClub, String msisdn, String transactionId, String trId) throws IOException {
        Markup m = MarkupHelper.createLabel("SVCBankToWalletConfirmation: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_BANK_TO_WALLET_CONFIRMATION,
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PAYID", DataFactory.getDefaultWallet().WalletId),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.TXNID", transactionId),
                set("COMMAND.TRID", trId),
                set("COMMAND.MSISDN", msisdn)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse svcGetPendingTransactions(SavingsClub sClub, String msisdn, String payId) throws IOException {
        Markup m = MarkupHelper.createLabel("SVCPendingTransactions: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_PENDING_TXN,
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PAYID", payId),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MSISDN", msisdn)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse svcBankBal(SavingsClub sClub, String msisdn, String payId) throws IOException {
        Markup m = MarkupHelper.createLabel("SVCPendingTransactions: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_BANK_BAL,
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PAYID", payId),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.MSISDN", msisdn),
                set("COMMAND.BANKID", sClub.bankId),
                set("COMMAND.ACCNO", sClub.ClubId)

        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse svcMiniStatement(SavingsClub sClub, String msisdn, String payId, String userId) throws IOException {
        Markup m = MarkupHelper.createLabel("SVCBankToWalletConfirmation: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.SVC_MINI_STATEMENT,
                set("COMMAND.USER_TYPE", sClub.UserType),
                set("COMMAND.USERID", userId),
                set("COMMAND.CLUBID", sClub.ClubId),
                set("COMMAND.PAYID", payId),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                set("COMMAND.PIN", ConfigInput.tPin),
                set("COMMAND.MPIN", ConfigInput.mPin),
                set("COMMAND.PROVIDER", defaultProvider.ProviderId),
                set("COMMAND.MSISDN", msisdn)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public TxnResponse retailerBankRequest(User retailer, String providerId, String bankId, boolean isBankIdRequired) throws IOException {
        Markup m = MarkupHelper.createLabel("retailerBankRequest: API", ExtentColor.BROWN);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = null;

        if (isBankIdRequired) {
            operation = new OldTxnOperations(TransactionType.RETAILER_BANK_REQ,
                    set("COMMAND.MSISDN", retailer.MSISDN),
                    set("COMMAND.PROVIDER", providerId),
                    set("COMMAND.BANKID", bankId),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
            );
        } else {
            operation = new OldTxnOperations(TransactionType.RETAILER_BANK_REQ,
                    set("COMMAND.MSISDN", retailer.MSISDN),
                    set("COMMAND.PROVIDER", providerId),
                    set("COMMAND.MPIN", ConfigInput.mPin),
                    set("COMMAND.PIN", ConfigInput.tPin),
                    set("COMMAND.LANGUAGE1", Constants.LANGUAGE1),
                    delete("COMMAND.BANKID")
            );
        }

        //Assertion.logAsInfo(operation.bodyString, pNode);
        return operation.postOldTxnURLForHSB(pNode);
    }

    public static float getReconciliationBalance() {
        ValidatableResponse response2 = checkReconciliationMismatch();
        return response2.extract().jsonPath().getFloat("walletBalances.balance");
    }

    public static Future<ValidatableResponse> setUMSPropertiesAsync(Map properties) {
        return fireCallable(() -> setUMSProperties(properties));
    }

    public static String getOperatorAvailableBalance(String providerId, String operatorWalletId) {
        ValidatableResponse response2 = fetchOperatorBalance(providerId + operatorWalletId);
        return response2.extract().jsonPath().getString("availableBalance");
    }

    public Transactions suspendResumeUser(User user, String action) throws Exception {
        Markup m = MarkupHelper.createLabel("suspendResumeUser: " + user.FirstName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        OldTxnOperations operation;
        try {
            if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                operation = new OldTxnOperations(TransactionType.SUSPEND_RESUME_USER,
                        set("COMMAND.ACTION", action),
                        set("COMMAND.USERTYPE", "CUSTOMER"),
                        set("COMMAND.MSISDN", user.MSISDN),
                        set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

                );
            } else {
                operation = new OldTxnOperations(TransactionType.SUSPEND_RESUME_USER,
                        set("COMMAND.ACTION", action),
                        set("COMMAND.USERTYPE", "CHANNEL"),
                        set("COMMAND.MSISDN", user.MSISDN),
                        set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)

                );
            }

            pNode.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(pNode);
            response.verifyStatus(Constants.TXN_SUCCESS);
            return this;

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public TxnResponse deleteCustomer(User user, String providerId, String action) throws IOException {
        Markup m = MarkupHelper.createLabel("deleteCustomer: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        OldTxnOperations operation = new OldTxnOperations(TransactionType.DELETE_CUSTOMER,
                set("COMMAND.MSISDN", user.MSISDN),
                set("COMMAND.PROVIDER", providerId),
                set("COMMAND.MODE", action),
                set("COMMAND.LANGUAGE1", Constants.LANGUAGE1)
        );

        pNode.info("Post Request: " + operation.bodyString);
        return operation.postOldTxnURLForHSB(pNode);
    }

    //send mobile number or email id
    public String fetchRecentNotification(String mobNum) {
        ValidatableResponse response = getRecentNotification(mobNum);
        return response.extract().jsonPath().getString("message");
    }

    public SfmResponse initiateP2PSendMoney(User sender, User receiver, BigDecimal txnAmt, String recvProductId, String senderProductId, String providerId) throws Exception {

        Markup m = MarkupHelper.createLabel("initiateP2PSendMoney: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(Services.P2P,
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.productId", recvProductId),
                set("transactionAmount", txnAmt),
                set("currency", providerId),
                set("initiator", "receiver"),
                delete("transactor"),
                delete("depositor"),
                put("$", "sender", serviceRequestDetails(
                        set("idType", "mobileNumber"),
                        set("idValue", sender.MSISDN),
                        set("productId", senderProductId)
                ))
        ).body("txnStatus", is("TI"));

        Assertion.verifyResponsePaused(response, pNode);
        return new SfmResponse(response, pNode);
    }

    public SfmResponse genericResumeRequest(User user, String requestId, String currency, String serviceCode) throws IOException {
        Markup m = MarkupHelper.createLabel("genericResumeRequest: API", ExtentColor.PURPLE);
        pNode.info(m); // Method Start Marker

        JigsawOperations.setUseServiceInBody(false);
        ValidatableResponse response = performServiceRequestResumeAndWaitForSuccessForSyncAPI(serviceCode,
                set("resumeServiceRequestId", requestId),
                set("currency", currency),
                set("party.idValue", user.MSISDN),
                set("party.mpin", ConfigInput.mPin),
                set("party.tpin", ConfigInput.tPin)
        );

        if (ConfigInput.isAssert) {
            Assertion.verifyResponseSucceeded(response, pNode);
        }

        return new SfmResponse(response, pNode);
    }

    public static SfmResponse initiateCashInWithSource(User receiver, User transactor, BigDecimal txnAmt, String... source) throws Exception {

        SfmResponse res = null;
        Markup m = MarkupHelper.createLabel("initiateCashInWithSource: API", ExtentColor.PINK);
        pNode.info(m); // Method Start Marker

        String serviceFlow = Services.CASHIN;

        pNode.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", serviceFlow));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                put("$", "source", source[0]),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                set("transactionAmount", txnAmt),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", receiver.Password),
                set("receiver.identificationNo", receiver.ExternalCode),
                delete("depositor")
        );

        res = new SfmResponse(response, pNode);
        if (ConfigInput.isAssert) {
            res.verifyMessage("txn.cashin.successful", DataFactory.getDefaultProvider().Currency, txnAmt.toString(), transactor.MSISDN, receiver.MSISDN, res.TransactionId);
        }

        return res;
    }

}