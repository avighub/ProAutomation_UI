package framework.features.apiManagement;

public class AuthenticationRequestContract {

    /*public static ValidatableResponse enableSecondPartyAuthenticationWhenInitiatorIsWithdrawer() {
        //TODO: replace using contract template and use service code
        String requestBody = multiLineString(
                        *//*
    <service name="secondPartyAuthenticationRequired">
        <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'withdrawer' and (request['serviceCode'] == 'CASHOUT')"/>
        <step name="triggerSecondPartyPinValidation" party="withdrawer" when="request['initiator'] == 'transactor' and (request['serviceCode'] == 'CASHOUT')"/>
        <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'receiver' and request['serviceCode'] == 'P2P'"/>
        <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'receiver' and (request['serviceCode'] == 'CASHIN' or
        request['serviceCode'] == 'LOANDIS' or request['serviceCode'] == 'CASHINOTHR' or request['serviceCode'] == 'TRCASHIN' or request['serviceCode'] == 'TRCASHINOT')"/>
        <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'depositor' and (request['serviceCode'] == 'CASHINOTHR'
        or request['serviceCode'] == 'TRCASHINOT' or request['serviceCode'] == 'MERCHPAYOT')"/>
        <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'MERCHPAY'"/>
        <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'LOANREPAY'"/>
        <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'UPDATEINV'"/>
        <step name="triggerSecondPartyPinValidation" party="receiverProxy" when="request['serviceCode'] == 'INVC2C'"/>
        <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'O2C'"/>
        <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'TXNCORRECT'"/>
</service>
                        *//*);
        return secondPartyAuthenticationRequest(requestBody);
    }*/

    /*public static ValidatableResponse disableSecondPartyAuthenticationWhenInitiatorIsWithdrawer() {
        //TODO: replace using contract template and use service code
        String requestBody = multiLineString(
                        *//*
    <service name="secondPartyAuthenticationRequired">
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'TXNCORRECT'"/>
        <!--<step name="triggerSecondPartyPinValidation" party="withdrawer" when="request['initiator'] == 'withdrawer' and (request['serviceCode'] == 'CASHOUT')"/>-->
        <step name="triggerSecondPartyPinValidation" party="withdrawer" when="request['initiator'] == 'transactor' and (request['serviceCode'] == 'CASHOUT')"/>
        <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'receiver' and request['serviceCode'] == 'P2P'"/>
        <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'receiver' and (request['serviceCode'] == 'CASHIN' or
        request['serviceCode'] == 'LOANDIS' or request['serviceCode'] == 'CASHINOTHR' or request['serviceCode'] == 'TRCASHIN' or request['serviceCode'] == 'TRCASHINOT')"/>
        <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'depositor' and (request['serviceCode'] == 'CASHINOTHR'
        or request['serviceCode'] == 'TRCASHINOT' or request['serviceCode'] == 'MERCHPAYOT')"/>
        <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'MERCHPAY'"/>
        <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'LOANREPAY'"/>
        <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'UPDATEINV'"/>
        <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'COUTBYCODE'"/>
        <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'O2C'"/>
    </service>
                        *//*);
        return secondPartyAuthenticationRequest(requestBody);
    }*/

    //In Selenium this should not be done
   /* public static ValidatableResponse enableSecondPartyAuthenticationWhenInitiatorIsTransactor() {
        //TODO: replace using contract template and use service code
        String requestBody = multiLineString(
                        *//*
                <service name="secondPartyAuthenticationRequired">
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'BULK_UPLOAD'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'MULTIDRCR'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'STKLIQREQ'"/>
                <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'receiver' and request['serviceCode'] == 'P2P'"/>
                <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'MERCHPAY'"/>
                <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'LOANREPAY'"/>
                <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'UPDATEINV'"/>
                <step name="triggerSecondPartyPinValidation" party="receiver" when="request['initiator'] == 'transactor' and (request['serviceCode'] == 'CASHIN' or
                request['serviceCode'] == 'LOANDIS' or request['serviceCode'] == 'CASHINOTHR' or request['serviceCode'] == 'TRCASHIN' or request['serviceCode'] == 'TRCASHINOT')"/>
                <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'receiver' and (request['serviceCode'] == 'CASHIN' or
                request['serviceCode'] == 'LOANDIS' or request['serviceCode'] == 'CASHINOTHR' or request['serviceCode'] == 'TRCASHIN' or request['serviceCode'] == 'TRCASHINOT')"/>
                 <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'receiver' and request['serviceCode'] == 'P2P'"/>
                <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'depositor' and (request['serviceCode'] == 'CASHINOTHR'
                or request['serviceCode'] == 'TRCASHINOT' or request['serviceCode'] == 'MERCHPAYOT')"/>
                <step name="triggerSecondPartyPinValidation" party="withdrawer" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'CASHOUT'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'O2C'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'STOCK'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'TXNCORRECT'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'COMDISB'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'ESCROWTRF'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'OPTW'"/>
                </service>
                        *//*);
        return secondPartyAuthenticationRequest(requestBody);
    }
*/
    //In Selenium this should not be done
  /*  public static ValidatableResponse disableSecondPartyAuthenticationWhenInitiatorIsTransactor() {
        //TODO: replace using contract template and use service code
        String requestBody = multiLineString(
                        *//*
                <service name="secondPartyAuthenticationRequired">
                <!--<step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'BULK_UPLOAD'"/>-->
                <!--<step name="triggerSecondPartyPinValidation" party="receiver" when="request['initiator'] == 'transactor'"/>-->
               <!-- <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'MERCHPAY'"/>-->
                <!-- <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'LOANREPAY'"/>-->
                <!-- <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'COMDISB'"/> -->
                <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'receiver' and (request['serviceCode'] == 'CASHIN' or
                request['serviceCode'] == 'LOANDIS' or request['serviceCode'] == 'CASHINOTHR' or request['serviceCode'] == 'TRCASHIN' or request['serviceCode'] == 'TRCASHINOT')"/>
                <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'depositor' and (request['serviceCode'] == 'CASHINOTHR'
                or request['serviceCode'] == 'TRCASHINOT' or request['serviceCode'] == 'MERCHPAYOT')"/>
                <step name="triggerSecondPartyPinValidation" party="sender" when="request['initiator'] == 'receiver' and request['serviceCode'] == 'P2P'"/>
                <step name="triggerSecondPartyPinValidation" party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'COUTBYCODE'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['serviceCode'] == 'O2C'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'TXNCORRECT'"/>
                <step name="triggerSecondPartyPinValidation" first-party="transactor" when="request['initiator'] == 'transactor' and request['serviceCode'] == 'OPTW'"/>
                </service>
                        *//*);
        return secondPartyAuthenticationRequest(requestBody);
    }*/

    /*private static ValidatableResponse secondPartyAuthenticationRequest(String requestBody) {
        return moneyUriInternalApi().contentType(ContentType.XML)
                .header("mfsTenantId", getTenantId())
                .body(requestBody).post("/auth/internal/serviceConfiguration/secondPartyAuthenticationRequired")
                .then().statusCode(200);
    }

    public static ValidatableResponse setBearerForInitiatorPartyOTPValidation() {
        //TODO: replace using contract template and use bearer
        return moneyUriInternalApi().contentType(ContentType.XML)
                .header("mfsTenantId", getTenantId())
                .body(MultiLineString.multiLineString(

            <service name="initiatorOTPValidation" description="Validate initiator from given request">
            <step selector="selectFromKeyValue" select-key="initiator" description="Pluck initiator party from request">
            <step name="generateOTP" persona="initiator" when="request['bearerCode'] == 'IVR'"/>
            </step>
            </service>
         ))
                .post("/sv/internal/otp/serviceConfiguration/initiatorOTPValidation")
                .then().statusCode(200);
    }*/
}
