package framework.features.apiManagement;


import framework.util.JsonPathOperation;

import java.util.Map;

import static framework.util.MultiLineString.multiLineString;
import static framework.util.common.Utils.json;
import static framework.util.common.Utils.jsonToMap;

public class OldTxnContracts {

    public static String transactionCorrectionInitiationJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
         {
        "COMMAND": {
        "TYPE": "REQTRCORIN",
        "USERID": "PT150918.1222.001181",
        "AMOUNT": "1",
        "MSISDN": "7732003200",
        "USERTYPE": "CHANNEL",
        "MSISDN2": "7731003100",
        "USERTYPE2": "SUBSCRIBER",
        "SCREVERSAL": "Y",
        "CMREVERSAL":"Y",
        "IS_TCP_CHECK_REQ":"N",
        "ACTION": "TI",
        "TXNID": "CI150528.0943.C00004",
        "REMARKS": "Correction"
    }
            }
       */), operations);

    }

    public static String transactionCorrectionApprovalJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
        "COMMAND": {
        "TYPE": "REQTRCORCF",
        "USERID": "PT150918.1222.001181",
	    "MSISDN": "7750050011",
        "USERTYPE": "CHANNEL",
        "MSISDN2": "7732003200",
        "USERTYPE2": "SUBSCRIBER",
         "AMOUNT": "1",
	    "CMREVERSAL":"Y",
	    "SCREVERSAL":"Y",
	    "TRID":"7750050011201610270552C0652",
        "STATUS": "0",
        "TXNID": "TC161027.0552.C00019"
            }
        }
       */), operations);

    }


    public static String LastNSuccessfulTransactionsForChannelJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
        "COMMAND": {
        "TYPE": "RLTREQ",
        "MSISDN": "",
        "PROVIDER": "101",
        "PAYID": "12",
        "BLOCKSMS": "",
        "LANGUAGE1": "1",
        "SERVICE": "",
        "TXNMODE": "",
        "NOOFTXNREQ": ""
    }
}
       */), operations);
    }

    public static String LastNSuccessfulTransactionsForSubscriberJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
        "COMMAND": {
        "TYPE": "CLTREQ",
        "MSISDN": "",
        "PROVIDER": "101",
        "PAYID": "12",
        "BLOCKSMS": "",
        "LANGUAGE1": "1",
        "SERVICE": "",
        "TXNMODE": "",
        "NOOFTXNREQ": ""
    }
}
       */), operations);
    }

    public static String lastNPendingTransactionsForChannelJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
    "COMMAND": {
        "TYPE": "CHSMTPREQ",
        "MSISDN": "",
        "PROVIDER": "101",
        "PAYID": "12",
        "TXNID": "Y",
        "BLOCKSMS": "",
        "LANGUAGE1": "1",
        "SERVICE": "",
        "TXNMODE": "",
        "NOOFTXNREQ": "",
        "CELLID": "Cellid1234",
        "FTXNID": "FTxnId345"
    }
}

       */), operations);
    }

    public static String lastNPendingTransactionsForSubscriberJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
    "COMMAND": {
        "TYPE": "CSMTPREQ",
        "MSISDN": "",
        "PROVIDER": "101",
        "PAYID": "12",
        "TXNID": "Y",
        "BLOCKSMS": "",
        "LANGUAGE1": "1",
        "SERVICE": "",
        "TXNMODE": "",
        "NOOFTXNREQ": "",
        "CELLID": "Cellid1234",
        "FTXNID": "FTxnId345"
    }
}

       */), operations);

    }

    public static String lastNTimeOutTransactionsForChannelJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
    "COMMAND": {
        "TYPE": "CHSMTTOREQ",
        "MSISDN": "",
        "PROVIDER": "101",
        "PAYID": "12",
        "TXNID": "Y",
        "BLOCKSMS": "",
        "LANGUAGE1": "1",
        "SERVICE": "",
        "TXNMODE": "",
        "NOOFTXNREQ": "",
        "CELLID": "Cellid1234",
        "FTXNID": "FTxnId345"
    }
}

       */), operations);
    }

    public static String lastNTimeOutTransactionsForSubscriberJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
    "COMMAND": {
        "TYPE": "CSMTTOREQ",
        "MSISDN": "",
        "PROVIDER": "101",
        "PAYID": "12",
        "TXNID": "Y",
        "BLOCKSMS": "",
        "LANGUAGE1": "1",
        "SERVICE": "",
        "TXNMODE": "",
        "NOOFTXNREQ": "",
        "CELLID": "Cellid1234",
        "FTXNID": "FTxnId345"
    }
}

       */), operations);
    }

    public static String followEmployeeActivityJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
            "COMMAND": {
            "TYPE": "AEREQ",
                    "MSISDN": "7731001300",
                    "EMPID": "1",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "LANGUAGE1": "1",
                    "CELLID": "Cellid1234",
                    "FTXNID": "FTxnId345"
        }
        }
        */), operations);
    }

    public static String transactionEnquiryApi(JsonPathOperation... operations) {
        return json(multiLineString(/*
         {
          "COMMAND": {
            "TYPE": "TRANSREQ",
            "REFERENCEID": "MP161019.0141.A00101"
          }
        }
        */), operations);
    }

    public static String modifyTCPDetailsJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
    "COMMAND": {
        "TYPE": "MODTCPDTLS",
        "PROFILE_ID": "TCP1601111620.000020",
        "PROFILE_BALANCES": {
            "MIN_RESIDUAL_BAL": "0",
            "MAX_BALANCE": "99999",
            "MIN_TXN_AMT": "0.1",
            "MAX_TXN_AMT": "99999",
            "MAX_PCT_ALLWD": "100"
        },
        "THRESHOLD_DETAILS": {
            "SERVICES": {
                "SERVICE_DETAIL": [
                    {
                        "SERVICE_TYPE": "CASHIN",
                        "AMOUNT": "99999",
                        "THRESHOLD_TYPE_ID": "3",
                        "COUNT": "99999",
                        "BEARER": "ALL",
                        "TYPES": "PAYER"
                    }
                ]
            }
        }
    }
}
        */), operations);
    }

    public static Map serviceDetailsOfTcp(JsonPathOperation... operations) {
        Map map = jsonToMap(json((multiLineString(/*
                    {
                        "SERVICE_TYPE": "P2P",
                        "AMOUNT": "99999999",
                        "THRESHOLD_TYPE_ID": "3",
                        "COUNT": "99999999",
                        "BEARER": "ALL",
                        "TYPES": "PAYER"
                    }
        */)), operations));

        return map;
    }

    public static String customerBankMiniStatmentJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
            "COMMAND":
	        {
                "TYPE": "CMINIREQ",
                "MSISDN": "7732003200",
                "PROVIDER": "101",
                "BANKID": "BK150911.1641.000003",
                "ACCNO": "77320032001",
                "MPIN":"1357",
                "TPIN":"1358",
                "LANGUAGE1":"1"
            }
          }
        */), operations);
    }

    public static String getStatusByFtxnId(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
          "COMMAND": {
            "TYPE": "FTXNENQ",
            "FTXNID": ""
          }
        }
        */), operations);
    }

    public static String channelUserChangeTPinJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
                  {
                    "COMMAND": {
                    "TYPE": "RCPNREQ",
                    "MSISDN": "1",
                    "MPIN": "4",
                    "PIN": "5",
                    "NEWPIN": "5",
                    "CONFIRMPIN": "5",
                    "LANGUAGE1": "1"
        }
        }
        */), operations);
    }

    public static String customerChangeTPinJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                  "COMMAND": {
                    "TYPE": "CCPNREQ",
                    "MSISDN": "1",
                    "MPIN": "4",
                    "PIN": "5",
                    "NEWPIN": "5",
                    "CONFIRMPIN": "5",
                    "LANGUAGE1": "1"
        }
        }
        */), operations);
    }

    public static String channelUserChangeMPinJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
             {
              "COMMAND": {
                "TYPE": "RCMPNREQ",
                "MSISDN": "1",
                "MPIN": "1",
                "NEWMPIN": "1",
                "CONFIRMMPIN": "1",
                "LANGUAGE1": "1"
          }
        }
        */), operations);
    }

    public static String customerChangeMPinJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
             {
              "COMMAND": {
                "TYPE": "CCMPNREQ",
                "MSISDN": "1",
                 "MPIN": "1",
                "NEWMPIN": "1",
                "CONFIRMMPIN": "1",
                "LANGUAGE1": "1"
          }
        }
        */), operations);
    }

    public static String firstTimeUserAPIJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
            "COMMAND":
	        {
                "TYPE": "FTUREQ",
                "MSISDN": "",
                "PROVIDER": "101",
                "USERTYPE":"CHANNEL",
                "LANGUAGE1":"1"
            }
          }
        */), operations);
    }

    public static String validatePinJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
              "COMMAND": {
                "TYPE": "CVMPINREQ",
                "MSISDN": "1",
                "PROVIDER":"101",
                 "MPIN": "1",
                 "PIN":"",
                "LANGUAGE1": "1"
          }
        }
        */), operations);
    }

    public static String generateSubscriberRegistrationOTP(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
          "COMMAND": {
                      "TYPE": "GENOTPREQ",
                     "MSISDN": "7701536975",
                       "PROVIDER": "101",
                        "SRVREQTYPE": "",
                       "LANGUAGE1": "1"
                 }
         }
        */), operations);
    }

    public static String postSelfSubscriberRegistration(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":
                    {
                        "TYPE":"CUSTREG",
                        "PROVIDER":"101",
                        "PAYID":"12",
                        "MSISDN":"",
                        "FNAME":"Mpin",
                        "LNAME":"Val",
                        "IDNUMBER":"",
                        "DOB":"20081980",
                        "GENDER":"Female",
                        "ADDRESS":"Address",
                        "DISTRICT":"District",
                        "OTP":"",
                        "CITY":"City",
                        "REGTYPEID":"NO_KYC",
                        "LOGINID":"",
                        "PASSWORD":"Com@1357",
                        "CPASSWORD":"Com@1357",
                        "BLOCKSMS":"PAYEE",
                        "LANGUAGE1":"1",
                        "CELLID":"Cellid1234",
                        "FTXNID":"FTxnId345"
                    }
                }
        */), operations);
    }


    public static String billAssociationAPIJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
            "COMMAND":
            {
                "TYPE": "BPREGREQ",
                "MSISDN": "7701535731",
                "PROVIDER": "101",
                "BPCODE": "",
                "MPIN": "1357",
                "PIN": "1357",
                "LANGUAGE1": "1",
                "LANGUAGE2": "1",
                "PREF1": ""
            }
           }
        */), operations);
    }

    public static String billAssociationDeletionAPIJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
              "COMMAND":
              {
                "TYPE": "CBPREGDREQ",
                "MSISDN": "7701535731",
                "BPCODE": "",
                "MPIN": "",
                "PIN": "",
                "LANGUAGE1": "1",
                "LANGUAGE2": "1",
                "PROVIDER": "101",
                "PAYID": "12",
                "FLAG": "Delete",
                "PREF1": ""
               }
             }
        */), operations);
    }

    public static String viewListOfBillersForSubscribersAPIJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
          {
              "COMMAND":
              {
                "TYPE": "GETREGBILLERSREQ",
                "INITIATOR_MSISDN": "7701535731",
                "INITIATOR_PROVIDER_ID": "101",
                "MPIN": "1357",
                "PIN": "1357",
                "LANGUAGE1": "1",
                "LANGUAGE2": "1",
                "PROVIDER": "101",
                "PAYID": "12",
                "FLAG": "Delete",
                "PREF1": ""
               }
             }
        */), operations);
    }

    public static String UserSuspendResumeJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
    {
        "COMMAND": {
                "TYPE": "SUSRESREQ",
                "ACTION": "Suspend",
                "USERTYPE": "CHANNEL",
                "MSISDN": "7712001500",
                "REASON": "BY_OPT",
                "REMARK": "Suspendhimplease"
                }
    }
    */), operations);
    }

    public static String UserBarUnbarJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
   {
        "COMMAND": {
        "TYPE": "BARRINGREQ",
        "ACTION": "UNBAR",
        "USERTYPE": "CHANNEL",
        "MSISDN": "",
        "LOGINID": "",
        "PROVIDER": "101",
        "BARTYPE": "BLK_PR_B",
        "REASON": "BY_OPT",
        "REMARK": "bar"
        }
    }
    */), operations);
    }

    public static String validateMpinJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
   {
        "COMMAND": {
        "TYPE": "CVMPINREQ",
        "MSISDN": "7791463625",
        "PROVIDER": "101",
        "MPIN": "1357",
        "LANGUAGE1": "1"
        }
    }
    */), operations);
    }

    public static String CustomerAcquisitionServiceJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE":"ACQREQ",
                    "MSISDN":"",
                    "STATUS":"0",
                    "PROVIDER":"",
                    "BLOCKSMS":"",
                    "LANGUAGE1":"1",
                    "CELLID":"Cellid1234",
                    "FTXNID":"FTxnId345"
        }
}
    */), operations);
    }

    public static String subscriberBillerAssociationByRetailerJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "RBPREGAREQ",
                    "MSISDN": "7789000018",
                    "MSISDN1": "7789000027",
                    "BPCODE": "PB1234",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "LANGUAGE1": "1",
                    "LANGUAGE2": "1",
                    "PROVIDER": "101",
                    "PROVIDER1": "101",
                    "PAYID1": "12",
                    "FLAG": "Add",
                    "PREF1": "7789000027",
                    "CELLID": "Cellid1234",
                    "FTXNID": "FTxnId345"
                    }
}
    */), operations);
    }

    public static String subscriberBillerAssociationApprovalByRetailerJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "RBPREGAPREQ",
                    "PROVIDER": "101",
                    "MSISDN": "7789000018",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "TXNID": "Add",
                    "LANGUAGE1": "7789000027",
                    "CELLID": "Cellid1234",
                    "FTXNID": "FTxnId345"
                    }

}
    */), operations);
    }

    public static String modifySubscriberBillerAssociationByRetailerJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND": {
                     "TYPE": "RBPREGMREQ",
                     "MSISDN": "7789000018",
                     "MSISDN1": "7789000027",
                     "BPCODE": "PB1234",
                     "MPIN": "1357",
                     "PIN": "1358",
                     "LANGUAGE1": "1",
                     "LANGUAGE2": "1",
                     "PROVIDER": "101",
                     "PROVIDER1": "101",
                     "PAYID1": "12",
                     "FLAG": "Modify",
                     "oldPREF1": "778900002700",
                     "PREF1": "7789000027",
                     "CELLID": "Cellid1234",
                     "FTXNID": "FTxnId345"

}
    */), operations);
    }

    public static String deleteSubscriberBillerAssociationByRetailerJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND": {
                    "TYPE": "RBPREGDREQ",
                    "MSISDN": "7789000018",
                    "MSISDN1": "7789000027",
                    "BPCODE": "PB1234",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "LANGUAGE1": "1",
                    "LANGUAGE2": "1",
                    "PROVIDER": "101",
                    "PROVIDER1": "101",
                    "PAYID1": "12",
                    "FLAG": "Delete",
                    "PREF1": "7789000027",
                    "CELLID": "Cellid1234",
                    "FTXNID": "FTxnId345"
                 }
}
    */), operations);
    }

    public static String deleteSubscriberBillerAssociationByRetailerApprovalJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND": {
                    "TYPE": "RBPREGDPREQ",
                    "MSISDN": "7789000018",
                    "PROVIDER": "101",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "TXNID": "Add",
                    "LANGUAGE1": "1",
                    "CELLID": "Cellid1234",
                    "FTXNID": "FTxnId345"
                 }
}
    */), operations);
    }

    public static String subscriberSelfRegistrationJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                   "COMMAND": {
                     "TYPE": "CUSTREG",
                     "IDTYPE": "",
                     "EXPIRYDATE": "",
                     "PROVIDER": "101",
                     "PAYID": "12",
                     "MSISDN": "",
                     "FNAME": "",
                     "LNAME": "",
                     "IDNUMBER": "",
                     "DOB": "20081980",
                     "GENDER": "Female",
                     "ADDRESS": "Address",
                     "DISTRICT": "District",
                     "CITY": "city",
                     "OTP": "",
                     "REGTYPEID": "NO_KYC",
                     "LOGINID": "7787008700",
                     "PASSWORD": "Com@1357",
                     "CPASSWORD": "Com@1357",
                     "BLOCKSMS": "PAYEE",
                     "LANGUAGE1": "1",
                     "CELLID": "Cellid1234",
                     "FTXNID": "FTxnId345",
                     "KINFIRSTNAME": "",
                     "KINLASTNAME": "",
                     "KINMIDDLENAME": "",
                     "KINAGE": "",
                     "KINCONTACTNO": "",
                     "KINNATIONALITY": "",
                     "KINNATIONALITYNO": "",
                     "KINRELATIONSHIP": "",
                     "ISIMTENABLE":"",
                     "IDTYPE":"",
                     "IDISSUEPLACE":"",
                     "IDISSUECOUNTRY":"",
                     "RCOUNTRY":"",
                     "NATIONALITY":"",
                     "IDISSUEDATE":"",
                     "ISIDEXPIRES":"",
                     "IDEXPIRYDATE":"",
                     "POSTAL_CODE":"",
                     "EMPLOYER_NAME":"",
                     "OCCUPATION":"",
                     "IMTIDTYPE":"",
                     "IMTIDNO":"",
                     "WUENABLE":"",
                     "MONEYGRAMENABLE":"",
                     "BIRTHCITY":"",
                     "BIRTHCOUNTRY":"",
                     "PASSPORTISSUECOUNTRY":"",
                     "PASSPORTISSUECITY":"",
                     "PASSPORTISSUEDATE":""

                   }
    }
    */), operations);
    }

    public static String generateOTPServiceJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND": {
                      "TYPE": "GENOTPREQ",
                      "MSISDN": "7787008700",
                      "PROVIDER": "101",
                      "SRVREQTYPE": "CUSTREG",
                      "LANGUAGE1": "1"
                    }
                 }
    */), operations);
    }

    public static String addSavingClubJSON(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                      "COMMAND": {
                      "TYPE": "CRCLUB",
                      "MSISDN": "7787008700",
                      "PROVIDER": "101",
                      "LANGUAGE1": "1",
                      "LANGUAGE1": "1",
                      "CLUBNAME": "CLUBASD",
                      "CLUBLOCATION": "Bangalore",
                      "CLUBTYPE": "Premium",
                      "USER_TYPE": "CHANNEL",
                      "USER_GRADE": "Gold Wholesaler"
                    }
                 }
    */), operations);
    }

    public static String subscriberRegistrationByChannelUserJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {

                       "COMMAND": {
                           "TYPE": "RSUBREG",
                           "IDTYPE": "",
                           "EXPIRYDATE": "",
                           "PROVIDER": "101",
                           "PAYID": "12",
                           "FNAME": "Dalia",
                           "LNAME": "Debnath",
                           "MSISDN": "7792045735",
                           "PROVIDER2": "101",
                           "PAYID2": "12",
                           "MSISDN2": "7750050012",
                           "IDNUMBER": "7750050012abcd",,
                           "MPIN": "1357",
                           "PIN": "1358",
                           "DOB": "24042017",
                           "GENDER": "Female",
                           "ADDRESS": "Address123",
                           "DISTRICT": "District",
                           "CITY": "Bangalore",
                           "LOGINID": "7750050011",
                           "PASSWORD": "Com@1357",
                           "CPASSWORD": "Com@1357",
                           "LANGUAGE1": "1",
                           "REGTYPEID": "NO_KYC",
                           "CELLID": "Cellid1234",
                           "FTXNID": "FTxnId345",
                           "KINFIRSTNAME": "",
                           "KINLASTNAME": "",
                           "KINMIDDLENAME": "",
                           "KINAGE": "",
                           "KINCONTACTNO": "",
                           "KINNATIONALITY": "",
                           "KINNATIONALITYNO": "",
                           "KINRELATIONSHIP": ""
                         }



  }

        */), operations);
    }

    /**
     * Join Saving Club
     *
     * @param operations
     * @return
     */
    public static String joinSavingClubJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                      "COMMAND": {
                      "TYPE": "JOINCLUB",
                      "MSISDN": "7787008700",
                      "PROVIDER": "101",
                      "MPIN": "1357",
                      "PIN": "1358",
                      "PAYID": "12",
                      "USER_TYPE": "SUBSCRIBER",
                      "CLUBID": "",
                      "USER_ID": "",
                      "CLUBNAME": "CHANNEL",
                      "LANGUAGE1": "1"
                    }
                 }
    */), operations);

    }

    public static String depositSavingClubJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                      "COMMAND": {
                      "TYPE": "DEPOCLUB",
                      "MSISDN": "7787008700",
                      "PROVIDER": "101",
                      "MPIN": "1357",
                      "PIN": "1358",
                      "PAYID": "12",
                      "USER_TYPE": "SUBSCRIBER",
                      "CLUBID": "",
                      "AMOUNT": "5"
                    }
                 }
    */), operations);

    }

    public static String resignClubJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                      "COMMAND": {
                      "TYPE": "RESIGNCLUB",
                      "MSISDN": "7787008700",
                      "PROVIDER": "101",
                      "MPIN": "1357",
                      "PIN": "1358",
                      "PAYID": "12",
                      "USER_TYPE": "SUBSCRIBER",
                      "CLUBID": "",
                      "USER_ID": "",
                      "CLUBNAME": "CHANNEL",
                      "LANGUAGE1": "1"
                    }
                 }

    */), operations);

    }

    public static String getSVCListJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                      "COMMAND": {
                      "TYPE": "GETSVCLIST",
                      "MSISDN": "7787008700",
                      "PROVIDER": "101",
                    }
                 }

    */), operations);

    }

    public static String getSVCWalletDetailJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                      "COMMAND": {
                      "TYPE": "SVCWD",
                      "MSISDN": "7787008700",
                      "CLUBID": "123645",
                      "PROVIDER": "101",
                    }
                 }

    */), operations);

    }

    //Added for Cashout
    public static String webCashOut(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "RCORREQ",
	                "MSISDN": "7789000018",
	                "MPIN": "1357",
	                "TXNID": "Add",
	                "LANGUAGE1": "1",
	                "STATUS": "0",
	                "AMOUNT": "10",
                    "PROVIDER": "101",
                    }
                   }
    */), operations);
    }

    public static String subsAccountClosureJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "RACRREQ",
	                "MSISDN": "7789000018",
                    "PROVIDER": "101",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "TXNID": "Add",
	                "STATUS": "0",
                    "LANGUAGE1": "1",
                    }
                   }
    */), operations);
    }

    public static String svcBalanceEnquiryJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "CBALENQ",
                    "PROVIDER": "101",
                    "PAYID": "18",
                    "CLUB_ID": "1",
                    "USER_TYPE": "SUBSCRIBER",
                    "MSISDN": "01231203",
	                "USER_ID": "PTxyz",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "TRID": "",
                    "LANGUAGE1": "1"
                    }
                   }
    */), operations);
    }

    public static String svcWithdrawClubJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "WITHCLUB",
	                "USER_TYPE": "SUBSCRIBER",
                    "CLUBID": "12312",
                    "MSISDN": "1357",
                    "PIN": "1358",
                    "MPIN": "1357",
	                "PROVIDER": "101",
                    "PAYID": "12",
                    "AMOUNT": "15"
                    }
                   }
    */), operations);
    }

    public static String svcWithdrawClubApprovalJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "WITHCLUBCFN",
	                "LANGUAGE": "1",
                    "USER_TYPE": "SUBSCRIBER",
                    "CLUBID": "clubid",
                    "PIN": "1358",
                    "MPIN": "1357",
	                "PROVIDER": "101",
                    "MSISDN": "12123123",
                    "ACTION": "1",
                    "TXNID": "",
                    "TRID": ""
                    }
                   }

    */), operations);
    }

    public static String svcBankMiniStatementJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "SVCMINIREQ",
                    "USER_TYPE": "SUBSCRIBER",
                    "PIN": "1358",
                    "MPIN": "1357",
                    "CLUB_ID": "asd",
	                "MSISDN": "719191",
                    "BANKID": "12123123",
                    "PAYID": "1",
                    "ACCNO": "",
                    "PROVIDER": "101",
                    "LANGUAGE": "1"
                    }
                   }

    */), operations);
    }

    public static String svcBankToWalletJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "SVCB2W",
                    "USER_TYPE": "SUBSCRIBER",
                    "AMOUNT": "",
                    "CLUBID": "",
                    "PAYID": "",
	                "LANGUAGE1": "1",
                    "MPIN": "1357",
                    "PIN": "1357",
                    "PROVIDER": "101",
                    "MSISDN": ""
                    }
                   }

    */), operations);
    }

    public static String svcWalletToBankJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "SVCW2B",
                    "USER_TYPE": "SUBSCRIBER",
                    "BANKID": "1358",
                    "AMOUNT": "5",
                    "CLUBID": "asd123",
                    "PAYID": "12",
	                "LANGUAGE1": "1",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "PROVIDER": "101",
                    "MSISDN": "123123"
                    }
                   }
    */), operations);
    }

    public static String subRegByAS400Json(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                "COMMAND": {
                "CELLID": "Cellid1234",
                "FTXNID": "FTxnId345",
                "TYPE": "REGST",
                "FNAME": "Dalia",
                "LNAME": "Debnath",
                "MSISDN": "7766006600",
                "IDNUMBER": "7766006600abcd",
                "IDTYPE": "PASSPORT",
                "DOB": "24041989",
                "GENDER": "Female",
                "ADDRESS": "Address",
                "CITY": "City",
                "DISTRICT": "District",
                "LANGUAGE": "1",
                "PROVIDER": "101",
                "REGTYPEID": "NO_KYC",
                "PAYID": "12",
                "LOGINID": "7766006600",
                "PASSWORD": "Com@1357",
                "ANNEXID": "",
                "CPASSWORD": "Com@1357",
                "KINFIRSTNAME": "",
                "KINLASTNAME": "",
                "KINMIDDLENAME": "",
                "KINAGE": "",
                "KINCONTACTNO": "",
                "KINNATIONALITY": "",
                "KINNATIONALITYNO": "",
                "KINRELATIONSHIP": ""
                           }
               }
    */), operations);
    }


    public static String svcDisburseAmtJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "SVCDSBINI",
                    "USER_TYPE": "SUBSCRIBER",
                    "MSISDN": "7748525252",
                    "CLUBID": "881000185",
                    "MSISDN2": "7711243973",
                    "PIN": "1357",
                    "MPIN": "1357",
                    "PROVIDER": "101",
                    "PAYID": "12",
                    "AMOUNT": "6"
                    }
                   }
    */), operations);
    }

    public static String svcDisburseConfirmJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                   "TYPE": "SVCDSBINTER",
                   "LANGUAGE": "1",
                    "USER_TYPE": "SUBSCRIBER",
                    "CLUBID": "881000185",
                    "PAYID": "12",
                    "LANGUAGE1": "1",
                    "PIN": "1357",
                    "MSISDN": "7711226262",
                    "MPIN": "1357",
                    "PROVIDER": "101",
                    "ACTION": "1",
                    "TXNID": "XX180831.0217.C00001",
                    "TRID": ""
                    }
                   }
    */), operations);
    }


    public static String getEmployeeList(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
                "COMMAND":{
                "TYPE": "ELREQ",
             "MSISDN": "7710002933",
                "MPIN":"1357",
                 "PIN":"1358",
                 "LANGUAGE1":"1",
               }
               }
*/), operations);
    }

    public static String AutoDebit_Confirmation_Customer(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
                "COMMAND":{
                "TYPE": "ADENAREQ5",
             "MSISDN": "7789000027",
             "PROVIDER":"101",
                "MPIN":"1357",
                 "PIN":"1358",
                 "TXNID":"",
                 "STATUS":"0",
                 "LANGUAGE1":"1",
               }
               }
*/), operations);
    }

    public static String Last_N_Transaction(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
                "COMMAND":{
                "TYPE": "CLTREQ",
                "MSISDN": "",
                "PROVIDER":"101",
                "PAYID":"12",
                "MPIN":"1357",
                 "PIN":"1358",
                 "LANGUAGE1":"1",
               }
               }
*/), operations);
    }


    public static String subsAccountClosurebyAgent(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "CCRACRREQ",
	                "MSISDN": "7789000018",
                    "PROVIDER": "101",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "TXNID": "",
	                "STATUS": "0",
                    "LANGUAGE1": "1",
                    "BLOCKSMS":"",
                    "TXNMODE":"",
                    "CELLID": "Cellid1234",
                    "FTXNID": "FTxnId345"

                    }
                   }
    */), operations);
    }

    public static String createEmployee(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "ADEREQ",
	                "MSISDN": "7789000018",
                    "FNAME":"Wholesaler",
                    "LNAME":"Emp",
                    "SNAME":"1st",
                    "EMPID":"1",
                    "PAYID":"12",
                    "MPIN":"1357",
                     "PIN":"1358",
                     "BLOCKSMS":"",
                     "LANGUAGE1":"1",
                     "CELLID":"Cellid1234",
                     "FTXNID":"FTxnId345"

                    }
                   }
    */), operations);
    }

    public static String changeEmployeeMpin(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "ERMPNREQ",
	                "MSISDN": "7789000018",
                    "EMPID":"1",
                    "MPIN":"1357",
                     "PIN":"1358",
                   "LANGUAGE1":"1",

                    }
                   }
    */), operations);
    }

    public static String changeEmployeeTpin(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "ERPNREQ",
	                "MSISDN": "7789000018",
                    "EMPID":"1",
                    "MPIN":"1357",
                     "PIN":"1358",
                   "LANGUAGE1":"1",

                    }
                   }
    */), operations);
    }

    public static String suspendEmployee(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                     "COMMAND":{
                    "TYPE": "SPEREQ",
	                "MSISDN": "7789000018",
                    "EMPID":"1",
                    "MPIN":"1357",
                     "PIN":"1358",
                   "LANGUAGE1":"1",

                    }
                   }
    */), operations);
    }

    public static String resumeEmployee(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                     "COMMAND":{
                    "TYPE": "RAEREQ",
	                "MSISDN": "7789000018",
                    "EMPID":"1",
                    "MPIN":"1357",
                     "PIN":"1358",
                   "LANGUAGE1":"1",

                    }
                   }
    */), operations);
    }

    public static String AutoDebitSubsEnable(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND": {
                         "TYPE": "CUSTADREQ",
                         "MSISDN": "7789000027",
                            "PROVIDER": "101",
                            "PAYID": "12",
                            "MPIN": "1357",
                            "PIN": "1358",
                            "MAXAMT": "55",
                            "BENCODE": "PB123",
                            "ACCNO": "7789000027",
                            "PROVIDER2": "101",
                            "LANGUAGE1": "1"
  }
}
    */), operations);
    }


    public static String AutoDebitEnableByChanneluser(JsonPathOperation... operations) {
        return json(multiLineString(/*
                        {
                            "COMMAND": {
                            "TYPE": "RCUSTADREQ",
                            "MSISDN": "7710012441",
                            "PROVIDER": "101",
                            "MSISDN1": "7742004200",
                            "PROVIDER1": "101",
                            "PAYID1": "12",
                            "MPIN": "1357",
                            "PIN": "1358",
                            "MAXAMT": "5",
                            "BENCODE": "PB123",
                            "ACCNO": "7742004200",
                            "LANGUAGE1": "1"
                          }
                        }
*/), operations);
    }

    public static String AuthorizationRequest(JsonPathOperation... operations) {
        return json(multiLineString(/*
                       {
                           "COMMAND": {
                                "TYPE": "AUTHREQ",
                                "REQ": "CO",
                                "MSISDN": "7700000018",
                                "MSISDN2": "7700000035",
                                "PROVIDER": "101",
                                "PAYID": "12",
                                "PROVIDER2": "101",
                                "PAYID2": "12",
                                "AMOUNT": "5",
                                "IDNO": "7700000018abcd",
                                "MPIN": "1357",
                                "PIN": "1358",
                                "CELLID": "Cellid1234",
                                "FTXNID": "FTxnId345"
                          }
                        }
*/), operations);
    }

    public static String automaticO2C(JsonPathOperation... operations) {
        return json(multiLineString(/*
                        {
                          "COMMAND": {
                            "TYPE": "REQAO2C",
                            "MSISDN": "7731003100",
                            "AMOUNT": "105",
                            "BANKID": "BK160229.1730.010987",
                            "REFERENCEID": "102"
                          }
                        }
*/), operations);
    }

    public static String automaticReimbursement(JsonPathOperation... operations) {
        return json(multiLineString(/*
                       {
                          "COMMAND": {
                            "TYPE": "REQAOPTW",
                            "MSISDN": "7731003100",
                            "AMOUNT": "2",
                            "BANKID": "BK160229.1730.010987",
                            "REFERENCEID": "112233"
                          }
                        }
*/), operations);
    }


    public static String svcPromoteOrDemoteMemberJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "CPDMEM",
                    "USER_TYPE": "SUBSCRIBER",
                    "USER_GRADE": "SUBS",
                    "ROLE": "Promote/Demote",
                    "MSISDN": "",
                    "MSISDN2": "",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "AMOUNT": "",
                    "PAYID": "12",
                    "PROVIDER": "101",
                    "LANGUAGE1": "1",
                    "CLUBID": "",
                    "USER_ID": "",
	                "ACTION": "1",
	                "TXNID": "",
	                "LANGUAGE1": "1",
	                "FLAG": "TRUE",
	                "TRID": "TRUE",
                    }
                   }
    */), operations);
    }

    public static String createShiftBasedEmployeeJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                { "COMMAND": {
                    "TYPE": "ADEREQ",
                    "MSISDN": "7701536637",
                    "FNAME": "1",
                    "LNAME": "1",
                    "SNAME": "sur",
                    "EMPID": "1",
                    "PAYID": "12",
                    "MPIN": "1357",
                    "PIN": "1357",
                    "LANGUAGE1": "1"
                    }
                    }
    */), operations);
    }


    public static String channelUserDayTransactionJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE":"RDTREQ",
                    "MSISDN":"",
                     "PAYID":"12",
                     "MPIN": "",
                    "PIN": "",
                    "PROVIDER": "101",
                    "LANGUAGE1":"1",
                    "CELLID":""

        }
}
    */), operations);
    }

    public static String channelUserAccountDetails(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE":"RADREQ",
                    "MSISDN":"",
                    "PROVIDER": "101",
                    "MPIN": "",
                    "PIN": "",
                    "CELLID":"Cellid1234",
                    "FTXNID":"FTxnId345",

        }
}
    */), operations);
    }


    public static String selfReimbursement(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
  "COMMAND": {
    "TYPE": "SLFREIMB",
    "PIN": "1358",
    "MPIN": "1357",
    "PAYID": "12",
    "STEP": "TI",
    "MSISDN": "7789008900",
    "PROVIDER": "101",
    "AMOUNT": "10"
  }
}
                   }
    */), operations);
    }

    public static String unBarEmployeeJson(JsonPathOperation... operations) {
        return json(multiLineString(/*

  {
  "COMMAND": {
    "TYPE": "BLEREQ",
    "MSISDN": "7710002933",
    "EMPID": "1",
    "MPIN": "1357",
    "PIN": "1358",
    "BLOCKSMS": "PAYEE",
    "LANGUAGE1": "1",
    "CELLID": "Cellid1234",
    "FTXNID": "FTxnId345"
  }
}
                   }
    */), operations);
    }

    public static String bankCashOutJson(JsonPathOperation... operations) {
        return json(multiLineString(/*

  {
  "COMMAND": {
    "TYPE": "RBCOREQ",
    "MSISDN": "7789000116",
    "MSISDN2": "7789000130",
    "PROVIDER": "101",
    "PROVIDER2": "101",
    "BANKID": "BK170622.1437.000001",
    "BANKID2": "BK170622.1437.000001",
    "ACCNO": "77890001160",
    "ACCNO2": "77890001300",
    "AMOUNT": "15",
    "MPIN": "1357",
    "PIN": "1358",
    "LANGUAGE1": "1",
    "CELLID": "Cellid1234",
    "FTXNID": "FTxnId345"
  }
}
                   }
    */), operations);
    }

    public static String bankCashInJson(JsonPathOperation... operations) {
        return json(multiLineString(/*

  {
  "COMMAND": {
    "TYPE": "RBCIREQ",
    "MSISDN": "7789000116",
    "MSISDN2": "7789000130",
    "PROVIDER": "101",
    "PROVIDER2": "101",
    "BANKID": "BK170622.1437.000001",
    "BANKID2": "BK170622.1437.000001",
    "ACCNO": "77890001160",
    "ACCNO2": "77890001300",
    "AMOUNT": "15",
    "MPIN": "1357",
    "PIN": "1358",
    "LANGUAGE1": "1",
    "CELLID": "Cellid1234",
    "FTXNID": "FTxnId345"
  }
}
                   }
    */), operations);
    }

    public static String bankCashOutConfirmJson(JsonPathOperation... operations) {
        return json(multiLineString(/*

  {
  "COMMAND": {
    "TYPE": "CBCODCREQ",
    "MSISDN": "7789000130",
    "TXNID":" ",
    "PROVIDER": "101",
    "MPIN": "1357",
    "PIN": "1358",
    "LANGUAGE1": "1",
    "CELLID": "Cellid1234",
    "FTXNID": "FTxnId345"
  }
}
                   }
    */), operations);
    }

    public static String balanceEnquiryJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
"COMMAND": {
"TYPE": "RBEREQ",
"MSISDN": "7711001100",
"PROVIDER": "101",
"PAYID": "12",
"MPIN": "1357",
"PIN": "1358",
"LANGUAGE1": "1",
                }
               }
*/), operations);
    }

    public static String subscriberbalanceEnquiryJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
    "COMMAND": {
		"TYPE": "CBEREQ",
		"MSISDN": "7788008800",
		"PROVIDER": "101",
		"PAYID": "12",
		"MPIN": "1357",
		"PIN": "1358",
		"LANGUAGE1": "1"
                    }
                   }
    */), operations);
    }

    public static String disableStandardInstructionBySelfJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
    "COMMAND": {
		"TYPE": "SIDISREQ",
		"MSISDN": "7732003200",
		"MSISDN2": "7732003277",
		"PROVIDER": "101",
		"MPIN": "1357",
		"PIN": "1358",
		"LANGUAGE1": "1"
                    }
                   }
    */), operations);
    }

    public static String enableStandardInstructionBySelfJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
    "COMMAND": {
		"TYPE": "SICHAREQ",
		"MSISDN": "7710010039",
		"MSISDN2": "7724002400",
		"MSISDN3": "7710000183",
		"PROVIDER": "101",
		"PROVIDER2": "101",
		"PROVIDER3": "101",
		"PAYID2": "12",
		"PAYID3": "12",
		"FREQUENCY": "DAILY",
		"DAY": "",
		"MONTH": "",
		"AMOUNT": "10",
		"MPIN": "1357",
		"PIN": "1358",
		"LANGUAGE1": "1",
                    }
                   }
    */), operations);
    }

    public static String enableStandardInstructionBySubscriberJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
    "COMMAND": {
		"TYPE": "SICUSTREQ",
		"MSISDN": "7789000130",
		"MSISDN2": "7789000115",
		"PROVIDER": "101",
		"PROVIDER2": "101",
		"PAYID": "12",
		"PAYID2": "12",
		"FREQUENCY": "DAILY",
		"DAY": "2",
		"MONTH": "1",
		"AMOUNT": "25",
		"MPIN": "1357",
		"PIN": "1358",
		"LANGUAGE1": "1",
	}
                    }
                   }
    */), operations);
    }

    public static String disableStandardInstructionBySubscriberJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
    "COMMAND": {
		"TYPE": "SIDISREQ",
		"MSISDN": "7732003200",
		"MSISDN2": "7732003277",
		"PROVIDER": "101",
		"MPIN": "1357",
		"PIN": "1358",
		"LANGUAGE1": "1"
	}
                    }
                   }
    */), operations);
    }

    public static String addBenificiaryforBankJson(JsonPathOperation... operations) {
        return json(multiLineString(/*

    {
  "COMMAND": {
    "TYPE": "ADDBEN1",
    "PROVIDER": "101",
    "MSISDN": "7788000030",
    "MPIN": "1357",
    "PIN": "1358",
    "PAYMENTTYPE": "BANK",
    "PAYEENICKNAME": "Dalia",
    "PAYEEMOBILE": "7788000031",
    "ACCNO": "7788000031",
    "BRANCHCODE": "019514",
    "BANKID": "019514",
    "IFSCCODE": "019514",
    "BRANCHNAME": "SBIDEL",
    "LANGUAGE1": "1",
    "CELLID": "Cellid1234",
    "FTXNID": "FTxnId345"
  }
}


    */), operations);
    }

    public static String deleteBenificiaryJson(JsonPathOperation... operations) {
        return json(multiLineString(/*

    {
  "COMMAND": {
    "TYPE": "DELBEN",
    "PROVIDER": "101",
    "MSISDN": "7742004200",
    "MPIN": "1357",
    "PIN": "1358",
    "PAYEENICKNAME": "Rishi",
    "CELLID": "Cellid1234",
    "FTXNID": "FTxnId345"
  }
}

    */), operations);
    }

    public static String channelUsrBalEnqBank(JsonPathOperation... operations) {
        return json(multiLineString(/*
                          {
                                  "COMMAND": {
                                    "TYPE": "RBALREQ",
                                    "MSISDN": "2012635801",
                                    "PROVIDER": "101",
                                    "BANKID": "BK160630.1256.000001",
                                    "ACCNO": "2012635801",
                                    "MPIN": "1357",
                                    "PIN": "1358",
                                    "LANGUAGE1": "1",
                                    "CELLID": "Cellid1234",
                                    "FTXNID": "FTxnId345"
                      }
                    }
    */), operations);
    }

    public static String lastNPendingTransactions(JsonPathOperation... operations) {
        return json(multiLineString(/*
               {
                  "COMMAND": {
                    "TYPE": "CSMTPREQ",
                    "MSISDN": "7720000959",
                    "PROVIDER": "101",
                    "MPIN": "1357",
                    "PIN": "1358",
                    "PAYID": "12",
                    "TXNID": "Y",
                    "BLOCKSMS": "PAYER",
                    "LANGUAGE1": "1",
                    "NOOFTXNREQ": "5",
                    "CELLID": "Cellid1234",
                    "FTXNID": "FTxnId345"
  }
}
    */), operations);
    }


    public static String p2pSendMoneyTransactionSubsJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "CTMREQ",
	                "MSISDN": "7789000018",
					"MPIN": "1357",
                    "PIN": "1358",
                    "PROVIDER": "101",
                    "PAYID":"12",
					"PROVIDER2": "101",
                    "PAYID2":"12",
					"MSISDN2": "7795458611",
                    "AMOUNT": "10",
	                "TXNPASSWORD": "null",
					"OTP": "null",
					"IDNO": "null",
					"TXNPASSWORD": "null",
                    "LANGUAGE1": "1",
					"LANGUAGE2": "1",
                    }
                   }
    */), operations);
    }

    public static String viewBillBySubscriber(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
                    "COMMAND":{
                    "TYPE": "CVMBREQ",
	                "MSISDN": "7789000018",
					"PROVIDER": "101",
                    "BILLCCODE": "10",
	                "BILLANO": "null",
					"BILLNO": "null",
					"MPIN": "1357",
					"PIN": "1357",
                    }
                   }
    */), operations);
    }

    public static String fetchListOfCurrencies(JsonPathOperation... operations) {
        return json(multiLineString(/*
                          {
                                  "COMMAND": {
                                    "TYPE": "GETUSRCUR",
                                    "MSISDN": "2012635801",
                                    "LANGUAGE1": "1",
                                    "USERROLE": "Channel"
                      }
                    }
    */), operations);
    }

    public static String changeCurrencyCode1(JsonPathOperation... operations) {
        return json(multiLineString(/*
                          {
                                  "COMMAND": {
                                    "TYPE": "CNGDEFCUR",
                                    "MSISDN": "2012635801",
                                    "LANGUAGE1": "1",
                                    "USERROLE": "Customer"
                                    "NEWCURRENCY": "INR",
                                    "PROVIDER": "101",
                                    "MPIN": "1357"

                      }
                    }
    */), operations);
    }


    public static String changeCurrencyCode(JsonPathOperation... operations) {
        return json(multiLineString(/*
                          {
                                  "COMMAND": {
                                     "TYPE": "CNGDEFCUR",
                                     "MSISDN": "7791450604",
                                     "LANGUAGE1": "1",
                                     "USERROLE": "CUSTOMER",
                                     "NEWCURRENCY": "USD",
                                     "PROVIDERID": "101",
                                     "MPIN": "1357"
                  }
                 }

    */), operations);
    }

    public static String userInfo(JsonPathOperation... operations) {
        return json(multiLineString(/*
                          {
                                  "COMMAND": {
                                     "TYPE": "USERINFOREQ",
                                     "IDENTIFICATION": "7799999991",
                                     "LEVEL": "HIGH",
                                     "INPUTTYPE": "MSISDN",
                                     "USERROLE": "CHANNEL"
                     }
                    }

    */), operations);
    }

    public static String SimSwap(JsonPathOperation... operations) {
        return json(multiLineString(/*
                          {
                                 "COMMAND": {
                                     "TYPE": "UPDSIMSWAP",
                                     "MSISDN": "7791450656",
                                     "LANGUAGE1": "1"
                    }
                    }
    */), operations);
    }


    public static String userEnquiryApi(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
        "COMMAND": {
            "TYPE": "USERINFOREQ",
                "IDENTIFICATION": "912002211",
                "LEVEL": "HIGH",
                "INPUTTYPE": "MSISDN",
                "USERROLE": []
        }
    }
    */), operations);
    }


    public static String bankRegistrationAutoEnhancementJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
        "COMMAND": {
                 "TYPE": "BANKREG",
                 "LANGUAGE1": "1",
                 "BANKID": "516514",
                 "BANKNAME": "USDSBM",
                 "MSISDN": "7777712345",
                 "PROVIDER": "101",
                 "PAYID": "12",
                 "FNAME": "hii12345",
                 "LNAME": "hii12345",
                 "USERROLE": "Channel",
                 "BRANCHCODE": "BRANCH1",
                 "IDNO": "43213456767899",
                 "BANKACCNUMBER": "12346679893",
                 "CUSTOMERID": "11313231",
                 "ACCOUNTTYPE":"01"
    }
}
       */), operations);
    }

    public static String bankDeRegistrationAutoEnhancementJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
        "COMMAND": {
                 "TYPE": "BKDREG",
                 "LANGUAGE1": "1",
                 "BANKID": "516514",
                 "BANKNAME": "USDSBM",
                 "MSISDN": "7777712345",
                 "PROVIDER": "101",
                 "PAYID": "12",
                 "FNAME": "hii12345",
                 "LNAME": "hii12345",
                 "USERROLE": "Channel",
                 "IDNO": "43213456767899",
                 "BANKACCNUMBER": "12346679893"

    }
}
       */), operations);
    }

    public static String viewListOfBeneficiaryADSIJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
        "COMMAND": {
                 "TYPE": "VIEWADSIREQ",
                 "MSISDN": "7789000027",
                 "PROVIDER": "101",
                 "MPIN": "1357",
                 "PIN": "1358",
                 "BENTYPE": "",
                 "LANGUAGE1": "1"
    }
}
       */), operations);
    }

    public static String disableAutoDebitByCustomer(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
        "COMMAND": {
                 "TYPE": "ADDISREQ",
                 "MSISDN": "7789000027",
                 "BENCODE": "101",
                 "ACCNO": "101",
                 "MPIN": "1357",
                 "PIN": "1358",
                 "LANGUAGE1": "1"
    }
}
       */), operations);
    }

    public static String getAnsForUsrQns(JsonPathOperation... operations) {
        return json(multiLineString(/*
                    {
                      "COMMAND": {
                        "TYPE": "GETANSREQ",
                        "MSISDN": "7791411565",
                        "PROVIDER": "101",
                        "USERTYPE": "CHANNEL",
                        "QNSCODE": "at2",
                        "LANGUAGE1": "1",
                        "CELLID": "Cellid1234",
                        "FTXNID": "FTxnId345"
                      }
                    }

    */), operations);
    }

    public static String associateUserWithBank(JsonPathOperation... operations) {
        return json(multiLineString(/*
                    {
                        "COMMAND": {
                            "TYPE": "BNKATREG",
                            "PAYID": "12",
                            "BANK_ID": "BK170627.1808.000001",
                            "PROVIDER": "101",
                            "MSISDN": "7789000012",
                            "USERTYPE": "SUBSCRIBER",
                            "CREATEDBY": "PT180115.1533.016467"
                        }
                    }

    */), operations);
    }

    public static String debitMoneyFromSVA(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
            "COMMAND": {
                "TYPE": "DRSVA",
                "MSISDN": "7715000000",
                "IS_P2P_TRANSFER": "Y",
                "PROVIDER": "101",
                "PAYID": "12",
                "BANKID": "BK150911.1057.002061",
                "AMOUNT": "5",
                "LANGUAGE1": "1",
                "FTXNID": "FTxnId345",
                "USERTYPE": "CHANNEL",
                "REMARKS": "Remarks",
                "BLOCKSMS": "BOTH",
                "PRIORITY_REQUEST_TYPE": "CTMMREQ",
                "THRESHOLD_SERVICE_TYPE": "RC",
                "OTP":"4356"
            }
        }
        */), operations);
    }

    public static String creditMoneyToSVA(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
            "COMMAND": {
                    "TYPE": "CRSVA",
                    "MSISDN2": "7715000000",
                    "PROVIDER": "101",
                    "PAYID": "12",
                    "BANKID": "BK150911.1057.002061",
                    "AMOUNT": "5",
                    "LANGUAGE1": "1",
                    "FTXNID": "FTxnId345",
                    "USERTYPE": "CHANNEL",
                    "REMARKS": "Remarks",
                    "BLOCKSMS": "BOTH",
                    "SVATYPE": ""
	}
        }
        */), operations);
    }

    public static String billerAssociationBySubscriber(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
   "COMMAND": {
    "TYPE": "BPREGREQ",
    "MSISDN": "7791966877",
    "PROVIDER": "101",
    "BPCODE": "1240",
    "MPIN": "1357",
    "PIN": "1358",
    "NICK_NAME": "nick",
    "LANGUAGE1": "1",
    "LANGUAGE2": "1",
    "PREF1": "7791966877"
  }
}
        */), operations);
    }

    public static String gradeNetworkAssociation(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
	"COMMAND": {
		"TYPE": "GRNETREQ",
		"GRADE_CODE": "SSUBS",
		"COUNTRY_CODE": "99",
		"GRADE_STATUS": "A"
	    }
    }
        */), operations);
    }

    public static String getSavingClubSingleStepJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
	"COMMAND": {
		"TYPE": "GRNETREQ",
		"GRADE_CODE": "SSUBS",
		"COUNTRY_CODE": "99",
		"GRADE_STATUS": "A"
	    }
    }
        */), operations);
    }

    public static String disbursementInitiation(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
  "COMMAND": {
    "TYPE": "SVCDSBINI",
    "USER_TYPE": "SUBSCRIBER",
    "MSISDN": "7712213154",
    "CLUBID": "881000185",
    "MSISDN2": "7712213156",
    "PIN": "1358",
    "MPIN": "1357",
    "PROVIDER": "101",
    "PAYID": "12",
    "AMOUNT": "1"
  }
}
        */), operations);
    }

    public static String disbursementConfirmation(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
  "COMMAND": {
    "TYPE": "SVCDSBINTER",
    "PROVIDER": "101",
    "USER_TYPE": "SUBSCRIBER",
    "MSISDN": "7712213154",
    "PAYID": "12",
    "PIN": "1358",
    "MPIN": "1357",
    "AMOUNT": "2",
    "CLUBID": "881000185",
    "ACTION": "1",
    "TXNID": "XX190103.0506.C00083"
  }
}
        */), operations);
    }

    public static String settlementInitiation(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
  "COMMAND": {
    "TYPE": "SVCSETINI",
    "USER_TYPE": "SUBSCRIBER",
    "MSISDN2": "7777534265",
    "PIN": "1358",
    "MPIN": "1357",
    "AMOUNT": "2",
    "MSISDN": "7792056163",
    "PAYID": "12",
    "PROVIDER": "101",
    "LANGUAGE1": "1",
    "CLUBID": "881000240",
    "DISP2PORP2PUNREG": "Y"
  }
}
        */), operations);
    }
    public static String settlementConfirmation(JsonPathOperation... operations) {
        return json(multiLineString(/*
        {
  "COMMAND": {
    "TYPE": "SVCSETINTER",
    "PROVIDER": "101",
    "USER_TYPE": "SUBSCRIBER",
    "MSISDN": "7777356352",
    "PAYID": "12",
    "PIN": "1358",
    "AMOUNT": "2",
    "CLUBID": "881000358",
    "ACTION": "1",
    "TXNID": "XX171130.0852.C00028"
  }
}
        */), operations);
    }

    public static String svcBankToWalletConfirmationJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
  "COMMAND": {
    "TYPE": "SVCB2WCFM",
    "LANGUAGE": "1",
    "USER_TYPE": "SUBSCRIBER",
    "CLUBID": "881000195",
    "PAYID": "12",
    "LANGUAGE1": "1",
    "PIN": "1358",
    "MPIN": "1357",
    "PROVIDER": "101",
    "MSISDN": "7713127508",
    "ACTION": "1",
    "TXNID": "TW190211.0519.C00006",
    "TRID": "7713127508201902110519C0288"
  }
}
    */), operations);
    }

    public static String svcWalletToBankConfirmationJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
  "COMMAND": {
    "TYPE": "SVCW2BCFM",
    "LANGUAGE": "1",
    "USER_TYPE": "SUBSCRIBER",
    "CLUBID": "881000195",
    "PAYID": "12",
    "LANGUAGE1": "1",
    "PIN": "1358",
    "MPIN": "1357",
    "PROVIDER": "101",
    "MSISDN": "7713127508",
    "ACTION": "1",
    "TXNID": "TW190211.0519.C00006",
    "TRID": "7713127508201902110519C0288"
  }
}
    */), operations);
    }

    public static String customerStatement(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
  "COMMAND": {
    "TYPE": "CMINIREQ",
    "MSISDN": "7710711972",
    "PROVIDER": "101",
    "BANKID": "0023",
    "ACCNO": "909100974",
    "MPIN": "1357",
    "PIN": "1357",
    "LANGUAGE1": "1",
    "CELLID": "Cellid1234",
    "FTXNID": "FTxnId345"
  }
}
    */), operations);
    }

    public static String svcPendingTransactionsJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
  "COMMAND": {
    "TYPE": "SVNPEDNTXN",
    "USER_TYPE": "",
    "CLUBID": "",
    "MSISDN": "",
    "PROVIDER": "",
    "PAYID": "",
    "LANGUAGE1": ""
  }
}
    */), operations);
    }

    public static String svcBankBalJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
  "COMMAND": {
    "TYPE": "SVCBNKBAL",
    "USER_TYPE": "SUBSCRIBER",
    "CLUBID": "",
    "MSISDN": "",
    "PIN": "1358",
    "MPIN": "1357",
    "PROVIDER": "",
    "BANKID": "",
    "PAYID": "",
    "ACCNO": "",
    "LANGUAGE1": ""
  }
}

    */), operations);
    }

    public static String svcMiniStatementJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
  "COMMAND": {
    "TYPE": "CWTXNHIS",
    "USER_ID": "",
    "USER_TYPE": "SUBSCRIBER",
    "CLUBID": "",
    "MSISDN": "",
    "MPIN": "",
    "PIN": "1358",
    "PROVIDER": "",
    "PAYID": "",
    "LANGUAGE1": "",

  }
}
    */), operations);
    }

    public static String retailerBankRequestJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
                {
	"COMMAND": {
		"TYPE": "RETBKRQ",
		"MSISDN": "7710000002",
		"PROVIDER": "101",
		"BANKID": "000",
		"MPIN": "1357",
		"PIN": "1358",
		"BLOCKSMS": "",
		"LANGUAGE1": "1",
		"CELLID": "Cellid1234",
		"FTXNID": "FTxnId345"
	}
}
    */), operations);
    }

    public static String deleteCustomerJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
	            "COMMAND": {
                "TYPE": "DELCUSTREQ",
                "MSISDN": "7735000001",
                "PROVIDER": "101",
                "MODE": "FORCE/NORMAL",
                "BLOCKSMS": "",
                "LANGUAGE1": "1",
                "CELLID": "Cellid1234",
                "FTXNID": "FTxnId345"
	        }
    }
    */), operations);
    }

}
