package framework.features.pinManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.pageObjects.pinManagement.AddQnToMasterList;
import framework.pageObjects.pinManagement.DeleteQuestions_pg1;
import framework.pageObjects.pinManagement.ManageSelfPinResetRules_pg1;
import framework.pageObjects.pinManagement.ResetPinPage;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PinManagement {

    private static WebDriver driver;
    private static ExtentTest pNode;

    public static PinManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new PinManagement();
    }

    public void deleteQuestion(String question) throws Exception {

        Markup m = MarkupHelper.createLabel("Delete Question from Master List", ExtentColor.ORANGE);
        pNode.info(m);

        DeleteQuestions_pg1 page1 = new DeleteQuestions_pg1(pNode);

        page1.navDeleteQuestions();
        page1.selectSpecificQuestion(question);
        page1.clickDeleteBtn();
        AlertHandle.acceptAlert(pNode);

        if (ConfigInput.isAssert) {
            Assertion.verifyActionMessageContain("question.delete.success", "Question deleteion successful", pNode, question);
        }
    }


    public void addPinResetRulesForCategory(User user, List<String> questions, String maxBalance) throws Exception {

        Markup m = MarkupHelper.createLabel("addPinResetRulesForCategory", ExtentColor.ORANGE);
        pNode.info(m);

        ManageSelfPinResetRules_pg1 page1 = new ManageSelfPinResetRules_pg1(pNode);

        page1.navManageSelfRestPinRules();
        page1.clickAddNewRule();

        //different category code user for whs in the app, hence used "Constants.WHS_CATEGORY_CODE"
        page1.selectDomain(Constants.WHS_CATEGORY_CODE);

        page1.selectCategory(Constants.WHOLESALER);
        for (String qn : questions) {
            page1.selectQuestions(qn);
        }
        page1.setMaxAccBalance(maxBalance);
        Thread.sleep(1000);

        page1.clickSubmit();
        Thread.sleep(2000);

        if (ConfigInput.isAssert) {
            Assertion.verifyActionMessageContain("pinmanagement.addinitiate.message.success", "Self PIN reset rule creation successful", pNode, user.CategoryName);
        }
    }

    public void editPinResetRulesForCategory(User user, List<String> questions, String maxBalance) throws Exception {

        Markup m = MarkupHelper.createLabel("editPinResetRulesForCategory", ExtentColor.ORANGE);
        pNode.info(m);

        ManageSelfPinResetRules_pg1 page1 = new ManageSelfPinResetRules_pg1(pNode);

        page1.navManageSelfRestPinRules();
        page1.clickOnEdit();

        for (String qn : questions) {
            page1.selectQuestions(qn);
        }
        page1.setMaxAccBalance(maxBalance);
        Thread.sleep(1000);
        page1.clickSubmit();

        if (ConfigInput.isAssert) {
            Assertion.verifyActionMessageContain("pinmanagement.modify.message.success", "Self PIN reset rule modified successful", pNode, user.CategoryName);
        }
    }

    public void resetPINbyUser(User usr, String mpinORtpin, String newPin) throws Exception {

        Markup m = MarkupHelper.createLabel("Reset PIN by user", ExtentColor.ORANGE);
        pNode.info(m);

        ArrayList<String> question = MobiquityGUIQueries.dbGetQuestionsForCategory(Constants.LANGUAGE1, usr.CategoryCode);

        ResetPinPage page1 = new ResetPinPage(pNode);

        page1.navRestPin();
        Thread.sleep(2000);

        for (int i = 0; i < question.size(); i++) {
            String qn = page1.getQuestion();
            String ques = MobiquityGUIQueries.dbGetQuestionCodeForQuestion(qn.trim());
            TxnResponse res = Transactions.init(pNode).getAnswerForUserQns(usr, ques);
            String ans = res.getResponse().extract().jsonPath().getString("COMMAND.ANSWER").toString();
            page1.setAnswer(ans);
            page1.clickNext();
        }

        if (ConfigInput.isConfirm) {
            page1.setNewPin(newPin);
            page1.setConfirmPin(newPin);
            page1.selectPinType(mpinORtpin);
            page1.clickSubmit();
        }

        if (ConfigInput.isAssert) {
            if (mpinORtpin.equalsIgnoreCase(Constants.RESET_MPIN_CONST))
                Assertion.verifyActionMessageContain("mPin.reset.success", "PIN Reset successful", pNode, "0.00", "0.00", "0.00");
            else
                Assertion.verifyActionMessageContain("pin.reset.success", "PIN Reset successful", pNode, "0.00", "0.00", "0.00");

        }
    }

    public void addQuestionsToMaster(HashMap<String, String> questions, String languageCode) throws Exception {

        Markup m = MarkupHelper.createLabel("Add Questions to Master List", ExtentColor.ORANGE);
        pNode.info(m);

        AddQnToMasterList page1 = new AddQnToMasterList(pNode);

        page1.navAddQnsToMasterList();
        page1.downloadQuestionTemplate();
        String file = page1.setQuestionsInTemplate(questions, languageCode);
        page1.setUploadFile(file);
        page1.clickSubmit();
        Utils.putThreadSleep(9000);

        if (ConfigInput.isAssert) {
            Assertion.verifyActionMessageContain("pinmanagement.questions.addedinthe.masterquestion.list", "Question Addition successful", pNode);
        }
    }

}
