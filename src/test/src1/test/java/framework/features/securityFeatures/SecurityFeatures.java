package framework.features.securityFeatures;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.features.common.Login;
import framework.pageObjects.userManagement.ChangePasswordPage;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

/**
 * Created by navin.pramanik on 9/7/2017.
 */
public class SecurityFeatures {

    private static ExtentTest pNode;
    private static WebDriver driver;


    public static SecurityFeatures init(ExtentTest t1) {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new SecurityFeatures();
    }


    public void goBack(String state) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("goBackAfterLogout", ExtentColor.TEAL);
            pNode.info(m);

            if (state.equalsIgnoreCase(Constants.CONSTANT_LOGIN)) {
                driver.navigate().back();
                pNode.info("Clicked on Back Button");
            } else {
                Login.init(pNode).performLogout();
                driver.navigate().back();
                pNode.info("Clicked on Back Button");
            }

            if (Login.init(pNode).checkLogoutLinkPresent()) {
                String code = "Test Case Failed \n" + "Security Alert..\n" + "After Clicking Back again Login Page Displayed.";
                m = MarkupHelper.createCodeBlock(code);
                pNode.fail(m);
                Assertion.attachScreenShotAndLogs(pNode);
            } else {
                Assertion.verifyErrorMessageContain("login.index.label.successlogoutxss", "Invalid Access", pNode);
                pNode.pass("Application does not allow to go back to the Application on clicking Browser Back button.");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void goBackAfterLogIn() throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("goBackAfterLogIn", ExtentColor.TEAL);
            pNode.info(m);

            driver.navigate().back();
            pNode.info("Clicked on Back Button");


            if (Login.init(pNode).checkLogoutLinkPresent()) {
                String code = "Test Case Failed \n" + "Security Alert..\n" + "After Clicking Back , Application not Logged out.";
                m = MarkupHelper.createCodeBlock(code);
                pNode.fail(m);
                Assertion.attachScreenShotAndLogs(pNode);
            } else {
                Assertion.verifyErrorMessageContain("login.index.label.successlogoutxss", "Invalid Access", pNode);
                pNode.pass("After Clicking back Application Logged Out");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void checkAutoComplete() throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("checkAutoComplete", ExtentColor.TEAL);
            pNode.info(m);

            Login.init(pNode).openApplication();

            boolean unameAutoOff = Assertion.verifyEqual(Login.init(pNode).checkUserNameAutoComplete(), true, "Verify UserName AutoComplete", pNode);
            boolean pwdAutoOff = Assertion.verifyEqual(Login.init(pNode).checkPasswordAutoComplete(), true, "Verify UserName AutoComplete", pNode);

            if (unameAutoOff) {
                pNode.pass(" UserName Field is set to Auto Complete OFF");
            }

            if (pwdAutoOff) {
                pNode.pass(" Password Field is set to Auto Complete OFF");
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void changePassword(String oldPwd, String newPwd, String confrmPwd) throws IOException {

        try {

            Markup m = MarkupHelper.createLabel("changePassword", ExtentColor.ORANGE);
            pNode.info(m);

            ChangePasswordPage changePasswordPage = ChangePasswordPage.init(pNode);

            changePasswordPage.clickChangePwdLink();
            changePasswordPage.setOldPassword(oldPwd);
            changePasswordPage.setNewPassword(newPwd);
            changePasswordPage.setConfirmPassword(confrmPwd);
            changePasswordPage.clickSubmit();

            if (ConfigInput.isAssert)
                Assertion.verifyActionMessageContain("changePassword.label.success", "Password Successfully Changed", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }


}
