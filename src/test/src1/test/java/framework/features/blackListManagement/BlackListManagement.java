/*
 * ******************************************************************************
 *  * COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  * <p>
 *  * This software is the sole property of Comviva and is protected by copyright
 *  * law and international treaty provisions. Unauthorized reproduction or
 *  * redistribution of this program, or any portion of it may result in severe
 *  * civil and criminal penalties and will be prosecuted to the maximum extent
 *  * possible under the law. Comviva reserves all rights not expressly granted.
 *  * You may not reverse engineer, decompile, or disassemble the software, except
 *  * and only to the extent that such activity is expressly permitted by
 *  * applicable law notwithstanding this limitation.
 *  * <p>
 *  * THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  * EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  * WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  * YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  * Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  * USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  * OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 9/12/2017
 *  Purpose: Feature of BlackListManagement
 */


package framework.features.blackListManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.blacklist.BlacklistP2P_Page1;
import framework.pageObjects.blacklist.CustomerBlacklist_Page;
import framework.pageObjects.blacklist.UnBlacklistP2P_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class BlackListManagement {


    private static volatile ExtentTest pNode;

    public static BlackListManagement init(ExtentTest t1) {
        pNode = t1;
        return new BlackListManagement();
    }



    /*
     *  ____  _        _    ____ _  ___     ___ ____ _____ __  __  ____ __  __ _____
     * | __ )| |      / \  / ___| |/ / |   |_ _/ ___|_   _|  \/  |/ ___|  \/  |_   _|
     * |  _ \| |     / _ \| |   | ' /| |    | |\___ \ | | | |\/| | |  _| |\/| | | |
     * | |_) | |___ / ___ \ |___| . \| |___ | | ___) || | | |  | | |_| | |  | | | |
     * |____/|_____/_/   \_\____|_|\_\_____|___|____/ |_| |_|  |_|\____|_|  |_| |_|
     */

    /**
     *
     * @param recMSISDN
     * @param sendMSISDN
     * @param provider
     * @return
     * @throws IOException
     */

    /**
     * Method is to do Blacklist P2P Sender
     *
     * @param recMSISDN  parameter for Receiving MSISDN
     * @param sendMSISDN parameter for Sending MSISDN
     * @param provider   parameter for MFS Provider
     * @return Return current instance
     */
    public BlackListManagement doBlacklistP2PSender(String recMSISDN, String sendMSISDN, String provider) throws IOException {

        try {
            Markup markup = MarkupHelper.createLabel("doBlacklistP2PSender", ExtentColor.ORANGE);
            pNode.info(markup);

            BlacklistP2P_Page1 page1 = new BlacklistP2P_Page1(pNode);

            page1.navigateToLink();
            page1.setReceiverMSISDN(recMSISDN);
            page1.selectRecProvider(provider);
            page1.setSenderMSISDN(sendMSISDN);
            page1.selectSenderProvider(provider);
            page1.setRemarks("Automation");
            page1.clickBlacklistButton();
            if(ConfigInput.isConfirm){
                page1.clickConfirm();

                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("blacklist.message.approved", "BlackList P2P", pNode);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param recMSISDN parameter for Receiver's MSISDN
     * @param provider  MFS Provider
     * @return current instance of Blacklist Management
     * @throws IOException IO Exception
     */
    public BlackListManagement unBlacklistP2P(String recMSISDN, String provider, String[] blackList) throws IOException {
        try {
            Markup markup = MarkupHelper.createLabel("unBlacklistP2P", ExtentColor.PINK);
            pNode.info(markup);

            UnBlacklistP2P_Page1 page1 = UnBlacklistP2P_Page1.init(pNode);

            for(String msisdn : blackList){
                page1.navigateToLink();
                page1.setReceiverMSISDN(recMSISDN);
                page1.selectProvider(provider);
                page1.clickUnblacklistP2P();
                page1.clickDeleteLink(msisdn);
                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("blacklist.error.blacklistuseremovedfromlist",
                            "Un-BlackList P2P for Sender MSISDN: "+ msisdn, pNode);
                }
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param fName    : First Name of
     * @param lastname
     * @param dob
     * @return
     * @throws IOException
     */
    public BlackListManagement doCustomerBlacklist(String fName, String lastname, String dob) throws IOException {
        try {
            Markup markup = MarkupHelper.createLabel("doCustomerBlacklist", ExtentColor.PINK);
            pNode.info(markup);

            CustomerBlacklist_Page page = new CustomerBlacklist_Page(pNode);

            page.navigateToLink();
            page.setFirstNameTbox(fName);
            page.setLastNameTbox(lastname);
            page.setDateOfBirth(dob);
            page.clickSubmitButton();

            if (ConfigInput.isConfirm) {
                page.clickConfirm();
            }
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("blacklist.addSingle.success", "Customer BlackList", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * uploadBulkCustomerBlacklist
     *
     * @param fileName
     * @return
     * @throws IOException
     */
    public BlackListManagement uploadBulkCustomerBlacklist(String fileName) throws IOException {
        try {
            Markup markup = MarkupHelper.createLabel("uploadBulkCustomerBlacklist", ExtentColor.PINK);
            pNode.info(markup);

            CustomerBlacklist_Page page = new CustomerBlacklist_Page(pNode);

            page.navigateToLink();
            page.uploadFile(fileName);
            page.clickSubmitButton();

            if (ConfigInput.isAssert) {
                page.clickBulkConfirm();
                String expected = MessageReader.getDynamicMessage("blacklist.addBulk.success", "1");
                String actual = Assertion.getActionMessage();
                Assertion.verifyEqual(actual, expected, "Bulk Customer BlackList", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public String generateJunkFile() {

        String filePath = "uploads/" + "Blacklist" + DataFactory.getTimeStamp();
        String fileName = filePath + ".txt";

        File f = new File(fileName);

        try {
            FileWriter fileWriter = new FileWriter(f);

            fileWriter.append("new");
            fileWriter.append("\n");

            fileWriter.append("Blkl");
            fileWriter.append(",");
            fileWriter.append("new");
            fileWriter.append(",");
            fileWriter.append("new");
            fileWriter.append(",");
            fileWriter.append("NEW");
            fileWriter.append("\n");

            fileWriter.flush();
            fileWriter.close();

        } catch (Exception e) {
            pNode.error("Error in CSV writer..");
        }finally {

        }
        return f.getAbsolutePath();
    }


    /**
     * @param msisdn
     * @return
     * @throws Exception
     */
    public String generateBulkBlacklistCsvFile(String msisdn)  {

        String filePath = "uploads/" + "Blacklist" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        String header = "First Name*,Last Name*,Date of Birth* (DD/MM/YYYY),Msisdn*";
        //String date = new DateAndTime().getYear(-20);

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = formatter.format(date);


        File f = new File(fileName);

        try {
            FileWriter fileWriter = new FileWriter(f);

            fileWriter.append(header);
            fileWriter.append("\n");

            fileWriter.append("Blkl" + DataFactory.getRandomNumberAsString(3));
            fileWriter.append(",");
            fileWriter.append("Blkl" + DataFactory.getRandomNumberAsString(3));
            fileWriter.append(",");
            fileWriter.append(strDate);
            fileWriter.append(",");
            fileWriter.append(msisdn);
            fileWriter.append("\n");

            fileWriter.flush();
            fileWriter.close();

        } catch (Exception e) {
            pNode.error("Error in CSV writer..");
        }
        return f.getAbsolutePath();
    }

    public String generateBulkInvalidHeaderCsvFile(String msisdn) throws Exception {

        String filePath = "uploads/" + "Blacklist" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        String header = "Junk date*,Junk Name*,Date of Birth* (DD/MM/YYYY),Msisdn*";
        String date = new DateAndTime().getYear(-20);

        File f = new File(fileName);

        try {
            FileWriter fileWriter = new FileWriter(f);

            fileWriter.append(header);
            fileWriter.append("\n");

            fileWriter.append("Blkl");
            fileWriter.append(",");
            fileWriter.append("Blkl");
            fileWriter.append(",");
            fileWriter.append(date);
            fileWriter.append(",");
            fileWriter.append(msisdn);
            fileWriter.append("\n");

            fileWriter.flush();
            fileWriter.close();

        } catch (Exception e) {
            pNode.error("Error in CSV writer..");
        }
        return f.getAbsolutePath();
    }


    public String generateBulkBlacklistCsvFile(String msisdn, String firstName, String lastName, String dob) throws Exception {

        String filePath = "uploads/" + "Blacklist" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        String header = "First Name*,Last Name*,Date of Birth* (DD/MM/YYYY),Msisdn*";
        String date = new DateAndTime().getYear(-20);

        File f = new File(fileName);

        try {
            FileWriter fileWriter = new FileWriter(f);

            fileWriter.append(header);
            fileWriter.append("\n");

            fileWriter.append(firstName);
            fileWriter.append(",");
            fileWriter.append(lastName);
            fileWriter.append(",");
            fileWriter.append(dob);
            fileWriter.append(",");
            fileWriter.append(msisdn);
            fileWriter.append("\n");

            fileWriter.flush();
            fileWriter.close();

        } catch (Exception e) {
            pNode.error("Error in CSV writer..");
        }
        return f.getAbsolutePath();
    }


}

