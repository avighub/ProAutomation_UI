package framework.features.notification;


import framework.util.JsonPathOperation;

import static framework.util.MultiLineString.multiLineString;
import static framework.util.common.Utils.json;

public class NotificationTemplateContracts {
    public static String notificationTemplateJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
               "templateName": "CASHIN.transactionIsSuccessful.sender",
                "description": "Successful response for Cashin for sender",
                 "defaultChannel": "SMS",
               "templateDefinition": [
        {
            "notificationDetails": {
                "toWhom": "{receiver.mobileNumber}",
                "basedOn": "{receiver.preferredChannel}",
                "channel": "SMS"
            },
            "messageDetails": {
                "basedOn": "{receiver.preferredLanguage}",
                "definition": [
                    {
                        "text": "You have successfully performed a Cash In Others transaction of Amt: {transactionAmount} {currency} using a/c of mobileNumber: {sender.mobileNumber}. Transaction was sent to mobile No: {receiver.mobileNumber} on behalf of depositor: {depositor.mobileNumber}. Txn ID: {transactionId}, Charges: {sender.serviceChargeAmount}, Commission: {sender.commissionReceivedAmount} {currency}",
                        "language": "1"
                    },
                    {
                        "text": "Cashin réussie {sender.partyId} et {receiver.partyId}",
                        "language": "2"
                    }
                ]
            }
        }
    ],

    "schedule": {
        "time": [
            "00:00-23:59"
        ]
    },
    "overwrite": true
}
            */), operations);
    }

    public static String deactivateTemplateJson(JsonPathOperation... operations) {
        return json(multiLineString(/*
            {
                  "templateStatus": true,
                  "templateName": "OTP"
            }
            */), operations);
    }
}
