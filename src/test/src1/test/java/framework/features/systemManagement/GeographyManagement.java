package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.Geography;
import framework.pageObjects.Geo_managment.Geography_ModifyPage1;
import framework.pageObjects.Geo_managment.Geography_Page1;
import framework.pageObjects.Geo_managment.Geography_Page2;
import framework.pageObjects.Geo_managment.Geography_Page3;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;


/**
 * Created by febin.thomas on 8/8/2017.
 *
 * @modified by navin.pramanik
 */


public class GeographyManagement {
    private static ExtentTest pNode;

    public static GeographyManagement init(ExtentTest t1) {
        pNode = t1;
        return new GeographyManagement();
    }

    public void addArea(Geography geo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addArea", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Area")
                    .clicksubmit()
                    .selectzone(geo.ZoneName)
                    .add()
                    .adddvalues(geo.AreaCode, geo.AreaName, geo.AreaShortName, geo.AreaDescription)
                    .addgeo();
            if (ConfigInput.isConfirm) {
                Geography_Page3 page1 = new Geography_Page3(pNode);
                page1.confirmbutton();
            }
            if (ConfigInput.isAssert) {
                Utils.putThreadSleep(NumberConstants.SLEEP_5000);
                Assertion.verifyActionMessageContain("grphdomain.addgrphdomain.msg.addsuccess", "Area is added", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Add Zone
     *
     * @param geo --> Geography object
     * @throws Exception
     */

    public GeographyManagement addZone(Geography geo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addZone", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Zone")
                    .clicksubmit()
                    .add()
                    .adddvalues(geo.ZoneCode, geo.ZoneName, geo.ZoneShortName, geo.ZoneDescription)
                    .addgeo();
            if (ConfigInput.isConfirm) {
                Geography_Page3 page2 = new Geography_Page3(pNode);
                page2.confirmbutton();
            }
            if (ConfigInput.isAssert) {
                Utils.putThreadSleep(NumberConstants.SLEEP_2000);
                Assertion.verifyActionMessageContain("grphdomain.addgrphdomain.msg.addsuccess", "Zone is added", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public void modifyArea(Geography geo, String status) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyArea", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Area")
                    .clicksubmit()
                    .selectzone(geo.ZoneName);
            Geography_ModifyPage1 page1 = new Geography_ModifyPage1(pNode);
            page1.selectgeo(geo.AreaName)
                    .clickUpdateButton()
                    .setDomName(geo.AreaName)
                    .setDomShortName(geo.AreaShortName)
                    .setGeoDesc(geo.AreaDescription)
                    .selectStatus(status)
                    .clickModify();
            if (ConfigInput.isConfirm) {
                page1.clickConfirmButton();
            }
            if (ConfigInput.isAssert) {
                Thread.sleep(2000);
                Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Area is Modified", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyZone(Geography geo, String status) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyZone", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Zone")
                    .clicksubmit();
            Geography_ModifyPage1 page1 = new Geography_ModifyPage1(pNode);
            page1.selectgeo(geo.ZoneName)
                    .clickUpdateButton()
                    .setDomName(geo.ZoneName)
                    .setDomShortName(geo.ZoneShortName)
                    .setGeoDesc(geo.ZoneDescription)
                    .selectStatus(status)
                    .clickModify();
            if (ConfigInput.isConfirm) {
                page1.clickConfirmButton();
            }
            if (ConfigInput.isAssert) {
                Thread.sleep(2000);
                Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Zone is Modified", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void deleteZone(Geography geo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("deletezone", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Zone")
                    .clicksubmit();
            Geography_Page2 page2 = new Geography_Page2(pNode);
            page2.selectRadio(geo.ZoneName)
                    .clickDeleteButton();
            AlertHandle.acceptAlert(pNode);
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grphdomain.deletegrphdomain.msg.deletesuccess", "Zone is Deleted", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void deleteArea(Geography geo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("deletearea", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Area")
                    .clicksubmit()
                    .selectzone(geo.ZoneName);
            Geography_Page2 page1 = new Geography_Page2(pNode);
            page1.selectRadio(geo.AreaName)
                    .clickDeleteButton();
            AlertHandle.acceptAlert(pNode);
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grphdomain.deletegrphdomain.msg.deletesuccess", "Area is Deleted", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /*******************************
     *//*SYSTEM CASES*//*
*******************************/
    public void modifyZone_DomName(Geography geo, String domName, String status) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyzone", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Zone")
                    .clicksubmit();
            Geography_ModifyPage1 page1 = new Geography_ModifyPage1(pNode);
            page1.selectgeo(geo.ZoneName)
                    .clickUpdateButton()
                    .setDomName(domName)
                    .setDomShortName(geo.ZoneShortName)
                    .setGeoDesc(geo.ZoneDescription)
                    .selectStatus(status)
                    .clickModify();
            if (ConfigInput.isConfirm) {
                page1.clickConfirmButton();
            }
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Zone is Modified", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void selectZone(Geography geo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addArea", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Area")
                    .clicksubmit()
                    .selectzone(geo.ZoneName)
                    .add();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void addAreaNeg(Geography geo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addArea", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page3 page = new Geography_Page3(pNode);
            page.adddvalues(geo.AreaCode, geo.AreaName, geo.AreaShortName, geo.AreaDescription)
                    .addgeo();
            if (ConfigInput.isConfirm) {
                page.confirmbutton();
            }
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grphdomain.addgrphdomain.msg.addsuccess", "Area is added", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void modifyArea_DomName(Geography geo, String status, String domName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyarea", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Area")
                    .clicksubmit()
                    .selectzone(geo.ZoneName);
            Geography_ModifyPage1 page1 = new Geography_ModifyPage1(pNode);
            page1.selectgeo(geo.AreaName)
                    .clickUpdateButton()
                    .setDomName(domName)
                    .setDomShortName(geo.AreaShortName)
                    .setGeoDesc(geo.AreaDescription)
                    .selectStatus(status)
                    .clickModify();
            if (ConfigInput.isConfirm) {
                page1.clickConfirmButton();
            }
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Area is Modified", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyZoneNoSelections() throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyzone", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Zone")
                    .clicksubmit();
            Geography_ModifyPage1 page1 = new Geography_ModifyPage1(pNode);
            page1.clickUpdateButton();
            Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "No option selected", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyAreaNoSelection(Geography geo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyarea", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Area")
                    .clicksubmit()
                    .selectzone(geo.ZoneName);
            Geography_ModifyPage1 page1 = new Geography_ModifyPage1(pNode);
            page1.clickUpdateButton();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grphdomain.updategrphdomain.msg.updatesuccess", "Area is Modified", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void addAreaSelectZone(Geography geo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addArea", ExtentColor.BROWN);
            pNode.info(m);
            Geography_Page1 page = new Geography_Page1(pNode);
            page.navigateToLink()
                    .selectgeo("Area")
                    .clicksubmit()
                    .submitBtn2();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grphdomain.addgrphdomain.msg.addsuccess", "Area is added", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }
}
