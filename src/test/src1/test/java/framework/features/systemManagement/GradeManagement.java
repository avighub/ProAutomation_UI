package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.Grade;
import framework.pageObjects.gradeManagement.GradeAddPage;
import framework.pageObjects.gradeManagement.GradeDeletePage;
import framework.pageObjects.gradeManagement.GradeModifyPage;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Button;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;


/**
 * Created by navin.pramanik on 8/8/2017.
 */
public class GradeManagement {

    private static ExtentTest pNode;
    private static WebDriver driver;

    public static GradeManagement init(ExtentTest t1) {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new GradeManagement();
    }

  /*
         ____ ____     _    ____  _____ __  __    _    _   _    _    ____ _____ __  __ _____ _   _ _____
       / ___|  _ \    / \  |  _ \| ____|  \/  |  / \  | \ | |  / \  / ___| ____|  \/  | ____| \ | |_   _|
      | |  _| |_) |  / _ \ | | | |  _| | |\/| | / _ \ |  \| | / _ \| |  _|  _| | |\/| |  _| |  \| | | |
      | |_| |  _ <  / ___ \| |_| | |___| |  | |/ ___ \| |\  |/ ___ \ |_| | |___| |  | | |___| |\  | | |
       \____|_| \_\/_/   \_\____/|_____|_|  |_/_/   \_\_| \_/_/   \_\____|_____|_|  |_|_____|_| \_| |_|

    */

    /**
     * @param grade - Object of Grade
     * @throws Exception
     */
    public void addGrade(Grade grade) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("addGrade", ExtentColor.AMBER);
            pNode.info(m);
            GradeAddPage gradeAddPage1 = new GradeAddPage(pNode);

            gradeAddPage1.navigateToLink();
            gradeAddPage1.clickAddButton();
            Utils.putThreadSleep(NumberConstants.SLEEP_1000);
            gradeAddPage1.selectDomainByValue(grade.DomainCode);
            gradeAddPage1.selectCategoryByValue(grade.CategoryCode);
            gradeAddPage1.enterGradeName(grade.GradeName);
            gradeAddPage1.enterGradeCode(grade.GradeCode);
            gradeAddPage1.clickSaveButton();
            // If Condition Added for Negative case for which Confirm Click is not required
            if (ConfigInput.isConfirm) {
                gradeAddPage1.clickConfirmButton();
            }
            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("channel.grade.add.success",
                        "Verify Successfully added Grade:" + grade.GradeName, pNode,
                        grade.GradeCode, grade.GradeName)) {
                    grade.setIsCreated(); // set the grade created value to Y
                }
            }


        } catch (NoSuchElementException ex) {
            Assertion.raiseExceptionAndContinue(ex, pNode);
        }

    }


    public void modifyGrade(Grade grade) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyGrade", ExtentColor.AMBER);
            pNode.info(m);
            GradeModifyPage grademodify = new GradeModifyPage(pNode);
            grademodify.navigateToLink();
            grademodify.selectGrade(grade.GradeCode);
            grademodify.modifyGradeName(grade.GradeCode, grade.GradeName + "Mod");
            grademodify.modifyButtonClick();
            grademodify.confirmButtonClick();

            if (ConfigInput.isAssert) {
                String expected = MessageReader.getDynamicMessage("channel.grade.modification.success", grade.GradeCode);
                Assertion.verifyDynamicActionMessageContain(expected, "Group Role Modify", pNode);
            }

        } catch (NoSuchElementException ex) {
            Assertion.raiseExceptionAndContinue(ex, pNode);
        }
    }


    /**
     * @param grade  : Pass the Object of Grade
     * @param accept : Pass true if want to Accept
     * @throws NoSuchElementException
     */
    public void deleteGrade(Grade grade, boolean accept) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("deleteGrade", ExtentColor.AMBER);
            pNode.info(m);
            GradeDeletePage gradeDeletePage = new GradeDeletePage(pNode);

            gradeDeletePage.navigateToLink();
            gradeDeletePage.selectGrade(grade.GradeCode);
            gradeDeletePage.clickDeleteButton();

            if (ConfigInput.isConfirm) {
                gradeDeletePage.confirmDelete();
                AlertHandle.handleAlert(accept, pNode);
            }


            if (ConfigInput.isAssert) {
                String expected = MessageReader.getDynamicMessage("channel.grade.delete.success", grade.GradeCode);
                Assertion.verifyDynamicActionMessageContain(expected, "Group Role Delete", pNode);
            }

        } catch (NoSuchElementException ex) {
            Assertion.raiseExceptionAndContinue(ex, pNode);
        }

    }

    /**
     * @param value Value of Grade
     * @return RowNumber
     */
    public String getGradeRowNumberFromGui(String value) throws Exception {
        String rowNum = null;
        try {
            Markup m = MarkupHelper.createLabel("getGradeRowNumberFromGui", ExtentColor.AMBER);
            pNode.info(m);

            GradeAddPage gradeAddPage1 = new GradeAddPage(pNode);

            gradeAddPage1.navigateToLink();

            rowNum = gradeAddPage1.findRowNoOfGrade(value);

        } catch (NoSuchElementException ex) {
            Assertion.raiseExceptionAndContinue(ex, pNode);
        }
        return rowNum;
    }


    /**
     * Test of the Buttons on GUI for System Test
     *
     * @param grade - Object of Grade
     * @throws NoSuchElementException (Throw NoSuchElementException when Element not found on Page)
     */
    public void addGradeModuleButtonTest(Grade grade, String button) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("addGradeModuleButtonTest", ExtentColor.AMBER);
            pNode.info(m);
            GradeAddPage gradeAddPage1 = new GradeAddPage(pNode);

            addGrade(grade);

            switch (button) {

                case Button.CONFIRM_BACK:
                    gradeAddPage1.clickConfirmBackButton();
                    Assertion.verifyEqual(gradeAddPage1.isSaveButtonVisible(), true, "Verify Successfully navigated to Previous Page. Save button is visible", pNode, true);
                    break;

                case Button.BACK:
                    gradeAddPage1.clickConfirmBackButton();
                    gradeAddPage1.clickBackButton();
                    Assertion.verifyEqual(gradeAddPage1.isAddButtonVisible(), true, "Verify Successfully navigated to Previous Page. Add button is visible", pNode, true);
                    break;

                case Button.ADD_MORE_GRADE:
                    gradeAddPage1.clickConfirmBackButton();
                    gradeAddPage1.clickAddMoreGradeButton();
                    String errorMsg = Assertion.checkErrorPresent();
                    if (errorMsg != null) {
                        String expected = MessageReader.getDynamicMessage("channel.grade.count.limit", AppConfig.maxGradePerCategory);
                        if (Assertion.verifyDynamicErrorMessageContain(expected, "Verify Max Grade Per Category Reached", pNode)) {
                            pNode.skip("Test Case Skipped as Maximum Grade Per Category has been Reached");
                        }
                    } else {
                        Assertion.verifyEqual(gradeAddPage1.isExtraGradeRowVisible(), true, "Verify Extra Grade Row is Visible", pNode, true);
                    }

                    break;

                case Button.ADD:
                    gradeAddPage1.clickConfirmBackButton();
                    gradeAddPage1.clickBackButton();
                    gradeAddPage1.clickAddButton();
                    Assertion.verifyEqual(gradeAddPage1.isSaveButtonVisible(), true, "Verify Successfully navigated to Next Page. Save button is visible", pNode, true);
                    break;

                case Button.SAVE:
                    gradeAddPage1.clickConfirmBackButton();
                    gradeAddPage1.clickSaveButton();
                    Assertion.verifyEqual(gradeAddPage1.isConfirmButtonVisible(), true, "Verify Successfully navigated to Next Page. Confirm button is visible", pNode, true);
                    break;

                case Button.DELETE_GRADE:
                    //TODO Complete the flow
                    break;

                default:
                    gradeAddPage1.clickConfirmBackButton();
            }


        } catch (NoSuchElementException ex) {
            Assertion.raiseExceptionAndContinue(ex, pNode);
        }

    }

    /**
     * For negative test
     * Set the assert flag to false
     * this will be reset once the method has completed in the @AfterMethod
     *
     * @return current instance
     */
    public GradeManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m);
        ConfigInput.isAssert = false;
        return this;
    }

    /**
     * For negative test
     * Set the isAssert flag to false
     * Set the isConfirm flag to false
     * this will be reset once the method has completed in the @AfterMethod
     *
     * @return current instance
     */
    public GradeManagement startNegativeTestWithoutConfirm() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m);
        ConfigInput.isAssert = false;
        ConfigInput.isConfirm = false;
        return this;
    }

}
