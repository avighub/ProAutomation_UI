package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.walletPreference.SystemPreference_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.dbManagement.OracleDB;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.ResultSet;
import java.util.*;

/**
 * Created by Dalia on 15-09-2017.
 */
public class SystemPreferenceManagement {
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static OperatorUser naUpdatePreference;
    private static WebDriverWait wait;

    public static SystemPreferenceManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        wait = new WebDriverWait(DriverFactory.getDriver(), 10);
        try {
            if (naUpdatePreference == null) {
                naUpdatePreference = DataFactory.getOperatorUserWithAccess("PREF001", Constants.NETWORK_ADMIN);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new SystemPreferenceManagement();
    }

    public static SystemPreferenceManagement baseinit(ExtentTest t1) throws Exception {
        pNode = t1;
        wait = new WebDriverWait(DriverFactory.getDriver(), 10);
        return new SystemPreferenceManagement();
    }
    /**
     * Update Specific Preferences
     *
     * @param preference
     * @param value
     * @return
     * @throws Exception
     */
    public SystemPreferenceManagement updateSystemPreference(String preference, String value) throws Exception {
        Markup m = MarkupHelper.createLabel("updateSystemPreference: " + preference, ExtentColor.BROWN);
        pNode.info(m);

        WebDriver driver = DriverFactory.getDriver();


        try {
            // get current preference value
            String cVal = MobiquityGUIQueries.fetchDefaultValueOfPreference(preference);

            if (!cVal.equalsIgnoreCase(value)) {
                if (checkDisplayAndModifiedAllowedFroGUI(preference)) {
                    MobiquityGUIQueries
                            .updateDisplayAndModifiedAllowedFromGUI(preference, "Y");
                    pNode.info("updated " + preference + " preference in the DB");
                }

                // Login as Network admin with update role
                Login.resetLoginStatus();
                Login.init(pNode)
                        .login(naUpdatePreference);

                SystemPreference_page1 page = SystemPreference_page1.init(pNode);

                page.navigateToSystemPreferencePage();
                WebElement ele = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@value='" + preference + "']"))));
                ele.click();
                /*
                todo -  do not commit this
                 */

                /*WebElement ele1 = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@value='LOYALTY_RETRY_COUNT']"))));
                Utils.scrollToAnElement(ele1);*/

                WebElement fieldElement = driver.findElement(By.xpath("//input[@value='" + preference + "']/ancestor::tr[1]/td/input[@type='text']"));
                fieldElement.clear();
                fieldElement.sendKeys(value);
                pNode.info("Set the Value for Preference - " + preference + ", to - " + value);

                page.clickSubmit();
                Utils.captureScreen(pNode);
                Thread.sleep(2000);
                page.clickPreferenceModifySubmit();
                Assertion.verifyActionMessageContain("mBanking.message.success.preferences.update",
                        "Successfully Updated Preference - " + preference, pNode);
            } else {
                pNode.pass("Preference: " + preference + "'s value is already set to: " + value);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        } finally {
            AppConfig.init(); // reload the Appconfig
            DataFactory.loadWallet();
        }
        return this;
    }

    public SystemPreferenceManagement removeServicesFromGroupRole() throws Exception {
        Markup m = MarkupHelper.createLabel("updateSystemPreference", ExtentColor.BROWN);
        pNode.info(m);

        driver = DriverFactory.getDriver();
        try {

            SystemPreference_page1 page = SystemPreference_page1.init(pNode);
            SystemPreference_page1.init(pNode).V5_grouproleradioButton();
            SystemPreference_page1.init(pNode).V5_updateButton();
            SystemPreference_page1.init(pNode).uncheckArole("32");
            SystemPreference_page1.init(pNode).V5_update_GroupRole_Modification();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param preference
     * @return
     * @throws Exception
     */
    public String getSystemPreferenceValue(String preference) throws Exception {
        Markup m = MarkupHelper.createLabel("getSystemPreferenceValue: " + preference, ExtentColor.BROWN);
        pNode.info(m);

        String PreValue = null;
        driver = DriverFactory.getDriver();
        try {
            // Login as Network admin with update role
            Login.init(pNode)
                    .login(naUpdatePreference);

            SystemPreference_page1 page = SystemPreference_page1.init(pNode);

            page.navigateToSystemPreferencePage();

            driver.findElement(By.xpath("//input[@value='" + preference + "']")).click();
            WebElement fieldElement = driver.findElement(By.xpath("//input[@value='" + preference + "']/ancestor::tr[1]/td/input[@type='text']"));

            PreValue = fieldElement.getAttribute("value");
            pNode.info("The Value of System Preference '" + preference + "' , is - " + PreValue);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return PreValue;
    }

    /**
     * @param preference
     * @return
     * @throws Exception
     * @deprecated
     */
    public SystemPreferenceManagement updateMultipleSystemPreference(HashMap<String, String> preference) throws Exception {
        Markup m = MarkupHelper.createLabel("updateMultipleSystemPreference: ", ExtentColor.BROWN);
        pNode.info(m);

        driver = DriverFactory.getDriver();

        Set<String> keys = preference.keySet();
        try {
            // Login as Network admin with update role
            Login.resetLoginStatus();
            Login.init(pNode)
                    .login(naUpdatePreference);

            SystemPreference_page1 page = SystemPreference_page1.init(pNode);
            page.navigateToSystemPreferencePage();
            for (String key : keys) {
                Thread.sleep(2000);
                driver.findElement(By.xpath("//input[@value='" + key + "']")).click();
                WebElement fieldElement = driver.findElement(By.xpath("//input[@value='" + key + "']/ancestor::tr[1]/td/input[@type='text']"));
                fieldElement.clear();
                fieldElement.sendKeys(preference.get(key));
                pNode.info("Set the Value for Preference - " + key + ", to - " + preference.get(key));
            }
            page.clickSubmit();
            Utils.captureScreen(pNode);
            page.clickPreferenceModifySubmit();

            Assertion.verifyActionMessageContain("mBanking.message.success.preferences.update",
                    "Successfully Updated Preferences", pNode);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        } finally {
            AppConfig.init(); // reload the
        }
        return this;
    }

    public static boolean checkDisplayAndModifiedAllowedFroGUI(String value) {
        boolean status = false;
        String query = "select modified_allowed, display_allowed from mtx_system_preferences where preference_code = '" + value + "'";
        OracleDB dbConn = new OracleDB();
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                String modifyAllowed = result.getString("MODIFIED_ALLOWED");
                String displayAllowed = result.getString("DISPLAY_ALLOWED");

                if (modifyAllowed.equals("N") || displayAllowed.equals("N")) {
                    status = true;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public void updateMultipleSystemPreferences(Map<String, String> preferences) throws Exception {
        Markup m = MarkupHelper.createLabel("Update Multiple System Preferences: ", ExtentColor.GREEN);
        pNode.info(m);
        SystemPreference_page1 page = SystemPreference_page1.init(pNode);
        try {

            Set<String> listOfPreferences = preferences.keySet();
            driver = DriverFactory.getDriver();

            boolean status = false;
            for (String preferenceName : listOfPreferences) {
                String preferenceDefaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference(preferenceName);

                if (!preferenceDefaultValue.equalsIgnoreCase(preferences.get(preferenceName))) {

                    status = true;
                    if (checkDisplayAndModifiedAllowedFroGUI(preferences.get(preferenceName))) {
                        MobiquityGUIQueries
                                .updateDisplayAndModifiedAllowedFromGUI(preferences.get(preferenceName), "Y");
                        pNode.info("updated " + preferences.get(preferenceName) + " preference in the DB");
                    }
                    WebElement element = driver.findElement(By.xpath("//input[@value='" + preferenceName + "']"));

                    Login.init(pNode).login(naUpdatePreference);

                    if(!element.isDisplayed()) {
                        page.navigateToSystemPreferencePage();
                    }

                    wait.until(ExpectedConditions.elementToBeClickable(element)).click();

                    WebElement fieldElement = driver.findElement(By.xpath("//input[@value='" + preferenceName + "']/ancestor::tr[1]/td/input[@type='text']"));
                    fieldElement.clear();
                    Utils.putThreadSleep(Constants.TWO_SECONDS);
                    fieldElement.sendKeys(preferences.get(preferenceName));
                    pNode.info("Set the Value for Preference - " + preferenceName + ", to - " + preferences.get(preferenceName));
                }
            }
            if (status) {
                page.clickSubmit();
                Utils.captureScreen(pNode);
                Utils.putThreadSleep(Constants.TWO_SECONDS);
                page.clickPreferenceModifySubmit();
                Assertion.verifyActionMessageContain("mBanking.message.success.preferences.update",
                        "Multiple Preferences Updates Successfully ", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        } finally {
            AppConfig.init();
            DataFactory.loadWallet();
        }
    }

    public void makeSureAppPreferencesAreIntact() throws Exception {
        Markup m = MarkupHelper.createLabel("Make Sure App Preferences Are Intact", ExtentColor.BLUE);
        pNode.info(m);

        try {

            List<String> prefNeedUpdate = new ArrayList<>();
            MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();

            // get preferences whose values are altered
            // get current Preference Map
            Map<String, String> mtxPrefMap = dbHandler.dbGetMtxPreferences();

            Set<String> preferences = GlobalData.prefMap.keySet();

            for (String prefCode : preferences) {
                // get preference default Value
                if (prefCode.equals(""))
                    continue;

                String defVal = GlobalData.prefMap.get(prefCode);
                String currentValue = mtxPrefMap.get(prefCode);
                if (!defVal.equalsIgnoreCase(currentValue))
                    prefNeedUpdate.add(prefCode);
            }

            if (prefNeedUpdate.size() == 0) {
                pNode.pass("App Preference are Intact!");
                return;
            }

            for (String prefCode : prefNeedUpdate) {
                if (checkDisplayAndModifiedAllowedFroGUI(prefCode)) {
                    MobiquityGUIQueries
                            .updateDisplayAndModifiedAllowedFromGUI(prefCode, "Y");
                    pNode.info("updated " + prefCode + " preference in the DB");
                }
            }

            // Login as admin and update the preferences
            Login.init(pNode).loginAsSuperAdmin("PREF001");

            SystemPreference_page1 page = SystemPreference_page1.init(pNode);
            page.navigateToSystemPreferencePage();

            for (String prefCode : prefNeedUpdate) {
                String valueToSet = GlobalData.prefMap.get(prefCode);

                wait.until(ExpectedConditions.elementToBeClickable(DriverFactory.getDriver().findElement(By.xpath("//input[@value='" + prefCode + "']")))).click();
                WebElement fieldElement = DriverFactory.getDriver().findElement(By.xpath("//input[@value='" + prefCode + "']/ancestor::tr[1]/td/input[@type='text']"));
                fieldElement.clear();
                Utils.putThreadSleep(Constants.TWO_SECONDS);
                fieldElement.sendKeys(valueToSet);
                pNode.info("Set the Value for Preference - " + prefCode + ", to - " + valueToSet);
            }

            page.clickSubmit();
            Utils.captureScreen(pNode);
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            page.clickPreferenceModifySubmit();
            Assertion.verifyActionMessageContain("mBanking.message.success.preferences.update",
                    "Multiple Preferences Updates Successfully ", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        } finally {
            AppConfig.init();
            DataFactory.loadWallet();
        }
    }
}
