package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.features.common.Login;
import framework.pageObjects.nonFinancialServiceCharge.*;
import framework.pageObjects.serviceCharge.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by rahul.rana on 5/17/2017.
 */
public class ServiceChargeManagement {
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static OperatorUser usrSChargeCreator, usrSChargeApprover, usrSChargeModify, usrSChargeView, usrSChargeSuspResume;
    private static OperatorUser usrNFSChargeCreator, usrNFSChargeApprover, usrNFSChargeModify, usrNFSChargeView, usrNFSChargeSuspResume;
    private static MobiquityGUIQueries dbHandle;
    private static FunctionLibrary fl;

    // Configurable
    List<String> notActiveStatus = Arrays.asList("UI", "DI", "AI");
    List<String> transferRuleNotRequired = Arrays.asList(Services.ACQFEE, Services.O2C);

    public static ServiceChargeManagement init(ExtentTest t1) throws Exception {

        try {
            driver = DriverFactory.getDriver();
            fl = new FunctionLibrary(driver);
            pNode = t1;

            if (usrSChargeCreator == null) {
                dbHandle = new MobiquityGUIQueries();
                usrSChargeCreator = DataFactory.getOperatorUsersWithAccess("SVC_ADD").get(0);
                usrSChargeApprover = DataFactory.getOperatorUsersWithAccess("SVC_APP").get(0);
                usrSChargeModify = DataFactory.getOperatorUsersWithAccess("SVC_MOD").get(0);
                usrSChargeView = DataFactory.getOperatorUsersWithAccess("SVC_VIEW").get(0);
                usrSChargeSuspResume = DataFactory.getOperatorUsersWithAccess("SVC_SUS").get(0);

                usrNFSChargeCreator = DataFactory.getOperatorUsersWithAccess("CHARGENON").get(0);
                usrNFSChargeApprover = DataFactory.getOperatorUsersWithAccess("CHARGENON_APP").get(0);
                usrNFSChargeModify = DataFactory.getOperatorUsersWithAccess("CHARGENON_MOD").get(0);
                usrNFSChargeSuspResume = DataFactory.getOperatorUsersWithAccess("CHARGENON_SUS").get(0);
                usrNFSChargeView = DataFactory.getOperatorUsersWithAccess("CHARGENON_VIEW").get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new ServiceChargeManagement();
    }

    /**
     * Configure Service Charge
     *
     * @param sCharge
     * @return
     */


    public void configureServiceCharge(ServiceCharge sCharge) throws Exception {
        int resumeCounter = 0;
        Markup m = MarkupHelper.createLabel("configureServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        // Make Sure That Transfer Rule Exists
        if (!transferRuleNotRequired.contains(sCharge.ServiceInfo.ServcieType)) {
            TransferRuleManagement.init(pNode)
                    .configureTransferRule(sCharge);
        }

        /*
         * Check if Service Charge Exists
         */
        Map<String, String> dbSCharge = dbHandle.dbGetServiceCharge(sCharge);

        if (!dbSCharge.isEmpty()) {
            sCharge.setServiceChargeName(dbSCharge.get("Name"));
            sCharge.setServiceChargeId(dbSCharge.get("Code"));
            /*
             * If suspended then Resume the same
             * if Approval Initiated, initiated for remove/delete then reject and Create New
             * If does not exist, create New		 *
             */
            if (dbSCharge.get("Status").equals("Y")) {
                String code[][] = {{"ProfileName", dbSCharge.get("Name")},
                        {"From Category/ Grade", sCharge.Payer.CategoryName + "/ " + sCharge.Payer.GradeName},
                        {"To Category/Grade", sCharge.Payee.CategoryName + "/ " + sCharge.Payee.GradeName}};
                Markup m1 = MarkupHelper.createTable(code);
                pNode.pass(m1);
                sCharge.setIsExisting();
            } else if (dbSCharge.get("Status").equals("S")) {
                resumeServiceCharge(sCharge);
            } else if (notActiveStatus.contains(dbSCharge.get("Status"))) {
                resumeCounter++;
                Login.init(pNode)
                        .login(usrSChargeApprover);

                // reject Service Charge
                rejectServiceCharge(sCharge);
                if (resumeCounter < 3) {
                    configureServiceCharge(sCharge);
                }
            } else if (dbSCharge.get("Status").equals("N")) {
                initiateAndApproveServiceCharge(sCharge);
            }
        } else {
            initiateAndApproveServiceCharge(sCharge);
        }
    }

    /**
     * Initiate Service Charge Modify
     * Initially Modif The service Charge Object and use the same Object to Modify Existing Service Charge
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement modifyServiceChargeGeneralInfo(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("modifyServiceChargeGeneralInfo", ExtentColor.AMBER);
        pNode.info(m); // Method Start Marker

        try {
            ServiceChargeDeleteModify_Pg1 page = ServiceChargeDeleteModify_Pg1.init(pNode);

            page.navModifyDeleteServiceCharge();
            page.selectServiceCharge(sCharge.ServiceChargeName);
            page.clickUpdate();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }

            page.modifyProfileName(sCharge.ServiceChargeName);
            page.modifyProfileId(sCharge.ServiceChargeId);
            page.modifyApplicableDate(sCharge.ApplicableDate);

            page.clickSaveModify();
            page.clickConfirmUpdate();

            Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessmodifymessage",
                    "Modify Existing Service Charge - " + sCharge.ServiceChargeName, pNode);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Delete Service Charge
     *
     * @param sCharge
     * @return TODO - Handle Suspended SCharges
     */
    public ServiceChargeManagement deleteServiceCharge(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("deleteServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Map<String, String> dbSCharge = dbHandle.dbGetServiceCharge(sCharge);
            if (!dbSCharge.isEmpty()) {
                sCharge.setServiceChargeName(dbSCharge.get("Name"));
                sCharge.setServiceChargeId(dbSCharge.get("Code"));
                String status = dbSCharge.get("Status");

                if (status.equals("N")) {
                    pNode.pass("Service Charge has a Status 'N'");
                } else if (notActiveStatus.contains(status)) {
                    Login.init(pNode).login(usrSChargeApprover);
                    rejectServiceCharge(sCharge);
                }
                deleteServiceChargeAllVersions(sCharge);
            } else {
                pNode.pass("Service Charge Does Not exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Delete All versions of a particular Service Charge
     *
     * @param sCharge
     * @return
     * @throws IOException
     */
    public ServiceChargeManagement deleteServiceChargeAllVersions(ServiceCharge sCharge) throws IOException {
        Markup m = MarkupHelper.createLabel("deleteServiceChargeAllVersions", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            configureServiceCharge(sCharge);
            ServiceChargeDeleteModify_Pg1 page = ServiceChargeDeleteModify_Pg1.init(pNode);
            Login.init(pNode).login(usrSChargeModify);

            page.navModifyDeleteServiceCharge();

            int activeVersions = page.getActiveVersionCount(sCharge.ServiceChargeName);

            for (int j = 1; j <= activeVersions; j++) {
                page.navModifyDeleteServiceCharge();
                page.selectServiceCharge(sCharge.ServiceChargeName);
                page.clickConfirmDelete();
                driver.switchTo().alert().accept();

                Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessdeletemessage",
                        "Delete Service Charge-" + sCharge.ServiceChargeName + " version-" + j, pNode);

                Login.init(pNode).login(usrSChargeApprover);

                approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_DELETE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Initiate Service Charge Delete
     *
     * @param sCharge
     * @return
     * @throws IOException
     */
    public ServiceChargeManagement initiateDeleteServiceCharge(ServiceCharge sCharge) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateDeleteServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            ServiceChargeDeleteModify_Pg1 page = ServiceChargeDeleteModify_Pg1.init(pNode);
            Login.init(pNode).login(usrSChargeModify);

            page.navModifyDeleteServiceCharge();
            page.selectServiceCharge(sCharge.ServiceChargeName);
            page.clickConfirmDelete();

            driver.switchTo().alert().accept();

            Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessdeletemessage",
                    "Delete Service Charge-" + sCharge.ServiceChargeName, pNode);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Check if Service charge is not present, create
     * If present and is in some other state then resume the same!
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement initiateAndApproveServiceCharge(ServiceCharge sCharge) throws Exception {
        Login.init(pNode).login(usrSChargeCreator);
        addInitiateServiceCharge(sCharge);
        setCommissionDetail(sCharge);
        completeServiceChargeCreation(sCharge);

        Login.init(pNode).login(usrSChargeApprover);
        approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_CREATE); //TODO - a more specific Method for Service Charge Approval Need to be written

        return this;
    }

    /**
     * Add Initiate Service Charge
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement addInitiateServiceCharge(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addInitiateServiceCharge: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            sCharge.ServiceChargeId = DataFactory.getRandomNumberAsString(6);

            ServiceCharge_Pg2 page2 = ServiceCharge_Pg1.init(pNode)
                    .navAddServiceCharge()
                    .setProfileName(sCharge.ServiceChargeName)
                    .setShortCode(sCharge.ServiceChargeId)
                    .selectSenderProvider(sCharge.Payer.ProviderName)
                    .selectSenderInstrument(sCharge.ServiceInfo.PayerPaymentTypeUI)
                    .selectSenderInstrumentType(sCharge.Payer.PaymentType)
                    .selectReceiverProvider(sCharge.Payee.ProviderName)
                    .selectReceiverInstrument(sCharge.ServiceInfo.PayeePaymentTypeUI)
                    .selectReceiverInstrumentType(sCharge.Payee.PaymentType)
                    .selectServiceType(sCharge.ServiceInfo.ServcieType)
                    .clickNext();

            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(fl.elementIsDisplayed(page2.PayerGrade), true,
                        "Verify that user is successfully navigated to next page once filling the service charge info on Page1", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Set Commission Details - Page 2
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement setCommissionDetail(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("setCommissionDetail", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ServiceCharge_Pg2 pageTwo = ServiceCharge_Pg2.init(pNode);

            /*
            Select Sender and Receiver Grade
             */
            Thread.sleep(Constants.MAX_WAIT_TIME);
            pageTwo.selectSenderGrade(sCharge.Payer.GradeName);
            pageTwo.selectReceiverGrade(sCharge.Payee.GradeName);
            Thread.sleep(Constants.MAX_WAIT_TIME);

            /**
             * Determine:
             *  Credit entity
             *  Payer Commission Category
             *  Payee Commission Category
             */
            String payEntityCode = getPayEntityCode(sCharge);
            String payerCommCatList = getPayerCommissionCategory(sCharge);
            String payeeCommCatList = getPayeeCommissionCategory(sCharge);
            String creditEntity = getCreditEntry(sCharge);

            // Additional settings for SAVINGS CLUB
            String savingClubId = DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId;
            if (sCharge.ServiceInfo.PayeePaymentType.equalsIgnoreCase("wallet") &&
                    sCharge.Payee.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER) &&
                    (DataFactory.getWalletId(sCharge.Payee.PaymentType).equals(savingClubId) ||
                            DataFactory.getWalletId(sCharge.Payer.PaymentType).equals(savingClubId))) {

                payeeCommCatList = sCharge.Payer.ProviderName + "-Payer-" + sCharge.Payee.CategoryName + "-" + sCharge.Payee.PaymentType;

            }

            // select Pay entity
            if (!(sCharge.Payer.CategoryCode.equalsIgnoreCase(Constants.BILL_COMPANY))) {
                pageTwo.selectPayingEntity(payEntityCode);
            } else {
                pageTwo.selectPayingEntityByValue(payEntityCode);
            }

            // Set Credit Entity
            pageTwo.selectCreditedEntity(creditEntity);

            /**
             * Set the constant to '0' in case of ACQFEE
             * as this service charge is required when creating SUBS, if value is above 1 then
             * additional dependencies need to be handled for the subscriber to be active
             */
            String constantVal = (sCharge.ServiceInfo.ServcieType.equals(Services.ACQFEE)) ? "0" : "1";

            // set the fields

            pageTwo.setMultipleOfTransctionAmount(sCharge.MultipleOfTransctionAmount);
            pageTwo.setMaxTransctionAmount(Constants.SCHARGE_MAX_TXN_AMT);
            pageTwo.setMinTransctionAmount(constantVal);
            pageTwo.setminFixedServiceCharged(constantVal);
            pageTwo.setmaxFixedServiceCharged("1");
            pageTwo.setServiceToRange(Constants.SCHARGE_MAX_TXN_AMT);

            if (!sCharge.ServiceInfo.ServcieType.equals(Services.ACQFEE)) {
                pageTwo.setminServiceChargedPCT("1");
                pageTwo.setMaxServiceChargedPCT("1");
                pageTwo.setServiceChargePCT("1");
                if (!sCharge.ServiceInfo.ServcieType.equals(Services.CHURN_REAC_SUB)) {
                    pageTwo.setMinCommissionPCT("0");
                    pageTwo.setMaxCommissionPCT("0");
                    pageTwo.setCommissionRangePCT("0");
                }
            }

            //TO create Acquisition Fees service charge with Charge value more than 0
            if (sCharge.serviceChargeFixed != null) {
                pageTwo.setServiceChargeFixed(sCharge.serviceChargeFixed);
            } else {
                pageTwo.setServiceChargeFixed(constantVal);
            }

            // set the commissions
            if (!sCharge.ServiceInfo.ServcieType.equals(Services.CHURN_REAC_SUB)) {
                pageTwo.setMinFixedCommission(constantVal);
                pageTwo.setMaxfixedCommission("1");

                // pageTwo.setCommissionFromRange("1");
                pageTwo.setCommissionFromRange(sCharge.CommissionFromRange);
                pageTwo.setCommissionToRange(Constants.SCHARGE_MAX_TXN_AMT);

                //TO create Acquisition Fees service charge with Commission value more than 0
                if (sCharge.commisisonRangeFixed != null) {
                    pageTwo.setCommissionRangeFixed(sCharge.commisisonRangeFixed);
                } else {
                    pageTwo.setCommissionRangeFixed(constantVal);
                }

                pageTwo.selectCommissionPayerCategory(payerCommCatList);
                if (!(sCharge.Payer.CategoryCode.equalsIgnoreCase(Constants.BILL_COMPANY))) {
                    pageTwo.selectCommissionPayeeCategory(payeeCommCatList);
                } else {
                    pageTwo.selectCommissionPayeeCategoryByValue(payeeCommCatList);
                }
            }

            if (ConfigInput.isConfirm)
                pageTwo.clickNextButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(fl.elementIsDisplayed(new ServiceCharge_Pg3(pNode).btnAddDetail), true,
                        "Successfully navigated to Add Commission Details Page [3]", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    private String getPayEntityCode(ServiceCharge sCharge) {
        String payEntityCode = null;
        if (sCharge.Payer.CategoryCode.equalsIgnoreCase(Constants.BILL_COMPANY)) {
            payEntityCode = "Payer-" + sCharge.Payer.CategoryCode;
        } else if (sCharge.Payer.GradeCode.equalsIgnoreCase(sCharge.Payee.GradeCode)) {
            payEntityCode = "Sender-" + sCharge.Payee.CategoryName;
        } else if (sCharge.Payee.GradeCode.equals(Constants.OPERATOR) ||
                sCharge.Payee.GradeCode.equals(Constants.BANK) ||
                sCharge.ServiceInfo.ServcieType.equals(Services.ACQFEE)) {

            payEntityCode = sCharge.Payer.CategoryName;
        } else if (sCharge.Payer.CategoryCode.equalsIgnoreCase(sCharge.Payee.CategoryCode)) {
            payEntityCode = "Sender-" + sCharge.Payee.CategoryName;
        } else {
            payEntityCode = sCharge.Payee.CategoryName;
        }
        return payEntityCode;
    }

    private String getCreditEntry(ServiceCharge sCharge) {
        String creditEntity = null;
        if (sCharge.ServiceInfo.PayeePaymentType.equalsIgnoreCase("bank")) {
            creditEntity = sCharge.Payer.ProviderName;
        } else if (sCharge.Payee.GradeCode.equals(Constants.OPERATOR)) {
            creditEntity = sCharge.Payer.ProviderName;
        } else if (sCharge.Payer.GradeCode.equals(Constants.OPERATOR)) {
            creditEntity = sCharge.Payee.ProviderName;
        } else {
            creditEntity = sCharge.Payer.ProviderName;
        }
        return creditEntity;
    }

    private String getPayerCommissionCategory(ServiceCharge sCharge) {
        String payerCommCategory = null;
        if (sCharge.ServiceInfo.PayeePaymentType.equalsIgnoreCase("bank")) {
            payerCommCategory = sCharge.Payee.ProviderName + "-" + sCharge.Payee.PaymentType;
        } else if (sCharge.Payee.GradeCode.equals(Constants.OPERATOR)) {
            payerCommCategory = sCharge.Payer.ProviderName;
        } else if (sCharge.Payer.GradeCode.equals(Constants.OPERATOR)) {
            payerCommCategory = sCharge.Payee.ProviderName;
        } else {
            payerCommCategory = sCharge.Payer.ProviderName;
        }
        return payerCommCategory;
    }

    private String getPayeeCommissionCategory(ServiceCharge sCharge) {
        String payeeCommCatList = null;

        List<String> specialCategoryList = Arrays.asList(
                Constants.SUBSCRIBER,
                Constants.ENTERPRISE
        );

        if (sCharge.ServiceInfo.PayeePaymentType.equalsIgnoreCase("bank")) {
            payeeCommCatList = sCharge.Payer.ProviderName + "-Payer-" + sCharge.Payer.CategoryName + "-" + sCharge.Payer.PaymentType;
        } else if (sCharge.Payee.GradeCode.equals(Constants.OPERATOR)) {
            if (specialCategoryList.contains(sCharge.Payer.CategoryCode)) {
                payeeCommCatList = sCharge.Payer.ProviderName + "-Payer-" + sCharge.Payer.CategoryName + "-" + sCharge.Payer.PaymentType;
            } else if (sCharge.Payer.CategoryCode.equals(Constants.BILL_COMPANY)) {
                payeeCommCatList = "Payer-" + sCharge.Payer.CategoryCode + "-" + sCharge.Payee.ProviderId + "-" + sCharge.Payee.PaymentTypeID;
            } else if (sCharge.Payer.CategoryCode.equals(Constants.ZEBRA_MER)) {
                payeeCommCatList = sCharge.Payer.ProviderName + "-Payer-" + sCharge.Payer.CategoryName + "-" + sCharge.Payer.PaymentType;
            } else {
                payeeCommCatList = sCharge.Payer.ProviderName + "-Payer-" + sCharge.Payer.CategoryName + "-Commission";
            }
        } else if (sCharge.Payer.GradeCode.equals(Constants.OPERATOR)) {
            if (specialCategoryList.contains(sCharge.Payer.CategoryCode)) {
                payeeCommCatList = sCharge.Payee.ProviderName + "-Payee-" + sCharge.Payee.CategoryName + "-" + sCharge.Payee.PaymentType;
            } else {
                payeeCommCatList = sCharge.Payee.ProviderName + "-Payee-" + sCharge.Payee.CategoryName + "-Commission";
            }
        } else {
            if (sCharge.Payer.GradeCode.equals(Constants.OPERATOR)) {
                payeeCommCatList = sCharge.Payee.ProviderName + "-Payee-" + sCharge.Payee.CategoryName + "-" + sCharge.Payee.PaymentType;
            } else {
                payeeCommCatList = sCharge.Payee.ProviderName + "-Payee-" + sCharge.Payee.CategoryName + "-Commission";
            }
        }
        return payeeCommCatList;
    }


    /**
     * Complete Service Charge Creation
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement completeServiceChargeCreation(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("completeServiceChargeCreation: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ServiceCharge_Pg3 pageThree = new ServiceCharge_Pg3(pNode);
            pageThree.fillTaxPage(sCharge);
            pageThree.addDetails();
            pageThree.saveServiceCharge();
            pageThree.confirmServiceCharge();
            Thread.sleep(Constants.TWO_SECONDS);

            Assertion.assertActionMessageContain("servicechargeprofile.message.initiatesuccessaddmessage", "Add Service Charge", pNode);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Approve Pending Service Charge
     *
     * @throws Exception
     */
    public ServiceChargeManagement approvePendingServiceCharges() throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approvePendingServiceCharges", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ServiceChargeApprove_Pg1 pageOne = ServiceChargeApprove_Pg1.init(pNode);

            // Navigate to TCP approval Page
            pageOne.navApproveServiceCharge();

            int rowCount = driver.findElements(By.xpath("//*[@id='content']/table/tbody/tr")).size();
            for (int i = 2; i <= rowCount; i++) {
                String xPathApprove = "//*[@id='content']/table/tbody/tr[2]/td[4]/a[1]";
                driver.findElement(By.xpath(xPathApprove)).click();
                pageOne.clickConfirm();

                Assertion.assertActionMessageContain("servicechargeprofile.message.successaddmessage", "Approve Service Charge", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Reject a specific Service Charge
     *
     * @throws Exception
     */
    public ServiceChargeManagement rejectServiceCharge(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectServiceCharge", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            if (sCharge.isNFSC) {
                NFSCApprove_pg1.init(pNode).navApproveNFSC();
            } else {
                ServiceChargeApprove_Pg1.init(pNode).navApproveServiceCharge();
            }

            int rowCount = driver.findElements(By.xpath("//*[@id='content']/table/tbody/tr")).size();
            for (int i = 2; i <= rowCount; i++) {
                String sNameXpath = "//*[@id='content']/table/tbody/tr[" + i + "]/td[2]";
                String xPathReject = "//*[@id='content']/table/tbody/tr[" + i + "]/td[4]/a[2]";
                if (driver.findElement(By.xpath(sNameXpath)).getText().equals(sCharge.ServiceChargeName)) {
                    Thread.sleep(1000);
                    driver.findElement(By.xpath(xPathReject)).click();
                    driver.switchTo().alert().accept();
                    break;
                }
            }
            Assertion.assertActionMessageContain("servicechargeprofile.rejectsuccessmessage", "Reject Service Charge - " + sCharge.ServiceChargeName, pNode);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Approve Service Charge
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement approveServiceCharge(ServiceCharge sCharge, String approveType) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveServiceCharge", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            if (sCharge.isNFSC) {
                NFSCApprove_pg1.init(pNode).navApproveNFSC();
            } else {
                ServiceChargeApprove_Pg1.init(pNode).navApproveServiceCharge();
            }

            int rowCount = driver.findElements(By.xpath("//*[@id='content']/table/tbody/tr")).size();
            for (int i = 2; i <= rowCount; i++) {
                String sNameXpath = "//*[@id='content']/table/tbody/tr[" + i + "]/td[2]";
                String xPathApprove = "//*[@id='content']/table/tbody/tr[" + i + "]/td[4]/a[1]";
                if (driver.findElement(By.xpath(sNameXpath)).getText().equals(sCharge.ServiceChargeName)) {
                    driver.findElement(By.xpath(xPathApprove)).click();

                    // If approve Delete the Handle the Alert
                    if (approveType.equals(Constants.SERVICE_CHARGE_APPROVE_DELETE)) {
                        driver.switchTo().alert().accept();
                        break;
                    }

                    if (sCharge.isNFSC) {
                        driver.findElement(By.id("chargingAppr_saveApproval_button_confirm")).click();
                    } else {
                        driver.findElement(By.id("profileApp_saveApproval_button_confirm")).click();
                    }
                    pNode.info("Click on Confirm button");
                    break;
                }
            }

            // Assertions
            String msgCode = null; // Actual Message Code
            String rMessage = null; // Reporter Message

            if (approveType.equals(Constants.SERVICE_CHARGE_APPROVE_DELETE)) {
                msgCode = "servicechargeprofile.deletesuccessmessage";
                rMessage = "Delete Service Charge";
            } else {
                msgCode = "servicechargeprofile.message.successaddmessage";
                rMessage = "Approve Service Charge";
            }
            Assertion.assertActionMessageContain(msgCode, rMessage, pNode);
            sCharge.setIsExisting();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * View Service Charge
     *
     * @param sCharge
     * @param lastDay
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement viewServiceChargesLatestVersion(ServiceCharge sCharge, String lastDay) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("viewServiceChargesLatestVersion", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(usrSChargeView);

            ViewServiceCharge_Pg1 page = ViewServiceCharge_Pg1.init(pNode);
            page.navAddServiceCharge();
            page.selectServiceName(sCharge.ServiceChargeName);
            page.setNumberOfDays(lastDay);
            page.clickSubmit();
            page.selectLatestVersion();
            page.clickConfirm();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                Utils.captureScreen(pNode);
                //Assertion.verifyEqual(page.fetchProfileName(), sCharge.ServiceChargeName, "Assert Profile Name", pNode);
                ///Assertion.verifyEqual(page.fetchLastDate(), lastDay, "Assert Profile ID", pNode); // [commented as UI has changed]
                // TODO this method can be extended for more field validations
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /*****************************************************************************************************
     * NEW SUBSCRIBER COMMISSION RULE
     ******************************************************************************************************/

    public void addNewSubsCommissionRule(String ruleName, String count, String valDays) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addNewSubsCommissionRule", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            NewSubsCommissionRule_Page1 commissionRule_page1 = NewSubsCommissionRule_Page1.init(pNode);
            commissionRule_page1.navigateToSubsCommRule();
            commissionRule_page1.setCommRuleName(ruleName);
            commissionRule_page1.setCount(count);
            commissionRule_page1.setValidityDays(valDays);
            commissionRule_page1.clickCheckAll();
            commissionRule_page1.clickSaveButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("ServiceChargeProfileAction.confirmSubsCommissionRule.add.rule", "Subs Comm Rule", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /*****************************************************************************************************
     * NON FINANCIAL SERVICE CHARGE
     ******************************************************************************************************/

    /**
     * Create Non Financial Service Charge Services
     *
     * @param sCharge
     * @throws Exception
     */
    public ServiceChargeManagement configureNonFinancialServiceCharge(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("configureNonFinancialServiceCharge", ExtentColor.BLUE);
        pNode.info(m);// Method Start Marker
        sCharge.setIsNFSC(); // set the flag marking it as NFSC service

        try {
            List<String> notActiveStatus = Arrays.asList("UI", "DI", "AI");
            MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();

            /*
             * Check if Service Charge Exists
             */
            Map<String, String> dbSCharge = dbHandle.dbGetNFSC(sCharge);

            if (!dbSCharge.isEmpty()) {
                sCharge.setServiceChargeName(dbSCharge.get("Name"));
                sCharge.setServiceChargeId(dbSCharge.get("Code"));
                /*
                 * If suspended then Resume the same
                 * if Approval Initiated, initiated for remove/delete then reject and Create New
                 * If does not exist, create New		 *
                 */
                if (dbSCharge.get("Status").equals("Y")) {
                    String code[][] = {{"ProfileName", dbSCharge.get("Name")},
                            {"From Category/ Grade", sCharge.Payer.CategoryName + "/ " + sCharge.Payer.GradeName},
                            {"To Category/Grade", sCharge.Payee.CategoryName + "/ " + sCharge.Payee.GradeName}};
                    Markup m1 = MarkupHelper.createTable(code);
                    pNode.pass(m1);

                    sCharge.setIsExisting();
                } else if (dbSCharge.get("Status").equals("S")) {
                    //resumeSuspendedServiceCharge(sCharge); // TODO
                } else if (notActiveStatus.contains(dbSCharge.get("Status"))) {
                    Login.init(pNode)
                            .login(usrNFSChargeApprover);

                    // Reject the Service Charge
                    rejectServiceCharge(sCharge);

                    // Create New Version of service Charge
                    initiateAndApproveNFSC(sCharge);
                } else if (dbSCharge.get("Status").equals("N")) {
                    initiateAndApproveNFSC(sCharge);
                }
            } else {
                initiateAndApproveNFSC(sCharge);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Check if Service charge is not present, create
     * If present and is in some other state then resume the same!
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement initiateAndApproveNFSC(ServiceCharge sCharge) throws Exception {
        Login.init(pNode).login(usrNFSChargeCreator);

        addInitiateNFSC(sCharge);
        setNfscCommissionValue(sCharge, true);

        Login.init(pNode).login(usrNFSChargeApprover);

        approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_CREATE);
        return this;
    }

    /**
     * Add Initiate Service Charge Page-I
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement addInitiateNFSC(ServiceCharge sCharge) throws Exception {
        List<String> arrServicesSetCommisionZero = Arrays.asList(
                Services.GET_WALLETS_SUBS,
                Services.GET_BANKS_PER_PROVIDER);

        try {
            Markup m = MarkupHelper.createLabel("addInitiateNFSC: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            NonFinancialServiceCharge_Page1 page1 = NonFinancialServiceCharge_Page1.init(pNode); // specific NFSC
            ServiceCharge_Pg1 pageOne = ServiceCharge_Pg1.init(pNode); // standard service charge objects

            sCharge.ServiceChargeId = DataFactory.getRandomNumberAsString(6);

            // * Navigate to Add Service Charge Page *
            page1.navigateToAddNonFinancialServiceCharge();
            pageOne.setProfileName(sCharge.ServiceChargeName);
            pageOne.setShortCode(sCharge.ServiceChargeName);
            page1.selectSenderCategoryName(sCharge.Payer.CategoryName);
            page1.selectSenderGradeName(sCharge.Payer.GradeName);
            pageOne.selectSenderProvider(sCharge.Payer.ProviderName);
            pageOne.selectSenderInstrument(sCharge.ServiceInfo.PayerPaymentTypeUI);

            if (!sCharge.ServiceInfo.PayerPaymentTypeUI.equals("NONE")) {
                pageOne.selectSenderInstrumentType(sCharge.Payer.PaymentType);
            }

            pageOne.selectServiceType(sCharge.ServiceInfo.ServcieType);

            if (arrServicesSetCommisionZero.contains(sCharge.ServiceInfo.ServiceName)) {
                page1.setServiceCharge("0");
            } else {
                page1.setServiceCharge(sCharge.NFSCServiceCharge);
            }

            pageOne.selectReceiverProvider(sCharge.Payee.ProviderName);
            page1.clickNext();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Set commission value Page-II
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement setNfscCommissionValue(ServiceCharge sCharge, boolean... commissionNeeded) throws Exception {
        boolean isCommissionNeeded = commissionNeeded.length > 0 ? commissionNeeded[0] : false;
        try {
            Markup m = MarkupHelper.createLabel("setNfscCommissionValue: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            if (isCommissionNeeded) {
                NonFinancialServiceCharge_Page2 nf = NonFinancialServiceCharge_Page2.init(pNode);
                try {
                    Utils.putThreadSleep(1000);
                    nf.selectPayee(DataFactory.getDefaultProvider().ProviderName);
                    Utils.putThreadSleep(1000);
                    nf.selectPayeewallet("Commission");
                    nf.setCommisionamount(sCharge.NFSCCommission);
                } catch (Exception e) {
                    nf.setComissionAmount(sCharge.NFSCCommission);
                }
            }
            NonFinancialServiceCharge_Page2.init(pNode)
                    .clickNextToBonusSetup()
                    .clickNextToAddTaxPage()
                    .fillAddTaxPage()
                    .clickOnAddServiceCharge()
                    .clickOnConfirm();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessaddmessage",
                        "Add NFSC - " + sCharge.ServiceInfo.ServiceName, pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ServiceChargeManagement viewNFSChargeLatestVersion(ServiceCharge sCharge, String lastDay) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("viewNFSChargeLatestVersion", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(usrNFSChargeView);

            NonFinancialServiceChargeView_Page1 page1 = NonFinancialServiceChargeView_Page1.init(pNode);
            // NonFinancialServiceChargeView_Page2 page2 = NonFinancialServiceChargeView_Page2.init(pNode);

            page1.navigateToViewNFSC();

            page1.selectProfile(sCharge.ServiceChargeName);
            page1.setNoOfdays(lastDay);
            page1.submit();
            page1.clickPage2Submit();


            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);

                Assertion.verifyEqual(page1.fetchProfileName(), sCharge.ServiceChargeName, "Assert Profile Name", pNode);
                //Assertion.verifyEqual(page1.fetchProfileCode(), sCharge.ServiceChargeId, "Assert Profile ID", pNode);
                //Assertion.verifyEqual(page.fetchApplicableDate(), sCharge.ApplicableDate, "Assert Applicable Date", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Initiate Service Charge Modification
     *
     * @param sName
     * @return
     * @throws IOException
     */
    public ServiceChargeManagement initiateServiceChargeModification(String sName) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateServiceChargeModification", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(usrSChargeModify);
            ModifyNFSC_Page1.init(pNode)
                    .navigateToModifyDeleteNFSC()
                    .selectServiceCharge(sName)
                    .clickUpdateButton();

            if (ConfigInput.isAssert) {
                Assertion.assertEqual(ModifyNFSC_Page2.init(pNode).isPageTwoOpen(), true,
                        "Verify that Application Service charge Modification pagfe 2 is open", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }


    /**
     * @param sCharge
     * @param serviceCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement modifyNFSCInitAndApprove(ServiceCharge sCharge, boolean... serviceCharge) throws Exception {
        boolean ser = serviceCharge.length > 0 ? serviceCharge[0] : false;

        Login.init(pNode).login(usrNFSChargeModify);

        if (ser) {
            modifyNFSChargeInitiate(sCharge, true);
        } else {
            modifyNFSChargeInitiate(sCharge);
        }

        Login.init(pNode).login(usrNFSChargeApprover);

        approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_MODIFY);
        return this;
    }


    /**
     * @param sCharge
     * @param modifyServiceCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement modifyNFSChargeInitiate(ServiceCharge sCharge, boolean... modifyServiceCharge) throws Exception {
        boolean amt1 = modifyServiceCharge.length > 0 ? modifyServiceCharge[0] : false;
        try {
            Markup m = MarkupHelper.createLabel("modifyNFSChargeInitiate", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker


            ModifyNFSC_Page1 page1 = ModifyNFSC_Page1.init(pNode);
            ModifyNFSC_Page2 page2 = ModifyNFSC_Page2.init(pNode);
            ModifyNFSC_Page3 page3 = ModifyNFSC_Page3.init(pNode);
            ModifyNFSC_Page4 page4 = ModifyNFSC_Page4.init(pNode);
            ModifyNFSC_Page5 page5 = ModifyNFSC_Page5.init(pNode);
            ModifyNFSC_Page6 page6 = ModifyNFSC_Page6.init(pNode);

            page1.navigateToModifyDeleteNFSC();
            page1.selectServiceCharge(sCharge.ServiceChargeName);
            page1.clickUpdateButton();

            page2.service(sCharge.NFSCServiceCharge);
//            if (amt1) {
//                page2.service("0");
//            }
            page2.next();

            page3.comission(sCharge.NFSCCommission);
            page3.NEXT();

            page4.NEXT();
            page5.submit_Click();
            Thread.sleep(3000);
            page6.clickOnConfirm();
            Thread.sleep(3000);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessmodifymessage", "Modify", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ServiceChargeManagement suspendNFSCharge(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("suspendNFSCharge", ExtentColor.TEAL);
            pNode.info(m);

            Login.init(pNode).login(usrNFSChargeSuspResume);

            SuspendNFSC_Page1 page1 = SuspendNFSC_Page1.init(pNode);

            page1.navigateToSuspendNFSC();
            page1.profile(sCharge.ServiceChargeName);
            page1.save();
            page1.confirm();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("servicechargeprofile.message.successsuspendmessage", "Suspend", pNode);
            }

        } catch (Exception e) {

        }
        return this;
    }

    public ServiceChargeManagement resumeNFSCharge(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("resumeNFSCharge", ExtentColor.TEAL);
            pNode.info(m);

            Login.init(pNode).login(usrNFSChargeSuspResume);

            ResumeNFSC_Page1 page1 = ResumeNFSC_Page1.init(pNode);

            page1.navigateToResumeNFSC();
            page1.profile(sCharge.ServiceChargeName);
            page1.save();
            page1.confirm();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("servicechargeprofile.message.successsuspendmessage", "Resume", pNode);
            }


        } catch (Exception e) {

        }
        return this;
    }

    /**
     * Delete service charge all versions
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement deleteNFSChargeAllVersions(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("deleteNFSChargeAllVersions", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(usrNFSChargeModify);

            ModifyNFSC_Page1 page1 = ModifyNFSC_Page1.init(pNode);

            page1.navigateToModifyDeleteNFSC();
            int activeVersions = page1.getActiveVersionCount(sCharge.ServiceChargeName);

            for (int j = 1; j <= activeVersions; j++) {
                page1.navigateToModifyDeleteNFSC();
                page1.selectServiceCharge(sCharge.ServiceChargeName);
                page1.clickDeleteButton();
                page1.acceptAlert(true);

                Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessdeletemessage", "Resume", pNode);
                approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_DELETE);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**********************************************************************************************************/

    /**
     * @param sCharge
     * @return
     * @throws IOException
     */
    public ServiceChargeManagement suspendServiceCharge(ServiceCharge sCharge) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("suspendServiceCharge", ExtentColor.AMBER);
            pNode.info(m);

            SuspendServiceCharge_Pg1 suspendServiceCharge_pg1 = SuspendServiceCharge_Pg1.init(pNode);

            suspendServiceCharge_pg1.navigatetoLink();
            suspendServiceCharge_pg1.selectprofiletosuspend(sCharge.ServiceChargeName);
            suspendServiceCharge_pg1.clickSaveButton();
            suspendServiceCharge_pg1.clickConfirm();

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("servicechargeprofile.message.successsuspendmessage", "Suspend/ Resume Service Charge ", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }


    /**
     * Resume Service Charge
     *
     * @param sCharge
     * @return --> Object reference
     * @throws IOException
     */
    public ServiceChargeManagement resumeServiceCharge(ServiceCharge sCharge) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("resumeServiceCharge", ExtentColor.AMBER);
            pNode.info(m);

            ResumeServiceCharge_Pg1 resumeServiceCharge_pg1 = ResumeServiceCharge_Pg1.init(pNode);

            resumeServiceCharge_pg1.navigatetoLink();
            resumeServiceCharge_pg1.selectprofiletoresume(sCharge.ServiceChargeName);
            resumeServiceCharge_pg1.clickonsave();
            resumeServiceCharge_pg1.clickConfirm();

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("servicechargeprofile.message.successsuspendmessage", "Suspend/ Resume Service Charge ", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * IN PROGRESS
     *
     * @return
     */
    public ServiceChargeManagement addNewSubsCommissionRule() throws IOException {
        Markup m = MarkupHelper.createLabel("addNewSubsCommissionRule", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            MobiquityGUIQueries dbConn = new MobiquityGUIQueries();
            String ruleId = dbConn.getSubscriberCommissionRuleId();

            if (ruleId != null) {
                pNode.pass("Subscribed Commission Rule Already Exist with id - " + ruleId);
            } else {
                // Add the New Subscriber Commission Rule
                OperatorUser naAddRule = DataFactory.getOperatorUsersWithAccess("SVC_ADDSUBCOM").get(0);
                Login.init(pNode).login(naAddRule);

                AddSubsCommissionRule_pg1 page = AddSubsCommissionRule_pg1.init(pNode);
                page.navAddSubsCommissionRule();
                page.setRuleName("ComRule" + DataFactory.getRandomNumber(3));
                page.setCount("1");
                page.setValidityDays("100");
                page.clickCheckAll();
                page.clickSave();

                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("ServiceChargeProfileAction.confirmSubsCommissionRule.add.rule",
                            "Add Subscriber Commission Rule",
                            pNode);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * Starting a negative Test
     *
     * @return NOTE - this method should be framework in all he Method Files
     */
    public ServiceChargeManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m); // Method Start Marker
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }


    /**
     * Configure nonfinacial service charge with commission amount when sender payment instrument is NONE
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement configureNonFinancialServiceChargewithCommission(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("configureNonFinancialServiceCharge", ExtentColor.BLUE);
        pNode.info(m);// Method Start Marker
        sCharge.setIsNFSC(); // set the flag marking it as NFSC service

        try {
            List<String> notActiveStatus = Arrays.asList("UI", "DI", "AI");
            MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();

            /*
             * Check if Service Charge Exists
             */
            Map<String, String> dbSCharge = dbHandle.dbGetNFSC(sCharge);

            if (!dbSCharge.isEmpty()) {
                sCharge.setServiceChargeName(dbSCharge.get("Name"));
                sCharge.setServiceChargeId(dbSCharge.get("Code"));
                /*
                 * If suspended then Resume the same
                 * if Approval Initiated, initiated for remove/delete then reject and Create New
                 * If does not exist, create New		 *
                 */
                if (dbSCharge.get("Status").equals("Y")) {
                    String code[][] = {{"ProfileName", dbSCharge.get("Name")},
                            {"From Category/ Grade", sCharge.Payer.CategoryName + "/ " + sCharge.Payer.GradeName},
                            {"To Category/Grade", sCharge.Payee.CategoryName + "/ " + sCharge.Payee.GradeName}};
                    Markup m1 = MarkupHelper.createTable(code);
                    pNode.pass(m1);

                    sCharge.setIsExisting();
                } else if (dbSCharge.get("Status").equals("S")) {
                    //resumeSuspendedServiceCharge(sCharge); // TODO
                } else if (notActiveStatus.contains(dbSCharge.get("Status"))) {
                    Login.init(pNode)
                            .login(usrSChargeApprover);

                    // Reject the Service Charge
                    rejectServiceCharge(sCharge);

                    // Create New Version of service Charge
                    initiateAndApproveNFSCwithCommission(sCharge);
                } else if (dbSCharge.get("Status").equals("N")) {
                    initiateAndApproveNFSCwithCommission(sCharge);
                }
            } else {
                initiateAndApproveNFSCwithCommission(sCharge);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ServiceChargeManagement initiateAndApproveNFSCwithCommission(ServiceCharge sCharge) throws Exception {
        Login.init(pNode).login(usrSChargeCreator);

        addInitiateNFSC(sCharge);
        setNfscCommissionValue(sCharge, true);

        if (!usrSChargeCreator.LoginId.equals(usrSChargeApprover.LoginId)) {
            Login.init(pNode).login(usrSChargeApprover);
        }
        approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_CREATE);
        return this;
    }

    public ServiceChargeManagement addInitiateNFSC1(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addInitiateNFSC: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            NonFinancialServiceCharge_Page1 page1 = NonFinancialServiceCharge_Page1.init(pNode); // specific NFSC
            ServiceCharge_Pg1 pageOne = ServiceCharge_Pg1.init(pNode); // standard service charge objects

            // * Navigate to Add Service Charge Page *
            page1.navigateToAddNonFinancialServiceCharge();
            pageOne.setProfileName(sCharge.ServiceChargeName);
            pageOne.setShortCode(sCharge.ServiceChargeName);
            page1.selectSenderCategoryName(sCharge.Payer.CategoryName);
            page1.selectSenderGradeName(sCharge.Payer.GradeName);
            pageOne.selectSenderProvider(sCharge.Payer.ProviderName);
            pageOne.selectSenderInstrument(sCharge.ServiceInfo.PayerPaymentTypeUI);

            if (!sCharge.ServiceInfo.PayerPaymentTypeUI.equals("NONE")) {
                pageOne.selectSenderInstrumentType(sCharge.Payer.PaymentType);
            }

            pageOne.selectServiceType(sCharge.ServiceInfo.ServcieType);

            if (sCharge.ServiceInfo.ServiceName.equals(Services.GET_WALLETS_SUBS) ||
                    sCharge.ServiceInfo.ServiceName.equals(Services.GET_BANKS_PER_PROVIDER)) {
                page1.setServiceCharge("0");
            } else {
                page1.setServiceCharge("10");
            }

            pageOne.selectReceiverProvider(sCharge.Payee.ProviderName);
            page1.clickNext();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ServiceChargeManagement deleteNFSChargeInitiate(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyNFSChargeInitiate", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker


            ModifyNFSC_Page1 page1 = ModifyNFSC_Page1.init(pNode);

            page1.navigateToModifyDeleteNFSC();
            page1.selectServiceCharge(sCharge.ServiceChargeName);
            page1.clickDeleteButton();
            page1.acceptAlert(true);


            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessdeletemessage", "Resume", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public ServiceChargeManagement deleteNFSChargeForSpecificWallet(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("deleteNFSChargeAllVersions", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            Map<String, String> result = dbHandle.dbGetNFSCForSpecificWallet(sCharge);

            Set<String> ids = result.keySet();

            Login.init(pNode).login(usrNFSChargeModify);

            ModifyNFSC_Page1 page1 = ModifyNFSC_Page1.init(pNode);

            // page1.navigateToModifyDeleteNFSC();

            for (String id : ids) {
                if (id != null) {
                    page1.navigateToModifyDeleteNFSC();
                    page1.selectServiceChargebyValue(id);
                    page1.clickDeleteButton();
                    page1.acceptAlert(true);

                    Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessdeletemessage", "Resume", pNode);
                    sCharge.setIsNFSC();
                    sCharge.setServiceChargeName(result.get(id));
                    approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_DELETE);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Following Method is not implemented fully.
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public ServiceChargeManagement financialServiceChargeCalculator(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("financialServiceChargeCalculator", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            ServiceChargeCalculator_Page1 page1 = ServiceChargeCalculator_Page1.init(pNode);
            page1.navigateCalculateServiceCharge();
            page1.selectSenderProvider(sCharge.Payer.ProviderName);
            page1.selectSenderInstrument(sCharge.Payer.PaymentType);
            page1.selectSenderInstrumentType(sCharge.Payer.PaymentType);
            page1.selectSenderGrade(sCharge.Payer.GradeCode);
            page1.selectReceiverProvider(sCharge.Payee.ProviderName);
            page1.selectReceiverInstrument(sCharge.Payee.PaymentType);
            page1.selectReceiverInstrumentType(sCharge.Payee.PaymentType);
            page1.selectReceiverGrade(sCharge.Payee.GradeCode);
            page1.selectServiceType(sCharge.ServiceInfo.ServiceName);
            page1.setTransactionAmount("3");
            page1.clickSubmit();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public void nonFinancialServiceChargeCalculator(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("financialServiceChargeCalculator", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            NonFinancialServiceChargeCalculator_Page1 page1 = NonFinancialServiceChargeCalculator_Page1.init(pNode);
            page1.navigateServiceChargeCalculator();
            page1.selectCategory(sCharge.Payer.CategoryCode);
            page1.selectSenderGrade(sCharge.Payer.GradeCode);
            page1.selectSenderProvider(sCharge.Payer.ProviderName);
            page1.selectSenderInstrument(sCharge.ServiceInfo.PayerPaymentTypeUI);
            page1.selectSenderInstrumentType(sCharge.Payer.PaymentType);
            page1.selectServiceType(sCharge.ServiceInfo.ServcieType);
            page1.clickSubmit();

            if (ConfigInput.isAssert) {
                String serviceChargeName = driver.findElement(By.xpath("//td[@class='tabcol'][contains(text(),'Service Charge Profile')]/following::td[1]")).getText();
                Assertion.verifyEqual(serviceChargeName, sCharge.ServiceChargeName, "Verify Service Charge Name.", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    /**
     * TODO Complete the flow and verify the method
     *
     * @param sCharge
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public String checkServiceChargeStatus(ServiceCharge sCharge) throws IOException, SQLException {
        Markup m = MarkupHelper.createLabel("checkServiceChargeStatus", ExtentColor.TEAL);
        pNode.info(m);
        Map<String, String> dbSCharge = dbHandle.dbGetServiceCharge(sCharge);

        if (!dbSCharge.isEmpty()) {
            sCharge.setServiceChargeName(dbSCharge.get("Name"));
            sCharge.setServiceChargeId(dbSCharge.get("Code"));

            if (dbSCharge.get("Status").equals(Constants.STATUS_ACTIVE)) {
                String code[][] = {{"ProfileName", dbSCharge.get("Name")},
                        {"From Category/ Grade", sCharge.Payer.CategoryName + "/ " + sCharge.Payer.GradeName},
                        {"To Category/Grade", sCharge.Payee.CategoryName + "/ " + sCharge.Payee.GradeName}};
                Markup m1 = MarkupHelper.createTable(code);
                pNode.info(m1);
                sCharge.setIsExisting();
                return "Y";
            } else if (dbSCharge.get("Status").equals(Constants.STATUS_SUSPEND)) {
                return "S";
            } else if (dbSCharge.get("Status").equals(Constants.STATUS_DELETE)) {
                return Constants.STATUS_DELETE;
            } else if (dbSCharge.get("Status").equals(Constants.DELETE_INITIATE_CONSTANT)) {
                return "N";
            }


        }
        return "N";
    }

    public void addNewSubsCommissionRuleForSpecificService(String ruleName, String count, String valDays, String serviceCode) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addNewSubsCommissionRule", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            NewSubsCommissionRule_Page1 commissionRule_page1 = NewSubsCommissionRule_Page1.init(pNode);
            commissionRule_page1.navigateToSubsCommRule();
            commissionRule_page1.setCommRuleName(ruleName);
            commissionRule_page1.setCount(count);
            commissionRule_page1.setValidityDays(valDays);
            commissionRule_page1.setDate(new DateAndTime().getDate(3));
            commissionRule_page1.clickParticularService(serviceCode);
            commissionRule_page1.clickSaveButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("ServiceChargeProfileAction.confirmSubsCommissionRule.add.rule", "Subs Comm Rule", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

}