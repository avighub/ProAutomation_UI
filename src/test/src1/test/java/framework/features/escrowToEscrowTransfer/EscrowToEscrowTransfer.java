package framework.features.escrowToEscrowTransfer;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.PageInit;
import framework.pageObjects.escrowToEscrowTransfer.escrowToEscrowTransferApproval_Page1;
import framework.pageObjects.escrowToEscrowTransfer.escrowToEscrowTransferInitiation_Page1;
import framework.pageObjects.escrowToEscrowTransfer.escrowToEscrowTransferInitiation_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.UserFieldProperties;
import org.testng.Assert;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dalia on 13-03-2018.
 */
public class EscrowToEscrowTransfer {
    private static ExtentTest pNode;

    public static EscrowToEscrowTransfer init(ExtentTest t1) {
        pNode = t1;
        return new EscrowToEscrowTransfer();
    }

    public String initiateEscrowToEscrowTransfer(String provider, String fromBank, String toBank, String refNo, String amount) throws Exception {
        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateEscrowToEscrowTransfer", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            escrowToEscrowTransferInitiation_Page1 page1 = escrowToEscrowTransferInitiation_Page1.init(pNode);

            page1.navigateToEscrowToEscrowTransferInitiationPage();
            page1.selectProviderName(provider);
            page1.setReferenceNumber(refNo);
            page1.selectFromBank(fromBank);
            page1.selectToBank(toBank);
            page1.setAmount(amount);
            page1.clickSubmit();

            if (ConfigInput.isConfirm) {
                page1.clickConfirm();
            }

            if (ConfigInput.isAssert) {
                // Initiate Approval
                String msg = Assertion.getActionMessage();
                //txnId = msg.split("Id is :")[1].split(" ")[0];
                txnId = msg.split("Id is : ")[1];
                Assertion.verifyActionMessageContain("escrow.transfer.initate.success", "Initiated Escrow To Escrow Transfer", pNode, txnId);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    public EscrowToEscrowTransfer approveEscrowToEscrowTransfer(String txnId, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;

        try {
            Markup m = MarkupHelper.createLabel("approveEscrowToEscrowTransfer", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            escrowToEscrowTransferApproval_Page1 page1 = escrowToEscrowTransferApproval_Page1.init(pNode);
            page1.navigateToEscrowToEscrowTransferApprovalPage();
            page1.selectRadioButton(txnId);
            page1.clickSubmit();

            if (remark != null)
                page1.enterRemarks(remark);

            //page1.verifyAmount();
            page1.clickApprove();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("e2e.approval.success", "Escrow To Escrow Transfer Approved", pNode, txnId);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public EscrowToEscrowTransfer rejectEscrowToEscrowTransfer(String txnId, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;
        try {
            Markup m = MarkupHelper.createLabel("rejectEscrowToEscrowTransfer", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            escrowToEscrowTransferApproval_Page1 page1 = escrowToEscrowTransferApproval_Page1.init(pNode);

            page1.navigateToEscrowToEscrowTransferApprovalPage();
            page1.selectRadioButton(txnId);
            page1.clickSubmit();
            page1.enterRemarks(remark);
            page1.clickReject();
            Thread.sleep(Constants.MAX_WAIT_TIME);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("escrow.approval.reject", "Reject Escrow to Escrow", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public EscrowToEscrowTransfer checkInitiateFields() throws Exception {
        //String remark = (remarks.length) > 0 ? remarks[0] : null;

        try {
            Markup m = MarkupHelper.createLabel("approveEscrowToEscrowTransfer", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            escrowToEscrowTransferInitiation_Page1 page1 = escrowToEscrowTransferInitiation_Page1.init(pNode);

            page1.navigateToEscrowToEscrowTransferInitiationPage();

            Object[] expectedData = UserFieldProperties.getLabels("EscrowToEscrowInit");

            Object[] actualData = PageInit.fetchLabelTexts("(.//*[@id='escrowInit_confirmInitiate']/table/tbody//tr/td[1]/label)|.//*[@id='escrowInit_confirmInitiate']/table/tbody//tr/td[3]");

            if (Arrays.equals(actualData, expectedData)) {
                pNode.pass("Labels verified successfully.");
            } else {
                pNode.fail("Label Validation Failed.");
                Utils.captureScreen(pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public EscrowToEscrowTransfer checkApprovalFields(String txnId) throws Exception {
        //String remark = (remarks.length) > 0 ? remarks[0] : null;

        try {
            Markup m = MarkupHelper.createLabel("approveEscrowToEscrowTransfer", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            escrowToEscrowTransferApproval_Page1 page1 = escrowToEscrowTransferApproval_Page1.init(pNode);

            page1.navigateToEscrowToEscrowTransferApprovalPage();

            page1.selectRadioButton(txnId);

            page1.clickSubmit();

            Object[] expectedData = UserFieldProperties.getLabels("EscrowToEscrowAppr");

            Object[] actualData = PageInit.fetchLabelTexts("//form[@id='escrowApprove_displayTransactionDetails']/table/tbody/tr/td[1]/label");

            if (Arrays.equals(actualData, expectedData)) {
                pNode.pass("Labels verified successfully.");
            } else {
                pNode.fail("Label Validation Failed.");
                Utils.captureScreen(pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public HashMap<String, BigDecimal> fetchFromAndToBalance(String provider, String fromBank, String toBank) throws Exception {
        HashMap<String, BigDecimal> map = new HashMap<>();
        try {
            Markup m = MarkupHelper.createLabel("fetchFromAndToBalance", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            escrowToEscrowTransferInitiation_Page1 page1 = escrowToEscrowTransferInitiation_Page1.init(pNode);

            page1.navigateToEscrowToEscrowTransferInitiationPage();
            page1.selectProviderName(provider);
            page1.selectFromBank(fromBank);
            page1.selectToBank(toBank);
            String fromBalance = page1.fetchFromBal();
            if (fromBalance != null || fromBalance != "") {
                BigDecimal fBal = new BigDecimal(fromBalance);
                map.put("fromBalance", fBal);
            }

            Thread.sleep(500);
            String toBalance = page1.fetchToBal();
            if (toBalance != null || toBalance != "") {
                BigDecimal tBal = new BigDecimal(toBalance);
                map.put("toBalance", tBal);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return map;
    }


    public EscrowToEscrowTransfer startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;

    }

    public EscrowToEscrowTransfer expectErrorB4Confirm() {
        Markup m = MarkupHelper.createLabel("expectErrorB4Confirm", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isConfirm = false;
        return this;
    }

    public void verifyEscroToEscrowConfirmScreen(String fromBank, String toBank, BigDecimal fromBal, BigDecimal toBalance, String refNo, String amount) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("verifyEscroToEscrowConfirmScreen", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            escrowToEscrowTransferInitiation_Page2 page2 = escrowToEscrowTransferInitiation_Page2.init(pNode);

            String frmBank = page2.getFromBankName();
            String toBnk = page2.getToBankName();
            BigDecimal frmBal = page2.getFromBalance();
            BigDecimal toBal = page2.getToBalance();
            String ref = page2.getRefNo();
            String requestedAmt = page2.getReqAmt();
            Utils.captureScreen(pNode);

            Assertion.verifyEqual(frmBank, fromBank, "Verify that From Bank Data Matches", pNode);
            Assertion.verifyEqual(toBnk, toBank, "Verify that To Bank Data Matches", pNode);
            Assertion.verifyEqual(frmBal, fromBal, "Verify that From Balance Matches", pNode);
            Assertion.verifyEqual(toBal, toBalance, "Verify that To Balance Matches", pNode);
            Assertion.verifyEqual(ref, refNo, "Verify that Reference Numbser Matches", pNode);
            Assertion.verifyEqual(requestedAmt, amount, "Verify that Amount Matches", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void confirmScreenBackbuttonValidation(String provider, String fromBank, String toBank, String refNo, String amount) throws Exception {

        try {

            startNegativeTest().expectErrorB4Confirm().initiateEscrowToEscrowTransfer(provider, fromBank, toBank, refNo, amount);
            escrowToEscrowTransferInitiation_Page1 page1 = escrowToEscrowTransferInitiation_Page1.init(pNode);
            page1.clickBack();

            Markup m = MarkupHelper.createLabel("InitiateScreenValidationAfterClickingOnBackButton", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Thread.sleep(2000);
            String mfs = page1.getSelectedMFSProvider();
            String frmBank = page1.getSelectedFromBank();
            String toBnk = page1.getSelectedToBank();
            String ref = page1.getRefNo();
            String requestedAmt = page1.getReqAmt();

            Assert.assertEquals(mfs, provider);
            Assert.assertEquals(frmBank, fromBank);
            Assert.assertEquals(toBnk, toBank);
            Assert.assertEquals(ref, refNo);
            Assert.assertEquals(requestedAmt, amount);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public EscrowToEscrowTransfer listOfInitiatedTxnValidation(String txnId, String userId) throws Exception {
        //String remark = (remarks.length) > 0 ? remarks[0] : null;

        try {
            Markup m = MarkupHelper.createLabel("List of Initiated transaction validation", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            String bank1 = DataFactory.getBankId(DataFactory.getTrustBankName(DataFactory.getDefaultProvider().ProviderName));
            String bank2 = DataFactory.getBankId(DataFactory.getNonTrustBankName(DataFactory.getDefaultProvider().ProviderName));

            escrowToEscrowTransferApproval_Page1 page1 = escrowToEscrowTransferApproval_Page1.init(pNode);
            page1.navigateToEscrowToEscrowTransferApprovalPage();

            List<String> txnIds = page1.listOftxnIds();
            int flag1 = 0, flag2 = 0;
            for (int i = 0; i < txnIds.size(); i++) {
                if (txnIds.get(i).contains(txnId))
                    flag1++;
                else
                    flag2++;
            }
            if (flag1 == 1 && flag2 == txnIds.size() - 1) {
                pNode.pass("Initiated Escrow to Escrow transaction is displayed in Approval page");
            } else {
                pNode.fail("Initiated Escrow to Escrow transaction is not displayed in Approval page");
                Assert.fail();
            }

            List<String> usrIds = page1.listOfuserIds();
            int flag = 0;
            for (int i = 0; i < usrIds.size(); i++) {
                //Asserting userId is not equal to bankId due to defect 4981
                if (!(usrIds.get(i).equalsIgnoreCase(userId)) && !(usrIds.get(i).equalsIgnoreCase(bank1)) && !(usrIds.get(i).equalsIgnoreCase(bank2))) {
                    flag++;
                } else {
                    pNode.fail("Transaction initiated by the logged in user is displayed in Approval page");
                    Assert.fail();
                }
            }

            if (flag == usrIds.size()) {
                pNode.pass("Transaction initiated by the logged in user is not displayed in Approval page");
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

}
