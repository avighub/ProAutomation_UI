package framework.features.operatorManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.operatorManagement.OperatorManagementPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class OperatorManagement {
    private static ExtentTest pNode;
    private static OperatorUser usrAddOperator, usrModifyOperator, usrDeleteOperator;
    private String modifiedInterfaceId;

    public static OperatorManagement init(ExtentTest t1) throws Exception {

        try {
            pNode = t1;

            if (usrAddOperator == null) {
                usrAddOperator = DataFactory.getOperatorUserWithAccess("OPPADD");
                usrModifyOperator = DataFactory.getOperatorUserWithAccess("MODOP");
                usrDeleteOperator = DataFactory.getOperatorUserWithAccess("DELOP");

            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return new OperatorManagement();
    }


    public void addRechargeOperator(String operatorName, String providerID) throws IOException, InterruptedException {
        Markup m = MarkupHelper.createLabel("addRechargeOperator", ExtentColor.BLUE);
        pNode.info(m);
        try {

            Login.init(pNode).loginAsOperatorUserWithRole(Roles.ADD_OPERATOR);
            OperatorManagementPage page1 = OperatorManagementPage.init(pNode);

            page1.navigateToAddOperatorLink();
            Thread.sleep(2000);
            page1.setOperatorName(operatorName);
            page1.clickAdd();
            page1.setgradeCode(Constants.GRADE_CODE);
            page1.setsmsc(Constants.SMSC);
            page1.settopUp(Constants.TOPUP);
            page1.selectRechargingOptionsRadio(Constants.RECHARGING_OPTIONS_DENOMINATIONS);
            page1.setrechargeDenominationsSupported(Constants.RECHARGING_DENOMINATIONS_SUPPORTED);
            page1.selectProvider(providerID);
            page1.setIdType(Constants.ID_TYPE_PASSPORT);
//Commented below lines because  its effecting others scripts,have to handle settings in setup
//            if(!ConfigInput.is5_0Build11){
//                page1.setLoginId("login.enter");
//                page1.setPassword("Com@135");
//                page1.setMsisdn("7789679856");
//                page1.setPin("1234");
//                page1.setExternalCode("77896798563333");
//                page1.setUrlDropDown("Recharge Operator");
//                page1.setCountryDropDown("INDIA");
//                page1.setInterfaceDropDown("MULTI COUNTRY");
//                page1.setInterfaceAlias("Interface");
//            }
//
            String idNumber = "" + DataFactory.getRandomNumber(7) + DataFactory.getRandomNumber(7);
            page1.setIdNumber(idNumber);

            page1.addBtn2();
            if (ConfigInput.isConfirm) {
                page1.confirmBtn();
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("operator.add.success", "Operator is successfully add", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public String modifyOperator(String operatorName) throws IOException, InterruptedException {
        Markup m = MarkupHelper.createLabel("modifyOperator", ExtentColor.BLUE);
        pNode.info(m);

        try {
            List<String> status = MobiquityGUIQueries.fetchStatus();

            if (status.contains("Y")) {

                Login.init(pNode).loginAsOperatorUserWithRole(Roles.MODIFY_OPERATOR_USER);
                OperatorManagementPage page1 = OperatorManagementPage.init(pNode);

                page1.navigateToModifyOperatorLink();
                page1.selectOperatorName(operatorName);

                page1.clickOnMod_submit();
                modifiedInterfaceId = page1.interfaceId();

                page1.mod_UpdateBtn();
                page1.mod_ConfirmBtn();

                Assertion.verifyActionMessageContain("operator.modify.success", "Operator is successfully modified", pNode);

                Map<String, String> operatorDetails = MobiquityGUIQueries.fetchRechargeOperatorDetails(operatorName);
                String interfaceId = operatorDetails.get("interfaceId");

                Assertion.assertEqual(modifiedInterfaceId, interfaceId, "InerfaceID modified", pNode);

            } else {
                Assertion.logAsPass("No Recharge Operator to modify... ", pNode);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return modifiedInterfaceId;
    }

    public static void deleteOperator(String operatorName) throws IOException, InterruptedException {
        Markup m = MarkupHelper.createLabel("deleteOperator", ExtentColor.BLUE);
        pNode.info(m);

        try {
            List<String> status = MobiquityGUIQueries.fetchStatus();

            if (status.contains("Y")) {

                Login.init(pNode).loginAsOperatorUserWithRole(Roles.DELETE_OPERATOR);
                OperatorManagementPage page1 = OperatorManagementPage.init(pNode);

                page1.navigateToDeleteOperatorLink();
                Thread.sleep(2000);

                DriverFactory.getDriver().findElement(By.xpath("//tr[td[contains(text(),'" + operatorName + "')]]/td/input[1]")).click();
                Thread.sleep(6000);

                page1.clickOnDeleteOperator();
                page1.clickOnDeleteConfirm();

                Assertion.verifyActionMessageContain("operator.delete.success", "Operator is successfully deleted", pNode);


            } else {
                Assertion.logAsPass("No Recharge Operator to Delete... ", pNode);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAllRechargeOperator() throws IOException, InterruptedException {
        Markup m = MarkupHelper.createLabel("deleteAllRechargeOperator", ExtentColor.BLUE);
        pNode.info(m);

        try {
            List<String> status = MobiquityGUIQueries.fetchStatus();

            if (status.contains("Y")) {

                Login.init(pNode).loginAsOperatorUserWithRole(Roles.DELETE_OPERATOR);
                OperatorManagementPage page1 = OperatorManagementPage.init(pNode);

                page1.navigateToDeleteOperatorLink();
                Thread.sleep(2000);

                List<WebElement> we = DriverFactory.getDriver().findElements(By.xpath("//*[@id='loadOperator1_delOptChecker_initCheck']"));

                for (WebElement w : we) {
                    w.click();
                }

                Thread.sleep(2000);

                page1.clickOnDeleteOperator();
                page1.clickOnDeleteConfirm();

                Assertion.verifyActionMessageContain("operator.delete.success", "Operator is successfully deleted", pNode);

            } else {
                Assertion.logAsPass("No Recharge Operator to Delete... ", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public OperatorManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public OperatorManagement expectErrorB4Confirm() {
        Markup m = MarkupHelper.createLabel("expectErrorB4Confirm", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isConfirm = false;
        return this;
    }

    public void addRechargeOperatorWithFlexiRechargeOption(String operatorName, String providerID) throws IOException, InterruptedException {
        Markup m = MarkupHelper.createLabel("addRechargeOperatorWithFlexiRechargeOption", ExtentColor.BLUE);
        pNode.info(m);
        try {
            Login.init(pNode).loginAsOperatorUserWithRole(Roles.ADD_OPERATOR);
            OperatorManagementPage page1 = OperatorManagementPage.init(pNode);

            page1.navigateToAddOperatorLink();
            Thread.sleep(2000);
            page1.setOperatorName(operatorName);
            page1.clickAdd();
            page1.setgradeCode(Constants.GRADE_CODE);
            page1.setsmsc(Constants.SMSC);
            page1.settopUp(Constants.TOPUP);
            page1.selectRechargingOptionsRadio(Constants.RECHARGING_OPTIONS_FLEXI_RECHARGE);
            page1.setrechargeMinAmountForFlexiRecharge(Constants.RECHARGING_FLEXI_MIN);
            page1.setrechargeMaxAmountForFlexiRecharge(Constants.RECHARGING_FLEXI_MAX);
            page1.selectProvider(providerID);
            page1.setIdType(Constants.ID_TYPE_PASSPORT);

            String idNumber = "" + DataFactory.getRandomNumber(7) + DataFactory.getRandomNumber(7);
            page1.setIdNumber(idNumber);

            page1.addBtn2();
            if (ConfigInput.isConfirm) {
                page1.confirmBtn();
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("operator.add.success", "Operator is successfully add", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


}
