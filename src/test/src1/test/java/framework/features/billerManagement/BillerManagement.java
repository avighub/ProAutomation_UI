/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: automation.team
 *  Date: 7-Dec-2017
 *  Purpose: Biller Management System Cases
 */
package framework.features.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.*;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.autoDebit.AutoDebitEnableApproval_pg1;
import framework.pageObjects.autoDebit.AutoDebit_pg1;
import framework.pageObjects.billerManagement.*;
import framework.pageObjects.userManagement.BarUser_pg1;
import framework.pageObjects.userManagement.UnBarUser_pg1;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.*;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BillerManagement {
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static WebDriver driver;
    private static ExtentTest pNode;
    private static OperatorUser categoryCreator, billerRegistor, billerRegApprovar, naBillerAddValidation,
            billerRegModifier, billerSuspendor, billerSuspendApprover,
            billerTerminator, billerTerminateApprover, naBulkBillerAssoc, naDeleteBillerAssoc, billerActivationInitiator, billerActivationApprover, billerNotificationManagement, billerNotificationAssociator, optBarBiller;

    String newCommonFileHeaderOLD = "Biller ID*,Billed Customer ID,Bill Account ID*,Reference 4,Reference 5," +
            "Bill Number*,Bill Date,Bill Due Date*,Bill Amount*,Requested Operation*,Reserved 1," +
            "Reserved 2,Reserved 3,ProviderID*";

    private FileWriter fileWriter = null;
    private String amount = "100";

    public static BillerManagement init(ExtentTest t1) throws Exception {
        driver = DriverFactory.getDriver();
        pNode = t1;

        try {
            if (categoryCreator == null) {
                optBarBiller = DataFactory.getOperatorUserWithAccess("PTY_BLKL");
                categoryCreator = DataFactory.getOperatorUserWithAccess("UTL_MCAT");
                billerRegistor = DataFactory.getOperatorUserWithAccess("UTL_CREG");
                billerRegApprovar = DataFactory.getOperatorUserWithAccess("UTL_CAPP");
                naBillerAddValidation = DataFactory.getOperatorUserWithAccess("UTL_CAVAL");
                billerRegModifier = DataFactory.getOperatorUserWithAccess("UTL_CMOD");
                billerSuspendor = DataFactory.getOperatorUserWithAccess("UTL_CSUS");
                billerSuspendApprover = DataFactory.getOperatorUserWithAccess("UTL_CSUSAPR");
                billerTerminator = DataFactory.getOperatorUserWithAccess("UTL_CDEL");
                billerTerminateApprover = DataFactory.getOperatorUserWithAccess("UTL_CDAP");
                naBulkBillerAssoc = DataFactory.getOperatorUserWithAccess("UTL_BLKBILASSOC");
                naDeleteBillerAssoc = DataFactory.getOperatorUserWithAccess("UTL_DBLREG");
                billerActivationInitiator = DataFactory.getOperatorUserWithAccess("UTL_CACT");
                billerActivationApprover = DataFactory.getOperatorUserWithAccess("UTL_CSUSAPR");
                billerNotificationAssociator = DataFactory.getOperatorUserWithAccess("NOTIF_CR");
                billerNotificationManagement = DataFactory.getOperatorUserWithAccess("UTL_NOT");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new BillerManagement();
    }

    /**
     * Get String entry corresponding to specific Bill Account Number
     *
     * @param billAccNum
     * @return
     * @throws Exception
     */
    public static String getLogEntry(String billAccNum) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(FilePath.fileBulkBillerRegistrationLog));
        String data = null;

        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                line = br.readLine().toString();
                if (line.contains(billAccNum)) {
                    return line;
                }
            }
            pNode.fail("No entry related to Bill - " + billAccNum + ", is available in Log File!");
        } finally {
            br.close();
        }
        return null;
    }

    /**
     * Get Existing Biller
     * if Biller is not available in the TempBiller.xlsx sheet
     * then this method will create a new biller update the sheet and return the same Biller
     *
     * @return Current instance of Biller
     */
    public Biller getDefaultBiller() throws Exception {
        Biller user = DataFactory.getBillerFromAppdata();

        if (user == null) {
            ExcelUtil.writeTempBillerHeader();

            user = new Biller(DataFactory.getDefaultProvider().ProviderName,
                    Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE,
                    Constants.BILL_SERVICE_LEVEL_PREMIUM);

            createBiller(user);
            user.writeDataToExcel();
        }
        return user;
    }

    public Biller getBillerFromAppData(String processType, String serviceLevel) throws Exception {
        List<Biller> billers = DataFactory.getBillerSet();

        if (billers.size() > 0) {
            for (Biller biller : billers) {
                if (biller.ProcessType.equals(processType) && biller.ServiceLevel.equals(serviceLevel)
                        && biller.Status.equalsIgnoreCase("y")) {

                    // check for DB status
                    String dbStatus = MobiquityGUIQueries.getBillerDbStatus(biller.BillerCode);
                    if (dbStatus != null && dbStatus.equals(Constants.STATUS_ACTIVE)) {
                        return biller;
                    } else {
                        ExcelUtil.deleteBiller(biller);
                    }
                }
            }
        }


        // create specific biller and write to excel for future reference
        Biller newBiller = new Biller(DataFactory.getDefaultProvider().ProviderName,
                Constants.BILLER_CATEGORY,
                processType,
                serviceLevel);
        createBiller(newBiller);

        if (newBiller.getStatus().equals(Constants.STATUS_ACTIVE)) {
            newBiller.writeDataToExcel();
            return newBiller;
        }
        return null;
    }

    /**
     *  Get Biller from App Data
     * @param providerName
     * @param billerCategory
     * @param processType
     * @param serviceLevel
     * @param billerType
     * @param billAmount
     * @param subType
     * @return
     * @throws Exception
     */
    public Biller getBillerFromAppData(
            String providerName,
            String billerCategory,
            String processType,
            String serviceLevel,
            String billerType,
            String billAmount,
            String subType

    ) throws Exception {

        List<Biller> billers = DataFactory.getBillerSet();

        if (billers.size() > 0) {
            for (Biller biller : billers) {
                if (biller.ProcessType.equals(processType) &&
                        biller.ServiceLevel.equals(serviceLevel) &&
                        biller.ProcessType.equals(processType) &&
                        biller.BillerType.equals(billerType) &&
                        biller.BillAmount.equals(billAmount) &&
                        biller.PaymentSubType.equals(subType) &&
                        biller.ProviderName.equals(providerName) &&
                        biller.BillerCategoryName.equals(billerCategory) &&
                        biller.Status.equalsIgnoreCase("y")) {

                    // check for DB status
                    String dbStatus = MobiquityGUIQueries.getBillerDbStatus(biller.BillerCode);
                    if (dbStatus != null && dbStatus.equals(Constants.STATUS_ACTIVE)) {
                        return biller;
                    } else {
                        ExcelUtil.deleteBiller(biller);
                    }
                }
            }
        }


        // create specific biller and write to excel for future reference
        Biller newBiller = new Biller(providerName,
                billerCategory,
                processType,
                serviceLevel,
                billerType,
                billAmount,
                subType
        );
        createBiller(newBiller);

        if (newBiller.getStatus().equals(Constants.STATUS_ACTIVE)) {
            newBiller.writeDataToExcel();
            return newBiller;
        }
        return null;
    }

    /**
     * Create a Biller User
     *
     * @param biller
     * @return
     * @throws Exception
     */
    public BillerManagement createBiller(Biller biller) throws Exception {
        // create Category
        Login.init(pNode).login(categoryCreator);
        addBillerCategory();

        Login.init(pNode).login(billerRegistor);
        initiateBillerRegistration(biller);

        Login.init(pNode).login(billerRegApprovar);
        initiateApproveBiller(biller, true);

        Login.init(pNode).login(naBillerAddValidation);
        addBillerValidation(biller);

        //Change Password for Biller
        CommonUserManagement.init(pNode)
                .changeFirstTimePasswordForBiller(biller);

        return this;
    }

    /**
     * Create a Biller Category, if already exist Continue
     *
     * @return
     */
    public BillerManagement addBillerCategory(String... categoryName) throws Exception {
        Markup m = MarkupHelper.createLabel("addBillerCategory", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String billCategory = (categoryName.length > 0) ? categoryName[0] : Constants.BILLER_CATEGORY;
        try {
            BillerCategoryManagement_pg1 page = new BillerCategoryManagement_pg1(pNode);

            page.navCreateBillerCategory();

            if (page.verifyRoleExists(billCategory) == 0) {
                page.clickOnAdd();
                if (ConfigInput.isAssert) {
                    Assertion.assertErrorInPage(pNode);
                }
                page.setCategoryName(billCategory);
                page.setCategoryCode(billCategory);
                page.setDescription(billCategory);
                page.addBillerCategory();
                page.confirm();

                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("merchant.category.add.message", "Add Biller Category", pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Create a Biller with Liquidation Bank
     *
     * @return
     */

    public BillerManagement createBillerWithLiquidationBank(Biller biller, boolean... isDefault) throws Exception {
        boolean isDefaultBank = isDefault.length > 0 ? isDefault[0] : false;
        // create Category
        Login.init(pNode).login(categoryCreator);
        addBillerCategory();

        Login.init(pNode).login(billerRegistor);
        initiateBillerRegistrationWithLiquidationBankDetails(biller, isDefaultBank);

        Login.init(pNode).login(billerRegApprovar);
        initiateApproveBiller(biller, true);

        Login.init(pNode).login(naBillerAddValidation);
        addBillerValidation(biller);

        return this;
    }

    public BillerManagement initiateBillerRegistrationWithLiquidationBankDetails(Biller biller, boolean... isDefault) throws InterruptedException, IOException {
        boolean isDefaultBank = isDefault.length > 0 ? isDefault[0] : false;
        Markup m = MarkupHelper.createLabel("initiateBillerRegistrationWithLiquidationBankDetails", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {

            BillerRegistrationPage1 page = new BillerRegistrationPage1(pNode);
            BillerRegisteration_pg2 page1 = new BillerRegisteration_pg2(pNode);

            page.navBillerRegistration();
            page.clickAddNewMerchant();
            page.selectBillerGradeName(biller.GradeName);
            page.setLoginId(biller.LoginId);
            page.setPassword(biller.Password);
            page.setConfirmPassword(biller.Password);
            page.setMerchantName(biller.BillerName);
            page.setMerchantCode(biller.BillerCode);
            page.setMerchantEmail(biller.Email);
            page.selectMerchantCategory(biller.BillerCategoryName);
            page.selectProcessType(biller.ProcessType);
            page.selectServiceLevel(biller.ServiceLevel);

            //Added for System Test Case --> Removed Hard Coded values
            page.setAutoBillDeleteFrequency(biller.DeleteFrequency);
            if (!biller.BillAmount.equals(BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE))
                page.selectBillAmountType(biller.BillAmount);

            page.selectBillerType(biller.BillerType);
            page.selectPaymentEffectedTo(biller.PaymentEffectedTo);

            if (!biller.PaymentSubType.equalsIgnoreCase(Constants.BLANK_CONSTANT) && !biller.PaymentSubType.equals(BillerAttribute.SUB_TYPE_NOT_APPLICABLE)) {
                page.selectPartPaymentSubType(biller.PaymentSubType);
            }

            if (!biller.ProviderName.equalsIgnoreCase(Constants.BLANK_CONSTANT)) {
                List<String> providers = DataFactory.getAllProviderNames();
                for (int i = 0; i < providers.size(); i++) {
                    String pId = DataFactory.getProviderId(providers.get(i));
                    page.selectProviderAndTcp(pId, i);
                }
            }

            page.clickButtonNext();

            if (ConfigInput.isConfirm) {

                if (AppConfig.isBankRequiredForBiller) {
                    page1.addBankForBiller(biller, isDefaultBank);
                    page1.clickButtonSubmit();
                    AlertHandle.acceptAlert(pNode);
                    AlertHandle.acceptAlert(pNode);
                }

                page.clickButtonSubmit();


                if (ConfigInput.isAssert) {
                    Assertion.isErrorInPage(pNode);
                    String dynamicMessage = MessageReader.getDynamicMessage("utility.merchant.addInitiated", biller.CategoryName.toLowerCase(), biller.BillerCode);
                    Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Verify Biller Registration", pNode);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }

    /**
     * @param categoryName
     * @param catCode
     * @param desc
     * @return --> Object of BillerManagement
     * @throws Exception
     */
    public BillerManagement addBillerCategory(String categoryName, String catCode, String desc) throws Exception {
        Markup m = MarkupHelper.createLabel("addBillerCategory", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            BillerCategoryManagement_pg1 page = new BillerCategoryManagement_pg1(pNode);

            page.navCreateBillerCategory();
            page.clickOnAdd();
            page.setCategoryName(categoryName);
            page.setCategoryCode(catCode);
            page.setDescription(desc);
            page.addBillerCategory();
            if (ConfigInput.isConfirm)
                page.confirm();


            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("merchant.category.add.message", "Add Biller Category", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    public BillerManagement addBillerCategory(BillerCategory billerCategory, String... back) throws Exception {
        Markup m = MarkupHelper.createLabel("addBillerCategory", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String backClick = (back.length > 0) ? back[0] : null;

        try {
            BillerCategoryManagement_pg1 page = new BillerCategoryManagement_pg1(pNode);

            page.navCreateBillerCategory();
            page.clickOnAdd();
            page.setCategoryName(billerCategory.BillerCategoryName);
            page.setCategoryCode(billerCategory.BillerCategoryCode);
            page.setDescription(billerCategory.Description);
            page.addBillerCategory();

            if (ConfigInput.isConfirm)
                page.confirm();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("merchant.category.add.message", "Add Biller Category", pNode);
                billerCategory.setIsCreated();
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * addBillerCategoryModuleButtonTest
     *
     * @param billerCategory
     * @param button         button
     * @return current instance
     * @throws IOException IOException
     */
    public BillerManagement addBillerCategoryModuleButtonTest(BillerCategory billerCategory, String button) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("billerCategoryButtonTest", ExtentColor.BLUE);
            pNode.info(m);

            BillerCategoryManagement_pg1 page = new BillerCategoryManagement_pg1(pNode);
            addBillerCategory(billerCategory);

            switch (button) {

                case Button.BACK:
                    page.clickOnConfirmBack();
                    page.clickOnBack();
                    if (page.isAddButtonDisplayed()) {
                        pNode.pass("Successfully navigate to previous page.Add button is displayed..");
                        Utils.captureScreen(pNode);
                    }
                    break;

                case Button.CONFIRM_BACK:
                    page.clickOnConfirmBack();
                    if (page.isFirstPageBackButtonDisplayed()) {
                        pNode.pass("Successfully navigate to Previous page.First Page Back button is displayed...");
                        Utils.captureScreen(pNode);
                    }
                    break;

                default:
                    pNode.info("No button clicked...");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * verify Biller Category ConfirmPage Details
     *
     * @param cat
     * @throws Exception
     */
    public void verifyCategoryConfirmPageDetails(BillerCategory cat) throws Exception {
        try {
            BillerCategoryManagement_pg1 page = new BillerCategoryManagement_pg1(pNode);

            Assertion.verifyEqual(page.getConfirmPageCatCode(), cat.BillerCategoryCode, "Verify Biller Category Code", pNode);
            Assertion.verifyEqual(page.getConfirmPageCatName(), cat.BillerCategoryCode, "Verify Biller Category Name", pNode);
            Assertion.verifyEqual(page.getConfirmPageDesc(), cat.Description, "Verify Biller Category Description", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode
            );
        }

    }

    /**
     * Update & Delete
     *
     * @param billerCategory
     * @return
     * @throws Exception
     */
    public BillerManagement updateBillerCategory(BillerCategory billerCategory) throws Exception {
        Markup m = MarkupHelper.createLabel("addBillerCategory", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            BillerCategoryManagement_pg1 page = new BillerCategoryManagement_pg1(pNode);
            BillerCategoryManagementUpdate_Page updatePage = new BillerCategoryManagementUpdate_Page(pNode);

            page.navCreateBillerCategory();
            page.selectBillerCategoryRadio(billerCategory.BillerCategoryCode.toUpperCase());
            page.clickOnUpdate();

            if (billerCategory.Status.equalsIgnoreCase(Constants.STATUS_ACTIVE)) {
                updatePage.setCategoryName(billerCategory.BillerCategoryName);
                updatePage.setDescription(billerCategory.Description);
            }

            updatePage.selectStatus(billerCategory.Status);
            updatePage.clickConfirmButton();
            if (ConfigInput.isConfirm)
                updatePage.clickFinalConfirmButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("merchant.category.modify.message", "Add Biller Category", pNode);
                billerCategory.setIsUpdated();
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * To verify the Details of  Biller Category Update Page
     *
     * @param cat
     * @throws Exception
     */
    public void verifyUpdateCategoryConfirmPageDetails(BillerCategory cat) throws Exception {

        BillerCategoryManagementUpdate_Page page = new BillerCategoryManagementUpdate_Page(pNode);

        Assertion.verifyEqual(page.getConfirmPageCatName(), cat.BillerCategoryName, "Verify Biller Category Name", pNode);
        Assertion.verifyEqual(page.getConfirmPageCatCode(), cat.BillerCategoryCode.toUpperCase(), "Verify Biller Category Code", pNode);
        Assertion.verifyEqual(page.getConfirmPageDesc(), cat.Description, "Verify Biller Category Description", pNode);
    }

    /**
     * For Modify and Delete Biller Category
     *
     * @param categoryCode
     * @param updateDesc
     * @param status
     * @return
     * @throws Exception
     */
    public BillerManagement updateBillerCategory(String categoryCode, String updateDesc, String status) throws Exception {
        Markup m = MarkupHelper.createLabel("updateBillerCategory", ExtentColor.BLUE);
        pNode.info(m);

        try {
            BillerCategoryManagement_pg1 page = new BillerCategoryManagement_pg1(pNode);
            BillerCategoryManagementUpdate_Page updatePage = new BillerCategoryManagementUpdate_Page(pNode);

            page.navCreateBillerCategory();
            page.selectBillerCategoryRadio(categoryCode.toUpperCase());
            page.clickOnUpdate();

            if (status.equalsIgnoreCase(Constants.STATUS_ACTIVE)) {
                updatePage.setDescription(updateDesc);
            }

            updatePage.selectStatus(status);
            updatePage.clickConfirmButton();
            updatePage.clickFinalConfirmButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("merchant.category.modify.message", "Add Biller Category", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }


    /*
     ****************************************************************************************
     ##     B I L L E R    R E G I S T R A T I O N     R E L A T E D   M E T H O D S     ##
     * **************************************************************************************
     */

    public BillerManagement updateBillerCategoryModuleButtonTest(BillerCategory billerCategory, String button) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("updateBillerCategoryModuleButtonTest", ExtentColor.BLUE);
            pNode.info(m);

            updateBillerCategory(billerCategory);
            BillerCategoryManagementUpdate_Page page = new BillerCategoryManagementUpdate_Page(pNode);

            switch (button) {

                case Button.BACK:
                    page.clickOnConfirmBack();
                    page.clickOnBack();
                    if (page.isUpdateButtonDisplayed()) {
                        pNode.pass("Successfully navigate to previous page.Update button is displayed..");
                        Utils.captureScreen(pNode);
                    }
                    break;

                case Button.CONFIRM_BACK:
                    page.clickOnConfirmBack();
                    if (page.isBackButtonDisplayed()) {
                        pNode.pass("Successfully navigate to Previous page.First Page Back button is displayed..");
                        Utils.captureScreen(pNode);
                    }
                    break;

                default:
                    pNode.info("No button clicked...");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * This method will initiate and approve Biller
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public BillerManagement initAndApproveBillerRegistration(Biller biller) throws Exception {

        Login.init(pNode).login(billerRegistor);
        initiateBillerRegistration(biller);

        Login.init(pNode).login(billerRegApprovar);
        initiateApproveBiller(biller, true);

        return this;
    }

    /**
     * Initiate Biller Registration
     *
     * @param biller
     * @throws InterruptedException
     * @throws IOException
     */
    public BillerManagement initiateBillerRegistration(Biller biller) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateBillerRegistration" + biller.ProcessType + "-" + biller.ServiceLevel, ExtentColor.BLUE);
        pNode.info(m);

        try {

            BillerRegistrationPage1 page = new BillerRegistrationPage1(pNode);

            page.navBillerRegistration();
            page.clickAddNewMerchant();
            page.selectBillerGradeName(biller.GradeName);
            page.setLoginId(biller.LoginId);
            if (!AppConfig.isRandomPasswordAllowed) {
                page.setPassword(ConfigInput.userCreationPassword);
                page.setConfirmPassword(ConfigInput.userCreationPassword);
            }
            page.setMerchantName(biller.BillerName);
            page.setMerchantCode(biller.BillerCode);
            page.setMerchantEmail(biller.Email);
            page.selectMerchantCategory(biller.BillerCategoryName);
            page.setContactName(biller.ContactName);
            page.selectServiceLevel(biller.ServiceLevel);

            //Added for System Case
            page.setPayerBillNotificationFrequency(biller.PaidNotificationFrequency);

            //Added for System Test Case --> Removed Hard Coded values
            page.setAutoBillDeleteFrequency(biller.DeleteFrequency);
            page.selectProcessType(biller.ProcessType);
            page.selectBillerType(biller.BillerType);

            if (!biller.BillAmount.equals(BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE))
                page.selectBillAmountType(biller.BillAmount);

            if (!biller.PaymentSubType.equalsIgnoreCase(Constants.BLANK_CONSTANT) && !biller.PaymentSubType.equals(BillerAttribute.SUB_TYPE_NOT_APPLICABLE))
                page.selectPartPaymentSubType(biller.PaymentSubType);

            page.selectPaymentEffectedTo(biller.PaymentEffectedTo);
            page.setOnlineReversalState(biller.supportOnlineTxnReversal);

            if (!biller.ProviderName.equalsIgnoreCase(Constants.BLANK_CONSTANT)) {
                List<String> providers = DataFactory.getAllProviderNames();
                for (int i = 0; i < providers.size(); i++) {
                    String pId = DataFactory.getProviderId(providers.get(i));
                    page.selectProviderAndTcp(pId, i);
                }
            }

            page.clickButtonNext();
            if (ConfigInput.isConfirm)
                //ACCEPT ALERT
                page.clickButtonSubmit();

            if (ConfigInput.isAssert) {
                Assertion.isErrorInPage(pNode);
                String dynamicMessage = MessageReader.getDynamicMessage("utility.merchant.addInitiated", biller.CategoryName.toLowerCase(), biller.BillerCode);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Verify Biller Registration", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }

    /**
     * Initiate Biller Approval
     *
     * @param biller
     * @param isApprove
     * @throws Exception
     */
    public BillerManagement initiateApproveBiller(Biller biller, boolean isApprove) throws Exception {
        Markup m = MarkupHelper.createLabel("approveRejectBiller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            BillerApproveInitiate_pg1 page = new BillerApproveInitiate_pg1(pNode);
            page.navBillerApproveInitiate();
            page.clickOnApproveOrReject(biller.BillerCode, isApprove);

            if (ConfigInput.isConfirm)
                page.clickApproveBiller();

            if (ConfigInput.isAssert) {
                Assertion.isErrorInPage(pNode);
                String dynamicMessage = MessageReader.getDynamicMessage("utility.merchant.approval.add", biller.CategoryName.toLowerCase(), biller.BillerCode);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Verify Biller Registration", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public BillerManagement modifyBiller(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("modifyBiller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(billerRegModifier);

            BillerModification_Page1 page1 = new BillerModification_Page1(pNode);
            BillerModification_Page2 page2 = new BillerModification_Page2(pNode);

            page1.navBillerModification();
            page1.selectBillerToModify(biller.BillerCode);
            page1.clickUpdateButton();
            page2.modifyInterfaceID(biller.BillerCode);

            page2.selectServiceLevel(biller.ServiceLevel);
            page2.selectProcessType(biller.ProcessType);
            page2.selectBillAmountType(biller.BillAmount);
            if (biller.BillerType == BillerAttribute.SERVICE_LEVEL_ADHOC) {
                page2.selectBillerType(BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE);
            } else {
                page2.selectBillerType(BillerAttribute.BILLER_TYPE_BILL_AVAILABLE);
            }
            //page2.selectBillAmountType(biller.BillAmount);
            //page2.selectPayementEffectedTo(biller.PaymentEffectedTo);
            //page2.selectPartPaymentSubType(biller.PaymentSubType);
            page2.clickNextButton();
            page2.clickConfirmButton();

            if (ConfigInput.isAssert) {
                String expected;
                if (ConfigInput.isCoreRelease) {
                    expected = MessageReader.getDynamicMessage("utility.merchant.update.initiate", biller.CategoryName.toLowerCase(), biller.BillerCode);
                    Assertion.verifyEqual(Assertion.getActionMessage(), expected, "Verify Biller Registration", pNode);
                    //TO Approve the Biller according to 5.0
                    Login.init(pNode).loginAsOperatorUserWithRole(Roles.BILLER_MODFICIATION_APPROVAL);
                    modifyApproveBiller(biller);
                } else {
                    expected = MessageReader.getDynamicMessage("utility.merchant.updated", biller.CategoryName.toLowerCase(), biller.BillerCode);
                    Assertion.verifyEqual(Assertion.getActionMessage(), expected, "Verify Biller Registration", pNode);
                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * ModifyBillerApproval
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */

    public BillerManagement modifyBillerApproval(Biller biller, boolean isApprove) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("modifyApproveBillerRegistration", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(billerRegModifier);

            BillerApproveModify_pg1 page = new BillerApproveModify_pg1(pNode);


            page.navBillerApproveInitiate();
            page.clickOnApproveOrReject(biller.BillerCode, isApprove);

            if (ConfigInput.isConfirm)
                page.clickApproveBiller();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("biller.update.success", "Biller Update Success", pNode, biller.BillerCode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    public BillerManagement modifyApproveBiller(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("modifyApproveBiller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            BillerModificationApproval_Page1 page1 = new BillerModificationApproval_Page1(pNode);

            page1.navBillerModificationApproval();
            page1.selectBillerToModifyApprove(biller.BillerCode);
            page1.clickApproveButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("utility.merchant.updated", "Verify Biller Modify Approval", pNode, biller.CategoryName.toLowerCase(), biller.BillerCode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * It will initiate Biller Deletion
     *
     * @param biller
     * @return
     * @throws Exception
     */
    public BillerManagement initiateBillerDeletion(Biller biller) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateBillerDeletion", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {

            BillerDeletion_Page1 page1 = new BillerDeletion_Page1(pNode);
            BillerDeletion_Page2 page2 = new BillerDeletion_Page2(pNode);

            page1.navBillerDeletion();
            page1.selectBillerToDelete(biller.BillerCode);
            page1.clickDeleteButton();

            page2.setRemarks("Automation");
            page2.clickConfirmButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("utility.merchant.delete", "Successfully Initiate Biller deletion",
                        pNode, biller.BillerCode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Approve Biller Deletion
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public BillerManagement approveBillerDeletion(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("initiateBillerRegistration", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {

            BillerDeletionApproval_Page page = new BillerDeletionApproval_Page(pNode);

            page.navBillerDeletion();
            page.clickApprovalLink(biller.BillerCode);
            page.clickApproveButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("utility.merchant.approval.deleted", "Verify Biller Deletion Approved", pNode, biller.BillerCode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Suspend Biller
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public BillerManagement suspendBiller(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("suspendBiller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.resetLoginStatus();

            Login.init(pNode).login(billerSuspendor);

            BillerSuspend_Page page = new BillerSuspend_Page(pNode);


            page.navBillerSuspension();
            page.selectBillerToSuspend(biller.BillerCode);
            page.clickSuspendButton();
            page.clickConfirmButton();

            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("utility.merchant.suspended", biller.CategoryName, biller.BillerCode);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Verify Biller Suspend", pNode);
                biller.setStatus(Constants.STATUS_SUSPEND);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Suspend Initiate Biller
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */

    public BillerManagement suspendInitiateBiller(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("suspendBiller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(billerSuspendor);

            BillerSuspend_Page page = new BillerSuspend_Page(pNode);


            page.navBillerSuspension();
            page.selectBillerToSuspend(biller.BillerCode);
            page.clickSuspendInitiateButton();
            page.clickConfirmButton();

            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("utility.merchant.suspend.initiated", biller.BillerCode);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Verify Biller Suspend Initiated", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Suspend Approve Biller
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */

    public BillerManagement suspendApproveBiller(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("suspendLMSProfile Approve Biller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(billerSuspendApprover);

            BillerSuspend_Page page = new BillerSuspend_Page(pNode);


            page.navBillerSuspensionForApproval();
            page.selectBillerToSuspend(biller.BillerCode);
            page.clickApproveButton();
            page.clickConfirmApproveButton();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("utility.merchant.suspend.approved", "Verify that Biller is succssfully Suspended", pNode, biller.BillerCode)) {
                    biller.setStatus(Constants.STATUS_SUSPEND);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * To activate Biller from Suspension
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public BillerManagement activateBiller(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("activateBiller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(billerActivationInitiator);
            BillerActivation_Page page = new BillerActivation_Page(pNode);


            page.navBillerActivation();
            page.selectBillerToActivate(biller.BillerCode);
            page.clickActivateButton();
            page.clickApproveButton();

            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("utility.merchant.active", biller.CategoryName, biller.BillerCode);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Verify Biller Activate", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * To activate Biller from Suspension
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public BillerManagement initiateActivateBiller(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("initiateActivateBiller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(billerActivationInitiator);

            BillerActivation_Page page = new BillerActivation_Page(pNode);


            page.navBillerActivation();
            page.selectBillerToActivate(biller.BillerCode);
            page.clickInitiateActivateButton();
            page.clickApproveButton();

            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("utility.merchant.activeInitiate", biller.BillerCode);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Verify Biller Activate", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /****************************************************************************************
     * ##      B I L L E R    V A L I D A T I O N     R E L A T E D   M E T H O D S     ##
     * **************************************************************************************/

    /**
     * To activate Biller from Suspension
     *
     * @param biller
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public BillerManagement approveActivateBiller(Biller biller) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("activateBiller", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(billerActivationApprover);

            BillerActivation_Page page = new BillerActivation_Page(pNode);

            page.navBillerApprovalActivation();
            page.navBillerActivation();
            page.navBillerApprovalActivation();
            page.selectBillerToActivate(biller.BillerCode);
            page.clickApproveActivateButton();
            page.clickApproveButton();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("utility.merchant.activeApprove", "Verify that Biller is successfully activated", pNode, biller.BillerCode)) {
                    biller.setStatus(Constants.STATUS_ACTIVE);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Add Biller Validations
     * Multiple validations could be added using biller.ValidationList
     *
     * @param biller
     * @throws IOException
     */
    public BillerManagement addBillerValidation(Biller biller) throws IOException {
        Markup m = MarkupHelper.createLabel("addBillerValidation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            AddBillerValidation_pg1.init(pNode)
                    .navAddBillerValidation()
                    .selectBillerToAddValidation(biller.BillerCode)
                    .clickAddValidation()
                    .addBillerValidation(biller.ValidationList)
                    .clickOnConfirm();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("add.BillerVal.Successfully", "Successfully added Biller VAlidation", pNode)) {
                    biller.setIsCreated();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * This method is to add Biller Validations
     * It takes two arguments one is Biller Object and another one is BillValidation object
     *
     * @param biller
     * @param validation
     * @return
     * @throws IOException
     */
    public BillerManagement addBillerValidation(Biller biller, BillValidation validation) throws IOException {
        Markup m = MarkupHelper.createLabel("addBillerValidation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker


        try {
            AddBillerValidation_pg1 pg1 = AddBillerValidation_pg1.init(pNode);
            AddBillerValidation_pg2 pg2 = AddBillerValidation_pg2.init(pNode);

            Login.init(pNode).login(naBillerAddValidation);//TODO give specific role to activate

            pg1.navAddBillerValidation();
            pg1.selectBillerToAddValidation(biller.BillerCode);
            pg1.clickAddValidation();

            pg2.selectValidationType(validation.Type);
            pg2.setMinValue(validation.Min);
            pg2.setMaxValue(validation.Max);

            if (validation.StartWith != null)
                pg2.setStartsWith(validation.StartWith);
            if (validation.Contains != null)
                pg2.setContains(validation.Contains);

            pg2.selectRefType(validation.ReferenceType);

            pg2.clickOnSubmit();
            pg2.clickOnConfirm();


            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("add.BillerVal.Successfully", "Successfully added Biller VAlidation", pNode)) {
                    //biller.setIsCreated();
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * This method will modify Biller Validation
     *
     * @param biller
     * @param min
     * @param max
     * @return
     * @throws IOException
     */
    public BillerManagement modifyBillerValidation(Biller biller, String min, String max) throws IOException {

        Markup m = MarkupHelper.createLabel("modifyBillerValidation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker


        try {
            ModifyBillerValidation_Pg1 pg1 = new ModifyBillerValidation_Pg1(pNode);
            ModifyBillerValidation_Pg2 pg2 = new ModifyBillerValidation_Pg2(pNode);

            pg1.navModifyBillerValidation();
            pg1.selectBillerToModifyValidation(biller.BillerCode);
            pg1.clickModifyBtn();

            pg2.setMinValue(min);
            pg2.setMaxValue(max);
            pg2.clickSubmitBtn();
            pg2.clickConfirmBtn();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("modify.BillerVal.Successfully", "Successfully modified Biller VAlidation", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

//****************************************************************************************************************

    /**
     * Delete Biller Validation
     *
     * @param biller
     * @return -> Object of BillerManagement
     * @throws IOException
     */
    public BillerManagement deleteBillerValidation(Biller biller) throws IOException {
        Markup m = MarkupHelper.createLabel("addBillerValidation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            DeleteBillerValidation_Page page = new DeleteBillerValidation_Page(pNode);

            page.navDeleteBillerValidation();
            page.selectBillerToDeleteValidation(biller.BillerCode);
            page.clickDeleteValidationBtn();
            page.clickConfirmBtn();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("delete.BillerVal.Successfully", "Successfully Deleted Biller Validation", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Add Biller Notification
     *
     * @param notification
     * @return
     * @throws IOException
     */
    public BillerManagement addBillerNotification(BillNotification notification) throws IOException {
        Markup m = MarkupHelper.createLabel("addBillerNotification", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        Login.init(pNode).login(billerRegistor);//TODO give specific role to activate

        try {
            BillerNotificationManagement_Page1 page1 = new BillerNotificationManagement_Page1(pNode);
            BillerNotificationManagement_Page2 page2 = new BillerNotificationManagement_Page2(pNode);

            page1.navBillerNotificationManagement();
            page1.clickAddButton();


            page2.setNotificationName(notification.Name);
            if (notification.BillType.equalsIgnoreCase(Constants.BILL_NOTIFICATION_TYPE_GENERATE_DATE_AFTER))
                page2.clickGenDateRadio();
            if (notification.BillType.equalsIgnoreCase(Constants.BILL_NOTIFICATION_TYPE_DUE_DATE_BEFORE))
                page2.clickDueDateRadio();

            page2.setNotificationDays(notification.Days);
            page2.clickSubmitButton();
            page2.clickConfirmButton();


            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("notification.notification.added.successfully", "Successfully Added Notification", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param notification
     * @param status
     * @return
     * @throws IOException
     */
    public BillerManagement modifyBillerNotification(BillNotification notification, String status) throws IOException {
        Markup m = MarkupHelper.createLabel("addBillerNotification", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            BillerNotificationManagement_Page1 page1 = new BillerNotificationManagement_Page1(pNode);
            BillerNotificationManagement_Page2 page2 = new BillerNotificationManagement_Page2(pNode);

            page1.navBillerNotificationManagement();
            page1.selectNotificationToUpdate(notification.BillTypeValue);
            page1.clickUpdateButton();

            page2.updateNotificationName(notification.Name);
            page2.selectStatusByValue(status);
            page2.clickUpdateSubmitButton();
            page2.clickUpdateConfirmButton();


            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("notification.notification.updated.successfully", "Successfully Updated Notification", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public BillerManagement deleteBillerNotification(BillNotification notification, String status) throws IOException {
        Markup m = MarkupHelper.createLabel("addBillerNotification", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            BillerNotificationManagement_Page1 page1 = new BillerNotificationManagement_Page1(pNode);
            BillerNotificationManagement_Page2 page2 = new BillerNotificationManagement_Page2(pNode);

            page1.navBillerNotificationManagement();
            page1.selectNotificationToUpdate(notification.BillTypeValue);
            page1.clickUpdateButton();

            page2.selectStatusByValue(status);
            page2.clickUpdateSubmitButton();
            page2.clickUpdateConfirmButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("notification.notification.updated.successfully", "Successfully Deleted Notification", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public BillerManagement addBillerNotificationAssociation(BillNotification notification, String billerName) throws IOException {

        Markup m = MarkupHelper.createLabel("addBillerNotificationAssociation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        Login.init(pNode).login(billerNotificationAssociator);

        try {
            BillerNotificationAssociation_Page1 page1 = new BillerNotificationAssociation_Page1(pNode);
            BillerNotificationAssociation_Page2 page2 = new BillerNotificationAssociation_Page2(pNode);

            page1.navBillerNotificationAssociation();
            page1.clickAddButton();

            page2.selectBiller(billerName);
            page2.setNotificationName(notification.Name);
            page2.selectNotificationType(notification.BillTypeValue);
            page2.clickSubmitButton();
            page2.clickConfirmButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("notification.notificatio.label.notificationAdded", "Successfully Added Notification Association", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


//****************************************************************************************************************

    public BillerManagement updateBillerNotificationAssociation(BillNotification notification, String status) throws IOException {

        Markup m = MarkupHelper.createLabel("updateBillerNotificationAssociation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        Login.init(pNode).login(billerNotificationManagement);

        try {
            BillerNotificationAssociation_Page1 page1 = new BillerNotificationAssociation_Page1(pNode);
            BillerNotificationAssociation_Page2 page2 = new BillerNotificationAssociation_Page2(pNode);

            page1.navBillerNotificationAssociation();
            page1.selectNotificationToUpdate(notification.Name);
            page1.clickUpdateButton();

            if (status.equalsIgnoreCase(Constants.STATUS_ACTIVE))
                page2.setUpdatedNotificationName(notification.Name);

            page2.selectStatusByValue(status);
            page2.clickUpdateSubmitButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("notification.notificatio.label.notificationUpdated", "Successfully Modified/Deleted Notification Association", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public BillerManagement initiateSubscriberBillerAssociation(Biller biller) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateSubscriberBillerAssociation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        // Check if there is a bill that can be associated with any customer
        CustomerBill bill = biller.getNonAssociatedBill();
        if (bill == null) {
            pNode.warning("There are No Bills or All bills are already associated with any Customer...");
            return null;
        }
        try {
            SubsBillerAssociation_pg1.init(pNode)
                    .navAddBillerValidation()
                    .selectProvider(biller.ProviderName)
                    .setSubscriberMsisdn(bill.CustomerMsisdn) // customer for which the bill association has to be done
                    .clickOnSubmit()
                    .checkBiller(biller.BillerCode)
                    .setAccountNumber(biller.BillerCode, bill.BillAccNum);

            if (ConfigInput.isConfirm) {
                Utils.putThreadSleep(Constants.TWO_SECONDS);
                SubsBillerAssociation_pg1.init(pNode)
                        .clickOnSubmitConfirmAssociation();
            }

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("bulk.biller.association.success.message.UI", "Verify Bulk Biller association for acc num:" + bill.BillAccNum, pNode, bill.CustomerMsisdn)) {
                    // Set the Bill status as Associated
                    bill.setIsAssociated();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Initiate Bulk Biller Registration
     *
     * @param biller
     * @throws IOException
     */
    public BillerManagement initiateBulkBillerRegistration(Biller biller) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateBulkBillerRegistration", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            // Download the template and update the Bill details
            BulkBillerAssociation_pg1.init(pNode)
                    .navBillerRegistration()
                    .downloadBulkTemplate();

            // update the sheet
            ExcelUtil.writeDataToBulkBillerAssociationCsv(biller.BillList, FilePath.fileBulkBillerRegistration);

            // Upload the Updated files
            BulkBillerAssociation_pg1.init(pNode)
                    .uploadBulkFile(FilePath.fileBulkBillerRegistration);

            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(BillUpload_pg1.init(pNode).isUploadSuccessful(), true,
                        "verify BulkBiller Association file is uploaded", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Download the Log file post Bulk Biller Registration Action
     *
     * @throws Exception
     */
    public void downloadBulkRegistrationLogFile() throws Exception {
        Markup m = MarkupHelper.createLabel("downloadBulkRegistrationLogFile", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_BILLER_ASSOC_LOG); // is hardcoded can be Generic TODO
            BulkBillAssociation_pg2.init(pNode)
                    .downloadLogs();
            Utils.putThreadSleep(4000);
        } finally {
            Utils.closeUntitledWindows(); // close any untitled windows if open
        }
    }

    /**
     * Delete Subscriber Biller Association
     *
     * @param bill
     * @throws IOException
     */
    public void deleteSubsBillerAssociation(CustomerBill bill) throws IOException {
        Markup m = MarkupHelper.createLabel("deleteSubsBillerAssociation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(naDeleteBillerAssoc);
            DeleteSubsBillerAssociation_pg1.init(pNode)
                    .navDeleteSubsBillerAssociation()
                    .selectProvider(bill.ProviderId)
                    .setSubscriberMsisdn(bill.CustomerMsisdn)
                    .clickOnSubmit()
                    .checkBill(bill.BillAccNum)
                    .clickDeleteBiller();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("message.success.delete.billerass", "Verify Biller is successfully Deleted", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * This method is added for checking if Biller Category already Exists
     * This method return true if Found otherwise false
     *
     * @param catName : pass the category name to check exist or not
     * @return true if category found otherwise false
     * @throws Exception
     */
    public boolean checkBillerCategoryExist(String catName) throws Exception {
        Markup m = MarkupHelper.createLabel("checkBillerCategoryExist", ExtentColor.GREEN);
        pNode.info(m);
        try {
            BillerCategoryManagement_pg1 pg1 = new BillerCategoryManagement_pg1(pNode);

            pg1.navCreateBillerCategory();

            if (pg1.verifyRoleExists(catName) > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return false;
    }

    /**
     * This method is added for checking if Biller already Exists
     * You first have to Login with a valid user then call this method
     * Pass Biller name and Code to check if Biller Already exists
     * This method return boolean value ifFound then true otherwise false
     *
     * @param name
     * @param code
     * @return
     * @throws Exception
     */
    public boolean checkBillerExists(String name, String code) throws Exception {

        try {
            BillerRegistrationPage1 pg1 = new BillerRegistrationPage1(pNode);

            pg1.navBillerRegistration();

            if (driver.findElements(By.xpath("//tr/td[contains(text(),'" + MessageReader.getMessage("pseudo.emptyList.label", null) + "')]")).size() > 0) {
                pNode.info("No Category Exist");
                return false;
            }

            int rowCount = driver.findElements(By.xpath("//*[@id='addMerchant_input']/table/tbody/tr")).size();

            // iterate through all the cells
            for (int i = 2; i <= rowCount - 1; i++) {

                String billerName = driver.findElement(By.xpath("//*[@id='addMerchant_input']/table/tbody/tr[" + i + "]/td[1]")).getText();
                String billerCode = driver.findElement(By.xpath("//*[@id='addMerchant_input']/table/tbody/tr[" + i + "]/td[2]")).getText();

                if (billerName.trim().equals(name) && billerCode.trim().equals(code)) {
                    pNode.pass("Biller for Automation already exists with name: " + name);
                    return true;
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return false;
    }

    /**
     * Set the isAssert flag to false
     *
     * @return Current instance
     */
    public BillerManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m);
        ConfigInput.isAssert = false;
        return this;
    }

    public BillerManagement resumeNegativeTest() {
        Markup m = MarkupHelper.createLabel("Resume Negative Test", ExtentColor.BROWN);
        pNode.info(m);
        ConfigInput.isAssert = true;
        return this;
    }

    /**
     * Set the isAssert flag to false and isConfirm Flag to false
     *
     * @return Current instance
     */
    public BillerManagement startNegativeWithoutConfirmation() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m);
        ConfigInput.isAssert = false;
        ConfigInput.isConfirm = false;
        return this;
    }

    public BillerManagement initiateSubscriberBillerAssociationWithoutBill(Biller biller, CustomerBill bill1) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateSubscriberBillerAssociation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            // Check if there is a bill that can be associated with any customer
            CustomerBill bill = biller.getNonAssociatedBill();

            if (bill == null) {
                pNode.warning("There are No Bills or All bills are already associated with any Customer...");
                return null;
            }

            SubsBillerAssociation_pg1.init(pNode)
                    .navAddBillerValidation()
                    .selectProvider(biller.ProviderName)
                    .setSubscriberMsisdn(bill1.CustomerMsisdn) // customer for which the bill association has to be done
                    .clickOnSubmit()
                    .checkBiller(biller.BillerCode)
                    .setAccountNumber(biller.BillerCode, bill1.BillAccNum)
                    .clickOnSubmitConfirmAssociation();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("bulk.biller.association.success.message.UI", "Partial Action Message Validation", pNode, bill1.CustomerMsisdn)) {
                    // Set the Bill status as Associated
                    bill.setIsAssociated();
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public BillerManagement approveorRejectAutoDebitEnableByBiller(User usr, boolean ApprovalRequired, String... reason) throws Exception {
        Markup m = MarkupHelper.createLabel("addBillerCategory", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String Reason = reason.length > 0 ? reason[0] : "";
        try {
            AutoDebitEnableApproval_pg1 page = AutoDebitEnableApproval_pg1.init(pNode);

            page.navAutoDebitApproval();
            page.clickCheckbox(usr.MSISDN);
            if (ApprovalRequired) {
                page.clickApprove();
                AlertHandle.acceptAlert(pNode);
            } else {
                page.clickReject();
                page.selectRejectReason(Reason);
                page.clickReject();
                AlertHandle.acceptAlert(pNode);
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("enableautodebit.label.approve.confirm", "Auto Debit Enable Approved", pNode, usr.MSISDN);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * This method is added for enabling autoDebit
     *
     * @param msisdn
     * @return
     * @throws Exception
     */
    public String enableAutoDebit(String msisdn, String accno) throws IOException {
        Markup m = MarkupHelper.createLabel("EnableAutoDebit", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String txnid = null;
        try {
            AutoDebit_pg1 page1 = AutoDebit_pg1.init(pNode);
            page1.navEnableAutoDebit();
            page1.setCustomerMSISDN(msisdn);
            Thread.sleep(2000);
            String providerName = DataFactory.getDefaultProvider().ProviderName;
            page1.selectProvider(providerName);
            page1.selectAccountNumber(accno);
            page1.setMaxAmtForDebit("1000");
            page1.confirm();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = null;
                txnid = actual.split("Id ")[1].trim();
                Assertion.verifyActionMessageContain("autodebit.enable.successful.ui",
                        "Successfully enable Auto Debit", pNode, msisdn, txnid);

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return txnid;
    }

    /**
     * This method is added for disabling autoDebit
     *
     * @param msisdn
     * @return
     * @throws Exception
     */
    public BillerManagement disableAutoDebit(String msisdn, String billercode, String accno) throws IOException {
        Markup m = MarkupHelper.createLabel("EnableAutoDebit", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            AutoDebit_pg1 page1 = AutoDebit_pg1.init(pNode);
            page1.navDisableAutoDebit();
            page1.setCustomerMSISDNID(msisdn);
            Thread.sleep(3000);
            page1.selectbenificiary1(billercode);
            page1.selectAccountNumber(accno);
            page1.clickDisableconfirmBtn();
            if (ConfigInput.isAssert) {
                //verify autodebit disabled
                Assertion.assertActionMessageContain("Autodebit.disabled.successfully.ui", "Autodebit disabled successfully.ui", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public BillerManagement performBillPayment(String msisdn, String billercode) throws InterruptedException, IOException {
        Markup m = MarkupHelper.createLabel("initiateBillerRegistration", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {

            BillPay_pg1 page1 = BillPay_pg1.init(pNode);
            page1.navBillPayment();
            page1.selectProcessType(Constants.BILL_PROCESS_TYPE_OFFLINE);
            page1.selectServices(Services.AgentAssistedBillPayment);
            page1.selectBillerCode(billercode);
            page1.selectProvider(DataFactory.getDefaultProvider().ProviderName);
            page1.selectpaymentInstrument(Constants.BILLER_PAYMENT_EFFECTED_TO_WALLET);
            page1.selectWalletType(Constants.NORMAL_WALLET);
            page1.setBilleraccountNo("12345");
            page1.setBillPaymentAmt("10");
            page1.setMsisdn(msisdn);
            page1.submit_click();
            page1.confirm_click();

            if (ConfigInput.isAssert) {
                //verify autodebit disabled
                Assertion.verifyActionMessageContain("BillPay.billPayment.success.message", "Billpayment successfull", pNode, "10");
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * For Enabling Auto Debit From CCE Portal
     *
     * @param msisdn
     * @param bcode
     * @param accno
     * @return
     * @throws IOException
     */
    public String enableAutoDebitForChannelUser(String msisdn, String bcode, String accno) throws IOException {
        Markup m = MarkupHelper.createLabel("EnableAutoDebit", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String txnid = null;
        try {
            AutoDebit_pg1 page1 = AutoDebit_pg1.init(pNode);
            page1.navEnableAutoDebit();
            page1.setCustomerMSISDN(msisdn);
//            Thread.sleep(2000);
            String providerName = DataFactory.getDefaultProvider().ProviderName;
            page1.selectProvider(providerName);
            page1.selectbenificiary(bcode);
            page1.selectAccountNumber(accno);
            page1.setMaxAmtForDebit("1000");
            page1.confirm();

            // assertion
            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = null;
                txnid = actual.split("Id ")[1].trim();
                Assertion.verifyActionMessageContain("autodebit.enable.successful.ui",
                        "Successfully enable Auto Debit by Customer", pNode, msisdn, txnid);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return txnid;
    }

    public void closeResources() {
        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error while flushing/closing fileWriter !!!");
            e.printStackTrace();
        }
    }

    public String generateBillForUpload(Biller biller, String accNo, String... billStatus) throws ParseException, InterruptedException {
        String status = (billStatus.length > 0) ? billStatus[0] : "N";
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileUploads, "ChurnUserInitiation");

        String mfsProvider = GlobalData.defaultProvider.ProviderId;
        //getting future date
        String dt = new SimpleDateFormat("dd/MM/yyyy").format(new Date());  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(dt));
        c.add(Calendar.DATE, 15);  // number of days to add
        String billDueDate = sdf.format(c.getTime());  // dt is now the new date


        String date = new SimpleDateFormat("MMddyyyy").format(new Date());
        String time = new SimpleDateFormat("HHmmss").format(new Date());

        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileUploads, mfsProvider + "_" + biller.BillerCode);

        String filePath = "uploads/" + mfsProvider + "_" + biller.BillerCode + "_" + date + "_" + time;
        String fileName = filePath + ".csv";

        File f = new File(fileName);


        try {
            fileWriter = new FileWriter(f);

            fileWriter.append("Total Records*=1");
            fileWriter.append("\n");
            fileWriter.append("Total New Records*=1");
            fileWriter.append("\n");
            fileWriter.append("Total Updated Records*=0");
            fileWriter.append("\n");
            fileWriter.append("Total Deleted Records*=0");
            fileWriter.append("\n");
            fileWriter.append("Reserved Header Value");
            fileWriter.append("\n");
            fileWriter.append(newCommonFileHeaderOLD);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append(biller.BillerCode);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(accNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(accNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("test");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("test");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(accNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(billDueDate);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(billDueDate);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(status);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("test");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("test");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("test");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    /**
     * Bill Upload
     *
     * @param filename
     * @throws Exception
     */
    public BillerManagement bulkBillerAssociationFileUpload(String filename) throws Exception {
        Markup m = MarkupHelper.createLabel("bulkBillerAssociationFileUpload", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            BillUpload_pg1.init(pNode)
                    .navBillUpload()
                    .uploadBulkAssociationFile(filename)
                    .clickSubmit();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("manualUpload.message.success", "Verify Bill Upload Message", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;

    }

    /**
     * Bar
     *
     * @param biller
     * @param userType
     * @param barType
     * @return
     * @throws Exception
     */
    public String barBiller(Biller biller, String userType, String barType) throws Exception {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("BarUser: " + biller.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(optBarBiller);
            BarUser_pg1 pageOne = BarUser_pg1.init(pNode);

            pageOne.navigateNAToBarUserLink();

            /*
             * Provide the general Information
             */
            pageOne.selectUserTypeByValue(userType);
            pageOne.setLoginIDTbox(biller.LoginId);
            pageOne.barType(barType);
            pageOne.reasonForBar("BY_OPT");
            pageOne.remarksForBar("autoBar");
            pageOne.clickSubmit();
            pageOne.clickConfirm();

            //TODO - actual message Validation
            if (ConfigInput.isAssert) {
                actualMessage = pageOne.getMessage();
                if (Assertion.verifyActionMessageContain("user.bar.success", "Successfully Barred a biller:" + biller.LoginId, pNode)) {
                    biller.setStatus(barType);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return actualMessage;
    }

    /**
     * unBarChannelUser
     *
     * @param biller
     * @param userType
     * @param barType
     * @return
     * @throws Exception
     */
    public String unBarBiller(Biller biller, String userType, String barType) throws Exception {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("UnBarUser: " + biller.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(optBarBiller);
            UnBarUser_pg1 pageOne = new UnBarUser_pg1(pNode);

            // * Navigate to Add Channel User Page *
            pageOne.navigateNAToUnBarUserLink();

            pageOne.selectUserTypeByValue(userType);
            pageOne.setLoginID(biller.LoginId);
            pageOne.clickSubmit();
            pageOne.selectUserFromList();
            pageOne.clickOnUnbarSubmit();
            Thread.sleep(3000);
            pageOne.clickOnUnbarConfirm();


            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                actualMessage = pageOne.getMessage();
                pNode.info("Web Message:" + actualMessage);
                biller.setStatus(Constants.STATUS_ACTIVE); // todo, check for message
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return actualMessage;
    }

    public String barBiller(Biller biller, String barType) throws IOException {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("barChannelUser: " + biller.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            BarUser_pg1 pageOne = BarUser_pg1.init(pNode);

            pageOne.navigateNAToBarUserLink();
            pageOne.selectUserTypeByValue(Constants.USER_TYPE_BILLER);
            pageOne.setLoginIDTbox(biller.LoginId);
            pageOne.barType(barType);
            pageOne.remarksForBar("autoBar");
            pageOne.clickSubmit();
            pageOne.clickConfirm();
            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("user.bar.success", "Verify Successfully Barring a Biller User:" + biller.LoginId, pNode)) {
                    actualMessage = pageOne.getMessage();
                    biller.setStatus(barType);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return actualMessage;
    }


    /**
     * //todo Method is not complete
     *
     * @param custMsisdn
     * @param billId
     * @param accountNo
     * @return
     */
    public String generateBulkBillerAssociationFile(String custMsisdn, String billId, String accountNo) {
        String filePath = "uploads/" + "bulkBillerAssociation" + DateAndTime.getTimeStampDayAndTime();
        String fileName = filePath + ".csv";

        File f = new File(fileName);

        /*try {
            fileWriter = new FileWriter(f);

            fileWriter.append(bulkSubsAssociationHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(custMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(billId);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(accountNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Remarks Ok");

            fileWriter.append(NEW_LINE_SEPARATOR);

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }*/
        return f.getAbsolutePath();
    }

    public Biller getBarredBiller(String barAsBoth) {
        //TODO Prashant to implement  this method
        return null;
    }
}
