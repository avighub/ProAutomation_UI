package framework.features.transactionCorrection;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.common.collect.ImmutableMap;
import framework.entity.OperatorUser;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.pageObjects.transactionCorrection.TxnCorrectionApproval_page1;
import framework.pageObjects.transactionCorrection.TxnCorrectionInitiation_page1;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static framework.util.jigsaw.CommonOperations.setTxnConfig;

/**
 * Created by navin.pramanik on 9/3/2017.
 */
public class TransactionCorrection {

    private static WebDriver driver;
    private static FunctionLibrary fl;
    private static ExtentTest pNode;
    private static OperatorUser usrInitTxnCorrection, usrAppTxnCorrection;

    public static TransactionCorrection init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        if (usrInitTxnCorrection == null) {
            usrInitTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRECTION");
            usrAppTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRAPP");

            if (usrInitTxnCorrection.LoginId.equals(usrAppTxnCorrection.LoginId)) {
                //               pNode.fail("Both initiator and approver are same user, re check the ConfigInput.xlsx");
//                Assert.fail("refer reports for more info!");
            }
        }
        return new TransactionCorrection();
    }

    /**
     * @param txnID
     * @param revScharge
     * @param revComm
     * @return
     * @throws Exception
     */
    public String initAndApproveTxnCorrection(String txnID, boolean revScharge, boolean... revComm) throws Exception {
        Markup m = MarkupHelper.createLabel("initAndApproveTxnCorrection", ExtentColor.AMBER);
        pNode.info(m);
        String txnCorrID = null;
        try {

            txnCorrID = initiateTxnCorrection(txnID, revScharge, revComm);

            if (txnCorrID != null) {
                approveOrRejectTxnCorrection(txnCorrID, true);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return txnCorrID;
    }


    /**
     * @param txnID1
     * @param revScharge
     * @param revComm
     * @return
     * @throws IOException
     */
    public String initiateTxnCorrection(String txnID1, boolean revScharge, boolean... revComm) throws IOException {
        boolean revCommRequired = revComm.length > 0 ? revComm[0] : true;

        String txnID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateTxnCorrection", ExtentColor.AMBER);
            pNode.info(m);

            Login.init(pNode).login(usrInitTxnCorrection);

            TxnCorrectionInitiation_page1 page = TxnCorrectionInitiation_page1.init(pNode);

            page.navigateToTxnCorrection();
            page.enterTransactionID(txnID1);
            if (revScharge == true) {
                page.clickRevScharge();
            }
            if (revCommRequired) {
                page.clickRevCommission();
            }
            page.clickSubmit();
            page.setRemarks("Automation");
            page.clickSubmit();
            page.clickConfirm();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                txnID = actual.split(":")[1].trim();
                Assertion.verifyActionMessageContain("correction.label.transactionCorrectionInitiated", "Transaction Correction", pNode, txnID);
                pNode.info("Transaction ID :" + txnID);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID), Constants.TXN_STATUS_INITIATED,
                        "Transaction Status in DB verified successfully.", pNode);
                return txnID;
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return txnID;
    }

    public String initiateTxnCorrectionWithTpcApplicable(String txnID1,boolean revScharge,boolean revTCP,boolean... revComm) throws IOException {
        boolean revCommRequired = revComm.length > 0 ? revComm[0] : true;

        String txnID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateTxnCorrection", ExtentColor.AMBER);
            pNode.info(m);

            Login.init(pNode).login(usrInitTxnCorrection);

            TxnCorrectionInitiation_page1 page = TxnCorrectionInitiation_page1.init(pNode);

            page.navigateToTxnCorrection();
            page.enterTransactionID(txnID1);
            if (revScharge == true) {
                page.clickRevScharge();
            }
            if (revCommRequired) {
                page.clickRevCommission();
            }

            if (revTCP) {
                page.clickRevTCP();
            }
            page.clickSubmit();
            page.setRemarks("Automation");
            page.clickSubmit();
            page.clickConfirm();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                txnID = actual.split(":")[1].trim();
                Assertion.verifyActionMessageContain("correction.label.transactionCorrectionInitiated", "Transaction Correction", pNode, txnID);
                pNode.info("Transaction ID :" + txnID);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID), Constants.TXN_STATUS_INITIATED,
                        "Transaction Status in DB verified successfully.", pNode);
                return txnID;
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return txnID;
    }

    /**
     * @param tid
     * @return
     * @throws IOException
     */
    public TransactionCorrection approveOrRejectTxnCorrection(String tid, Boolean isApprove) throws IOException {
        try {

            Markup m = MarkupHelper.createLabel("approveOrRejectTxnCorrection", ExtentColor.AMBER);
            pNode.info(m);

            Login.init(pNode).login(usrAppTxnCorrection);

            TxnCorrectionApproval_page1 page1 = TxnCorrectionApproval_page1.init(pNode);

            page1.navigateToTxnCorrApproval();
            page1.selectTransactionId(tid);
            page1.clickSubmit();
            if (isApprove) {
                page1.clickApproveButton();
            } else {
                page1.clickRejectButton();
            }
            Thread.sleep(Constants.TWO_SECONDS);
            if (ConfigInput.isAssert) {

                if (isApprove) {
                    if (Assertion.verifyActionMessageContain("correction.label.transactionCorrectionapproval.txnid", "Transaction Correction Approval", pNode)) {
                        DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(tid), Constants.TXN_STATUS_SUCCESS,
                                "Transaction Status in DB verified successfully.", pNode);
                    }
                } else {
                    Assertion.verifyActionMessageContain("correction.label.transactionrejection.txnid", "Transaction Correction is Rejected", pNode);
                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * @param tid
     * @return
     * @throws IOException
     */
    public TransactionCorrection rejectTxnCorrection(String tid) throws IOException {
        try {

            Markup m = MarkupHelper.createLabel("rejectTxnCorrection", ExtentColor.AMBER);
            pNode.info(m);

            OperatorUser txnCorrUsr = DataFactory.getOperatorUserWithAccess("TXN_CORRAPP");

            Login.init(pNode).login(txnCorrUsr);

            TxnCorrectionApproval_page1 page1 = TxnCorrectionApproval_page1.init(pNode);

            page1.navigateToTxnCorrApproval();
            page1.selectTransactionId(tid);
            page1.clickSubmit();
            page1.clickRejectButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("correction.label.transactionrejection.txnid", "Transaction Rejected Successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    public TransactionCorrection startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }


}
