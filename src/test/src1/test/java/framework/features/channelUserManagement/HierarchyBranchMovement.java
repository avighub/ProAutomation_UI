package framework.features.channelUserManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.pageObjects.channelUserManagement.HierarchyBranchMovementPage;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

/**
 * Created by navin.pramanik on 12/12/2018.
 */
public class HierarchyBranchMovement {

    private static ExtentTest featureNode;
    private static WebDriver driver;
    private static FunctionLibrary fl;


    public static HierarchyBranchMovement init(ExtentTest test) throws IOException {
        featureNode = test;
        try {

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, test);
        }

        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return new HierarchyBranchMovement();
    }

    public HierarchyBranchMovement enterFirstPageDetailsOnHierarchyMovementPage(User userToMove) throws Exception {
        Markup m = MarkupHelper.createLabel("enterFirstPageDetailsOnHierarchyMovementPage: " + userToMove.LoginId, ExtentColor.BLUE);
        featureNode.info(m);

        HierarchyBranchMovementPage page = new HierarchyBranchMovementPage(featureNode);

        page.navHierarchyMovement();
        page.setMsisdnforBranchMovement(userToMove.MSISDN);
        page.submitforBranchMovement();
        return this;
    }

    /**
     * @param user
     * @param ownerUser
     * @param parentUser
     * @throws Exception
     */
    public HierarchyBranchMovement enterSecondPageDetailsOnHierarchyMovementPage(User user, User ownerUser, User parentUser,boolean... balanceCheck) throws Exception {
        boolean balanceCheckRequired = balanceCheck.length > 0 ? balanceCheck[0] : false;
        Markup m = MarkupHelper.createLabel("enterSecondPageDetailsOnHierarchyMovementPage: " + user.LoginId, ExtentColor.BLUE);
        featureNode.info(m);

        try {
            HierarchyBranchMovementPage page = new HierarchyBranchMovementPage(featureNode);
            page.navHierarchyMovement();
            page.setMsisdnforBranchMovement(user.MSISDN);
            page.submitforBranchMovement();
            page.selectOwnerNameforBranchMovement(ownerUser.getPartialName());
            page.selectParentCategory(parentUser.CategoryName);
            page.setParentName(parentUser.getFullName());
            page.submitBranchMovementPageOne();
            if(balanceCheckRequired){
                 page.clickonBalanceCheck();
            }

            if (ConfigInput.isConfirm) {
                page.clickonConfirm();
                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("hyrarchy.movement.success", "Branch Movement Successful", featureNode);
                }

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, featureNode);
        }
        return this;
    }

    /**
     * Initiate Hierarchy Branch Movement
     *
     * @param user
     * @param parentUser
     * @return
     * @throws Exception
     */
    public HierarchyBranchMovement initiateChUserHierarchyMovement(User user, User parentUser) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateChUserHierarchyMovement: " + user.LoginId, ExtentColor.BLUE);
            featureNode.info(m); // Method Start Marker

            HierarchyBranchMovementPage page = new HierarchyBranchMovementPage(featureNode);

            page.navHierarchyMovement();
            page.setMsisdnforBranchMovement(user.MSISDN);
            page.submitforBranchMovement();
            page.selectOwnerNameforBranchMovement(parentUser.getPartialName());
            page.selectParentCategory(parentUser.CategoryName);
            page.setParentName(parentUser.getFullName());
            page.submitBranchMovementPageOne();
            page.clickonConfirm();

            String error = Assertion.checkErrorPresent();
            if (error != null) {
                Assertion.verifyErrorMessageContain("mapping.already.exists", "Mapping Already Exists", featureNode);
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("hyrarchy.movement.success", "Branch Movement Successful", featureNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, featureNode);
        }
        return this;

    }


    public HierarchyBranchMovement initiateChUserHierarchyMovement(User user, User ownerUser, User parentUser) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateChUserHierarchyMovement: " + user.LoginId, ExtentColor.BLUE);
            featureNode.info(m); // Method Start Marker

            HierarchyBranchMovementPage page = new HierarchyBranchMovementPage(featureNode);

            page.navHierarchyMovement();
            page.setMsisdnforBranchMovement(user.MSISDN);
            page.submitforBranchMovement();
            page.selectOwnerNameforBranchMovement(ownerUser.getPartialName());
            page.selectParentCategory(parentUser.CategoryName);
            page.setParentName(parentUser.getFullName());
            page.submitBranchMovementPageOne();

            if (ConfigInput.isConfirm)
                page.clickonConfirm();

            String error = Assertion.checkErrorPresent();
            if (error != null) {
                Assertion.verifyErrorMessageContain("mapping.already.exists", "Mapping Already Exists", featureNode);
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("hyrarchy.movement.success", "Branch Movement Successful", featureNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, featureNode);
        }
        return this;

    }
}
