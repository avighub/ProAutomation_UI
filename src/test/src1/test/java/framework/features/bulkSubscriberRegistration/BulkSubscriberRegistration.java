package framework.features.bulkSubscriberRegistration;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import framework.entity.KinUser;
import framework.entity.User;
import framework.pageObjects.bulkSubscriberUpload.BulkSubscriberUpload_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


/**
 * This class contains the methods for the bulk Subscriber Regn
 *
 * @author
 */
public class BulkSubscriberRegistration {

    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static ExtentTest pNode;
    WebDriver driver;
    String csvFileHeader = "NamePrefix (Mr./Mrs./Ms/M/S)*,FirstName*,LastName*,MSISDN*," +
            "IdentificationNumber*,FormNumber,DateofBirth(dd/MM/yyyy)*,Gender (Male/Female)*," +
            "Address,District,City*,State,Country,Description,Preferred Language*,Web Group Role Id*," +
            "Login Id*,Type of Identity Proof,Type of Address Proof,Type of Photo Proof,Mobile Group Role Id*," +
            "Grade Code*,TCP Profile Id*,Primary Account (Y/N)*,Customer Id,Account Number,Account Type," +
            "Wallet Type/Linked Bank Status* (A/M/S/D),User Status*(A/M/N),MiddleName,Nationality," +
            "Id Type,IMT Id No,Issue Place,IssuedCountryCode,ResidencyCountryCode,IssuedDate(dd/MM/yyyy)," +
            "IsIDExpires(TRUE/FALSE),ExpireDate(dd/MM/yyyy),PostalCode,EmployerName,Kin First Name/" +
            "Kin Last Name,Kin Middle Name,Kin Relationship,Kin Nationality,Kin Contact Number," +
            "Kin Identification Number,Kin DOB,Email,IMT Enable(Y/N),IMT ID Type,Enable WU Services(Y/N)," +
            "Enable MoneyGram Services(Y/N),Birth Country,Passport Issue Country,Passport Issue City," +
            "Passport Issue Date,Occupation,Birth City";
    private FileWriter fileWriter = null;

    // Old File headers
    /*String csvFileHeader = "NamePrefix (Mr./Mrs./Ms/M/S)*,FirstName*,LastName*,MSISDN*,IdentificationNumber*,FormNumber,DateofBirth(dd/MM/yyyy)*," +
            "Gender (Male/Female)*,Address,District,City*,State,Country,Description,Preferred Language*,Web Group Role Id*,Login Id*,Type of Identity Proof," +
            "Type of Address Proof,Type of Photo Proof,Mobile Group Role Id*,Grade Code*,TCP Profile Id*,Primary Account (Y/N)*,Customer Id,Account Number,Account Type," +
            "Wallet Type/Linked Bank Status* (A/M/S/D),User Status*(A/M/N),MiddleName,Nationality,Id Type,Id No,PlaceOfIDIssued,IssuedCountryCode,ResidencyCountryCode," +
            "IssuedDate(dd/MM/yyyy),IsIDExpires(TRUE/FALSE),ExpireDate(dd/MM/yyyy),PostalCode,EmployerName";*/

    //New as per Econet
    /*String csvFileHeader = "NamePrefix (Mr./Mrs./Ms/M/S)*,FirstName*,LastName*,MSISDN*,IdentificationNumber*,FormNumber,DateofBirth(dd/MM/yyyy)*," +
            "Gender (Male/Female)*,Address,District,City*,State,Country,Description,Preferred Language*,Web Group Role Id*,Login Id*,Type of Identity Proof," +
            "Type of Address Proof,Type of Photo Proof,Mobile Group Role Id*,Grade Code*,TCP Profile Id*,Primary Account (Y/N)*,Customer Id,Account Number,Account Type," +
            "Wallet Type/Linked Bank Status* (A/M/S/D),User Status*(A/M/N),MiddleName,Nationality,Id Type,Id No,PlaceOfIDIssued,IssuedCountryCode,ResidencyCountryCode," +
            "IssuedDate(dd/MM/yyyy),IsIDExpires(TRUE/FALSE),ExpireDate(dd/MM/yyyy),PostalCode,EmployerName," +
            "Kin First Name,Kin Last Name,Kin Middle Name,Kin Relationship,Kin Nationality,Kin Contact Number,Kin Identification Number,Kin DOB,Email";
*/

    public static BulkSubscriberRegistration init(ExtentTest t1) {
        pNode = t1;
        return new BulkSubscriberRegistration();
    }

    public String initiateBulkSubscriberUpload(String fileName) throws IOException {
        String bulkID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateBulkSubscriberUpload", ExtentColor.TEAL);
            pNode.info(m);

            BulkSubscriberUpload_Page1 page = new BulkSubscriberUpload_Page1(pNode);

            page.navigateToBulkSubscriber();
            page.uploadFile(fileName);
            page.clickSubmit();

            if (ConfigInput.isAssert) {
                String errorStringPresent = Assertion.checkErrorPresent();
                if (errorStringPresent != null) {
                    pNode.fail("Error occurred :" + errorStringPresent);
                    Utils.captureScreen(pNode);
                    page.downloadErrorLogs();
                } else if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("bulkUpload.label.success", "Bulk Upload", pNode);
                    bulkID = Assertion.getActionMessage().split(":")[1].trim();
                    pNode.info("Bulk ID Generated: " + bulkID);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return bulkID;
    }


    public String generateBulkSubsRegnCsvFile(User subs, String status) {

        String filePath = "uploads/" + Constants.FILEPREFIX_BULK_SUBS_REGISTER + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        File f = new File(fileName);

        try {
            String tcpProfileID = MobiquityGUIQueries.fetchTCPId(subs.CategoryCode, subs.GradeCode);
            String mobGroupRoleName = MobiquityGUIQueries.fetchMobileRole(subs.CategoryCode, subs.GradeCode);
            String dob = new DateAndTime().getYear(-20);
            String gender = null;

            if (ConfigInput.is4o9Release)
                gender = "Homme";
            else
                gender = "Male";

            createUploadDirectory();


            fileWriter = new FileWriter(f);

            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append("Mr.");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(subs.FirstName);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(subs.LastName);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(subs.MSISDN);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(subs.ExternalCode);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(dob);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(gender);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Gurgaon");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("English");
            fileWriter.append(COMMA_DELIMITER);

            if (ConfigInput.isCoreRelease || ConfigInput.is4o14Release) {
                fileWriter.append(subs.WebGroupRole);
            } else {
                fileWriter.append(mobGroupRoleName);
            }
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(subs.LoginId);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mobGroupRoleName);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(subs.GradeCode);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(tcpProfileID);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Y");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.BLANK_CONSTANT);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(status);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(status);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append("\n");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            pNode.error("Error in CsvFileWriter !!!");
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }


    /**
     * To Generate Bulk Subs Regn and Modn File based on Input status
     *
     * @param noOfSubs     : Pass a list of subscriber
     * @param walletStatus
     * @param userStatus
     */
    public String generateBulkSubsRegnModnCsvFile(int noOfSubs, String walletStatus, String userStatus) throws Exception {

        String filePath = "uploads/" + Constants.FILEPREFIX_BULK_SUBS_REGISTER + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        File f = new File(fileName);
        String namePrefix = "Mr.";

        try {
            fileWriter = new FileWriter(f);

            //It will create the Uploads directory if not already exist
            createUploadDirectory();

            //Write CSV File headers
            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);

            for (int i = 0; i < noOfSubs; i++) {

                User subs = new User(Constants.SUBSCRIBER);
                subs.setKinUser(new KinUser());
                String tcpProfileID = MobiquityGUIQueries.fetchTCPId(subs.CategoryCode, subs.GradeCode);
                String mobGroupRoleName = MobiquityGUIQueries.fetchMobileRole(subs.CategoryCode, subs.GradeCode);

                String[] valuesToPutInCSV = {namePrefix, subs.FirstName, subs.LastName, subs.MSISDN, subs.ExternalCode, Constants.BLANK_CONSTANT, subs.DateOfBirth,
                        "Male", subs.Address, subs.District, subs.City, Constants.BLANK_CONSTANT
                        , Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, "English", subs.WebGroupRole,
                        subs.LoginId, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, mobGroupRoleName, subs.GradeCode, tcpProfileID,
                        "Y", Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, walletStatus, userStatus, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                        Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                        Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, subs.KinUser.FirstName, subs.KinUser.LastName, subs.KinUser.MiddleName,
                        subs.KinUser.RelationShip, "India", subs.KinUser.ContactNum, subs.KinUser.IdNo, subs.KinUser.DateOfBirth, Constants.BLANK_CONSTANT};

                for (String data : valuesToPutInCSV) {
                    fileWriter.append(data);
                    fileWriter.append(COMMA_DELIMITER);
                }

                fileWriter.append(NEW_LINE_SEPARATOR);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeResources();
        }

        return f.getAbsolutePath();
    }
    // ----------------------------------------------------------------------------------------------------------------------
    // ---------------------- Helper  FILE METHODS
    // ----------------------------------------------------------------------------------------------------------------------


    private void createUploadDirectory() {
        File directory = new File(FilePath.dirFileUploads);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }


    public void closeResources() {
        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error while flushing/closing fileWriter !!!");
            e.printStackTrace();
        }
    }


    public String modifyStausBulkSubsRegnCSV(String filename, String status) {
        File inputFile = new File(filename);
        try {

            // Read existing file
            CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
            List<String[]> csvBody = reader.readAll();
            // get CSV row column and replace with by using row and column
            csvBody.get(1)[27] = status; //Target replacement
            csvBody.get(1)[28] = status;
            reader.close();
            // Write to CSV file which is open
            CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',');
            writer.writeAll(csvBody);
            writer.flush();
            writer.close();
        } catch (Exception e) {

        }
        return inputFile.getAbsolutePath();
    }


    /**
     * This method is for generating Bulk Subscriber Registration/Modification CSV file
     * This method takes below arguments
     *
     * @param usrList      : Pass list of Users you want to put in CSV file
     * @param walletStatus : Pass Status of Wallet as "A" "M" or "D"
     * @param userStatus   :Pass Status of User as "A" -> Add ,"M" - > Modify or "D" -> Delete
     * @return return CSV file absolute Path
     * @throws Exception
     */
    public String generateBulkSubsRegnModnCsvFile(List<User> usrList, String walletStatus, String userStatus) throws Exception {

        String filePath = "uploads/" + Constants.FILEPREFIX_BULK_SUBS_REGISTER + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        File f = new File(fileName);
        String namePrefix = "Mr.";

        try {
            fileWriter = new FileWriter(f);

            //It will create the Uploads directory if not already exist
            createUploadDirectory();

            //Write CSV File headers
            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);

            for (User subs : usrList) {

                subs.setKinUser(new KinUser());
                String tcpProfileID = MobiquityGUIQueries.fetchTCPId(subs.CategoryCode, subs.GradeCode);
                String mobGroupRoleName = MobiquityGUIQueries.fetchMobileRole(subs.CategoryCode, subs.GradeCode);

                String[] valuesToPutInCSV = {namePrefix, subs.FirstName, subs.LastName, subs.MSISDN, subs.ExternalCode,
                        Constants.BLANK_CONSTANT, subs.DateOfBirth,
                        "Male", subs.Address, subs.District, subs.City, Constants.BLANK_CONSTANT
                        , Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, "English", subs.WebGroupRole,
                        subs.LoginId, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                        mobGroupRoleName, subs.GradeCode, tcpProfileID,
                        "Y", Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, walletStatus,
                        userStatus, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                        Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                        Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                        Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                        subs.KinUser.FirstName, subs.KinUser.LastName, subs.KinUser.MiddleName,
                        subs.KinUser.RelationShip, "India", subs.KinUser.ContactNum, subs.KinUser.IdNo,
                        subs.KinUser.DateOfBirth, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                        Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT};

                for (String data : valuesToPutInCSV) {
                    fileWriter.append(data);
                    fileWriter.append(COMMA_DELIMITER);
                }

                fileWriter.append(NEW_LINE_SEPARATOR);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeResources();
        }

        return f.getAbsolutePath();
    }

}
