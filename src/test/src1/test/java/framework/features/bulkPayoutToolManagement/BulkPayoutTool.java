package framework.features.bulkPayoutToolManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import framework.dataEntity.BulkCashInCsv;
import framework.dataEntity.BulkMerchantPaymentCSV;
import framework.dataEntity.BulkStockLiquidationCSV;
import framework.entity.Bank;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.pageObjects.bulkPayoutTool.BulkPayoutApprove_page1;
import framework.pageObjects.bulkPayoutTool.BulkPayoutDashboard_page1;
import framework.pageObjects.bulkPayoutTool.BulkPayoutInitiate_page1;
import framework.pageObjects.enterpriseManagement.BulkPaymentPage1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * This class contains the methods for the bulk reporting tools
 *
 * @author
 */
public class BulkPayoutTool {

    private static final String mfsProvider = DataFactory.getDefaultProvider().ProviderId;
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String walletNo = DataFactory.getDefaultWallet().WalletId;
    private static final String payInst = "WALLET";
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static OperatorUser optBulkInitiator, optBulkApprover;
    // File headers
    private String CommonFileHeaderOLD = "SrNo*,FromMFSProvider*,FromPaymentInstrument*,FromWalletNo*,FromMobileNumber/FromUserType*,ToMFSProvider*,ToPaymentInstrument*,ToWalletNo*,ToMobileNumber/ToUserType*,Amount*,IndividualRemarks";
    private String newCommonFileHeaderOLD = "Serial Number*,Sender SVA Type ID*,Sender Mobile Number*,Receiver SVA Type ID*,Receiver Mobile Number*,Amount*,Individual Remarks";
    private String newCommonFileHeaderCashIn = "Serial Number*,MFS Provider*,Sender SVA Type ID*,Sender Mobile Number*,Receiver SVA Type ID*,Receiver Mobile Number*,Amount*,Individual Remarks";
    private String newCommonFileHeaderC2C = "Serial Number*,MFS Provider*,Sender SVA Type ID*,Sender Mobile Number*,Receiver SVA Type ID*,Receiver Mobile Number*,Amount*,Individual Remarks";
    private String fileHeaderForBilPay = "Serial Number*,MFS Provider*,Sender SVA Type ID*,Sender Mobile Number*,biller code*,Amount*,Biller Account Number*,Individual Remarks";
    private String fileHeaderForReimbursement = "Serial Number*,External Reference Number*,External Reference Date*,User Identifier Type*,User Identifier Value*,User Role,MFS Provider*,Amount*,Product Id*,Remarks*";
    private String CommonFileHeaderO2Cnew = "Serial Number*,MFS Provider*,Receiver SVA Type ID*,Receiver Mobile Number*,Amount*,Transfer Date*,Payment Type*,Reference number*,Remarks*,Payment Number,Payment Date,Individual Remarks";
    private String Date = DataFactory.getCurrentDateSlash();
    private FileWriter fileWriter = null;
    private String amount = "50";
    private static OperatorUser netAdminInitiator, netAdminApprover_1, netAdminApprover_2;
    private Bank trustBank, nonTrustBank;

    public static BulkPayoutTool init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        if (optBulkInitiator == null) {
            optBulkInitiator = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            optBulkApprover = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE");
            netAdminInitiator = DataFactory.getOperatorUserWithAccess("BULK_INITIATE", Constants.NETWORK_ADMIN);
            netAdminApprover_1 = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE", Constants.NETWORK_ADMIN);
            netAdminApprover_2 = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE_2", Constants.NETWORK_ADMIN);
        }

        return new BulkPayoutTool();
    }

    /**
     * @param user
     * @param trustBank
     * @param liquidationBank
     * @param providerId
     * @param liquidationAmount
     * @throws IOException
     */
    public void initiateAndApproveStockLiquidationForChannelUser(User user,
                                                                 String trustBank,
                                                                 String liquidationBank,
                                                                 String providerId,
                                                                 String liquidationAmount) throws IOException {

        Markup m = MarkupHelper.createLabel("initiateBulkPayout", ExtentColor.BLUE);
        pNode.info(m);

        try {

            /*
            Initiate Stock Liquidation
            download the csv file from stock liquidation approval Page1
             */
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(pNode).stockLiquidationGeneration(user, batchId);

            Login.init(pNode).login(netAdminInitiator);
            String fileName = StockManagement.init(pNode)
                    .downloadStockLiquidationCSV(liquidationBank, providerId);

            /*
            Once the file is successfully downloaded, get the information only relevant to chUser
            Update the csv file
            Make sure that liquidation amount is a little less than available balance, as service charges are configured,
            initiation / approval can yield error
             */
            BulkStockLiquidationCSV entry1 = StockManagement.init(pNode).getStockLiquidationDetailsForUser(batchId, fileName);
            entry1.liquidationAmount = liquidationAmount;
            List<BulkStockLiquidationCSV> stockLiquidationList = new ArrayList<>();
            stockLiquidationList.add(entry1);

            String fileName1 = BulkPayoutTool.init(pNode)
                    .downloadAndUpdateBulkStockLiquidationCsv(stockLiquidationList);

            String batchId1 = BulkPayoutTool.init(pNode)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION, fileName1);


            if (batchId1 != null) {
                OperatorUser bankAdmApprove_L3 = DataFactory.getBankAdminWithAccessAndBank("NEW_BULK_APPROVE_3", trustBank);

                //Approve bulk payout for Stock Liquidation
                BulkPayoutTool.init(pNode)
                        .approveNewBulkPayoutForStockLiquidation(netAdminApprover_1,
                                batchId1, trustBank, true, 1);

                //Approve bulk payout for Stock Liquidation
                BulkPayoutTool.init(pNode)
                        .approveNewBulkPayoutForStockLiquidation(netAdminApprover_2, batchId1, trustBank, true, 2);

                //Approve bulk payout for Stock Liquidation
                BulkPayoutTool.init(pNode)
                        .approveNewBulkPayoutForStockLiquidation(bankAdmApprove_L3, batchId1, trustBank, true, 3);

                Login.init(pNode).login(netAdminInitiator);
                //Check Bulk in Dashboard
                BulkPayoutTool.init(pNode)
                        .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION, batchId1, true, true);

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public BulkPayoutTool initiateAndApproveBulkPayout(String service, String fileName) throws Exception {

        OperatorUser saBulkPay = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");

        Login.init(pNode).login(saBulkPay);

        String bulkID = initiateBulkPayout(service, fileName);

        if (bulkID != null) {
            OperatorUser saBulkPayApp = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE ");
            Login.init(pNode).login(saBulkPayApp);
            approveBulkPayout(bulkID);
        }

        return this;
    }


    public String initiateBulkPayout(String service, String fileName) throws IOException {
        String bulkID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateBulkPayout", ExtentColor.BLUE);
            pNode.info(m);

            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(pNode);

            page.navigateToLink();
            page.service_SelectValue(service);
            //page.fileUpload_SetText(fileName);
            page.fileUpload(fileName);
            page.remark_SetText("OK");
            page.submitButton_Click();

            if (ConfigInput.isAssert) {
                String msg = MessageReader.getMessage("bulkupload.success.batchNumber", null);
                msg = msg.split("\\{")[0].trim();
                String actual = Assertion.getActionMessage();
                Assertion.verifyContains(actual, msg, "Verify Bulk Success", pNode);
                bulkID = actual.split("BA")[1].trim().substring(0, 18);
                bulkID = "BA" + bulkID;
                pNode.info("Bulk ID Generated :" + bulkID);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return bulkID;
    }

    public void checkBulkPayoutDashboardforSelectedStatus(Boolean Status) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("verifyBulkPayoutDashboard", ExtentColor.BLUE);
            pNode.info(m);

            BulkPayoutDashboard_page1 page = BulkPayoutDashboard_page1.init(pNode);

            page.navigateToLink();
            page.selectStatusFilter(Status);
            page.sortNewest();
            page.clickOnEntry();


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public void approveBulkPayout(String id) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("approveBulkPayout", ExtentColor.BLUE);
            pNode.info(m);

            BulkPayoutApprove_page1 page = BulkPayoutApprove_page1.init(pNode);

            page.navigateToLink();
            page.selectTransactionID(id);
            page.approveButton_Click();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("bulkupload.approved.records.message", id);
                Assertion.verifyEqual(actual, expected, "Bulk Payout Approved Success", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }


    public String generateBulkO2CCsvFile(String FromMsisdn, String toMsisdn, Boolean... invalidfromMfsProvider) {

        String filePath = "uploads/" + "BulkPayOut" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        File f = new File(fileName);
        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(CommonFileHeaderOLD);
            fileWriter.append(NEW_LINE_SEPARATOR);

          /*  for(String key : msisdn){
                if(!(users.get(key).CategoryCode.contains("Enterprise"))){*/

            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);

            if (invalidfromMfsProvider.length > 0) {
                fileWriter.append("" + DataFactory.getRandomNumber(4));

            } else {
                fileWriter.append(mfsProvider);
            }
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(payInst);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(FromMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(payInst);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(toMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public String generateBulkCashinCsvFile(String FromMsisdn, String toMsisdn) {

        String filePath = "uploads/" + "BulkPayOut" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        File f = new File(fileName);


        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(newCommonFileHeaderCashIn);
            fileWriter.append(NEW_LINE_SEPARATOR);

          /*  for(String key : msisdn){

                if(!(users.get(key).CategoryCode.contains("Enterprise"))){*/

            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(FromMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(toMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public String generateBulkP2PCsvFile(String FromMsisdn, String toMsisdn, String amount) {

        String filePath = "uploads/" + "BulkPayOut" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        File f = new File(fileName);


        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(CommonFileHeaderOLD);
            fileWriter.append(NEW_LINE_SEPARATOR);

          /*  for(String key : msisdn){

                if(!(users.get(key).CategoryCode.contains("Enterprise"))){*/

            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(payInst);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(FromMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(payInst);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(toMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public String generateBulkC2CcsvFile(String FromMsisdn, String toMsisdn) {

        String filePath = "uploads/" + "BulkPayOut" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        File f = new File(fileName);

        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(newCommonFileHeaderC2C);
            fileWriter.append(NEW_LINE_SEPARATOR);

          /*  for(String key : msisdn){

                if(!(users.get(key).CategoryCode.contains("Enterprise"))){*/

            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(FromMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(toMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }


    // ----------------------------------------------------------------------------------------------------------------------
    // ---------------------- BPT FILE METHODS
    // ----------------------------------------------------------------------------------------------------------------------

    public String generateFile(String fileName, String data) throws FileNotFoundException {
        File f = new File("uploads/" + fileName);
        PrintWriter out = new PrintWriter(f);
        out.println(data);
        out.close();

        return f.getAbsolutePath();
    }

    /**
     * Genereate File with the provided data for batchPayout tool
     *
     * @param SrNo
     * @param FromMFSProvider
     * @param FromPaymentInstrument
     * @param FromWalletNo
     * @param FromMobileNumber
     * @param ToMFSProvider
     * @param ToPaymentInstrument
     * @param ToWalletNo
     * @param ToMobileNumber
     * @param Amount
     * @param IndividualRemarks
     * @return
     * @throws FileNotFoundException
     */
    public String generateBulkPayFile_OLD(String SrNo, String FromMFSProvider, String FromPaymentInstrument,
                                          String FromWalletNo, String FromMobileNumber, String ToMFSProvider, String ToPaymentInstrument,
                                          String ToWalletNo, String ToMobileNumber, String Amount, String IndividualRemarks)
            throws FileNotFoundException {
        String fileHeader = CommonFileHeaderOLD;


        String filedata2 = SrNo + "," + FromMFSProvider + "," + FromPaymentInstrument + "," + FromWalletNo + ","
                + FromMobileNumber + "," + ToMFSProvider + "," + ToPaymentInstrument + "," + ToWalletNo + ","
                + ToMobileNumber + "," + Amount + "," + IndividualRemarks;

        String data = fileHeader + "\r\n" + filedata2;
        String filename = generateFile(DataFactory.getTimeStamp() + ".csv", data);
        return filename;
    }


    /**
     * Return the last batch payout ID
     *
     * @return
     * @throws Exception
     */
    public String getLastBatchPayoutID() throws Exception {
        MobiquityGUIQueries dBHandler = new MobiquityGUIQueries();
        String id = dBHandler.dbGetLastBatchPayoutID();
        dBHandler.closeConnection();
        return id;
    }

    public void closeResources() {
        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error while flushing/closing fileWriter !!!");
            e.printStackTrace();
        }
    }

    /**
     * Initiate New Bulk Payout
     *
     * @param service
     * @param fileName
     * @return
     * @throws IOException Note - Network admin login is handled implicitly,
     *                     if superadmin login is required, it has to handled in the script before calling this method
     */
    public String initiateNewBulkPayout(String service, String fileName) throws IOException {
        String bulkID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateNewBulkPayout:" + service, ExtentColor.BLUE);
            pNode.info(m);

            Login.init(pNode).login(optBulkInitiator);
            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(pNode);

            page.navigateToNewLink();
            page.serviceSelectText(service);
            Thread.sleep(2000);
            if (page.fileUpload(fileName)) {
                page.enterRemark("OK");
                page.clickSubmitButton();
                Thread.sleep(5000);

                if (ConfigInput.isAssert) {
                    String actualMessage = page.getActionMessage();
                    if (actualMessage != null) {
                        if (Assertion.verifyMessageContain(actualMessage, "bulkupload.success.initiate", "Bulk Payout Initiate", pNode)) {
                            //  return actualMessage.split("batch ID:")[1].trim();
                            bulkID = actualMessage.split("batch ID:")[1].trim();
                        } else {
                            return null;
                        }

                    } else {
                        Utils.captureScreen(pNode);
                        pNode.fail("Failed to initiate Bulk Payout for Service:" + service);
                        Assert.fail("Failed to Initiate Bulk Payout, exiting the test");
                    }
                }
            } else {
                Assert.fail("Failed to Initiate Bulk Payout, exiting the test");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return bulkID;
    }

    public String initiateNewBulkPayoutNegative(String service, String fileName) throws IOException {
        String bulkID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateNewBulkPayoutNegative:" + service, ExtentColor.BLUE);
            pNode.info(m);

            String lastLogin = Login.init(pNode).getLastLogin();
            if (lastLogin == null || !lastLogin.toLowerCase().contains("superadmin")) {
                Login.init(pNode).login(optBulkInitiator);
            }
            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(pNode);

            page.navigateToNewLink();
            page.serviceSelectText(service);
            Thread.sleep(2000);
            page.fileUpload(fileName);
            page.enterRemark("OK");
            page.clickSubmitButton();
            Thread.sleep(5000);

            if (ConfigInput.isAssert) {
                String actualMessage = page.getActionMessage();
                if (actualMessage != null) {
                    if (Assertion.verifyMessageContain(actualMessage, "bulkupload.success.initiate", "Bulk Payout Initiate", pNode)) {
                        return actualMessage.split("batch ID:")[1].trim();
                    } else {
                        return null;
                    }

                } else {
                    Utils.captureScreen(pNode);
                    pNode.fail("Failed to initiate Bulk Payout for Service:" + service);
                    Assert.fail("Failed to Initiate Bulk Payout, exiting the test");
                }
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return bulkID;
    }

    /**
     * Download Bulk CashIn Csv and Update the Csv File with Bulk Data
     *
     * @param entryList
     * @return
     * @throws IOException
     */
    public String downloadAndUpdateBulkCashInCsv(List<BulkCashInCsv> entryList) throws IOException {

        try {
            Login.init(pNode).login(optBulkInitiator);
            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(pNode);

            page.navigateToNewLink();
            page.serviceSelectText(Constants.BULK_PAYOUT_SERVICE_CASHIN);

            if (page.downloadTemplateBulkCSV(Constants.FILEPREFIX_BULK_CASH_IN)) {
                BufferedWriter out = new BufferedWriter(new FileWriter(FilePath.fileBulkCashInTemplate, true));
                for (BulkCashInCsv row : entryList) {
                    out.newLine();
                    out.write(row.serialNum + "," +
                            row.providerId + "," +
                            row.senderSvaTypeId + "," +
                            row.senderMsisdn + "," +
                            row.receiverSvaTypeId + "," +
                            row.receiverMsisdn + "," +
                            row.amount + "," +
                            row.remark);
                }

                out.newLine();
                out.close();
                return FilePath.fileBulkCashInTemplate;
            } else {
                Assert.fail("Failed to download Template file");
                Assertion.markAsFailure("Failed to download Template file");
                return null;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public String downloadAndUpdateBulkMerchantPayCsv(List<BulkMerchantPaymentCSV> mercPayList) throws IOException {

        try {
            Login.init(pNode).login(optBulkInitiator);
            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(pNode);

            page.navigateToNewLink();
            page.serviceSelectText(Constants.BULK_PAYOUT_SERVICE_MER_PAY);

            if (page.downloadTemplateBulkCSV(Constants.FILEPREFIX_BULK_MERC_PAY)) {
                BufferedWriter out = new BufferedWriter(new FileWriter(FilePath.fileBulkMerchPayTemplate, true));
                for (BulkMerchantPaymentCSV row : mercPayList) {
                    out.newLine();
                    out.write(row.serialNum + "," +
                            row.provider + "," +
                            row.senderPayId + "," +
                            row.senderMsisdn + "," +
                            row.receiverPayId + "," +
                            row.receiverMsisdn + "," +
                            row.amount + "," +
                            row.remark);
                }

                out.newLine();
                out.close();
                return FilePath.fileBulkMerchPayTemplate;
            } else {
                Assert.fail("Failed to download Template file");
                Assertion.markAsFailure("Failed to download Template file");
                return null;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return null;
    }

    public String downloadAndUpdateBulkStockLiquidationCsv(List<BulkStockLiquidationCSV> entryList) throws IOException {

        try {
            Login.init(pNode).login(optBulkInitiator);
            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(pNode);

            page.navigateToNewLink();
            page.serviceSelectText(Constants.BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION);

            if (page.downloadTemplateBulkCSV(Constants.FILEPREFIX_BULK_STOCK_LIQUIDATION)) {
                BufferedWriter out = new BufferedWriter(new FileWriter(FilePath.fileBulkStockLiquidationTemplate, true));
                for (BulkStockLiquidationCSV row : entryList) {
                    out.newLine();
                    out.write(row.serialNum + "," +
                            row.liquidationTxnId + "," +
                            row.batchId + "," +
                            row.transactionDate + "," +
                            row.transactionTime + "," +
                            row.usrCategoryCode + "," +
                            row.usrName + "," +
                            row.usrMsisdn + "," +
                            row.usrCode + "," +
                            row.bankAccountNum + "," +
                            row.bankBranchName + "," +
                            row.providerId + "," +
                            row.liquidationAmount + "," +
                            row.productId + "," +
                            row.isApproved + "," +
                            row.remark);
                }

                out.newLine();
                out.close();
                return FilePath.dirFileDownloads + Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            } else {
                Assertion.markTestAsFailure("Failed to download Template file", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public String generateBulkBilPaycsvFile(String FromMsisdn, String billCode, String billAccountNumber) {

        String filePath = "uploads/" + "BulkPayOut" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        File f = new File(fileName);

        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(fileHeaderForBilPay);
            fileWriter.append(NEW_LINE_SEPARATOR);

          /*  for(String key : msisdn){

                if(!(users.get(key).CategoryCode.contains("Enterprise"))){*/

            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(FromMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(billCode);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(billAccountNumber);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public String generateNewBulkCashinCsvFile(String FromMsisdn, String toMsisdn) throws InterruptedException {

        pNode.info("Deleting existing file with prefix - NewBulkPayOut");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "NewBulkPayOut"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);


        String filePath = "uploads/" + "NewBulkPayOut" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        File f = new File(fileName);

        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(newCommonFileHeaderCashIn);
            fileWriter.append(NEW_LINE_SEPARATOR);

          /*  for(String key : msisdn){

                if(!(users.get(key).CategoryCode.contains("Enterprise"))){*/

            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(FromMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(toMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public void approveRejectNewBulkPayout(String service, String batchId, boolean isApprove) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("approveRejectNewBulkPayout", ExtentColor.BLUE);
            pNode.info(m);
            //login with different user to approve
            Login.init(pNode).login(optBulkApprover);

            BulkPayoutApprove_page1 page = BulkPayoutApprove_page1.init(pNode);
            page.navigateNewToLink();
            page.selectService(service);
            page.clickSortNewest();

            // get all the requests and check for the bacthc Id
            List<WebElement> pendingReqs = driver.findElements(By.cssSelector(".pending-policy-name"));

            boolean isFound = false;
            for (WebElement elem : pendingReqs) {
                elem.findElement(By.xpath("parent::*")).click();
                Thread.sleep(1000);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    isFound = true;
                    // approve the entry
                    if (isApprove) {
                        page.clickApprove();
                        Thread.sleep(2000);
                    } else {
                        page.clickReject();
                    }

                    break;
                }
            }

            if (ConfigInput.isAssert && isFound) {
                String actual = page.getActionMessage();

                if (isApprove) {
                    Assertion.verifyMessageContain(actual, "bulkupload.success.approve", "Bulk Payout Approval", pNode);
                } else {
                    Assertion.verifyMessageContain(actual, "bulkupload.reject.approve", "Bulk Payout request has been rejected successfully", pNode, batchId);
                }
            }
            if (!isFound) {
                pNode.fail("Could not find the request for Bulk Batch Id:" + batchId);
                Utils.captureScreen(pNode);
                Assert.fail("Failed to approve Bulk Payout Request with id:" + batchId);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Approve Bulk Payout for Stock Liquidation
     *
     * @param approver
     * @param batchId
     * @param bankName  - Bank Name is required when approving Stock Liquidation
     * @param isApprove
     * @throws IOException
     */
    public void approveNewBulkPayoutForStockLiquidation(OperatorUser approver, String batchId, String bankName, boolean isApprove, int approveLevel) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("approveNewBulkPayoutForStockLiquidation", ExtentColor.TEAL);
            pNode.info(m);

            Login.init(pNode).login(approver);

            BulkPayoutApprove_page1 page = BulkPayoutApprove_page1.init(pNode);
            if (approveLevel == 1) {
                page.navigateNewToLink();
            } else if (approveLevel == 2) {
                page.navApproveLevelTwo();
            } else if (approveLevel == 3) {
                page.navApproveLevelThree();
            } else {
                Assertion.markAsFailure("Please select correct approval level");
            }

            // get all the requests and check for the batch Id
            List<WebElement> pendingReqs = driver.findElements(By.cssSelector(".pending-policy-name"));

            boolean isFound = false;
            for (WebElement elem : pendingReqs) {
                elem.findElement(By.xpath("parent::*")).click();
                Thread.sleep(1000);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    isFound = true;
                    Utils.captureScreen(pNode);
                    // approve the entry
                    if (isApprove && approveLevel == 1) {
                        page.selectBank(bankName);
                        page.clickApprove();
                    }else if(isApprove){
                        page.clickApprove();
                    }else {
                        page.clickReject();
                    }

                    break;
                }
            }

            if (ConfigInput.isAssert && isFound) {
                String actual = page.getActionMessage();

                if (isApprove) {
                    Assertion.verifyMessageContain(actual, "bulkupload.success.approve", "Bulk Payout Approval", pNode);
                } else {
                    Assertion.verifyMessageContain(actual, "bulkupload.reject.approve", "Bulk Payout request has been rejected successfully", pNode, batchId);
                }
            }
            if (!isFound) {
                pNode.fail("Could not find the request for Bulk Batch Id:" + batchId);
                Utils.captureScreen(pNode);
                Assert.fail("Failed to approve Bulk Payout Request with id:" + batchId);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }


    public String verifyBulkPayoutDashboard(String serviceName, String batchId, boolean isApproved, boolean expectSuccess) throws IOException {
        String strApproveReject = (expectSuccess) ? "Approved" : "Rejected";
        ArrayList<String> errors = new ArrayList<String>();
        String fileName = null;
        try {
            Markup m = MarkupHelper.createLabel("verifyBulkPayoutDashboard", ExtentColor.BLUE);
            pNode.info(m);

            BulkPayoutDashboard_page1 page = BulkPayoutDashboard_page1.init(pNode);

            page.navigateToLink();
            page.sortNewest();
            Thread.sleep(2000);
            page.selectServiceType(serviceName);
            Thread.sleep(2000);
            page.selectStatusFilter(isApproved);

            // get all the requests and check for the bacthc Id
            List<WebElement> avaibaleEntry = driver.findElements(By.cssSelector(".lh.collapse_btnPD1"));

            boolean isFound = false;
            for (WebElement elem : avaibaleEntry) {
                elem.findElement(By.xpath("parent::*")).click();
                Thread.sleep(1000);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    isFound = true;
                    // approve the entry
                    // todo, verify for the Success 1 and Failure 0  and vice versa fro Failure
                    break;
                }
            }


            if (isFound) {
                pNode.pass("Successfully found the entry for Bulk Batch Id:" + batchId + ". Approval status: " + strApproveReject);
                fileName = page.downloadStatusFile();
                Thread.sleep(1000);
                checkStatusFile(expectSuccess);


            } else {
                Assertion.markAsFailure("Failed to find entry for Bulk Batch Id:" + batchId + ". Approval status: " + strApproveReject);
            }
            Utils.captureScreen(pNode);


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return fileName;
    }

    public String generateBulkCashOutCsvFile(String FromMsisdn, String toMsisdn) {

        String filePath = "uploads/" + "BULK_CASHOUT-template" /*+ DataFactory.getTimeStamp()*/;
        String fileName = filePath + ".csv";

        File f = new File(fileName);


        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(newCommonFileHeaderOLD);
            fileWriter.append(NEW_LINE_SEPARATOR);

          /*  for(String key : msisdn){

                if(!(users.get(key).CategoryCode.contains("Enterprise"))){*/

            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(FromMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(toMsisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public String generateBulkRechargeSelfCsvFile(String senderMSISDN) {
        String filePath = "uploads/" + "BULK_RECHARGE-template" /*+ DataFactory.getTimeStamp()*/;
        String fileName = filePath + ".csv";

        File f = new File(fileName);

        String header = "Serial Number*,MFS Provider*,Sender SVA Type ID*,Sender Mobile Number*,Receiver Operator Id*,Amount*,Individual Remarks";

        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(header);
            fileWriter.append(NEW_LINE_SEPARATOR);
            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(walletNo);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(senderMSISDN);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public String generateBulkOperatorWithdraw(String fromMSISDN, String productID) {
        String filePath = "uploads/" + "BulkPayoutTool" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        String extRefNum = "" + DataFactory.getRandomNumber(3);
        File f = new File(fileName);


        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(fileHeaderForReimbursement);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(extRefNum);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Date);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("mobileNumber");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(fromMSISDN);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Channel");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(DataFactory.getDefaultProvider().ProviderId);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(amount);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(productID);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Automation");
            fileWriter.append("\n");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }


    public String checkLog(int j, String idvalue, String fileName) throws IOException {
        String errors = null;
        try {

            BufferedReader br = null;
            ArrayList<String> list = new ArrayList<String>();
            try {
                //Reading the csv file
                br = new BufferedReader(new FileReader(fileName));

                String line = "";

                //Read to skip the header
                br.readLine();

                //Reading from the second line
                while ((line = br.readLine()) != null) {
                    list.add(line);
                }
                //for (int i = 0; i < errors.size(); i++) {
                String[] details = list.get(j).split(",");

                if (details.length > 0) {
                    if (list.get(j).contains("SUCCEEDED")) {
                        pNode.info(idvalue + " , " + details[details.length - 1]);
                    } else {
                        pNode.info(idvalue + " , " + details[details.length - 3] + " , " + details[details.length - 3]);
                        errors = (details[details.length - 3].trim().replace("\"", " ").trim());
                    }
                }
                //}

            } catch (Exception ee) {
                ee.printStackTrace();
            } finally {
                try {
                    br.close();
                } catch (IOException ie) {
                    System.out.println("Error occured while closing the BufferedReader");
                    ie.printStackTrace();
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return errors;
    }



    /*public void downloadStatusFile() throws InterruptedException {
        pNode.info("Deleting existing file with prefix - bulk-upload");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
        Thread.sleep(5000L);
        BulkPayoutDashboard_page1.init(pNode).getstatusFile();
        pNode.info("Click On downLoad status link");
        pNode.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000L);

    }*/


    /**
     * check status in commission disbursement dashboard
     *
     * @throws Exception
     */
    public void checkStatusFile(boolean status) throws Exception {
        Markup m = MarkupHelper.createLabel("checkStatusFile", ExtentColor.BLUE);
        pNode.info(m);
        String matchStr = (status) ? "SUCCEEDED" : "FAILED";

        try {

           /* FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
            BulkPayoutDashboard_page1.init(pNode)
                    .getstatusFile();*/


            String status_File = FilePath.dirFileDownloads + "/bulk-upload-" + BulkPayoutDashboard_page1.init(pNode).getbatchID() + ".csv";

            File file = new File(status_File);
            Scanner s = new Scanner(file);
            int lineIndex = 1;
            boolean success = true;
            String[] matcherArray = {matchStr};

            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (lineIndex == 2) {

                    if (line.contains(matchStr)) {
                        pNode.info("CSV Text: " + line);
                        pNode.pass("found text as " + matchStr);
                    } else {
                        pNode.info("CSV Text: " + line);
                        pNode.fail("Transaction Failed: Text to Contain :" + matchStr);
                        break;
                    }
                    //now we don't want to read further
                    break;
                }
                lineIndex++;
            }
            s.close();
        } catch (Exception e) {
            System.out.println(e);
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }


    public void downloadTemplateFile(String service) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("StockLiquidationTemplateFile", ExtentColor.BLUE);
            pNode.info(m);

            pNode.info("Deleting existing file with prefix - BULK_STKLIQREQ");
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BULK_STKLIQREQ"); // is hardcoded can be Generic TODO
            Thread.sleep(5000);
            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(pNode);
            page.navigateToNewLink();
            page.serviceSelectText(service);
            page.fileDownload();

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    public BulkPayoutTool startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m); // Method Start Marker
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public void updateDetailsInCSVFile(String fileToUpdate, String userBalance, String updatedBalance) throws InterruptedException, IOException {

        File inputFile = new File(fileToUpdate);

        // Read existing file
        CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
        List<String[]> csvBody = reader.readAll();
        // get CSV row column and replace with by using row and column
        for (int i = 1; i < csvBody.size(); i++) {
            String[] strArray = csvBody.get(i);
            for (int j = 0; j < strArray.length; j++) {
                if (strArray[j].equalsIgnoreCase(userBalance)) { //String to be replaced
                    csvBody.get(i)[j] = updatedBalance; //Target replacement
                }
            }
        }
        reader.close();

        // Write to CSV file which is open
        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',', CSVWriter.NO_QUOTE_CHARACTER);
        writer.writeAll(csvBody);
        writer.flush();
        writer.close();
    }

    public String generateFileForReimbursement(String usercode, User subs, Biller biller, String operatorId, User enterprise) throws IOException, InterruptedException {

        pNode.info("Deleting existing file with prefix - BULK_OPTW-template");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BULK_OPTW-template"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        File f = new File(FilePath.dirFileDownloads + "BULK_OPTW-template.csv");

        CSVWriter csvWriter = new CSVWriter(new FileWriter(f), ',', CSVWriter.NO_QUOTE_CHARACTER);
        csvWriter.writeNext(new String[]{"Serial Number*", "External Reference Number*", "External Reference Date*", "User Identifier Type*", "User Identifier Value*", "User Role", "MFS Provider*", "Amount*", "Product Id*", "Remarks*"});
        csvWriter.writeNext(new String[]{"1", DataFactory.getRandomNumberAsString(4) + DataFactory.getRandomString(3), DataFactory.getCurrentDateSlash(), "userCode", usercode, "Channel", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"2", DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(4), DataFactory.getCurrentDateSlash(), "mobileNumber", subs.MSISDN, "CUSTOMER", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"3", DataFactory.getRandomNumberAsString(5) + DataFactory.getRandomString(2), DataFactory.getCurrentDateSlash(), "billerCode", biller.BillerCode, "", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"4", DataFactory.getRandomNumberAsString(2) + DataFactory.getRandomString(4), DataFactory.getCurrentDateSlash(), "operatorId", operatorId, "", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"5", DataFactory.getRandomNumberAsString(6) + DataFactory.getRandomString(1), DataFactory.getCurrentDateSlash(), "loginId", enterprise.LoginId, "Channel", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"6", DataFactory.getRandomNumberAsString(1) + DataFactory.getRandomString(5), DataFactory.getCurrentDateSlash(), "systemId", "IND03", "Channel", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});

        csvWriter.close();
        return f.getAbsolutePath();
    }

    public String generateErrorFileForReimbursement(String usercode, User subs, Biller biller, String operatorId, User enterprise, String RCoperatorBalance) throws IOException, InterruptedException {

        pNode.info("Deleting existing file with prefix - BULK_OPTW-template");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BULK_OPTW-template"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        File f = new File(FilePath.dirFileDownloads + "BULK_OPTW-template.csv");

        CSVWriter csvWriter = new CSVWriter(new FileWriter(f), ',', CSVWriter.NO_QUOTE_CHARACTER);
        csvWriter.writeNext(new String[]{"Serial Number*", "External Reference Number*", "External Reference Date*", "User Identifier Type*", "User Identifier Value*", "User Role", "MFS Provider*", "Amount*", "Product Id*", "Remarks*"});
        csvWriter.writeNext(new String[]{"1", DataFactory.getRandomNumberAsString(4) + DataFactory.getRandomString(3), DataFactory.getCurrentDateSlash(), "userCode", usercode, "CHANNEL", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"2", DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(4), DataFactory.getCurrentDateSlash(), "mobileNumber", subs.MSISDN, "CHANNEL", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"3", DataFactory.getRandomNumberAsString(5) + DataFactory.getRandomString(2), DataFactory.getCurrentDateSlash(), "billerCode", biller.LoginId, "", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"4", DataFactory.getRandomNumberAsString(2) + DataFactory.getRandomString(4), DataFactory.getCurrentDateSlash(), "operatorId", operatorId, "", DataFactory.getDefaultProvider().ProviderId, RCoperatorBalance, "12", "test"});
        csvWriter.writeNext(new String[]{"5", DataFactory.getRandomNumberAsString(6) + DataFactory.getRandomString(1), DataFactory.getCurrentDateSlash(), "loginId", enterprise.LoginId, "CHANNEL", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});
        csvWriter.writeNext(new String[]{"6", DataFactory.getRandomNumberAsString(1) + DataFactory.getRandomString(5), DataFactory.getCurrentDateSlash(), "systemId", "IND03B", "CHANNEL", DataFactory.getDefaultProvider().ProviderId, "2", "12", "test"});

        csvWriter.close();
        return f.getAbsolutePath();
    }

    public String generateFileForO2C(String providerId, String productId, User usr, String amt) throws IOException, InterruptedException {

        pNode.info("Deleting existing file with prefix - BULK_O2C-template");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BULK_O2C-template"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        File f = new File(FilePath.dirFileDownloads + "BULK_O2C-template.csv");

        CSVWriter csvWriter = new CSVWriter(new FileWriter(f), ',', CSVWriter.NO_QUOTE_CHARACTER);
        csvWriter.writeNext(new String[]{"Serial Number*", "MFS Provider*", "Receiver SVA Type ID*", "Receiver Mobile Number*", "Amount*", "Transfer Date*", "Payment Type*", "Reference number*", "Remarks*", "Payment Number", "Payment Date", "Individual Remarks"});
        csvWriter.writeNext(new String[]{"1", providerId, productId, usr.MSISDN, amt, DataFactory.getCurrentDateSlash(), "Cash", DataFactory.getRandomString(2) + DataFactory.getRandomNumberAsString(3), "test", "", "", ""});
        csvWriter.writeNext(new String[]{"2", providerId, productId, "8856458758", amt, DataFactory.getCurrentDateSlash(), "Cheque", DataFactory.getRandomString(3) + DataFactory.getRandomNumberAsString(3), "test", "", "", ""});
        csvWriter.close();
        return f.getAbsolutePath();
    }

    public String generateErrorFileForO2C(String providerId, String productId, User usr, String amt) throws IOException, InterruptedException {

        pNode.info("Deleting existing file with prefix - BULK_O2C-template");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BULK_O2C-template"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        File f = new File(FilePath.dirFileDownloads + "BULK_O2C-template.csv");

        CSVWriter csvWriter = new CSVWriter(new FileWriter(f), ',', CSVWriter.NO_QUOTE_CHARACTER);
        csvWriter.writeNext(new String[]{"Serial Number*", "MFS Provider*", "Receiver SVA Type ID*", "Receiver Mobile Number*", "Amount*", "Transfer Date*", "Payment Type*", "Reference number*", "Remarks*", "Payment Number", "Payment Date", "Individual Remarks"});
        csvWriter.writeNext(new String[]{"1", providerId, "", usr.MSISDN, amt, DataFactory.getCurrentDateSlash(), "Demand Draft", DataFactory.getRandomString(3) + DataFactory.getRandomNumberAsString(2), "test", "", "", ""});
        csvWriter.writeNext(new String[]{"2", providerId, productId, usr.MSISDN, amt, DataFactory.getCurrentDateSlash(), "mMoney", DataFactory.getRandomString(2) + DataFactory.getRandomNumberAsString(2), "test", "", "", ""});

        csvWriter.close();
        return f.getAbsolutePath();
    }

    public String generateFileForReversal(ArrayList<String> txnIDs, String isServiceChargeReversible, String isCommissionReversible, String isTCPReversible) throws IOException, InterruptedException {

        pNode.info("Deleting existing file with prefix - BULK_TXNCORRECT-template");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BULK_TXNCORRECT-template"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        File f = new File(FilePath.dirFileDownloads + "BULK_TXNCORRECT-template.csv");

        CSVWriter csvWriter = new CSVWriter(new FileWriter(f));
        csvWriter.writeNext(new String[]{"Transaction Id*", "Reverse Service Charge and Taxes*", "Reverse Commission and Taxes*", "Reverse TCP limits for Original txn*"});
        csvWriter.writeNext(new String[]{txnIDs.get(0), isServiceChargeReversible, isCommissionReversible, isTCPReversible});
        csvWriter.writeNext(new String[]{txnIDs.get(1), isServiceChargeReversible, isCommissionReversible, isTCPReversible});
        csvWriter.writeNext(new String[]{txnIDs.get(2), isServiceChargeReversible, isCommissionReversible, isTCPReversible});
        csvWriter.writeNext(new String[]{txnIDs.get(3), isServiceChargeReversible, isCommissionReversible, isTCPReversible});
        csvWriter.writeNext(new String[]{txnIDs.get(4), isServiceChargeReversible, isCommissionReversible, isTCPReversible});
        csvWriter.close();

        return f.getAbsolutePath();
    }

    public void verifyErrorLogs(String... messageCode) throws Exception {

        if (messageCode.length == 0) {
            Assertion.markAsFailure("Expected message is required");
            Assert.fail("Expected message is required");
        }
        try {
            BulkPaymentPage1 page = new BulkPaymentPage1(pNode);
            String actualMsg = page.getErrorString();

            Assertion.verifyContains(actualMsg, "File processing has failed",
                    "Verify Error Message From UI", pNode);

            String filePath = page.downloadErrorLogFile();
            if (filePath != null) {

                for (String msgCode : messageCode) {
                    String expectedMsg = MessageReader.getMessage(msgCode, null);
                    Utils.checkLogFileSpecificMessage(filePath, expectedMsg, pNode);
                }

            } else {
                Assertion.markAsFailure("Failed to download error logs");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


}



