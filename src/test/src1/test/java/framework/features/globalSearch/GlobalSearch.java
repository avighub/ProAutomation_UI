package framework.features.globalSearch;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.pageObjects.globalSearch.GlobalSearch_page1;
import framework.pageObjects.globalSearch.GlobalSearch_page2;
import framework.util.common.Assertion;
import framework.util.globalConstant.Button;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;

import java.io.IOException;

/**
 * Created by naveen.karkra on 24/11/2017.
 */
public class GlobalSearch {

    private static ExtentTest pNode;

    public static GlobalSearch init(ExtentTest t1) throws Exception {
        pNode = t1;
        return new GlobalSearch();
    }


    public GlobalSearch globalSearch(String searchType, Object user, String... searchValue) throws IOException {

        String searchData = searchValue.length > 0 ? searchValue[0] : null;

        String loginId = null, firstName = null, lastName = null, msisdn = null;
        if (user instanceof User) {
            loginId = ((User) user).LoginId;
            firstName = ((User) user).FirstName;
            lastName = ((User) user).LastName;
            msisdn = ((User) user).MSISDN;
        } else if (user instanceof OperatorUser) {
            loginId = ((OperatorUser) user).LoginId;
            firstName = ((OperatorUser) user).FirstName;
            lastName = ((OperatorUser) user).LastName;
            msisdn = ((OperatorUser) user).MSISDN;
        }

        try {
            Markup m = MarkupHelper.createLabel("initiateGlobalSearch: using searchType " + searchType + " for searchValue " + searchValue + "", ExtentColor.ORANGE);
            pNode.info(m);

            GlobalSearch_page1 page1 = new GlobalSearch_page1(pNode);

            page1.navigateToLink();


            switch (searchType) {
                case "LOGINID":
                    page1.setLoginID(loginId);
                    break;
                case "MSISDN":
                    page1.setmsisdn(msisdn);
                    break;
                case "FIRSTNAME":
                    page1.setFirstName(firstName);
                    break;
                case "LASTNAME":
                    page1.setLastName(lastName);
                    break;
                case "DATEOFCREATION":
                    page1.setCreatedOn(searchData);
                    break;
                case "DATEOFMODIFICATION":
                    page1.setModifiedOn(searchData);
                    break;
                case "LASTTRANSACTION":
                    page1.setLastTransaction(searchData);
                    break;
                case "LASTLOGIN":
                    page1.setLastLogin(searchData);
                    break;
                default:
                    pNode.warning("Please provide valid search Type!");

            }
            page1.clickSubmit();

            if (ConfigInput.isConfirm) {
                GlobalSearch_page2 page2 = new GlobalSearch_page2(pNode);
                page2.selectRadioButton(msisdn);
                page2.clickFinalSubmit();
            }

            if (!ConfigInput.isAssert)
                return this;

            if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LOGINID))
                Assertion.verifyEqual(page1.getLoginIDText(), searchValue, "Verifying Login ID in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_MSISDN))
                Assertion.verifyEqual(page1.getMSISDNText(), searchValue, "Verifying MSISDN in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_FIRSTNAME))
                Assertion.verifyEqual(page1.getFirstName(), searchValue, "Verifying First Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LASTNAME))
                Assertion.verifyEqual(page1.getLastName(), searchValue, "Verifying Last Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_DATE_OF_CREATION))
                Assertion.verifyEqual(page1.getFirstName(), firstName, "Verifying First Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_DATE_OF_MODIFICATION))
                Assertion.verifyEqual(page1.getFirstName(), firstName, "Verifying First Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LAST_TRANSACTION))
                Assertion.verifyEqual(page1.getFirstName(), firstName, "Verifying First Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LAST_LOGIN))
                Assertion.verifyEqual(page1.getFirstName(), firstName, "Verifying First Name in results", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public GlobalSearch globalSearch(String searchType, String value) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("globalSearch: using searchType " + searchType, ExtentColor.ORANGE);
            pNode.info(m);

            GlobalSearch_page1 page1 = new GlobalSearch_page1(pNode);

            page1.navigateToLink();

            if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LOGINID))
                page1.setLoginID(value);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_MSISDN))
                page1.setmsisdn(value);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_FIRSTNAME))
                page1.setFirstName(value);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LASTNAME))
                page1.setLastName(value);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_DATE_OF_CREATION))
                page1.setCreatedOn(value);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_DATE_OF_MODIFICATION))
                page1.setModifiedOn(value);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LAST_TRANSACTION))
                page1.setLastTransaction(value);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LAST_LOGIN))
                page1.setLastLogin(value);

            page1.clickSubmit();

            if (ConfigInput.isConfirm) {
                GlobalSearch_page2 page2 = new GlobalSearch_page2(pNode);
                page2.selectRadioButton(value);
                page2.clickFinalSubmit();
            }

            if (!ConfigInput.isAssert)
                return this;

            if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LOGINID))
                Assertion.verifyEqual(page1.getLoginIDText(), value, "Verifying Login ID in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_MSISDN))
                Assertion.verifyEqual(page1.getMSISDNText(), value, "Verifying MSISDN in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_FIRSTNAME))
                Assertion.verifyEqual(page1.getFirstName(), value, "Verifying First Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LASTNAME))
                Assertion.verifyEqual(page1.getLastName(), value, "Verifying Last Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_DATE_OF_CREATION))
                Assertion.verifyEqual(page1.getFirstName(), value, "Verifying First Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_DATE_OF_MODIFICATION))
                Assertion.verifyEqual(page1.getFirstName(), value, "Verifying First Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LAST_TRANSACTION))
                Assertion.verifyEqual(page1.getFirstName(), value, "Verifying First Name in results", pNode);
            else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LAST_LOGIN))
                Assertion.verifyEqual(page1.getFirstName(), value, "Verifying First Name in results", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * @param button
     * @param msisdn
     * @return
     * @throws Exception
     */
    public GlobalSearch globalSearchButtonTest(String button, String... msisdn) throws Exception {

        String MSISDN = msisdn.length > 0 ? msisdn[0] : "";

        GlobalSearch_page1 page1 = new GlobalSearch_page1(pNode);
        GlobalSearch_page2 page2 = new GlobalSearch_page2(pNode);
        page1.navigateToLink();
        page1.setmsisdn(MSISDN);

        switch (button) {
            case Button.RESET:
                page1.getMSISDN();
                page1.clickResetButton();
                Assertion.verifyEqual(page1.getMSISDN(), Constants.BLANK_CONSTANT, "Verify Reset button Functionality.", pNode);
                break;
            case Button.BACK:
                page1.clickSubmit();
                page2.selectRadioButton(MSISDN);
                page2.clickFinalSubmit();
                page2.clickResultPageBackButton();
                page2.isSubmit2ButtonDisplayed();
                break;
            case Button.SUBMIT:
                page1.clickSubmit();
                break;
            default:
                break;
        }

        return this;
    }


}
