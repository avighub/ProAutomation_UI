package framework.features.kpi;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.kpi.KPI_Pg1;
import framework.pageObjects.kpi.KPI_pg2;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.NumberConstants;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

/**
 * Created by navin.pramanik on 9/11/2017.
 */
public class KPIManagement {

    private static WebDriver driver;
    private static ExtentTest pNode;

    public static KPIManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new KPIManagement();
    }

    public KPIManagement performKPIUsingCurrentDate(String providerID) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("performKPIUsingCurrentDate", ExtentColor.ORANGE);
            pNode.info(m);

            KPI_Pg1 kpiPage1 = KPI_Pg1.init(pNode);

            kpiPage1.navigateToLink();
            kpiPage1.selectProviderByValue(providerID);
            kpiPage1.selectCurrentDateRadio();
            Thread.sleep(NumberConstants.SLEEP_2000);
            kpiPage1.clickSubmit();

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    public KPIManagement performKPIUsingPreviousDate(String providerID) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("performKPIUsingPreviousDate", ExtentColor.ORANGE);
            pNode.info(m);

            KPI_Pg1 kpiPage1 = KPI_Pg1.init(pNode);

            kpiPage1.navigateToLink();
            kpiPage1.selectProviderByValue(providerID);
            kpiPage1.selectPreviousDateRadio();
            kpiPage1.clickSubmit();

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    public KPIManagement performKPIUsingDateRange(String providerID, String fromDate, String toDate) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("performKPIUsingDateRange", ExtentColor.ORANGE);
            pNode.info(m);

            KPI_Pg1 kpiPage1 = KPI_Pg1.init(pNode);

            kpiPage1.navigateToLink();
            kpiPage1.selectProviderByValue(providerID);
            kpiPage1.selectDateRangeRadio();
            Thread.sleep(Constants.TWO_SECONDS);
            kpiPage1.setFromDate(fromDate);
            kpiPage1.setToDate(toDate);
            kpiPage1.clickSubmit();

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public double getKpiValueFromUI(String kpiId) throws Exception {
        Markup m = MarkupHelper.createLabel("getKpiValueFromUI", ExtentColor.ORANGE);
        pNode.info(m);
        KPI_pg2 page = new KPI_pg2(pNode);
        Thread.sleep(Constants.WAIT_TIME);
        return Double.parseDouble(page.getKPIResult(MessageReader.getKpiMessage(kpiId, null)));
    }
}
