/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Autoamtion team
 *  Date: 9/12/2017
 *  Purpose: Feature of stockManagement
 */

package framework.features.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import framework.dataEntity.BulkStockLiquidationCSV;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.RechargeOperator;
import framework.entity.User;
import framework.features.common.Login;
import framework.pageObjects.Promotion.Reconciliation;
import framework.pageObjects.stockManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public class StockManagement {
    private static ExtentTest pNode;
    private static OperatorUser optNetStockLimiter, optEAStockLimiter, optInitNetworkStock, optAppNetworkStockL1, optAppNetworkStockL2,
            optInitEAStock, optAppEAStockL1, optAppEAStockL2, optInitIMT, optAppIMTL1, stockReimUser;

    public static StockManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        if (optInitNetworkStock == null) {
            // Network Stock
            optNetStockLimiter = DataFactory.getOperatorUserWithAccess("STOCK_LIMIT", Constants.NETWORK_ADMIN);
            optInitNetworkStock = DataFactory.getOperatorUserWithAccess("STOCK_INIT");
            optAppNetworkStockL1 = DataFactory.getOperatorUserWithAccess("STOCK_APP1");
            optAppNetworkStockL2 = DataFactory.getOperatorUserWithAccess("STOCK_APP2");

            // EA Stock
            optEAStockLimiter = DataFactory.getOperatorUserWithAccess("STOCKTR_LIMIT", Constants.NETWORK_ADMIN);
            optInitEAStock = DataFactory.getOperatorUserWithAccess("STR_INIT");
            optAppEAStockL1 = DataFactory.getOperatorUserWithAccess("STOCKTR_APP1");
            optAppEAStockL2 = DataFactory.getOperatorUserWithAccess("STOCKTR_APP2");

            //IMT
            optInitIMT = DataFactory.getOperatorUserWithAccess("STK_IMT_INI");
            optAppIMTL1 = DataFactory.getOperatorUserWithAccess("STK_IMT_APPROVAL1");
            stockReimUser = DataFactory.getOperatorUserWithAccess("STOCK_REINIT", Constants.NETWORK_ADMIN);
        }
        return new StockManagement();
    }

    /**
     * Get Stock Liquidation details
     *
     * @param - stock liquidation Transaction Id
     * @return
     */
    public static BulkStockLiquidationCSV getStockLiquidationDetailsForUser(String batchId, String fileName) {
        try {
            FileReader filereader = new FileReader(fileName);

            // create csvReader object and skip first Line
            CSVReader csvReader = new CSVReaderBuilder(filereader)
                    .withSkipLines(1)
                    .build();
            List<String[]> allData = csvReader.readAll();

            // print Data
            for (String[] row : allData) {
                if (row[2].equalsIgnoreCase(batchId)) {
                    return new BulkStockLiquidationCSV(row[0],
                            row[1],
                            row[2],
                            row[3],
                            row[4],
                            row[5],
                            row[6],
                            row[7],
                            row[8],
                            row[9],
                            row[10],
                            row[11],
                            row[12],
                            row[13],
                            row[14],
                            ""); // keep the remark as empty
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertion.markTestAsFailure("Could not fine entry in Downloaded CSV file for bulk liquidation Batch id:" + batchId, pNode);
        return null;
    }

    /**
     * Adding stock limit
     *
     * @param provider
     * @param limit
     * @return previous set limit, if already set
     * @throws Exception
     */
    public BigDecimal addNetworkStockLimit(String provider, String limit) throws Exception {
        BigDecimal previousLimit = null;
        Markup m = MarkupHelper.createLabel("addNetworkStockLimit", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode).login(optNetStockLimiter);

            StockLimit_Page1 page1 = StockLimit_Page1.init(pNode);
            page1.navigateToStockLimitPage();
            page1.selectProviderName(provider);
            previousLimit = page1.getStockLimitUI();
            page1.setStockLimit(limit);
            page1.clickSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.limit.updated", "Update Network Stock Limit", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return previousLimit;
    }

    /**
     * Add Stock Limit EA
     *
     * @param provider - Provider Name
     * @param limit    - Limit
     * @return
     * @throws Exception
     */
    public String addEAStockLimit(String provider, String limit) throws Exception {
        String prevEALimit = null;
        Markup m = MarkupHelper.createLabel("addEAStockLimit", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode).login(optEAStockLimiter);

            StockLimitEA_Page1 page1 = StockLimitEA_Page1.init(pNode);
            page1.navigateToStockLimitPage();
            page1.selectProviderName(provider);
            prevEALimit = page1.getStockLimitFromUI();
            page1.setStockLimit(limit);
            page1.clickSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stocktr.limit.updated", "Update EA Stock Limit", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return prevEALimit;
    }

    /**
     * Initiate stock to IND01
     *
     * @param provider
     * @param bankname
     * @param amount
     * @return
     * @throws Exception
     */
    public String initiateAndApproveNetworkStock(String provider, String bankname, String amount) throws Exception {
        Login.init(pNode).login(optInitNetworkStock);
        String txnId = initiateNetworkStock(provider, bankname, amount);

        if (txnId != null) {
            Login.init(pNode).login(optAppNetworkStockL1);
            approveNetworkStockL1(txnId);
            DBAssertion.requestedAmount = MobiquityDBAssertionQueries.getRequestedAmount(txnId);
            DBAssertion.verifyDBAssertionEqual(DBAssertion.requestedAmount.toString(), amount, "Verify DB Amount", pNode);
        }

        return txnId;
    }

    /**
     * Initiate Network Stock Transfer
     *
     * @param provider
     * @param bankName
     * @param amount
     * @return
     * @throws Exception
     */
    public String initiateNetworkStock(String provider, String bankName, String amount) throws Exception {

        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateNetworkStock", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockInitiation_Page1 page1 = StockInitiation_Page1.init(pNode);

            page1.navigateToStockInitiationPage();
            page1.selectProviderName(provider);
            page1.selectBankName(bankName);
            page1.setReferenceNumber("12345");
            page1.setStockAmount(amount);
            page1.clickSubmit();
            if (ConfigInput.isConfirm) {
                page1.clickConfirm();
                if (ConfigInput.isAssert) {
                    // Initiate Approval
                    String msg = Assertion.getActionMessage();
                    if (ConfigInput.isCoreRelease) {
                        txnId = msg.split("ID: ")[1].split(" ")[0];
                    } else {
                        txnId = msg.split("ID :")[1].split(" ")[0];
                    }

                    Assertion.verifyActionMessageContain("stock.initate.success", "Initiate Network Stock", pNode, txnId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    /**
     * Initiate Network Stock Transfer
     *
     * @param provider
     * @param bankName
     * @param amount
     * @param refNumber (Optional Parameter fo reference number)
     * @return
     * @throws Exception
     */
    public String initiateNetworkStock(String provider, String bankName, String amount, String refNumber, String... remark) throws Exception {
        String remarks = (remark.length) > 0 ? remark[0] : "Automation";
        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateNetworkStock", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockInitiation_Page1 page1 = StockInitiation_Page1.init(pNode);

            page1.navigateToStockInitiationPage();
            page1.selectProviderName(provider);
            page1.selectBankName(bankName);
            page1.setReferenceNumber(refNumber);
            page1.setStockAmount(amount);
            page1.setRemarks(remarks);
            page1.clickSubmit();


            if (ConfigInput.isAssert) {
                page1.clickConfirm();
                // Initiate Approval
                String msg = Assertion.getActionMessage();
                txnId = msg.split("ID: ")[1].split(" ")[0];
                Assertion.verifyActionMessageContain("stock.initate.success", "Initiate Network Stock", pNode, txnId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    public void initStockTransferWithInvalidDetails(String provider, String bankname, String amount, String referenceNumber) throws Exception {

        OperatorUser stockInitiator = DataFactory.getOperatorUsersWithAccess("STOCK_INIT").get(0);


        Login.init(pNode).login(stockInitiator);
        try {
            Markup m = MarkupHelper.createLabel("initStockTransfer", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockInitiation_Page1 page1 = StockInitiation_Page1.init(pNode);

            page1.navigateToStockInitiationPage();
            page1.selectProviderName(provider);
            page1.selectBankName(bankname);
            page1.setReferenceNumber(referenceNumber);
            page1.setStockAmount(amount);
            page1.clickSubmit();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Approving stock initiation @ Level-1
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement approveNetworkStockL1(String txnId, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;

        try {
            Markup m = MarkupHelper.createLabel("approveNetworkStockL1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(optAppNetworkStockL1);

            StockApproval_Page1 page1 = StockApproval_Page1.init(pNode);
            page1.navigateToStockApproval1Page();
            page1.selectRadioButton(txnId);
            page1.clickSubmit();

            if (remark != null)
                page1.enterRemarks(remark);

            page1.verifyAmount();

            if (ConfigInput.isConfirm)
                page1.clickApprove();

            if (ConfigInput.isAssert) {
                String actualMessage = Assertion.getActionMessage();
                if (actualMessage.contains(MessageReader.getMessage("stock.approval.secondLevelNeeded", null))) {
                    pNode.pass("Successfully Approved Level 1, Level 2 Approval Is Required!");
                    Assertion.verifyActionMessageContain("stock.approval.secondLevelNeeded", "Second Level Approval Needed", pNode, txnId);
                    approveNetworkStockL2(txnId);
                } else {
                    Assertion.verifyActionMessageContain("stock.approve.success", "Approve Network Stock", pNode, txnId);
                    DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                            Constants.TXN_STATUS_SUCCESS, "Verify DB Status", pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Network Stock Approval L1 Detail Verification
     *
     * @param txnId
     * @param ref
     * @return
     * @throws Exception
     */
    public StockManagement verifyNetworkStockApproveL1Page(String txnId, String ref) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("verifyNetworkStockApproveL1Page", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockApproval_Page1 page = StockApproval_Page1.init(pNode);
            page.navigateToStockApproval1Page();
            page.selectRadioButton(txnId);

            Assertion.verifyEqual(page.getTransactionID(txnId), txnId, "Assert Transcation ID", pNode);
            //Assertion.verifyEqual(page.getDate(txnId), CMDExecutor.getServerDate(), "Assert Date", pNode);
            Assertion.verifyEqual(page.getReferenceNumber(txnId), ref, "Assert ReferenceNumber", pNode);
            Assertion.verifyEqual(page.getCurrentStatus(txnId), Constants.Stock_Statuslevel1, "Assert Current Status", pNode);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * reject Network Stock Level 1
     *
     * @param txnId
     * @return
     * @throws Exception *
     */
    public StockManagement rejectNetworkStockL1(String txnId) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectNetworkStockL1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(optAppNetworkStockL1);

            StockApproval_Page1 page1 = StockApproval_Page1.init(pNode);

            page1.navigateToStockApproval1Page();
            page1.selectRadioButton(txnId);
            page1.clickSubmit();
            page1.clickOnReject();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.reject.success", "Stock Rejection Check", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Approving stock @ Level-2
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement approveNetworkStockL2(String txnId, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;
        try {
            Markup m = MarkupHelper.createLabel("approveNetworkStockL2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(optAppNetworkStockL2);

            StockApproval_Page2 page1 = new StockApproval_Page2(pNode);
            page1.navigateToStockApproval2Page();
            page1.selectRadioButton(txnId);
            page1.clickSubmit();
            if (remark != null)
                page1.enterRemarks(remark);

            page1.VerifyAmount();

            if (ConfigInput.isConfirm)
                page1.clickApprove();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.approve.success", "Approve Network Stock", pNode, txnId);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                        Constants.TXN_STATUS_SUCCESS, "Verify DB Status", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * verify Network Stock Approval Page Level 2
     *
     * @param txnId
     * @param ref
     * @return
     * @throws Exception
     */
    public StockManagement verifyNetworkStockApproveL2Page(String txnId, String ref) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("approveNetworkStockL2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(optAppNetworkStockL2);

            StockApproval_Page2 page = new StockApproval_Page2(pNode);
            page.navigateToStockApproval2Page();
            page.selectRadioButton(txnId);

            Assertion.verifyEqual(page.getTransactionID(txnId), txnId, "Assert Transcation ID", pNode);
            //Assertion.verifyEqual(page.getDate(txnId), CMDExecutor.getServerDate(), "Assert Date", pNode);
            Assertion.verifyEqual(page.getReferenceNumber(txnId), ref, "Assert ReferenceNumber", pNode);
            Assertion.verifyEqual(page.getCurrentStatus(txnId), Constants.Stock_Statuslevel2, "Assert Current Status", pNode);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Reject Network Stock Level2
     *
     * @param txnId
     * @return
     * @throws Exception *
     */
    public StockManagement rejectNetworkStockL2(String txnId) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectNetworkStockL2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(optAppNetworkStockL2);

            StockApproval_Page2 page1 = new StockApproval_Page2(pNode);
            page1.navigateToStockApproval2Page();
            page1.selectRadioButton(txnId);
            page1.clickSubmit();
            page1.clickReject();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.reject.success", "Verify Stock Rejection", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * initaite the EA stock
     *
     * @param amount
     * @return
     * @throws Exception
     */
    public String initiateApproveEAStock(String refNum, String amount, String remarks, boolean... isapprove) throws Exception {
        Boolean approve = (isapprove.length) > 0 ? isapprove[0] : true;
        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateApproveEAStock", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            txnId = initiateEAStock(refNum, amount, remarks);

            if (txnId != null && approve) {
                approveEAStockL1(txnId);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    /**
     * Initiate EA Stock Transfer
     *
     * @param refNum
     * @param amount
     * @param remarks
     * @return
     * @throws Exception
     */
    public String initiateEAStock(String refNum, String amount, String remarks) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateEAStock", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            StockTransferInitiate_pg1 page = StockTransferInitiate_pg1.init(pNode);
            page.navStockTransferInitiatePage();
            page.refNo_SetText(refNum);
            page.requestAmount_SetText(amount);
            page.remark_SetText(remarks);
            page.clickOnSubmitTransferInitiate();
            if (ConfigInput.isConfirm)
                page.clickOnConfirmTransferInitiate();

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                if (Assertion.verifyActionMessageContain("stock.transfer.success", "verify action message", pNode)) {
                    pNode.pass("Successfully Initiated Stock Amount : " + amount);
                    return msg.split(":")[1].split(",")[0].trim();
                } else {
                    pNode.fail("Failed to Initiate Stock!");
                    Utils.captureScreen(pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    /**
     * initiateAndApproveIMT :  Both IMT initiation And Approval included
     *
     * @param refNumber Reference Number
     * @param amount    Amount
     * @param remark    Remarks
     * @return current Instance
     */
    public StockManagement initiateAndApproveIMT(String refNumber, String amount, String remark) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateAndApproveIMT", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {

            Login.init(pNode).login(optInitIMT);

            String txnid = initiateIMT(refNumber, amount, remark);

            if (txnid != null) {
                Login.init(pNode).login(optAppIMTL1);
                approveIMTStockL1(txnid);
                BigDecimal requestedAmount = MobiquityDBAssertionQueries.getRequestedAmount(txnid);
                DBAssertion.verifyDBAssertionEqual(requestedAmount.toString(), amount, "Verify DB Amount", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * This method will initiate the IMT STOCK
     *
     * @param refNumber
     * @param amount
     * @param remark
     * @return
     * @throws Exception
     */
    public String initiateIMT(String refNumber, String amount, String remark) throws Exception {

        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateIMT", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            // IMT initiate
            IMTinitiation_page1 imtInitiation_page1 = IMTinitiation_page1.init(pNode);
            IMTinitiation_page2 imtInitiation_page2 = new IMTinitiation_page2(pNode);

            imtInitiation_page1.navigateToLink();
            imtInitiation_page1.remittancePartner_SelectIndex(1);
            imtInitiation_page1.refNo_SetText(refNumber);
            imtInitiation_page1.requestedAmount_SetText(amount);
            imtInitiation_page1.remark_SetText(remark);
            imtInitiation_page1.submitButton_Click();

            if (ConfigInput.isConfirm)
                imtInitiation_page2.confirmBtn_Click();

            //check if assert is enable
            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                txnId = msg.split("ID :")[1].split(",")[0].trim();
                //String expected = MessageReader.getDynamicMessage("IMT.stock.initate.success",txnId, CMDExecutor.getServerDate());
                //Assertion.verifyEqual(msg,expected,"Verify IMT Stock success",pNode);
                if (Assertion.checkActionMessageContain("stock.initate.success", "IMT STOCK Initiate", pNode)) {
                    txnId = msg.split("ID :")[1].split(",")[0].trim();
                    pNode.info("Transation Id is : " + txnId);
                }
                DBAssertion.verifyDBAssertionEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnId), Constants.TXN_STATUS_INITIATED, "Check DB Status", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        //return the Transaction ID
        return txnId;
    }

    /**
     * This Method will approve the IMT STOCK at level 1
     *
     * @param txnID
     */
    public void approveIMTStockL1(String txnID) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveIMTStockL1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            // IMT Approval 1
            IMTapproval1_page1 imtApproval1_page1 = new IMTapproval1_page1(pNode);

            imtApproval1_page1.NavigateToLink();
            imtApproval1_page1.selectTransactionID(txnID);
            imtApproval1_page1.submitBtn_Click();
            if (ConfigInput.isConfirm)
                imtApproval1_page1.approveBtn_Click();
            Thread.sleep(2000);

            //check if assert is enable
            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getMessage("stock.approve.success", null);
                if (actual.contains(expected)) {
                    pNode.info("Stock sucessFully Approved with txnid " + txnID);
                    DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID),
                            Constants.TXN_STATUS_SUCCESS, "Verify DB Status", pNode);

                } else if (Assertion.checkActionMessageContain("stock.approval.secondLevelNeeded", "Second Level Approval", pNode)) {
                    pNode.info("Stock sucessFully Approved and Second Level Approval required " + txnID);
                    approveIMTStockL2(txnID); // Navigate to Level 2
                } else {
                    pNode.fail("Failed to approve Stock !");
                    Utils.captureScreen(pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * @param txnId
     * @param ReferenceNumber
     * @param remarks
     * @throws Exception
     * @deprecated method is used for specific assertion and cannot be used in generic testing
     */
    public void approveIMTStockL1(String txnId, String ReferenceNumber, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;
        try {
            Markup m = MarkupHelper.createLabel("approveIMTStockL1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            // IMT Approval 1
            IMTapproval1_page1 imtApproval1_page1 = new IMTapproval1_page1(pNode);

            imtApproval1_page1.NavigateToLink();
            imtApproval1_page1.selectTransactionID(txnId);
            Assertion.verifyEqual(imtApproval1_page1.getTransactionID(txnId), txnId, "Assert Transcation ID", pNode);
            //Assertion.verifyEqual(imtApproval1_page1.getDate(txnId), CMDExecutor.getServerDate(), "Assert Date", pNode);
            Assertion.verifyEqual(imtApproval1_page1.getReferenceNumber(txnId), ReferenceNumber, "Assert ReferenceNumber", pNode);
            Assertion.verifyEqual(imtApproval1_page1.getCurrentStatus(txnId), Constants.Stock_Statuslevel1, "Assert Current Status", pNode);

            imtApproval1_page1.submitBtn_Click();


            if (remark != null)
                imtApproval1_page1.enterRemarks(remark);

            if (ConfigInput.isConfirm)
                imtApproval1_page1.approveBtn_Click();

            Thread.sleep(2000);

            //check if assert is enable
            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getMessage("stock.approve.success", null);
                if (actual.equalsIgnoreCase(expected)) {
                    pNode.info("Stock sucessFully Approved with txnid " + txnId);
                } else if (Assertion.checkActionMessageContain("stock.approval.secondLevelNeeded", "Second Level Approval", pNode)) {
                    pNode.info("Stock sucessFully Approved and Second Level Approval required " + txnId);
                    approveIMTStockL2(txnId); // Navigate to Level 2
                } else {
                    pNode.fail("Failed to approve Stock !");
                }
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void rejectIMTStockLevel1(String txnID) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectIMTStockLevel1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            // IMT Approval 1
            IMTapproval1_page1 imtApproval1_page1 = new IMTapproval1_page1(pNode);

            imtApproval1_page1.NavigateToLink();
            imtApproval1_page1.selectTransactionID(txnID);
            imtApproval1_page1.submitBtn_Click();
            imtApproval1_page1.rejectBtn_Click();
            Thread.sleep(2000);

            //check if assert is enable
            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("stock.reject.success", "Network Stock Rejection", pNode)) {
                    pNode.info("Stock sucessFully rejected with txnid " + txnID);
                } else {
                    pNode.fail("Failed to reject Stock at level 1!");
                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * This method will approve the IMT Stock at level 2
     *
     * @param txnID
     * @throws Exception
     */
    public void approveIMTStockL2(String txnID) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approve2ImtStock", ExtentColor.BLUE);
            pNode.info(m);

            OperatorUser imtApprover2 = DataFactory.getOperatorUserWithAccess("STK_IMT_APPROVAL2");
            Login.init(pNode).login(imtApprover2);

            if (imtApprover2 == null) {
                pNode.fail("Operator User with access 'STK_IMT_APPROVAL2' not found. Please check OperatorUser and RnR sheet.");
                Assert.fail("Operator User with access 'STK_IMT_APPROVAL2' not found. Please check OperatorUser and RnR sheet.");
            }

            // IMT Approval 2 Page
            IMTapproval2_page1 imtApproval2_page1 = new IMTapproval2_page1(pNode);

            imtApproval2_page1.navigateToLink();
            imtApproval2_page1.selectTransactionID(txnID);

            imtApproval2_page1.submitBtn_Click();

            if (ConfigInput.isConfirm)
                imtApproval2_page1.approveBtn_Click();
            Thread.sleep(2000);

            //check if assert is enable
            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("stock.approve.success", "Approval 2", pNode, txnID)) {
                    pNode.info("Stock sucessFully Approved with txnid " + txnID);
                    DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID),
                            Constants.TXN_STATUS_SUCCESS, "Verify DB Status", pNode);
                } else {
                    pNode.fail("Failed to approve Stock at level 2!");
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    public void approveIMTStockL2(String txnID, String ReferenceNumber, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;
        try {
            Markup m = MarkupHelper.createLabel("approve2ImtStock", ExtentColor.BLUE);
            pNode.info(m);

            // IMT Approval 2 Page
            IMTapproval2_page1 imtApproval2_page1 = new IMTapproval2_page1(pNode);

            imtApproval2_page1.navigateToLink();
            imtApproval2_page1.selectTransactionID(txnID);
            Assertion.verifyEqual(imtApproval2_page1.getTransactionID(txnID), txnID, "Assert Transcation ID", pNode);
            //TODO remove the commented line for additional verification
            //Assertion.verifyEqual(imtApproval2_page1.getDate(txnID), CMDExecutor.getServerDate(), "Assert Date", pNode);
            Assertion.verifyEqual(imtApproval2_page1.getReferenceNumber(txnID), ReferenceNumber, "Assert ReferenceNumber", pNode);
            Assertion.verifyEqual(imtApproval2_page1.getCurrentStatus(txnID), Constants.Stock_Statuslevel2, "Assert Current Status", pNode);

            imtApproval2_page1.submitBtn_Click();

            if (ConfigInput.isConfirm)
                imtApproval2_page1.approveBtn_Click();
            Thread.sleep(2000);

            //check if assert is enable
            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("stock.approve.success", "Approval 2", pNode)) {
                    pNode.info("Stock sucessFully Approved with txnid " + txnID);
                } else {
                    pNode.fail("Failed to approve Stock at level 2!");
                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    //=======================================================================
    //================= STOCK ENQUIRY FLOW ==================================
    //=======================================================================

    public void rejectIMTStockLevel2(String txnID) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectIMTStockLevel2", ExtentColor.BLUE);
            pNode.info(m);

            // IMT Approval 2 Page
            IMTapproval2_page1 imtApproval2_page1 = new IMTapproval2_page1(pNode);

            imtApproval2_page1.navigateToLink();
            imtApproval2_page1.selectTransactionID(txnID);
            imtApproval2_page1.submitBtn_Click();
            imtApproval2_page1.rejectBtn_Click();
            Thread.sleep(2000);

            //check if assert is enable
            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("stock.reject.success", "Network Stock Rejection", pNode)) {
                    pNode.info("Stock successfully rejected with Transaction ID: " + txnID);
                } else {
                    pNode.fail("Failed to Reject Stock at level 2!");
                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Enquire about the stock
     *
     * @deprecated
     */
    public void stockEnquiry(String transID, String amount) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("stockEnquiry", ExtentColor.BLUE);
            pNode.info(m);

            StockEnquiry_page1 stockEnquiry_page1 = new StockEnquiry_page1(pNode);
            StockEnquiry_page2 stockEnquiry_page2 = new StockEnquiry_page2(pNode);

            stockEnquiry_page1.navigateToLink();
            stockEnquiry_page1.transactionID_SetText(transID);
            stockEnquiry_page1.submitButton_Click();
            stockEnquiry_page2.getTransactionStatus();
            stockEnquiry_page2.submitButton_Click();
            stockEnquiry_page2.VerifyTransID(transID);
            stockEnquiry_page2.backButton_Click();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    public void stockEnquiry(String transID) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("stockEnquiry", ExtentColor.BLUE);
            pNode.info(m);

            StockEnquiry_page1 stockEnquiry_page1 = new StockEnquiry_page1(pNode);
            StockEnquiry_page2 stockEnquiry_page2 = new StockEnquiry_page2(pNode);

            stockEnquiry_page1.navigateToLink();
            stockEnquiry_page1.transactionID_SetText(transID);
            stockEnquiry_page1.submitButton_Click();
            if (ConfigInput.isConfirm) {
                stockEnquiry_page2.submitButton_Click();
                String actualStatus = stockEnquiry_page2.getTransactionStatus();
                String expectedStatus = MobiquityGUIQueries.dbGetTransactionStatus(transID);
                String Actual = null;
                switch (actualStatus) {
                    case "Transaction Success":
                        Actual = Constants.TXN_STATUS_SUCCESS;
                        break;
                    case "Transaction Initiated":
                        Actual = Constants.TXN_STATUS_INITIATED;
                        break;
                    case "Transaction Failed":
                        Actual = Constants.TXN_STATUS_FAIL;
                        break;

                }

                Assertion.verifyEqual(Actual, expectedStatus,
                        "Verify Status of the Transaction", pNode);

                Assertion.verifyEqual(stockEnquiry_page2.getTransactionID(), transID,
                        "Verify Transaction ID for Enquiry", pNode);
                stockEnquiry_page2.backButton_Click();
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }


    //=======================================================================
    //================= STOCK TRANSFER EA ENQUIRY FLOW ======================
    //=======================================================================

    /**
     * Perfrom transction based on the date range If transaction ID is null then none will be selected     *
     *
     * @param fromDate
     * @param toDate
     * @param status
     * @param transID
     */
    public void stockEnquiry_date(String fromDate, String toDate, String status, String transID) throws IOException {
        try {

            Markup m = MarkupHelper.createLabel("stockEnquiry_date", ExtentColor.BLUE);
            pNode.info(m);
            StockEnquiry_page1 stockEnquiry_page1 = new StockEnquiry_page1(pNode);
            StockEnquiry_page2 stockEnquiry_page2 = new StockEnquiry_page2(pNode);

            stockEnquiry_page1.navigateToLink();
            stockEnquiry_page1.fromDate_SetText(fromDate);
            stockEnquiry_page1.toDate_SetText(toDate);
            stockEnquiry_page1.status_SelectValue(status);
            stockEnquiry_page1.submitButton_Click();

            if (transID != null) {
                stockEnquiry_page2.selectTransaction(transID);
            }
            stockEnquiry_page2.submitButton_Click();

            Assertion.verifyEqual(stockEnquiry_page2.getTransactionID(), transID,
                    "Verify Transaction ID for Enquiry", pNode);


            String actualStatus = stockEnquiry_page2.getTransactionStatus();
            String expectedStatus = MobiquityGUIQueries.dbGetTransactionStatus(transID);
            String Actual = null;
            switch (actualStatus) {
                case "Transaction Success":
                    Actual = "TS";
                    break;
                case "Transaction Initiated":
                    Actual = "TI";
                    break;
                case "Transaction Failed":
                    Actual = "TF";
                    break;

            }
            Assertion.verifyEqual(Actual, expectedStatus,
                    "Verify Status of the Transaction", pNode);

            /*String expectedStatus;

            if (status.equalsIgnoreCase(Constants.TXN_STATUS_SUCCESS)) {
                expectedStatus = Constants.TXN_SUCCESS_STRING;
            } else {
                expectedStatus = Constants.TXN_FAIL_STRING;
            }

            Assertion.verifyEqual(stockEnquiry_page2.getTransactionStatus(), expectedStatus,
                    "Verify Transaction Status", pNode);*/

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Initiate EA Enquiry Page,
     *
     * @param transID - transaction ID
     * @return - page instance, other values can also be validated
     * @throws IOException
     */
    public StockTransferEaEnquiry_page2 initiateStockEATransferEnquiry(String transID) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateStockEATransferEnquiry", ExtentColor.BLUE);
        pNode.info(m);
        StockTransferEaEnquiry_page2 enqPage = new StockTransferEaEnquiry_page2(pNode);
        StockTransferEaEnquiry_page1 enquiry_page1 = new StockTransferEaEnquiry_page1(pNode);
        try {
            enquiry_page1.navigateToLink()
                    .transactionID_SetText(transID)
                    .submitButton_Click();
            if (ConfigInput.isConfirm) {
                enqPage.submitButton_Click_pg2();

            }
            if (ConfigInput.isAssert) {
                // generic Assertion
                Utils.putThreadSleep(Constants.TWO_SECONDS);
                if (enqPage.isTxnIdShown(transID)) {
                    String tidUI = enqPage.getTxnIDFromUI();
                    Assertion.verifyEqual(tidUI, transID, "Verify Transaction ID", pNode);
                    String expectedStatus;

                    if (MobiquityGUIQueries.dbGetTransactionStatus(transID).equalsIgnoreCase(Constants.TXN_STATUS_SUCCESS)) {
                        expectedStatus = Constants.TXN_SUCCESS_STRING;
                    } else if (MobiquityGUIQueries.dbGetTransactionStatus(transID).equalsIgnoreCase(Constants.TXN_STATUS_INITIATED)) {
                        expectedStatus = Constants.Stock_Statuslevel1;
                    } else {
                        expectedStatus = Constants.TXN_FAIL_STRING;
                    }
                    Assertion.verifyEqual(enqPage.getTransactionStatus(), expectedStatus,
                            "Verify Transaction Status", pNode);

                } else {
                    pNode.fail("Transaction Id is Not present on the EA Enquiry Page");
                    Utils.captureScreen(pNode);
                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return enqPage;
    }


    /*
      ===========================================================================
      ======================= STOCK WITHDRAWAL FLOW =============================
      ==========================================================================
    */

    /**
     * Perfrom transction based on the date range If transaction ID is null then none will be selected
     *
     * @param fromDate
     * @param toDate
     * @param status
     * @param tid
     */
    public void stockTransferEaEnquiry_Date(String fromDate, String toDate, String status, String tid) throws IOException {

        try {

            Markup m = MarkupHelper.createLabel("stockTransferEaEnquiry_Date", ExtentColor.BLUE);
            pNode.info(m);

            StockTransferEaEnquiry_page1 stockTransferEaEnquiry_page1 = new StockTransferEaEnquiry_page1(pNode);
            StockTransferEaEnquiry_page2 stockTransferEaEnquiry_page2 = new StockTransferEaEnquiry_page2(pNode);

            stockTransferEaEnquiry_page1.navigateToLink();
            stockTransferEaEnquiry_page1.fromDate_SetText(fromDate);
            stockTransferEaEnquiry_page1.toDate_SetText(toDate);
            stockTransferEaEnquiry_page1.status_SelectValue(status);
            stockTransferEaEnquiry_page1.submitButton_Click();


            // select Transaction ID if not null
            if (tid != null) {
                stockTransferEaEnquiry_page2.selectTransaction(tid);
            }

            stockTransferEaEnquiry_page2.submitButton_Click_pg2();
            Assertion.verifyEqual(stockTransferEaEnquiry_page2.getTxnIDFromUI(), tid, "Stock EA Enquiry", pNode);
            stockTransferEaEnquiry_page2.backButton_Click();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    /*===================================================================================
    //=========================== STOCK REIMBURSEMENT ======================================
    //=====================================================================================*/

    /**
     * @Author :
     * STOCK WITHDRAWAL
     * pass a value to walletType only if you want to select walletType different from IND01 else don't pass anything
     */
    public String stockWithdrawal(String amount, String... walletType) throws IOException {
        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("stockWithdrawal", ExtentColor.BROWN);
            pNode.info(m);

            String walletTypeName = walletType.length > 0 ? walletType[0] : "IND01";
            StockWithdrawal_page1 stockWithdrawl_page1 = new StockWithdrawal_page1(pNode);
            StockWithdrawal_page2 stockWithdrawl_page2 = new StockWithdrawal_page2(pNode);

            stockWithdrawl_page1.navToStockWithdrawalPage();
            stockWithdrawl_page1.walletID_Select(walletTypeName);
            stockWithdrawl_page1.selectProviderByValue(DataFactory.getDefaultProvider().ProviderId);
            stockWithdrawl_page1.selectBankName(DataFactory.getDefaultBankNameForDefaultProvider());
            stockWithdrawl_page1.bankAccno_SelectDefault();
            stockWithdrawl_page1.setTransferAmount(amount);
            stockWithdrawl_page1.submit_Click();
            stockWithdrawl_page2.confirm_Click();

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                txnId = msg.split(" ")[5].trim();
                pNode.info("Transaction ID :" + txnId);

                String actual = Assertion.getActionMessage();
                String expectedMsg = MessageReader.getDynamicMessage("stock.withdraw.successful", txnId);
                Assertion.verifyEqual(actual, expectedMsg, "Stock withdraw status", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return txnId;
    }

    public String initiateStockWithdrawal(String amount, String providerId, String bankName) throws IOException {
        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateStockWithdrawal", ExtentColor.BLACK);
            pNode.info(m);

            StockWithdrawal_page1 p1 = new StockWithdrawal_page1(pNode);
            StockWithdrawal_page2 p2 = new StockWithdrawal_page2(pNode);

            p1.navToStockWithdrawalPage();
            p1.walletID_Select("IND01");
            p1.selectProviderByValue(providerId);
            p1.selectBankName(bankName);
            p1.bankAccno_SelectDefault();
            p1.setTransferAmount(amount);
            p1.submit_Click();
            p2.confirm_Click();

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                txnId = msg.split("id:")[1].trim();
                pNode.info("Transaction ID :" + txnId);

                Assertion.verifyActionMessageContain("stock.withdraw.initiation.success",
                        "Verify that stock Withdrawal initiation is successful", pNode, txnId);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return txnId;
    }

    public void approveRejectStockWithdrawal(String txnId, boolean isApprove) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("approveRejectStockWithdrawal", ExtentColor.BLACK);
            pNode.info(m);

            StockWithdrawalApproval_page1 p1 = new StockWithdrawalApproval_page1(pNode);

            p1.navApproveStockWithdrawal();
            p1.selectRequestId(txnId);
            p1.clickOnSubmit();
            p1.setRemark("Automation");

            if (isApprove) {
                p1.clickOnApprove();
            } else {
                p1.clickOnReject();
            }

            if (ConfigInput.isAssert) {
                if (isApprove) {
                    Assertion.verifyActionMessageContain("stock.withdrawal.successfully.completed",
                            "Verify Stock Withdraw with refId:" + txnId + " is successfully Completed", pNode);
                } else {
                    Assertion.verifyActionMessageContain("stock.withdrawal.successfully.rejected",
                            "Verify Stock Withdraw with refId:" + txnId + " is successfully Rejected", pNode);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * This method will initiate Reimbursement
     * Since Reimbursement can be initiate for different for different user
     * The UI elements get some dynamically generated
     * So for different type provide corresponding null value to the field
     *
     * @param user
     * @param referenceNumber
     * @param amount
     * @param remark
     * @throws Exception
     */
    public String initiateStockReimbursement(Object user,
                                             String referenceNumber,
                                             String amount,
                                             String remark,
                                             String walletType,
                                             boolean... isLoginReq) throws Exception {
        String txnId = null;
        boolean isLoginRequired = isLoginReq.length > 0 ? isLoginReq[0] : true;
        Markup m = MarkupHelper.createLabel("initiateStockReimbursement", ExtentColor.BLUE);
        pNode.info(m);

        try {
            OperatorUser remInitiate = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");
            Login.init(pNode).login(remInitiate);
            //// Reimbursement page
            Reimbursement_page1 pg1 = new Reimbursement_page1(pNode);
            Reimbursement_page2 reimbursement_page2 = new Reimbursement_page2(pNode);
            Reimbursement_page3 reimbursement_page3 = new Reimbursement_page3(pNode);
            String value;

            if (isLoginRequired) {
                Login.init(pNode).forceLogin(stockReimUser);
            }

            pg1.navigateToLink();

            if (user instanceof User && ((User) user).CategoryCode.contains(Constants.SUBSCRIBER)) {
                pg1.type_SelectValue(Constants.SUBSCRIBER_REIMB);
                pg1.msisdn_SetText(((User) user).MSISDN);
            } else if (user instanceof RechargeOperator) {
                pg1.type_SelectValue(Constants.MERCHANT_REIMB);
                pg1.companyCodeSetText(((RechargeOperator) user).OperatorName);
            } else if (user instanceof Biller) {
                pg1.type_SelectValue(Constants.MERCHANT_REIMB);
                pg1.companyCodeSetText(((Biller) user).BillerCode);
            }else if (user instanceof OperatorUser) {
                pg1.type_SelectValue(Constants.OPERATOR_REIMB);
                pg1.accountNumber_SetText("IND03"); //TODO - need to make this generic
            } else {
                pg1.type_SelectValue(Constants.CHANNEL_REIMB);
                pg1.msisdn_SetText(((User) user).MSISDN);
            }

            pg1.clickOnProvider();

            if (walletType != null) {
                pg1.wallet_SelectValue(walletType);
            }
            pg1.refNumber_SetText(referenceNumber);
            pg1.remark_SetText(remark);
            pg1.submit_Click();
            Thread.sleep(Constants.MAX_WAIT_TIME); // sleep is required

            if (ConfigInput.isConfirm) {
                reimbursement_page2.amount_SetText(amount);
                Thread.sleep(Constants.THREAD_SLEEP_1000);
                reimbursement_page2.confirm_Click();
            }

            if (ConfigInput.confirmReq) {
                Thread.sleep(Constants.THREAD_SLEEP_1000);
                reimbursement_page3.confirm_Click();
            }

            if (ConfigInput.isAssert) {
                Thread.sleep(Constants.MAX_WAIT_TIME);
                String msg = Assertion.getActionMessage();
                txnId = msg.split("ID:")[1].trim();
                Assertion.verifyActionMessageContain("optremb.initiate.success", "Reimbursement is Initiated", pNode, txnId);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    public String stockReimbursementInitiation(Object user, String providerId,
                                               String referenceNumber,
                                               String amount,
                                               String remark,
                                               String walletType,
                                               boolean isBankReq, String... bankName) throws Exception {
        String txnId = null;
        Markup m = MarkupHelper.createLabel("Initiate Stock Reimbursement", ExtentColor.BLUE);
        pNode.info(m);

        String bank = null;
        if (bankName.length > 0) {
            bank = bankName[0];
        }

        try {
            OperatorUser remInitiate = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");
            Login.init(pNode).login(remInitiate);
            //// Reimbursement page
            Reimbursement_page1 pg1 = new Reimbursement_page1(pNode);
            Reimbursement_page2 reimbursement_page2 = new Reimbursement_page2(pNode);
            Reimbursement_page3 reimbursement_page3 = new Reimbursement_page3(pNode);
            String value;

            pg1.navigateToLink();

            if (user instanceof User && ((User) user).CategoryCode.contains(Constants.SUBSCRIBER)) {
                pg1.type_SelectValue(Constants.SUBSCRIBER_REIMB);
                pg1.msisdn_SetText(((User) user).MSISDN);
            } else if (user instanceof RechargeOperator) {
                pg1.type_SelectValue(Constants.MERCHANT_REIMB);
                pg1.companyCodeSetText(((RechargeOperator) user).OperatorName);
            } else if (user instanceof Biller) {
                pg1.type_SelectValue(Constants.MERCHANT_REIMB);
                pg1.companyCodeSetText(((Biller) user).BillerCode);
            }else if (user instanceof OperatorUser) {
                pg1.type_SelectValue(Constants.OPERATOR_REIMB);
                pg1.accountNumber_SetText("IND03"); //TODO - need to make this generic
            } else {
                pg1.type_SelectValue(Constants.CHANNEL_REIMB);
                pg1.msisdn_SetText(((User) user).MSISDN);
            }

            Thread.sleep(Constants.MAX_WAIT_TIME);
            pg1.provider_SelectValue(providerId);

            if (walletType != null) {
                pg1.wallet_SelectValue(walletType);
            }
            pg1.refNumber_SetText(referenceNumber);
            pg1.remark_SetText(remark);
            if (isBankReq) {
                pg1.selectOptInstTypeBank();
                pg1.selectBank(bank);
            }
            pg1.submit_Click();
            Utils.putThreadSleep(Constants.TWO_SECONDS); // sleep is required

            if (ConfigInput.isConfirm) {
                reimbursement_page2.amount_SetText(amount);
                reimbursement_page2.confirm_Click();
            }

            if (ConfigInput.confirmReq) {
                reimbursement_page3.confirm_Click();
            }

            if (ConfigInput.isAssert) {
                Utils.putThreadSleep(Constants.TWO_SECONDS);
                String msg = Assertion.getActionMessage();
                txnId = msg.split("ID:")[1].trim();
                Assertion.verifyActionMessageContain("optremb.initiate.success", "Reimbursement is Initiated", pNode, txnId);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    /**
     * This method will approve the initiated Reimbursement
     *
     * @param txnId
     * @throws Exception
     */
    public String approveReimbursement(String txnId, String... remark) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveReimbursement", ExtentColor.BLUE);
            pNode.info(m);
            // ReimbursementApproval

            String providerName = DataFactory.getDefaultProvider().ProviderName;
            OperatorUser remApproval = DataFactory.getOperatorUserWithAccess("STOCK_REMB");
            Login.init(pNode).login(remApproval);


            ReimbursementApproval_page2 reimbursementApproval_page2 = new ReimbursementApproval_page2(pNode);
            ReimbursementApproval_page1 reimbursementApproval_page1 = new ReimbursementApproval_page1(pNode);

            reimbursementApproval_page1.navigate();
            reimbursementApproval_page1.selectTransaction(txnId);
            reimbursementApproval_page1.submit_Click();
            if (remark.length > 0)
                reimbursementApproval_page2.setRemark(remark[0]);
            if (ConfigInput.isConfirm)
                reimbursementApproval_page2.approve_Click();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.reimburse.approve",
                        "Verify Reimbursement is successfully Approved",
                        pNode,
                        txnId);
                DBAssertion.verifyDBAssertionEqual(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                        Constants.TXN_STATUS_SUCCESS, "Verify DB Status", pNode);

            }

            int flag = 0;
            List<WebElement> txnIds = reimbursementApproval_page1.getTxnIds();
            for (WebElement id : txnIds) {
                if (!(id.getText().equalsIgnoreCase(txnId))) {
                    flag = 0;
                } else {
                    flag = 1;
                    break;
                }
            }

            if (flag == 1) {
                pNode.fail("Approved Transaction is displayed after approval in approval page");
            } else {
                pNode.pass("Approved Transaction is not displayed after approval in approval page");
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }


    /*
     *************************EA STOCK TRANSFER*****************************************
     */

    public void rejectReimbursement(String txnid, String... remark) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectReimbursement", ExtentColor.BLUE);
            pNode.info(m);
            // ReimbursementApproval
            ReimbursementApproval_page2 reimbursementApproval_page2 = new ReimbursementApproval_page2(pNode);
            ReimbursementApproval_page1 reimbursementApproval_page1 = new ReimbursementApproval_page1(pNode);

            reimbursementApproval_page1.navigate();
            reimbursementApproval_page1.selectTransaction(txnid);
            reimbursementApproval_page1.submit_Click();
            if (remark.length > 0)
                reimbursementApproval_page2.setRemark(remark[0]);
            reimbursementApproval_page2.reject_Click();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("opt.withdraw.reject", "Reimbursement Rejected", pNode, txnid);
            }

            int flag = 0;
            List<WebElement> txnIds = reimbursementApproval_page1.getTxnIds();
            for (WebElement id : txnIds) {
                if (!(id.getText().equalsIgnoreCase(txnid))) {
                    flag = 0;
                } else {
                    flag = 1;
                    break;
                }
            }

            if (flag == 1) {
                pNode.fail("Approved Transaction is displayed after approval in approval page");
            } else {
                pNode.pass("Approved Transaction is not displayed after approval in approval page");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * initaite the EA stock
     *
     * @param amount
     * @return
     * @throws Exception
     */
    public String initiateApproveEAStock(String refNum, String amount, String remarks, String provider, boolean... isapprove) throws Exception {
        Boolean approve = (isapprove.length) > 0 ? isapprove[0] : true;
        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateApproveEAStock", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockTransferInitiate_pg1 page = StockTransferInitiate_pg1.init(pNode);

            page.navStockTransferInitiatePage();
            page.selectMfsProvider(DataFactory.getProviderId(provider));
            page.refNo_SetText(refNum);
            page.requestAmount_SetText(amount);
            page.remark_SetText(remarks);
            page.clickOnSubmitTransferInitiate();
            page.clickOnConfirmTransferInitiate();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("stock.transfer.initate.success", "Stock EA initiate", pNode)) {
                    pNode.pass("Successfully Initiated Stock Amount : " + amount);
                    String msg = Assertion.getActionMessage().replace(" ", ""); // replace all the WhiteSpaces
                    txnId = msg.split(":")[1].split(",")[0].trim();
                    DBAssertion.verifyDBAssertionEqual(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                            Constants.TXN_STATUS_INITIATED, "DB Status Initiation", pNode);
                    approveEAStockL1(txnId);
                } else {
                    pNode.fail("Failed to Initiate Stock!");
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return txnId;
    }

    /**
     * Initiate EA Stock
     *
     * @param refNum
     * @param amount
     * @param remarks
     * @param provider
     * @return
     * @throws Exception
     */
    public String initiateEAStock(String refNum, String amount, String remarks, String provider) throws Exception {
        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateEAStock", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockTransferInitiate_pg1 page = StockTransferInitiate_pg1.init(pNode);

            page.navStockTransferInitiatePage();
            page.selectMfsProvider(DataFactory.getProviderId(provider));
            page.refNo_SetText(refNum);
            page.requestAmount_SetText(amount);
            page.remark_SetText(remarks);
            page.clickOnSubmitTransferInitiate();
            page.clickOnConfirmTransferInitiate();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.transfer.initate.success", "Stock EA initiate", pNode);
                String msg = Assertion.getActionMessage().replace(" ", ""); // replace all the WhiteSpaces
                txnId = msg.split(":")[1].split(",")[0].trim();
                DBAssertion.verifyDBAssertionEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnId), Constants.TXN_STATUS_INITIATED, "DB Transaction Initiated", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return txnId;
    }

    /**
     * Approving EA stock initiation @ Level-1
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement approveEAStockL1(String txnId) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveEAStockL1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockTransferToEAapproval1_page1 page = new StockTransferToEAapproval1_page1(pNode);
            StockTransferToEAapproval1_page2 page2 = new StockTransferToEAapproval1_page2(pNode);

            OperatorUser approver = DataFactory.getOperatorUserWithAccess("STOCKTR_APP1", Constants.NETWORK_ADMIN);

            Login.init(pNode).login(optAppEAStockL1);

            page.navigateToLink();
            page.selectTransactionID(txnId);

            Assertion.verifyEqual(page.getTransactionID(txnId), txnId, "Assert Transcation ID", pNode);
            //Assertion.verifyEqual(page.getDate(txnId), CMDExecutor.getServerDate(), "Assert Date", pNode);
            page.submitBtn_Click();
            page.verifyAmount();
            if (ConfigInput.isConfirm)
                page2.submitBtn_Click_pg2();

            if (ConfigInput.isAssert) {
                if (Assertion.checkActionMessageContain("stock.approval.secondLevelNeeded", "Second Level Required", pNode)) {
                    pNode.pass("Successfully Approved Network Stock Transfer Second Level needed");
                    approveEAStockL2(txnId);
                } else if (Assertion.verifyActionMessageContain("stock.approve.success", "Stock Approved", pNode, txnId)) {
                    pNode.pass("Successfully Approved Network Stock Transfer");
                    DBAssertion.verifyDBAssertionEqual(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                            Constants.TXN_STATUS_SUCCESS, "Verify DB Status Success", pNode);
                } else {
                    pNode.fail("Failed to Approve Network Stock Transfer");
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * This is very specific to Test Case and could not be used for Generic Validation
     *
     * @param txnId
     * @param referenceNumber
     * @param remarks
     * @return
     * @throws Exception
     */
    public StockManagement approveEAStockL1(String txnId, String referenceNumber, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;
        Markup m = MarkupHelper.createLabel("approveEAStockL1", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            StockTransferToEAapproval1_page1 page = new StockTransferToEAapproval1_page1(pNode);
            StockTransferToEAapproval1_page2 page2 = new StockTransferToEAapproval1_page2(pNode);

            Login.init(pNode).login(optAppEAStockL1);

            page.navigateToLink();
            page.selectTransactionID(txnId);

            Assertion.verifyEqual(page.getTransactionID(txnId), txnId, "Assert Transcation ID", pNode);
            //Assertion.verifyEqual(page.getDate(txnId), CMDExecutor.getServerDate(), "Assert Date", pNode);
            Assertion.verifyEqual(page.getReferenceNumber(txnId), referenceNumber, "Assert ReferenceNumber", pNode);
            Assertion.verifyEqual(page.getCurrentStatus(txnId), Constants.Stock_Statuslevel1, "Assert Current Status", pNode);

            page.submitBtn_Click();
            if (remark != null)
                page2.enterRemarks(remark);

            if (ConfigInput.isConfirm)
                page2.submitBtn_Click_pg2();
            if (ConfigInput.isAssert) {
                if (Assertion.checkActionMessageContain("stock.approval.secondLevelNeeded", "Second Level Required", pNode)) {
                    pNode.pass("Successfully Approved Network Stock Transfer Second Level needed");
                } else if (Assertion.verifyActionMessageContain("stock.approve.success", "Stock Approved", pNode)) {
                    pNode.pass("Successfully Approved Network Stock Transfer");
                    DBAssertion.verifyDBAssertionEqual(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                            Constants.TXN_STATUS_SUCCESS, "Verify DB Status Success", pNode);
                } else {
                    pNode.fail("Failed to Approve Network Stock Transfer");
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Reject EA Stock Transfer Level One
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement rejectEAStockL1(String txnId) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectEAStockL1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockTransferToEAapproval1_page1 page = new StockTransferToEAapproval1_page1(pNode);
            StockTransferToEAapproval1_page2 page2 = new StockTransferToEAapproval1_page2(pNode);

            Login.init(pNode).login(optAppEAStockL1);

            page.navigateToLink();
            page.selectTransactionID(txnId);

            Assertion.verifyEqual(page.getTransactionID(txnId), txnId, "Assert Transcation ID", pNode);
            //Assertion.verifyEqual(page.getDate(txnId), CMDExecutor.getServerDate(), "Assert Date", pNode);

            page.submitBtn_Click();
            page2.rejectBtn_Click();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.reject.success", "Stock Rejection Check", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * EA Stock Transfer Approval Level 2
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement approveEAStockL2(String txnId) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveEAStockL2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            StockTransferToEaApproval2_page1 page1 = new StockTransferToEaApproval2_page1(pNode);
            StockTransferToEaApproval2_Page2 page2 = new StockTransferToEaApproval2_Page2(pNode);

            Login.init(pNode).login(optAppEAStockL2);

            page1.navigateToEAStockApprovalLevel2();
            page1.selectTransactionID(txnId);
            page1.clickSubmit();

            if (ConfigInput.isConfirm)
                page2.clickOnSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.approve.success", "Approve EA Stock Level 2", pNode, txnId);
                DBAssertion.verifyDBAssertionEqual(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                        Constants.TXN_STATUS_SUCCESS, "Verify DB Status Success", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Approve EA Stock Level 2 with UI Validation
     *
     * @param txnId
     * @param referenceNumber
     * @param remarks
     * @return
     * @throws Exception
     * @deprecated
     */
    public StockManagement approveEAStockL2(String txnId, String referenceNumber, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;
        try {
            Markup m = MarkupHelper.createLabel("approveEAStockL2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockTransferToEaApproval2_page1 page1 = new StockTransferToEaApproval2_page1(pNode);
            StockTransferToEaApproval2_Page2 page2 = new StockTransferToEaApproval2_Page2(pNode);


            OperatorUser approver2 = DataFactory.getOperatorUserWithAccess("STOCKTR_APP2", Constants.NETWORK_ADMIN);

            Login.init(pNode).login(approver2);

            page1.navigateToEAStockApprovalLevel2();
            page1.selectTransactionID(txnId);

            Assertion.verifyEqual(page1.getTransactionID(txnId), txnId, "Assert Transaction ID", pNode);
            //TODO Remove the commented line
            //Assertion.verifyEqual(page1.getDate(txnId), CMDExecutor.getServerDate(), "Assert Date", pNode);
            Assertion.verifyEqual(page1.getReferenceNumber(txnId), referenceNumber, "Assert ReferenceNumber", pNode);
            Assertion.verifyEqual(page1.getCurrentStatus(txnId), Constants.Stock_Statuslevel2, "Assert Current Status", pNode);


            page1.clickSubmit();

            if (remark != null)
                page2.setRemarks(remark);

            if (ConfigInput.isConfirm)
                page2.clickOnSubmit();


            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.approve.success", "Approve Network Stock Transfer", pNode);
                DBAssertion.verifyDBAssertionEqual(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                        Constants.TXN_STATUS_SUCCESS, "DB Assertion", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Reject EA Stock Level 2 Approval
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement rejectEAStockL2(String txnId) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectEAStockL2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockTransferToEaApproval2_page1 page1 = new StockTransferToEaApproval2_page1(pNode);
            StockTransferToEaApproval2_Page2 page2 = new StockTransferToEaApproval2_Page2(pNode);

            Login.init(pNode).login(optAppEAStockL2);

            page1.navigateToEAStockApprovalLevel2();
            page1.selectTransactionID(txnId);
            page1.clickSubmit();
            page1.clickOnRejectButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.reject.success", "Reject Network Stock Transfer", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Verify Reimbursement Status
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement verifyReimbursementGUIAndDBStatus(String txnId, String dbStatus) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("verifyReimbursementGUIAndDBStatus", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ReimbursementStatus_Page1 reimbursementStatusPage1 = new ReimbursementStatus_Page1(pNode);

            reimbursementStatusPage1.navigateToLink();

            String webStatus = reimbursementStatusPage1.getTransactionStatusWeb(txnId);

            if (dbStatus.contains("TS")) {
                dbStatus = "Transaction Success";
            } else if (dbStatus.contains("TI")) {
                dbStatus = "Transaction Initiated";
            } else if (dbStatus.contains("TF")) {
                dbStatus = "Transaction Failed";
            }

            String txnID = reimbursementStatusPage1.getTransactionID(txnId);
            Assertion.assertEqual(txnID, txnId, "verify transaction id", pNode);

            String txnDate = reimbursementStatusPage1.getTransferDate(txnId);
            Assertion.assertEqual(txnDate, DateAndTime.getCurrentDate("dd/MM/yy"), "verify transaction id", pNode);

            Assertion.verifyEqual(webStatus, dbStatus, "Status Verify", pNode, true);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Initiate and Approve RA Stock
     *
     * @param refNo
     * @param amount
     * @return
     * @throws Exception
     */
    public StockManagement initiateApproveRAStock(String refNo, String amount) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateApproveRAStock", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            String txnid = null;

            StockTransferRAInitiation_Approval_pg1 page = StockTransferRAInitiation_Approval_pg1.init(pNode);

            OperatorUser initiate = DataFactory.getOperatorUserWithAccess("STK_LOYALTY_INI", Constants.NETWORK_ADMIN);

            Login.init(pNode).login(initiate);

            page.navigateToInitLink();
            page.refNo_SetText(refNo);
            page.requestAmount_SetText(amount);
            page.clickOnSubmit();
            page.clickOnConfirm();

            if (ConfigInput.isAssert) {
                String actualMessage = Assertion.getActionMessage();
                String value = actualMessage.split("ID :")[1].trim();
                txnid = value.split(",")[0].trim();
                Assertion.verifyActionMessageContain("IMT.stock.initate.success", "RA Stock initiated", pNode, value);
            }
            OperatorUser approve = DataFactory.getOperatorUserWithAccess("STK_LOYALTY_APPR1", Constants.NETWORK_ADMIN);

            Login.init(pNode).login(approve);
            page.navigateToApproveLink();
            page.clickRadioBtn(txnid);
            page.clickOnSubmitBtn();
            page.clickOnApproveBtn();

            if (ConfigInput.isAssert) {
                //String actualMessage = Assertion.getActionMessage();
                //txnid = actualMessage.split("ID :")[1].trim();
                //stock.approval.secondLevelNeeded
                Assertion.verifyActionMessageContain("stock.approve.success", "RA Stock Approved", pNode, txnid);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Get Loyalty Re-Conciliation from UI
     *
     * @return
     * @throws Exception
     */
    public HashMap<String, String> getLoyaltyReConciliation() throws Exception {

        Markup m = MarkupHelper.createLabel("getLoyaltyReConciliation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        HashMap<String, String> stock = new HashMap<String, String>();
        try {

            Reconciliation page = Reconciliation.init(pNode);

            Reconciliation.init(pNode).NavigateNA();

            String totalStock = page.loyaltyStock();
            stock.put("totalStock", totalStock);

            String convertedAmt = page.convertedAmount();
            stock.put("convertedLoyaltyStock", convertedAmt);

            String walletBalance = page.ind09Balance();
            stock.put("IND09Balance", convertedAmt);

            String result = page.result();
            stock.put("ReconciliatioRresult", result);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return stock;
    }

    /**
     * Download Stcok Liquidation CSV
     *
     * @param bankName
     * @param providerid
     * @return
     * @throws Exception
     */
    public String downloadStockLiquidationCSV(String bankName, String providerid) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("downloadStockLiquidationCSV", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            StockLiquidationApproval_page1 page = StockLiquidationApproval_page1.init(pNode);
            page.NavigateToLink();
            page.SelectFromandToDateForApproval(new DateAndTime().getDate(0), new DateAndTime().getDate(0));
            page.selectProvider(providerid);
            page.selectBankName(bankName);
            if (page.downloadLiquidationFile())
                return FilePath.fileBulkStockLiquidation;

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        Assertion.markTestAsFailure("Failed to download Stock Liquidation File", pNode);
        return null;
    }

    public StockManagement expectErrorB4Confirm() {
        Markup m = MarkupHelper.createLabel("expectErrorB4Confirm", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.confirmReq = false;
        return this;
    }

    /**
     * verify Approving stock approval at Level-1
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement verifyStockApprovalLevel1(String txnId, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;

        try {
            Markup m = MarkupHelper.createLabel("stockApprovalLevel1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockApproval_Page1 page1 = StockApproval_Page1.init(pNode);
            page1.navigateToStockApproval1Page();

            List<String> txnIds = page1.getTransactionIDList();
            int flag1 = 0, flag2 = 0;
            for (int i = 0; i < txnIds.size(); i++) {
                if (txnIds.get(i).contains(txnId))
                    flag1++;
                else
                    flag2++;
            }
            if (flag1 == 0 && flag2 == txnIds.size()) {
                pNode.pass("Transaction cannot be resumed by given party transactionId is not displayed in Approval page");
            } else {
                pNode.fail("Transaction can be resumed by given party transactionId is displayed in Approval page");
                Assert.fail();
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * verify Approving stock at Level-2
     *
     * @param txnId
     * @return
     * @throws Exception
     */
    public StockManagement verifyStockApprovalLevel2(String txnId, String... remarks) throws Exception {
        String remark = (remarks.length) > 0 ? remarks[0] : null;
        try {
            Markup m = MarkupHelper.createLabel("stockApprovalLevel2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockApproval_Page2 page2 = new StockApproval_Page2(pNode);
            page2.navigateToStockApproval2Page();
            List<String> txnIds = page2.getTransactionIDList();
            int flag1 = 0, flag2 = 0;
            for (int i = 0; i < txnIds.size(); i++) {
                if (txnIds.get(i).contains(txnId))
                    flag1++;
                else
                    flag2++;
            }
            if (flag1 == 0 && flag2 == txnIds.size()) {
                pNode.pass("Transaction cannot be resumed by given party transactionId is not displayed in Approval page");
            } else {
                pNode.fail("Transaction can be resumed by given party transactionId is displayed in Approval page");
                Assert.fail();
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public StockManagement verifyEAStockTransferApproval2TableHeaders() throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("EAstockTransferApprovalLevel2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            StockTransferToEaApproval2_page1 page = new StockTransferToEaApproval2_page1(pNode);
            //TransferApprove page = new StockTransferApprove(pNode);

            String[] expected = {"Requester", "Transfer Date", "Reference Number", "Transaction ID", "Transfer Amount", "Current Status"};

            OperatorUser approver2 = DataFactory.getOperatorUserWithAccess("STOCKTR_APP2");
            Login.init(pNode).login(approver2);
            page.navigateToEAStockApprovalLevel2();
            List<WebElement> list = page.getTableHeaders();
            list.remove(0);
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getText().equalsIgnoreCase(expected[i])) {
                    Assertion.verifyEqual(list.get(i).getText(), expected[i], "Verify Table Headers.", pNode);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public String stockLimitRA(String provider, String limit) throws Exception {
        String previousLimit = null;
        Markup m = MarkupHelper.createLabel("addNetworkStockLimitRA", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            StockLimitRA_Page1 page1 = StockLimitRA_Page1.init(pNode);
            page1.navigateToStockLimitPage();
            page1.selectProviderName(provider);
            previousLimit = page1.getStockLimitUI();
            page1.setStockLimit(limit);
            page1.clickSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("stock.limit.updated", "Update Network Stock Limit", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return previousLimit;
    }

    public String initiateRAStock(String providerName, String refNo, String amount) throws Exception {
        String txnid = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateRAStock", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker


            StockTransferRAInitiation_Approval_pg1 page = StockTransferRAInitiation_Approval_pg1.init(pNode);
            page.navigateToInitLink();
            page.selectProvider(providerName);
            page.selectBankAccount(Constants.WALLET_108);
            page.refNo_SetText(refNo);
            page.requestAmount_SetText(amount);
            page.clickOnSubmit();
            page.clickOnConfirm();

            if (ConfigInput.isAssert) {
                String actualMessage = Assertion.getActionMessage();
                String value = actualMessage.split("ID :")[1].trim();
                txnid = value.split(",")[0].trim();
                Assertion.verifyActionMessageContain("IMT.stock.initate.success", "RA Stock initiated", pNode, value);
                DBAssertion.verifyDBAssertionEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnid), Constants.TXN_STATUS_INITIATED, "DB Transaction Initiated", pNode);
                return txnid;
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnid;
    }

    public StockManagement approveRAStockL1(String txnid, String refNo, String remarks, String amount, Boolean isApprove) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("RAStockTransferLevel1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockTransferRAInitiation_Approval_pg1 page = StockTransferRAInitiation_Approval_pg1.init(pNode);

            page.navigateToApproveLink();
            page.clickRadioBtn(txnid);
            page.clickOnSubmitBtn();
            page.setRemarks(remarks);
            page.clickOnApproveBtn();

            if (ConfigInput.isAssert) {
                if (isApprove) {
                    if (Assertion.checkActionMessageContain("stock.approval.secondLevelNeeded", "", pNode)) {
                    } else {
                        Assertion.verifyActionMessageContain("stock.approve.success", "RA Stock initiated", pNode, txnid);
                    }
                } else {

                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public StockManagement approveRAStockL2(String txnid, String refNo, String remarks, String amount, Boolean isApprove) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("RAStockTransferLeve2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            StockTransferRAInitiation_Approval_Pg2 page = new StockTransferRAInitiation_Approval_Pg2(pNode);

            page.navigateToLink();
            page.clickRadioBtn(txnid);
            page.clickOnSubmitBtn();
            page.setRemarks(remarks);
            page.clickOnApproveBtn();

            if (ConfigInput.isAssert) {
                if (isApprove) {
                    Assertion.verifyActionMessageContain("stock.approve.success", "RA Stock initiated", pNode, txnid);
                    DBAssertion.verifyDBAssertionEqual(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnid),
                            Constants.TXN_STATUS_SUCCESS, "Verify DB Status Success", pNode);
                } else {
                    //TODO for future Use add rejection
                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public String loyaltyWithdrawal(String amount, String remarks) throws IOException {
        String txnId = null;
        try {
            Markup m = MarkupHelper.createLabel("loyalty Stock Withdrawal", ExtentColor.BROWN);
            pNode.info(m);


            StockWithdrawalLoyalty p1 = new StockWithdrawalLoyalty(pNode);
            StockWithdrawal_page2 stockWithdrawl_page2 = new StockWithdrawal_page2(pNode);

            p1.navToStockWithdrawalPage();
            p1.walletID_Select(Constants.WALLET_109);
            p1.selectProviderByValue(DataFactory.getDefaultProvider().ProviderId);
            p1.selectBankName(DataFactory.getDefaultBankNameForDefaultProvider());
            p1.bankAccno_SelectDefault();
            p1.setTransferAmount(amount);
            p1.submit_Click();
            stockWithdrawl_page2.confirm_Click();
            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                txnId = msg.split("ID : ")[1].split(" ")[0];
                Assertion.verifyActionMessageContain("stock.withdraw.success", "Assert Loyalty message", pNode);
                DBAssertion.verifyDBAssertionEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnId), Constants.TXN_STATUS_SUCCESS, "DB Transaction Initiated", pNode);
                return txnId;
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }
}

