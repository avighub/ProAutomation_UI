package framework.features.accountManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.accountManagement.AddWallet_Page1;
import framework.pageObjects.accountManagement.DeleteWallet_Page1;
import framework.pageObjects.accountManagement.ModifyWallet_Page1;
import framework.pageObjects.accountManagement.ViewWallet_Page1;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

/**
 * Created by surya.dhal on 9/5/2018.
 */
public class AccountManagement {

    private static ExtentTest pNode;
    private static WebDriver driver;

    public static AccountManagement init(ExtentTest t1) {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new AccountManagement();
    }


    public String addWallet(String walletName, boolean... deletionRequired) throws IOException {

        Boolean required = deletionRequired.length > 0 ? deletionRequired[0] : false;

        if (required) {
            MobiquityGUIQueries.deleteTodayCreatedWallet();
        }

        String walletID = null;
        try {
            Markup m = MarkupHelper.createLabel("addWallet", ExtentColor.TEAL);
            pNode.info(m);

            AddWallet_Page1 page1 = new AddWallet_Page1(pNode);
            page1.navAddWalletLink();
            page1.setWalletName(walletName);
            page1.clickOnSaveButton();

            if (ConfigInput.isAssert) {
                walletID = MobiquityGUIQueries.getWalletID(walletName);
                String expected = MessageReader.getDynamicMessage("walletName.is.addedSuccessfulMsg", walletName, walletID);
                Assertion.verifyDynamicActionMessageContain(expected, "Add Wallet", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return walletID;
    }

    public void viewWallet(String walletName) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("viewWallet", ExtentColor.YELLOW);
            pNode.info(m);

            String walletID = MobiquityGUIQueries.getWalletID(walletName);

            ViewWallet_Page1 page1 = new ViewWallet_Page1(pNode);
            page1.navViewWalletLink();
            page1.clickOnViewDetailsLink(walletID);
            page1.switchToChildWindow();
            Thread.sleep(2000);

            if (ConfigInput.isAssert) {
                if (Utils.checkElementPresent("button.close", Constants.FIND_ELEMENT_BY_NAME, pNode)) {
                    pNode.pass("User is able to view Wallet Details.");
                } else {
                    pNode.pass("User is not able to view Wallet Details.");
                }
            }

            Utils.captureScreen(pNode);
            page1.closeChildWindow();
            page1.switchToMainWindow();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyWallet(String walletName, String newWalletName) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("modifyWallet", ExtentColor.INDIGO);
            pNode.info(m);

            String walletID = MobiquityGUIQueries.getWalletID(walletName);

            ModifyWallet_Page1 page1 = new ModifyWallet_Page1(pNode);
            page1.navModifyWalletLink();
            page1.selectRadioButton(walletID);
            page1.clickOnUpdateButton();
            page1.setWalletName(newWalletName);
            page1.clickOnSaveButton();

            if (ConfigInput.isAssert) {
                String expected = MessageReader.getDynamicMessage("walletName.is.updatedSuccessfulMsg", walletID, newWalletName);
                Assertion.verifyDynamicActionMessageContain(expected, "Modify Wallet", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void deleteWallet(String walletName) throws IOException {
        try {
            String walletID = MobiquityGUIQueries.getWalletID(walletName);
            Markup m = MarkupHelper.createLabel("deleteWallet", ExtentColor.CYAN);
            pNode.info(m);

            DeleteWallet_Page1 page1 = new DeleteWallet_Page1(pNode);
            page1.navDeleteWalletLink();
            page1.selectRadioButton(walletID);
            page1.clickOnDeleteButton();
            page1.clickOnConfirmButton();

            if (ConfigInput.isAssert) {
                String expected = MessageReader.getDynamicMessage("wallet.is.deleted", walletName, walletID);
                Assertion.verifyDynamicActionMessageContain(expected, "Delete Wallet", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void verifyLink(String parentLinkName, String parentLinkID, String... childLinkID) {

        try {
            if (Utils.checkElementPresent("//a[contains(@id,'" + parentLinkID + "')]", Constants.FIND_ELEMENT_BY_XPATH, pNode)) {
                pNode.fail("User is able to Perform the Service." + parentLinkName);
            } else {
                pNode.pass("User don't have access to perform " + parentLinkName);
            }
            if (childLinkID.length > 0) {
                if (Utils.checkElementPresent("//a[contains(@id,'" + childLinkID[0] + "')]", Constants.FIND_ELEMENT_BY_XPATH, pNode)) {
                    pNode.fail("User is able to Perform the Service." + childLinkID[0]);
                } else {
                    pNode.pass("User don't have access to perform " + childLinkID[0]);
                }
            }

            Utils.captureScreen(pNode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifyDefaultServiceAssoication(String walletName) throws IOException {
        Markup m = MarkupHelper.createLabel("viewWallet", ExtentColor.YELLOW);
        pNode.info(m);

        try{
            String walletID = MobiquityGUIQueries.getWalletID(walletName);

            ViewWallet_Page1 page1 = new ViewWallet_Page1(pNode);
            page1.navViewWalletLink();
            page1.clickOnViewDetailsLink(walletID);
            page1.switchToChildWindow();
            Thread.sleep(2000);
            if (Utils.checkElementPresent("//td[contains(text(),'No Record Exists..')]", Constants.FIND_ELEMENT_BY_NAME, pNode)) {
                String actualMessage = driver.findElement(By.xpath("//td[contains(text(),\"No Record Exists..\")]")).getText();
                Assertion.assertEqual(actualMessage, "No Record Exists..", "Verify Services Added or Not", pNode);
            }
            Utils.captureScreen(pNode);
            page1.closeChildWindow();
            page1.switchToMainWindow();
        }catch (Exception e){
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }
}
