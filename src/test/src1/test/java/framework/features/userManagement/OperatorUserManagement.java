package framework.features.userManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.*;
import framework.features.common.Login;
import framework.pageObjects.userManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.DBStatus;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class OperatorUserManagement {

    private static ExtentTest pNode;
    private static SuperAdmin saMaker, saChecker;

    public static OperatorUserManagement init(ExtentTest t1) throws Exception {

        try {
            pNode = t1;

            if (saMaker == null) {
                saMaker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
                saChecker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
        return new OperatorUserManagement();
    }

    /**
     * Create Network Admin
     *
     * @throws Exception Tried with HashMap, but the maker checked in roles could not be tested
     *                   rnr can have multiple columns for same category code, which could not be maintained in HashMap
     */
    public void baseSetCreateAdmin(String catCode) throws Exception, MoneyException {
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;

        for (int i = 0; i < rnrData.size(); i++) {
            if (rnrData.get(i).CategoryCode.equals(catCode)) {
                int userCount = rnrData.get(i).NumberOfUser;
                String webRoleName = GlobalData.rnrDetails.get(i).RoleName;
                for (int j = 0; j < userCount; j++) {
                    OperatorUser optUser = new OperatorUser(catCode, webRoleName);    // Create Operator User
                    createAdmin(optUser);
                    optUser.writeDataToExcel();// base set data need to be dump into the AppData Folder
                }
            }
        }
    }

    /**
     * Base Setup ,create Bank admin
     *
     * @throws Exception
     * @throws MoneyException
     */
    public void baseSetCreateBankAdmin() throws Exception, MoneyException {
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;

        for (int i = 0; i < rnrData.size(); i++) {

            if (rnrData.get(i).CategoryCode.equals(Constants.BANK_ADMIN)) {
                int userCount = rnrData.get(i).NumberOfUser;
                String webRoleName = GlobalData.rnrDetails.get(i).RoleName;

                for (int j = 0; j < userCount; j++) { // loop for number of users mentioned in ConfigInput File

                    for (String provider : DataFactory.getAllProviderNames()) { // loop for all providers
                        List<String> bankList = DataFactory.getTrustBankNameListForBankAdminCreation(provider);

                        for (String bankName : bankList) { // loop on each bank in Config Input
                            OperatorUser optUser = new OperatorUser(Constants.BANK_ADMIN);
                            optUser.setBankName(bankName);
                            optUser.setWebGroupRole(webRoleName);
                            createAdmin(optUser);
                            optUser.writeDataToExcel();// base set data need to be dump into the AppData Folder
                        }
                    }
                }
            }
        }
    }

    public void baseSetCreateBankUsers() throws Exception, MoneyException {
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;

        for (int i = 0; i < rnrData.size(); i++) {

            if (rnrData.get(i).CategoryCode.equals(Constants.BANK_USER)) {
                int userCount = rnrData.get(i).NumberOfUser;
                String webRoleName = GlobalData.rnrDetails.get(i).RoleName;

                for (int j = 0; j < userCount; j++) { // loop for number of users mentioned in ConfigInput File

                    for (String provider : DataFactory.getAllProviderNames()) { // loop for all providers
                        List<String> bankList = DataFactory.getTrustBankNameListForBankAdminCreation(provider);

                        for (String bankName : bankList) { // loop on each bank in Config Input
                            OperatorUser optUser = new OperatorUser(Constants.BANK_USER);
                            optUser.setBankName(bankName);
                            optUser.setWebGroupRole(webRoleName);
                            createBankUser(optUser);
                            optUser.writeDataToExcel();// base set data need to be dump into the AppData Folder
                        }
                    }
                }
            }
        }
    }

    /**
     * Initiate and approve Operator User
     *
     * @param optUser
     * @return
     * @throws Exception
     */
    public OperatorUserManagement initiateAndApproveOptUser(OperatorUser optUser) throws Exception {

        Login.init(pNode).loginAsSuperAdmin("PTY_MSU");

        initiateOperatorUser(optUser);

        // Approve Operator User
        Login.init(pNode).loginAsSuperAdmin("PTY_MSUAP");

        approveOperatorUser(optUser);

        return this;
    }

    public OperatorUserManagement initAndApproveModifyOrDeleteOptUsr(OperatorUser user, boolean forModify) throws Exception {

        Login.init(pNode).loginAsSuperAdmin("PTY_MSU");
        initModifyOrDeleteOperator(user, forModify);

        Login.init(pNode).loginAsSuperAdmin("PTY_MSUAP");
        if (forModify) {
            approveModifyOperatorUser(user, true);
        } else {
            approveDeletionOperatorUser(user, true);
        }

        return this;
    }

    public OperatorUserManagement approveModifyOperatorUser(OperatorUser user, boolean isApprove) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyApprovalOperatorUser: "
                    + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m);

            ModifyApprovalOperator_Page page = ModifyApprovalOperator_Page.init(pNode);

            if (!user.CategoryCode.equalsIgnoreCase(Constants.BULK_PAYER_ADMIN))
                page.navigateToNALink();
            else
                page.navigateToBPALink();

            page.selectUserTypeByValue(user.CategoryCode);

            page.enterLoginID(user.LoginId);
            page.clickSubmitButton();
            Thread.sleep(Constants.TWO_SECONDS);
            page.selectUserFromList(user.MSISDN);
            page.clickFinalSubmitButton();

            if (isApprove) {
                page.clickApproveButton();
            } else {
                page.clickRejectButton();
            }
            Thread.sleep(Constants.TWO_SECONDS);
            if (ConfigInput.isAssert) {
                if (isApprove) {
                    Assertion.verifyActionMessageContain("systemparty.message.updation", "Modify OPT Approve", pNode);
                    String dbStatus = MobiquityGUIQueries.getUserStatus(user.MSISDN, user.CategoryCode);
                    Assertion.verifyEqual(dbStatus, DBStatus.ACTIVE, "Verify User Status When Approved.", pNode);
                } else {
                    Assertion.verifyActionMessageContain("systemparty.message.rejected", "Modify OPT Rejection", pNode);
                }

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Create Bulk Payer admin
     *
     * @param user
     * @param parentuser
     * @return
     * @throws Exception
     */
    public OperatorUserManagement createBulkPayerAdmin(OperatorUser user, User parentuser) throws Exception {

        // set the parent user name as Enterprse User's login ID
        user.setParentUserName(parentuser.LoginId);
        //create operator user
        Login.init(pNode).login(parentuser);
        initiateOperatorUser(user);

        // Approve Operator User
        Login.init(pNode).login(parentuser);
        approveOperatorUser(user);

        // Change First Time Password
        changeFirstTimePasswordOpt(user);

        return this;
    }


    public OperatorUserManagement approveDeletionOperatorUser(OperatorUser user, boolean forApprove) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveDeletionOperatorUser: "
                    + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m);

            DeletionApprovalOperator_Page page = DeletionApprovalOperator_Page.init(pNode);

            if (!user.CategoryCode.equalsIgnoreCase(Constants.BULK_PAYER_ADMIN)) {
                page.navigateToNALink();
            } else {
                page.navigateToNALinkBPA();
                //page.selectUserTypeByValue(user.CategoryCode);
            }

            page.enterLoginID(user.LoginId);
            page.clickSubmitButton();
            page.selectUserFromList(user.MSISDN);
            page.clickFinalSubmitButton();
            if (forApprove) {
                page.clickApproveButton();
            } else {
                page.clickRejectButton();
            }

            if (ConfigInput.isAssert) {
                if (forApprove) {
                    String actual = Assertion.getActionMessage();
                    String expected = MessageReader.getDynamicMessage("systemparty.message.deletion", user.FirstName);
                    Assertion.verifyEqual(actual, expected, "Delete Operator Approve", pNode);


                    //For User DB Status check
                    DBAssertion.verifyDBAssertionEqual(MobiquityGUIQueries.getUserStatus(user.MSISDN, user.CategoryCode), DBStatus.DELETED, "Verify DB Status for Delete User", pNode);
                    //For DB SMS Assertion check
                    String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "DESC");
                    DBAssertion.verifyDBMessageContains(dbMessage, "003301", "Verify DB Message When User Deletion Approved", pNode, user.LoginId);

                } else {
                    Assertion.verifyActionMessageContain("systemparty.message.rejected", "Delete initiate is rejected", pNode);

                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Get The bank admin having specific role and Bank Associated
     *
     * @param rolecode
     * @param bankName
     * @return
     * @throws Exception
     */
    public static OperatorUser getBankAdminWithAccess(String rolecode, String bankName) throws Exception {
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, OperatorUser> map = DataFactory.getOperatorUserSet();

        /*
         * Fetch the List of operator users having specific role
         */
        String webRoleName = "";
        for (int i = 0; i < rnrData.size(); i++) {
            if (rnrData.get(i).ApplicableRoles.contains(rolecode)
                    && rnrData.get(i).CategoryCode.equals(Constants.BANK_ADMIN)) {

                webRoleName = rnrData.get(i).RoleName;

                for (OperatorUser usr : map.values()) {
                    if (rnrData.get(i).RoleName.equals(usr.WebGroupRole) && usr.isPwdReset.equals("Y") && usr.BankName.equals(bankName)) {
                        return usr;
                    }
                }
            }
        }

        // create the user Bank Admin/ bank User and return the same
        OperatorUser optUser = new OperatorUser(Constants.BANK_ADMIN);
        optUser.setBankName(bankName);
        optUser.setWebGroupRole(webRoleName);
        new OperatorUserManagement().createAdmin(optUser);
        if (optUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
            optUser.writeDataToExcel();
            return optUser;
        }
        return null;
    }

    /**
     * Get Bank User with specific role and Bank associated
     *
     * @param rolecode
     * @param bankName
     * @return
     * @throws Exception
     */
    public static OperatorUser getBankUserWithAccess(String rolecode, String bankName) throws Exception {
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, OperatorUser> map = DataFactory.getOperatorUserSet();

        /*
         * Fetch the List of operator users having specific role
         */
        String webRoleName = "";
        for (int i = 0; i < rnrData.size(); i++) {
            if (rnrData.get(i).ApplicableRoles.contains(rolecode)
                    && rnrData.get(i).CategoryCode.equals(Constants.BANK_USER)) {

                webRoleName = rnrData.get(i).RoleName;

                for (OperatorUser usr : map.values()) {
                    if (rnrData.get(i).RoleName.equals(usr.WebGroupRole) && usr.isPwdReset.equals("Y") && usr.BankName.equals(bankName)) {
                        return usr;
                    }
                }
            }
        }

        // create the user Bank Admin/ bank User and return the same
        OperatorUser optUser = new OperatorUser(Constants.BANK_USER);
        optUser.setBankName(bankName);
        optUser.setWebGroupRole(webRoleName);
        OperatorUser creator = OperatorUserManagement.init(pNode).getBankAdminWithAccess("PTY_ASU", bankName);
        OperatorUser approver = OperatorUserManagement.init(pNode).getBankAdminWithAccess("PTY_ASUA", bankName);

        Login.init(pNode).login(creator);
        OperatorUserManagement.init(pNode).initiateOperatorUser(optUser);

        // Approve Operator User
        Login.init(pNode).login(creator);
        OperatorUserManagement.init(pNode).approveOperatorUser(optUser);

        // Change First Time Password
        OperatorUserManagement.init(pNode).changeFirstTimePasswordOpt(optUser);

        if (optUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
            optUser.writeDataToExcel();
            return optUser;
        }
        return null;
    }

    /**
     * This method will modify the operator user provided to the system
     *
     * @param user
     * @return
     */
    public OperatorUserManagement initModifyOrDeleteOperator(OperatorUser user, boolean forModify) throws Exception {

        try {

            Markup m = MarkupHelper.createLabel("initModifyOrDeleteOperator: "
                    + user.CategoryCode + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            //init the pages
            ModifyOperatorUser_page1 modifyOperatorUser_page1 = ModifyOperatorUser_page1.init(pNode);
            ModifyOperatorUser_page2 modifyOperatorUser_page2 = ModifyOperatorUser_page2.init(pNode);
            ModifyOperatorUser_page3 modifyOperatorUser_page3 = ModifyOperatorUser_page3.init(pNode);

            /*
             *Page 1 : navigation link is different for BPA and network admin
             */
            if (!user.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                modifyOperatorUser_page1.NavigateNA();
            } else {
                modifyOperatorUser_page1.NavigateBPA();
            }

            modifyOperatorUser_page1.selectUserTypeByValue(user.CategoryCode);
            //First name can also be modified from the front end so it need to be
            modifyOperatorUser_page1.firstName_SetText(user.FirstName);
            modifyOperatorUser_page1.clickSubmitButton();

            if (forModify) {
                modifyOperatorUser_page2.prefix_SelectValue(user.UserPrefix);
                modifyOperatorUser_page2.firstName_SetText(user.FirstName);
                modifyOperatorUser_page2.lastName_SetText(user.LastName);
                //Commented below line as it is not editable in Libya setup
                // modifyOperatorUser_page2.msisdn_SetText(user.MSISDN);
                //modifyOperatorUser_page2.externalCode_SetText(user.ExternalCode);
                modifyOperatorUser_page2.department_SetText(user.Dept);
                modifyOperatorUser_page2.division_SetText("Automation");
                modifyOperatorUser_page2.contactNo_SetText(user.ContactNum);
                modifyOperatorUser_page2.updateButton_Click();
                modifyOperatorUser_page3.confirmButton_Click();
            } else {
                modifyOperatorUser_page2.deleteButton_Click();
                modifyOperatorUser_page3.delConfirmButton_Click();
            }

            if (ConfigInput.isAssert) {
                if (forModify == true) {
                    Assertion.verifyActionMessageContain("systemparty.message.updationapproval", "Modify", pNode);
                    String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(user.MSISDN, user.CategoryCode);
                    DBAssertion.verifyDBAssertionEqual(dbStatus, "UI", "Verify User Status When Modify Initiated", pNode);
                } else {
                    String actual = Assertion.getActionMessage();
                    String expected = MessageReader.getDynamicMessage("systemparty.message.deleteInitiated", user.FirstName);
                    Assertion.verifyEqual(actual, expected, "Delete Operator", pNode);
                    //For Status check in DB
                    String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(user.MSISDN, user.CategoryCode);
                    DBAssertion.verifyDBAssertionEqual(dbStatus, DBStatus.DELETE_INITIATED, "Verify User Status When Delete Initiated", pNode);

                    //For Status check in DB


                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Create Operator Users
     *
     * @param categoryCode
     * @param creatorCode
     * @param approverCode
     * @throws Exception
     */
    public void baseSetCreateOptUser(String categoryCode, String creatorCode, String approverCode) throws Exception, MoneyException {

        try {
            OperatorUser creatorUser = DataFactory.getOperatorUserWithAccess("PTY_ASU", creatorCode);
            OperatorUser approverUser = DataFactory.getOperatorUserWithAccess("PTY_ASUA", approverCode);

            for (int i = 0; i < GlobalData.rnrDetails.size(); i++) {
                if (GlobalData.rnrDetails.get(i).CategoryCode.equals(categoryCode)) {
                    int userCount = GlobalData.rnrDetails.get(i).NumberOfUser;
                    String webRoleName = GlobalData.rnrDetails.get(i).RoleName;

                    for (int j = 0; j < userCount; j++) {
                        OperatorUser optUser = new OperatorUser(categoryCode, webRoleName);    // Create Operator User

                        //create operator user
                        Login.init(pNode).login(creatorUser);
                        initiateOperatorUser(optUser);

                        // approve Operator User
                        Login.init(pNode).login(approverUser);
                        approveOperatorUser(optUser);

                        // change First Time Password
                        changeFirstTimePasswordOpt(optUser);
                        if (creatorCode.equals(Constants.BANK_USER)) {
                            optUser.BankName = creatorUser.BankName; // assign the same bank for Bank user
                        }
                        optUser.writeDataToExcel();// base set data need to be dump into the AppData Folder
                    }
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    public OperatorUserManagement createOptUser(OperatorUser user, String creatorCode, String approverCode) throws Exception {
        //create operator user
        OperatorUser creatorUser = DataFactory.getOperatorUserWithAccess("PTY_ASU", creatorCode);
        OperatorUser approverUser = DataFactory.getOperatorUserWithAccess("PTY_ASUA", approverCode);

        Login.init(pNode).login(creatorUser);
        initiateOperatorUser(user);

        Login.init(pNode).login(approverUser);
        approveOperatorUser(user);

        // Change First Time Password
        changeFirstTimePasswordOpt(user);

        return this;
    }

    /**
     * Initiate Operator User
     *
     * @param user
     * @throws Exception
     */
    public OperatorUserManagement initiateOperatorUser(OperatorUser user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateOperatorUser: "
                    + user.CategoryCode + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(pNode);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(pNode);

            /*
             * Page One
             */
            if (!user.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                p1.NavigateNA();
            } else {
                p1.NavigateBPA();
            }

            p1.selectUserTypeByValue(user.CategoryCode);
            p1.clickSubmitButton();

            /*
             * Page 2
             */
            // only for bank admin and Bank User
            if (user.CategoryCode.equalsIgnoreCase(Constants.BANK_ADMIN)) {
                p2.selectBankName(user.BankName);
            }

            p2.prefix_SelectIndex(1);
            p2.firstName_SetText(user.FirstName);
            p2.lastName_SetText(user.LastName);
            p2.email_SetText(user.Email);
            p2.contactNumber_SetText(user.MSISDN);
            p2.msisdn_SetText(user.MSISDN);
            p2.identificationNumber_SetText(user.ExternalCode);
            p2.department_SetText(user.Dept);
            p2.division_SetText(user.Division);
            p2.webLoginID_SetText(user.LoginId);
            p2.preferredLanguage_SelectIndex(1);
            p2.contactNumber_SetText(user.ContactNum);

            if (!AppConfig.isRandomPasswordAllowed) {
                p2.password_SetText(user.CreationPassword);
                p2.confirmPassword_SetText(user.CreationPassword);
            }

            p2.allowedDaysCheckAll_Click();
            p2.selectRole(user.WebGroupRole);
            p2.selectGeography(user);
            p2.add_Click();

            /*
             * Page 3
             */
            if (ConfigInput.isAssert) {
                p2.saveButton_Click();
                Thread.sleep(Constants.TWO_SECONDS);
                Assertion.assertActionMessageContain("systemparty.message.addInitiated", "Add Initiate Operator User", pNode);
                String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(user.MSISDN, user.CategoryCode);
                DBAssertion.verifyDBAssertionEqual(dbStatus, "AI", "Verify DB Status When User is Add initiated", pNode);
                user.setStatus(dbStatus);
                if (!AppConfig.isRandomPasswordAllowed) {
                    String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "asc");
                    DBAssertion.verifyDBMessageContains(dbMessage, "01616", "Verify DB Message When User Creation Initiated", pNode, user.LoginId);
                } else {
                    String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "asc");
                    DBAssertion.verifyDBMessageContains(dbMessage, "01616", "Verify DB Message When User Creation Initiated", pNode, user.LoginId);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Approve Operator User
     *
     * @param user
     * @throws Exception
     */
    public OperatorUserManagement approveOperatorUser(OperatorUser user, boolean... isApprove) throws Exception {
        boolean approve = (isApprove.length > 0) ? isApprove[0] : true;
        try {
            Markup m = MarkupHelper.createLabel("approveOperatorUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ApproveOperatorUser_pg1 ap1 = ApproveOperatorUser_pg1.init(pNode);
            if (!user.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                ap1.Navigate();
            } else {
                ap1.NavigateBPA();
            }

            ap1.loginID_SetText(user.LoginId);
            ap1.clickSubmitPage1();

            //approve page 2
            ap1.clickSubmitPage2();

            //approve page 3
            if (approve) {
                ap1.clickApprovePage3();
                Assertion.assertActionMessageContain("systemparty.message.approved", "Approve Operator User", pNode);
                String dbStatus = MobiquityGUIQueries.getUserStatus(user.MSISDN, user.CategoryCode);
                Assertion.verifyEqual(dbStatus, "Y", "Verify DB Status of the user", pNode);
                String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "desc");
                Assertion.verifyDBMessageContains(dbMessage, "00668", "Verify DB Message When Operator User Approved", pNode, user.LoginId);
            } else {
                ap1.clickReject();
                Assertion.assertActionMessageContain("systemparty.message.rejected", "Reject  Operator User", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Change First Time Password
     *
     * @param user
     */
    public OperatorUserManagement changeFirstTimePasswordOpt(OperatorUser user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("changeFirstTimePassword: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ChangePasswordPage changPwd = ChangePasswordPage.init(pNode);
            FunctionLibrary fl = new FunctionLibrary(DriverFactory.getDriver());
            String oldPassword = null;

            if (AppConfig.isRandomPasswordAllowed) {
                oldPassword = MobiquityGUIQueries.getPasswordFromSentSMS(user.MSISDN);
            } else {
                oldPassword = ConfigInput.userCreationPassword;
            }


            Login.init(pNode).login(user.LoginId, oldPassword);
            fl.contentFrame();

            changPwd.setOldPassword(oldPassword);
            changPwd.setNewPassword(user.Password);
            changPwd.setConfirmPassword(user.Password);
            changPwd.clickSubmit();

            if (Assertion.verifyActionMessageContain("changePassword.label.success", "Change Operator User Password", pNode)) {
                user.setIsApprover();
                user.setIsPwdReset();
                user.setStatus(Constants.STATUS_ACTIVE);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * This method will modify the operator user provided to the system
     *
     * @param user
     * @return
     */
    public OperatorUserManagement modifyOperatorUser(OperatorUser user) throws Exception {

        try {

            Markup m = MarkupHelper.createLabel("ModifyOperatorUser: "
                    + user.CategoryCode + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            //init the pages
            ModifyOperatorUser_page1 modifyOperatorUser_page1 = ModifyOperatorUser_page1.init(pNode);
            ModifyOperatorUser_page2 modifyOperatorUser_page2 = ModifyOperatorUser_page2.init(pNode);
            ModifyOperatorUser_page3 modifyOperatorUser_page3 = ModifyOperatorUser_page3.init(pNode);

            /*
             *Page 1 : navigation link is different for BPA and network admin
             */
            if (!user.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                modifyOperatorUser_page1.NavigateNA();
            } else {
                modifyOperatorUser_page1.NavigateBPA();
            }

            modifyOperatorUser_page1.selectUserTypeByValue(user.CategoryCode);
            //First name can also br modified from the front end so it need to be
            modifyOperatorUser_page1.firstName_SetText(user.FirstName);
            modifyOperatorUser_page1.clickSubmitButton();

            /*
             * page 2
             */
            modifyOperatorUser_page2.prefix_SelectValue(user.UserPrefix);
            modifyOperatorUser_page2.lastName_SetText(user.LastName);
            modifyOperatorUser_page2.lastName_SetText(user.FirstName);
            if (AppConfig.isOptUserMsisdnieEditable) {
                modifyOperatorUser_page2.msisdn_SetText(user.MSISDN);
            }
            //modifyOperatorUser_page2.externalCode_SetText(user.ExternalCode);
            modifyOperatorUser_page2.department_SetText(user.Dept);
            modifyOperatorUser_page2.division_SetText(user.Division);
            //modifyOperatorUser_page2.contactNo_SetText(user.ContactNum);
            if (!AppConfig.isRandomPasswordAllowed) {
                modifyOperatorUser_page2.webloginID_SetText(user.LoginId);
                modifyOperatorUser_page2.password_SetText(user.Password);
                modifyOperatorUser_page2.confirmPassword_SetText(user.Password);
            } else {

            }
            modifyOperatorUser_page2.updateButton_Click();

            /*
             *page 3
             */
            if (ConfigInput.isConfirm)
                modifyOperatorUser_page3.confirmButton_Click();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("systemparty.message.updationapproval", "verify Operator user modification initiated", pNode);
                String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(user.MSISDN, user.CategoryCode);
                Assertion.verifyEqual(dbStatus, "UI", "Verify DB Status when Modify Initiated.", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        //return current instance
        return this;
    }

    public OperatorUserManagement viewSelfDetails(SuperAdmin viewSelfUser) throws Exception {

        try {


            Markup m = MarkupHelper.createLabel("viewSelfDetails: ", ExtentColor.ORANGE);
            pNode.info(m);

            ViewSelfDetails_Page1 page = ViewSelfDetails_Page1.init(pNode);

            page.navigateToLink();

            String actualMsisdn = page.getMSISDNLabelText();
            String actualLoginID = page.getWebLoginIDText();


            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(actualLoginID, viewSelfUser.LoginId, "Verify WEB Login ID", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public OperatorUserManagement viewSelfDetails(OperatorUser viewSelfUser) throws Exception {

        try {


            Markup m = MarkupHelper.createLabel("viewSelfDetails: ", ExtentColor.ORANGE);
            pNode.info(m);

            ViewSelfDetails_Page1 page = ViewSelfDetails_Page1.init(pNode);

            page.navigateToLink();

            String actualMsisdn = page.getMSISDNLabelText();
            String actualLoginID = page.getWebLoginIDText();


            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(actualMsisdn, viewSelfUser.MSISDN, "Verify MSISDN", pNode);
                Assertion.verifyEqual(actualLoginID, viewSelfUser.LoginId, "Verify WEB Login ID", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public OperatorUserManagement viewOperatorDetails(OperatorUser user) throws Exception {

        try {

            Markup m = MarkupHelper.createLabel("viewOperatorDetails", ExtentColor.BLUE);
            pNode.info(m);

            ViewOperatorDetails_Page1 page = ViewOperatorDetails_Page1.init(pNode);

            page.navigateToLink();
            page.selectUserTypeByValue(user.CategoryCode);
            page.setUserFirstName(user.FirstName);
            page.clickSubmit();

            String actualMsisdn = page.getMSISDNLabelText();
            String actualUsername = page.getUserNameLabelText();
            String actualLoginID = page.getWebLoginIDLabelText();

            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(actualMsisdn, user.MSISDN, "Verify MSISDN", pNode);
                Assertion.verifyEqual(actualUsername, user.FirstName, "Verify UserName", pNode);
                Assertion.verifyEqual(actualLoginID, user.LoginId, "Verify WEB Login ID", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param initOptbalance
     * @param isDebit
     * @param providerId
     * @param walletId
     * @return
     * @throws Exception
     */
    public BigDecimal operatorBalancecheck(BigDecimal initOptbalance, boolean isDebit, String providerId, String walletId) throws Exception {
        BigDecimal currentbalance = new BigDecimal(0);
        try {

            Markup m = MarkupHelper.createLabel("operatorBalancecheck: ", ExtentColor.BLUE);
            pNode.info(m);// Method Start Marker

            currentbalance = MobiquityGUIQueries.fetchOperatorBalance(providerId, walletId);
            pNode.info("Current balance of " + walletId + " is " + currentbalance);

            if (isDebit) {
                Assertion.verifyEqual(initOptbalance.compareTo(currentbalance) > 0, true, "Verify that Balance is Debited", pNode);
            } else {
                Assertion.verifyEqual(initOptbalance.compareTo(currentbalance) < 0, true, "Verify that Balance is Credited", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return currentbalance;
    }

    public void checkForAddedZone(OperatorUser user, String zone) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("VerifyaddedZoneDisplayedinOperator", ExtentColor.BROWN);
            pNode.info(m);
            AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(pNode);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(pNode);

            /*
             * Page One
             */
            if (!user.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                p1.NavigateNA();
            } else {
                p1.NavigateBPA();
            }

            p1.selectUserTypeByValue(user.CategoryCode);
            p1.clickSubmitButton();

            /*
             * Page 2
             */
            p2.CheckGeography(zone);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public Boolean addRechargeOperator(RechargeOperator user) throws Exception {
        Markup m = MarkupHelper.createLabel("addOperator" + user.OperatorName, ExtentColor.BLUE);
        pNode.info(m);
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("OPPADD");
            Login.init(pNode).login(optUsr);

            AddOperatorId page = AddOperatorId.init(pNode);

            page.NavigateNA();
            page.setOperatorname(user.OperatorName);
            page.selectgradecode(user.GradeCode);
            page.selectSmsid();
            page.selecttopup();
            page.setDenomination("10");
            page.selectProviderAndConfirm(user.ProviderId);

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("operator.add.success", "Operator is successfully added", pNode)) {
                    return true;
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return false;
    }


    /**
     * Specific to P1 TC
     *
     * @param user
     * @param fromTime
     * @param toTime
     * @param allowidIP
     * @return
     * @throws Exception
     */
    public OperatorUserManagement initiateOperatorUserWithAllowedTimeAndIP(OperatorUser user, String fromTime, String toTime, String allowidIP) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateOperatorUser: "
                    + user.CategoryCode + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(pNode);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(pNode);

            /*
             * Page One
             */
            if (!user.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                p1.NavigateNA();
            } else {
                p1.NavigateBPA();
            }

            p1.selectUserTypeByValue(user.CategoryCode);
            p1.clickSubmitButton();

            /*
             * Page 2
             */
            // only for bank admin
            if (user.CategoryCode.equals(Constants.BANK_ADMIN)) {
                p2.selectBankName(user.BankName);
            } else {
                user.setBankName("N/A");
            }

            p2.prefix_SelectIndex(1);
            p2.firstName_SetText(user.FirstName);
            p2.lastName_SetText(user.LastName);
            p2.email_SetText(user.Email);
            p2.contactNumber_SetText(user.MSISDN);
            p2.msisdn_SetText(user.MSISDN);
            p2.identificationNumber_SetText(user.ExternalCode);
            p2.department_SetText(user.Dept);
            p2.division_SetText(user.Division);
            p2.webLoginID_SetText(user.LoginId);
            p2.preferredLanguage_SelectIndex(1);
            p2.contactNumber_SetText(user.ContactNum);
            p2.setFromTime(fromTime);
            p2.setToTime(toTime);
            p2.setIP(allowidIP);

            if (!AppConfig.isRandomPasswordAllowed) {
                p2.password_SetText(ConfigInput.userCreationPassword);
                p2.confirmPassword_SetText(ConfigInput.userCreationPassword);
            }

            p2.allowedDaysCheckAll_Click();
            p2.selectRole(user.WebGroupRole);
            p2.selectGeography(user);
            p2.add_Click();

            /*
             * Page 3
             */
            if (ConfigInput.isAssert) {
                Thread.sleep(2000);
                p2.saveButton_Click();
                Assertion.assertActionMessageContain("systemparty.message.addInitiated", "Add Initiate Operator User", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public OperatorUserManagement initiateOperatorUserWithFull(OperatorUser user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("initiateOperatorUser: "
                    + user.CategoryCode + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(pNode);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(pNode);

            /*
             * Page One
             */
            if (!user.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                p1.NavigateNA();
            } else {
                p1.NavigateBPA();
            }

            p1.selectUserTypeByValue(user.CategoryCode);
            p1.clickSubmitButton();

            /*
             * Page 2
             */
            // only for bank admin
            if (user.CategoryCode.equals(Constants.BANK_ADMIN)) {
                p2.selectBankName(user.BankName);
            } else {
                user.setBankName("N/A");
            }

            p2.prefix_SelectIndex(1);
            p2.firstName_SetText(user.FirstName);
            p2.lastName_SetText(user.LastName);
            p2.msisdn_SetText(user.MSISDN);

            p2.setShortName_SetText(user.shortName);
            p2.setAddress1_SetText(user.address1);
            p2.setAddress2_SetText(user.address2);
            p2.setSSN_SetText(user.SSN);
            p2.setDesignation_SetText(user.designation);
            p2.setCity_SetText(user.city);
            p2.setState_SetText(user.State);
            p2.setContactPerson_SetText(user.contactPerson);

            p2.email_SetText(user.Email);
            p2.contactNumber_SetText(user.ContactNum);

            p2.identificationNumber_SetText(user.ExternalCode);
            p2.department_SetText(user.Dept);
            p2.division_SetText(user.Division);
            p2.webLoginID_SetText(user.LoginId);
            p2.preferredLanguage_SelectIndex(1);

            if (!AppConfig.isRandomPasswordAllowed) {
                p2.password_SetText(user.Password);
                p2.confirmPassword_SetText(user.ConfirmPass);
            }

            p2.allowedDaysCheckAll_Click();

            if (user.isDateAndTime) {
                p2.setFromTime(user.from);
                p2.setToTime(user.to);
            }

            if (user.isAllowedIp)
                p2.setIP(user.allowedIP);

            p2.selectRole(user.WebGroupRole);
            p2.selectGeography(user);
            p2.add_Click();

            /*
             * Page 3
             */


            if (ConfigInput.isAssert) {
                p2.saveButton_Click();

                Assertion.assertActionMessageContain("systemparty.message.addInitiated", "Add Initiate Operator User", pNode);
                user.setIsCreated();
                String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(user.MSISDN, user.CategoryCode);
                Assertion.verifyEqual(dbStatus, "AI", "Verify DB Status When User Addition initiated", pNode);

                String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "asc");
                Assertion.verifyDBMessageContains(dbMessage, "01616", "Verify DB Message When User Creation Initiated", pNode, user.LoginId, user.Password);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    public OperatorUserManagement barOperatorUser(OperatorUser user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("barOperatorUser:" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m);

            BarUser_pg1 pageOne = BarUser_pg1.init(pNode);

            pageOne.navigateNAToBarUserLink();
            pageOne.selectUserTypeByValue(Constants.USER_TYPE_OPT_USER);
            pageOne.setLoginIDTbox(user.LoginId);
            pageOne.remarksForBar("Bar for Automation");
            pageOne.clickSubmit();
            pageOne.clickConfirm();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("user.bar.success", "Verify Successfully Bared the Operator User", pNode)) {
                    user.setStatus(Constants.BAR_AS_BOTH);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public RechargeOperator getRechargeOperator(String providerId) throws Exception {
        try {
            //getting OperatorID
            RechargeOperator RCoperator = MobiquityGUIQueries.getRechargeOperatorDetails(providerId);


            if (RCoperator == null) {
                // Recharge operator is not present, create new
                OperatorUser nwAdm = DataFactory.getOperatorUserWithAccess("OPPADD");
                RechargeOperator newOperator = new RechargeOperator(providerId);

                Login.init(pNode).login(nwAdm);
                if (OperatorUserManagement.init(pNode)
                        .addRechargeOperator(newOperator)) {

                    return MobiquityGUIQueries.getRechargeOperatorDetails(providerId);
                }
            } else {
                return RCoperator;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;

    }


    public OperatorUser createBulkPayerByhisOwner(OperatorUser user, String roleCode, User enterp) throws Exception {

        ExtentTest t1 = pNode.createNode("Base User Creation",
                "Creating Bulk Payer Admin User With Parent:" + enterp.LoginId);
        // Create the User with Specific Role
        String roleName = DataFactory.getRoleNameFromRoleCode(roleCode);
        user.setWebGroupRole(roleName);

        OperatorUserManagement.init(t1)
                .createBulkPayerAdmin(user, enterp);

        if (user.getStatus().equals(Constants.STATUS_ACTIVE)) {
            user.writeDataToExcel();
            return user;
        }


        return user;
    }

    /**
     * Create Admin User
     *
     * @param user
     * @return
     * @throws Exception
     */
    public OperatorUserManagement createAdmin(OperatorUser user) throws Exception {

        //create operator user
        Login.init(pNode).loginAsSuperAdmin(saMaker);
        initiateOperatorUser(user);

        // Approve Operator User
        Login.init(pNode).loginAsSuperAdmin(saChecker);
        approveOperatorUser(user);

        // Change First Time Password
        changeFirstTimePasswordOpt(user);

        return this;
    }

    public OperatorUserManagement createBankUser(OperatorUser user) throws Exception {

        try {
            //create operator user
            OperatorUser banAdmin = DataFactory.getBankAdminWithBank(user.BankName);
            Login.init(pNode).login(banAdmin);
            initiateOperatorUser(user);
            approveOperatorUser(user);

            // Change First Time Password
            changeFirstTimePasswordOpt(user);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }


        return this;
    }
}
