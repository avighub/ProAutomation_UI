package framework.features.userManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.TxnResponse;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.pageObjects.bulkSubscriberUpload.BulkSubscriberUpload_Page1;
import framework.pageObjects.subscriberManagement.*;
import framework.pageObjects.userManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class SubscriberManagement {

    private static WebDriver driver;
    private static FunctionLibrary fl;
    private static ExtentTest pNode;
    private static OperatorUser usrCreator, usrApprover, bankApprover, optBulkUploadApprove;
    private static User usrModify, usrModifyApp;
    private static SuperAdmin saMaker, saChecker;
    private Boolean isOptionalFieldRequired = false;
    public static Boolean isBankRequired = true;

    public static SubscriberManagement init(ExtentTest t1) throws Exception {

        try {
            driver = DriverFactory.getDriver();
            fl = new FunctionLibrary(driver);
            pNode = t1;

            if (usrCreator == null) {
                usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
                optBulkUploadApprove = DataFactory.getOperatorUserWithAccess("SUBBUREG");
                usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");
                bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
                saMaker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
                saChecker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
                usrModify = DataFactory.getChannelUserWithAccess("SUBSMOD", Constants.WHOLESALER);
                usrModifyApp = DataFactory.getChannelUserWithAccess("SUBSMODAP", Constants.WHOLESALER);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
        return new SubscriberManagement();
    }

    /**
     * un Bar a subscriber
     *
     * @param user
     * @return Action message
     * @throws Exception
     */

    public static String unBarSubscriber(User user) throws Exception {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("UnBarUser: " + user.MSISDN, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            if (user.isBarred()) {
                UnBarUser_pg1 pageOne = new UnBarUser_pg1(pNode);

                // * Navigate to Add Channel User Page *
                pageOne.navigateNAToUnBarUserLink();

                pageOne.selectUserTypeByValue(Constants.USER_TYPE_SUBS);
                pageOne.setMsisdn(user.MSISDN);
                pageOne.clickSubmit();
                pageOne.selectUserFromList();
                pageOne.clickOnUnbarSubmit();
                pageOne.clickOnUnbarConfirm();

                if (ConfigInput.isAssert) {
                    if (Assertion.verifyActionMessageContain("user.unbar.success",
                            "Verify User is Successfully Unbarred", pNode)) {
                        user.setStatus(Constants.STATUS_ACTIVE);
                    }
                }
            } else {
                pNode.info("User is already unbarred - Check the user status manually");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return actualMessage;
    }

    public SubscriberManagement createBaseSetSubscriber(User user) throws Exception {

        // Add Subscriber Commission Rule
        ServiceChargeManagement.init(pNode)
                .addNewSubsCommissionRule();

        // Make sure Acqq fee service charge is configured
        User chUser = DataFactory.getChannelUserWithAccess("CIN_WEB");
        ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, user, chUser, null, null, null, null);
        ServiceChargeManagement.init(pNode).configureServiceCharge(sCharge);

        // Mapp Special Wallet preferece if required, based on the System preferences
        String[] payIdArr = DataFactory.getPayIdApplicableForSubs();
        CurrencyProviderMapping.init(pNode)
                .mapWalletPreferencesUserRegistration(user, payIdArr);

        // Create subscriber User
        SubscriberManagement.init(pNode)
                .createSubscriber(user, chUser.GradeName);

        return this;
    }

    /**
     * Create Subscribed with Default Mapping
     *
     * @param subs
     * @param parentGrade
     * @return
     * @throws Exception
     */
    public SubscriberManagement createSubscriberDefaultMapping(User subs, boolean isWalletRequired, boolean isBankRequired, String... parentGrade) throws Exception {

        createSubscriberWithSpecificPayIdUsingAPI(subs, GlobalData.defaultWallet.WalletId, isBankRequired);

        /*User chAddSubs = null, chApproveSubs = null;
        if (parentGrade.length > 0) {
            chAddSubs = DataFactory.getChannelUserWithAccessAndGrade("SUBSADD", parentGrade[0]);
            chApproveSubs = DataFactory.getChannelUserWithAccessAndGrade("SUBSADDAP", parentGrade[0]);
        } else {
            chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
            chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");
        }

        ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs, chAddSubs, null, null, null, null);

        // Configure the Service Charge
        ServiceChargeManagement.init(pNode)
                .configureServiceCharge(sCharge);

        // Login as User having Subscriber Creation Rights
        Login.init(pNode)
                .login(chAddSubs);
        addInitiateSubscriber(subs);

        if (ConfigInput.isCoreRelease || ConfigInput.is4o14Release) {
            CommonUserManagement.init(pNode).assignWebGroupRole(subs);
        }

        // is wallet required
        if (isWalletRequired) {
            CommonUserManagement.init(pNode).mapDefaultWalletPreferences(subs);
        } else {
            AddChannelUser_pg4.init(pNode).clickNext(subs.CategoryCode);
        }

        // is Bank required
        if (isBankRequired) {
            CommonUserManagement.init(pNode).mapBankPreferences(subs);
        } else {
            AddChannelUser_pg5.init(pNode).clickNext(subs.CategoryCode);
            AlertHandle.handleUnExpectedAlert(pNode);
            AlertHandle.handleUnExpectedAlert(pNode);
            CommonUserManagement.init(pNode)
                    .verifyAndConfirmDetails(subs, false);
        }

        // Add Approval
        if (AppConfig.isSubsMakerCheckerAllowed) {
            // Channel User has to approve the subscriber
            Login.init(pNode).login(chApproveSubs);
            addInitiatedApprovalSubs(subs);
        }

        // Approve All banks associated with the Subscriber
        if (isBankRequired) {
            Login.init(pNode).login(bankApprover);
            CommonUserManagement.init(pNode)
                    .approveAllAssociatedBanks(subs);
        }

        // Add Subscriber Aquisition
        if (AppConfig.isTwoStepRegistrationForSubs) {
            Transactions.init(pNode)
                    .subscriberAcquisition(subs);
        }

        // Change Mpin Tpin
        Transactions.init(pNode)
                .changeCustomerMpinTpin(subs);*/
        return this;
    }

    /**
     * Delete Subscriber By Agent
     *
     * @param subs
     * @param provider
     * @param isloginreq
     * @return
     * @throws IOException
     */
    public String deleteSubscriberByAgent(User subs, String provider, boolean... isloginreq) throws IOException {
        String txnid = null;
        boolean isLoginRequired = isloginreq.length > 0 ? isloginreq[0] : true;
        try {
            if (isLoginRequired) {
                User delSubs = DataFactory.getChannelUserWithAccess("SUBSDELBYAGT");
                Login.init(pNode).login(delSubs);
            }

            Markup m = MarkupHelper.createLabel("deleteSubscriberByAgent", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            DeleteSubscriber_Page1 page = DeleteSubscriber_Page1.init(pNode);

            page.navSubsDeleteInitiateByagent();
            page.selectProviderByValue(provider);
            page.setMSISDN(subs.MSISDN);
            page.clickDeleteSubsByAgentSubmit();

            if (ConfigInput.isConfirm)
                page.confirmDeleteByAgent();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                String actual = Assertion.getActionMessage();
                txnid = actual.split("id:")[1].trim();
                pNode.info(MarkupHelper.createLabel("Transaction ID :" + txnid, ExtentColor.ORANGE));
                String expected = MessageReader.getDynamicMessage("accClosure.label.accsuccess", subs.FirstName, subs.LastName, txnid);
                Assertion.verifyContains(actual, expected, "Delete Subs By Agent Subscriber", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return txnid;
    }

    /**
     * Create Subscriber User
     *
     * @param subs
     * @return
     * @throws Exception
     */
    public SubscriberManagement createSubscriber(User subs, String... parentGrade) throws Exception {
        String[] payIdArr = DataFactory.getPayIdApplicableForSubs();
        CurrencyProviderMapping.init(pNode)
                .mapWalletPreferencesUserRegistration(subs, payIdArr);

        User chAddSubs = null, chApproveSubs = null;
        if (parentGrade.length > 0) {
            chAddSubs = DataFactory.getChannelUserWithAccessAndGrade("SUBSADD", parentGrade[0]);
            chApproveSubs = DataFactory.getChannelUserWithAccessAndGrade("SUBSADDAP", parentGrade[0]);
        } else {
            chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
            chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");
        }

        /*
        Make Sure Acq fee service charge is configured
        for all the providers
         */
        // Configure the Service Charge
        for (String provider : DataFactory.getAllProviderNames()) {
            ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs, chAddSubs, null, null, provider, provider);
            ServiceChargeManagement.init(pNode)
                    .configureServiceCharge(sCharge);
        }

        // Login as User having Subscriber Creation Rights
        Login.init(pNode).login(chAddSubs);

        addInitiateSubscriber(subs);

        if (ConfigInput.isCoreRelease && AppConfig.isSubsLoginAllowed) {

            CommonUserManagement.init(pNode).assignWebGroupRole(subs);
        }

        CommonUserManagement.init(pNode).mapWalletPreferences(subs);

        if (isBankRequired) {
            CommonUserManagement.init(pNode).mapBankPreferences(subs);
        } else {
            AddChannelUser_pg5.init(pNode).clickNext(subs.CategoryCode);
            AlertHandle.handleUnExpectedAlert(pNode);
            AlertHandle.handleUnExpectedAlert(pNode);
            CommonUserManagement.init(pNode).verifyAndConfirmDetails(subs, false);
        }

        // Add Approval
        if (AppConfig.isSubsMakerCheckerAllowed) {
            // Channel User has to approve the subscriber
            Login.init(pNode).login(chApproveSubs);
            addInitiatedApprovalSubs(subs);
        }

        // Approve All banks associated with the Subscriber
        if (isBankRequired) {
            Login.init(pNode).login(bankApprover);
            CommonUserManagement.init(pNode)
                    .approveAllAssociatedBanks(subs);
        }

        if (AppConfig.isTwoStepRegistrationForSubs) {
            Transactions.init(pNode)
                    .subscriberAcquisition(subs);
        }

        // Change Mpin Tpin
        Transactions.init(pNode)
                .changeCustomerMpinTpin(subs);

        return this;
    }

    /**
     * Create Default Subscriber Using API
     *
     * @param subs
     * @return
     * @throws Exception
     */
    public SubscriberManagement createDefaultSubscriberUsingAPI(User subs) throws Exception {

        createSubscriberWithSpecificPayIdUsingAPI(subs, GlobalData.defaultWallet.WalletId, false);

        /*CurrencyProviderMapping.init(pNode)
                .mapPrimaryWalletPreference(subs);

        ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs, new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);

        ServiceChargeManagement.init(pNode)
                .configureServiceCharge(sCharge);

        TxnResponse response = Transactions.init(pNode)
                .selfRegistrationForSubscriberWithKinDetails(subs);

        if (response.getMessageVerificationStatus("dob.enhancement.reg.success", subs.MSISDN)) {
            subs.setStatus(Constants.STATUS_ACTIVE);

            // Change Mpin Tpin
            Transactions.init(pNode)
                    .changeCustomerMpinTpin(subs);
        }*/
        return this;
    }

    /**
     * Create Subscriber viw API, using specific Pay Id
     *
     * @param subs
     * @param payId
     * @return
     * @throws Exception NOTE - Primary wallet preferences are not mapped
     */
    public TxnResponse createSubscriberWithSpecificPayIdUsingAPI(User subs, String payId, boolean isBankRequired) throws Exception {
        ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs, new OperatorUser(Constants.NETWORK_ADMIN), null, null, null, null);
        ServiceChargeManagement.init(pNode).configureServiceCharge(sCharge);
        CurrencyProviderMapping.init(pNode)
                .mapWalletPreferencesUserRegistration(subs, new String[]{payId});

        TxnResponse response = Transactions.init(pNode).subsSelfRegistrationWithPayId(subs, payId);

        if (!ConfigInput.isAssert)
            return response;

        if (response.getMessageVerificationStatus("dob.enhancement.reg.success", subs.MSISDN)) {
            subs.setStatus(Constants.STATUS_ACTIVE);

            // add bank if isBankRequired
            if (isBankRequired) {

                // map bank Preferences
                CurrencyProviderMapping.init(pNode).linkPrimaryBankWalletPreference(subs);

                for (String provider : DataFactory.getAllProviderNames()) {
                    for (Bank bank : DataFactory.getAllTrustBanksLinkedToProvider(provider)) {
                        // link bank to the User
                        Transactions.init(pNode)
                                .bankRegistration(subs, bank, Constants.CUSTOMER_REIMB, null, null)
                                .verifyStatus(Constants.TXN_SUCCESS);
                    }
                }

            }

            if (ConfigInput.changePin) {
                // Change Mpin Tpin
                Transactions.init(pNode)
                        .changeCustomerMpinTpin(subs);

                subs.setIsCreated();
            }
        }

        return response;
    }

    /**
     * Initiate Delete Subscriber
     *
     * @param subs
     * @param deleteInitiator
     * @param providerId
     * @return
     * @throws IOException TODO - if user has Wallet and Banks Associated, then it need to be disassociated
     */
    public SubscriberManagement initiateDeleteSubscriber(User subs, Object deleteInitiator, String providerId) throws IOException {
        ;
        Markup m = MarkupHelper.createLabel("initiateDeleteSubscriber", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            // Check if Account closure Service Charge and Transfer Rules are configured
            OperatorUser optUser = new OperatorUser(Constants.OPERATOR);
            ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, optUser, null, null, null, null);
            ServiceChargeManagement.init(pNode)
                    .configureServiceCharge(sChargeAccClose);

            // Login as Delete Initiator
            Login.init(pNode)
                    .login(deleteInitiator);

            DeleteSubscriber_Page1 page = DeleteSubscriber_Page1.init(pNode);

            page.navSubscriberDeleteInitiate();
            page.selectProviderByValue(providerId);
            page.setMSISDN(subs.MSISDN);
            page.confirmSubmit();

            if (ConfigInput.isAssert) {
                page.confirmDelete();
                Assertion.isErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Bar A Subscriber
     *
     * @param user
     * @param barType
     * @return
     * @throws Exception
     */
    public String barSubscriber(User user, String barType) throws Exception {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("barSubscriber: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            if (!user.isBarred()) {
                BarUser_pg1 pageOne = BarUser_pg1.init(pNode);

                pageOne.navigateNAToBarUserLink();
                /*
                 * Provide the general Information
                 */
                pageOne.selectUserTypeByValue(Constants.USER_TYPE_SUBS);
                pageOne.setMsisdn(user.MSISDN);
                pageOne.barType(barType);
                pageOne.reasonForBar("BY_OPT");
                pageOne.remarksForBar("autoBar");
                pageOne.clickSubmit();
                pageOne.clickConfirm();

                if (ConfigInput.isAssert) {
                    if (Assertion.verifyActionMessageContain("user.bar.success", "Bar User - " + user.LoginId, pNode)) {
                        user.setStatus(barType);
                    }
                }
            } else {
                pNode.warning("User " + user.LoginId + " is already barred, Unbar first to re bar the user");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return actualMessage;
    }

    /**
     * Add Initiate Subscriber User
     *
     * @param user
     * @return
     * @throws Exception
     * @author dalia.debnath
     */
    public SubscriberManagement addInitiateSubscriber(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addInitiateSubscriber", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddSubscriber_Page1 page1 = AddSubscriber_Page1.init(pNode);

            page1.navAddSubscriber();
            page1.selectPrefix();
            page1.setFirstName(user.FirstName);
            page1.setLastName(user.LastName);
            page1.setMobileNumber(user.MSISDN);
            page1.setIdentificationNumber(user.ExternalCode);
            page1.setDateOfBirth(user.DateOfBirth);
            page1.selectRegistrationType(user.RegistrationType);
            page1.selectGender();
            page1.selectLanguage("English");
            /**
             * Checking the preference for IS_IMT_SEND_MONEY_ENABLED
             *  IF true then input fields are mandatory
             */

            enterThirdPartyRequiredFields(user);

            /**
             * Check If Id Type is a required field
             */
            if (AppConfig.isSubsLoginAllowed) {
                page1.setWebLoginId(user.LoginId);
                if (!AppConfig.isRandomPasswordAllowed) {
                    page1.setPassword(ConfigInput.userCreationPassword);
                    page1.setConfirmPassword(ConfigInput.userCreationPassword);
                }
            }

            page1.setEmail(user.Email);
            page1.selectIdType();
            page1.setCity(user.City);
            page1.setDistrict(user.District);
            page1.setAddress1(user.Address);
            page1.selectMaritalStatus();

            if (AppConfig.isAgeLimitIsRequired && user.KinUser != null) {
                pNode.info("KIN details Are Required");
                AddChannelUser_pg1.init(pNode)
                        .setKinDetails(user.KinUser);
            }

            page1.selectThirdPartyOption(user.thirdParties);
            page1.uploadIdentityProof(user.IDProof);
            page1.selectIdentityProof();
            page1.uploadAddressProof(user.AddressProof);
            page1.selectAddressProof();
            page1.uploadPhotoProof(user.PhotoProof);
            page1.selectPhotoProof();
            page1.selectRegion();

            if (ConfigInput.shouldClickNext) {
                page1.clickNext();
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Approve Subscriber User
     *
     * @param user - user object of user that need to be approved
     * @throws Exception
     * @Optional reject - Pass true if the Approval need to be rejected, else don't pass nothing TODO
     */
    public SubscriberManagement addInitiatedApprovalSubs(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addInitiatedApprovalSubs : " + user.MSISDN, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ApprovalSubscriber_Page1 page = ApprovalSubscriber_Page1.init(pNode);

            page.navSubscriberApproval();
            page.selectUsertoApprove(user.MSISDN);
            page.setReason("Automation");
            page.clickApprove();
            page.confirmApproval();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("subs.add.message.success", "Approve Subscriber", pNode)) {
                    user.setIsCreated();
                    String actualMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "DESC");
                    if (AppConfig.isConsumerPortalRequired) {
                        DBAssertion.verifyDBMessageContains(actualMessage, "1024", "Verify User  Creation Message", pNode, user.LoginId);
                    } else {

                        if (AppConfig.useDefaultPin)
                            DBAssertion.verifyDBMessageContains(actualMessage, "8021", "Verify User  Creation Message", pNode, AppConfig.defaultPin);
                        else
                            pNode.info("SMS validation not done for Random PIN");
                    }
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * SUBSCRIBER MODIFY
     *
     * @author navin.pramanik
     */
    public SubscriberManagement modifySubscriber(User modifyUser, User subs) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("modifySubscriber", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            ModifySubscriber_page1 modifySubscriberPage1 = ModifySubscriber_page1.init(pNode);
            ModifySubscriber_page2 modifySubscriberPage2 = ModifySubscriber_page2.init(pNode);
            ModifySubscriber_page3 modifySubscriberPage3 = ModifySubscriber_page3.init(pNode);
            ModifySubscriber_page4 modifySubscriberPage4 = ModifySubscriber_page4.init(pNode);

            Login.init(pNode).login(modifyUser);

            modifySubscriberPage1.navSubscriberModification();
            modifySubscriberPage1.setMSISDN(subs.MSISDN);
            modifySubscriberPage1.clickOnSubmitPg1();
            modifySubscriberPage2.setNamePrefix();
            modifySubscriberPage2.setFirstnameTbox(subs.FirstName);
            modifySubscriberPage2.lastname_SetText(subs.FirstName);
            modifySubscriberPage2.externalCode_SetText(subs.ExternalCode);


            modifySubscriberPage2.uploadIdentityProof(subs.IDProof);
            modifySubscriberPage2.selectIdentityProof();
            modifySubscriberPage2.uploadAddressProof(subs.AddressProof);
            modifySubscriberPage2.selectAddressProof();
            modifySubscriberPage2.uploadPhotoProof(subs.PhotoProof);
            modifySubscriberPage2.selectPhotoProof();

            modifySubscriberPage2.setIdType();
            enterThirdPartyRequiredDetailsOnModification(subs);
            modifySubscriberPage2.clickOnNextPg2();
            modifySubscriberPage3.selectWebGroupRole(subs.WebGroupRole);
            modifySubscriberPage3.clickOnNextPg3();
            modifySubscriberPage4.clickSubmitPg4();
            modifySubscriberPage4.clickOnConfirmPg4();
            modifySubscriberPage4.clickFinalConfirm();
            Utils.putThreadSleep(5000);

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("subs.modify.message.initiateBy", modifyUser.FirstName, modifyUser.LastName);
                Assertion.verifyEqual(actual, expected, "Modify User", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * SUBSCRIBER MODIFY
     */
    public SubscriberManagement modifySubscriberRegisteredOnHandset(User modifyUser, User subs, String id) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("modifySubscriber", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            ModifySubscriber_page1 modifySubscriberPage1 = ModifySubscriber_page1.init(pNode);
            ModifySubscriber_page2 modifySubscriberPage2 = ModifySubscriber_page2.init(pNode);
            ModifySubscriber_page3 modifySubscriberPage3 = ModifySubscriber_page3.init(pNode);
            ModifySubscriber_page4 modifySubscriberPage4 = ModifySubscriber_page4.init(pNode);


            modifySubscriberPage1.navSubscriberModification();
            modifySubscriberPage1.setMSISDN(subs.MSISDN);
            modifySubscriberPage1.clickOnSubmitPg1();
            modifySubscriberPage1.selectPrefix();
            modifySubscriberPage1.selectIdType(id);
            modifySubscriberPage1.setPassword(ConfigInput.userCreationPassword);
            modifySubscriberPage1.setConfirmPassword(ConfigInput.userCreationPassword);
            modifySubscriberPage1.selectIdentityProof();
            modifySubscriberPage1.selectAddressProof();
            modifySubscriberPage1.selectPhotoProof();
            modifySubscriberPage1.uploadIdentityProof(subs.IDProof);
            modifySubscriberPage1.uploadAddressProof(subs.AddressProof);
            modifySubscriberPage1.uploadPhotoProof(subs.PhotoProof);
            modifySubscriberPage2.clickOnNextPg2();
            if (ConfigInput.isCoreRelease || ConfigInput.is4o14Release) {
                AddChannelUser_pg3 groupRole = AddChannelUser_pg3.init(pNode);
                groupRole.selectWebGroupRole(subs.WebGroupRole);
            }
            modifySubscriberPage3.clickOnNextPg3();
            modifySubscriberPage4.clickSubmitPg4();
            modifySubscriberPage4.clickOnConfirmPg4();
            modifySubscriberPage4.clickFinalConfirm();
            Utils.putThreadSleep(3000);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("subs.modify.message.initiateBy",
                        "Verify Successful Modification of Subscriber.", pNode, modifyUser.FirstName, modifyUser.LastName);

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;


    }


    /**
     * @param modifyUser
     * @param subs
     * @param walletName
     * @param walletStatus
     * @return
     * @throws IOException
     */
    public SubscriberManagement modifyWalletStatusOfSubscriber(User modifyUser, User subs, String walletName, String walletStatus) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("modifySubscriberToDeleteOneWallet", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifySubscriber_page4 modifySubscriberPage4 = ModifySubscriber_page4.init(pNode);

            Login.init(pNode).login(modifyUser);

            ModifySubscriber_page1.init(pNode).
                    navSubscriberModification().
                    setMSISDN(subs.MSISDN).
                    clickOnSubmitPg1();

            ModifySubscriber_page2.init(pNode).
                    clickOnNextPg2();


            if (AppConfig.isSubsLoginAllowed) {
                ModifySubscriber_page3.init(pNode).
                        clickOnNextPg3();
            }

            modifySubscriberPage4.
                    selectWalletStatus(walletName, walletStatus);

            modifySubscriberPage4.clickSubmitPg4();
            if (ConfigInput.isConfirm) {
                modifySubscriberPage4.clickOnConfirmPg4();
                modifySubscriberPage4.clickFinalConfirm();
                Utils.putThreadSleep(3000);
            }

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                Assertion.verifyActionMessageContain("subs.modify.message.initiateBy", "Modify User", pNode, modifyUser.FirstName, modifyUser.LastName);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    public SubscriberManagement modifyInitiateSubscriber(User modifyUser, User subs, String id, String msisdn) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("modifyInitiateSubscriber", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddSubscriber_Page1 page1 = new AddSubscriber_Page1(pNode);

            ModifySubscriber_page1 modifySubscriberPage1 = ModifySubscriber_page1.init(pNode);
            ModifySubscriber_page2 modifySubscriberPage2 = ModifySubscriber_page2.init(pNode);
            ModifySubscriber_page3 modifySubscriberPage3 = ModifySubscriber_page3.init(pNode);
            ModifySubscriber_page4 modifySubscriberPage4 = ModifySubscriber_page4.init(pNode);

            Login.init(pNode).login(modifyUser);

            modifySubscriberPage1.navSubscriberModification();
            modifySubscriberPage1.setMSISDN(subs.MSISDN);
            modifySubscriberPage1.clickOnSubmitPg1();

            modifySubscriberPage2.setNamePrefix();
            Thread.sleep(3000);
            modifySubscriberPage1.selectIdType(id);
            Thread.sleep(3000);
            modifySubscriberPage2.msisdn_SetText(msisdn);
            page1.uploadIdentityProof(subs.IDProof);
            page1.selectIdentityProof();
            page1.uploadAddressProof(subs.AddressProof);
            page1.selectAddressProof();
            page1.uploadPhotoProof(subs.PhotoProof);
            page1.selectPhotoProof();

            modifySubscriberPage2.clickOnNextPg2();
            AddChannelUser_pg3 groupRole = AddChannelUser_pg3.init(pNode);
            groupRole.selectWebGroupRole(subs.WebGroupRole);
            modifySubscriberPage3.clickOnNextPg3();

            modifySubscriberPage4.clickSubmitPg4();
            modifySubscriberPage4.clickOnConfirmPg4();

            modifySubscriberPage4.clickFinalConfirm();
            Utils.putThreadSleep(3000);

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("subs.modify.message.initiateBy", modifyUser.FirstName, modifyUser.LastName);
                Assertion.verifyEqual(actual, expected, "Modify User", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;


    }

    /**
     * Initiate Subscriber Approval
     *
     * @param subs
     * @return
     * @throws Exception
     */
    public SubscriberManagement verifySubscriberApprovalView(User subs) throws Exception {
        Markup m = MarkupHelper.createLabel("addInitiateSubscriber", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            /**
             * Navigate to Subscriber Management >  Add Subscriber Approval page
             */
            ApprovalSubscriber_Page1 page = ApprovalSubscriber_Page1.init(pNode);
            page.navSubscriberApproval();

            //now iterating the table for reading the contents
            WebElement subscriberTable = DriverFactory.getDriver().findElement(By.className("wwFormTableC"));
            WebElement eLink = subscriberTable.findElement(By.xpath(".//tr/td[contains(text(),'" + subs.MSISDN + "')]/ancestor::tr[1]/td/a"));

            eLink.click();
            pNode.info("Clicked on the Info Link for subs:" + subs.LoginId);
            pNode.pass("Subscriber approval entry found for user: " + subs.LoginId);
            String parentWindow = driver.getWindowHandle();
            Set<String> handles = driver.getWindowHandles();
            for (String windowHandle : handles) {
                if (!windowHandle.equals(parentWindow)) {
                    driver.switchTo().window(windowHandle);
                    CommonUserManagement.init(pNode)
                            .verifyAndConfirmDetails(subs, true);
                }
                driver.switchTo().window(parentWindow);
            }
        } catch (
                Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * Initiate Subscriber User Modification
     *
     * @param user
     * @return
     * @throws Exception
     */
    public SubscriberManagement initiateSubscriberModification(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateSubscriberModification: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode)
                    .login(usrModify);

            ModifySubscriber_page1.init(pNode)
                    .navSubscriberModification()
                    .setMSISDN(user.MSISDN)
                    .clickOnSubmitPg1();

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param user
     * @return
     * @throws Exception
     * @deprecated - Use only for specific cases, in which only Last name has to be updated or use's state change is required
     */
    public SubscriberManagement subscriberModificationInitiation(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("subscriberModificationInitiation: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode)
                    .login(usrModify);

            ModifySubscriber_page1.init(pNode)
                    .navSubscriberModification()
                    .setMSISDN(user.MSISDN)
                    .clickOnSubmitPg1();

            ModifySubscriber_page2.init(pNode)
                    .clickOnNextPg2();
            ModifySubscriber_page3.init(pNode).nextPage()
                    .clickSubmitPg4()
                    .clickOnConfirmPg4();
            Thread.sleep(3000);
            ModifySubscriber_page4.init(pNode).clickFinalConfirm();
            Thread.sleep(3000);

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * verifyFieldInSubscriberReg -  Can verify various files on Subscriber Registration Page
     *
     * @param user
     * @param fieldvalue
     * @return
     * @throws Exception
     */
    public SubscriberManagement verifyFieldInSubscriberReg(User user, String fieldvalue) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("verifyFieldInSubscriberReg" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navSuspendChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModifiedApproval(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitModifiedApprovalDetail();
            try {
                String temTxt = DriverFactory.getDriver()
                        .findElement(By.xpath("//label[contains(text(),'" + fieldvalue + "')]")).getText();

                Assertion.verifyEqual(temTxt, fieldvalue + ":",
                        "Verify for field: " + fieldvalue + " On registration page for user - " + user.LoginId, pNode);
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                pNode.fail("failed to find the field " + fieldvalue + " On registration page for user - " + user.LoginId);
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param providerID
     * @param payInst
     * @param walletType
     * @param subs
     * @return
     * @throws Exception
     * @author Navin.pramanik
     */
    public SubscriberManagement doSubsInformation(String providerID, String payInst, String walletType, User subs) throws Exception {

        SubscriberInformation_Page1 subscriberInformationPage1 = new SubscriberInformation_Page1(pNode);
        SubscriberInformation_Page2 subscriberInformationPage2 = SubscriberInformation_Page2.init(pNode);

        subscriberInformationPage1.navigateToSubsInfo();
        subscriberInformationPage1.selectProvider(providerID);
        subscriberInformationPage1.selectPaymentInstrument(payInst);
        subscriberInformationPage1.selectWalletType(walletType);
        subscriberInformationPage1.setMSISDN(subs.MSISDN);
        subscriberInformationPage1.clickSubmit();

        subscriberInformationPage2.clickSubsInfoLink();
        subscriberInformationPage2.switchToWindow();
        String popMsisdn = subscriberInformationPage2.getMSISDNLabelText();
        String popName = subscriberInformationPage2.getUsernameText();
        subscriberInformationPage2.clickCloseButton();
        subscriberInformationPage2.switchToMainWindow();

        Assertion.verifyEqual(subs.MSISDN, popMsisdn, "Verify MSISDN", pNode);
        Assertion.verifyContains(popName, subs.FirstName, "Verify UserName", pNode);

        return this;
    }

    public SubscriberManagement verifyFieldOnAddSubscriberPage(User user, String fieldvalue) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("verifyFieldOnAddSubscriberPage" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navigateToAddSubscriberPage();

            try {
                String temTxt = DriverFactory.getDriver()
                        .findElement(By.xpath("//label[contains(text(),'" + fieldvalue + "')]")).getText();

                Assertion.verifyEqual(temTxt, fieldvalue + ":",
                        "Verify for field: " + fieldvalue + " On registration page for user - " + user.LoginId, pNode);
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                pNode.fail("failed to find the field " + fieldvalue + " On registration page for user - " + user.LoginId);
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param user
     * @param fieldvalue
     * @return
     * @throws Exception
     */
    public SubscriberManagement verifyFieldOnModSubscriberPage(User user, String fieldvalue) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("verifyFieldOnModSubscriberPage" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifySubscriber_page1.init(pNode)
                    .navSubscriberModification()
                    .setMSISDN(user.MSISDN)
                    .clickOnSubmitPg1();

            try {
                String temTxt = DriverFactory.getDriver()
                        .findElement(By.xpath("//label[contains(text(),'" + fieldvalue + "')]")).getText();

                Assertion.verifyEqual(temTxt, fieldvalue + ":",
                        "Verify for field: " + fieldvalue + " On registration page for user - " + user.LoginId, pNode);
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                pNode.fail("failed to find the field " + fieldvalue + " On registration page for user - " + user.LoginId);
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public SubscriberManagement subsModifyInitAndApprove(User subs) throws Exception {
        try {
            User usrSubsModify = DataFactory.getChannelUserWithAccess("SUBSMOD");
            Login.init(pNode).login(usrSubsModify);
            modifySubscriber(usrSubsModify, subs);
            User usrSubsModifyApp = DataFactory.getChannelUserWithAccess("SUBSMODAP");
            Login.init(pNode).login(usrSubsModifyApp);
            modifyApprovalSubs(subs);
        } catch (Exception e) {
            pNode.error("Check User in Excel sheet with specific Role to perform this service...");
        }
        return this;
    }

    public SubscriberManagement modifyApprovalSubs(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyApprovalSubs : " + user.MSISDN, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifySubscriberApproval_page1 page = new ModifySubscriberApproval_page1(pNode);

            page.navigateToLink();
            Thread.sleep(Constants.TWO_SECONDS);
            page.selectUsertoApprove(user.MSISDN);
            page.setReason("Automation");
            page.approveButton_Click();
            page.clickFinalSubmitButton();
            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("subs.modify.message.success")
                        .replace("{0}", user.FirstName).replace("{1}", user.LastName);

                Assertion.verifyEqual(actual, expected, "Modify User Approve", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public SubscriberManagement suspendAndResumeSubscriber(User subs, String utype) throws IOException {
        try {

            OperatorUser usrSuspResume = DataFactory.getOperatorUserWithAccess("SR_USR", Constants.NETWORK_ADMIN);

            Login.init(pNode).login(usrSuspResume);

            suspendSubscriber(subs, utype);

            resumeSubscriber(subs, utype);

        } catch (Exception e) {
            pNode.error("Check Operator User in Excel sheet with specific Role to perform this service...");
        }

        return this;
    }

    /**
     * Initiate Subscriber Bank association,run if no banks are associated with a Subs
     *
     * @param user
     * @param bankName
     * @param providerName
     * @return
     * @throws IOException
     */
    public SubscriberManagement initiateSubsBankAssociation(User user, String bankName, String providerName) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateSubsBankAssociation" + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            new SubsBankAssociation_Page1(pNode)
                    .navSubsBankAssociation()
                    .setMsisdn(user.MSISDN)
                    .selectBankType(bankName)
                    .selectProvider(providerName)
                    .clickSubmit()
                    .clickConfirm();

            Assertion.verifyActionMessageContain("subs.management.association.bank.success",
                    "Verify that Subscriber Bank Association is success", pNode);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * @param subs
     * @param utype --> User Type (SUBSCRIBER or BILLER)
     * @return
     * @throws IOException
     */
    public SubscriberManagement suspendSubscriber(User subs, String... utype) throws IOException {

        String userType = (utype.length > 0) ? utype[0] : Constants.USER_TYPE_SUBS;
        try {

            Markup m = MarkupHelper.createLabel("suspendSubscriber", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            if (!subs.isSuspended()) {
                SuspendResumeSubscriber_Page1 page1 = SuspendResumeSubscriber_Page1.init(pNode);

                page1.navigateToLink();
                page1.selectUserTypeByValue(userType);
                page1.setMSISDN(subs.MSISDN);
                page1.clickSuspendButton();

                if (Assertion.isErrorInPage(pNode) && ConfigInput.isAssert) {
                    pNode.fail("An unexpected Error has happened!");
                    Utils.captureScreen(pNode);
                    Assert.fail("Error: Check reports!");
                }

                if (ConfigInput.isConfirm) {
                    page1.clickConfirmButton(); // confirm the delete action
                }

                if (ConfigInput.isAssert) {
                    if (Assertion.verifyActionMessageContain("sub.suspended.successfully", "Verify successfully suspend Subscriber : " + subs.LoginId, pNode, subs.FirstName, subs.LastName)) {
                        subs.setStatus(Constants.STATUS_SUSPEND);
                    }
                }
            } else {
                pNode.warning("The user is already suspended, Check User status manually");
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    public SubscriberManagement resumeSubscriber(User subs, String utype) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("resumeSubscriber", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            if (subs.isSuspended()) {
                SuspendResumeSubscriber_Page1 page1 = SuspendResumeSubscriber_Page1.init(pNode);

                page1.navigateToLink();
                page1.selectUserTypeByValue(utype);
                page1.setMSISDN(subs.MSISDN);
                page1.clickResumeButton();
                page1.clickConfirmButton();

                if (ConfigInput.isAssert) {
                    Assertion.assertErrorInPage(pNode);
                    String actual = Assertion.getActionMessage();
                    String expected = MessageReader.getDynamicMessage("sub.resume/suspend.operation", subs.FirstName, subs.LastName);
                    if (Assertion.verifyEqual(actual, expected, "Resume Subscriber", pNode))
                        subs.setStatus(Constants.STATUS_ACTIVE);
                }
            } else {
                pNode.info("The User is already Active, Check user state manually");
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Delete Subscriber
     *
     * @param subs
     * @param providerID
     * @return
     * @throws IOException
     */
    public String deleteSubscriber(User subs, String providerID) throws IOException {
        String txnid = null;
        try {
            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance(providerID, "IND01");

            User delSubs = DataFactory.getChannelUserWithAccess("SUBSDEL");

            Login.init(pNode).login(delSubs);

            Markup m = MarkupHelper.createLabel("deleteSubscriber", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            DeleteSubscriber_Page1 page = DeleteSubscriber_Page1.init(pNode);

            page.navSubscriberDeleteInitiate();
            page.selectProviderByValue(providerID);
            page.setMSISDN(subs.MSISDN);
            page.confirmSubmit();

            Utils.captureScreen(pNode);

            if (ConfigInput.isConfirm) {
                page.confirmDelete(); // confirm the delete action
            }

            if (!ConfigInput.isAssert)
                return null; // as no further assertion is expected

            String actual = Assertion.getActionMessage();
            txnid = actual.split("id:")[1].trim();
            pNode.info(MarkupHelper.createLabel("Transaction ID :" + txnid, ExtentColor.ORANGE));

            if (AppConfig.isAccountClosureTwoStep) {
                if (Assertion.verifyActionMessageContain("accClosure.label.accsuccess",
                        "Verify Account closure is Initiated Successfully when Account closure is TWO Step",
                        pNode, subs.FirstName, subs.LastName, txnid)) {

                    // perform account closure
                    TxnResponse res = Transactions.init(pNode).subsAccountClosure(subs, txnid);

                    if (Assertion.verifyEqual(res.TxnStatus, Constants.TXN_SUCCESS, "Verify Subscriber is successfully deleted", pNode)) {
                        subs.setStatus(Constants.AUT_STATUS_DELETED);
                        BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance(Constants.WALLET_101);
                        Assertion.verifyAccountIsCredited(preOperatorBalance, postOperatorBalance, postOperatorBalance.subtract(preOperatorBalance), "Verify that Wallet IND01 is credited successfully", pNode);
                    }
                }
            } else {
                if (Assertion.verifyActionMessageContain("accClosure.label.success",
                        "Delete Subscriber", pNode, subs.FirstName, subs.LastName, txnid)) {
                    subs.setStatus(Constants.AUT_STATUS_DELETED);

                    BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance(Constants.WALLET_101);
                    Assertion.verifyAccountIsCredited(preOperatorBalance, postOperatorBalance, postOperatorBalance.subtract(preOperatorBalance), "Verify that Wallet IND01 is credited successfully", pNode);
                    String lastTxnId = MobiquityGUIQueries.getLastTransferIDFromOperator(Constants.WALLET_101);
                    Assertion.verifyEqual(lastTxnId, txnid, "Verifying Last Transfer For Operator", pNode);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return txnid;
    }

    public SubscriberManagement initiateApproveBulkSubscriberModification(String file) throws IOException {

        try {
            Login.init(pNode).login(optBulkUploadApprove);
            BulkSubscriberUpload_Page1 page = new BulkSubscriberUpload_Page1(pNode);
            page.navigateToBulkSubscriber();
            page.uploadFile(file);
            page.clickSubmit();

            // Verify that user Registration is Successful
            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("bulkUpload.channel.label.success", "Verify that the Bulk User Modification is successful", pNode)) {
                    String message = driver.findElements(By.className("actionMessage")).get(0).getText();
                    String batchId = message.split("ID :")[1].trim();
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * @param file
     * @param user
     * @param providerName
     * @param payInstName
     * @param userStatus          User Status*(A/M/N)
     * @param payInstrumentStatus Wallet Type/Linked Bank Status* (A/M/S/D)
     * @param isPayInstPrimary    true/false
     * @param isAppend            true/false
     * @return
     * @throws Exception
     */
    public SubscriberManagement writeDataToBulkSubsModificationCsv(User user,
                                                                   String file,
                                                                   String providerName,
                                                                   String payInstName,
                                                                   String userStatus,
                                                                   String payInstrumentStatus,
                                                                   boolean isPayInstPrimary,
                                                                   boolean isAppend) throws Exception {
        Markup m = MarkupHelper.createLabel("writeDataToBulkSubsRegistrationCsv: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            String isPrimary = (isPayInstPrimary) ? "Y" : "N";
            String dateOfBirth = "28/02/1986";
            String expireDate = "28/02/2024";
            String issueDate = DataFactory.getCurrentDateSlash();

            String custId, accountNum;

            if (user.custIdOptional != null) {
                custId = user.custIdOptional;
            } else if (AppConfig.isBankAccLinkViaMsisdn) {
                custId = user.MSISDN;
            } else {
                custId = DataFactory.getRandomNumberAsString(9);
            }

            if (user.accNumOtional != null) {
                accountNum = user.accNumOtional;
            } else if (AppConfig.isBankAccLinkViaMsisdn) {
                accountNum = user.MSISDN;
            } else {
                accountNum = DataFactory.getRandomNumberAsString(9);
            }

            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, providerName, payInstName);

            String mobileRoleName = null;
            mobileRoleName = user.hasPayInstTypeLinked(payInstName, providerName).ProfileName;
            String l_tcpID = MobiquityGUIQueries.getIDofTCPprofile(insTcp.ProfileName);

            if (user.KinUser == null) {
                user.setKinUser(new KinUser()); // assign a kin user with empty details
                user.KinUser.KinNumber = "";
                user.KinUser.DateOfBirth = "";
                user.KinUser.FirstName = "";
                user.KinUser.LastName = "";
                user.KinUser.MiddleName = "";
                user.KinUser.RelationShip = "";
                user.KinUser.ContactNum = "";
                user.KinUser.Age = "";
                user.KinUser.ContactNum = "";
                user.KinUser.Nationality = "";
            }

            String[] allData = {"Mr.",    //	NamePrefix (Mr./Mrs./Ms/M/S)*
                    user.LoginId,    //	FirstName*
                    user.LoginId,    //	LastName*
                    user.MSISDN,    //	MSISDN*
                    user.ExternalCode,    //	IdentificationNumber*
                    user.MSISDN,    //	FormNumber
                    dateOfBirth,    //	DateofBirth(dd/MM/yyyy)*
                    "Male",    //	Gender (Male/Female)*
                    "address1",    //	Address
                    "",    //	District
                    "Gurgaon",    //	City*
                    "Haryana",    //	State
                    "IN",    //	Country
                    "",    //	Description
                    "English",    //	Preferred Language*
                    user.WebGroupRole,    //	Web Group Role Id*
                    user.LoginId,    //	Login Id*
                    "",    //	Type of Identity Proof
                    "",    //	Type of Address Proof
                    "",    //	Type of Photo Proof
                    mobileRoleName,    //	Mobile Group Role Id*
                    user.GradeCode,    //	Grade Code*
                    l_tcpID,    //	TCP Profile Id*
                    isPrimary,    //	Primary Account (Y/N)*
                    custId,    //	Customer Id
                    accountNum,    //	Account Number
                    "Saving",    //	Account Type
                    payInstrumentStatus,    //	Wallet Type/Linked Bank Status* (A/M/S/D)
                    userStatus,    //	User Status*(A/M/N)
                    "",    //	MiddleName
                    "IN",    //	Nationality
                    "PAN_CARD",    //	Id Type
                    DataFactory.getRandomNumberAsString(5),    //	IMT Id No
                    "IN",    //	Issue Place
                    "IN",    //	IssuedCountryCode
                    "IN",    //	ResidencyCountryCode
                    issueDate,    //	IssuedDate(dd/MM/yyyy)
                    "FALSE",    //	IsIDExpires(TRUE/FALSE)
                    expireDate,    //	ExpireDate(dd/MM/yyyy)
                    "120010",    //	PostalCode
                    "",    //	EmployerName
                    user.KinUser.FirstName,    //	Kin First Name
                    user.KinUser.LastName,    //	Kin Last Name
                    user.KinUser.MiddleName,    //	Kin Middle Name
                    user.KinUser.RelationShip,    //	Kin Relationship
                    user.KinUser.Nationality,    //	Kin Nationality
                    user.KinUser.ContactNum,    //	Kin Contact Number
                    user.KinUser.IdNo,    //	Kin Identification Number
                    user.KinUser.DateOfBirth,    //	Kin DOB
                    user.Email,    //	Email
                    "N",    //	IMT Enable(Y/N)
                    "PAN_CARD",    //	IMT ID Type
                    "N",    //	Enable WU Services(Y/N)
                    "N",    //	Enable MoneyGram Services(Y/N)
                    "IN",    //	Birth Country
                    "IN",    //	Passport Issue Country
                    "IN",    //	Passport Issue City
                    issueDate,    //	Passport Issue Date
                    "bond",    //	Occupation
                    "Earth",    //	Birth City
                    "",//	PartnerName
                    "WEST", //	Region
                    "",//	SecondName
                    "",//	HusbandLastName
                    "",//	MaritalStatus
                    "",//	ContactPerson
                    "",//	ContactNo
                    "",//	BankCode
                    "",//	BankAccountName
                    "",//	BankAccountNumber
            };

            pNode.info("Writing Bulk subscriber  info to CSV");
            ExcelUtil.writeDataToBulkSubscriberRegistrationCsv(file, allData, isAppend);

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param file
     * @param user
     * @param providerName
     * @param payInstName
     * @param userStatus          User Status*(A/M/N)
     * @param payInstrumentStatus Wallet Type/Linked Bank Status* (A/M/S/D)
     * @param isPayInstPrimary    true/false
     * @param isAppend            true/false
     * @return
     * @throws Exception
     */
    public SubscriberManagement writeDataToBulkSubsRegistrationCsv(User user,
                                                                   String file,
                                                                   String providerName,
                                                                   String payInstName,
                                                                   String userStatus,
                                                                   String payInstrumentStatus,
                                                                   boolean isPayInstPrimary,
                                                                   boolean isAppend) throws Exception {
        Markup m = MarkupHelper.createLabel("writeDataToBulkSubsRegistrationCsv: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            String isPrimary = (isPayInstPrimary) ? "Y" : "N";
            String dateOfBirth = "28/02/1986";
            String expireDate = "28/02/2024";
            String issueDate = DataFactory.getCurrentDateSlash();

            String custId, accountNum;

            if (user.custIdOptional != null) {
                custId = user.custIdOptional;
            } else if (AppConfig.isBankAccLinkViaMsisdn) {
                custId = user.MSISDN;
            } else {
                custId = DataFactory.getRandomNumberAsString(9);
            }

            if (user.accNumOtional != null) {
                accountNum = user.accNumOtional;
            } else if (AppConfig.isBankAccLinkViaMsisdn) {
                accountNum = user.MSISDN;
            } else {
                accountNum = DataFactory.getRandomNumberAsString(9);
            }

            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, providerName, payInstName);
            MobileGroupRole mRole = user.hasPayInstTypeLinked(payInstName, providerName);
            String l_tcpID = MobiquityGUIQueries.getIDofTCPprofile(insTcp.ProfileName);

            if (user.KinUser == null) {
                user.setKinUser(new KinUser()); // assign a kin user with empty details
                user.KinUser.KinNumber = "";
                user.KinUser.DateOfBirth = "";
                user.KinUser.FirstName = "";
                user.KinUser.LastName = "";
                user.KinUser.MiddleName = "";
                user.KinUser.RelationShip = "";
                user.KinUser.ContactNum = "";
                user.KinUser.Age = "";
                user.KinUser.ContactNum = "";
                user.KinUser.Nationality = "";
            }

            String[] allData = {
                    "Mr.",//	NamePrefix (Mr./Mrs./Ms/M/S)*
                    user.FirstName,//	FirstName*
                    user.LastName,//	LastName*
                    user.MSISDN,//	MSISDN*
                    user.ExternalCode,//	Identification Number*
                    user.MSISDN,//	FormNumber
                    user.DateOfBirth,//	DateofBirth(dd/MM/yyyy)*
                    "Male",//	Gender (Male/Female)*
                    "Bulk City",//	Address
                    "CSV",//	District
                    "SomeCity",//	City*
                    "SomeState",//	State
                    "IN",//	Country
                    "",//	Description
                    "English",//	Preferred Language*
                    user.WebGroupRole,//	Web Group Role Id*
                    user.LoginId,//	Login Id*
                    "",//	Type of Identity Proof
                    "",//	Type of Address Proof
                    "",//	Type of Photo Proof
                    mRole.ProfileCode,//	Mobile Group Role Id*
                    user.GradeCode,//	Grade Code*
                    l_tcpID,//	TCP Profile Id*
                    isPrimary,//	Primary Account (Y/N)*
                    custId,//	Customer Id
                    accountNum,//	Account Number
                    "Saving",//	Account Type
                    payInstrumentStatus,//	Wallet Type/Linked Bank Status* (A/M/S/D)
                    userStatus,//	User Status*(A/M/N)
                    "",//	MiddleName
                    "PAN_CARD",//	Id Type*
                    "",//	IsIMTEnable(Y/N)
                    "",//	IMT Id Type
                    "",//	IMT Id No
                    "",//	PlaceOfIDIssued
                    "",//	IssuedCountryCode
                    "",//	ResidencyCountryCode
                    "",//	Nationality
                    "",//	IssuedDate(dd/MM/yyyy)
                    "FALSE",//	IsIDExpires(TRUE/FALSE)
                    "",//	ExpireDate(dd/MM/yyyy)
                    "",//	PostalCode
                    "",//	EmployerName
                    "",//	Occupation
                    "",//	WU Enable(Y/N)
                    "",//	MoneyGram Enable(Y/N)
                    "",//	Birth City
                    "",//	Birth Country
                    "",//	PassportIssueCountryCode
                    "",//	PassportIssueCity
                    "",//	PassportIssueDate(dd/MM/yyyy)
                    user.KinUser.FirstName,    //	Kin First Name
                    user.KinUser.LastName,    //	Kin Last Name
                    user.KinUser.MiddleName,    //	Kin Middle Name
                    user.KinUser.RelationShip,    //	Kin Relationship
                    user.KinUser.Nationality,    //	Kin Nationality
                    user.KinUser.ContactNum,    //	Kin Contact Number
                    user.KinUser.IdNo,    //	Kin Identification Number
                    user.KinUser.DateOfBirth,    //	Kin DOB
                    "",//	Region
                    "M1"//	NickName

            };

            pNode.info("Writing Bulk subscriber  info to CSV");
            ExcelUtil.writeDataToBulkSubscriberRegistrationCsv(file, allData, isAppend);

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public SubscriberManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public SubscriberManagement viewSubscriberInformation(String providerID, String payInst, String walletType, User subs) throws Exception {

        SubscriberInformation_Page1 subscriberInformationPage1 = new SubscriberInformation_Page1(pNode);
        SubscriberInformation_Page2 subscriberInformationPage2 = SubscriberInformation_Page2.init(pNode);

        subscriberInformationPage1.navigateToSubsInfo();
        subscriberInformationPage1.selectProvider(providerID);
        subscriberInformationPage1.selectPaymentInstrument(payInst);
        subscriberInformationPage1.selectWalletType(walletType);
        subscriberInformationPage1.setMSISDN(subs.MSISDN);
        subscriberInformationPage1.clickSubmit();

        return this;
    }


    public SubscriberManagement suspendOrDeleteSubsWallet(User user, String status, String walletName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("suspendOrDeleteUserWallet: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            User modifyUser = DataFactory.getChannelUserWithAccess("SUBSMOD");

            ModifySubscriber_page1 modifySubscriberPage1 = ModifySubscriber_page1.init(pNode);
            ModifySubscriber_page2 modifySubscriberPage2 = ModifySubscriber_page2.init(pNode);
            ModifySubscriber_page3 modifySubscriberPage3 = ModifySubscriber_page3.init(pNode);
            ModifySubscriber_page4 modifySubscriberPage4 = ModifySubscriber_page4.init(pNode);

            Login.init(pNode).login(modifyUser);

            modifySubscriberPage1.navSubscriberModification();
            modifySubscriberPage1.setMSISDN(user.MSISDN);
            modifySubscriberPage1.clickOnSubmitPg1();

            modifySubscriberPage2.clickOnNextPg2();

            modifySubscriberPage3.nextPage();

            modifySubscriberPage4.updateWalletStatus(status, walletName);
            if (ConfigInput.isCoreRelease) {
                modifySubscriberPage4.clickOnConfirmPg4();
            }
            modifySubscriberPage4.clickFinalConfirm();
            if (ConfigInput.isCoreRelease) {
                modifySubscriberPage4.nextFinalConfirm_click();
            }

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("subs.modify.message.initiate", modifyUser.FirstName, modifyUser.LastName);
                Assertion.verifyEqual(actual, expected, "Modify User", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;


    }

    /**
     * Add Subscriber no Approval is added
     *
     * @param user
     * @return
     * @throws Exception
     */
    public SubscriberManagement addSubscriber(User user, boolean... isBankAssoReq) throws Exception {
        boolean bankAssociation = true;
        if (isBankAssoReq.length > 0) {
            bankAssociation = isBankAssoReq[0];
        }
        addInitiateSubscriber(user);

        if (ConfigInput.isCoreRelease || ConfigInput.is4o14Release) {
            CommonUserManagement.init(pNode)
                    .assignWebGroupRole(user);
        }

        CommonUserManagement.init(pNode)
                .mapWalletPreferences(user);

        if (bankAssociation) {
            CommonUserManagement.init(pNode)
                    .mapBankPreferences(user);
        } else {
            AddChannelUser_pg5.init(pNode).clickNext(user.CategoryCode);
            AlertHandle.handleUnExpectedAlert(pNode);
            AlertHandle.handleUnExpectedAlert(pNode);
            if (ConfigInput.isAssert) {
                CommonUserManagement.init(pNode).verifyAndConfirmDetails(user, false);
            }
        }

        return this;
    }


    /**
     * rejectSubsModification
     *
     * @param user
     * @return
     * @throws Exception
     */
    public SubscriberManagement rejectSubsModification(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("rejectSubsModification : " + user.MSISDN, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifySubscriberApproval_page1 page = new ModifySubscriberApproval_page1(pNode);

            page.navigateToLink();
            page.selectUsertoApprove(user.MSISDN);
            page.setReason("Automation");
            page.clickOnRejectButton();
            page.clickFinalSubmitButton();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("subscriber.mod.message.reject", user.FirstName, user.LastName);
                Assertion.verifyEqual(actual, expected, "Modify Reject User", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    public SubscriberManagement setIsConfirmActionRequired(boolean isRequired) {
        Markup m = MarkupHelper.createLabel("setIsConfirmActionRequired", ExtentColor.YELLOW);
        pNode.info(m);
        ConfigInput.isConfirm = isRequired;
        return this;
    }

    public SubscriberManagement setIsContactPersonRequired() {
        this.isOptionalFieldRequired = true;
        return this;
    }

    public SubscriberManagement modifySubscriberToDeleteWallet(User modifyUser, User subs) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("modifySubscriberToDeleteWallet", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifySubscriber_page1 modifySubscriberPage1 = ModifySubscriber_page1.init(pNode);
            ModifySubscriber_page2 modifySubscriberPage2 = ModifySubscriber_page2.init(pNode);
            ModifySubscriber_page3 modifySubscriberPage3 = ModifySubscriber_page3.init(pNode);
            ModifySubscriber_page4 modifySubscriberPage4 = ModifySubscriber_page4.init(pNode);

            Login.init(pNode).login(modifyUser);

            modifySubscriberPage1.navSubscriberModification();
            modifySubscriberPage1.setMSISDN(subs.MSISDN);
            modifySubscriberPage1.clickOnSubmitPg1();

            modifySubscriberPage2.setNamePrefix();
            modifySubscriberPage2.setFirstnameTbox(subs.FirstName);
            modifySubscriberPage2.lastname_SetText(subs.FirstName);
            modifySubscriberPage2.externalCode_SetText(subs.ExternalCode);
            modifySubscriberPage2.clickOnNextPg2();
            modifySubscriberPage3.clickOnNextPg3();

            List<WebElement> wallets = driver.findElements(By.xpath("(//*[@id='subsRegistrationUpdateServiceBean_back2']/table[2]//tr) [position()>1]"));
            int count = wallets.size();

            pNode.info("No of Wallets Associated :: " + count);
            if (count > 0) {

                //Iterate to delete all the Non-Primary Wallets Associated with user
                for (int i = count - 1; i >= 0; i--) {

                    String isNonPrimary = driver.findElement(By.xpath("//*[@id='walletTypeID" + i + "']/option[@selected='selected']")).getText();

                    if (!isNonPrimary.equalsIgnoreCase(Constants.NORMAL_WALLET_STRING)) {

                        Select deleteNonPrimaryWallet = new Select(driver.findElement(By
                                .id("subsRegistrationUpdateServiceBean_back2_counterList_" + i + "__statusSelected")));

                        deleteNonPrimaryWallet.selectByVisibleText("Deleted");
                    }
                }
            }

            modifySubscriberPage4.clickSubmitPg4();
            modifySubscriberPage4.clickOnConfirmPg4();
            modifySubscriberPage4.clickFinalConfirm();
            Utils.putThreadSleep(3000);

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("subs.modify.message.initiateBy", modifyUser.FirstName, modifyUser.LastName);
                Assertion.verifyEqual(actual, expected, "Modify User", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    private void enterThirdPartyRequiredFields(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("enterThirdPartyRequiredFields", ExtentColor.BLUE);
        pNode.info(m);
        AddSubscriber_Page1 page1 = new AddSubscriber_Page1(pNode);
        if (AppConfig.isImtSendMoneyEnabled) {

            try {
                if (!ConfigInput.isCoreRelease) {
                    page1.setIsIMTEnabled(user.isIMTEnabled);
                }

                page1.selectIMTIdType();
                page1.setImtIdNumber(user.ExternalCode);
                page1.selectBirthCountry();
                page1.selectPassportIssueCountry();
                page1.selectPassportIssueCity("Bangalore");
                page1.setOccupation("QA");
                page1.selectNationality();
                page1.selectIssueCountry();
                page1.setPlaceOfIssueId("Banglore");
                page1.selectResidenceCountry();

                page1.setPassportIssueDate(new DateAndTime().getDate(-700));
                // todo , below is a work around, might need to refactor in future
                page1.setIssueDate(new DateAndTime().getDate(-9));
                if (user.isIdExpires()) {
                    page1.checkIsIDExpire(true);
                    page1.setExpiryDate(new DateAndTime().getDate(700));
                } else {
                    page1.checkIsIDExpire(false);
                }

                page1.setPostalCode("560024");
                page1.setEmployerName("Comviva");

                if (ConfigInput.isCoreRelease) {
                    page1.setBirthPlace(user.City);
                }
            } catch (Exception e) {
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        }

        // page1.expiryCheckBox();
        /*page1.setPassportIssueDate(new DateAndTime().getDate(-700));
        page1.setIssueDate(new DateAndTime().getDate(0));
        page1.setExpiryDate(new DateAndTime().getDate(700));*/
    }

    private void enterThirdPartyRequiredDetailsOnModification(User user) throws Exception {

        Markup m = MarkupHelper.createLabel("enterImtDetailsOnModification", ExtentColor.BLUE);
        pNode.info(m);

        if (AppConfig.isImtSendMoneyEnabled) {
            AddSubscriber_Page1 page1 = new AddSubscriber_Page1(pNode);
            ModifySubscriber_page2 page2 = new ModifySubscriber_page2(pNode);
            try {
                if (!ConfigInput.isCoreRelease) {
                    page1.setIsIMTEnabled(user.isIMTEnabled);
                }

                if (user.isIMTEnabled) {
                    page2.selectImtIdType();
                    page2.setImtIdNumber(user.ExternalCode);
                    page1.selectThirdPartyOption(user.thirdParties);
                    page2.selectBirthCountry();
                    page2.selectPassportCountry();
                    page2.setPassportIssueCity();
                    page2.setOccupation();
                    page2.selectNationality();
                    page2.selectIdIssueCountry();
                    page2.setIdIssuePlace();
                    page2.selectResidencyCountry();
                    page2.unCheckIdExpire();
                    if (!ConfigInput.isCoreRelease) {
                        page1.setIssueDate(new DateAndTime().getDate(-10));
                        page1.setExpiryDate(new DateAndTime().getDate(180));
                    } else {
                        page1.setPassportIssueDate(new DateAndTime().getDate(-700));
                        page2.setIdIssueDate(new DateAndTime().getDate(-700));
                    }
                    page2.setPostalCode();
                    page2.setEmployeeName();

                    if (ConfigInput.isCoreRelease) {
                        page1.setBirthPlace(user.City);
                    }
                }
            } catch (Exception e) {
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        }
    }

    public SubscriberManagement navigateToModifySubscriberWalletAssociationPage(User subs) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("navigateToModifySubscriberWalletAssociationPage", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            ModifySubscriber_page1 modifySubscriberPage1 = ModifySubscriber_page1.init(pNode);
            ModifySubscriber_page2 modifySubscriberPage2 = ModifySubscriber_page2.init(pNode);
            ModifySubscriber_page3 modifySubscriberPage3 = ModifySubscriber_page3.init(pNode);

            Login.init(pNode).login(usrModify);

            modifySubscriberPage1.navSubscriberModification();
            modifySubscriberPage1.setMSISDN(subs.MSISDN);
            modifySubscriberPage1.clickOnSubmitPg1();
            modifySubscriberPage1.selectPrefix();
            modifySubscriberPage1.selectIdType();
            modifySubscriberPage1.selectIdentityProof();
            modifySubscriberPage1.selectAddressProof();
            modifySubscriberPage1.selectPhotoProof();
            modifySubscriberPage1.uploadIdentityProof(subs.IDProof);
            modifySubscriberPage1.uploadAddressProof(subs.AddressProof);
            modifySubscriberPage1.uploadPhotoProof(subs.PhotoProof);
            modifySubscriberPage2.clickOnNextPg2();

            if (AppConfig.isSubsLoginAllowed) {
                modifySubscriberPage3.selectWebGroupRole(subs.WebGroupRole);
                modifySubscriberPage3.clickOnNextPg3();
            }
            Utils.putThreadSleep(1000);

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;


    }

    public SubscriberManagement modifySubscriberExistingBankStatus(String status, String accountNumber) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("modifySubscriberExistingBankStatus ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            ModifySubscriber_page4 page4 = new ModifySubscriber_page4(pNode);

            page4.modifyStatusOfBank(status, accountNumber);
            page4.clickOnConfirmPg4();
            page4.clickFinalConfirm();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("subs.modify.message.initiateBy", usrModify.FirstName, usrModify.LastName);
                Assertion.verifyEqual(actual, expected, "Modify User", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

}
