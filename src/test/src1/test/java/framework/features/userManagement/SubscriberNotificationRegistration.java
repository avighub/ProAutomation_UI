package framework.features.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;

/**
 * Created by navin.pramanik on 8/31/2017.
 */
public class SubscriberNotificationRegistration {

    private static WebDriver driver;
    private static ExtentTest pNode;

    public static SubscriberNotificationRegistration init(ExtentTest t1) {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new SubscriberNotificationRegistration();
    }


}

