package framework.features.userManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.GradeDB;
import framework.entity.*;
import framework.features.billerManagement.BillerManagement;
import framework.features.blackListManagement.BlackListManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.accessManagement.ResetPassword_Page;
import framework.pageObjects.billerManagement.BillerApproveModify_pg1;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page1;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ApprovalSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.pageObjects.userManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import tests.core.CR_B9.Suite_v5_ChurnRecord;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CommonUserManagement {
    private static WebDriver driver;
    private static ExtentTest pNode;
    private static OperatorUser barUser, optChurnInit, optChurnApprove;

    public static CommonUserManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        if (barUser == null) {
            barUser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");
            optChurnInit = DataFactory.getOperatorUserWithAccess("CHURNMGMT_MAIN", Constants.CHANNEL_ADMIN);
            optChurnApprove = DataFactory.getOperatorUserWithAccess("CHURN_APPROVE", Constants.CHANNEL_ADMIN);

        }

        return new CommonUserManagement();
    }

    public static CommonUserManagement baseInit(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new CommonUserManagement();
    }

    /**
     * Verify Wallet Page Details
     *
     * @param user
     * @return
     * @throws Exception
     */
    public static void verifyWalletPageDetails(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyWalletPageDetails", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            //now iterating the table for reading the contents
            WebElement subscriberTable = driver.findElement(By.className("wwFormTableC"));
            List<WebElement> tableRows = subscriberTable.findElements(By.tagName("tr"));
            int column = 0;
            int count = 0;
            boolean isValid = true;
            List<String> walletTypes = DataFactory.getWalletForChannelUser();

            outerloop:
            for (int i = 0; i < tableRows.size(); i++) //this for loop increments each of these tr's
            {
                if (i == 0)
                    continue;
                List<WebElement> rowTDs = tableRows.get(i).findElements(By.tagName("td"));//this gets every td in the current tr and puts it into a list
                for (WebElement singleTD : rowTDs) //this increments through that list of td's
                {
                    //increment the column counter
                    if (column == 1) {
                        Select select = new Select(singleTD.findElement(By.tagName("select")));
                        if (!walletTypes.contains(select.getFirstSelectedOption().getText())) {
                            isValid = false;
                            break outerloop;
                        }
                    } else if (column >= 1) {
                        continue;
                    }
                    column++;
                    count++;
                }
                column = 0; //reset the column
            }

            Assertion.verifyEqual(isValid, true, "Wallets verified for channel user", pNode, true);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    /**
     * @param user
     * @throws Exception
     */

    public static void verifyBankPageDetails(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyBankPageDetails", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            //now iterating the table for reading the contents
            WebElement subscriberTable = driver.findElement(By.className("wwFormTableC"));
            List<WebElement> tableRows = subscriberTable.findElements(By.tagName("tr"));
            int column = 0;
            int count = 0;
            boolean isValid = true;
            List<String> bankTypes = DataFactory.getAllBank();

            outerloop:
            for (int i = 0; i < tableRows.size(); i++) //this for loop increments each of these tr's
            {
                if (i == 0)
                    continue;
                List<WebElement> rowTDs = tableRows.get(i).findElements(By.tagName("td"));//this gets every td in the current tr and puts it into a list
                for (WebElement singleTD : rowTDs) //this increments through that list of td's
                {
                    //increment the column counter
                    if (column == 1) {
                        Select select = new Select(singleTD.findElement(By.tagName("select")));
                        if (!bankTypes.contains(select.getFirstSelectedOption().getText())) {
                            isValid = false;
                            break outerloop;
                        }
                    } else if (column >= 1) {
                        continue;
                    }
                    column++;
                    count++;
                }
                column = 0; //reset the column
            }

            Assertion.verifyEqual(isValid, true, "Banks verified for channel user", pNode);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    /**
     * Assign Web Group Role
     *
     * @param user
     */
    public CommonUserManagement assignWebGroupRole(User user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("assignWebGroupRole: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddChannelUser_pg3 groupRole = AddChannelUser_pg3.init(pNode);
            groupRole.selectWebGroupRole(user.WebGroupRole);

            //For the Enterprise logo need to be uploaded
            if (user.CategoryCode.equals("Enterprise")) {
                groupRole.uploadLogo();
            }

            if (user.CategoryCode.equals("SUBS")) {
                groupRole.clickNextSUBS();
            } else {
                groupRole.clickNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * navigate and select Web Role
     *
     * @param user
     * @return
     * @throws IOException
     */
    public CommonUserManagement navigateAndSelectWebGroupRole(User user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("Navigate And Select Web Group Role: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();
            if (ConfigInput.isAssert) {
                //TODO - check next page is open
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * MAP Default Wallet Preferences i.e. just the Default Provider and Default Wallet type
     *
     * @param user
     * @return
     * @throws Exception
     */
    public CommonUserManagement mapDefaultWalletPreferences(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("mapDefaultWalletPreferences: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(pNode);

            String dProvider = DataFactory.getDefaultProvider().ProviderName;
            String dWallet = DataFactory.getDefaultWallet().WalletName;
            // get instrument TCP
            InstrumentTCP insTcp;
            try {
                insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, dProvider, dWallet);
            } catch (NullPointerException ne) {
                insTcp = null;
            }

            Select selProviderUI = new Select(driver.findElement(By.name("counterList[0].providerSelected")));
            Select selPaymentTypeUI = new Select(driver.findElement(By.name("counterList[0].paymentTypeSelected")));
            Select selGrade = new Select(driver.findElement(By.name("counterList[0].channelGradeSelected")));

            selProviderUI.selectByVisibleText(dProvider);
            pNode.info("Select Default Provider - " + dProvider);
            Thread.sleep(Constants.MAX_WAIT_TIME);
            selPaymentTypeUI.selectByVisibleText(dWallet);
            pNode.info("Select Default Wallet - " + dWallet);
            Thread.sleep(Constants.MAX_WAIT_TIME);
            selGrade.selectByVisibleText(user.GradeName);
            pNode.info("Select User Grade Wallet - " + user.GradeName);
            Thread.sleep(Constants.MAX_WAIT_TIME);

            WebElement tcpSelect = driver.findElement(By.name("counterList[0].tcpSelected"));
            Select tcp = new Select(tcpSelect);
            if (insTcp != null) {
                tcp.selectByVisibleText(insTcp.ProfileName);
                pNode.info("Select Instrument TCP - " + insTcp.ProfileName);
            } else {
                if (tcp.getOptions().size() > 1) {
                    tcp.selectByIndex(1);
                } else {
                    tcp.selectByIndex(0);
                }
            }

            Thread.sleep(Constants.MAX_WAIT_TIME);

            page4.clickNext(user.CategoryCode);
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Map Wallet Preferences
     *
     * @param user
     * @throws Exception
     */
    public CommonUserManagement mapWalletPreferences(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("mapWalletPreferences: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            WebDriver driver = DriverFactory.getDriver();
            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(pNode);
            /*
             * Provide and Wallet List
             */
            List<String> providerList = DataFactory.getAllProviderNames();

            List<String> paymentTypeList;
            if (user.CategoryCode.equals(Constants.SUBSCRIBER)) {
                paymentTypeList = DataFactory.getWalletForSubscriberUserCreation();
            } else {
                paymentTypeList = DataFactory.getWalletForChannelUserCreation();
            }
            /*
             * Map all the provides and wallets
             */
            int counter = 0;
            for (String provider : providerList) {
                boolean isPrimary = true;
                boolean isSkipped = false; // select just one primary account per currency provider
                for (String paymentType : paymentTypeList) {

                    if (counter != 0 && !isSkipped)
                        page4.clickAddMore(user.CategoryCode);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].providerSelected"))).selectByVisibleText(provider);
                    pNode.info("Selected Provider: " + provider);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].paymentTypeSelected"))).selectByVisibleText(paymentType);
                    pNode.info("Selected Payment Type: " + paymentType);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].channelGradeSelected"))).selectByVisibleText(user.GradeName);
                    pNode.info("Select Grade: " + user.GradeName);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    /*
                    If a specific Mobile role is associated with the User Specific Wallet, select that Mobile Role.
                     */
                    String roleName = user.hasPayInstTypeLinked(paymentType, provider).ProfileName;

                    new Select(driver.findElement(By.name("counterList[" + counter + "].groupRoleSelected"))).selectByVisibleText(roleName);
                    pNode.info("Select Mobile Role: " + roleName);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    // get instrument TCP
                    InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, provider, paymentType);
                    if (insTcp != null) {
                        new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected"))).selectByVisibleText(insTcp.ProfileName);
                        pNode.info("Select Instrument TCP: " + insTcp.ProfileName);
                        Thread.sleep(Constants.MAX_WAIT_TIME);
                    } else {
                        Select sel = new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected")));
                        if (sel.getOptions().size() > 0)
                            sel.selectByIndex(0);
                        else
                            sel.selectByIndex(1);
                        pNode.info("Select Instrument TCP By Index");
                        Thread.sleep(Constants.MAX_WAIT_TIME);
                    }

                    /*
                     * If the wallet type is not applicable then skip the same!
                     */
                    Select selPrimaryAccount = new Select(driver.findElement(By.name("counterList[" + counter + "].primaryAccountSelected")));

                    if (isPrimary && DataFactory.getDefaultWallet().WalletName.equals(paymentType)) {
                        selPrimaryAccount.selectByIndex(0);
                        isPrimary = false;
                    } else {
                        selPrimaryAccount.selectByIndex(1);
                    }
                    counter++;
                    isSkipped = false;
                }
            }
            page4.clickNext(user.CategoryCode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * Map Bank Preferences
     *
     * @param user
     * @throws Exception
     */
    public CommonUserManagement mapBankPreferences(User user, boolean... isModifyLiquidationData) throws Exception {
        boolean isModified = isModifyLiquidationData.length > 0 ? isModifyLiquidationData[0] : false;
        try {
            Markup m = MarkupHelper.createLabel("mapBankPreferences: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);
            if (isModified) {
                CommonChannelUserPage.init(pNode).clickAddMoreBank();
            }

            page5.addBankPreferences(user);

            // add liquidation bank , if user category is not Subscriber
            if (!user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                page5.addLiquidationBankPreferences(user, isModified);
            }

            // navigate to the next page
            if (ConfigInput.isConfirm) {
                page5.clickNext(user.CategoryCode);
            }

            /*
             * Complete User Creation
             */
            if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                verifyAndConfirmDetails(user, false);
            } else {
                page5.completeUserCreation(user, isModified);
                Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

                if (!ConfigInput.isAssert)
                    return this;

                if (!isModified)
                    Assertion.verifyActionMessageContain("channeluser.add.approval", "Map Bank Preferences", pNode);
                else {
                    Assertion.verifyActionMessageContain("channeluser.modify.approval", "Modified User Information ", pNode, user.FirstName, user.LastName);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    public CommonUserManagement approveAllAssociatedLiquidationBanksForBiller(Biller biller) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveAllAssociatedLiquidationBanks: " + biller.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddModifyApproveBank pageOne = AddModifyApproveBank.init(pNode);
            pageOne.navAddModifyDeleteBank();

            List<String> providerNames = DataFactory.getAllProviderNames();
            for (String provider : providerNames) {
                pageOne.selectProvider(provider);
                List<String> linkedBanks = DataFactory.getAllLiquidationBanksLinkedToProvider(provider);
                for (String bank : linkedBanks) {
                    pageOne.navAddModifyDeleteBank();
                    pageOne.selectProvider(provider);
                    pageOne.selectBank(bank);
                    pageOne.setFromDate();
                    pageOne.setToDate();
                    pageOne.clickSubmit();
                    pageOne.checkBanksForUser(biller.LoginId);
                    pageOne.approve();

                    if (ConfigInput.isAssert) {
                        //TODO - Specific Assertion is required
                        Assertion.assertErrorInPage(pNode);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * Approve Channel User
     *
     * @param user - user object of user that need to be approved
     * @throws Exception
     * @Optional approve - Pass true if the Approval need to be Approved, false to reject an approval
     */
    public CommonUserManagement addInitiatedApproval(User user, boolean isApprove) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addInitiatedApproval: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navigateToChannelUserApproval();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforApproval(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitApprovalDetail();
            if (ConfigInput.isConfirm) {
                if (isApprove) {
                    Thread.sleep(8000);
                    page.confirmApproval();
                    if (Assertion.verifyActionMessageContain("channeluser.message.approval", "Add Initiate Approve, " + user.LoginId, pNode))
                        user.isApproved = "Y";

                    String actualMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "DESC");
                    DBAssertion.verifyDBMessageContains(actualMessage, "00328", "Verify Channel User Creation Message", pNode, user.LoginId);
                } else {
                    page.rejectApproval();
                    Assertion.verifyActionMessageContain("channeluser.message.reject", "Add Initiate Reject, " + user.LoginId, pNode, user.FirstName, user.LastName);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Change First Time Password
     *
     * @param user
     */
    public CommonUserManagement changeFirstTimePassword(User user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("changeFirstTimePassword: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ChangePasswordPage changPwd = ChangePasswordPage.init(pNode);
            FunctionLibrary fl = new FunctionLibrary(driver);

            String oldPassword = null;
            if (AppConfig.isRandomPasswordAllowed) {
                oldPassword = MobiquityGUIQueries.dbFetchPassWordFromEmailQueue(user.MSISDN);
                if (ConfigInput.isCoreRelease) {
                    oldPassword = Utils.decryptMessage(oldPassword);
                }

            } else {
                oldPassword = ConfigInput.userCreationPassword;
            }

            Login.init(pNode).login(user.LoginId, oldPassword);
            fl.contentFrame();

            changPwd.setOldPassword(oldPassword);
            changPwd.setNewPassword(user.Password);
            changPwd.setConfirmPassword(user.Password);
            Thread.sleep(Constants.TWO_SECONDS);
            Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
            changPwd.clickSubmit();
            Thread.sleep(Constants.TWO_SECONDS);
            Assertion.verifyActionMessageContain("changePassword.label.success",
                    "Verify first time Password is successfully changed", pNode);

            if (driver.findElements(By.id("skip_for_later")).size() > 0) {
                changPwd.clickSkip();
                driver.switchTo().alert().accept();
            }
            user.setIsCreated();
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public CommonUserManagement changeAdminFirstTimePassword(String username) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("changeAdminFirstTimePassword: " + ConfigInput.superadminUserId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ChangePasswordPage changPwd = ChangePasswordPage.init(pNode);
            FunctionLibrary fl = new FunctionLibrary(driver);

            Login.init(pNode).login(username, ConfigInput.superadminSchemaPass);
            fl.contentFrame();

            changPwd.setOldPassword(ConfigInput.superadminSchemaPass);
            changPwd.setNewPassword(ConfigInput.superadminPass);
            changPwd.setConfirmPassword(ConfigInput.superadminPass);
            Thread.sleep(3000);
            changPwd.clickSubmit();

            Assertion.verifyActionMessageContain("changePassword.label.success",
                    "Verify first time Password is successfully changed", pNode);

            if (driver.findElements(By.id("skip_for_later")).size() > 0) {
                changPwd.clickSkip();
                driver.switchTo().alert().accept();
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public CommonUserManagement changeFirstTimePasswordwithRandomPassword(User user) throws Throwable {
        try {
            Markup m = MarkupHelper.createLabel("changeFirstTimePasswordwithRandomPassword: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ChangePasswordPage changPwd = ChangePasswordPage.init(pNode);
            FunctionLibrary fl = new FunctionLibrary(driver);

            MobiquityGUIQueries m1 = new MobiquityGUIQueries();

            String randompassword = m1.dbFetchPassWordFromEmailQueue(user.MSISDN);

            DesEncryptor d = new DesEncryptor();
            String decryptedPassword = d.decrypt(randompassword);

            Login.init(pNode).login(user.LoginId, decryptedPassword);
            fl.contentFrame();

            changPwd.setOldPassword(decryptedPassword);
            changPwd.setNewPassword(user.Password);
            changPwd.setConfirmPassword(user.Password);
            Thread.sleep(3000);
            changPwd.clickSubmit();

            Assertion.verifyActionMessageContain("changePassword.label.success",
                    "Verify first time Password is successfully changed", pNode);
            if (driver.findElements(By.id("skip_for_later")).size() > 0) {
                changPwd.clickSkip();
                driver.switchTo().alert().accept();
            }
            user.setIsCreated();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Approve all the associated Banks to the Channel User
     *
     * @param user
     * @return
     */
    public CommonUserManagement approveAllAssociatedBanks(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveAllAssociatedBanks: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddModifyApproveBank pageOne = AddModifyApproveBank.init(pNode);
            pageOne.navAddModifyDeleteBank();

            List<String> providerNames = DataFactory.getAllProviderNames();
            for (String provider : providerNames) {
                pageOne.selectProvider(provider);

                for (String bankName : user.linkedBanks) {
                    Bank bank = DataFactory.getBankFromAppData(bankName);

                    if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER) && bank.IsLiquidation)
                        continue;

                    pageOne.navAddModifyDeleteBank();
                    pageOne.selectProvider(provider);
                    pageOne.selectBank(bank.BankName);

                    pageOne.setFromDate();
                    pageOne.setToDate();
                    pageOne.clickSubmit();

                    if (!pageOne.checkBanksForUser(user.MSISDN)) {
                        pNode.info("No banks are available for approval");
                        return this;
                    }

                    pageOne.approve();
                    if (ConfigInput.isAssert) {
                        Assertion.verifyActionMessageContain("channeluser.bankapproved", "Verify Bank association", pNode,
                                provider, user.MSISDN, bank.BankName);
                    }
                }
            }
        } catch (
                Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Approve just the Default Banks Associated with a Channel User
     *
     * @param user
     * @return
     */
    public CommonUserManagement approveDefaultBank(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveDefaultBank: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddModifyApproveBank pageOne = AddModifyApproveBank.init(pNode);
            pageOne.navAddModifyDeleteBank();
            pageOne.selectProvider(); // Select Provide as all

            try {
                pageOne.selectBank(DataFactory.getDefaultBankNameForDefaultProvider());
            } catch (Exception e) {
                if (e.getMessage().contains("element is not attached")) {
                    Thread.sleep(3000);
                    pageOne.selectBank(DataFactory.getDefaultBankNameForDefaultProvider());
                }
            }

            pageOne.setFromDate();
            pageOne.setToDate();
            pageOne.clickSubmit();
            pageOne.checkBanksForUser(user.MSISDN);
            pageOne.approve();
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Verify Subscriber Information and confirm user creation/approval view verification
     *
     * @param user
     * @param isConfirm
     * @throws Exception
     */
    public void verifyAndConfirmDetails(User user, boolean isConfirm) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyAndConfirmDetails", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            ApprovalSubscriber_Page1 confirmPage = ApprovalSubscriber_Page1.init(pNode);

            Assertion.verifyEqual(confirmPage.getFirstName(isConfirm), user.FirstName, "Assert First Name", pNode);
            Assertion.verifyEqual(confirmPage.getLastName(isConfirm), user.LastName, "Assert Last Name", pNode);
            Assertion.verifyEqual(confirmPage.getMobileNumber(isConfirm), user.MSISDN, "Assert Mobile No.", pNode);
            Assertion.verifyContains(confirmPage.getIdentificationNumber(isConfirm), user.ExternalCode, "Assert Identification Name", pNode);

            if (isConfirm) {
                confirmPage.clickOnCloseButton();
            } else {
                confirmPage.clickOnConfirmClose();
                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("subs.addinitiate.message.success", "Add Subscriber", pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Navigate to Modify Bank Mapping Page
     *
     * @return
     * @throws IOException
     */
    public CommonUserManagement navModifyToBankMapping(User user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("navModifyToBankMapping: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();

            if (user.CategoryCode.equals("Enterprise")) {
                page.clickNextEnterprisePreference();
                page.clickNextWalletCombination();
                page.clickNextWalletMap();
                page.clickNextEnterprisePreferenceApproval();
            } else {
                page.clickNextWalletMap();
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * Reset User Password through SuperAdmin
     *
     * @param user
     * @throws Exception
     */
    public void resetPasswordAsSuperAdmin(Object user) throws Exception {

        String userType = null, loginID = null;
        if (user instanceof User) {
            loginID = ((User) user).LoginId;
            userType = "CH_USERS";
        } else if (user instanceof OperatorUser) {
            loginID = ((OperatorUser) user).LoginId;
            userType = "OPT_USERS";
        }

        Markup m = MarkupHelper.createLabel("resetPasswordAsSuperAdmin: " + loginID, ExtentColor.BLUE);
        pNode.info(m);

        try {
            //Login SuperAdmin
            Login.init(pNode)
                    .loginAsSuperAdmin("SR_PASS");

            ResetPassword_Page resetPasswordPage1 = ResetPassword_Page.init(pNode);

            resetPasswordPage1.navResetPasswordLink();
            resetPasswordPage1.setUserTypeByValue(userType);
            resetPasswordPage1.setLoginId(loginID);
            resetPasswordPage1.clickSubmit();
            resetPasswordPage1.clickReset();

            if (ConfigInput.isAssert) {
                if (userType.equalsIgnoreCase("CH_USERS")) {
                    Assertion.verifyActionMessageContain("sendReset.label.pwdResetSend", "Password Reset for " + loginID, pNode, ((User) user).FirstName, ((User) user).LastName);
                } else {
                    Assertion.verifyActionMessageContain("sendReset.label.pwdResetSend", "Password Reset for " + loginID, pNode, ((OperatorUser) user).FirstName, ((OperatorUser) user).LastName);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    public void resetPasswordAsNetworkAdmin(Object user) throws Exception {

        String userType = null, loginID = null;
        if (user instanceof User) {
            loginID = ((User) user).LoginId;
            userType = "CH_USERS";
        } else if (user instanceof OperatorUser) {
            loginID = ((OperatorUser) user).LoginId;
            userType = "OPT_USERS";
        }

        OperatorUser net = DataFactory.getOperatorUserWithAccess("SR_PASS");

        Markup m = MarkupHelper.createLabel("resetPasswordAsNetworkAdmin for User: " + loginID, ExtentColor.BLUE);
        pNode.info(m);

        try {
            //Login NetworkAdmin
            Login.init(pNode)
                    .login(net);

            ResetPassword_Page resetPasswordPage1 = ResetPassword_Page.init(pNode);

            resetPasswordPage1.navResetPasswordLink();
            resetPasswordPage1.setUserTypeByValue(userType);
            resetPasswordPage1.setLoginId(loginID);
            resetPasswordPage1.clickSubmit();
            resetPasswordPage1.clickReset();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("sendReset.label.pwdResetSend", "Password Reset for " + loginID, pNode, ((User) user).FirstName, ((User) user).LastName);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }


    /**
     * Reset User Password through SuperAdmin
     * Change password to the value set for ConfigInput.defaultPass
     *
     * @param user
     * @throws Exception
     */
    public boolean resetPasswordForMigratedUser(Object user) throws Exception, MoneyException {

        String userType = null, loginID = null, defaultPassword = null, msisdn = null;
        if (user instanceof User) {
            loginID = ((User) user).LoginId;
            msisdn = ((User) user).MSISDN;
            userType = "CH_USERS";
        } else if (user instanceof OperatorUser) {
            loginID = ((OperatorUser) user).LoginId;
            msisdn = ((OperatorUser) user).MSISDN;
            userType = "OPT_USERS";
        } else if (user instanceof PseudoUser) {
            loginID = ((PseudoUser) user).LoginId;
            msisdn = ((PseudoUser) user).MSISDN;
            userType = "CH_USERS_P";
        }

        Markup m = MarkupHelper.createLabel("resetPasswordAsSuperAdmin for User: " + loginID, ExtentColor.BLUE);
        pNode.info(m);

        /*
        Check if user passsword is already equals to ConfigInput.defaultPass
         */
        if (Login.init(pNode).tryLogin(loginID, ConfigInput.defaultPass)) {
            pNode.pass(loginID + "'s password is already set to " + ConfigInput.defaultPass);
            return true;
        }

        try {
            //Login SuperAdmin
            Login.init(pNode)
                    .loginAsSuperAdmin("SR_PASS");

            ResetPassword_Page resetPasswordPage1 = ResetPassword_Page.init(pNode);

            resetPasswordPage1.navResetPasswordLink();
            resetPasswordPage1.setUserTypeByValue(userType);
            resetPasswordPage1.setLoginId(loginID);
            resetPasswordPage1.clickSubmit();
            resetPasswordPage1.clickReset();

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("sendReset.label.pwdResetSend", "Password Reset for " + loginID, pNode);
            }

            // IS_RANDOM_PASS_ALLOW true then password will be sent to notification
            if (AppConfig.isRandomPasswordAllowed) {
                defaultPassword = CommonUserManagement.init(pNode).getResetPasswordFromEmailQueue(msisdn);
            } else {
                defaultPassword = AppConfig.defaultUsrPwd;
            }

            Login.init(pNode).tryLogin(loginID, defaultPassword);

            ChangePasswordPage page = ChangePasswordPage.init(pNode);
            page.setOldPassword(defaultPassword)
                    .setNewPassword(ConfigInput.defaultPass)
                    .setConfirmPassword(ConfigInput.defaultPass)
                    .clickSubmit();
            Login.resetLoginStatus();
            return Assertion.verifyActionMessageContain("changePassword.label.success", "Update Password for User: " + loginID, pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return false;
    }

    /**
     * Initiate Providers Verification
     *
     * @return
     * @throws Exception
     */
    public void verifyProvidersDetails() throws Exception {
        Markup m = MarkupHelper.createLabel("verifyProvidersDetails", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            int count = 0;
            boolean isValid = true;
            List<String> providerTypes = DataFactory.getAllProviderNames();

            outerloop:
            for (int i = 0; i < providerTypes.size(); i++) {   //first checking whether the provider element exists or not in UI
                if (driver.findElements(By.xpath("//*[@id=\"+i+\"]")).size() != 0) {
                    Select providerDropdown = new Select(driver.findElement(By.xpath("//*[@id=\"+i+\"]")));
                    WebElement providerName = providerDropdown.getFirstSelectedOption();
                    if (!providerTypes.contains(providerName.getText())) {
                        isValid = false;
                        break outerloop;
                    }
                }
            }
            if (isValid) {
                pNode.pass("Providers verified for channel user");
            } else {
                pNode.fail("Providers coming wrong for channel user");
            }
            Utils.captureScreen(pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /*      Multiple Wallets having Multiple Primary Account
     * */
    public CommonUserManagement MultipleWalletWitOnePrimaryAccnt(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("MultipleWalletWitOnePrimaryAccnt: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(pNode);
            /*
             * Provide and Wallet List
             */
            List<String> providerList = DataFactory.getAllProviderNames();
            List<String> paymentTypeList;
            if (user.CategoryCode.equals(Constants.SUBSCRIBER)) {
                paymentTypeList = DataFactory.getWalletForSubscriberUserCreation();
            } else {
                paymentTypeList = DataFactory.getWalletForChannelUserCreation();
            }
            /*
             * Map all the provides and wallets
             */
            int counter = 0;
            for (String provider : providerList) {
                boolean isPrimary = true;
                boolean isSkipped = false; // select just one primary account per currency provider
                for (String paymentType : paymentTypeList) {

                    if (counter != 0 && !isSkipped) {
                        pNode.info("Clicked on Add More!");
                        try {
                            page4.clickAddMore(user.CategoryCode);
                        } catch (Exception e) {
                            if (e.getMessage().contains("element is not attached")) {
                                Thread.sleep(Constants.TWO_SECONDS);
                                page4.clickAddMore(user.CategoryCode);
                            }
                        }
                    }
                    // get instrument TCP
                    InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, provider, paymentType);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].providerSelected"))).selectByVisibleText(provider);
                    pNode.info("Selected Provider:" + provider);
                    Thread.sleep(1000);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].paymentTypeSelected"))).selectByVisibleText(paymentType);
                    pNode.info("Selected Payment Type:" + paymentType);
                    Thread.sleep(2000);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].channelGradeSelected"))).selectByVisibleText(user.GradeName);
                    pNode.info("Select Grade:" + user.GradeName);
                    Thread.sleep(2500);

                    if (insTcp != null) {
                        new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected"))).selectByVisibleText(insTcp.ProfileName);
                        pNode.info("Select Instrument TCP - " + insTcp.ProfileName);
                        Thread.sleep(1500);
                    } else {
                        Select sel = new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected")));
                        if (sel.getOptions().size() > 0)
                            sel.selectByIndex(0);
                        else
                            sel.selectByIndex(1);
                        pNode.info("Select Instrument TCP By Index- ");
                        Thread.sleep(1500);
                    }

                    /*
                     * If the wallet type is not applicable then skip the same!
                     */
                    Select selPrimaryAccount = new Select(driver.findElement(By.name("counterList[" + counter + "].primaryAccountSelected")));

                    if (isPrimary && DataFactory.getDefaultWallet().WalletName.equals(paymentType)) {
                        selPrimaryAccount.selectByIndex(0);
                        isPrimary = false;
                    } else {
                        selPrimaryAccount.selectByIndex(1);
                    }
                    counter++;
                    isSkipped = false;
                }
                // to add multiple wallets with one primary Account

                page4.btnAddMore();
                pNode.info("Clicked on Add More!");

                new Select(driver.findElement(By.id("walletTypeID1"))).selectByIndex(1);
                Thread.sleep(1000);

                new Select(driver.findElement(By.id("grade1"))).selectByVisibleText(user.GradeName);
                pNode.info("Select Grade:" + user.GradeName);
                Thread.sleep(2500);


                new Select(driver.findElement(By.id("primaryAc1"))).selectByIndex(1);
            }
            page4.clickNext(user.CategoryCode);
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public CommonUserManagement mulWalletWitMulPrimaryAcc(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("mulWalletWitMulPrimaryAcc: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(pNode);
            /*
             * Provide and Wallet List
             */
            List<String> providerList = DataFactory.getAllProviderNames();
            List<String> paymentTypeList;
            if (user.CategoryCode.equals(Constants.SUBSCRIBER)) {
                paymentTypeList = DataFactory.getWalletForSubscriberUserCreation();
            } else {
                paymentTypeList = DataFactory.getWalletForChannelUserCreation();
            }
            /*
             * Map all the provides and wallets
             */
            int counter = 0;
            for (String provider : providerList) {
                boolean isPrimary = true;
                boolean isSkipped = false; // select just one primary account per currency provider
                for (String paymentType : paymentTypeList) {

                    if (counter != 0 && !isSkipped) {
                        pNode.info("Clicked on Add More!");
                        try {
                            page4.clickAddMore(user.CategoryCode);
                        } catch (Exception e) {
                            if (e.getMessage().contains("element is not attached")) {
                                Thread.sleep(Constants.TWO_SECONDS);
                                page4.clickAddMore(user.CategoryCode);
                            }
                        }
                    }
                    // get instrument TCP
                    InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, provider, paymentType);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].providerSelected"))).selectByVisibleText(provider);
                    pNode.info("Selected Provider:" + provider);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].paymentTypeSelected"))).selectByVisibleText(paymentType);
                    pNode.info("Selected Payment Type:" + paymentType);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].channelGradeSelected"))).selectByVisibleText(user.GradeName);
                    pNode.info("Select Grade:" + user.GradeName);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    if (insTcp != null) {
                        new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected"))).selectByVisibleText(insTcp.ProfileName);
                        pNode.info("Select Instrument TCP - " + insTcp.ProfileName);
                        Thread.sleep(Constants.MAX_WAIT_TIME);
                    } else {
                        Select sel = new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected")));
                        if (sel.getOptions().size() > 0)
                            sel.selectByIndex(0);
                        else
                            sel.selectByIndex(1);
                        pNode.info("Select Instrument TCP By Index- ");
                        Thread.sleep(Constants.MAX_WAIT_TIME);
                    }

                    /*
                     * If the wallet type is not applicable then skip the same!
                     */
                    Select selPrimaryAccount = new Select(driver.findElement(By.name("counterList[" + counter + "].primaryAccountSelected")));

                    if (isPrimary && DataFactory.getDefaultWallet().WalletName.equals(paymentType)) {
                        selPrimaryAccount.selectByIndex(0);
                        isPrimary = false;
                    } else {
                        selPrimaryAccount.selectByIndex(1);
                    }
                    counter++;
                    isSkipped = false;
                }

//  Add Multiple Wallet with Multiple Primary Accnt

                Thread.sleep(2000);
                AddChannelUser_pg4.init(pNode).btnAddMore();
                pNode.info("Clicked on Add More!");

                new Select(driver.findElement(By.id("walletTypeID1"))).selectByIndex(1);
                Thread.sleep(1000);

                new Select(driver.findElement(By.id("grade1"))).selectByVisibleText(user.GradeName);
                pNode.info("Select Grade:" + user.GradeName);
                Thread.sleep(2500);

                // new Select(driver.findElement(By.id("tcp2"))).selectByIndex(1);
                new Select(driver.findElement(By.id("primaryAc1"))).selectByVisibleText("Yes");


            }
            page4.clickNext(user.CategoryCode);
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }



    /*      Mapping Multiple banks with multiple Primary Account
     * */

    public CommonUserManagement mapMultipletBankPreferencesWitMulPrimaryAcc(User user, boolean isPrimary) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("mapDefaultBankPreferences: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();

            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);

            new Select(driver.findElement(By.name("bankCounterList[0].providerSelected"))).selectByVisibleText(defaultProvider.ProviderName);
            pNode.info("Select Provider - " + defaultProvider.ProviderName);
            Thread.sleep(Constants.TWO_SECONDS);

            new Select(driver.findElement(By.name("bankCounterList[0].paymentTypeSelected"))).selectByVisibleText(DataFactory.getDefaultBankNameForDefaultProvider());
            pNode.info("Select Bank - " + DataFactory.getDefaultBankNameForDefaultProvider());
            Thread.sleep(Constants.TWO_SECONDS);

            new Select(driver.findElement(By.name("bankCounterList[0].channelGradeSelected"))).selectByVisibleText(user.GradeName);
            pNode.info("Select Grade - " + user.GradeName);
            Thread.sleep(Constants.TWO_SECONDS);

            new Select(driver.findElement(By.name("bankCounterList[0].tcpSelected"))).selectByIndex(0);
            Thread.sleep(Constants.TWO_SECONDS);

            // UI elements
            WebElement customerID = driver.findElement(By.name("bankCounterList[0].customerId"));
            WebElement accountNum = driver.findElement(By.name("bankCounterList[0].accountNumber"));

            /*
            Set The Customer ID----add1_addChannelUser_submit;;;;;bankCounterList[0].primaryAccountSelected
             */
            String custId = "" + DataFactory.getRandomNumber(9);
            customerID.sendKeys(custId);
            user.setDefaultCustId(custId);

            /*
            Set The account number
             */
            String accNum = "" + DataFactory.getRandomNumber(9);
            accountNum.sendKeys(accNum);
            user.setDefaultAccNum(accNum);

//      Add More

            AddChannelUser_pg5.init(pNode).ClickOnAddMoreToRegistrMorWallets();
            Thread.sleep(Constants.TWO_SECONDS);

            defaultProvider = DataFactory.getDefaultProvider();
            String bankName = DataFactory.getNonTrustBankName(DataFactory.getDefaultProvider().ProviderName);


            new Select(driver.findElement(By.name("bankCounterList[1].paymentTypeSelected"))).selectByVisibleText(bankName);
            pNode.info("Select Bank - " + bankName);
            Thread.sleep(Constants.TWO_SECONDS);

            new Select(driver.findElement(By.name("bankCounterList[1].channelGradeSelected"))).selectByVisibleText(user.GradeName);
            pNode.info("Select Grade - " + user.GradeName);
            Thread.sleep(Constants.TWO_SECONDS);

            new Select(driver.findElement(By.name("bankCounterList[1].tcpSelected"))).selectByIndex(0);
            Thread.sleep(Constants.TWO_SECONDS);

            // UI elements
            customerID = driver.findElement(By.name("bankCounterList[1].customerId"));
            accountNum = driver.findElement(By.name("bankCounterList[1].accountNumber"));

            /*
            Set The Customer ID
             */
            custId = "" + DataFactory.getRandomNumber(9);
            customerID.sendKeys(custId);
            user.setDefaultCustId(custId);

            /*
            Set The account number
             */
            accNum = "" + DataFactory.getRandomNumber(9);
            accountNum.sendKeys(accNum);
            user.setDefaultAccNum(accNum);
            if (isPrimary) {
                new Select(driver.findElement(By.name("bankCounterList[1].primaryAccountSelected"))).selectByIndex(0);
            } else {
                new Select(driver.findElement(By.name("bankCounterList[1].primaryAccountSelected"))).selectByIndex(1);
            }
            Thread.sleep(Constants.TWO_SECONDS);

            page5.clickNext(user.CategoryCode);
            /*
             * Complete User Creation
             */
            // page5.completeUserCreation(user, false);

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public CommonUserManagement changeFirstTimePasswordForBiller(Biller biller) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("changeFirstTimePassword: " + biller.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ChangePasswordPage changPwd = ChangePasswordPage.init(pNode);
            FunctionLibrary fl = new FunctionLibrary(driver);

            Login.init(pNode).login(biller.LoginId, ConfigInput.userCreationPassword);
            fl.contentFrame();

            changPwd.setOldPassword(ConfigInput.userCreationPassword);
            changPwd.setNewPassword(biller.Password);
            changPwd.setConfirmPassword(biller.Password);
            changPwd.clickSubmit();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("changePassword.label.success", "Change User Password", pNode)) {
                    biller.setStatus(Constants.STATUS_ACTIVE);
                }

                if (driver.findElements(By.id("skip_for_later")).size() > 0) {
                    changPwd.clickSkip();
                    driver.switchTo().alert().accept();
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public void checkBarUserLink(OperatorUser user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("Check BarUser Link: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(user);

            BarUser_pg1 page1 = BarUser_pg1.init(pNode);
            page1.navigateToBarUserLink();

            if (driver.findElement(By.id("PARTY_PTY_BLKL")).isDisplayed()) {
                pNode.info("'Bar User' Role is available");
            } else {
                pNode.info("'Bar User' Role is not Available");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public void verifyProviders() throws Exception {
        Markup m = MarkupHelper.createLabel("verifyProvidersDetails", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            new Commission_Disbursement_Page1(pNode).navigateToLink();


            List<String> allProviders = new Commission_Disbursement_Page1(pNode).getAllProvider();
            List<String> providerFromAppData = DataFactory.getAllProviderNames();
            pNode.info("From UI: " + allProviders.toString());
            pNode.info("From AppData: " + providerFromAppData.toString());

            if (allProviders.containsAll(providerFromAppData)) {
                pNode.pass("Successfully verified all providers Available");
            } else {
                pNode.fail("Failed to verify All providers Name");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public void verifyCategoryDetails() throws Exception {
        Markup m = MarkupHelper.createLabel("verifyCategoryDetails", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            new Commission_Disbursement_Page1(pNode).selectProvider();
            new Commission_Disbursement_Page1(pNode).SelectDomain(DataFactory.getDomainName(Constants.WHOLESALER));

            Select select = new Select(driver.findElement(By.id("domainCode")));
            List<WebElement> options = select.getOptions();
            for (WebElement option : options) {

                if (option.getText().equalsIgnoreCase("Wholesaler")) {

                    select.selectByVisibleText("Wholesaler");
                    Thread.sleep(2000);
                    select = new Select(driver.findElement(By.id("categoryCode")));
                    List<WebElement> option1 = select.getOptions();
                    for (WebElement subOption : option1) {

                        if (subOption.getText().equalsIgnoreCase("Wholesaler")) {

                            pNode.pass("Successfully verified the fields");


                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * DefaultBank Preferences for liquidation
     *
     * @param user
     * @param liq
     * @param isModify
     * @return
     * @throws Exception
     */

    public CommonUserManagement mapDefaultBankPreferencesforliquidation(User user, String bankName, Liquidation liq, boolean... isModify) throws Exception {
        boolean isModified = isModify.length > 0 ? isModify[0] : false;
        try {
            Markup m = MarkupHelper.createLabel("mapDefaultBankPreferencesforliquidation: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);

            if (isModified) {
                CommonChannelUserPage.init(pNode).clickAddLiquidationBank();
            } else {

                page5.liquidationclick();
            }

            // UI elements
            Thread.sleep(3000);
            new Select(driver.findElement(By.id("liquidationProvider0"))).selectByVisibleText(defaultProvider.ProviderName);
            pNode.info("Select Provider - " + defaultProvider.ProviderName);
            Thread.sleep(1000);

            new Select(driver.findElement(By.id("liquidationBankListId0"))).selectByVisibleText(bankName);
            pNode.info(bankName + " Bank is selected");
            Thread.sleep(2000);


            page5.addvalues(liq.LiquidationBankAccountNumber, liq.LiquidationBankBranchName, liq.AccountHolderName, liq.FrequencyTradingName);

            page5.selectLiquidationFrequency(user.LiquidationFrequency, user.LiquidationDay);
            page5.clickNext(user.CategoryCode);
            if (!isModified) {
                AlertHandle.acceptAlert(pNode);
                AlertHandle.acceptAlert(pNode);
            }
            page5.completeUserCreation(user, isModified);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * ModifyExistingFrequencyforLiquidationBank
     *
     * @param user
     * @param FrequencyType
     * @param dayofweek
     * @param day
     * @param dateOfMonth
     * @param isModify
     * @return
     * @throws IOException
     */

    public CommonUserManagement modifyExistingFrequencyForLiquidationBank(User user, String FrequencyType, String dayofweek, String day, String dateOfMonth, boolean... isModify) throws IOException {
        boolean isModified = isModify.length > 0 ? isModify[0] : false;
        try {
            Markup m = MarkupHelper.createLabel("modifyExistingFrequencyforLiquidationBank ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);
            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();
            page.clickNextWalletMap();

            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);

            if (FrequencyType.equals("Daily")) {
                driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='Daily']")).click();
                pNode.info("Daily is selected");
            } else if (FrequencyType.equals("Weekly")) {
                page5.selectWeekly(dayofweek);
            } else if (FrequencyType.equals("Fortnightly")) {
                page5.selectFortnightly(day);
            } else {
                page5.selectMonthly(dateOfMonth);
            }


            page5.clickNext(user.CategoryCode);
            page5.completeUserCreation(user, isModified);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * ModifyExistingstatusofLiquidationBank
     *
     * @param user
     * @param status
     * @return
     * @throws IOException
     */

    public CommonUserManagement modifyExistingStatusOfLiquidationBank(User user, String status) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("modifyExistingstatusofLiquidationBank ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);
            page.selectStatusofLiquidationBank(status);
            AddChannelUser_pg5 page5 = new AddChannelUser_pg5(pNode);
            page5.clickNext(user.CategoryCode);
            page5.completeUserCreation(user, true);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * ModifyExistingstatusofBank
     *
     * @param user
     * @param status
     * @return
     * @throws IOException
     */

    public CommonUserManagement modifyExistingStatusOfBank(User user, String status, String accountNumber) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("modifyExistingstatusofBank ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);
            page.selectStatusOfBank(status, accountNumber);
            AddChannelUser_pg5 page5 = new AddChannelUser_pg5(pNode);
            page5.clickNext(user.CategoryCode);
            page5.completeUserCreation(user, true);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * ModifyExistingStatusOfLiquidationBankForBiller
     *
     * @param
     * @param status
     * @return
     * @throws IOException
     */

    public CommonUserManagement modifyExistingStatusOfLiquidationBankForBiller(String status) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("modifyExistingstatusofLiquidationBank ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            BillerApproveModify_pg1 page1 = new BillerApproveModify_pg1(pNode);
            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);
            page.selectStatusofLiquidationBank(status);
            page1.clickButtonSubmit();
            Thread.sleep(3000);
            page1.clickConfirmButton();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * ModifyExistingFrequencyForLiquidationBankForBiller
     *
     * @param
     * @param FrequencyType
     * @param dayofweek
     * @param day
     * @param dateOfMonth
     * @param
     * @return
     * @throws IOException
     */

    public CommonUserManagement modifyExistingFrequencyForLiquidationBankForBiller(String FrequencyType, String dayofweek, String day, String dateOfMonth) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("modifyExistingFrequencyforLiquidationBankForBiller ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);
            BillerApproveModify_pg1 page = new BillerApproveModify_pg1(pNode);

            if (FrequencyType.equals("Daily")) {
                driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='Daily']")).click();
                pNode.info("Daily is selected");
            } else if (FrequencyType.equals("Weekly")) {
                page5.selectWeekly(dayofweek);
            } else if (FrequencyType.equals("Fortnightly")) {
                page5.selectFortnightly(day);
            } else {
                page5.selectMonthly(dateOfMonth);
            }

            page.clickButtonSubmit();
            Thread.sleep(3000);
            page.clickConfirmButton();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public CommonUserManagement approveAssociatedBanksForUser(User user, String providerName, String bank) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveAssociatedBanksForUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddModifyApproveBank pageOne = AddModifyApproveBank.init(pNode);
            pageOne.navAddModifyDeleteBank();
            pageOne.selectProvider(providerName);
            pageOne.selectBank(bank);

            pageOne.setFromDate();
            pageOne.setToDate();
            pageOne.clickSubmit();
            pageOne.checkBanksForUser(user.MSISDN);
            pageOne.approve();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("channeluser.bankapproved", "Approve the bank", pNode, providerName, user.MSISDN, bank);
                Assertion.assertErrorInPage(pNode);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Reject Associated Bank For User
     *
     * @param user
     * @param providerName
     * @param bank
     * @return
     * @throws Exception
     */
    public CommonUserManagement rejectAssociatedBanksForUser(User user, String providerName, String bank, String... custId) throws Exception {
        try {

            String refId = (custId.length > 0) ? custId[0] : user.MSISDN;
            Markup m = MarkupHelper.createLabel("rejectAssociatedBanksForUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddModifyApproveBank pageOne = AddModifyApproveBank.init(pNode);
            pageOne.navAddModifyDeleteBank();
            pageOne.selectProvider(providerName);
            pageOne.selectBank(bank);

            pageOne.setFromDate();
            pageOne.setToDate();
            pageOne.clickSubmit();
            pageOne.checkBanksForUser(refId);
            pageOne.reject();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("channeluser.bankreject", "Reject the bank", pNode, user.MSISDN, bank);
                Assertion.assertErrorInPage(pNode);
                Thread.sleep(1000);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * get rest password from the Email Queue
     *
     * @param msisdn
     * @return
     * @throws MoneyException
     */
    public String getResetPasswordFromEmailQueue(String msisdn) throws MoneyException, IOException {
        try {
            String actualpassword = MobiquityGUIQueries.dbFetchResetPassWordFromEmailQueue(msisdn);
            //decrypt the password
            DesEncryptor d = new DesEncryptor();
            return d.decrypt(actualpassword);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return null;
    }

    /**
     * Get Suspended User
     *
     * @param catCode
     * @param gradeCode
     * @return
     * @throws Exception
     */
    public User getSuspendedUser(String catCode, String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getSuspendedUserList(catCode, gradeCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                if (usr.getStatus().equalsIgnoreCase(Constants.STATUS_SUSPEND)) {
                    String dbStatus = MobiquityGUIQueries.getUserStatus(usr.MSISDN, usr.CategoryCode);
                    if (dbStatus != null && dbStatus.equals(Constants.STATUS_SUSPEND)) {
                        return usr;
                    } else {
                        // delete this entry from Excel, as its no longer a valid suspended User
                        ExcelUtil.removeTempUser(usr);
                    }
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = createAndSuspendTempUser(catCode, gradeName);

        if (usr == null) {
            Assert.fail("Failed to create Suspended User, Category:" + catCode);
        } else {
            usr.writeDataToExcel(true);
        }
        return usr;
    }

    /**
     * Create and Suspend a Temp User
     *
     * @param catCode
     * @param gradeName
     * @return
     * @throws IOException
     */
    private User createAndSuspendTempUser(String catCode, String gradeName) throws IOException {
        Markup m = MarkupHelper.createLabel("createAndSuspendTempUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            User newUser = new User(catCode, gradeName);
            if (catCode.equals(Constants.SUBSCRIBER)) {
                SubscriberManagement.init(pNode).createDefaultSubscriberUsingAPI(newUser);
                OperatorUser usrSuspResume = DataFactory.getOperatorUserWithAccess("SR_USR", Constants.NETWORK_ADMIN);
                Login.init(pNode).login(usrSuspResume);
                SubscriberManagement.init(pNode).suspendSubscriber(newUser);
            } else {
                ChannelUserManagement.init(pNode)
                        .createChannelUserDefaultMapping(newUser, false)
                        .suspendChannelUser(newUser);
            }

            if (newUser.isSuspended()) {
                return newUser;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        Assertion.markAsFailure("Failed to create Suspended User, Category:" + catCode);
        return null;
    }

    /**
     * Get barred User
     *
     * @param catCode   - Category Code
     * @param barType   - Constants.BAR_AS_RECIEVER, Constants.BAR_AS_SENDER, Constants.BAR_TYPE_BOTH
     * @param gradeCode - optional, when pased as null, default first grade is considered
     * @return
     * @throws Exception
     */
    public User getBarredUser(String catCode, String barType, String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getBarredUserList(catCode, gradeCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                if (usr.getStatus().equalsIgnoreCase(barType)) {
                    String dbStatus = MobiquityGUIQueries.getUserBarStatus(usr.LoginId);
                    if (dbStatus != null && dbStatus.equals(barType)) {
                        return usr;
                    } else {
                        // delete this entry from Excel, as its no longer a valid suspended User
                        ExcelUtil.removeTempUser(usr);
                    }
                }
            }

        }

        // if valid user is not found, Create a new User and return the same
        User usr = createAndBarTempUser(catCode, barType, gradeName);

        if (usr == null)
            Assert.fail("Failed to create Barred User, Category:" + catCode + ", Bar as:" + barType);

        return usr;
    }


    /**
     * Create and Bar User
     *
     * @param catCode
     * @param barType
     * @param gradeName
     * @return
     * @throws IOException
     */
    private User createAndBarTempUser(String catCode, String barType, String gradeName) throws IOException {
        Markup m = MarkupHelper.createLabel("createAndBarTempUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            User newUser = new User(catCode, gradeName);
            if (catCode.equals(Constants.SUBSCRIBER)) {
                SubscriberManagement.init(pNode)
                        .createSubscriberDefaultMapping(newUser, true, true);

                Login.init(pNode).login(barUser);
                SubscriberManagement.init(pNode)
                        .barSubscriber(newUser, barType);
            } else {
                ChannelUserManagement.init(pNode)
                        .createChannelUserDefaultMapping(newUser, false);

                Login.init(pNode).login(barUser);
                ChannelUserManagement.init(pNode)
                        .barChannelUser(newUser, Constants.USER_TYPE_CHANNEL, barType);
            }

            if (newUser.isBarred()) {
                newUser.writeDataToExcel(true);
                return newUser;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        Assertion.markAsFailure("Failed to create Barred User, Category:" + catCode + ", Bar as:" + barType);
        return null;
    }

    /**
     * Unbar operator User
     *
     * @param user
     * @return
     * @throws Exception
     */
    public String unBarOperatorUser(OperatorUser user) throws Exception {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("UnBarUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            UnBarUser_pg1 pageOne = new UnBarUser_pg1(pNode);

            pageOne.navigateNAToUnBarUserLink();

            pageOne.selectUserTypeByValue(Constants.USER_TYPE_OPT_USER);
            pageOne.setLoginID(user.LoginId);
            pageOne.clickSubmit();
            pageOne.selectUserFromList();
            pageOne.clickOnUnbarSubmit();

            if (ConfigInput.isAssert) {
                pageOne.clickOnUnbarConfirm();
                if (Assertion.verifyActionMessageContain("user.unbar.success", "Verify Un-Bar Channel user: " + user.LoginId, pNode)) {
                    user.setStatus(Constants.STATUS_ACTIVE);
                    actualMessage = pageOne.getMessage();
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return actualMessage;
    }

    public User getBlacklistedUser(String catCode, String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getBlacklistedUserList(catCode, gradeCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                if (usr.getStatus().equalsIgnoreCase(Constants.STATUS_BLACKLIST)) {
                    String dateOfBirth1 = new MobiquityGUIQueries().getDOB(usr);
                    usr.DateOfBirth = DateAndTime.convertDateFormatToDBFormat(dateOfBirth1.toString());
                    String existUser = MobiquityGUIQueries.getBlackListedUser(usr);
                    if (existUser != null) {
                        return usr;
                    } else {
                        // delete this entry from Excel, as its no longer a valid suspended User
                        ExcelUtil.removeTempUser(usr);
                    }
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = createAndBlacklistUser(catCode, gradeName);

        if (usr == null)
            Assert.fail("Failed to create Black Listed User, Category:" + catCode);

        return usr;
    }

    /**
     * Create and Blacklist a Temp User
     *
     * @param catCode
     * @param gradeName
     * @return
     * @throws IOException
     */
    private User createAndBlacklistUser(String catCode, String gradeName) throws IOException {
        Markup m = MarkupHelper.createLabel("createAndBlacklistUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            User newUser = new User(catCode, gradeName);
            if (catCode.equals(Constants.SUBSCRIBER)) {
                SubscriberManagement.init(pNode)
                        .createSubscriberDefaultMapping(newUser, true, false);

                OperatorUser custblkl = DataFactory.getOperatorUserWithAccess("BLK_ABL");
                Login.init(pNode).login(custblkl);

                BlackListManagement.init(pNode).doCustomerBlacklist(newUser.FirstName, newUser.LastName, newUser.DateOfBirth);
            } else {
                ChannelUserManagement.init(pNode)
                        .createChannelUserDefaultMapping(newUser, false);
                BlackListManagement.init(pNode).doCustomerBlacklist(newUser.FirstName, newUser.LastName, newUser.DateOfBirth);
                newUser.setStatus(Constants.STATUS_BLACKLIST);
            }

            if (newUser.isBlackListed()) {
                newUser.writeDataToExcel(true);
                return newUser;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        Assertion.markAsFailure("Failed to create Black Listed User, Category:" + catCode);
        return null;
    }

    /**
     * Get Churned User
     *
     * @param catCode
     * @param gradeCode
     * @return curned user, settlement  = N
     * @throws Exception
     */
    public User getChurnedUser(String catCode, String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getChurnedUserList(catCode, gradeCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                String dbStatus = MobiquityGUIQueries.getChurnUserSettledStatus(usr.MSISDN);
                if (dbStatus != null && dbStatus.equalsIgnoreCase("N")) {
                    return usr;
                } else {
                    // delete this entry from Excel, as its no longer a valid suspended User
                    ExcelUtil.removeTempUser(usr);
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = createAndChurnUser(catCode, gradeName);

        if (usr == null)
            Assert.fail("Failed to create Churned User, Category:" + catCode);
        else {
            usr.writeDataToExcel(true);
        }

        return usr;
    }

    public User getChurneInitiatedUser(String catCode, String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getChurnInitiatedUserList(catCode, gradeCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                String dbStatus = MobiquityGUIQueries.getChurnUserSettledStatus(usr.MSISDN);
                if (dbStatus != null && dbStatus.equalsIgnoreCase("N")) {
                    return usr;
                } else {
                    // delete this entry from Excel, as its no longer a valid suspended User
                    ExcelUtil.removeTempUser(usr);
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = createAndChurnInitiateUser(catCode, gradeName);

        if (usr == null)
            Assert.fail("Failed to create Churn Initiated User, Category:" + catCode);
        else {
            usr.writeDataToExcel(true);
        }

        return usr;
    }

    /**
     * Get Churned User
     *
     * @param catCode
     * @param gradeCode
     * @return
     * @throws Exception
     */
    public User getDeletedUser(String catCode, String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getDeletedUserList(catCode, gradeCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                String dbStatus = MobiquityGUIQueries.getUserStatus(usr.MSISDN, usr.CategoryCode);
                if (dbStatus != null && dbStatus.equalsIgnoreCase("N")) {
                    return usr;
                } else {
                    // delete this entry from Excel, as its no longer a valid suspended User
                    ExcelUtil.removeTempUser(usr);
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = createAndDeleteUser(catCode, gradeName);

        if (usr == null)
            Assert.fail("Failed to create Deleted User, Category:" + catCode);
        else {
            usr.writeDataToExcel(true);
        }

        return usr;
    }

    public User getDeleteInitiatedUser(String catCode, String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getDeleteInitiateUserList(catCode, gradeCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                String dbStatus = MobiquityGUIQueries.getUserDeleteStatus(usr.MSISDN, usr.CategoryCode);
                if (dbStatus != null && dbStatus.equalsIgnoreCase("DI")) {
                    return usr;
                } else {
                    // delete this entry from Excel, as its no longer a valid suspended User
                    ExcelUtil.removeTempUser(usr);
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = createAndDeleteInitiateUser(catCode, gradeName);

        if (usr == null)
            Assert.fail("Failed to create Delete Initiated User, Category:" + catCode);
        else {
            usr.writeDataToExcel(true);
        }

        return usr;
    }

    /**
     * Create a User with specific Category and grade, make sure the user has balance b4 it's churned!
     *
     * @param catCode
     * @param gradeName
     * @return
     * @throws Exception
     */
    public User createAndChurnUser(String catCode, String gradeName) throws Exception {

        Markup m = MarkupHelper.createLabel("createAndChurnUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            User usr = new User(catCode, gradeName);

            if (catCode.equals(Constants.SUBSCRIBER)) {
                SubscriberManagement.init(pNode)
                        .createSubscriberDefaultMapping(usr, true, false);
                TransactionManagement.init(pNode)
                        .makeSureLeafUserHasBalance(usr, new BigDecimal(250));
            } else {
                ChannelUserManagement.init(pNode)
                        .createChannelUserDefaultMapping(usr, false); // don't add bank as user will not be able to churned
                TransactionManagement.init(pNode)
                        .makeSureChannelUserHasBalance(usr, new BigDecimal(250));
            }

            // Churn initiate And Approve
            churnInitiateAndApprove(usr);

            if (usr.getStatus().equals(Constants.AUT_STATUS_CHURNED)) {
                return usr;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return null;
    }

    /**
     * Creates and churn Initiate a User
     *
     * @param catCode
     * @param gradeName
     * @return
     * @throws Exception
     */
    public User createAndChurnInitiateUser(String catCode, String gradeName) throws Exception {

        Markup m = MarkupHelper.createLabel("createAndChurnInitiateUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            User usr = new User(catCode, gradeName);

            if (catCode.equals(Constants.SUBSCRIBER)) {
                SubscriberManagement.init(pNode)
                        .createSubscriberDefaultMapping(usr, true, false);
                TransactionManagement.init(pNode)
                        .makeSureLeafUserHasBalance(usr, new BigDecimal(250));
            } else {
                ChannelUserManagement.init(pNode)
                        .createChannelUserDefaultMapping(usr, false); // don't add bank as user will not be able to churned
                TransactionManagement.init(pNode)
                        .makeSureChannelUserHasBalance(usr, new BigDecimal(250));
            }

            // Churn initiate And Approve
            churnInitiateUser(usr);

            if (usr.getStatus().equals(Constants.AUT_STATUS_CHURN_INITIATE)) {
                return usr;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return null;
    }

    /**
     * Create a User with specific Category and grade, make sure the user has balance b4 it's Deleted!
     *
     * @param catCode
     * @param gradeName
     * @return
     * @throws Exception
     */
    public User createAndDeleteUser(String catCode, String gradeName) throws Exception {

        Markup m = MarkupHelper.createLabel("createAndDeleteUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            User usr = new User(catCode, gradeName);

            if (catCode.equals(Constants.SUBSCRIBER)) {
                SubscriberManagement.init(pNode)
                        .createSubscriberDefaultMapping(usr, true, false);
                TransactionManagement.init(pNode)
                        .makeSureLeafUserHasBalance(usr, new BigDecimal(10));
                SubscriberManagement.init(pNode).deleteSubscriber(usr, DataFactory.getDefaultProvider().ProviderId);
            } else {
                ChannelUserManagement.init(pNode)
                        .createChannelUserDefaultMapping(usr, false);
                ChannelUserManagement.init(pNode).deleteChannelUser(usr);
            }


            if (usr.getStatus().equals(Constants.AUT_STATUS_DELETED)) {
                return usr;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return null;
    }

    /**
     * Create a User with specific Category and grade, Delete Initiate the user
     *
     * @param catCode
     * @param gradeName
     * @return
     * @throws Exception
     */
    public User createAndDeleteInitiateUser(String catCode, String gradeName) throws Exception {

        Markup m = MarkupHelper.createLabel("createAndDeleteUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            User usr = new User(catCode, gradeName);
            ChannelUserManagement.init(pNode)
                    .createChannelUserDefaultMapping(usr, false);

            ChannelUserManagement.init(pNode)
                    .initiateChannelUserDelete(usr);

            if (usr.getStatus().equals(Constants.AUT_STATUS_DELETE_INITIATE)) {
                return usr;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return null;
    }


    public Biller getSuspendedBiller(String catCode, String barType, String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<Biller> sList = DataFactory.getSuspendedBillerList(catCode, barType, gradeCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (Biller usr : sList) {
                if (usr.getStatus().equalsIgnoreCase(barType)) {
                    String dbStatus = MobiquityGUIQueries.getUserBarStatus(usr.LoginId);
                    if (dbStatus != null && dbStatus.equals(barType)) {
                        return usr;
                    } else {
                        // delete this entry from Excel, as its no longer a valid suspended User
                        ExcelUtil.deleteBiller(usr);
                    }
                }
            }

        }

        // if valid user is not found, Create a new User and return the same
        Biller usr = createAndSuspendedTempBiller(catCode, barType, gradeName);

        if (usr == null)
            Assert.fail("Failed to create Barred User, Category:" + catCode + ", Bar as:" + barType);

        return usr;
    }


    /**
     * Create and Bar Biller
     *
     * @param catCode
     * @param gradeName
     * @return
     * @throws IOException
     */
    private Biller createAndSuspendedTempBiller(String catCode, String barType, String gradeName) throws IOException {
        Markup m = MarkupHelper.createLabel("createAndSuspendTempUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            Biller newUser = null;
            BillerManagement.init(pNode).createBiller(newUser);
            BillerManagement.init(pNode).suspendBiller(newUser);

            if (newUser.isSuspended()) {
                newUser.writeDataToExcel();
                return newUser;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        Assertion.markAsFailure("Failed to create Suspend User, Category:" + catCode + ", Bar as:" + barType);
        return null;
    }

    /**
     * Get barred User
     *
     * @param catCode - Category Code
     * @return
     * @throws Exception
     */
    public OperatorUser getBarredOperatorUser(String catCode) throws Exception {
        ExcelUtil.writeTempOptUserHeaders();
        List<OperatorUser> sList = DataFactory.getBarredOperatorUserList(catCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (OperatorUser usr : sList) {
                if (usr.getStatus().equalsIgnoreCase(Constants.BAR_AS_BOTH)) {
                    String dbStatus = MobiquityGUIQueries.getUserBarStatus(usr.LoginId);
                    if (dbStatus != null && dbStatus.equals(Constants.BAR_AS_BOTH)) {
                        return usr;
                    } else {
                        // delete this entry from Excel, as its no longer a valid suspended User
                        ExcelUtil.removeTempOperatorUser(usr);
                    }
                }
            }

        }

        // if valid user is not found, Create a new User and return the same
        OperatorUser usr = createAndBarTempOperatorUser(catCode);

        if (usr != null && usr.isBarred()) {
            usr.writeDataToExcel(true);
            return usr;
        } else {
            Assert.fail("Failed to create Barred User, Category:" + catCode);
        }
        return null;
    }

    /**
     * TODO - in progress
     *
     * @param catCode
     * @return
     * @throws Exception
     */
    public OperatorUser getDeleteInitiatedOperatorUser(String catCode) throws Exception {
        ExcelUtil.writeTempOptUserHeaders();
        List<OperatorUser> sList = DataFactory.getBarredOperatorUserList(catCode);
        if (sList.size() > 0) {
            // check for user status in DB
            for (OperatorUser usr : sList) {
                if (usr.getStatus().equalsIgnoreCase(Constants.BAR_AS_BOTH)) {
                    String dbStatus = MobiquityGUIQueries.getUserBarStatus(usr.LoginId);
                    if (dbStatus != null && dbStatus.equals(Constants.BAR_AS_BOTH)) {
                        return usr;
                    } else {
                        // delete this entry from Excel, as its no longer a valid suspended User
                        ExcelUtil.removeTempOperatorUser(usr);
                    }
                }
            }

        }

        // if valid user is not found, Create a new User and return the same
        OperatorUser usr = createAndBarTempOperatorUser(catCode);

        if (usr != null && usr.isBarred()) {
            usr.writeDataToExcel(true);
            return usr;
        } else {
            Assert.fail("Failed to create Barred User, Category:" + catCode);
        }
        return null;
    }


    /**
     * Create and Bar Operator User
     *
     * @param catCode
     * @return
     * @throws IOException
     */
    private OperatorUser createAndBarTempOperatorUser(String catCode) throws IOException {
        Markup m = MarkupHelper.createLabel("createAndBarTempOperatorUser: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            OperatorUser newUser = new OperatorUser(catCode);

            if (catCode.equals(Constants.NETWORK_ADMIN) || catCode.equals(Constants.BANK_ADMIN)) {
                OperatorUserManagement.init(pNode)
                        .createAdmin(newUser);

                Login.init(pNode).loginAsSuperAdmin("PTY_BLKL");
            } else {
                OperatorUserManagement.init(pNode)
                        .createOptUser(newUser, Constants.NETWORK_ADMIN, Constants.NETWORK_ADMIN);

                OperatorUser optNwAdm = DataFactory.getOperatorUserWithAccess("PTY_BLKL", Constants.NETWORK_ADMIN);
                Login.init(pNode).login(optNwAdm);
            }

            OperatorUserManagement.init(pNode)
                    .barOperatorUser(newUser);

            return newUser;
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        Assertion.markAsFailure("Failed to create Barred Operator User, Category:" + catCode);
        return null;
    }


    public User getDefaultSubscriber(String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(Constants.SUBSCRIBER).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getTempSubscriberList(Constants.STATUS_ACTIVE);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                String dbStatus = MobiquityGUIQueries.getUserStatus(usr.MSISDN, usr.CategoryCode);
                if (dbStatus != null && dbStatus.equals(Constants.STATUS_ACTIVE)) {
                    return usr;
                } else {
                    // delete this entry from Excel, as its no longer a valid User
                    ExcelUtil.removeTempUser(usr);
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = new User(Constants.SUBSCRIBER);
        usr.GradeCode = gradeCode;
        usr.GradeName = gradeName;


        SubscriberManagement.init(pNode)
                .createSubscriberDefaultMapping(usr, true, false);


        if (!usr.isCreated.equalsIgnoreCase("Y")) {
            Assert.fail("Failed to create Subscriber");
            return null;
        } else
            usr.writeDataToExcel(true);

        return usr;
    }


    public User getTempDeleteInitiatedSubscriber(String gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;

        if (gradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(Constants.SUBSCRIBER).get(0);
            gradeName = grade.GradeName;
            gradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getTempSubscriberList(Constants.STATUS_DELETE_INITIATE);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                String dbStatus = MobiquityGUIQueries.getUserStatus(usr.MSISDN, usr.CategoryCode);
                if (dbStatus != null && dbStatus.equals(Constants.STATUS_DELETE_INITIATE)) {
                    return usr;
                } else {
                    // delete this entry from Excel, as its no longer a valid User
                    ExcelUtil.removeTempUser(usr);
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User subs = new User(Constants.SUBSCRIBER);
        subs.GradeCode = gradeCode;
        subs.GradeName = gradeName;


        SubscriberManagement.init(pNode)
                .createSubscriberDefaultMapping(subs, true, false);

        User chUserDelSubsByAgent = DataFactory.getChannelUserWithAccess("SUBSDELBYAGT");
        ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, subs, chUserDelSubsByAgent, null, null, null, null);
        ServiceChargeManagement.init(pNode)
                .configureServiceCharge(sChargeAccClose);

        TransactionManagement.init(pNode).makeSureLeafUserHasBalance(subs, new BigDecimal(100));

        Login.init(pNode).login(chUserDelSubsByAgent);

        SubscriberManagement.init(pNode)
                .deleteSubscriberByAgent(subs, DataFactory.getDefaultProvider().ProviderId);

        return subs;
    }

    public CommonUserManagement churnInitiateAndApprove(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("churnInitiateAndApprove", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(optChurnInit);

            // initiate Churn
            String batchId = churnInitiateUser(user);

            if (batchId != null) {
                // approve churn
                Login.init(pNode).login(optChurnApprove);
                approveRejectChurnInitiate(user, batchId, true);
            }

        } catch (IOException e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * Churn initiate a User
     *
     * @param user
     * @return - batch Id if the Churn is successfully Initiated
     * @throws Exception
     */
    public String churnInitiateUser(User user) throws Exception {

        Markup m = MarkupHelper.createLabel("churnInitiateUser", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String batchId = null;
        try {
            // Login as Network Admin
            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("CHURNMGMT_MAIN", Constants.NETWORK_ADMIN);
            Login.init(pNode)
                    .login(netAdmin);
            ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
            int failCount = page
                    .navUserChurnInitiate()
                    .downloadChurnSampleFile()
                    .setChurnUserMsisdn(user)
                    .uploadChurnUserExcel()
                    .getChurnInitiateFailCount();

            Utils.captureScreen(pNode);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("churn.success.churnUploadFile", "Churn Initiation Successfull", pNode);
                if (Assertion.verifyEqual(failCount, 0, "Verify that the User " + user.LoginId + " is successfully Churn Initiated, fail count should be '0'", pNode)) {
                    user.setStatus(Constants.AUT_STATUS_CHURN_INITIATE);
                }
                page.checkLogFileSpecificMessage(user.MSISDN, Constants.TXN_SUCCESS);
                batchId = page.getbatchID();
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return batchId;

    }

    /**
     * Approve Churn Initiate Single User
     *
     * @param user
     * @param batchId
     * @return
     * @throws Exception
     */
    public CommonUserManagement approveRejectChurnInitiate(User user, String batchId, boolean isApprove) throws Exception {
        Markup m = MarkupHelper.createLabel("approveRejectChurnInitiate", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            User optUserApr;
            optChurnApprove = DataFactory.getOperatorUsersWithAccess("CHURN_APPROVE").get(0);

            Login.init(pNode).login(optChurnApprove);

            ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
            page.navUserChurnApproval()
                    .selectBatchId(batchId);

            if (!isApprove) {
                page.rejectBatch();
            }

            page.clickOnSubmitBtn();

            AlertHandle.acceptAlert(pNode);

            if (ConfigInput.isAssert) {
                if (isApprove) {
                    if (Assertion.verifyActionMessageContain("churn.progress.approve", "Verify Churn with Batch Id:" + batchId + " is approved", pNode, batchId)) {
                        // todo check the log file and set the user status
                        user.setStatus(Constants.AUT_STATUS_CHURNED);
                    }
                } else {
                    Assertion.verifyActionMessageContain("churn.progress.reject", "Verify Churn with Batch Id:" + batchId + " is Rejected", pNode, batchId);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }


    public CommonUserManagement approveRejectChurnInitiateBySelection(User user, String batchId, boolean isApprove) throws Exception {
        Markup m = MarkupHelper.createLabel("approveRejectChurnInitiate", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            User optUserApr;
            optChurnApprove = DataFactory.getOperatorUsersWithAccess("CHURN_APPROVE").get(0);

            Login.init(pNode).login(optChurnApprove);

            ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
            page.navUserChurnApproval()
                    .selectBatchId(batchId)
                    .checkForApprovalBySelection();

            if (!isApprove) {
                page.rejectBatch();
            }

            page.clickOnSubmitBtn();

            page.checkAllApproval();
            page.approveBySelection();

            //AlertHandle.acceptAlert(pNode);

            if (ConfigInput.isAssert) {
                if (isApprove) {
                    if (Assertion.verifyActionMessageContain("churn.progress.approve", "Verify Churn with Batch Id:" + batchId + " is approved", pNode, batchId)) {
                        // todo check the log file and set the user status
                        user.setStatus(Constants.AUT_STATUS_CHURNED);
                    }
                } else {
                    Assertion.verifyActionMessageContain("churn.progress.reject", "Verify Churn with Batch Id:" + batchId + " is Rejected", pNode, batchId);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }


    /**
     * Set Churned User Details
     *
     * @param user
     * @return
     * @throws IOException
     */
    public CommonUserManagement setChurnUserDetails(User user) throws IOException {

        try {
            String userType = (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) ? "Subscriber" : "Channel User";
            Login.init(pNode).login(optChurnInit);
            ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
            page.navUserChurn_Settlement_Initiate();
            page.enterChurnedMssidn(user.MSISDN);
            page.selectusertypebytext(userType);
            page.clickOnSubmitBtn();
            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(page.isSettlementPageOpen(), true, "Verify Successfully Open the Churn User Settlement Page", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public CommonUserManagement initiateChurnSettlement(User user, User receiver) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateChurnSettlement", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            setChurnUserDetails(user);
            ChurnUser_Page1.init(pNode)
                    .enterReceiverMsisdn(receiver.MSISDN)
                    .clickOnSubmitBtn();

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("churn.settle.success.initiate.ui",
                        "Verify Churn Settlement is initiated successfully for User:" + user.LoginId, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }

    public CommonUserManagement reconCheck() throws Exception {
        Markup m = MarkupHelper.createLabel("Reconciliation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }

    public CommonUserManagement approveChurnSettlement(String msisdn, boolean... isReactivation) throws Exception {
        boolean isReact = isReactivation.length > 0 ? isReactivation[0] : false;
        Markup m = MarkupHelper.createLabel("approveChurnSettlement", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            ChurnUser_Page1.init(pNode)
                    .navUserChurn_Settlement_Approval()
                    .selectRecord(msisdn)
                    .acceptRecord()
                    .confirmApproval();
            if (ConfigInput.isAssert) {
                if (isReact) {
                    Assertion.assertActionMessageContain("churn.react.success.approval",
                            "Verify Successfully Approved Churn Reactivation for MSISDN: " + msisdn, pNode);
                } else {
                    Assertion.assertActionMessageContain("churn.settle.success.approval",
                            "Verify Successfully Approved Churn Settlement for MSISDN: " + msisdn, pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public CommonUserManagement rejectChurnrecords(User channeluser) throws Exception {
        Markup m = MarkupHelper.createLabel("addInitiateSubscriber", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            ChurnUser_Page1.init(pNode)
                    .navUserChurn_Settlement_Approval()
                    .selectRecord(channeluser.MSISDN)
                    .rejectRecord()
                    .clickOnSubmitBtn();
            if (ConfigInput.isAssert) {
                Assertion.verifyErrorMessageContain("churn.reactivation.reject", "churn settlement rejected for: " + channeluser.FirstName + "", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }

    public CommonUserManagement reactivationOfChurnRecord(User churnUser) throws Exception {
        Markup m = MarkupHelper.createLabel("reactivationOfChurnRecord", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode).login(optChurnInit);
            ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
            page.navUserChurn_Settlement_Initiate()
                    .enterChurnedMssidn(churnUser.MSISDN);

            if (churnUser.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                page.selectUsrType("1");
            } else {
                page.selectUsrType("2");
            }

            page.clickOnSubmitBtn();

            page.setchurnReactivation()
                    .clickOnSubmitBtn_ChurnReactivation();

            if (churnUser.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                page.confirmChurnReactivation1();
            } else {
                page.clickNext()
                        .confirmChurnReactivation2();
            }

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("churn.user.modify.message.initiate",
                        "Churn Settlement Initiation done successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public CommonUserManagement initiateChurnReactivation(User churnUser) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateChurnReactivation", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode).login(optChurnInit);
            ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
            page.navUserChurn_Settlement_Initiate()
                    .enterChurnedMssidn(churnUser.MSISDN);

            if (churnUser.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                page.selectUsrType("1");
            } else {
                page.selectUsrType("2");
            }

            page.clickOnSubmitBtn();

            page.setchurnReactivation()
                    .clickOnSubmitBtn_ChurnReactivation();

            if (churnUser.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                AddSubscriber_Page1 p2 = AddSubscriber_Page1.init(pNode);
                p2.selectPrefix();
                p2.uploadIdentityProof(FilePath.uploadFile);
                p2.selectIdentityProof();
                p2.uploadAddressProof(FilePath.uploadFile);
                p2.selectAddressProof();
                p2.uploadPhotoProof(FilePath.uploadFile);
                p2.selectPhotoProof();
                p2.selectRegion();
                page.confirmChurnReactivation1();
            } else {
                page.clickNext()
                        .confirmChurnReactivation2();
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("churn.user.modify.message.initiate",
                        "Verify Churn Reactivation is Initiated Successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * initiateChurnSettlement
     *
     * @return
     * @throws Exception
     */
    public CommonUserManagement initiateChurnSettlement(User channeluser, String type) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateChurnSettlement", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
            page.navUserChurn_Settlement_Initiate();
            page.enterChurnedMssidn(channeluser.MSISDN);
            page.selectusertypebytext(type);
            page.clickOnSubmitBtn();

            if (ConfigInput.isAssert) {
                Assertion.isErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }

    public CommonUserManagement initiateApproveChurnSettlement(User churnedUser, User receiver) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateApproveChurnSettlement" + churnedUser.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).login(optChurnInit);

            setChurnUserDetails(churnedUser);

            ChurnUser_Page1.init(pNode)
                    .enterReceiverMsisdn(receiver.MSISDN)
                    .clickOnSubmitBtn();
            Thread.sleep(1000);
            if (receiver.isBarred()) {
                Assertion.verifyErrorMessageContain("churn.error.receivermsisdn.barred", "Receiver User is barred as receiver", pNode);
            } else {
                Assertion.verifyActionMessageContain("churn.settle.success.initiate", "Churn Settlement Initiation done successfully", pNode);
                Login.init(pNode).login(optChurnApprove);
                approveChurnSettlement(churnedUser.MSISDN);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;

    }

    /**
     * Churn Users
     *
     * @param churnRecords
     * @return
     * @throws Exception
     */
    public String churnUsers(List<Suite_v5_ChurnRecord> churnRecords) throws Exception {
        Markup m = MarkupHelper.createLabel("churnInitiate", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String batchid = null;
        try {
            // Login as Network Admin
            OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("CHURNMGMT_MAIN", Constants.NETWORK_ADMIN).get(0);
            Login.init(pNode)
                    .login(netAdmin);

            batchid = ChurnUser_Page1.init(pNode)
                    .navUserChurnInitiate()
                    .downloadChurnSampleFile()
                    .setChurnchannelUser(churnRecords)
                    .uploadChurnUserExcel()
                    .getbatchID();
            if (ConfigInput.isAssert) {
                Assertion.isErrorInPage(pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return batchid;
    }

    public CommonUserManagement changeFirstTimePasswordWithoutSkippingQuestions(User user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("changeFirstTimePassword: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ChangePasswordPage changPwd = ChangePasswordPage.init(pNode);
            FunctionLibrary fl = new FunctionLibrary(driver);

            Login.init(pNode).login(user.LoginId, ConfigInput.userCreationPassword);
            fl.contentFrame();

            changPwd.setOldPassword(ConfigInput.userCreationPassword);
            changPwd.setNewPassword(user.Password);
            changPwd.setConfirmPassword(user.Password);
            Thread.sleep(3000);
            changPwd.clickSubmit();

            Assertion.verifyActionMessageContain("changePassword.label.success", "Change User Password", pNode);

            user.setIsCreated();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public CommonUserManagement setSecurityQuestions(User user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("setSecurityQuestions: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ArrayList<String> questions = MobiquityGUIQueries.dbGetQuestionsForCategory(Constants.LANGUAGE1, user.CategoryCode);

            SecurityQuestions_pg1 page = new SecurityQuestions_pg1(pNode);

            for (int i = 0; i < questions.size(); i++) {
                page.setAnswer(Constants.ANSWER);
                page.clickNext();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public CommonUserManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }


    public CommonUserManagement postBalance() {
        DBAssertion.prePayerBal = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
        return this;
    }

    public CommonUserManagement approveAssociatedBanks(User user, String type) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveAllAssociatedBanks: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddModifyApproveBank pageOne = AddModifyApproveBank.init(pNode);
            pageOne.navAddModifyDeleteBank();

            List<String> providerNames = DataFactory.getAllProviderNames();
            for (String provider : providerNames) {
                pageOne.selectProvider(provider);
                List<String> linkedBanks = DataFactory.getAllBankNamesLinkedToProvider(provider);
                for (String bank : linkedBanks) {
                    pageOne.navAddModifyDeleteBank();
                    pageOne.selectProvider(provider);
                    pageOne.selectBank(bank);

                    pageOne.setFromDate();
                    pageOne.setToDate();
                    pageOne.clickSubmit();

                    pageOne.checkBanksForUser(user.MSISDN);
                    pageOne.approve();

                    if (!ConfigInput.isAssert) {
                        continue;
                    }

                    if (type.contains(Constants.CREATE)) {
                        Assertion.verifyActionMessageContain("channeluser.bankapproved",
                                "Verify Bank Is successfully Associated",
                                pNode, provider, user.MSISDN, bank);
                    } else if (type.contains(Constants.MODIFY)) {
                        String expected = MessageReader.getDynamicMessage("channeluser.bankapproved", provider, user.MSISDN, bank);
                        Assertion.verifyDynamicActionMessageContain(expected, "Bank Association", pNode);
                        Assertion.assertErrorInPage(pNode);
                    } else if (type.contains(Constants.DELETE)) {
                        String expected = MessageReader.getDynamicMessage("channeluser.bankapproved", provider, user.MSISDN, bank);
                        Assertion.verifyDynamicActionMessageContain(expected, "Bank Association", pNode);
                        Assertion.assertErrorInPage(pNode);
                    }
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * @param gradeCode
     * @return
     * @throws Exception
     */
    public User getTempChannelUser(String catCode, String... gradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null, usrGradeCode = null;
        boolean gradeReq = false;

        if (gradeCode.length > 0) {
            gradeReq = true;
            usrGradeCode = gradeCode[0];
        }

        if (usrGradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            usrGradeCode = grade.GradeCode;
        }

        List<User> sList = DataFactory.getTempChannelUserList(catCode, Constants.STATUS_ACTIVE);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                String dbStatus = MobiquityGUIQueries.getUserStatus(usr.MSISDN, usr.CategoryCode);
                if (gradeReq) {
                    if (dbStatus != null && dbStatus.equals(Constants.STATUS_ACTIVE) && usr.GradeCode.equals(usrGradeCode)) {
                        return usr;
                    }
                } else if (dbStatus != null && dbStatus.equals(Constants.STATUS_ACTIVE)) {
                    return usr;
                } else {
                    // delete this entry from Excel, as its no longer a valid User
                    if (dbStatus == null)
                        ExcelUtil.removeTempUser(usr);
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = new User(catCode);
        usr.GradeCode = usrGradeCode;
        usr.GradeName = DataFactory.getGradeName(usrGradeCode);

        ChannelUserManagement.init(pNode).createChannelUser(usr);

        if (!usr.isCreated.equalsIgnoreCase("Y")) {
            Assert.fail("Failed to create User");
            return null;
        } else
            usr.writeDataToExcel(true);

        return usr;
    }


    public CommonUserManagement approveAssociatedSpecificBank(User user, String bankName, String type) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveAllAssociatedBanks: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddModifyApproveBank pageOne = AddModifyApproveBank.init(pNode);
            pageOne.navAddModifyDeleteBank();

            List<String> providerNames = DataFactory.getAllProviderNames();
            for (String provider : providerNames) {
                pageOne.selectProvider(provider);

                pageOne.navAddModifyDeleteBank();
                pageOne.selectProvider(provider);
                pageOne.selectBank(bankName);

                pageOne.setFromDate();
                pageOne.setToDate();
                pageOne.clickSubmit();

                pageOne.checkBanksForUser(user.MSISDN);
                pageOne.approve();

                if (ConfigInput.isAssert) {

                    if (type.contains(Constants.CREATE)) {
                        Assertion.verifyActionMessageContain("channeluser.bankapproved",
                                "Verify Bank Is successfully Associated",
                                pNode, provider, user.MSISDN, bankName);
                    }
                    if (type.contains(Constants.MODIFY)) {
                        String expected = MessageReader.getDynamicMessage("channeluser.bankapproved", provider, user.MSISDN, bankName);
                        Assertion.verifyDynamicActionMessageContain(expected, "Bank Association", pNode);
                        Assertion.assertErrorInPage(pNode);
                    }
                    if (type.contains(Constants.DELETE)) {
                        String expected = MessageReader.getDynamicMessage("channeluser.bankapproved", provider, user.MSISDN, bankName);
                        Assertion.verifyDynamicActionMessageContain(expected, "Bank Association", pNode);
                        Assertion.assertErrorInPage(pNode);
                    }

                }

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * @param usrGradeCode
     * @return
     * @throws Exception
     */
    public User getTempChannelUser(String catCode, String merchantType, String usrGradeCode) throws Exception {
        ExcelUtil.writeTempUserHeaders();
        String gradeName = null;
        //boolean gradeReq = false;


        if (usrGradeCode == null) {
            GradeDB grade = DataFactory.getGradesForCategory(catCode).get(0);
            gradeName = grade.GradeName;
            usrGradeCode = grade.GradeCode;
        }


        List<User> sList = DataFactory.getTempChannelUserList(catCode, Constants.STATUS_ACTIVE);
        if (sList.size() > 0) {
            // check for user status in DB
            for (User usr : sList) {
                String dbStatus = MobiquityGUIQueries.getUserStatus(usr.MSISDN, usr.CategoryCode);

                if (dbStatus.equals(Constants.STATUS_ACTIVE) && usr.MerchantType.equalsIgnoreCase(merchantType)) {
                    return usr;
                } else {
                    // delete this entry from Excel, as its no longer a valid User
                    if (dbStatus == null)
                        ExcelUtil.removeTempUser(usr);
                }
            }
        }

        // if valid user is not found, Create a new User and return the same
        User usr = new User(catCode);
        usr.setMerchantType(merchantType);
        usr.GradeCode = usrGradeCode;
        usr.GradeName = DataFactory.getGradeName(usrGradeCode);

        ChannelUserManagement.init(pNode).createChannelUser(usr);

        if (!usr.isCreated.equalsIgnoreCase("Y")) {
            Assert.fail("Failed to create User");
            return null;
        } else
            usr.writeDataToExcel(true);

        return usr;
    }

}
