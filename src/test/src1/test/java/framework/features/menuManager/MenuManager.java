package framework.features.menuManager;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.typesafe.config.ConfigException;
import framework.pageObjects.menuManager.MenuManagerChecker_Page;
import framework.pageObjects.menuManager.MenuManager_Page;
import framework.pageObjects.menuManager.MenuManager_Page2;
import framework.pageObjects.menuManager.MenuServlet;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import jdk.nashorn.internal.ir.CatchNode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;

public class MenuManager {

    private static WebDriver driver;
    private static ExtentTest pNode;


    public static MenuManager init(ExtentTest t1) throws Exception {
        try {
            driver = DriverFactory.getDriver();
            pNode = t1;
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new MenuManager();
    }

    public void  addMenu(String url, String category, String menuName) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("Add Menu", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            driver.navigate().to(url);

            MenuManager_Page menuManager  = new MenuManager_Page(pNode);
            menuManager.selectCategory(category);
            Thread.sleep(2000);
            try{
            menuManager.clickGatheruserInput();}
            catch (Exception e){
                pNode.info(e);
                menuManager.clickDiscardAllLink();
                Thread.sleep(2000);
                menuManager.selectCategory(category);
                Thread.sleep(2000);
                menuManager.clickGatheruserInput();
            }
            menuManager.enterText("Welcome to ECOCASH");
            Thread.sleep(2000);
            menuManager.saveHeaderText();
            Thread.sleep(2000);
            menuManager.editLanguage();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage("Welcome to ECOCASH");
            menuManager.clickOnSaveBtn();
            Thread.sleep(3000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
            menuManager.enterMenuLabel(menuName);
            Thread.sleep(2000);
            menuManager.saveMenu();
            Thread.sleep(2000);
            menuManager.clickDefineInOtherlang();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage(menuName);
            menuManager.clickOnSaveBtn();
            Thread.sleep(2000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
            menuManager.clickFieldNameBtn();
            Thread.sleep(2000);
            menuManager.enterFieldName("PIN");
            menuManager.clickSavePlaceholder();
            Thread.sleep(2000);


        } catch (Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void addMainMenuItem(String header,String menuName) throws IOException{
        try{
            Markup m = MarkupHelper.createLabel("Add Main Menu Item", ExtentColor.BLUE);
            pNode.info(m);
            MenuManager_Page menuManager  = new MenuManager_Page(pNode);
            MenuManager_Page2 menuManager_page2  = new MenuManager_Page2(pNode);

            menuManager.enterText(header);
            menuManager.saveHeaderText();
            Thread.sleep(2000);
            menuManager.editLanguage();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage(header);
            menuManager.clickOnSaveBtn();
            Thread.sleep(3000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
            menuManager_page2.clickAddMenuBtn();
            menuManager_page2.enterMenuName(menuName);
            menuManager_page2.clickOnSaveMenuItem();
            Thread.sleep(2000);
            menuManager.clickDefineInOtherlang();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage(menuName);
            menuManager.clickOnSaveBtn();
            Thread.sleep(2000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
            //menuManager.clickNextBtn();
            menuManager_page2.clickNextMAndSM();
        }
        catch(Exception e){ Assertion.raiseExceptionAndContinue(e, pNode);}
    }

    public void addSubMenuItem(String header,String menuName) throws IOException{
        try{
            Markup m = MarkupHelper.createLabel("Add Sub Menu Item", ExtentColor.BLUE);
            pNode.info(m);
            MenuManager_Page menuManager  = new MenuManager_Page(pNode);
            MenuManager_Page2 menuManager_page2  = new MenuManager_Page2(pNode);

            menuManager.enterText(header);
            menuManager.saveHeaderText();
            Thread.sleep(2000);
            menuManager.editLanguage();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage(header);
            menuManager.clickOnSaveBtn();
            Thread.sleep(3000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
            menuManager_page2.clickAddMenuBtn();
            menuManager_page2.enterMenuName(menuName);
            menuManager_page2.clickOnSaveMenuItem();
            Thread.sleep(2000);
            menuManager.clickDefineInOtherlang();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage(menuName);
            menuManager.clickOnSaveBtn();
            Thread.sleep(2000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
            menuManager_page2.clickNextMAndSM();
        }
        catch(Exception e){ Assertion.raiseExceptionAndContinue(e, pNode);}
    }

    public void submitForApproval(String remarks) throws IOException{
        try{
            Markup m = MarkupHelper.createLabel("Submit for Approval", ExtentColor.BLUE);
            pNode.info(m);
            MenuManager_Page menuManager  = new MenuManager_Page(pNode);
            menuManager.clickSubmitForApproval();
            Thread.sleep(2000);
            menuManager.clickCalenderIcon();
            Thread.sleep(2000);
            menuManager.clickToSelectActiveDate();
            Thread.sleep(2000);
            menuManager.enterRemarks(remarks);
            menuManager.clickSubmitBtnToApprove();
            Thread.sleep(2000);
        }
        catch(Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void associateTerminate(String successText, String errorText) throws IOException{
        try {
            Markup m = MarkupHelper.createLabel("Associate Terminate service", ExtentColor.BLUE);
            pNode.info(m);

            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(pNode);
            MenuManager_Page menuManager_page = new MenuManager_Page(pNode);

            menuManager_page2.enterSuccessText(successText);
            menuManager_page2.clickToSaveSuccessText();
            Thread.sleep(2000);
            menuManager_page2.clickToEditSuccessTextLang();
            Thread.sleep(2000);
            menuManager_page.enterInOtherLanguage(successText);
            menuManager_page.clickOnSaveBtn();
            Thread.sleep(3000);
            menuManager_page.closepopupwindow();
            Thread.sleep(2000);

            menuManager_page2.enterErrorText(errorText);
            menuManager_page2.clickToSaveErrorText();
            Thread.sleep(2000);
            menuManager_page2.clickToEditErrorTextLang();
            Thread.sleep(2000);
            menuManager_page.enterInOtherLanguage(errorText);
            menuManager_page.clickOnSaveBtn();
            Thread.sleep(3000);
            menuManager_page.closepopupwindow();
            Thread.sleep(2000);
        }
        catch(Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyMenuLabel(String menuName) throws IOException{
        try{
            Markup m = MarkupHelper.createLabel("Modify Menu Label", ExtentColor.BLUE);
            pNode.info(m);

            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(pNode);
            MenuManager_Page menuManager = new MenuManager_Page(pNode);
            menuManager_page2.enterMenuName(menuName);
            menuManager_page2.clickToSaveEditMenu();
            Thread.sleep(2000);
            menuManager.clickDefineInOtherlang();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage(menuName);
            menuManager.clickOnSaveBtn();
            Thread.sleep(2000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
        }
        catch (Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void rejectAtChecker(String url,String category) throws IOException{
        try {
            Markup m = MarkupHelper.createLabel("Reject Menu at Checker", ExtentColor.BLUE);
            pNode.info(m);

            MenuManagerChecker_Page checkerPage = new MenuManagerChecker_Page(pNode);
            MenuManager_Page makerPage = new MenuManager_Page(pNode);

            driver.navigate().to(url);
            checkerPage.selectCategory(category);
            Thread.sleep(2000);
            checkerPage.clickCheckerActionBtn();
            Thread.sleep(2000);
            makerPage.enterRemarks("Rejecting Menu via Automation");
            checkerPage.clickRejectBtn();
        }catch(Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void approveAtChecker(String url, String category,String remarks, int days) throws IOException{
        try {
            Markup m = MarkupHelper.createLabel("Approve Menu at Checker", ExtentColor.BLUE);
            pNode.info(m);

            MenuManagerChecker_Page checkerPage = new MenuManagerChecker_Page(pNode);
            MenuManager_Page makerPage = new MenuManager_Page(pNode);

            driver.navigate().to(url);
            checkerPage.selectCategory(category);
            Thread.sleep(2000);
            checkerPage.clickCheckerActionBtn();
            makerPage.clickCalenderIcon();
            Thread.sleep(2000);
            makerPage.selectDate(days);
            Thread.sleep(2000);
            makerPage.enterRemarks(remarks);
            checkerPage.clickApproveBtn();
            checkerPage.clickContinueApprovalBtn();
        }catch(Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public String[] checkMenuViaChecker(String url,String category,String[] menuElements) throws IOException {
        String[] values=new String[2];
        try {
            Markup m = MarkupHelper.createLabel("Approve Menu at Checker", ExtentColor.BLUE);
            pNode.info(m);

            MenuManagerChecker_Page checkerPage = new MenuManagerChecker_Page(pNode);
            MenuManager_Page makerPage = new MenuManager_Page(pNode);

            driver.navigate().to(url);
            checkerPage.selectCategory(category);
            values[0] = checkerPage.getRemarks();
            checkerPage.clickSelectLanguage();
            checkerPage.selectLanguageFromList("ENGLISH");
            checkerPage.clickProceedBtn();
            values[1] = checkerPage.checkMenuAtChecker(menuElements);
        }
            catch(Exception e){
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        return values;
    }

    public void addmultipleMenus(int number, String[] menuNames) throws IOException{
      try{
          Markup m = MarkupHelper.createLabel("Add multiple menu items", ExtentColor.BLUE);
          pNode.info(m);
        MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(pNode);
        MenuManager_Page menuManager = new MenuManager_Page(pNode);
        for(int i=0;i<number;i++){
            menuManager_page2.clickAddMenuBtn();
            driver.findElement(By.xpath("//tr["+(i+1)+"]//*[@id='menuDiv']/input")).sendKeys(menuNames[i]);
            menuManager_page2.clickOnSaveMenuItem();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id='menuListTable']//tr["+(i+1)+"]//img[@title='Define in Other Language']")).click();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage(menuNames[i]);
            menuManager.clickOnSaveBtn();
            Thread.sleep(2000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
        }
    }catch(Exception e){
          Assertion.raiseExceptionAndContinue(e, pNode);
      }
    }

    public void enterValidationDetail() throws IOException{
        try{
            Markup m = MarkupHelper.createLabel("Validation Details", ExtentColor.BLUE);
            pNode.info(m);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(pNode);
            MenuManager_Page menuManager = new MenuManager_Page(pNode);

            menuManager.clickValidationBtn();
            menuManager.selectValidationcheck("equals");
            menuManager.enterValueforValidation("[0-9][0-9][0-9][0-9]");
            menuManager.enterErrorMessagForValidation("Pin Incorrect");
            menuManager.clickSaveInput();
            Thread.sleep(2000);
            menuManager.clickButtonForLangInValidation();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage("Pin Incorrect");
            menuManager.clickOnSaveBtn();
            Thread.sleep(3000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
            menuManager.clickCloseBtnForValidation();
        }
            catch(Exception e){
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
    }

    public void enterErrorMessageDetail() throws IOException{
        try{
            Markup m = MarkupHelper.createLabel("Error Message details", ExtentColor.BLUE);
            pNode.info(m);
            MenuManager_Page menuManager = new MenuManager_Page(pNode);

            menuManager.clickErrorMessageBtn();
            menuManager.enterErrorMessage("PIN invalid");
            menuManager.clickErrorMsgEditLang();
            Thread.sleep(2000);
            menuManager.enterInOtherLanguage("PIN invalid");
            menuManager.clickOnSaveBtn();
            Thread.sleep(3000);
            menuManager.closepopupwindow();
            Thread.sleep(2000);
            menuManager.clickSaveErrorMessageBtn();
        }
        catch(Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void enterPrefixDetail() throws IOException{
        try{
            Markup m = MarkupHelper.createLabel("Prefix detail", ExtentColor.BLUE);
            pNode.info(m);
            MenuManager_Page menuManager = new MenuManager_Page(pNode);

            menuManager.clickPrefixBtn();
            menuManager.enterPrefixRemoval("12");
            menuManager.clickSavePrefixButton();
        }
        catch(Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public String checkMenuAtServlet(String inputValue) throws IOException{
        String output=null;
        try{
            Markup m = MarkupHelper.createLabel("Check Menu", ExtentColor.BLUE);
            pNode.info(m);
            MenuServlet menuServlet = new MenuServlet(pNode);

            menuServlet.enterInput(inputValue);
            menuServlet.clickGoButton();
            output = menuServlet.getOutput();
        }
        catch(Exception e){
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return output;
    }
}
