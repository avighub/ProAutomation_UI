package framework.features.inventoryManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.features.common.Login;
import framework.pageObjects.bulkPayoutTool.BulkPayoutDashboard_page1;
import framework.pageObjects.inventoryManagementSystem.InitiateBulkUpload_Page01;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InventoryManagement {
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static User merBulkInitiator, merBulkApprover, merBulkDashBoadr;
    // File headers
    String CommonFileHeaderinventory = "Product Id*,Product Code*";
    String CommonFileHeaderCatalogue = "Product Code*,Product Name*,Product Category*,Product Brand*,Product Price*,Product Description";
    private FileWriter fileWriter = null;
    private String price = "100.00";
    private String productName = "PN" + DataFactory.getRandomString(4);
    private String productCatagory = "CAT" + DataFactory.getRandomString(4);
    private String productBrand = "PB" + DataFactory.getRandomString(4);
    private String productCode = "CODE" + DataFactory.getRandomNumber(4);
    private String productID = "" + DataFactory.getRandomNumber(4);


    public static InventoryManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        if (merBulkInitiator == null) {
            merBulkInitiator = DataFactory.getChannelUserWithAccess("IMS_BULK_INIT");
            merBulkApprover = DataFactory.getChannelUserWithAccess("IMS_BULK_APPROVE");
            merBulkDashBoadr = DataFactory.getChannelUserWithAccess("IMS_BULK_DASHBOARD");
        }

        return new InventoryManagement();
    }

    public String initiateBulkUpload(String service, String fileName) throws IOException {
        String bulkID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateBulkUpload", ExtentColor.BLUE);
            pNode.info(m);

            InitiateBulkUpload_Page01 page = InitiateBulkUpload_Page01.init(pNode);
            page.navigateToInitiateBulkUploadLink();
            Thread.sleep(2000);

            page.serviceButton_Click();
            Thread.sleep(2000);

            page.service_SelectValue(service);
            Thread.sleep(2000);

            page.fileUpload(fileName);
            Thread.sleep(2000);

            page.remark_SetText("OK");

            page.submitButton_Click();
            Thread.sleep(2000);

            if (ConfigInput.isAssert) {
                String actualMessage = page.getMessage();
                if (actualMessage != null) {

                    if (Assertion.verifyMessageContain(actualMessage,
                            "bulkupload.success.initiate.product.inventory.batchNum",
                            "initiate Bulk Upload", pNode)) {

                        //  return actualMessage.split("batch ID:")[1].trim();
                        bulkID = actualMessage.split("batch ID: ")[1].trim();

                    } else {
                        return null;
                    }

                } else {
                    Utils.captureScreen(pNode);
                    pNode.fail("Failed to upload:" + service);
                    Assert.fail("Failed to upload, exiting the test");
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return bulkID;
    }

    public String verifyIMSDashboard(String serviceName, String batchId, boolean isApproved, boolean expectSuccess) throws IOException {
        String strApproveReject = (expectSuccess) ? "Approved" : "Rejected";
        ArrayList<String> errors = new ArrayList<String>();
        String fileName = null;
        try {
            Markup m = MarkupHelper.createLabel("verifyIMSDashboard", ExtentColor.BLUE);
            pNode.info(m);

            Login.init(pNode).login(merBulkInitiator);
            InitiateBulkUpload_Page01 page = InitiateBulkUpload_Page01.init(pNode);

            page.navigateToBulkUploadDashBoardLink();
            Thread.sleep(2000);
            page.sortNewest();
            Thread.sleep(2000);
            page.selectServiceType(serviceName);
            Thread.sleep(2000);
            page.selectStatusFilter(isApproved);

            // get all the requests and check for the bacthc Id
            List<WebElement> avaibaleEntry = driver.findElements(By.cssSelector(".lh.collapse_btnPD1"));

            boolean isFound = false;
            for (WebElement elem : avaibaleEntry) {
                elem.findElement(By.xpath("parent::*")).click();
                Thread.sleep(1000);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    isFound = true;
                    break;
                }
            }


            if (isFound) {
                pNode.pass("Successfully found the entry for Bulk Batch Id:" + batchId + ". Approval status: " + strApproveReject);
                fileName = page.downloadStatusFile();
                Thread.sleep(1000);
                checkStatusFile(expectSuccess);


            } else {
                Assertion.markAsFailure("Failed to find entry for Bulk Batch Id:" + batchId + ". Approval status: " + strApproveReject);
            }
            Utils.captureScreen(pNode);


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return fileName;
    }

    public String generateProductInventoryCsvFile() {

        String filePath = "uploads/" + "ProductInventory" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        File f = new File(fileName);

        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(CommonFileHeaderinventory);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append(productID);
            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(productCode);
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();

        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public String generateProductCatalogueCsvFile() {

        String filePath = "uploads/" + "ProductCatalogue" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";

        File f = new File(fileName);

        try {
            fileWriter = new FileWriter(f);

            fileWriter.append(CommonFileHeaderCatalogue);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append(productCode);
            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(productName);
            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(productCatagory);
            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(productBrand);
            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(price);
            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append("Automation");
            fileWriter.append("\n");
                /*}
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();

        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public InventoryManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public void checkStatusFile(boolean status) throws Exception {
        Markup m = MarkupHelper.createLabel("checkStatusFile", ExtentColor.BLUE);
        pNode.info(m);
        String matchStr = (status) ? "SUCCEEDED" : "FAILED";

        try {

           /* FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
            BulkPayoutDashboard_page1.init(pNode)
                    .getstatusFile();*/


            String status_File = FilePath.dirFileDownloads + "/bulk-upload-" + BulkPayoutDashboard_page1.init(pNode).getbatchID() + ".csv";

            File file = new File(status_File);
            Scanner s = new Scanner(file);

            boolean success = true;
            String[] matcherArray = {matchStr};

            int lineIndex = 1;
            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (lineIndex != 2) {
                    lineIndex++;
                    continue;
                }

                //reading the 2nd line for message
                for (int i = 0; i < matcherArray.length; i++) {
                    if (line.contains(matcherArray[i])) {
                        pNode.pass("found text as " + matcherArray[i]);
                    } else {
                        success = false;
                        pNode.fail("commission disbursement failed");
                        break;
                    }
                }
                if (success) {
                    pNode.pass("Success");
                }
                //now we don't want to read further
                break;
            }
            s.close();
        } catch (IOException ioex) {
            System.out.println(ioex);
        }

    }

    public void closeResources() {
        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error while flushing/closing fileWriter !!!");
            e.printStackTrace();
        }
    }
}
