/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: surya.dhal
 *  Date: 22-May-2018
*  Purpose: Feature Class of Category Management.
*/
package framework.features.categoryManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.Category;
import framework.entity.Domain;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.CategoryManagement.AddCategory_Page1;
import framework.pageObjects.CategoryManagement.ApproveCategory_Page1;
import framework.pageObjects.CategoryManagement.ApproveCategory_Page2;
import framework.pageObjects.CategoryManagement.CategoryModification_Page1;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Map;

/**
 * Created by surya.dhal on 5/23/2018.
 */
public class CategoryManagement {

    private static WebDriver driver;
    private static FunctionLibrary fl;
    private static ExtentTest pNode;
    private static OperatorUser catApprover, catInitiator;
    Map<String, String> categoryDetails;

    public static CategoryManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        if (catApprover == null) {
            catApprover = DataFactory.getOperatorUserWithAccess("CAT_APPRL");
            catInitiator = DataFactory.getOperatorUserWithAccess("ADD_CAT");
        }

        return new CategoryManagement();

    }

    /**
     * Initiate New Category
     *
     * @param cat
     */
    public void initiateNewCategory(Category cat) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateNewCategory", ExtentColor.ORANGE);
        pNode.info(m);
        try {
            AddCategory_Page1 page1 = new AddCategory_Page1(pNode);
            Login.init(pNode).login(catInitiator);
            page1.navigateCategoryManagementPage();

            if (Assertion.checkErrorMessageContain("category.AI.exists", "Check Error Present or not", pNode)) {
                Login.init(pNode).login(catApprover);
                rejectPendingCategory();// reject any category for approval
                Login.init(pNode).login(catInitiator);
                page1.navigateCategoryManagementPage();
            }
            page1.enterCategoryName(cat.getCategoryName());
            page1.enterCategoryCode(cat.getCategoryCode());
            page1.selectDomainByValue(cat.getDomainCode());
            Thread.sleep(5000);
            page1.selectParentCategory();
            page1.clickOnSubmitButton();
            if (ConfigInput.isConfirm) {
                page1.clickOnConfirmButton();
                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("category.success.msg",
                            "Verify Successfully initiated add category", pNode);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Feature to Add Category in system.
     */
    public CategoryManagement addInitiateCategory(Category category) throws Exception {
        OperatorUser initiator = DataFactory.getOperatorUserWithAccess("ADD_CAT");

        try {
            Markup m = MarkupHelper.createLabel("addInitiateCategory", ExtentColor.ORANGE);
            pNode.info(m);
            Login.init(pNode).login(initiator);
            AddCategory_Page1 page1 = new AddCategory_Page1(pNode);
            page1.navigateCategoryManagementPage();
            if (Assertion.checkErrorMessageContain("category.AI.exists", "Check Error Present or not", pNode)) {
                rejectPendingCategory();
                Login.init(pNode).login(initiator);
                page1.navigateCategoryManagementPage();
            }


                page1.enterCategoryName(category.getCategoryName());
                page1.enterCategoryCode(category.getCategoryCode());

                if(category.getDomainCode() !=null)
                page1.selectDomainByValue(category.getDomainCode());

                if(category.getParentCategory() !=null)
                page1.selectParentCategoryByValue(category.getParentCategory());

            page1.clickOnSubmitButton();
            if (ConfigInput.isConfirm)
                page1.clickOnConfirmButton();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("category.success.msg", "Verify Successfully initiated add category", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;

    }


    /**
     * Feature to Add Category in system.
     */
    public CategoryManagement rejectPendingCategory() throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("rejectPendingCategory", ExtentColor.ORANGE);
            pNode.info(m);

            ApproveCategory_Page1 page1 = new ApproveCategory_Page1(pNode);
            Map<String, String> categoryDetails = MobiquityGUIQueries.fetchInitiatedCategoryDetails();
            page1.navAddCategoryApprovalPage();
            page1.selectDomainByValue(categoryDetails.get("DOMAIN_CODE"));

            page1.selectParentCategoryByValue(categoryDetails.get("PARENT_CATEGORY_CODE"));
            page1.selectCategoryNameByValue(categoryDetails.get("CATEGORY_CODE"));
            page1.clickRejectButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("category.success.msg.rejected", "Verify Reject Success Message", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;

    }

    /**
     * Approving a category with minimum service and bearers - only for testing purpose
     *
     * @return
     * @throws Exception
     */
    public CategoryManagement approveCategoryDefaultServiceForTesting() throws Exception {
        String expectedMessage = null, actualMessage, Message = null;
        try {
            Markup m = MarkupHelper.createLabel("approveCategoryDefaultServiceForTesting", ExtentColor.GREEN);
            pNode.info(m);
            ApproveCategory_Page1 page1 = new ApproveCategory_Page1(pNode);
            ApproveCategory_Page2 page2 = new ApproveCategory_Page2(pNode);
            ApproveCategory_Page2 page3 = new ApproveCategory_Page2(pNode);
            Login.init(pNode).login(catApprover);
            categoryDetails = MobiquityGUIQueries.fetchInitiatedCategoryDetails();

            page1.navAddCategoryApprovalPage();
            page1.selectDomainByValue(categoryDetails.get("DOMAIN_CODE"));
            page1.selectParentCategoryByValue(categoryDetails.get("PARENT_CATEGORY_CODE"));
            page1.selectCategoryNameByValue(categoryDetails.get("CATEGORY_CODE"));

            page1.clickOnApproveButton();

            page2.selectSingleServiceWithBearer();
            page2.clickOnNextButton();

            page2.selectSingleServiceWithBearer();
            page2.clickOnNextButton();

            page3.selectSingleServiceWithBearer();
            page3.clickOnNextButton();

            page3.clickCheckAll();

            page3.clickFinalSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("Category.Approved.Successfully",
                        "Verify successfully approving category with minimum services and bearers for testing", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * Method to Approve or Reject a Category Initiation.
     */
    public CategoryManagement approveAddCategory() throws IOException {
        String expectedMessage = null, actualMessage, Message = null;
        try {
            Markup m = MarkupHelper.createLabel("approveAddCategory", ExtentColor.GREEN);
            pNode.info(m);
            ApproveCategory_Page1 page1 = new ApproveCategory_Page1(pNode);
            ApproveCategory_Page2 page2 = new ApproveCategory_Page2(pNode);
            ApproveCategory_Page2 page3 = new ApproveCategory_Page2(pNode);
            Login.init(pNode).login(catApprover);
            categoryDetails = MobiquityGUIQueries.fetchInitiatedCategoryDetails();

            page1.navAddCategoryApprovalPage();
            page1.selectDomainByValue(categoryDetails.get("DOMAIN_CODE"));
            page1.selectParentCategoryByValue(categoryDetails.get("PARENT_CATEGORY_CODE"));
            page1.selectCategoryNameByValue(categoryDetails.get("CATEGORY_CODE"));

            page1.clickOnApproveButton();

            page2.selectAllCheckBox();
            page2.clickOnNextButton();

            page2.selectAllCheckBox();
            page2.clickOnNextButton();

            page3.selectAllCheckBox();
            page3.clickOnNextButton();

            page3.clickCheckAll();

            page3.clickFinalSubmit();

            expectedMessage = MessageReader.getMessage("Category.Approved.Successfully", null);
            Message = "Verify Approve Success Message.";

            if (ConfigInput.isAssert) {
                actualMessage = Assertion.getActionMessage();
                Assertion.verifyEqual(actualMessage, expectedMessage, Message, pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Method to Modify category.
     */
    public void modifyCategory() {
        try {
            Markup m = MarkupHelper.createLabel("modifyCategory", ExtentColor.ORANGE);
            categoryDetails = MobiquityGUIQueries.fetchInitiatedCategoryDetails();
            pNode.info(m);
            CategoryModification_Page1 page1 = new CategoryModification_Page1(pNode);
            page1.navCategoryManagementLink();
            page1.clickOnModify(categoryDetails.get("CATEGORY_CODE"));
            page1.clickOnSubmitButton();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("category.success.msg.modified.initiation", "Verify Message when Modify Successful", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * MOVED FROM OPERATOR MANAGEMENT
     */
    public CategoryManagement addAndApproveCategory(Category cat, String domainName) throws Exception {

        Login.init(pNode).login(catInitiator);
        addCategory(cat,domainName);
        Login.init(pNode).login(catApprover);
        approveCategory(domainName,cat.getCategoryName());

        return this;
    }


    /**
     * This is should be used while adding Category
     * //TODO implement the Domain name in Category Clas. Category class need to be refactored
     * @param cat
     * @return
     * @throws Exception
     */
    public CategoryManagement addCategory(Category cat) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("addcategory",
                    ExtentColor.BLUE);
            pNode.info(m);

            AddCategory_Page1 page = new AddCategory_Page1(pNode);

            page.navigateCategoryManagementPage();
            page.enterCategoryName(cat.getCategoryName());
            page.enterCategoryCode(cat.getCategoryCode());
            page.selectDomain(cat.getDomainName());
            page.selectParentCategory();
            page.clickOnSubmitButton();

            if(ConfigInput.isConfirm){
                page.clickOnConfirmButton();
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("category.success.msg", "Category Add Initiated Successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public CategoryManagement addCategory(Category cat, String domainName) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("addcategory",
                     ExtentColor.BLUE);
            pNode.info(m);

            AddCategory_Page1 page = new AddCategory_Page1(pNode);

            page.navigateCategoryManagementPage();
            page.enterCategoryName(cat.getCategoryName());
            page.enterCategoryCode(cat.getCategoryCode());
            page.selectDomain(domainName);
            page.selectParentCategory();
            page.clickOnSubmitButton();

            if(ConfigInput.isConfirm){
                page.clickOnConfirmButton();
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("category.success.msg", "Category Add Initiated Successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public CategoryManagement modifyCategory(OperatorUser user, String cat) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("modifycategory "
                    + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m);

            CategoryModification_Page1 page = new CategoryModification_Page1(pNode);

            page.navCategoryManagementLink();
            page.selectParticularCategoryToModify(cat);

            if (ConfigInput.isAssert) {

                Assertion.verifyActionMessageContain("category.modify.parent", "Parent Category modified successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public CategoryManagement approveCategory(String domainName, String catName) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("approveCategory "
                   , ExtentColor.BLUE);
            pNode.info(m);

            ApproveCategory_Page1 page = new ApproveCategory_Page1(pNode);
            AddCategory_Page1 page2 = new AddCategory_Page1(pNode);

            page.navAddCategoryApprovalPage();
            page.selectDomain(domainName);
            page2.selectParentCategory();
            page.selectCategoryName(catName);
            page.clickOnApproveButton();

            ApproveCategory_Page2 page1 = new ApproveCategory_Page2(pNode);

            page1.clickOnWalletApproval();
            page1.clickOnBankApproval();
            page1.clickOnInitiatorApproval();
            page1.clickOnAllServicesApproval();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("Category.Approved.Successfully", "Category Approved Successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public CategoryManagement rejectCategory(OperatorUser user, String val, String val1, String val2) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("rejectcategory "
                    + "-" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m);

            ApproveCategory_Page1 page = new ApproveCategory_Page1(pNode);
            AddCategory_Page1 page2 = new AddCategory_Page1(pNode);

            page.navAddCategoryApprovalPage();
            page.selectDomain(val);
            page2.selectParentCategory();
            page.selectCategoryName(val2);
            Thread.sleep(2000);
            page.clickRejectButton();

            if (ConfigInput.isAssert) {

                Assertion.verifyActionMessageContain("Category.Rejected.Successfully", "Category Rejected Successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     *  Method to check if Category is already in initiated State.
     *  This method will return true if Category is already initiated. If you want to reject the already initiated Category
     *  the pass true in method argument
     * @param wantToRejectInitiatedCat
     * @return
     * @throws Exception
     */
    public boolean checkForCategoryAlreadyInitiatedError(boolean wantToRejectInitiatedCat) throws Exception {
        Utils.createLabelForMethod("checkForCategoryAlreadyInitiatedError",pNode);
        AddCategory_Page1 page1 = new AddCategory_Page1(pNode);
        Login.init(pNode).login(catInitiator);
        page1.navigateCategoryManagementPage();

        if (Assertion.checkErrorPresent()!=null  && Assertion.checkErrorMessageContain("category.AI.exists", "Check Error Present or not", pNode)) {
            if(wantToRejectInitiatedCat){
                Login.init(pNode).login(catApprover);
                rejectPendingCategory();
            }
            return true;
        }

        return false;
    }

}
