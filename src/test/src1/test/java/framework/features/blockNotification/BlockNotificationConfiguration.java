package framework.features.blockNotification;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.PageInit;
import framework.pageObjects.blockNotification.BlockNotificationConfiguration_Page2;
import framework.util.common.Assertion;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

public class BlockNotificationConfiguration {

    private static ExtentTest pNode;

    public static BlockNotificationConfiguration init(ExtentTest t1) {
        pNode = t1;
        return new BlockNotificationConfiguration();
    }

    public BlockNotificationConfiguration blockNotificationTemplate(String sourceName, String serviceName, String[] templateName) throws IOException {

        try{
            Markup m = MarkupHelper.createLabel("blockNotificationTemplate", ExtentColor.TEAL);
            pNode.info(m);
        BlockNotificationConfiguration_Page2.init(pNode)
                .navigateToBlockNotification()
                .selectSourceName(sourceName.toLowerCase())
                .selectServiceType(serviceName)
                .clickSubmit()
                .selectNotificationTemplate(templateName)
                .clickSave()
                .accept();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("message.record.save", "Verify Notification Template blocked successfully", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public BlockNotificationConfiguration unblockNotificationTemplate(String sourceName, String serviceName, String[] templateName) throws IOException{

        try{
            Markup m = MarkupHelper.createLabel("unblockNotificationTemplate", ExtentColor.TEAL);
            pNode.info(m);
        BlockNotificationConfiguration_Page2.init(pNode)
                .navigateToBlockNotification()
                .selectSourceName(sourceName.toLowerCase())
                .selectServiceType(serviceName)
                .clickSubmit()
                .selectNotificationTemplate(templateName)
                .clickSave()
                .accept();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("message.record.save", "Verify Notification Template unblocked successfully", pNode);
            }
    } catch (Exception e) {
        Assertion.raiseExceptionAndContinue(e, pNode);
    }
        return this;
    }

    public BlockNotificationConfiguration cancelBlockNotificationTemplate(String sourceName, String serviceName, String[] templateName) throws IOException {

        try{
            Markup m = MarkupHelper.createLabel("cancelBlockNotificationTemplate", ExtentColor.TEAL);
            pNode.info(m);
        BlockNotificationConfiguration_Page2.init(pNode)
                .navigateToBlockNotification()
                .selectSourceName(sourceName.toLowerCase())
                .selectServiceType(serviceName)
                .clickSubmit()
                .selectNotificationTemplate(templateName)
                .clickSave()
                .dismiss();
    } catch (Exception e) {
        Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }
}



