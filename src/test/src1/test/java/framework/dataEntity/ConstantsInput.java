package framework.dataEntity;

/**
 * Created by Dalia on 29-05-2017.
 */
public class ConstantsInput {
    public String Name, Value1, Value2, Value3, Value4;

    public ConstantsInput(String name, String value1, String value2, String value3, String value4) {
        this.Name = name;
        this.Value1 = value1;
        this.Value2 = value2;
        this.Value3 = value3;
        this.Value4 = value4;
    }
}
