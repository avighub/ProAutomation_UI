package framework.dataEntity;

import java.util.List;

/**
 * Created by automation team on 3/14/2019.
 */
public class Partner {
    public String partnerName;
    public List<String> mandatoryList;

    public Partner(String name, List<String> labels){
        this.partnerName = name;
        this.mandatoryList = labels;
    }
}
