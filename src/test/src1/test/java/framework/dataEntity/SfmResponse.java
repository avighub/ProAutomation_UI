package framework.dataEntity;

import com.aventstack.extentreports.ExtentTest;
import com.jayway.restassured.response.ValidatableResponse;
import framework.util.common.Assertion;
import framework.util.propertiesManagement.MessageReader;

import java.io.IOException;
import java.text.MessageFormat;

import static framework.util.jigsaw.CommonOperations.getTransactionDetails;

/**
 * Created by rahul.rana on 9/7/2017.
 */
public class SfmResponse {
    public String Message, TransactionId, Status, ServiceFlow, ServiceRequestId, TxnStatus, errCode, errMessage;
    public ValidatableResponse validatableResponse;
    private ExtentTest pNode;

    public SfmResponse(ValidatableResponse response, ExtentTest t1) {
        t1.info("API RESPONSE : " + response.extract().asString());
        this.pNode = t1;
        this.Message = response.extract().jsonPath().getString("message");
        if (this.Message == null) {
            this.errMessage = response.extract().jsonPath().getString("errors.message").replace("[", "").replace("]", "");
            this.errCode = response.extract().jsonPath().getString("errors.code").replace("[", "").replace("]", "");
        }
        this.TransactionId = response.extract().jsonPath().getString("transactionId");
        this.TxnStatus = response.extract().jsonPath().getString("txnStatus");
        this.Status = response.extract().jsonPath().getString("status");
        this.ServiceFlow = response.extract().jsonPath().getString("serviceFlow");
        this.ServiceRequestId = response.extract().jsonPath().getString("serviceRequestId");
        this.validatableResponse = response;
        if (this.Message == null) {
            this.Message = response.extract().jsonPath().getString("errors.message").replaceAll("\\[|\\]", "");
        }
    }

    public SfmResponse(String id) {
        this.TransactionId = id;
    }

    public boolean assertMessage(String messageCode, ExtentTest t1) throws IOException {
        String message = MessageReader.getMessage(messageCode, null);
        return Assertion.assertEqual(this.Message, message, "Verify Message", t1, false);
    }

    public boolean assertStatus(String statusCode, ExtentTest t1) throws IOException {
        return Assertion.assertEqual(this.Status, statusCode, "Verify Status code", t1, false);
    }

    public SfmResponse assertMessage(String messageCode, String... params) throws IOException {
        try {
            pNode.info("Transaction Status : " + this.Status);
            String message = MessageReader.getMessage(messageCode, null);
            if (params != null && params.length != 0) {
                message = MessageFormat.format(message, params); // dynamic Message
            }

            Assertion.assertEqual(this.Message, message, "Assert API RESPONSE Message", pNode, false);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public SfmResponse verifyMessage(String messageCode, String... params) throws IOException {
        String message = MessageReader.getMessage(messageCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        Assertion.verifyContains(this.Message, message, "Verify Message", pNode, false);
        return this;
    }
    public boolean verifySuccessMessage(String messageCode, String... params) throws IOException {
        String message = MessageReader.getMessage(messageCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        return Assertion.verifyContains(this.Message, message, "Verify Message", pNode, false);
    }

    public SfmResponse verifyErrorMessage(String messageCode, String... params) throws IOException {
        String message = MessageReader.getMessage(messageCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        Assertion.verifyContains(this.errMessage, message, "Verify Error Message", pNode, false);
        return this;
    }

    public SfmResponse verifyErrorCode(String errorCode) throws IOException {
        Assertion.verifyEqual(this.errCode, errorCode, "Verify Error Codes", pNode, false);
        return this;
    }

    public SfmResponse assertStatus(String statusCode) throws IOException {
        pNode.info("Message: " + this.Message);
        Assertion.assertEqual(this.Status, statusCode, "Assert Status code", pNode, false);
        return this;
    }

    public SfmResponse verifyStatus(String statusCode) throws IOException {
        Assertion.verifyEqual(this.Status, statusCode, "Verify API Status", pNode, false);
        return this;
    }

    public SfmResponse verifyTxnStatus(String txnStatus) throws IOException {
        Assertion.verifyEqual(this.TxnStatus, txnStatus, "Verify Txn Status", pNode, false);
        return this;
    }

    public String getSerialNumber() {
        return getTransactionDetails(this.TransactionId).extract().jsonPath().getString("serialNumber");
    }

    public String getServiceRequestId(){
        return this.ServiceRequestId;
    }

    public String getMessage() {
        return this.Message;
    }
}
