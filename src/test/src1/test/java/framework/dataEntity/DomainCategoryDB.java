package framework.dataEntity;

/**
 * Created by rahul.rana on 7/5/2017.
 */
public class DomainCategoryDB {
    public String DomainCode, DomainName, CategoryCode, CategoryName, ParentCategoryCode;
    public Boolean isOperatorDomain = false;
    public Boolean isLeafDomain = false;
    public Boolean isSpecialDomain = false;
    public Boolean isChannelDomain = false;

    /**
     * Data Map From DB
     */
    public DomainCategoryDB(String categoryName, String categoryCode, String domainName, String domainCode, String parentCatCode) {
        this.CategoryName = categoryName;
        this.CategoryCode = categoryCode;
        this.DomainName = domainName;
        this.DomainCode = domainCode;
        this.ParentCategoryCode = parentCatCode;
    }

    public void setDomainAsOperator() {
        this.isOperatorDomain = true;
    }

    public void setDomainAsLeaf() {
        this.isLeafDomain = true;
    }

    public void setDomainAsSpecial() {
        this.isSpecialDomain = true;
    }

    public void setIsChannelDomain() {
        this.isChannelDomain = true;
    }
}
