package framework.dataEntity;

public class BulkRechargeOtherCSV {
    public String serialNum,
            operatorId,
            amount,
            rechargeReceiverMsisdn,
            providerId,
            remark;

    public BulkRechargeOtherCSV(String serialNum,
                                String operatorId,
                                String amount,
                                String rechargeReceiverMsisdn,
                                String providerId,
                                String remark) {
        this.serialNum = serialNum;
        this.operatorId = operatorId;
        this.amount = amount;
        this.rechargeReceiverMsisdn = rechargeReceiverMsisdn;
        this.providerId = providerId;
        this.remark = remark;
    }

}
