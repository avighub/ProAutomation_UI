package framework.dataEntity;

import framework.entity.Bank;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;

import java.util.List;

/**
 * Created by rahul.rana on 5/16/2017.
 */
public class CurrencyProvider {
    public String ProviderName, ProviderId, Currency;
    public boolean IsDefault = false;
    public Bank Bank;

    public CurrencyProvider(String providerName, String providerId, String currency, String isDefProvider, Bank bank) {
        this.ProviderName = providerName;
        this.ProviderId = providerId;
        this.Currency = currency;
        this.IsDefault = (isDefProvider.equalsIgnoreCase("true")) ? true : false;
        this.Bank = bank;
    }

}

