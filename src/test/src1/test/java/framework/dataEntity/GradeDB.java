package framework.dataEntity;

/**
 * Created by rahul.rana on 7/5/2017.
 */
public class GradeDB {

    public String GradeName, GradeCode, CategoryCode;

    public GradeDB(String gradeName, String gradeCode, String catCode) {
        this.GradeName = gradeName;
        this.GradeCode = gradeCode;
        this.CategoryCode = catCode;
    }
}
