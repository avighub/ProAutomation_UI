package framework.dataEntity;

import java.math.BigDecimal;

public class UserWallets {
    public String payId, providerId, isPrimaryStr, status, partyStatus, partyAccessStatus, walletStatus;
    public BigDecimal balance, frozenAmount, fic;
    public boolean isPrimary;

    public UserWallets(String payId, String providerId, String isPrimary, String status) {
        this.payId = payId;
        this.providerId = providerId;
        this.isPrimaryStr = (isPrimary.equalsIgnoreCase("Y")) ? "Primary" : "Not Primary";
        this.isPrimary = (isPrimary.equalsIgnoreCase("Y")) ? true : false;
        this.status = status;
    }

    public UserWallets(String partyStatus, String partyAccessStatus, String walletStatus, BigDecimal balance, BigDecimal frozenAmount, BigDecimal fic, String providerId,
                       String payId) {
        this.partyStatus = partyStatus;
        this.partyAccessStatus = partyAccessStatus;
        this.walletStatus = walletStatus;
        this.balance = balance;
        this.frozenAmount = frozenAmount;
        this.fic = fic;
        this.providerId = providerId;
        this.payId = payId;
    }
}
