package framework.dataEntity;

public class BulkBillPaymentCSV {
    public String serialNum,
            billerCode,
            amount,
            billAccountNum,
            providerId,
            remark;

    public BulkBillPaymentCSV(String serialNum,
                              String billerCode,
                              String amount,
                              String billAccountNum,
                              String providerId,
                              String remark) {
        this.serialNum = serialNum;
        this.billerCode = billerCode;
        this.amount = amount;
        this.billAccountNum = billAccountNum;
        this.providerId = providerId;
        this.remark = remark;
    }

}
