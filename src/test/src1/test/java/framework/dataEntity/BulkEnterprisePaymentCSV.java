package framework.dataEntity;

import framework.util.common.DataFactory;

public class BulkEnterprisePaymentCSV {
    public String msisdn,
            billerCode,
            amount,
            firstName,
            lastName,
            providerId,
            nationalId,
            idNum,
            individualRemark;

    public BulkEnterprisePaymentCSV(String providerId,
                                    String msisdn,
                                    String amount,
                                    String firstName,
                                    String lastName,
                                    String nationalID) {
        this.providerId = providerId;
        this.msisdn = msisdn;
        this.amount = amount;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationalId = nationalID;
        this.idNum = idNum;
        this.individualRemark = "remark"+ DataFactory.getRandomNumberAsString(5);
    }

}
