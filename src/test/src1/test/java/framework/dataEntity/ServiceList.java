package framework.dataEntity;

/**
 * Created by rahul.rana on 5/16/2017.
 */
public class ServiceList {
    public String ServiceName, ServcieType, PayerPaymentType, PayeePaymentType;
    public String PayerPaymentTypeUI = null;
    public String PayeePaymentTypeUI = null;

    public ServiceList(String serviceName, String servcieType, String payerPaymentType, String payeePaymentType) {
        this.ServcieType = servcieType;
        this.ServiceName = serviceName;
        this.PayerPaymentType = payerPaymentType;
        this.PayeePaymentType = payeePaymentType;
    }
}
