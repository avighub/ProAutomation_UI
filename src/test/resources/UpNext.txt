Up Next :
1. DriverFactory: how to create a configurable DriverFactory to get the WebDriver based on the browser preference

   Required:
   a. Explanation on reading property file
   b. Explain About Preference and Capabilities
   c. Explain TestInit and how using testNG it is configured





2. Wait in WebDriver
        a. Implicit Wait and example
        b. Explicit wait and example
        d. Fluent Wait and example